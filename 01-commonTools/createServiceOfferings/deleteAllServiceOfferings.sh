#!/bin/bash

# Copyright(c) 2013 CREATIONLINE,INC. All Right Reserved.

#DEBUG
# echo "  [debug] all args: $@"
# echo "  [debug] arg0: $0"
# echo "  [debug] arg1: $1"
# echo "  [debug] arg2: $2"
# echo "  [debug] arg3: $3"
# echo "  [debug] arg4: $4"
#endDEBUG


##----------------------------------------------------------------

#check for required conf include file
csScriptConfFile="cs-script.conf"
if [ -f "${csScriptConfFile}" ]
then
  source ${csScriptConfFile}
else
  echo -e "configuration error: restartVmOnHaHost.sh requires the following support files in the same directory to function: ${csScriptConfFile} \n"
  exit 1
fi


##----------------------------------------------------------------
# export LC_ALL='C'

# temp files used to save intermediate results between operations
# should be automatically deleted once script is done
rawXmlResponse=temp_apiResponseBuffer
parsedServiceOfferingIds=temp_parsedServiceOfferingIds
parsedServiceOfferingNames=temp_parsedServiceOfferingNames


## "constants" used throughout the script
#script-specific
BRIDGE_STR="\xE2\x87\xA3" #used to bridge operations that are one flow (down arrow char)



# Sends listServiceOfferings request to cs api, with listall parameter
# Note that the issystem=false parameter is also included as a safety measure;
# listServiceOfferings does not seem to include system vm service offerings by default,
# but just to be safe, we will explicitly exclude any system vm service offerings.
# usage: listAllServiceOfferings
# return: raw xml response from cs
# outputs: raw xml response from cs to rawXmlResponse file
#
function listAllServiceOfferings()
{
  execute_command command=listServiceOfferings listall=true issystem=false > $rawXmlResponse

  echo ${rawXmlResponse}
}

# Returns the id(s) of any service offering(s) in the provided listServiceOfferings response xml
# Note that system vm service offerings are explicitly not included as a safety measure
# usage: extractServiceOfferingIds <listServiceOfferingsResponse>
# return: expanded array of service offering uuids
# outputs: parsed output of response to parsedServiceOfferingIds file
#
function extractServiceOfferingIds()
{
  local listServiceOfferingsResponseAsXml=$1
  
  #find id value of every serviceoffering node
  xmllint --shell $listServiceOfferingsResponseAsXml 2>/dev/null > $parsedServiceOfferingIds <<EOL
    cat //serviceoffering/issystem[text()='false']/../id/text()
    quit
EOL
  #read in parsed response, replacing delimiters with a single space
  declare -a serviceOfferingIdList
  serviceOfferingIdList=$(cat $parsedServiceOfferingIds | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")

  echo ${serviceOfferingIdList[@]}
}

# Returns the name(s) of any service offering(s) in the provided listServiceOfferings response xml
# Note that system vm service offerings are explicitly not included as a safety 
# usage: extractServiceOfferingNames <listServiceOfferingsResponse>
# return: expanded array of service offering names
# outputs: parsed output of response to parsedServiceOfferingNames file
#
function extractServiceOfferingNames()
{
  local listServiceOfferingsResponseAsXml=$1
  
  #find name value of every serviceoffering node
  xmllint --shell $listServiceOfferingsResponseAsXml 2>/dev/null > $parsedServiceOfferingNames <<EOL
    cat //serviceoffering/issystem[text()='false']/../name/text()
    quit
EOL
  #read in parsed response,
  #replacing actual spaces with NBSP_PLACEHOLDER (to conserve spaces within the bash array),
  #replacing carriage returns with a single space (to act as array entry delimiter)
  declare -a serviceOfferingNameList
  serviceOfferingNameList=$(cat $parsedServiceOfferingNames | sed -n /[0-9a-zA-Z]/p | sed -e "s/ /${NBSP_PLACEHOLDER}/g" | tr "\n" " ")

  echo ${serviceOfferingNameList[@]}
}

# Sends deleteServiceOffering request cs api with specified service offering id value
# usage: deleteServiceOffering <serviceOfferingId>
# return: raw xml response from cs
# outputs: raw xml response from cs to rawXmlResponse file
#
function deleteServiceOfferingWithId()
{
  local offeringUuid=$1
  execute_command command=deleteServiceOffering id=${offeringUuid} > $rawXmlResponse
  
  echo ${rawXmlResponse}
}





#====================
#(1) get all service offerings from cs
echo -e "Looking on the CloudStack for (non-system vm) service offerings..."
listAllServiceOfferingsResponse=$(listAllServiceOfferings)
declare -a serviceOfferingIdList
declare -a serviceOfferingNameList
serviceOfferingIdList=($(extractServiceOfferingIds ${listAllServiceOfferingsResponse}))
serviceOfferingNameList=($(extractServiceOfferingNames ${listAllServiceOfferingsResponse}))
echo -e "  found the following service offerings:"
for i in "${!serviceOfferingIdList[@]}"; do
  sanitizedNameForDisplay=$(replaceNbspPlaceholderWithSpace ${serviceOfferingNameList[${i}]})
  echo -e "  ${i}.  ${serviceOfferingIdList[${i}]}  (${sanitizedNameForDisplay})"
done
echo -e "  (total of ${#serviceOfferingIdList[@]} service offerings)"
echo -e "...done."

  #check for api errors
  checkForAndExitOnCsApiError $listAllServiceOfferingsResponse
  #check to be sure service offerings were found
  if [ -z "$serviceOfferingIdList" ]
  then
    echo -e "error: service offerings could not be found.\n"
    exit 1
  fi


#====================
#(2) confirm delete action with user
echo -e ${BRIDGE_STR}
echo -e "Do you really wish to delete all ${#serviceOfferingIdList[@]} service offerings listed above?"
echo -e "${bldylw}** This action is unrecoverable! **${txtrst}"
select userInput in "Yes" "No"; do
  case $userInput in
    Yes ) echo -e "Proceeding with deletion"; break;;
    No ) echo -e "Understood, aborting script.  No service offerings were deleted.\n"; exit;;
  esac
done


#====================
#(3) loop and delete all service offerings
echo -e ${BRIDGE_STR}
echo -e "Deleting each service offering..."
for i in "${!serviceOfferingIdList[@]}"; do
  #just delete each service offering in order
  sanitizedNameForDisplay=$(replaceNbspPlaceholderWithSpace ${serviceOfferingNameList[${i}]})
  formattedText="  deleting [${i}] ${serviceOfferingIdList[${i}]} (${sanitizedNameForDisplay})"
  rightFillLine="...................................................................................."
  #the rightFillLine construct fills any leftover spaces to the right of formattedText
  #with dots until a uniform width is reached
  printf "%s%s" "${formattedText}" "${rightFillLine:${#formattedText}}"  
  deleteServiceOfferingWithIdResponse=$(deleteServiceOfferingWithId ${serviceOfferingIdList[${i}]})

    #check for api errors
    checkForAndExitOnCsApiError $deleteServiceOfferingWithIdResponse "[${bldred}FAIL${txtrst}]" 

  #assume delete was success if no error was returned
  printf "[${bldgrn}OK${txtrst}]\n"
done
echo -e "...done"







#(100) cleanup any temp files we created as part of processing
rm -f $rawXmlResponse $queryAsyncJobRawXmlResponse $parsedServiceOfferingIds $parsedServiceOfferingNames

echo -e ""
echo -e "$(basename ${0}) has run to completion."
echo -e ""