#!/bin/bash

# Copyright(c) 2013 CREATIONLINE,INC. All Right Reserved.

#DEBUG
# echo "  [debug] all args: $@"
# echo "  [debug] arg0: $0"
# echo "  [debug] arg1: $1"
# echo "  [debug] arg2: $2"
# echo "  [debug] arg3: $3"
# echo "  [debug] arg4: $4"
#endDEBUG


##----------------------------------------------------------------

#check for required conf include file
csScriptConfFile="cs-script.conf"
if [ -f "${csScriptConfFile}" ]
then
  source ${csScriptConfFile}
else
  echo -e "configuration error: $(basename ${0}) requires the following support files in the same directory to function: ${csScriptConfFile} \n"
  exit 1
fi


##----------------------------------------------------------------
# export LC_ALL='C'

# temp files used to save intermediate results between operations
# should be automatically deleted once script is done
rawXmlResponse=temp_apiResponseBuffer
parsedServiceOfferingIds=temp_parsedServiceOfferingIds


## "constants" used throughout the script
#script-specific
BRIDGE_STR="\xE2\x87\xA3" #used to bridge operations that are one flow (down arrow char)


# This pair of functions, meant to be used together, switch the active IFS delimiter.
# useCommaDelimiter() changes the active IFS delimiter to "," so all subsequent text
# reads or array parses use that character to delimit separate words.  useCommaDelimiter()
# also saves the originally-set IFS, so a call to revertToDefaultDelimiter() will restore
# the original.
# usage: useCommaDelimiter
# return: none
# effects: IFS becomes comma
#
##thsu> currently unused
##ORIGINAL_IFS=$IFS
##function useCommaDelimiter()
##{
##  ORIGINAL_IFS=$IFS
##  IFS=,
##}
# usage: revertToDefaultDelimiter
# return: none
# effect: IFS becomes whatever was saved when useCommaDelimiter() was last called
#
##thsu> currently unused
##function revertToDefaultDelimiter() #thsu> currently unused
##{
##  IFS=$ORIGINAL_IFS
##}

# Parses the comma-delimited input file, one line per service offering.
# For each offering, this function further parses the line,
# extracting specific data values for the offering.
# usage: parseFileForServiceOfferingDefinitions <serviceOfferingsDefinitionFile>
#          serviceOfferingsDefinitionFile = file listing comma-delimited data for
#          each service offering to create, one line per offering of the format: 
#            <cpuNumber> <cpuSpeed> <memory> <hostTags> <name> <displayText> <networkRate> <limitCpuUse>
# return: none
# effects: populates the following global arrays with data parsed from the file:
#            - cpuNumberList
#            - cpuSpeedList
#            - memoryList
#            - hostTagsList
#            - nameList
#            - displayTextList
#            - networkRate
#            - limitCpuUse
#          these global arrays hold the service offering definitions the rest of the script will operate on
declare -a cpuNumberList
declare -a cpuSpeedList
declare -a memoryList
declare -a hostTagsList
declare -a nameList
declare -a displayTextList
declare -a networkRateList
declare -a limitCpuUseList
function parseFileForServiceOfferingDefinitions()
{
  local serviceOfferingsDefinitionFile=$1
  
  #useCommaDelimiter
  local i=0
  while read cpuNumber cpuSpeed memory hostTags name displayText networkRate limitCpuUse status
  do
    #"#EOF" on a line by itself is a reserved word, immediately marking end of input
    if [ "${cpuNumber}" = "#EOF" ]
    then
      break
    fi
    #if first character of line is "#", treat as comment
    if [ "${cpuNumber:0:1}" = "#" ]
    then
      continue
    fi
    #skip all blank lines
    if [ -z "${cpuNumber}" ]
    then
      continue
    fi
  
    #replace <<CPU>>/<<MEM>> placeholders in name with core/memory values
    name="${name//<<CPU>>/${cpuNumber}}"
    name="${name//<<MEM>>/${memory}}"
    #replace <<CPU>>/<<MEM>> placeholders in displayText with core/memory values
    displayText="${displayText//<<CPU>>/${cpuNumber}}"
    displayText="${displayText//<<MEM>>/${memory}}"

    #save offering definition to global vars;
    #these will be the offerings created later in the script
    cpuNumberList[${i}]=${cpuNumber}
    cpuSpeedList[${i}]=${cpuSpeed}
    memoryList[${i}]=${memory}
    hostTagsList[${i}]=${hostTags}
    nameList[${i}]=${name}
    displayTextList[${i}]=${displayText}
    networkRateList[${i}]=${networkRate}
    limitCpuUseList[${i}]=${limitCpuUse}
  
    #echo -e "  [debug] cpuNumberList[${i}]=${cpuNumberList[${i}]}"
    #echo -e "  [debug] cpuSpeedList[${i}]=${cpuSpeedList[${i}]}"
    #echo -e "  [debug] memoryList[${i}]=${memoryList[${i}]}"
    #echo -e "  [debug] hosttagsList[${i}]=${hostTagsList[${i}]}"
    #echo -e "  [debug] nameList[${i}]=${nameList[${i}]}"
    #echo -e "  [debug] displayTextList[${i}]=${displayTextList[${i}]}"
    #echo -e "  [debug] networkRateList[${i}]=${networkRateList[${i}]}"
    #echo -e "  [debug] limitCpuUseList[${i}]=${limitCpuUseList[${i}]}"
    #echo -e "  [debug] --"
    ((i++))
  done < $serviceOfferingsDefinitionFile
  #revertToDefaultDelimiter
}



# Returns the id of the service offering from a createServiceOffering response xml
# usage: extractCreatedServiceOfferingId <createServiceOfferingResponse>
# return: uuid of created offering
# outputs: parsed output of response to parsedServiceOfferingIds file
#
function extractCreatedServiceOfferingId()
{
  local createServiceOfferingResponseAsXml=$1
  
  #find id value of serviceoffering node
  xmllint --shell $createServiceOfferingResponseAsXml 2>/dev/null > $parsedServiceOfferingIds <<EOL
    cat //serviceoffering/id/text()
    quit
EOL
  #read in parsed response, replacing delimiters with a single space
  declare -a serviceOfferingIdList
  serviceOfferingIdList=$(cat $parsedServiceOfferingIds | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")

  echo ${serviceOfferingIdList[0]}
}


# Sends createServiceOffering request cs api with specified parameters
# usage: createServiceOffering <cpuNumber> <cpuSpeed> <memory> <hostTags> <name> <displayText> <networkRate> <limitCpuUse>
# return: raw xml response from cs
# outputs: raw xml response from cs to rawXmlResponse file
#
function createServiceOffering()
{
  local cpuNumber=$1
  local cpuSpeed=$2
  local memoryInGb=$3
  local hostTags=$4
  local name=$5
  local displayText=$6
  local networkRate=$7
  local limitCpuUse=$8
  
  #memory is specified in GB, so convert to bytes before sending to cs
  #(decimals allowed: eg. use memory=0.5 to get 512MB)
  memoryInBytes=$(perl -e "print ${memoryInGb} * 1024")
  
  #the networkrate parameter is optional; only define it for
  #the call below if networkRate has a non-"null" value
  local optionalNetworkRateParameter=""
  if [ "${networkRate}" != "null" ]
  then
    optionalNetworkRateParameter="networkrate=${networkRate}"
  fi
  
  #the limitCpuUse parameter is optional; only define it for
  #the call below if limitCpuUse is true
  local optionalLimitCpuUseParameter=""
  if [ "${limitCpuUse}" == "true" ]
  then
    optionalLimitCpuUseParameter="limitcpuuse=true"
  fi
 
  execute_command command=createServiceOffering \
    cpunumber=${cpuNumber} \
    cpuspeed=${cpuSpeed} \
    memory=${memoryInBytes} \
    hosttags=${hostTags} \
    name=${name} \
    displaytext=${displayText} \
    storagetype=shared \
    offerha=true \
    tags=ROOT_DISK \
    issystem=false \
    ${optionalNetworkRateParameter} \
    ${optionalLimitCpuUseParameter} \
    > $rawXmlResponse 
  
  echo ${rawXmlResponse}
}




#====================
#(0) get script argument
serviceOfferingsDefinitionFile=$1
  
  #check script argument
  if [ ! -e "$serviceOfferingsDefinitionFile" ]
  then
    echo -e "error: Service Offerings definition file argument required."
    echo -e "       Definition file lists space-delimited data for each service offering to create,"
    echo -e "       one line per offering of the format (lines beginning with # are ignored):"
    echo -e "         <cpuNumber> <cpuSpeed> <memory> <hostTags> <name> <displayText> <networkRate> <limitCpuUse>"
    echo -e "       These values are the parameters expected by the createServiceOffering api call."
    echo -e "       <<CPU>> & <<MEM>> placeholders are allowed within the name & displayText values,"
    echo -e "       which will be substituted with real values by this script."
    echo -e "       The networkRate is value is optional; to set no networkrate, specify 'null'."
    outputUsageMessageAndExit=true
  fi
  if [ -n "$outputUsageMessageAndExit" ]
  then
    echo -e "usage: $(basename ${0}) <serviceOfferingsDefinitionFile>\n"
    exit 1
  fi


#====================
#(1) process service offering definition file
parseFileForServiceOfferingDefinitions ${serviceOfferingsDefinitionFile}
#useCommaDelimiter
echo -e "The following service offerings will be created on the CloudStack (${#cpuNumberList[@]} total):"
printf "    %-15s %-5s %-8s %-49s %-5s %-5s\n" "______CPU______" "_Mem_" "__Type__" "_____________________Display_____________________" "_Net_"
printf "    %-4s %-4s %-5s %-5s %-8s %-24s %-24s %-5s\n" "core" "MHz" "limit" "GB" "hosttags" "name" "text" "MBps"
for i in ${!cpuNumberList[@]}
do
  #replace nbsp placeholders with actual spaces for output purposes
  sanitizedNameForDisplay=$(replaceNbspPlaceholderWithSpace ${nameList[${i}]})
  sanitizedDisplayTextForDisplay=$(replaceNbspPlaceholderWithSpace ${displayTextList[${i}]})
  printf "${txtund}%02d${txtrst}. %-4s %-4s %-5s %-5s %-8s %-24s %-24s %-5s\n" ${i} ${cpuNumberList[${i}]} ${cpuSpeedList[${i}]} ${limitCpuUseList[${i}]} ${memoryList[${i}]} ${hostTagsList[${i}]} "${sanitizedNameForDisplay}" "${sanitizedDisplayTextForDisplay}" ${networkRateList[${i}]}
done
#revertToDefaultDelimiter


#====================
#(2) confirm delete action with user
echo -e ${BRIDGE_STR}
echo -e "Do you wish proceed with creating the service offerings above?"
select userInput in "Yes" "No"; do
  case $userInput in
    Yes ) echo -e "Proceeding with creation"; break;;
    No ) echo -e "Understood, aborting script.  No service offerings were created.\n"; exit;;
  esac
done


#====================
#(3) loop and create all service offerings
echo -e ${BRIDGE_STR}
echo -e "Creating each service offering on the CloudStack..."
for i in "${!cpuNumberList[@]}"; do
  #replace nbsp placeholders with actual spaces for output purposes
  sanitizedNameForDisplay=$(replaceNbspPlaceholderWithSpace ${nameList[${i}]})
  formattedText="  ${sanitizedNameForDisplay} (${cpuNumberList[${i}]}core, ${cpuSpeedList[${i}]}Mhz, ${memoryList[${i}]}GB, ${networkRateList[${i}]}MBps, ${hostTagsList[${i}]}, ${limitCpuUseList[${i}]}) [${i}]"
  #the rightFillLine construct fills any leftover spaces to the right of formattedText
  #with dots until a uniform width is reached
  rightFillLine="...................................................................................."
  printf "%s%s" "${formattedText}" "${rightFillLine:${#formattedText}}"  
  #call with non-replaced nbsp placeholders as they will need to be replaced with url-encoded space
  createServiceOfferingResponse=$(createServiceOffering ${cpuNumberList[${i}]} ${cpuSpeedList[${i}]} ${memoryList[${i}]} ${hostTagsList[${i}]} ${nameList[${i}]} ${displayTextList[${i}]} ${networkRateList[${i}]} ${limitCpuUseList[${i}]})

    #check for api errors
    checkForAndExitOnCsApiError $createServiceOfferingResponse "[${bldred}FAIL${txtrst}]" 
    
  #create successful if no errors in response found
  uuidOfCreatedOffering=$(extractCreatedServiceOfferingId $createServiceOfferingResponse)
  printf "[${bldgrn}OK${txtrst}]  ${uuidOfCreatedOffering} \n"
done
echo -e "...done"







#(100) cleanup any temp files we created as part of processing
rm -f $rawXmlResponse $queryAsyncJobRawXmlResponse $parsedServiceOfferingIds

echo -e ""
echo -e "$(basename ${0}) has run to completion."
echo -e ""
