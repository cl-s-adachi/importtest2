#!/bin/sh
#########################################################################################
#
# Clusterを指定して配下のhostの設定設定もしくは参照する
#  CSV のフォーマットは（ClusterName,hostName,hosttagString,oscategoryNameString）の４項目
#
#  引数:update Clusterを指定して配下のhostの設定をCSVの設定ファイルから抜き出してhosttagとOS基本設定を設定する
#  引数:update-tag　Clusterを指定して配下のhostの設定をCSVの設定ファイルから抜き出してhosttagを設定する
#  引数:なし。Clusterを指定するとその配下のHostの設定が表示できる
#
#########################################################################################

IP=ck2wjih-pcldb00
KICK="/home/ckkcl/script"
csvfile=`dirname ${0}`/updatehostdata.csv
readonly LOGFILE=`dirname ${0}`/host_setting.sh.log
Num=0

#########################################################################################
# 関数:target_cluster_list_host
# CloudStackAPIのlisthostsから指定clustrtidのデータを引き出して表示する
# $1 指定clustrtid(cluster.id)
# $2 ログ出力パス
#
#########################################################################################
function target_cluster_list_host(){
  local targetClustertId="$1"
  local logfilePath=$2
  if [ targetClustertId = "" ]
  then
    echo "引数を指定して下さい。" |tee -a ${logfilePath}
    # 中断。
    exit
  fi
  #CpusterIDを検索条件にMysqlからデータを取得する（まず表示あとで消す）
  mysql -u root cloud -h ${IP} -e "select h.id,h.name,h.status,t.tag from host h left outer join host_tags t on h.id = t.host_id where h.removed is NULL and h.cluster_id=${targetClustertId};" |tee -a ${logfilePath}
  echo "API経由での一覧情報" |tee -a ${logfilePath}
  #hostIDでのid一覧を取得する
  local hostIdListCMD="echo 'select h.id from host h left outer join host_tags t on h.id = t.host_id where h.removed is NULL and h.cluster_id=${targetClustertId};'| mysql -u root cloud -h ${IP} -N"
  local hostIDList=(`eval $hostIdListCMD`)

  for hostId in "${hostIDList[@]}"
  do
    # タグデータとOS基本設定をAPIから取得する
    local hostNameCMD="echo 'select name from host where id = ${hostId};'| mysql -u root cloud -h ${IP} -N"
    local hostname=(`eval $hostNameCMD`)
    local hostTag=`${KICK}/kick_api.sh command=listHosts id=${hostId} 2>/dev/null | grep "<hosttags>" | awk -F ">" '{print $2}' | awk -F "<" '{print $1}'`
    local hostTagView=$(printf "%-25s" $hostTag)
    local hostOs=`${KICK}/kick_api.sh command=listHosts id=${hostId} 2>/dev/null | grep "<oscategoryname>" | awk -F ">" '{print $2}' | awk -F "<" '{print $1}'`
    echo "hostid:$hostId | hostname:$hostname | hostTag:$hostTagView | hostOS: $hostOs" |tee -a ${logfilePath}
  done
}


#########################################################################################
#main処理
#########################################################################################
#ZONE選択
echo "対象ZONEの選択" |tee ${LOGFILE}
mysql -u root cloud -h ${IP} -e "select id,name from data_center where removed is NULL"|tee -a ${LOGFILE}

echo -n "please select zone number:"|tee -a ${LOGFILE}
read input_zone

echo ${input_zone}|tee -a ${LOGFILE}
echo
echo "対象クラスタの選択" |tee -a ${LOGFILE}

echo "[Cluster]"
mysql -u root cloud -h ${IP} -e "select id,name,hypervisor_type,allocation_state,managed_state from cluster where removed is NULL and data_center_id=${input_zone};"|tee -a ${LOGFILE}
echo -n "please input clustrtid:"|tee -a ${LOGFILE}
read input_cluster

#クラスタIDをもとに配下の表示を行う(ログファイルはcluster名を付加する)
logfileCMD="echo 'select name from cluster where id=${input_cluster};'| mysql -u root cloud -h ${IP} -N"
logfile1=(`eval $logfileCMD`)
logfile=(`eval echo ${logfile1}|awk -F '/' '{print $2$3}'`)
logfilepath=`dirname ${0}`/${logfile}.log
echo ${input_cluster}|tee ${logfilepath}
echo
echo "指定クラスタの状態表示"|tee -a ${logfilepath}
target_cluster_list_host $input_cluster $logfilepath

# 更新指定時動作
while [ "$1" == "update" ]
do
  # 更新前確認
  echo
  echo "CSVデータによるHostTagとOS基本設定の更新を実行しますか? [y/n] "|tee -a ${logfilepath}
  read UPDATEYN
  echo
  YN=`echo ${UPDATEYN} |tr "[a-z]" "[A-Z]"`
  echo ${UPDATEYN}|tee -a ${logfilepath}
  case $YN in
    Y) echo "更新を実行します。";;
    *) echo "シェルを中止します。"
       exit;;
  esac

  #hostIDでのid一覧を取得する
  hostIdListCMD="echo 'select h.id from host h left outer join host_tags t on h.id = t.host_id where h.removed is NULL and h.cluster_id=${input_cluster};'| mysql -u root cloud -h ${IP} -N"
  hostIDList=(`eval $hostIdListCMD`)

  for hostId in "${hostIDList[@]}"
  do
    # タグデータとOS基本設定をAPIから取得する
    hostNameCMD="echo 'select name from host where id = ${hostId};'| mysql -u root cloud -h ${IP} -N"
    hostname=(`eval $hostNameCMD`)
    targetClustername=`${KICK}/kick_api.sh command=listClusters id=${input_cluster} 2>/dev/null | grep "<name>" | awk -F ">" '{print $2}' | awk -F "<" '{print $1}'`

    #CSV取得(ClusterName,hostName,hosttagString,oscategoryNameString)
    for line in `cat ${csvfile} | grep -v ^#`
    do
      csvClustername=`echo ${line} | cut -d ',' -f 1`
      csvHostname=`echo ${line} | cut -d ',' -f 2`
      csvHosttag=`echo ${line} | cut -d ',' -f 3`
      csvHostype=`echo ${line} | cut -d ',' -f 4`

      if [ $csvClustername = $targetClustername ]
      then
        if [ $csvHostname = $hostname ]
        then
          # CSV設定がターゲットclustername
          targetOscategoryidCMD="echo 'select uuid from guest_os_category where name =\"${csvHostype}\";'| mysql -u root cloud -h ${IP} -N"
          targetOscategoryid=(`eval $targetOscategoryidCMD`)
          echo updateSetParam id=$hostId[$csvHostname] hosttags=$csvHosttag oscategoryid=$targetOscategoryid[$csvHostype]|tee -a ${logfilepath}
           ${KICK}/kick_api.sh command=updateHost id=$hostId hosttags=$csvHosttag oscategoryid=$targetOscategoryid
          usleep 80000
        fi
      fi
    done
  done

  echo
  echo "更新後の指定クラスタのHostの登録状態"|tee -a ${logfilepath}
  target_cluster_list_host $input_cluster $logfilepath
  echo
  echo "処理を終了します"|tee -a ${logfilepath}
  exit
done


while [ "$1" == "update-tag" ]
do
  # 更新前確認
  echo
  echo "CSVデータによるHostTagの更新を実行しますか? [y/n] "|tee -a ${logfilepath}
  read UPDATEYN
  echo
  YN=`echo ${UPDATEYN} |tr "[a-z]" "[A-Z]"`
  echo ${UPDATEYN}|tee -a ${logfilepath}
  case $YN in
    Y) echo "更新を実行します。";;
    *) echo "シェルを中止します。"
       exit;;
  esac

  #hostIDでのid一覧を取得する
  hostIdListCMD="echo 'select h.id from host h left outer join host_tags t on h.id = t.host_id where h.removed is NULL and h.cluster_id=${input_cluster};'| mysql -u root cloud -h ${IP} -N"
  hostIDList=(`eval $hostIdListCMD`)

  for hostId in "${hostIDList[@]}"
  do
    # タグデータとOS基本設定をAPIから取得する
    hostNameCMD="echo 'select name from host where id = ${hostId};'| mysql -u root cloud -h ${IP} -N"
    hostname=(`eval $hostNameCMD`)
    targetClustername=`${KICK}/kick_api.sh command=listClusters id=${input_cluster} 2>/dev/null | grep "<name>" | awk -F ">" '{print $2}' | awk -F "<" '{print $1}'`

    #CSV取得(ClusterName,hostName,hosttagString,oscategoryNameString)
    for line in `cat ${csvfile} | grep -v ^#`
    do
      csvClustername=`echo ${line} | cut -d ',' -f 1`
      csvHostname=`echo ${line} | cut -d ',' -f 2`
      csvHosttag=`echo ${line} | cut -d ',' -f 3`
      #csvHostype=`echo ${line} | cut -d ',' -f 4`

      if [ $csvClustername = $targetClustername ]
      then
        if [ $csvHostname = $hostname ]
        then
          # CSV設定がターゲットclustername
          #targetOscategoryidCMD="echo 'select uuid from guest_os_category where name =\"${csvHostype}\";'| mysql -u root cloud -h ${IP} -N"
          #targetOscategoryid=(`eval $targetOscategoryidCMD`)
          echo updateSetParam id=$hostId[$csvHostname] hosttags=$csvHosttag|tee -a ${logfilepath}
           ${KICK}/kick_api.sh command=updateHost id=$hostId hosttags=$csvHosttag
          usleep 80000
        fi
      fi
    done
  done

  echo
  echo "更新後の指定クラスタのHostの登録状態"|tee -a ${logfilepath}
  target_cluster_list_host $input_cluster $logfilepath
  echo
  echo "処理を終了します"|tee -a ${logfilepath}
  exit
done
