#! /usr/bin/env python
# coding: utf-8
#
# updateHostByAnyHosttagsKvmOnly.py
# ホストタグを一括変更するスクリプト
# removedがNULLかつ、KVMonly
#
# - usage
# ./updateHostByAnyHosttags.py <change_from_tag> <change_to_tag>
# 引数未指定時：全てのKVMホストの情報を表示
# 第1引数指定時：指定したホストタグのホスト情報を表示
# 第2引数指定時：第1引数で指定したホストタグのホスト情報を表示し、
#                第2引数で指定したホストタグに変更
#
# - note
# http通信を行う勉強用に作っただけなので、可読性無視。
# updateHostの戻り値はチェックしていません。
# スクリプト実行後、第1引数のみ指定してホストタグが変わっているかチェックすること。
# 全agent停止する際にHA->araiタグに変えてるけど、今後その作業があれば使えるかもスクリプト。
#
# - date
# 2013/12/16 ver1.0(new)
# 2014/02/01 ver1.1(KVMホストのみ変更可能なように変更)
#
# - author
#  taira

## import lib
import MySQLdb
import sys

import urllib
import urllib2
import commands

## common parameter
# url parameter
url = 'http://localhost:8080'
path = '/client/api?'
# key
apikey = 'bV7-Bboaco4OGNNi_YCvOXVFbCut1xZEbj3HyvG82tx0zkH0I-cMKk62dvMwcA-H7n4VsN5Jy62zxjtmVVJ5Pw'
secret_key = 'P3QqoKbgJ_zniM-ugOrK5lE9fkaulzLk0a2FbrJpciGRT5LLmVK5YqbuHwOZHP6NzIxUvE_ttOauAXZCBuUMaQ'
## db server
dbHost = 'tckktky4-vcldb01'
dbUser = 'root'
dbName = 'cloud'
dbPasswd = ''

### execution

# コマンドライン引数取得
# 未指定時は空白
argvs = sys.argv        # コマンドライン引数リスト
argc = len(argvs)       # 引数の個数

# SQL文ひな形
execSQL = "select ht.tag, h.name, h.uuid, h.status, h.removed FROM host as h " + \
          "INNER JOIN host_tags as ht ON h.id = ht.host_id" + \
          " WHERE h.type='Routing' AND h.removed IS NULL AND h.hypervisor_type='KVM'"

# 引数チェック
if (2 < argc):
        # 第1引数を変更元のホストタグとして設定
        execSQL+= ' AND ht.tag ='
        execSQL+='"'
        execSQL+=argvs[1]
        execSQL+='"'
        execSQL+=';'
        # 第2引数を変更先のホストタグとして設定
        changeTag = argvs[2]
elif (2 == argc):
        # 第1引数を変更元のホストタグとして設定
        execSQL+= ' AND ht.tag ='
        execSQL+='"'
        execSQL+=argvs[1]
        execSQL+='"'
        execSQL+=';'
else:
        # 引数が2未満の場合は、全ホスト対象
        execSQL += ';'

# MySQL接続
connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
cur = connect.cursor()

# 特定のホストタグのついたホスト情報を取得
cur.execute(execSQL)

# ホスト情報表示 & ホストタグ変更用にホストIDリスト作成
hostidList = [];
rows = int(cur.rowcount)
print('No', 'host tag', 'status', 'host uuid', 'host name' )
for x in range(0, rows):
        row = cur.fetchone()
        print(x, row[0], row[3], row[2], row[1])
        hostidList.append(row[2])

cur.close()
connect.close()

if (2 >= argc):
        sys.exit()

if (0 == len(hostidList)):
        print("該当ホストタグのホストがありません")
        sys.exit()

# hostのタグ変更を実施するかの確認
while True:
        res = raw_input('ホストタグ変更を実施しますか？(y/n)')
        if res == 'y':
                print("change host tags")

                for x in hostidList:
                        command = 'updateHost'

                        ## create signature ( need a called CloudStackAPI)
                        signature = 'apikey=' + apikey.lower() + '&' + 'command=' + command.lower() + '&' + 'hosttags=' + changeTag.lower() + '&' + 'id=' + x
                        cmd = 'echo -n' + ' ' + "'" + signature + "'" + ' | ' + 'openssl sha1 -binary -hmac' + ' ' + secret_key + ' | ' + 'openssl base64'
                        signature = commands.getoutput(cmd)

                        # HTTP request parameter
                        params = {
                                'command' : command,
                                'id' : x,
                                'hosttags' : changeTag,
                                'apikey' : apikey,
                                'signature' : signature
                        }
                        params = urllib.urlencode(params)

                        # send http get request
                        req = urllib2.urlopen(url + path + params)

                        print("changed host tag"),
                        print(":id="),
                        print(x)
                sys.exit()

        elif res == 'n':
                print("no changed host tags. bye")
                sys.exit()
        else:
                print("ホストタグ変更を実施しますか？")
