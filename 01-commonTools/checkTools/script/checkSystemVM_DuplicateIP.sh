#!/bin/sh

BASEDIR=`dirname $0`
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`

logger -p local0.info -t CloudStack.INFO "Start checkSystemVM_DuplicateIP.sh"

IPaddresses=`mysql -u root cloud -h ${DBHOST} -N -e 'select nics.ip4_address from nics where removed is null and reserver_name="PublicNetworkGuru" group by nics.ip4_address having count(*) > 1;'`

if [ x"${IPaddresses}" == "x" ];then
  continue
else
  for IPaddress in ${IPaddresses}
  do
    VMname=`mysql -u root cloud -h ${DBHOST} -N -B -e "select vm_instance.name from vm_instance,nics where vm_instance.id=nics.instance_id and vm_instance.removed is null and nics.ip4_address='${IPaddress}';" | tr "\n" "," | sed s/,$//`
    logger -p local0.warn -t CloudStack.ERROR "IP is duplicated: ${VMname}/${IPaddress}"
  done
fi

logger -p local0.info -t CloudStack.INFO "End checkSystemVM_DuplicateIP.sh"
