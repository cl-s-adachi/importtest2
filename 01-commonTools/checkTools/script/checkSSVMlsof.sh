#!/bin/bash

#KCPS2 20180418
######################################################
BASEDIR=`dirname $0`
TEMPDIR="${BASEDIR}/tmp"
mkdir -p ${TEMPDIR}
LOGFILE="${TEMPDIR}/SSVMlsofCheck.log"
OPENNUM="${TEMPDIR}/lsofOpenNum.list"
SSVMs="${TEMPDIR}/SSVMs.list"
RESULT="${TEMPDIR}/lsofResult.list"

KICK_API=${BASEDIR}/./kick_api.sh
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`

SSHLOGIN=${BASEDIR}/tool/SshTool/SshSSVM.sh

timeout=300

lsofnum=3276
#lsofnum=900

INFO_LV="[INFO]"
WARN_LV="[WARN]"
ERROR_LV="[ERROR]"
TAG="[CloudStack]"
SCRIPT="`basename $0`"

#######################################################

logger -t "${INFO_LV} ${TAG}" "Start ${SCRIPT}"
#echo "Openが${lsofnum}以上の場合は対象のSSVMを再起動してください。"

getAllSsvmList(){
        SSVMList=`mysql -u root cloud -h ${DBHOST} -N -e 'select name from host where type="SecondaryStorageVm" and removed is null and status="Up";'`
}

getSsvmListFromZonename(){
        SSVMList=`mysql -u root cloud -h ${DBHOST} -N -e "select host.name from host,data_center where host.data_center_id=data_center.id and host.removed is null and host.status=\"Up\" and host.type=\"SecondaryStorageVm\" and data_center.name=\"${zonename}\";" `
}

getSsvmPublicIps(){
PublicIps=`mysql -u root cloud -h ${DBHOST} -N -e "select public_ip_address from host where type='SecondaryStorageVM' and removed is null;"`

}

execCommandToSSVM(){
echo ${CMD}
echo ${SSVMList}
for SSVM in ${SSVMList}
 do
        echo ${SSHLOGIN} ${SSVM}
        expect -c "
set timeout ${timeout}
spawn ${SSHLOGIN} ${SSVM}
expect \"Are you sure you want to continue connecting (yes/no)?\" {
   send \"Yes\n\"
   expect \"root@s*-VM:~# \"
   send \"${CMD}\n\"
}  \"root@s*-VM:~# \" {
   send \"${CMD}\n\"
}
expect \"root@s*-VM:~# \"
send \"exit \n\"
expect \"*@*\"
send \"exit \n\"
expect \"*@*\"
send \"exit \n\"
"

 done
} > ${LOGFILE}

opt_flg="a"

while getopts :z::n::a OPT
do
 case ${OPT} in
  "z" ) opt_flg="z" ; zonename="${OPTARG}";;
  "n" ) opt_flg="n" ; ssvmname="${OPTARG}";;
  "a" ) opt_flg="a";;
 esac
done
shift $((OPTIND - 1))

case ${opt_flg} in
  "z") getSsvmListFromZonename;;
  "n") SSVMList=${ssvmname};;
  "a") getAllSsvmList;;
esac

case "$1" in
 "")
	lsof="lsof |egrep '^java'  |egrep -v 't identify protocol' |egrep -Ev '/usr/.*jar' |egrep -Ev '/lib/.*so' |wc -l"
        CMD="
echo -n 'SSVM:' > /var/tmp/lsof.log; 
hostname >> /var/tmp/lsof.log;
echo -n 'Open:' >> /var/tmp/lsof.log;
${lsof} >> /var/tmp/lsof.log;
cat /var/tmp/lsof.log;
rm -f /var/tmp/lsof.log"
	execCommandToSSVM
	;;

*)
        echo $"Usage: $0 Please Execute without arguments!"
esac

########################################################

cat ${LOGFILE} | grep -e "^SSVM:" -e "^Open:" > ${RESULT}
grep "^SSVM:" ${LOGFILE} > ${SSVMs}
grep "^Open:" ${LOGFILE} > ${OPENNUM}

a=1
cat ${OPENNUM} | while read line
do
  open=$(awk -F":" '{print $2}' <<<${line})
  open2=`echo ${open} | sed -e "s/[\r\n]\+//g"`
#  echo "Open: $open2"
  if [ $open2 -ge $lsofnum ]; then
    S=`sed -n ${a}P ${SSVMs}`
    SS=`echo ${S} | sed -e "s/[\r\n]\+//g"`
   
    logger -t "${WARN_LV} ${TAG}" "lsof exceeds the threshold! :${SS}"
#    grep -B1 "$open" ${RESULT} | grep SSVM | awk -F":" '{print $2}'
#  else
#    echo -n "[OK]"
#    grep -B1 "$open" ${RESULT} | grep SSVM | awk -F":" '{print $2}'
  fi
  a=$(($a+1))
#  echo "OPEN: ${open}"
#  echo "---"
done

logger -t "${INFO_LV} ${TAG}" "End ${SCRIPT}"
