#!/bin/sh

BASEDIR=`dirname $0`
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`

logger -p local0.info -t CloudStack.INFO "Start checkSystemVM_PremiumHost.sh"

VRNames=`mysql -u root cloud -h ${DBHOST} -N -e 'select vm_instance.name from vm_instance,host,cluster where vm_instance.host_id=host.id and host.cluster_id=cluster.id and vm_instance.removed is null and cluster.id in (select cluster.id from host,host_tags,cluster where host.id=host_tags.host_id and host.cluster_id=cluster.id and host.removed is null and (vm_instance.type="DomainRouter" or vm_instance.type="SecondaryStorageVm" or vm_instance.type="ConsoleProxy") and ( host_tags.tag like "%@%" or host_tags.tag="PREMIUM") group by cluster_id);'`

if [ x"${VRNames}" == "x" ] ; then 
   continue
else 
  for VRName in ${VRNames}
  do
     logger -p local0.warn -t CloudStack.ERROR "VR or SystemVM has Running on PREMIUM HA HOST: ${VRName}"
  done
fi

logger -p local0.info -t CloudStack.INFO "End checkSystemVM_PremiumHost.sh"
