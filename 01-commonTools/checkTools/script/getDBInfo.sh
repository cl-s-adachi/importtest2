#!/bin/sh
X_CMD(){ # user pw host cmd ##  ssh ログインしてコマンドを実行するshell関数
    local U=$1 ; shift # user
    local PW=$1 ; shift # password
    local H=$1 ; shift # host
    local PR='(#|\\$) $'  # prompt regular expression
    expect -c "
    set timeout 20
    spawn ssh -l $U $H
    while (1) {
      expect timeout { break } \"(yes/no)?\" { sleep 1;send \"yes\r\" } \"word: \" { sleep 1;send \"$PW\r\" } -re \"$PR\" { sleep 1;send \"\r\";break }
    }
    expect -re \"$PR\" ; sleep 1; send \"$*\r\"
    expect -re \"$PR\" ; sleep 1; send \"exit\r\"
    "
}

subcmd=$1

if [ "x$subcmd" == "x"  ];then
   echo $"Usage: $0 {ALL|ActiveName|StandbyName}"
   exit 2
fi


# Production
# DB VIP
#VIP="10.189.0.15"
# DB HOST 1 IP ADDRESS
#DB1="ckktky4-pcldb01"
# DB HOST 2 IP ADDRESS
#DB2="ckktky4-pcldb02"
# USER ID
#USERID="ckkcl"
# USER PASSWORD
#PASS="CKK@6004"

# Development
# DB VIP
VIP="10.193.128.111"
# DB HOST 1 IP ADDRESS
DB1="tckkejo2-vcldb01"
# DB HOST 2 IP ADDRESS
DB2="tckkejo2-vcldb02"
# USER ID
USERID="tckkcl"
# USER PASSWORD
PASS="CKK@6004"

 X_CMD $USERID $PASS $DB1 ifconfig | grep $VIP > /dev/null
  if [ $? -eq 0 ] ; then
   # "DB1 Active"
   ACTIVE_NAME=$DB1
   STANDBY_NAME=$DB2
  else
   # "DB2 Active"
   ACTIVE_NAME=$DB2
   STANDBY_NAME=$DB1
  fi

case "$subcmd" in
 ALL)
   echo "Active Name:"$ACTIVE_NAME
   echo "Standby Name:"$STANDBY_NAME
 ;;
 ActiveName)
   echo $ACTIVE_NAME
 ;;
 StandbyName)
   echo $STANDBY_NAME
 ;;
 *)
   echo $"Usage: $0 {ALL|ActiveName|StandbyName}"
   exit 2
esac
