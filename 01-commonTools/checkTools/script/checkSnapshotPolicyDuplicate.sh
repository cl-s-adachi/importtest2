#!/bin/bash

#########################################
BASEDIR=`dirname $0`
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`
DBUSER="root"
database="cloud"
inter="\`interval\`"

TMPDIR="${BASEDIR}/tmp"
mkdir -p ${TMPDIR}
Vid="${TMPDIR}/vid.list"
policys="${TMPDIR}/policy.list"

INFO_LV="[INFO]"
WARN_LV="[WARN]"
ERROR_LV="[ERROR]"
TAG="[CloudStack]"
SCRIPT="`basename $0`"

########################################

#####################
##main##
#####################

logger -t "${INFO_LV} ${TAG}" "Start ${SCRIPT}"

rm -f ${Vid}
rm -f ${policys}

mysql -u ${DBUSER} ${database} -h ${DBHOST} -N -e "select volume_id from snapshot_policy group by volume_id,schedule,timezone,${inter},max_snaps having count(*) > 1;" > ${Vid}

while read line
do
  mysql -v -u ${DBUSER} ${database} -h ${DBHOST} -t -e "select * from snapshot_policy where volume_id in (select volume_id from snapshot_policy where volume_id=$line) and schedule in (select schedule from snapshot_policy where volume_id=$line) and timezone in (select timezone from snapshot_policy where volume_id=$line) and ${inter} in (select ${inter} from snapshot_policy where volume_id=$line)" >> ${policys}

  if [ -s ${Vid} ]; then
    logger -t "${WARN_LV} ${TAG}" "Snapshot_policy of volume_id=$line is set to duplicate. Please,use API to delete duplicate policy."
  fi
done < ${Vid}

logger -t "${INFO_LV} ${TAG}" "End ${SCRIPT}"
