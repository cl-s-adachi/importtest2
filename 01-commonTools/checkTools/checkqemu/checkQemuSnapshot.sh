#!/bin/bash

qemuimgListFile=$1
hostName=`hostname -s`

if [ ! -e ${qemuimgListFile} ] ; then
 exit 0
fi

for qemuimg in `grep ${hostName} ${qemuimgListFile} | awk '{print $2}'`
do
   result=`qemu-img snapshot -l ${qemuimg}`
   if [ "${result}" != "" ] ; then
      echo "# qemu-img snapshot -l ${qemuimg}"
      echo ${result} 
   fi
done

