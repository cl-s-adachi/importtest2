#!/bin/bash

basedir=`dirname $0`
temp=${basedir}/tmp
clusterList=${temp}/clusterList
psList=${temp}/psList
haHost=${temp}/haHost
hostList=${temp}/hostList
volumeList=${temp}/volumeList
targetDir=${basedir}/target/
qemuPathList=qemupathlist
vmList=vmlist

scriptdir=${HOME}/script
DBHOST=`${scriptdir}/getDBInfo.sh StandbyName`

#KVMクラスターの情報(id,name)を取得
getKvmClusterList(){
	mysql -u root cloud -h ${DBHOST} -N -e 'select id,name from cluster where removed is null and hypervisor_type="KVM";' > ${clusterList} 
}

#クラスターをキーとしてプライマリストレージの情報(id,name)を取得
getPsListFromCluster(){
	mysql -u root cloud -h ${DBHOST} -N -e "select id,name from storage_pool where cluster_id=${clusterId};" > ${psList}
}

#クラスターをキーとしてHAホストの情報(id,name)を1台のみ取得
getHAHostFromCluster(){
	mysql -u root cloud -h ${DBHOST} -N -e "select host.id,host.name from host,host_tags where host.id=host_tags.host_id and host_tags.tag='HA' and host.cluster_id=${clusterId} and host.removed is null limit 1;" > ${haHost}
}

#プライマリストレージのマウントポイントを取得
getMountFromPsAndHAHost(){
	mountpoint=`mysql -u root cloud -h ${DBHOST} -N -e "select local_path from storage_pool_host_ref where host_id=${hostId} and pool_id=${psId};"`
}

#プライマリストレージをキーとしてボリュームの情報(id,path)を取得
getVolumeListFromPs(){
	mysql -u root cloud -h ${DBHOST} -N -e "select id,path from volumes where pool_id=${psId} and removed is null;" > ${volumeList}
}

#クラスターからホストの情報(id,name)のリストを取得
getHostsFromCluster(){
       mysql -u root cloud -h ${DBHOST} -N -e "select id,name from host where removed is null and cluster_id=${clusterId};" > ${hostList}
}

#ホストからインスタンスの情報(instance_name)のリストを取得
getVmListFromHost(){
      mysql -u root cloud -h ${DBHOST} -N -e "select instance_name from vm_instance where hypervisor_type='KVM' and vm_type='User' and removed is null and host_id=${hostId};" > ${targetDir}/${vmList}.${hostName}
}


getKvmClusterList

for clusterId in `awk '{print $1}' ${clusterList}`
do
   clusterName=`grep -w ${clusterId} ${clusterList} | awk '{print $2}'`
   getPsListFromCluster
   getHAHostFromCluster
   getHostsFromCluster
   hostId=`awk '{print $1}' ${haHost}`
   hostName=`awk '{print $2}' ${haHost} | awk -F. '{print $1}'`
   if [ "${hostId}" == "" ] ; then
     echo "${clusterName}にHAホストが存在しません"
   else
    cp /dev/null ${targetDir}/${qemuPathList}.${hostName}
    for psId in `awk '{print $1}' ${psList}`
    do
       getMountFromPsAndHAHost
       getVolumeListFromPs 
       for volume in `awk '{print $2}' ${volumeList}`
       do
          #HAホスト名 ボリュームのパスを出力 
          echo ${hostName} ${mountpoint}/${volume} >> ${targetDir}/${qemuPathList}.${hostName}
       done
     done
   fi

   for hostName in `awk '{print $2}' ${hostList} | awk -F. '{print $1}'` 
    do
     cp /dev/null ${targetDir}/${vmList}.${hostName}
     hostId=`grep ${hostName} ${hostList} | awk '{print $1}'` 
     getVmListFromHost
    done

    hosts=`awk '{print $2}' ${hostList} | awk -F. '{print $1}'`
    echo 'role :targets, *%w('${hosts}')' > ${targetDir}/hosts.${clusterName}.rb
done
 
