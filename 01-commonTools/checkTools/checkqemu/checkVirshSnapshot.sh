#!/bin/bash

vmListFile=$1
hostName=`hostname -s`

for vm in `cat ${vmListFile}`
do
   result=`virsh snapshot-list ${vm} | grep running`
   if [ "${result}" != "" ] ; then
      echo "# virsh snapshot-list ${vm}"
      echo ${result} 
   fi
done
