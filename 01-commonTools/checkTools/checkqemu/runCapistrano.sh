#!/bin/bash

#####################################################
#
# capistrano 実行スクリプト
# usage. runCapistrano.sh [-t rbfile-keyword] -e EXECOMMAND
# -t : 対象rbファイルの前方一致キーワード(ex:hosts.v-tckktky4) *未指定時は全rbファイル対象
# -e : capistranoのコマンド名(ex:cloud_agent:status)
# ex. ./runCapistrano.sh [-t hosts.v-tckktky4] -e cloud_agent:status
#
####################################################

### 共通パラメータ

## 対象のrbファイルが格納されているディレクトリ
DIR="./target"

## capistrano実行時のrbファイル名
CAPFILE="hosts.rb"

## 1つ1つrbファイル実行時の確認MSG表示確認
#isExecutionConfirm=true
isExecutionConfirm=false

## debug情報出力オプション
#isDebug=true
isDebug=false

### define API's ###

## ファイル削除
deleteFile() {
	if [ -e $1 ]; then
		`rm $1`
	fi
}

## 処理確認
confirmExecution() {
	while :
	do
		echo -n "$1"
		read ans
		case $ans in
			[yY]) return 0;;
			[nN]) return 1;;
			*) continue;;
		esac
	done
}

## usage表示&エラー終了
usageExit() {
	echo "Usage: $CMDNAME -e VALUE [-t VALUE]"
	exit 1
}

## 処理対象のファイル一覧表示
showFiles() {
	CNT=0
	echo "[INFO] target files"
	for FILE in `ls $1`
	do
		CNT=`expr $CNT + 1`

		# 5件を1行に表示する.
		if [ `expr $CNT % 5` -eq 0 ]; then
			# ファイル名を表示
			echo ${FILE##*/}
		else
			echo -en ${FILE##*/}"\t"
		fi
	done
	echo " "
}

## デバッグ情報出力関数
printDebug() {

	# デバッグモードが有効な場合のみ出力
	if [ $isDebug == "true" ];then
		echo "$1"
	fi 
}
### main logic ###

## 引数精査 ##
CMDNAME=`basename $0`
while getopts t:e:h OPTS
do
	case $OPTS in
		e)	COMMAND=$OPTARG;;
		t)	TARGET=$OPTARG;;
		h)	usageExit;;
		*)	usageExit;;
	esac
done

# -eオプションの値なし時は、終了
if [ -z "${COMMAND}" ]; then
	usageExit
fi

# -tは任意オプションのため処理しない
# 全てのrbファイルを処理対象とする.

## ファイル存在チェック
SEARCHPATH=$DIR/$TARGET*.rb

# 対象ファイル一覧表示
showFiles "$SEARCHPATH"
echo "[INFO] execute command"
echo $COMMAND

# 処理開始確認
confirmExecution "[CONFIRM] 処理を開始しますか？(y/n):"
if [ $? -eq 1 ]; then
	echo "[INFO] スクリプトを終了します"
	exit 1;
fi

# RBファイルコピー & capistranoコマンド実行
for RB in `ls $SEARCHPATH`
do
	# 1ファイルずつの処理確認
	# isExecutionConfirmがtrueの場合のみ、毎回確認MSGを表示する.
	if [ $isExecutionConfirm == "true" ]; then

		confirmExecution "[CONFIRM] $RBでcapistranoを実行しますか？(y/n):"
		# 1がreturnされる場合は、"n"選択時
		if [ $? -eq 1 ]; then
			echo "  [INFO] $RBをスキップします"
			continue
		fi
	fi

	# 処理開始メッセージ
	echo "[INFO]starting run **cap $COMMAND** ::::: $RB->./$CAPFILE"

	# delete hosts.rb
	deleteFile "$CAPFILE"

	# copy from DIR to CURRENTDIR
	`cp -p $RB $CAPFILE`

	# copy成功時はコピーしたファイルを表示
	printDebug "  [DEBUG] $RB copy to $CAPFILE"

	# execute capistrano
	`cap $COMMAND`

	# 処理終了メッセージ
	echo "[INFO] finished run **cap $COMMAND** ::::: $RB->./$CAPFILE"
	echo " "
done

echo "[INFO] finished all run!"

