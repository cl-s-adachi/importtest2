#!/bin/bash

qemuimgListFile=$1
hostName=`hostname -s`

if [ ! -e ${qemuimgListFile} ] ; then
 exit 0
fi

for qemuimg in `grep ${hostName} ${qemuimgListFile} | awk '{print $2}'`
do
   psize=`ls -l ${qemuimg} | cut -d" " -f5`
   vsize=`qemu-img info ${qemuimg} | grep "virtual size" | cut -d" " -f4 | sed s/\(//g`
   
   if [ ${psize} -gt ${vsize} ] ; then
      echo "PSIZE:${psize} VSIZE:${vsize} ${qemuimg}"
   fi
done
