##------------------------------------------------------------------------------
## 使用するアカウントとパスワードを指定してください。
## 一部のコマンドはroot権限、またはsudoでrootになれる権限が必要です。

set :user, "root"
set :password, "Admin123ckk!"
#set :password, "Creation@Line"


#ssh_options[:keys] = %w(/root/.ssh/pdsh.nonpass.kvmhost)
default_run_options[:pty]=true

##------------------------------------------------------------------------------
## 対象ホストの指定
## 指定方法：同梱のtargetsディレクトリ内のhost.*-*-*.rbファイルを、
##           Capfileと同じ階層に「hosts.rb」としてコピーしてください。
##           その後「cap コマンド名」でコマンドを実行します。
##           コピーしたhostsファイルが正しいか確認する場合は
##           「cap hostname」で取得したホスト名が一致するか確認してください。

load "hosts.rb"

#-------------------------------------------------------------------------------

## Valiables

# show line logs count
set :line_number, 10

# update packege definitions
user_home = '/root/'
user_home = "/home/#{user}/" if user != 'root'

## Tasks
desc 'ホスト名を表示します'
task :hostname, :roles=>[:targets] do
   run "echo $HOSTNAME"
end

desc 'ホストの時刻を表示します'
task :date, :roles=>[:targets] do
   run "date"
end

desc 'ホストの情報を確認します'
task :"hostinfo", :roles=>[:targets] do
   run "date; uname -n; id"
end


namespace(:snapshot) do
  ## チェックツールとターゲットファイルを配布
  desc 'チェックツールとターゲットファイルを配布します'
  task :"deploy-tool", :roles=>[:targets] do
     file = 'checkQemuSnapshot.sh' 
     upload(file, user_home + file, :via=>:scp)
     file = 'checkVirshSnapshot.sh' 
     upload(file, user_home + file, :via=>:scp)
     target_dir = "./target/"
     file = "qemupathlist.$CAPISTRANO:HOST$"
     upload(target_dir + file, user_home + file, :via=>:scp)
     file = "vmlist.$CAPISTRANO:HOST$"
     upload(target_dir + file, user_home + file, :via=>:scp)
  end

  desc 'チェックを実行します'
  task :"run-check", :roles=>[:targets] do
     run "cd #{user_home}; #{sudo} sh checkQemuSnapshot.sh qemupathlist.`hostname -s` > checkQemuSnapshot.log"
     run "cd #{user_home}; #{sudo} sh checkVirshSnapshot.sh vmlist.`hostname -s` > checkVirshSnapshot.log"
  end

  desc '実行結果を収集します'
  task :"get-result",:roles=>[:targets] do
     result_dir = "./result/" 
     srcfile = "checkQemuSnapshot.log"
     destfile = "checkQemuSnapshot.$CAPISTRANO:HOST$.log"
     download(user_home + srcfile, result_dir + destfile, :via=>:scp)
     srcfile = "checkVirshSnapshot.log"
     destfile = "checkVirshSnapshot.$CAPISTRANO:HOST$.log"
     download(user_home + srcfile, result_dir + destfile, :via=>:scp)
  end
end

namespace(:volumesize) do
  ## チェックツールとターゲットファイルを配布
  desc 'チェックツールとターゲットファイルを配布します'
  task :"deploy-tool", :roles=>[:targets] do
     file = 'checkVolumeSize.sh' 
     upload(file, user_home + file, :via=>:scp)
     target_dir = "./target/"
     file = "qemupathlist.$CAPISTRANO:HOST$"
     upload(target_dir + file, user_home + file, :via=>:scp)
  end

  desc 'チェックを実行します'
  task :"run-check", :roles=>[:targets] do
     run "cd #{user_home}; #{sudo} sh checkVolumeSize.sh qemupathlist.`hostname -s` > checkVolumeSize.log"
  end

  desc '実行結果を収集します'
  task :"get-result",:roles=>[:targets] do
     srcfile = "checkVolumeSize.log"
     result_dir = "./result/" 
     destfile = "checkVolumeSize.$CAPISTRANO:HOST$.log"
     download(user_home + srcfile, result_dir + destfile, :via=>:scp)
  end
end
