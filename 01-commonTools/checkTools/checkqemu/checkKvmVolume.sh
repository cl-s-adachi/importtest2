#!/bin/sh

basedir=`dirname $0`
temp=${basedir}/tmp
resultdir=${basedir}/result
alertdir=${basedir}/alert
scriptdir=${HOME}/script

logger -p local0.info -t CloudStack.INFO "Start checkKvmVolume.sh" 

DBHOST=`${scriptdir}/getDBInfo.sh StandbyName`

cd ${basedir}

## Create Target
./createCeckVolumeTartget.sh

## Check Snapshot
yes | ./runCapistrano.sh -e snapshot:deploy-tool
yes | ./runCapistrano.sh -e snapshot:run-check
yes | ./runCapistrano.sh -e snapshot:get-result

for resultlog in `ls ${resultdir}/checkQemuSnapshot*log`
do
  for fullpath in `grep "qemu-img snapshot -l" ${resultlog} | awk '{print $5}'`
    do
      path=`basename ${fullpath}`
      volumename=`mysql -u root cloud -h ${DBHOST} -N -e "select name from volumes where path = \"${path}\";"`
      volumeid=`mysql -u root cloud -h ${DBHOST} -N -e "select id from volumes where path = \"${path}\";"`
      if [ "x${volume}" != "x" ] ; then
         touch ${alertdir}/QemuSnapshot_alert.txt
         grep -w ${volumeid} ${alertdir}/QemuSnapshot_alert.txt
         if [ $? != 0 ] ;then
            echo ${volumeid} ${volumename} >> ${alertdir}/QemuSnapshot_alert.txt
            logger -p local0.warn -t CloudStack.ERROR "Qemu snapshot there are still:${volumeid}/${volumename}"
         fi
      fi
    done
done

for resultlog in `ls  ${resultdir}/checkVirshSnapshot*log`
do
 for vm in `grep "virsh snapshot-list" ${resultlog} | awk '{print $4}'`
 do
  touch ${alertdir}/VirshSnapshot_alert.txt
  grep -w ${vm} ${alertdir}/VirshSnapshot_alert.txt 
  if [ $? != 0 ] ;then
     echo ${vm} >> ${alertdir}/VirshSnapshot_alert.txt
     logger -p local0.warn -t CloudStack.ERROR "Virsh snapshot there are still:${vm}"
  fi
 done
done

## Check Volume Size
yes | ./runCapistrano.sh -e volumesize:deploy-tool
yes | ./runCapistrano.sh -e volumesize:run-check
yes | ./runCapistrano.sh -e volumesize:get-result

for resultlog in `ls ${resultdir}/checkVolumeSize*log`
do
  for fullpath in `cat ${resultlog} | awk '{print $3}'`
    do
      path=`basename ${fullpath}`
      volumename=`mysql -u root cloud -h ${DBHOST} -N -e "select name from volumes where path = \"${path}\";"`
      volumeid=`mysql -u root cloud -h ${DBHOST} -N -e "select id from volumes where path = \"${path}\";"`
      if [ "x${volumeid}" != "x" ] ; then 
         touch ${alertdir}/VolumeSize_alert.txt
         alertvolume=`grep -w ${volumeid} ${alertdir}/VolumeSize_alert.txt`
         if [ "x${alertvolume}" != "x" ] ;then
            echo ${volumeid} ${volumename} >> ${alertdir}/VolumeSize_alert.txt
            logger -p local0.warn -t CloudStack.ERROR "Volume size is greater:${volumeid}/${volumename}"
         fi
      fi
    done
done

logger -p local0.info -t CloudStack.INFO "End checkKvmVolume.sh" 
