#!/bin/bash
# Copyright(c) 2014 CREATIONLINE,INC. All Right Reserved.

#
# Global Setting UPDATE script
# location is current directory
#

UPDATE_GLOBAL_VALUES_FILE="Global_Setting_values.txt"
UPDATE_GLOBAL_VALUES_DIR="/home/ckkcl/ACS49-SETUP/ACS_tools/GlobalConfig/UpdateGlobalconfig/"
LOG_FILE="01_Update_Global_Setting.log"

NOW_DATE=`/bin/date +"%Y%m%d%H%M"`
NEW_FILE=${UPDATE_GLOBAL_VALUES_DIR}${LOG_FILE}
RNE_FILE="${UPDATE_GLOBAL_VALUES_DIR}${LOG_FILE}.${NOW_DATE}"

if [ ! -e ${UPDATE_GLOBAL_VALUES_DIR}/${UPDATE_GLOBAL_VALUES_FILE} ]; then
    echo -e "["${UPDATE_GLOBAL_VALUES_DIR}"/"${UPDATE_GLOBAL_VALUES_FILE}"] does not exist."
    exit 1
fi

echo -e "===== START Updates a configuration.(Global Setting) ====="
cat ${UPDATE_GLOBAL_VALUES_DIR}/${UPDATE_GLOBAL_VALUES_FILE} | \
while read NAME VALUE
do
  if [ -z ${NAME} ]; then
    break
  elif [ -z ${VALUE} ]; then
    break
  fi

  ${UPDATE_GLOBAL_VALUES_DIR}kick_api.sh command=updateConfiguration name=${NAME} value=${VALUE} >> ${NEW_FILE}
#  ${UPDATE_GLOBAL_VALUES_DIR}kick_api.sh command=listConfigurations name=${NAME} >> ${NEW_FILE}
done


if [ -e ${NEW_FILE} ]; then
    if [ ! -e ${RNE_FILE} ]; then
        mv ${NEW_FILE} ${RNE_FILE}
    else
        mv ${RNE_FILE} "${RNE_FILE}.bak"
        mv ${NEW_FILE} ${RNE_FILE}
    fi
fi

echo -e "===== FINISH Updates a configuration.(Global Setting) ====="

