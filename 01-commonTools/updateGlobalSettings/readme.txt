# create 2014/1/30
# How to use
#
# 1.I placed under the AP server for which you want to change the Global value this file group.
# 2.You need api key for the root user to be present in the target CS is issued.
# 3.You need to edit the following: Open the kick_api.sh supplied.
#   And to specify the parameters it becomes the key for the root user.
#   api_key
#   secret_key
# 4.You need to open and edit Global_Setting_values.txt supplied.
#   Method that you specify to Global name to change the first argument, the value to change the second argument.
#   Delimiter is a blank, I will specify a line-by-line basis.
#   I lists the Sample below.
#   (Sample)
#    secstorage.service.offering 385
#    system.vm.default.hypervisor VMware
# 5.I run with no arguments 01_Update_Global_Setting.sh supplied.
# 6.There is a need to restart the service manegement for setting affect if the script after the end, management service is started.
#
#
# 
#日本語説明
# 1.このファイル群(tar)をGlobal設定を変更したいCSサーバ配下に配置します。
# 2.CSにてrootユーザのapiキーが発行されている必要があります。
# 3.同梱のkick_api.shを編集する必要があります。
#   以下のパラメータにrootユーザのapiキー、secretキーを指定します。
#   api_key
#   secret_key
# 4.同梱のGlobal_Setting_values.txtを編集する必要があります。
#   第１引数に変更するglobal名、第２引数に変更後のglobal値を指定します。
#   デリミタは空白で、１行単位に指定します。
#   以下にSampleを記載。
#   (Sample)
#    secstorage.service.offering 385
#    system.vm.default.hypervisor VMware
# 5.同梱の01_Update_Global_Setting.shを引数なしで起動します。
# 6.スクリプト終了後、managementサービスが起動している場合は設定反映の為にmanegementサービスを再起動する必要があります。
