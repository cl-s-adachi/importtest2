#! /usr/bin/env python
# coding: utf-8
#
# createHostRb.py
# キャピストラーノ用のhostrbファイル生成スクリプト
#
# - usage
# ./createHostRb.py
#
# - note
# hosts.<VALUEorPREMUM>-<cluster name>.rb のファイル名でファイル出力
# ファイル内容は、status upのホストのみ
# ホストのstatusがupでないものは、そのホストをコンソール出力する
# <VALUEorPREMIUM>は、VALUEの場合、v、PREMIUMの場合、pを出力する.
# ホストタグがないホストは出力対象にならない.
#
# note: CloudStackはVALUE/PREMIUMクラスタの概念がないため、以下をもって、VALUEorCLUSTERを判定する.
# 配下のホストに"VALUE"タグのホストが1つでも存在する場合 -> VALUE
# 上記以外の場合、->PREMIUM
#
# - date
# 2014/02/01 ver1.0(new)
# 2014/02/07 ver1.1
#  コード整理(メインロジックとAPIロジックを分離)
#  VALUEクラスタの場合とPREMIUMクラスタの場合で、出力ファイル名を分離する.
#
# - author
#  taira

## import lib
import MySQLdb
import sys

import urllib
import urllib2
import commands


############################## Define parameter ##############################

# db server
dbHost = 'tckktky4-vcldb01'
dbUser = 'root'
dbName = 'cloud'
dbPasswd = ''
# mode
debug = True
#debug = False

############################## Define API ##############################

########## def getCluster ##########
# データベースから、クラスタIDのリスト、クラスタ名のリストを取得する関数
# 対象クラスタは、KVMかつ、有効なクラスタのみ 
#
# param[in]	dbHost		:接続先DBホスト 
# param[in]	dbUser		:接続先dbのユーザ
# param[in]	dbPasswd	:接続先dbのパスワード 
# param[in]	dbName		:接続先DB名
# param[out]			:タプルでクラスタID、クラスタ名を返す
#				:|-[0] : クラスタIDリスト
#				:|-[1] : クラスタ名リスト
def getCluster(dbHost, dbUser, dbPasswd, dbName): 

	# DB接続文
	# KVMクラスタ一覧取得SQL文ひな形
	execClusterSQL = "SELECT id,uuid,name,allocation_state,hypervisor_type FROM cluster WHERE removed is NULL AND hypervisor_type='KVM'"

	# MySQL接続
	connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
	cur = connect.cursor()

	# KVMクラスタ一覧取得
	cur.execute(execClusterSQL)

	# cluster idリスト表示
	# クラスタidリスト作成
	rows = int(cur.rowcount)
	
	print("==========KVM cluster list==========")
	print('No', 'id', 'uuid', 'name', 'allocation_state' ,'hypervisor_type')

	idList =[];
	nameList = [];	
	for x in range(0, rows):
		row = cur.fetchone()
		print(x, str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4]))

		# 有効なクラスタのみを対象クラスタとする.
		if ("Enabled" == row[3]):
                	idList.append(str(row[0]));
                	nameList.append(str(row[2]));
		else:
			print("Cluster:%s in not Enabled" % str(row[3]))

	# SQLコネクションクローズ
	cur.close()
	connect.close()

	return (idList, nameList);

########### def outputFiles ##########
# クラスタ単位に、host.<クラスタ名>.rbを出力する関数
#
# param[in]     dbHost          :接続先DBホスト
# param[in]     dbUser          :接続先dbのユーザ
# param[in]     dbPasswd        :接続先dbのパスワード
# param[in]     dbName          :接続先DB名
# param[in]	clusteridList	:クラスタidのリスト
# param[in]	clusternameList	:クラスタ名のリスト
def outputFiles(dbHost, dbUser, dbPasswd, dbName, clusteridList, clusternameList):

	# ホスト名リストの前に出力する固定文字列
	line1 = "role :targets, *%w("

	# ホスト名リストの後ろに出力する固定文字列
	line2 = ")"

	print("===========create host.rb file==========")
	# クラスタ単位のhost.rbファイル生成処理

	while True:
		res = raw_input('create rb files?(y/n)')
		if res == 'y':

			number = 0;
			# クラスタ単位にホスト精査
			for clusterid in clusteridList:
				print("")
				print('[INFO]creating host.%s.rb file. clusterid(%s)' % (clusternameList[number], clusterid))
	
				# ホストSQLひな形作成
				execHostSQL = "SELECT h.id, h.uuid, h.name, h.status, ht.tag from host as h INNER JOIN host_tags as ht ON h.id=ht.host_id where h.cluster_id=" + clusterid + " and removed is NULL;"

				# mysql接続
				connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
				cur = connect.cursor()	

				# ホスト一覧取得
				cur.execute(execHostSQL)

				rows = int(cur.rowcount)
				hostnameList=[]

				# VALUEクラスタかPREMIUMクラスタの精査のため必要
				isValue = False

				for host in range(0, rows):

					row = cur.fetchone()

					# "."でsplitした最初の要素をホスト名として取得
					tmpHostList = str(row[2]).split('.')
					hostname = tmpHostList[0]

					# ステータスがUpのホストのみ対象とする
					if ("Up" == str(row[3])):
						if (debug):
							print("   [DEBUG]Existing host:%s(%s)" % (hostname, str(row[4])))

						# ホスト名リストにホスト名((.cloudplatform.ne.jpは除く))を代入
						hostnameList.append(hostname)
					else:
						print("   [WARN]Host:%s state is not Up(%s)" % (hostname, str(row[4])))

					# ホストタグがVALUEのホストが存在する場合は、VALUEクラスタと認識する.
					if ("VALUE" == str(row[4])):
						isValue = True

				cur.close()
				connect.close()

				# 対象ホストが存在しない場合は、ファイル出力せず、次クラスタの精査
				if (0 == len(hostnameList)):
					print("[WARN]skip create file. reason: no target host in cluster %s" % clusternameList[number])
					number += 1
					continue

				## ファイル出力
				# 出力文字列の生成をlineにまとめる.
				line = line1 + " ".join(hostnameList) + line2

				filename = ""
				if(isValue):
					filename = "hosts.v-" + clusternameList[number] + ".rb"
				else:
					filename = "hosts.p-" + clusternameList[number] + ".rb"

				# ファイルが存在する場合は、空にして開く
				fp = open(filename, 'w+')
				fp.write(line)
				fp.close()

				print("[OK]created hosts.%s.rb" % clusternameList[number])
				number += 1

			print("")
			print("==========finished create rb files script...bye==========")
			sys.exit()

		elif res == 'n':
			print("not created rb file")
			sys.exit()
		else:
			print("create host.rb files?(y/n)")

#################### main logic  ####################

# クラスタリスト取得
tupCluster = getCluster(dbHost, dbUser, dbPasswd, dbName)

# 要素数だと分かりづらいので、別名で今後使用
clusteridList = tupCluster[0]
clusternameList = tupCluster[1]

# 該当クラスタが存在しない場合は、スクリプト終了
if ( (0 == len(clusteridList)) or (0 == len(clusternameList)) ):
        print("no exist cluster")
        sys.exit()

# ファイル出力
outputFiles(dbHost, dbUser, dbPasswd, dbName, clusteridList, clusternameList)
