set :application, "KCPS remote control recipe (duramente)"

#### Global Configuration -----------------------------------------------------------------------------------------

##base directories contains deployment artifacts & backup folder
set :baseDir, "duramente"
set :portalBaseDir,  "#{baseDir}/portal"  ;  set :portalBackupDir,  "backup_prevDeploy"
set :rpxyBaseDir,    "#{baseDir}/rpxy"    ;  set :rpxyBackupDir,    "backup_prevDeploy"
set :drApiBaseDir,   "#{baseDir}/drApi"   ;  set :drApiBackupDir,   "backup_prevDeploy"
set :drBatchBaseDir, "#{baseDir}/drBatch" ;  set :drBatchBackupDir, "backup_prevDeploy"
set :tomcatBaseDir,  "#{baseDir}/tomcat"  ;  set :tomcatBackupDir,  "backup_prevDeploy"
set :csBaseDir,      "#{baseDir}/cs"      ;  set :csBackupDir,      "backup_prevDeploy"

##global switch defining what sort of cloudstack<->portal<->tomcat deployment is used by the portal.
##recognized values (uncomment 1):
#DEPLOYMENT_STYLE = :cs3X             #using cloudstack 3.X; portal resides on same tomcat as management server
#DEPLOYMENT_STYLE = :cs4X             #using cloudstack 4.X; portal resdes on same tomcat as management server
DEPLOYMENT_STYLE = :separateTomcat   #using cloudstack 3.X/4.X; portal reside on independent tomcat


##misc global vars/constants
SCRIPT_VERSION = "1.52"
#root password cache since it will be used multiple times during the script
$rootPassword = nil
#all global vars below with a "<* Not Initialized>" default
$backupDirPath     = "<BackupDirPath Not Initialized>"
                     #value be filled in with an actual path when script is run
$backupConfFile    = "<BackupConfFile Not Initialized>"
                     #to be filled with path for backed-up httpd conf file from previous deploy
$existingConfFile  = "<ExistingConfFile Not Initialized>"
                     #to be filled with path for existing, deployed httpd conf file
$newConfFile       = "<NewConfFile Not Initialized>"
                     #to be filled with path for new httpd conf file to deploy
$backupHtmlMsgsDir = "<BackupHtmlMsgsDir Not Initialized>"
                     #to be filled with path for backed-up html msgs dir from previous deploy


##deployment style hashes (contains different dirs depending on deployment style)
$tomcatWebappsHash = Hash.new('<Uninitialized TomcatWebAppsHash Value>')  #default error message in case wrong key used
$tomcatWebappsHash[:cs3X] = "/usr/share/cloud/management/webapps"
$tomcatWebappsHash[:cs4X] = "/usr/share/cloudstack-management/webapps"
$tomcatWebappsHash[:separateTomcat] = "/var/lib/tomcat6/webapps"
#tomcat command hash
$tomcatCmdHash= Hash.new('<Uninitialized Tomcat Command>')  #default error message in case wrong key used
$tomcatCmdHash[:cs3X] = "/etc/init.d/cloud-management"
$tomcatCmdHash[:cs4X] = "/etc/init.d/cloud-management"
$tomcatCmdHash[:separateTomcat] = "/etc/init.d/tomcat6"

#tomcat stopped status code
$tomcatStoppedStatusCodeHash= Hash.new('<Uninitialized TomcatStoppedStatus Code>')  #default error message in case wrong key used
$tomcatStoppedStatusCodeHash[:cs3X] = "1"
$tomcatStoppedStatusCodeHash[:cs4X] = "1"
$tomcatStoppedStatusCodeHash[:separateTomcat] = "3"

#tomcat installation dir
$etcTomcat6Dir = "/etc/tomcat6"

#httpd status codes
$httpdStatusCodeHash= Hash.new('<Uninitialized HttpdStatus Codes>')  #default error message in case wrong key used
$httpdStatusCodeHash[:running] = "0"
$httpdStatusCodeHash[:stopped] = "3"

#portal deployment directory hash & related values
$portalDeployDirHash = Hash.new('<Uninitialized PortalDeployDir Value>')  #default error message in case wrong key used
$portalDeployDirHash[:cs3X] = "#{$tomcatWebappsHash[:cs3X]}/portal"
$portalDeployDirHash[:cs4X] = "#{$tomcatWebappsHash[:cs4X]}/portal"
$portalDeployDirHash[:separateTomcat] = "#{$tomcatWebappsHash[:separateTomcat]}/portal"
#last part of deploy dir should be tomcat app name for portal; this value is useful later on
$portalAppName = Pathname.new($portalDeployDirHash[DEPLOYMENT_STYLE]).basename

#reverse proxy deployment related values
#/etc/httpd/conf.d/ conf files
$httpdPath = "/etc/httpd"
$confdDir = "conf.d"
$httpdConfdPath = "#{$httpdPath}/#{$confdDir}"
$proxyConfFile = "proxy.conf"
$proxyAdminConfFile = "proxy_admin.conf"
$proxyApiConfFile = "proxy_api.conf"
$proxyApiAdminConfFile = "proxy_api_admin.conf"
#/var/www/html/messages/ error files
$htmlMsgsDir = "html_msgs"
$messagesDir = "messages"
$htmlMessagesPath = "/var/www/html/#{$messagesDir}"
$htmlMsgFiles = [     "createVolume.error.json.asis",     "createVolume.error.xml.asis",
                               "forbidden.json.asis",              "forbidden.xml.asis",
                           "general.error.json.asis",          "general.error.xml.asis",
                  "registerTemplate.error.json.asis", "registerTemplate.error.xml.asis",
                    "createVolumeFS.error.json.asis",   "createVolumeFS.error.xml.asis",
                ]

#tomcat cache directory hash
$tomcatCacheDirHash = Hash.new('<Uninitialized TomcatCacheDir Value>')  #default error message in case wrong key used
$tomcatCacheDirHash[:cs3X] = "/var/cache/cloud/management/work/Catalina/localhost/portal/org/apache/jsp/WEB_002dINF/views"
$tomcatCacheDirHash[:cs4X] = "/var/cache/cloud/management/work/Catalina/localhost/portal/org/apache/jsp/WEB_002dINF/views"
$tomcatCacheDirHash[:separateTomcat] = "/var/cache/tomcat6/work/Catalina/localhost/portal/org/apache/jsp/WEB_002dINF/views"

#DR API deployment directory hash & related values
$drApiDeployDirHash = Hash.new('<Uninitialized DrApiDeployDirHash Value>')  #default error message in case wrong key used
$drApiDeployDirHash[:cs3X] = "#{$tomcatWebappsHash[:cs3X]}/dr"
$drApiDeployDirHash[:cs4X] = "#{$tomcatWebappsHash[:cs4X]}/dr"
$drApiDeployDirHash[:separateTomcat] = "#{$tomcatWebappsHash[:separateTomcat]}/dr"

#DR Batch deployment directory
$usrShareCloudDir = "/usr/share/cloud"
$drBatchDeployDir = "#{$usrShareCloudDir}/dr"

#CS conf directory
$csConfDirHash = Hash.new('<Uninitialized CsConfDirHash Value>')  #default error message in case wrong key used
$csConfDirHash[:cs3X] = "/usr/share/cloudstack/conf"
$csConfDirHash[:cs4X] = "/usr/share/cloudstack-management/webapps/client/WEB-INF/classes"
$csConfDirHash[:separateTomcat] = "/usr/share/cloudstack-management/webapps/client/WEB-INF/classes"

#hostUrl->metadata info hash
#(defines which entityId goes with which metadata file for recognized KBI-integrated envs)
$metadataFileHash = {
  #prod
  'portal2-east.cloud-platform.kddi.ne.jp'=>{:entityId=>'kcps2e',:fileName=>'prod_kcps2e.xml',:site=>'east'},  #oyama
  'portal2-west.cloud-platform.kddi.ne.jp'=>{:entityId=>'kcps2w',:fileName=>'prod_kcps2w.xml',:site=>'west'},  #imaike
  #hosyu
  '27.80.251.35'=>{:entityId=>'kcps2e',:fileName=>'hosyu_kcps2e.xml',:site=>'east'},  #oyama
  '27.80.251.83'=>{:entityId=>'kcps2w',:fileName=>'hosyu_kcps2w.xml',:site=>'west'},  #imaike
  #giji
  '106.162.226.27' =>{:entityId=>'kcps2edev',:fileName=>'giji_kcps2e.xml',:site=>'east'},  #oyama
  '106.162.229.109'=>{:entityId=>'kcps2wdev',:fileName=>'giji_kcps2w.xml',:site=>'west'},  #imaike
}



#### Portal Pre-Update Check Process definitions -----------------------------------------------------------------

def runPortalPreupdateCheckProcess()
  puts "Running portal script #{SCRIPT_VERSION} #{gray('pre-update check')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'portal:update' tasks."
  puts ""

  checkExpectedFilesForPortalUpdate()

  calcMd5SumsForPortalWarFiles()

  puts "Portal #{gray('pre-update check')} task run to completion."
  puts "If the MD5 sums match your expectations, please proceed to running the upgrade script proper."
end


def checkExpectedFilesForPortalUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{portalBaseDir}")
  validateExistsOrFail("#{portalBaseDir}/#{build}")
  validateExistsOrFail("#{portalBaseDir}/#{build}/#{build}.war")

  puts "[check] ...Hosts contain the expected files & directories."
end


def calcMd5SumsForPortalWarFiles()
  puts "[check] Calculating MD5 sums for portal WAR files..."

  calcMd5Sum("#{portalBaseDir}/#{build}/#{build}.war")

  puts "[check] ...MD5 sum calculations complete."
end




#### Portal Update Process definitions ---------------------------------------------------------------------------

def runPortalUpdateProcess()
  puts "Running portal script #{SCRIPT_VERSION} #{blue('update')} task with the following parameters:"
  puts "  build    = #{build}"
  puts "  entityId = #{entityId}"  if exists? :entityId
  puts "  hostUrl  = #{hostUrl}"   if exists? :hostUrl
  #output info about run
  if recognizedKbiIntegrationEnv?
    puts "HostUrl is recognized KBI-integrated environment; using pre-defined entityId."
  end
  #option validation
  if !recognizedKbiIntegrationEnv? and DEPLOYMENT_STYLE==:separateTomcat and !exists? :entityId
    puts "    [#{ERROR_RED}] -SentityId option not specified."
    puts "            For non-KBI-integrated environments, the -SentityId option is required."
    exit
  end

  checkExpectedFilesForPortalUpdate()

  checkTomcatIsStopped()

  backupPrevPortalDeployment()

  deployNewPortalToTomcat()

  configurePortalSettings()

  cleanupTomcatCache()

  puts "Portal #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def checkTomcatIsStopped()
  puts "[check] Checking tomcats are stopped..."

  #execute tomcat status cmd on host(s)
  results, exitCodes = executeCmdAsRoot("#{$tomcatCmdHash[DEPLOYMENT_STYLE]} status", nil, 'status')

  #check all status responses; if any one is not stopped, output error & exit
  allTomcatsStopped = exitCodes.values.all? {|e| e==$tomcatStoppedStatusCodeHash[DEPLOYMENT_STYLE]}
  unless allTomcatsStopped
    puts "    [#{ERROR_RED}] One or more tomcats are NOT in the stopped state."
    puts "            The portal webserver must be stopped before update can proceed."
    exit
  end

  puts "[check] ...Tomcats are stopped."
end


def backupPrevPortalDeployment()
  puts "[backup] Backing up previous portal deployments..."

  #common paths to be used below
  portalDeployDir = $portalDeployDirHash[DEPLOYMENT_STYLE]
  $backupDirPath = "#{portalBaseDir}/#{build}/#{portalBackupDir}"
  softDeleteDirPath = "#{$backupDirPath}_DELETEME"

  #check status of various files on hosts
  hostsWithExistingBackup = listHostsWithFile($backupDirPath)
  hostsWithPreviousPortalDeploy = listHostsWithFile(portalDeployDir)
  hostsWithPreviousPortalDeployAndExistingBackup = hostsWithPreviousPortalDeploy & hostsWithExistingBackup

  #check for any existing backups (perhaps from previous script run); if it exists, soft-delete
  #(written with this if-guard so in the rare case a host has a backup folder but no existing portal
  # deployment, we leave the backup folder untouched since we cannot replace it w/ anything newer)
  if hostsWithPreviousPortalDeployAndExistingBackup.any?
    puts "    Soft-deleting existing backups"
    run("mv #{$backupDirPath} #{softDeleteDirPath}", :hosts=>hostsWithPreviousPortalDeployAndExistingBackup)
  end

  #backup any existing previous portal deployment to predetermined backup folder
  #(do nothing if host has no previous portal deployment)
  if hostsWithPreviousPortalDeploy.any?
    puts "    Backing-up existing portal deployment"
    run("mkdir #{$backupDirPath}", :hosts=>hostsWithPreviousPortalDeploy)
    executeCmdAsRoot("cp -Rp #{portalDeployDir} #{$backupDirPath}", hostsWithPreviousPortalDeploy)
  end

  #cleanup any soft-deletes on hosts we may have accumulated from above
  if hostsWithPreviousPortalDeployAndExistingBackup.any?
    puts "    Cleaning up soft-deletes"
    executeCmdAsRoot("rm -Rf #{softDeleteDirPath}", hostsWithPreviousPortalDeployAndExistingBackup)
  end

  puts "[backup] ...Previous portal deployment backups complete."
end


def deployNewPortalToTomcat()
  puts "[deploy] Removing previous portal deployments & deploying new portal..."
  portalDeployDir = $portalDeployDirHash[DEPLOYMENT_STYLE]

  #check for existing portal dir in webapps & delete if it exists
  hostsWithPreviousPortalDeploy = listHostsWithFile(portalDeployDir)
  if hostsWithPreviousPortalDeploy.any?
    puts "    Deleting existing portal deployments"
    executeCmdAsRoot("rm -Rf #{portalDeployDir}", hostsWithPreviousPortalDeploy)
  end

  puts "    Deploying new portal to tomcat"
  #create new portal deploy dir
  executeCmdAsRoot("mkdir #{portalDeployDir}", nil)
  #deploy new portal
  executeCmdAsRoot("unzip #{portalBaseDir}/#{build}/#{build}.war -d #{portalDeployDir}", nil)

  puts "[deploy] ...New portal deployments to tomcats complete."
end


def configurePortalSettings()
  puts "[config] Configure portal settings of new deployments..."

  #DEBUG
  #$backupDirPath = "#{portalBaseDir}/#{build}/#{portalBackupDir}"
  #endDEBUG

  springSubDir  = "WEB-INF/spring"
  springDirPath = "#{$portalDeployDirHash[DEPLOYMENT_STYLE]}/#{springSubDir}"

  #for any hosts with an existing hibernate-context.xml, re-use that file as-is
  hibernateContextFile = "hibernate-context.xml"
  hibernateContextOfPrevDeployment = "#{$backupDirPath}/#{$portalAppName}/#{springSubDir}/#{hibernateContextFile}"
  hostsWithPrevHibernateContexts = listHostsWithFile(hibernateContextOfPrevDeployment)
  if hostsWithPrevHibernateContexts.any?
    #overwrite current hibernate-context.xml with version used by previously deployed portal
    puts "    Overwriting hibernate context file with one from previous deployment"
    executeCmdAsRoot("cp -f #{hibernateContextOfPrevDeployment} #{springDirPath}", hostsWithPrevHibernateContexts)
  end

  #insert a flyway import definition into hibernate-context.xml if it does not have one
  puts "    Searching hibernate context file for Flyway import definition"
  hibernateContextPath = "#{springDirPath}/#{hibernateContextFile}"
  flywayBeanImport     = '<import resource="flyway.xml" \/>'
  results, exitCodes   = executeCmd("grep '#{flywayBeanImport}' #{hibernateContextPath}", nil)  #grep returns '0' if found, '1' if not
  requiresFlywayBeanInsertion = exitCodes.values.any? {|code| code!='0'}  #true if any one server's hibernate-context.xml did not contain a Flyway declaration
  if requiresFlywayBeanInsertion
    puts "    Inserting Flyway import definition into hibernate context file"
    #copy duramente_importFlyway.def.def to pre-agreed global location so command inside duramente_insertFlyway.sed can access it
    executeCmdAsRoot("cp #{springDirPath}/duramente_importFlyway.def /tmp/duramente_importFlyway.def", results.keys)
    #insert flyway import declaration into hibernate-context.xml (using .sed file as trying to escape various special chars in the cmdln is a nightmare)
    executeCmdAsRoot("sed -f #{springDirPath}/duramente_insertFlyway.sed -i #{hibernateContextPath}", results.keys)
    #clean up the copied duramente_importFlyway.def
    executeCmdAsRoot("rm -f /tmp/duramente_importFlyway.def", results.keys)
    #customize locations declaration with appropriate east/west value based on hosturl
    if recognizedKbiIntegrationEnv?
      site = $metadataFileHash[hostUrl][:site]
      puts "    Customize flyway.xml locations declaration for this site (#{site})"
      flywayBeanPath  = "#{springDirPath}/flyway.xml"
      defaultLocation = 'db\/migration\/east'
      customLocation  = 'db\/migration\/'+site
      executeCmdAsRoot(%(sed -i "s/#{defaultLocation}/#{customLocation}/g" #{flywayBeanPath}), results.keys)
    end
  else
    puts "    Skipping Flyway import definition insertion as it already exists in hibernate context file"
  end

  #decide value of entityId to use; this is used for both customizing security.xml
  #and the metadata file (for KBI-integrated envs).  Use official value for
  #KBI-integrated envs; otherwise, default to value of user-specified option.
  if recognizedKbiIntegrationEnv?
    entityIdToUse = $metadataFileHash[hostUrl][:entityId]
  elsif exists? :entityId
    entityIdToUse = entityId
  end

  #security.xml configuration is only used for new portal deployments
  if DEPLOYMENT_STYLE==:separateTomcat and exists? :entityId and exists? :hostUrl
    #replace the *_MASTER placeholders in security.xml with values specified by cmd parameters
    securityXmlFile = "security.xml"
    securityXmlFileOfCurrentDeployment = "#{springDirPath}/#{securityXmlFile}"
    puts "    Inserting entityId (#{entityIdToUse}) into security.xml"
    executeCmdAsRoot("sed -i 's/ENTITY_ID_MASTER/#{entityIdToUse}/g' #{securityXmlFileOfCurrentDeployment}", nil)
    puts "    Inserting hostUrl (#{hostUrl}) into security.xml"
    executeCmdAsRoot("sed -i 's/HOST_URL_MASTER/#{hostUrl}/g' #{securityXmlFileOfCurrentDeployment}", nil)
  end

  #only need to deploy metadatafile for KBI-integrated environments
  if recognizedKbiIntegrationEnv?
    puts "    Detected KBI-integrated environment (#{hostUrl})"
    #each metadata file is for a specific environment
    appropriateMetadataFileName = $metadataFileHash[hostUrl][:fileName]
    puts "    Deploying appropriate metadata file: #{appropriateMetadataFileName}"

    #paths used in the metadata file deployment
    metadataDirPath = "#{$portalDeployDirHash[DEPLOYMENT_STYLE]}/WEB-INF/classes/metadata"
    metadataFileToDeploy = "#{metadataDirPath}/#{appropriateMetadataFileName}"
    deployedMetadataFile = "#{metadataDirPath}/#{entityIdToUse}.xml"

    #copy&rename appropriate metadata file (skip hosts without file):
    #file must be deployed with name matching entityId used in security.xml
    hostsWithMetadataFileToDeploy = listHostsWithFile(metadataFileToDeploy)
    if hostsWithMetadataFileToDeploy.any?
      puts "    Deploying appropriate metadata file"
      executeCmdAsRoot("cp #{metadataFileToDeploy} #{deployedMetadataFile}", hostsWithMetadataFileToDeploy)
    end
  elsif
    puts "    Detected non-KBI-integrated environment.  Skipping metadata file deployment process."
  end

  puts "[config] ...Configurations complete."
end


def cleanupTomcatCache
  puts "[cleanup] Deleting portal-related tomcat caches"
  tomcatCacheDir = $tomcatCacheDirHash[DEPLOYMENT_STYLE]

  #delete all .java & .class files from tomcat cache for portal
  hostsWithTomcatCache = listHostsWithFile(tomcatCacheDir)
  if hostsWithTomcatCache.any?
    puts "    Deleting all cached files related to the portal"
    executeCmdAsRoot("rm -f #{tomcatCacheDir}/*.java", hostsWithTomcatCache)
    executeCmdAsRoot("rm -f #{tomcatCacheDir}/*.class", hostsWithTomcatCache)
  end

  puts "[cleanup] ...Cache deletions complete."
end




#### Portal Revert Process definitions ---------------------------------------------------------------------------

def runPortalRevertProcess()
  puts "Running portal script #{SCRIPT_VERSION} #{magenta('revert')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will check for the existence of a #{portalBackupDir} directory in the specified"
  puts "      build directory, but it cannot recognize valid or invalid backup files.  Whatever is in,"
  puts "      or not in, the backup directory will be blindly copied to the portal deploy directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForPortalRevert()

  checkTomcatIsStopped()

  revertPortalFromBackup()

  cleanupTomcatCache()

  puts "Portal #{magenta('revert')} task run to completion.  The deployed portal has been reverted from backup."
end


def checkExpectedFilesForPortalRevert()
  puts "[check] Checking hosts for expected directories..."

  portalDirExists = remoteFileExists?("#{portalBaseDir}")
  unless portalDirExists
    puts "    [#{ERROR_RED}] The required '#{portalBaseDir}' base directory could not be found on one or more hosts."
    exit
  end

  buildDirExists = remoteFileExists?("#{portalBaseDir}/#{build}")
  unless buildDirExists
    puts "    [#{ERROR_RED}] The '#{portalBaseDir}/#{build}' directory could not be found on one or more hosts."
    exit
  end

  backupDirExists = remoteFileExists?("#{portalBaseDir}/#{build}/#{portalBackupDir}")
  unless backupDirExists
    puts "    [#{ERROR_RED}] The '#{portalBaseDir}/#{build}/#{portalBackupDir}' directory could not be found on one or more hosts."
    exit
  end

  backupPortalDirExists = remoteFileExists?("#{portalBaseDir}/#{build}/#{portalBackupDir}/#{$portalAppName}")
  unless backupPortalDirExists
    puts "    [#{ERROR_RED}] The '#{portalBaseDir}/#{build}/#{portalBackupDir}/portal' directory could not be found on one or more hosts."
    exit
  end

  puts "[check] ...Hosts contain the expected directories."
end


def revertPortalFromBackup()
  puts "[revert] Reverting portal from backup..."
  portalDeployDir = $portalDeployDirHash[DEPLOYMENT_STYLE]

  #delete currently-deployed portal
  hostsWithPortalDeploy = listHostsWithFile(portalDeployDir)
  if hostsWithPortalDeploy.any?
    puts "    Deleting current portal deployments"
    executeCmdAsRoot("rm -Rf #{portalDeployDir}", hostsWithPortalDeploy)
  end

  puts "    Deploying portal to tomcat from backup"

  #recreate portal deploy dir on hosts where existing portal deploy was deleted
  executeCmdAsRoot("mkdir #{portalDeployDir}", hostsWithPortalDeploy)
  #copy backup portal files to portal deploy directory
  backupPortalContents = "#{portalBaseDir}/#{build}/#{portalBackupDir}/#{$portalAppName}/*"
  executeCmdAsRoot("cp -Rp #{backupPortalContents} #{portalDeployDir}", hostsWithPortalDeploy)

  puts "[revert] ...Portal revert complete."
end




#### Portal Tomcat Control definitions ---------------------------------------------------------------------------

def runTomcatStartCmd()
  puts "Running portal script #{SCRIPT_VERSION} Tomcat #{green('start')} command."
  puts ""

  puts "[tomcat] Executing tomcat start commands..."
  results, exitCodes = executeCmdAsRoot("#{$tomcatCmdHash[DEPLOYMENT_STYLE]} start", nil, 'start')
  puts "[tomcat] ...Tomcats started."

  puts "Portal Tomcat #{green('start')} command run to completion."
end


def runTomcatStopCmd()
  puts "Running portal script #{SCRIPT_VERSION} Tomcat #{lightRed('stop')} command."
  puts ""

  puts "[tomcat] Executing tomcat stop commands..."
  results, exitCodes = executeCmdAsRoot("#{$tomcatCmdHash[DEPLOYMENT_STYLE]} stop", nil, 'stop')
  puts "[tomcat] ...Tomcats stopped."

  puts "Portal Tomcat #{lightRed('stop')} command run to completion."
end


def runTomcatStatusCmd()
  puts "Running portal script #{SCRIPT_VERSION} Tomcat #{gray('status')} command."
  puts ""

  puts "[tomcat] Executing tomcat status commands..."
  results, exitCodes = executeCmdAsRoot("#{$tomcatCmdHash[DEPLOYMENT_STYLE]} status", nil, 'status')
  puts "[tomcat] ...Tomcat statuses returned."

  puts "Portal Tomcat #{gray('status')} command run to completion."
end


def runTomcatConfUpdateProcess()
  puts "Running portal script #{SCRIPT_VERSION} Tomcat conf #{blue('update')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"

  checkExpectedFilesForTomcatConfUpdate()

  checkTomcatIsStopped()

  backupPrevTomcatConfDeployment()

  deployNewConfFilesToTomcat()

  puts "Previous tomcat7.conf backed up to: #{$backupDirPath}/"
  puts "Portal Tomcat conf #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def checkExpectedFilesForTomcatConfUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{tomcatBaseDir}")
  validateExistsOrFail("#{tomcatBaseDir}/#{timestamp}")
  validateExistsOrFail("#{tomcatBaseDir}/#{timestamp}/tomcat6.conf")

  puts "[check] ...Hosts contain the expected files & directories."
end


def backupPrevTomcatConfDeployment()
  puts "[backup] Backing up previous Tomcat conf deployments..."

  #common paths to be used below
  $backupDirPath = "#{tomcatBaseDir}/#{timestamp}/#{tomcatBackupDir}"
  softDeleteDirPath = "#{$backupDirPath}_DELETEME"

  #check status of various files on hosts
  hostsWithExistingBackup = listHostsWithFile($backupDirPath)
  hostsWithPreviousTomcatConfDeploy = listHostsWithFile($etcTomcat6Dir)
  hostsWithPreviousTomcatConfDeployAndExistingBackup = hostsWithPreviousTomcatConfDeploy & hostsWithExistingBackup

  #check for any existing backups (perhaps from previous script run); if it exists, soft-delete
  #(written with this if-guard so in the rare case a host has a backup folder but no existing tomcat
  # deployment, we leave the backup folder untouched since we cannot replace it w/ anything newer)
  if hostsWithPreviousTomcatConfDeployAndExistingBackup.any?
    puts "    Soft-deleting existing backups"
    run("mv #{$backupDirPath} #{softDeleteDirPath}", :hosts=>hostsWithPreviousTomcatConfDeployAndExistingBackup)
  end

  #backup any existing previous tomcat deployment to predetermined backup folder
  #(do nothing if host has no previous tomcat deployment)
  if hostsWithPreviousTomcatConfDeploy.any?
    puts "    Backing-up existing Tomcat conf deployment"
    run("mkdir #{$backupDirPath}", :hosts=>hostsWithPreviousTomcatConfDeploy)
    executeCmdAsRoot("cp -Rp #{$etcTomcat6Dir}/* #{$backupDirPath}", hostsWithPreviousTomcatConfDeploy)
  end

  #cleanup any soft-deletes on hosts we may have accumulated from above
  if hostsWithPreviousTomcatConfDeployAndExistingBackup.any?
    puts "    Cleaning up soft-deletes"
    executeCmdAsRoot("rm -Rf #{softDeleteDirPath}", hostsWithPreviousTomcatConfDeployAndExistingBackup)
  end

  puts "[backup] ...Previous Tomcat conf deployment backups complete."
end


def deployNewConfFilesToTomcat()
  puts "[deploy] Removing previous Tomcat conf deployments & deploying new Tomcat conf..."

  tomcat6ConfFile = "#{$etcTomcat6Dir}/tomcat6.conf"

  #check for existing tomcat6.conf in /etc/tomcat6 & delete if it exists
  hostsWithPreviousTomcatConfDeploy = listHostsWithFile(tomcat6ConfFile)
  if hostsWithPreviousTomcatConfDeploy.any?
    puts "    Deleting existing Tomcat conf deployments"
    executeCmdAsRoot("rm -f #{tomcat6ConfFile}", hostsWithPreviousTomcatConfDeploy)
  end

  #copy new conf file to /etc/tomcat6
  puts "    Deploying new Tomcat conf"
  executeCmdAsRoot("cp -p #{tomcatBaseDir}/#{timestamp}/tomcat6.conf #{tomcat6ConfFile}", nil)

  puts "[deploy] ...New Tomcat conf deployments complete."
end


def runTomcatConfRevertProcess()
  puts "Running portal script #{SCRIPT_VERSION} Tomcat conf #{magenta('revert')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will check for the existence of a #{tomcatBackupDir} directory in the specified"
  puts "      timestamp directory, but it cannot recognize valid or invalid backup files.  Whatever is in,"
  puts "      or not in, the backup directory will be blindly copied to the tomcat conf directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForTomcatConfRevert()

  checkTomcatIsStopped()

  revertTomcatConfFromBackup()

  puts "Portal Tomcat conf #{magenta('revert')} task run to completion.  The deployed Tomcat conf has been reverted from backup."
end


def checkExpectedFilesForTomcatConfRevert()
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{tomcatBaseDir}")
  validateExistsOrFail("#{tomcatBaseDir}/#{timestamp}")
  validateExistsOrFail("#{tomcatBaseDir}/#{timestamp}/#{tomcatBackupDir}")
  validateExistsOrFail("#{tomcatBaseDir}/#{timestamp}/#{tomcatBackupDir}/tomcat6.conf")

  puts "[check] ...Hosts contain the expected directories."
end


def revertTomcatConfFromBackup()
  puts "[revert] Reverting Tomcat conf from backup..."
  tomcat6ConfFile = "#{$etcTomcat6Dir}/tomcat6.conf"

  #delete currently-deployed tomcat6.conf
  hostsWithTomcatConfDeploy = listHostsWithFile(tomcat6ConfFile)
  if hostsWithTomcatConfDeploy.any?
    puts "    Deleting current Tomcat conf deployments"
    executeCmdAsRoot("rm -f #{tomcat6ConfFile}", hostsWithTomcatConfDeploy)
  end

  puts "    Deploying Tomcat conf from backup"

  #copy backup tomcat6.conf to tomcat conf directory
  backupTomcat6ConfFile = "#{tomcatBaseDir}/#{timestamp}/#{tomcatBackupDir}/tomcat6.conf"
  executeCmdAsRoot("cp -p #{backupTomcat6ConfFile} #{tomcat6ConfFile}", hostsWithTomcatConfDeploy)

  puts "[revert] ...Tomcat conf revert complete."
end




#### Flyway DB Down Process definitions -----------------------------------------------------------

def runFlywayDownProcess()
  puts "Running portal script #{SCRIPT_VERSION} Flyway DB #{magenta('down')} task."
  puts "The flyway.xml file of the currently-deployed portal will be edited into 'down'"
  puts "(ie. revert/downgrade) mode.  Note that the MySQL database itself will not be"
  puts "downgraded until the portal is restarted via the 'portal:tomcat:start' task."
  puts ""
  puts "NOTE: This change to flyway.xml is irreversible."
  puts ""

  checkExpectedFilesForFlywayDown()

  checkTomcatIsStopped()

  configFlywayXmlToDownLocations()

  puts "Portal Flyway DB #{magenta('down')} task run to completion.  The deployed portal's flyway.xml has been edited."
end


def checkExpectedFilesForFlywayDown()
  puts "[check] Checking hosts for expected directories..."

  portalDeployDir = $portalDeployDirHash[DEPLOYMENT_STYLE]

  portalDirExists = remoteFileExists?("#{portalDeployDir}")
  unless portalDirExists
    puts "    [#{ERROR_RED}] The '#{portalDeployDir}' directory could not be found on one or more hosts."
    exit
  end

  webinfDirExists = remoteFileExists?("#{portalDeployDir}/WEB-INF")
  unless webinfDirExists
    puts "    [#{ERROR_RED}] The '#{portalDeployDir}/WEB-INF' directory could not be found on one or more hosts."
    exit
  end

  springDirExists = remoteFileExists?("#{portalDeployDir}/WEB-INF/spring")
  unless webinfDirExists
    puts "    [#{ERROR_RED}] The '#{portalDeployDir}/WEB-INF/spring' directory could not be found on one or more hosts."
    exit
  end

  flywayFileExists = remoteFileExists?("#{portalDeployDir}/WEB-INF/spring/flyway.xml")
  unless flywayFileExists
    puts "    [#{ERROR_RED}] The '#{portalDeployDir}/WEB-INF/spring/flyway.xml' could not be found on one or more hosts."
    exit
  end

  puts "[check] ...Hosts contain the expected directories."
end


def configFlywayXmlToDownLocations()
  puts "[config] Configure Flyway XML targets to down locations..."

  flywayXml = "#{$portalDeployDirHash[DEPLOYMENT_STYLE]}/WEB-INF/spring/flyway.xml"

  #replace all occurrences of 'db/migration' with 'db/migration/down'
  puts "    Edit flyway.xml locations values into down versions"
  dbMigrationPath = 'db\/migration'
  dbDownPath      = 'db\/migration\/down'
  executeCmdAsRoot(%(sed -i "s/#{dbMigrationPath}/#{dbDownPath}/g" #{flywayXml}), nil)

  puts "[config] ...Configurations complete."
end




#### ReverseProxy Httpd Control definitions ---------------------------------------------------------------------------

def runHttpdStartCmd()
  puts "Running reverse proxy script #{SCRIPT_VERSION} Httpd #{green('start')} command."
  puts ""

  puts "[httpd] Executing httpd start commands..."
  results, exitCodes = executeCmdAsRoot("/etc/init.d/httpd start", nil, 'start')
  puts "[httpd] ...Httpd started."

  puts "Reverse proxy Httpd #{green('start')} command run to completion."
end


def runHttpdStopCmd()
  puts "Running reverse proxy script #{SCRIPT_VERSION} Httpd #{lightRed('stop')} command."
  puts ""

  puts "[httpd] Executing httpd stop commands..."
  results, exitCodes = executeCmdAsRoot("/etc/init.d/httpd stop", nil, 'stop')
  puts "[httpd] ...Httpd stopped."

  puts "Reverse proxy Httpd #{lightRed('stop')} command run to completion."
end


def runHttpdStatusCmd()
  puts "Running reverse proxy script #{SCRIPT_VERSION} Httpd #{gray('status')} command."
  puts ""

  puts "[httpd] Executing httpd status commands..."
  results, exitCodes = executeCmdAsRoot("/etc/init.d/httpd status", nil, 'status')
  puts "[httpd] ...Httpd statuses returned."

#  for i in 0..results['172.17.173.22'].size-1 do
#    puts "results['172.17.173.22'][#{i}]='#{results['172.17.173.22'][i]}'"
#  end
#  puts "results['172.17.173.22'].last='#{results['172.17.173.22'].last}'"
#  puts "exitCodes['172.17.173.22']=#{exitCodes['172.17.173.22']}"

  puts "Reverse proxy Httpd #{gray('status')} command run to completion."
end


def runHttpdConfigTestCmd()
  puts "Running reverse proxy script #{SCRIPT_VERSION} Httpd #{lightCyan('configtest')} command."
  puts ""

  puts "[httpd] Executing httpd configtest commands..."
  results, exitCodes = executeCmdAsRoot("/etc/init.d/httpd configtest", nil, 'configtest')
  puts "[httpd] ...Httpd configtest results returned."

  puts "Reverse proxy Httpd #{lightCyan('configtest')} command run to completion."
end




#### ReverseProxy Pre-Update Check Process definitions -----------------------------------------------------------

def runReverseProxyPreupdateCheckProcess(confFile=nil)
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold(confFile)} #{gray('pre-update check')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'rpxy:*:update' tasks."
  puts ""

  checkExpectedFilesForReverseProxyUpdate(confFile)

  puts "Rpxy #{bold(confFile)} #{gray('pre-update check')} task run to completion."
  puts "Please proceed to running the upgrade script proper."
end

def checkExpectedFilesForReverseProxyUpdate(confFile=nil)
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{rpxyBaseDir}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}")

  #only checks for specified confFile, or all if none specified
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{$proxyConfFile}")          if confFile==$proxyConfFile         || confFile.nil?
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{$proxyAdminConfFile}")     if confFile==$proxyAdminConfFile    || confFile.nil?
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{$proxyApiConfFile}")       if confFile==$proxyApiConfFile      || confFile.nil?
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{$proxyApiAdminConfFile}")  if confFile==$proxyApiAdminConfFile || confFile.nil?

  puts "[check] ...Hosts contain the expected files & directories."
end




#### ReverseProxy Update Process definitions --------------------------------------------------------------------

def runReverseProxyUpdateProcess(confFileToUpdate)
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold(confFileToUpdate)} #{blue('update')} task with the following parameters:"
  puts "  timestamp   = #{timestamp}"
  puts "  hostIp      = #{hostIp}"       if exists? :hostIp       #optional
  puts "  portalAppIp = #{portalAppIp}"  if exists? :portalAppIp  #optional

  checkExpectedFilesForReverseProxyUpdate(confFileToUpdate)

  checkHttpdIsStopped()

  backupReverseProxyDeployment(confFileToUpdate)

  deployNewConfFilesToHttpd(confFileToUpdate)

  if exists?(:hostIp) and exists?(:portalAppIp)
    #init conf file with values specified by optional hostIp & portalAppIp cmdln options
    initConfFilesWithSpecifiedIpValues(confFileToUpdate)
  else
    #config conf file host & portalApp IP values extracted from backed-up files
    inheritIpsFromConfFileBackups(confFileToUpdate)
  end

  puts "Previous #{confFileToUpdate} backed up to: #{$backupDirPath}/"
  puts "Rpxy #{confFileToUpdate} #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def checkHttpdIsStopped()
  puts "[check] Checking httpds are stopped..."

  #execute httpd status cmd on host(s)
  results, exitCodes = executeCmdAsRoot("/etc/init.d/httpd status", nil, 'status')

  #check all status responses; if any one is not stopped, output error & exit
  allHttpdsStopped = exitCodes.values.all? {|e| e==$httpdStatusCodeHash[:stopped]}
  unless allHttpdsStopped
    puts "    [#{ERROR_RED}] One or more httpd are NOT in the stopped state."
    puts "            The reverse proxys' Apache HTTP servers must be stopped before update can proceed."
    exit
  end

  puts "[check] ...Httpds are stopped."
end


def backupReverseProxyDeployment(confFile)
  puts "[backup] Backing up previous rpxy #{confFile} deployments..."

  #create backup dir base if it does not already exist
  baseBackupDirPath = "#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}"
  hostsWithoutBaseBackupDir = listHostsWithoutFile(baseBackupDirPath)
  if hostsWithoutBaseBackupDir.any?
    puts "    Creating base backup directory"
    run("mkdir #{baseBackupDirPath}", :hosts=>hostsWithoutBaseBackupDir)
  end

  pathToCheck  = $httpdConfdPath
  pathToBackup = $httpdConfdPath
  underscoreName = confFile.gsub('.', '_')
  backupDir    = "#{baseBackupDirPath}/#{underscoreName}"
  backupPrevDeployment(pathToCheck, pathToBackup, backupDir)

  puts "[backup] ...Previous rpxy #{confFile} deployment backups complete."
end


def deployNewConfFilesToHttpd(confFile)
  puts "[deploy] Removing previous #{confFile} files & deploying new files..."

  #path of existing confFile
  $existingConfFile = "#{$httpdConfdPath}/#{confFile}"

  #check for existing httpd conf dir & delete conf files if they exists
  hostsWithConfFile = listHostsWithFile($existingConfFile)
  if hostsWithConfFile.any?
    puts "    Deleting existing #{confFile} files"
    executeCmdAsRoot("rm -f #{$existingConfFile}", hostsWithConfFile)
  end

  #paths of new confFile to deploy
  $newConfFile = "#{rpxyBaseDir}/#{timestamp}/#{confFile}"

  puts "    Deploying new #{confFile} files to httpd"
  executeCmdAsRoot("cp #{$newConfFile} #{$existingConfFile}", nil)

  puts "[deploy] ...New #{confFile} deployments to httpd complete."
end


def initConfFilesWithSpecifiedIpValues(confFile)
  puts "[config] Configure newly deployed #{confFile} files using command line values..."

  #replace all occurrences of HOST_IP in conf file with specified hostIp value
  insertHostIp = "sed -i \"s/HOST_IP/#{hostIp}/g\" #{$existingConfFile}"
  hostIps, exitCodes = executeCmdAsRoot(insertHostIp, nil, 'init')

  #just to make sure each insert cmd succeeded; exit if any failed
  exitCodes.each{|rpxyIp, exitCode|
    if exitCode!="0"
      puts "    [#{ERROR_RED}] HOST_IP insert failed for reverse proxy #{rpxyIp} (exitCode=#{exitCode})."
      puts "            Please check the httpd #{confFile} files on that machine."
      puts "              backed-up:  ~/#{backedUpConfFile}"
      puts "              new deploy: #{$existingConfFile}"
      exit
    end
  }

  #replace all occurrences of PORTAL_APP_IP in conf file with specified portalAppIp value
  insertPortalAppIp = "sed -i \"s/PORTAL_APP_IP/#{portalAppIp}/g\" #{$existingConfFile}"
  hostIps, exitCodes = executeCmdAsRoot(insertPortalAppIp, nil, 'init')

  #just to make sure each insert cmd succeeded; exit if any failed
  exitCodes.each{|rpxyIp, exitCode|
    if exitCode!="0"
      puts "    [#{ERROR_RED}] PORTAL_APP_IP insert failed for reverse proxy #{rpxyIp} (exitCode=#{exitCode})."
      puts "            Please check the httpd #{confFile} files on that machine."
      puts "              backed-up:  ~/#{backedUpConfFile}"
      puts "              new deploy: #{$existingConfFile}"
      exit
    end
  }

  puts "[config] ...Configurations complete."
end


def inheritIpsFromConfFileBackups(confFile)
  puts "[config] Auto-configure newly deployed #{confFile} files from backups..."

  backedUpConfFile = "#{$backupDirPath}/#{$confdDir}/#{confFile}"

  #make sure backed-up conf file exists before trying to inherit its values
  #(it may not exist if duramente is being used to "install" this conf file for the first time;
  # if so, user should specify specific IP values with the -ShostIp & -SportalAppIp options)
  validateExistsOrFail("#{backedUpConfFile}")

  #The grep+sed command below extracts the host IP from the backed-up conf file,
  #then replaces all occurrences of HOST_IP in the newly-deployed conf file master.
  #All conf files share a common format for the HOST_IP location.
  #The regex used to extract the HOST_IP value from the conf file below
  #looks for a variable IP value in a VirtualHost cmd (with 2 possible ports) of the following form:
  #    <VirtualHost ***.***.***.***:80>
  #The start-of-line option (^) is used to ignore commented-out lines.
  #The regex accepts both ports 80 or 10080.
  puts "    Extracting HOST_IP value from backed-up config and inserting into newly deployed config"
  extractIpFromBackupAndInsertAsHostIp = "ip=$(grep -Po \"^<VirtualHost \\K[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*(?=:(80|10080)>)\" #{backedUpConfFile}); echo $ip; sed -i \"s/HOST_IP/$ip/g\" #{$existingConfFile}"
  hostIps, exitCodes = executeCmdAsRoot(extractIpFromBackupAndInsertAsHostIp, nil, 'extract')

  #just to make sure each extract&replace cmd succeeded; exit if any failed
  exitCodes.each{|rpxyIp, exitCode|
    if exitCode!="0"
      puts "    [#{ERROR_RED}] HOST_IP extract->insert failed for reverse proxy #{rpxyIp} (exitCode=#{exitCode})."
      puts "            Please check the httpd #{confFile} files on that machine."
      puts "              backed-up:  ~/#{backedUpConfFile}"
      puts "              new deploy: #{$existingConfFile}"
      exit
    end
  }

  #The location from which to extract the PORTAL_APP_IP value changes depending on the exact conf file.
  #proxy.conf/proxy_admin.conf share a common location, proxy_api.conf/proxy_api_admin.conf uses a different location.
  #Though the location is different, in the end, the function of this block is to extract the
  #PORTAL_APP_IP value from the backed-up conf file, then replace all occurrences of PORTAL_APP_IP
  #in the newly-deployed conf file master.
  if confFile==$proxyApiConfFile or confFile==$proxyApiAdminConfFile
    #the regex used to extract the PORTAL_APP_IP value from proxy_api.conf/proxy_api_admin.conf
    #below looks for a variable IP value in a RewriteRule of the following form:
    #    RewriteRule ^/client/api$ http://***.***.***.***:8080/client/api [P,QSA,L]
    #the start-of-line option (^) is used to ignore commented-out lines
    puts "    Extracting PORTAL_APP_IP value from backed-up config and inserting into newly deployed config"
    extractIpFromBackupAndInsertAsPortalAppIp = "ip=$(grep -Po \"^RewriteRule\\s*\\^\\/client\\/api\\\\$ http:\\/\\/\\K[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(?=:8080\\/client\\/api \\[P,QSA,L\\])\" #{backedUpConfFile}); echo $ip; sed -i \"s/PORTAL_APP_IP/$ip/g\" #{$existingConfFile}"
    portalAppIps, exitCodes = executeCmdAsRoot(extractIpFromBackupAndInsertAsPortalAppIp, nil, 'extract')
  else
    #the regex used to extract the PORTAL_APP_IP value from proxy.conf/proxy_admin.conf below
    #looks for a variable IP value in a ProxyPass cmd of the following form:
    #    ProxyPass              /portal http://***.***.***.***:18080/portal retry=0
    #the start-of-line option (^) is used to ignore commented-out lines
    puts "    Extracting PORTAL_APP_IP value from backed-up config and inserting into newly deployed config"
    extractIpFromBackupAndInsertAsPortalAppIp = "ip=$(grep -Po \"^ProxyPass\\s*\\/portal http:\\/\\/\\K[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(?=:18080\\/portal retry=0)\" #{backedUpConfFile}); echo $ip; sed -i \"s/PORTAL_APP_IP/$ip/g\" #{$existingConfFile}"
    portalAppIps, exitCodes = executeCmdAsRoot(extractIpFromBackupAndInsertAsPortalAppIp, nil, 'extract')
  end

  #just to make sure each extract&replace cmd succeeded; exit if any failed
  exitCodes.each{|rpxyIp, exitCode|
    if exitCode!="0"
      puts "    [#{ERROR_RED}] PORTAL_APP_IP extract->insert failed for reverse proxy #{rpxyIp} (exitCode=#{exitCode})."
      puts "            Please check the httpd proxy.conf files on that machine."
      puts "              backed-up:  ~/#{backedUpConfFile}"
      puts "              new deploy: #{$existingConfFile}"
      exit
    end
  }

  puts "[config] ...Configurations complete."
end




#### ReverseProxy Revert Process definitions ----------------------------------------------~----------------------

def runReverseProxyRevertProcess(confFileToRevert)
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold(confFileToRevert)} #{magenta('revert')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will check for the existence of backed-up files under the"
  puts "      #{rpxyBackupDir} directory in the specified build directory,"
  puts "      but it cannot recognize valid or invalid backup files."
  puts "      The backed-up file will be blindly copied to the httpd conf.d directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForReverseProxyRevert(confFileToRevert)

  checkHttpdIsStopped()

  revertReverseProxyFromBackup(confFileToRevert)

  puts "Rpxy #{bold(confFileToRevert)} #{magenta('revert')} task run to completion.  The httpd config has been reverted from backup."
end


def checkExpectedFilesForReverseProxyRevert(confFile)
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{rpxyBaseDir}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}")
  underscoreName = confFile.gsub('.', '_')
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}/#{underscoreName}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}/#{underscoreName}/#{$confdDir}")
  $backupConfFile    = "#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}/#{underscoreName}/#{$confdDir}/#{confFile}"
  validateExistsOrFail($backupConfFile)

  puts "[check] ...Hosts contain the expected directories."
end


def revertReverseProxyFromBackup(confFile)
  puts "[deploy] Reverting #{confFile} deployments..."

  #path of existing confFile
  $existingConfFile = "#{$httpdConfdPath}/#{confFile}"

  #delete currently-deployed confFile
  hostsWithConfFile = listHostsWithFile($existingConfFile)
  if hostsWithConfFile.any?
    puts "    Deleting current #{confFile} deployments"
    executeCmdAsRoot("rm -f #{$existingConfFile}", hostsWithConfFile)
  end

  puts "    Deploying #{confFile} to httpd from backup"

  #copy backed-up confFile file to httpd config directory
  executeCmdAsRoot("cp -p #{$backupConfFile} #{$httpdConfdPath}", hostsWithConfFile)

  puts "[deploy] ...#{confFile} deployments reverted."
end




#### ReverseProxy Html Messages Pre-Update Check Process definitions -----------------------------------------------------------

def runRpxyHtmlMsgsPreupdateCheckProcess()
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold('Html Messages')} #{gray('pre-update check')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'rpxy:html_msgs:update' task."
  puts ""

  checkExpectedFilesForRpxyHtmlMsgsUpdate()

  puts "Rpxy #{bold('Html Messages')} #{gray('pre-update check')} task run to completion."
  puts "Please proceed to running the upgrade script proper."
end

def checkExpectedFilesForRpxyHtmlMsgsUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{rpxyBaseDir}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}")

  #checks for all recognized html message files
  $htmlMsgFiles.each do |msgFile|
    validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{msgFile}")
  end

  puts "[check] ...Hosts contain the expected files & directories."
end




#### ReverseProxy Html Messages Update Process definitions --------------------------------------------------------------------

def runRpxyHtmlMsgsUpdateProcess()
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold('Html Messages')} #{blue('update')} task with the following parameters:"
  puts "  timestamp   = #{timestamp}"

  checkExpectedFilesForRpxyHtmlMsgsUpdate()

  checkHttpdIsStopped()

  backupRpxyHtmlMsgs()

  deployNewRpxyHtmlMsgsToHttpd()

  puts "Updated the following html message files: #{$htmlMessagesPath}/"
  $htmlMsgFiles.each do |msgFile|
    puts "    #{msgFile}"
  end
  puts "Previous html message files backed up to: #{$backupDirPath}/"
  puts "Rpxy #{bold('Html Messages')} #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def backupRpxyHtmlMsgs()
  puts "[backup] Backing up previous rpxy html messages..."

  #create backup dir base if it does not already exist
  baseBackupDirPath = "#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}"
  hostsWithoutBaseBackupDir = listHostsWithoutFile(baseBackupDirPath)
  if hostsWithoutBaseBackupDir.any?
    puts "    Creating base backup directory"
    run("mkdir #{baseBackupDirPath}", :hosts=>hostsWithoutBaseBackupDir)
  end

  pathToCheck  = $htmlMessagesPath
  pathToBackup = $htmlMessagesPath
  backupDir    = "#{baseBackupDirPath}/#{$htmlMsgsDir}"
  backupPrevDeployment(pathToCheck, pathToBackup, backupDir)

  puts "[backup] ...Previous rpxy html messages backups complete."
end


def deployNewRpxyHtmlMsgsToHttpd()
  puts "[deploy] Removing previous rpxy html messages & deploying new files..."

  $htmlMsgFiles.each do |msgFile|
    #path of existing msgFile
    existingMsgFile = "#{$htmlMessagesPath}/#{msgFile}"

    #check for existing httpd html messages dir & delete msgFile files if they exist
    hostsWithMsgFile = listHostsWithFile(existingMsgFile)
    if hostsWithMsgFile.any?
      puts "    Deleting existing #{msgFile} files"
      executeCmdAsRoot("rm -f #{existingMsgFile}", hostsWithMsgFile)
    end

    #paths of new msgFile to deploy
    newMsgFile = "#{rpxyBaseDir}/#{timestamp}/#{msgFile}"

    puts "    Deploying new #{msgFile} files to httpd"
    executeCmdAsRoot("cp #{newMsgFile} #{existingMsgFile}", nil)
  end

  puts "[deploy] ...New rpxy html messages deployments to httpd complete."
end




#### ReverseProxy Html Messages Revert Process definitions ----------------------------------------------~----------------------

def runRpxyHtmlMsgsRevertProcess()
  puts "Running rpxy script #{SCRIPT_VERSION} #{bold('Html Messages')} #{magenta('revert')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will check for the existence of a backed-up folder under the"
  puts "      #{rpxyBackupDir}/#{$htmlMsgsDir} directory in the specified build directory,"
  puts "      but it cannot recognize valid or invalid backups."
  puts "      The backed-up files will be blindly copied to the httpd html messages directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForRpxyHtmlMsgsRevert()

  checkHttpdIsStopped()

  revertRpxyHtmlMsgsFromBackup()

  puts "Rpxy #{bold('Html Messages')} #{magenta('revert')} task run to completion.  The httpd html messages has been reverted from backup."
end


def checkExpectedFilesForRpxyHtmlMsgsRevert()
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{rpxyBaseDir}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}")
  validateExistsOrFail("#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}/#{$htmlMsgsDir}")
  $backupHtmlMsgsDir = "#{rpxyBaseDir}/#{timestamp}/#{rpxyBackupDir}/#{$htmlMsgsDir}/#{$messagesDir}"
  validateExistsOrFail($backupHtmlMsgsDir)

  puts "[check] ...Hosts contain the expected directories."
end


def revertRpxyHtmlMsgsFromBackup()
  puts "[deploy] Reverting html messages..."

  #path of existing html msgs folder
  existingMsgsDir = "#{$htmlMessagesPath}"

  #delete all files in existing html msgs folder
  hostsWithHtmlMsgsDir = listHostsWithFile(existingMsgsDir)
  if hostsWithHtmlMsgsDir.any?
    puts "    Deleting all files in #{existingMsgsDir}/"
    executeCmdAsRoot("rm -Rf #{existingMsgsDir}/*", hostsWithHtmlMsgsDir)
  end

  puts "    Deploying to #{existingMsgsDir}/ from backup"

  #copy backed-up files to html msgs folder
  executeCmdAsRoot("cp -Rp #{$backupHtmlMsgsDir}/* #{existingMsgsDir}", hostsWithHtmlMsgsDir)

  puts "[deploy] ...html messages reverted."
end





#### DR API Pre-Update Check Process definitions -----------------------------------------------------------

def runDrApiPreupdateCheckProcess()
  puts "Running DR API script #{SCRIPT_VERSION} #{gray('pre-update check')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'dr:api:update' task."
  puts ""

  checkExpectedFilesForDrApiUpdate()

  calcMd5SumsForDrApiWarFiles()

  puts "DR API #{gray('pre-update check')} task run to completion."
  puts "If the MD5 sums match your expectations, please proceed to running the upgrade script proper."
end


def checkExpectedFilesForDrApiUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{drApiBaseDir}")
  validateExistsOrFail("#{drApiBaseDir}/#{build}")
  validateExistsOrFail("#{drApiBaseDir}/#{build}/#{build}.war")

  puts "[check] ...Hosts contain the expected files & directories."
end


def calcMd5SumsForDrApiWarFiles()
  puts "[check] Calculating MD5 sums for DR API WAR files..."

  calcMd5Sum("#{drApiBaseDir}/#{build}/#{build}.war")

  puts "[check] ...MD5 sum calculations complete."
end




#### DR API Update Process definitions ---------------------------------------------------------------------------

def runDrApiUpdateProcess()
  puts "Running DR API script #{SCRIPT_VERSION} #{blue('update')} task with the following parameters:"
  puts "  build= #{build}"

  checkExpectedFilesForDrApiUpdate()

  checkTomcatIsStopped()

  backupPrevDrApiDeployment()

  deployNewDrApiToTomcat()

  puts "DR API #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def backupPrevDrApiDeployment()
  puts "[backup] Backing up previous DR API deployments..."

  #common paths to be used below
  drApiDeployDir = $drApiDeployDirHash[DEPLOYMENT_STYLE]
  $backupDirPath = "#{drApiBaseDir}/#{build}/#{drApiBackupDir}"
  softDeleteDirPath = "#{$backupDirPath}_DELETEME"

  #check status of various files on hosts
  hostsWithExistingBackup = listHostsWithFile($backupDirPath)
  hostsWithPreviousDrApiDeploy = listHostsWithFile(drApiDeployDir)
  hostsWithPreviousDrApiDeployAndExistingBackup = hostsWithPreviousDrApiDeploy & hostsWithExistingBackup

  #check for any existing backups (perhaps from previous script run); if it exists, soft-delete
  #(written with this if-guard so in the rare case a host has a backup folder but no existing drApi
  # deployment, we leave the backup folder untouched since we cannot replace it w/ anything newer)
  if hostsWithPreviousDrApiDeployAndExistingBackup.any?
    puts "    Soft-deleting existing backups"
    run("mv #{$backupDirPath} #{softDeleteDirPath}", :hosts=>hostsWithPreviousDrApiDeployAndExistingBackup)
  end

  #backup any existing previous drApi deployment to predetermined backup folder
  #(do nothing if host has no previous drApi deployment)
  if hostsWithPreviousDrApiDeploy.any?
    puts "    Backing-up existing DR API deployment"
    run("mkdir #{$backupDirPath}", :hosts=>hostsWithPreviousDrApiDeploy)
    executeCmdAsRoot("cp -Rp #{drApiDeployDir} #{$backupDirPath}", hostsWithPreviousDrApiDeploy)
    executeCmdAsRoot("cp -p #{$tomcatWebappsHash[:separateTomcat]}/dr.war #{$backupDirPath}", hostsWithPreviousDrApiDeploy)
  end

  #cleanup any soft-deletes on hosts we may have accumulated from above
  if hostsWithPreviousDrApiDeployAndExistingBackup.any?
    puts "    Cleaning up soft-deletes"
    executeCmdAsRoot("rm -Rf #{softDeleteDirPath}", hostsWithPreviousDrApiDeployAndExistingBackup)
  end

  puts "[backup] ...Previous DR API deployment backups complete."
end


def deployNewDrApiToTomcat()
  puts "[deploy] Removing previous DR API deployments & deploying new DR API..."
  drApiDeployDir = $drApiDeployDirHash[DEPLOYMENT_STYLE]

  #check for existing dr dir in webapps & delete if it exists
  hostsWithPreviousDrApiDeploy = listHostsWithFile(drApiDeployDir)
  if hostsWithPreviousDrApiDeploy.any?
    puts "    Deleting existing DR API deployments"
    executeCmdAsRoot("rm -Rf #{drApiDeployDir}", hostsWithPreviousDrApiDeploy)
  end

  puts "    Deploying new DR API to tomcat"
  #copy war to webapps
  executeCmdAsRoot("cp #{drApiBaseDir}/#{build}/#{build}.war #{$tomcatWebappsHash[:separateTomcat]}/dr.war", nil)
  #deploy new DR API
  executeCmdAsRoot("unzip #{$tomcatWebappsHash[:separateTomcat]}/dr.war -d #{$tomcatWebappsHash[:separateTomcat]}/dr", nil)

  puts "[deploy] ...New DR API deployments to tomcats complete."
end




#### DR Api Revert Process definitions ----------------------------------------------~----------------------

def runDrApiRevertProcess()
  puts "Running DR API script #{SCRIPT_VERSION} #{magenta('revert')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will check for the existence of backed-up files under the"
  puts "      #{drApiBackupDir} directory in the specified build directory,"
  puts "      but it cannot recognize valid or invalid backup files."
  puts "      The backed-up file will be blindly copied to the DR API deploy directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForDrApiRevert()

  checkTomcatIsStopped()

  revertDrApiFromBackup()

  puts "DR API #{magenta('revert')} task run to completion.  The DR API deployment has been reverted from backup."
end


def checkExpectedFilesForDrApiRevert()
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{drApiBaseDir}")
  validateExistsOrFail("#{drApiBaseDir}/#{build}")
  validateExistsOrFail("#{drApiBaseDir}/#{build}/#{drApiBackupDir}")
  validateExistsOrFail("#{drApiBaseDir}/#{build}/#{drApiBackupDir}/dr")
  validateExistsOrFail("#{drApiBaseDir}/#{build}/#{drApiBackupDir}/dr.war")

  puts "[check] ...Hosts contain the expected directories."
end


def revertDrApiFromBackup()
  puts "[deploy] Reverting DR API deployments..."

  #delete currently-deployed DR API exploded contents
  drApiDeployDir = $drApiDeployDirHash[DEPLOYMENT_STYLE]
  hostsWithDrApiDeployDir = listHostsWithFile(drApiDeployDir)
  if hostsWithDrApiDeployDir.any?
    puts "    Deleting current DR API deployments"
    executeCmdAsRoot("rm -Rf #{drApiDeployDir}", hostsWithDrApiDeployDir)
  end

  #delete currently-deployed DR Batch zip
  deployedDrApiWar = "#{$tomcatWebappsHash[:separateTomcat]}/dr.war"
  hostsWithDeployedDrApiWar = listHostsWithFile(deployedDrApiWar)
  if hostsWithDeployedDrApiWar.any?
    puts "    Deleting current DR API WARs"
    executeCmdAsRoot("rm -f #{deployedDrApiWar}", hostsWithDeployedDrApiWar)
  end

  puts "    Deploying DR API from backup"
  executeCmdAsRoot("cp -Rp #{drApiBaseDir}/#{build}/#{drApiBackupDir}/dr #{drApiDeployDir}", nil)
  executeCmdAsRoot("cp -p #{drApiBaseDir}/#{build}/#{drApiBackupDir}/dr.war #{deployedDrApiWar}", nil)

  puts "[deploy] ...DR API deployments reverted."
end




#### DR Batch Pre-Update Check Process definitions -----------------------------------------------------------

def runDrBatchPreupdateCheckProcess()
  puts "Running DR Batch script #{SCRIPT_VERSION} #{gray('pre-update check')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'dr:batch:update' task."
  puts ""

  checkExpectedFilesForDrBatchUpdate()

  calcMd5SumsForDrBatchZipFiles()

  puts "DR Batch #{gray('pre-update check')} task run to completion."
  puts "If the MD5 sums match your expectations, please proceed to running the upgrade script proper."
end


def checkExpectedFilesForDrBatchUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{drBatchBaseDir}")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}/#{build}.zip")

  puts "[check] ...Hosts contain the expected files & directories."
end


def calcMd5SumsForDrBatchZipFiles()
  puts "[check] Calculating MD5 sums for DR Batch zip files..."

  calcMd5Sum("#{drBatchBaseDir}/#{build}/#{build}.zip")

  puts "[check] ...MD5 sum calculations complete."
end




#### DR Batch Update Process definitions -------------------------------------------------------------------------

def runDrBatchUpdateProcess()
  puts "Running DR Batch script #{SCRIPT_VERSION} #{blue('update')} task with the following parameters:"
  puts "  build= #{build}"

  checkExpectedFilesForDrBatchUpdate()

  backupPrevDrBatchDeployment()

  deployNewDrBatch()

  puts "DR Batch #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def backupPrevDrBatchDeployment()
  puts "[backup] Backing up previous DR Batch deployments..."

  #common paths to be used below
  $backupDirPath = "#{drBatchBaseDir}/#{build}/#{drBatchBackupDir}"
  softDeleteDirPath = "#{$backupDirPath}_DELETEME"

  #check status of various files on hosts
  hostsWithExistingBackup = listHostsWithFile($backupDirPath)
  hostsWithPreviousDrBatchDeploy = listHostsWithFile($drBatchDeployDir)
  hostsWithPreviousDrBatchDeployAndExistingBackup = hostsWithPreviousDrBatchDeploy & hostsWithExistingBackup

  #check for any existing backups (perhaps from previous script run); if it exists, soft-delete
  #(written with this if-guard so in the rare case a host has a backup folder but no existing drBatch
  # deployment, we leave the backup folder untouched since we cannot replace it w/ anything newer)
  if hostsWithPreviousDrBatchDeployAndExistingBackup.any?
    puts "    Soft-deleting existing backups"
    run("mv #{$backupDirPath} #{softDeleteDirPath}", :hosts=>hostsWithPreviousDrBatchDeployAndExistingBackup)
  end

  #backup any existing previous drBatch deployment to predetermined backup folder
  #(do nothing if host has no previous drBatch deployment)
  if hostsWithPreviousDrBatchDeploy.any?
    puts "    Backing-up existing DR Batch deployment"
    run("mkdir #{$backupDirPath}", :hosts=>hostsWithPreviousDrBatchDeploy)
    executeCmdAsRoot("cp -Rp #{$usrShareCloudDir}/* #{$backupDirPath}", hostsWithPreviousDrBatchDeploy)
  end

  #cleanup any soft-deletes on hosts we may have accumulated from above
  if hostsWithPreviousDrBatchDeployAndExistingBackup.any?
    puts "    Cleaning up soft-deletes"
    executeCmdAsRoot("rm -Rf #{softDeleteDirPath}", hostsWithPreviousDrBatchDeployAndExistingBackup)
  end

  puts "[backup] ...Previous DR Batch deployment backups complete."
end


def deployNewDrBatch()
  puts "[deploy] Removing previous DR Batch deployments & deploying new DR Batch..."

  #check for existing /usr/share/cloud & create if it does not exist
  hostsWithoutUsrShareCloudDir = listHostsWithoutFile($usrShareCloudDir)
  if hostsWithoutUsrShareCloudDir.any?
    puts "    Auto-creating /usr/share/cloud directory"
    executeCmdAsRoot("mkdir #{$usrShareCloudDir}", hostsWithoutUsrShareCloudDir)
  end

  #check for existing dr dir in /usr/share/cloud & delete if it exists
  hostsWithPreviousDrBatchDeploy = listHostsWithFile($drBatchDeployDir)
  if hostsWithPreviousDrBatchDeploy.any?
    puts "    Deleting existing DR Batch deployments"
    executeCmdAsRoot("rm -Rf #{$drBatchDeployDir}", hostsWithPreviousDrBatchDeploy)
  end

  puts "    Deploying new DR Batch"
  #copy zip to /usr/share/cloud
  executeCmdAsRoot("cp #{drBatchBaseDir}/#{build}/#{build}.zip #{$usrShareCloudDir}/dr.zip", nil)
  #deploy new DR Batch
  executeCmdAsRoot("unzip #{$usrShareCloudDir}/dr.zip -d #{$usrShareCloudDir}", nil)

  puts "[deploy] ...New DR Batch deployments complete."
end




#### DR Batch Revert Process definitions ----------------------------------------------~----------------------

def runDrBatchRevertProcess()
  puts "Running DR Batch script #{SCRIPT_VERSION} #{magenta('revert')} task with the following parameters:"
  puts "  build= #{build}"
  puts ""
  puts "NOTE: This task will check for the existence of backed-up files under the"
  puts "      #{drBatchBackupDir} directory in the specified build directory,"
  puts "      but it cannot recognize valid or invalid backup files."
  puts "      The backed-up file will be blindly copied to the DR Batch deploy directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForDrBatchRevert()

  revertDrBatchFromBackup()

  puts "DR Batch #{magenta('revert')} task run to completion.  The DR Batch deployment has been reverted from backup."
end


def checkExpectedFilesForDrBatchRevert()
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{drBatchBaseDir}")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}/#{drBatchBackupDir}")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}/#{drBatchBackupDir}/dr")
  validateExistsOrFail("#{drBatchBaseDir}/#{build}/#{drBatchBackupDir}/dr.zip")

  puts "[check] ...Hosts contain the expected directories."
end


def revertDrBatchFromBackup()
  puts "[deploy] Reverting DR Batch deployments..."

  #delete currently-deployed DR Batch exploded contents
  hostsWithDrBatchDeploy = listHostsWithFile($drBatchDeployDir)
  if hostsWithDrBatchDeploy.any?
    puts "    Deleting current DR Batch deployments"
    executeCmdAsRoot("rm -Rf #{$drBatchDeployDir}", hostsWithDrBatchDeploy)
  end
  #delete currently-deployed DR Batch zip
  deployedDrBatchZip = "#{$usrShareCloudDir}/dr.zip"
  hostsWithDeployedDrBatchZip = listHostsWithFile(deployedDrBatchZip)
  if hostsWithDeployedDrBatchZip.any?
    puts "    Deleting current DR Batch zips"
    executeCmdAsRoot("rm -f #{deployedDrBatchZip}", hostsWithDeployedDrBatchZip)
  end

  puts "    Deploying DR Batch from backup"
  executeCmdAsRoot("cp -Rp #{drBatchBaseDir}/#{build}/#{drBatchBackupDir}/dr #{$drBatchDeployDir}", nil)
  executeCmdAsRoot("cp -p #{drBatchBaseDir}/#{build}/#{drBatchBackupDir}/dr.zip #{deployedDrBatchZip}", nil)

  puts "[deploy] ...DR Batch deployments reverted."
end




#### CS ACL Pre-Update Check Process definitions -----------------------------------------------------------

def runCsAclPreupdateCheckProcess()
  puts "Running CS ACL script #{SCRIPT_VERSION} #{gray('pre-update check')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will only perform a check for required files & directories."
  puts "      It will not actually perform any updates on the target hosts."
  puts "      To perform an actual update, please use the 'cs:acl:update' task."
  puts ""

  checkExpectedFilesForCsAclUpdate()

  calcMd5SumsForCsAclFiles()

  puts "CS ACL #{gray('pre-update check')} task run to completion."
  puts "If the MD5 sums match your expectations, please proceed to running the upgrade script proper."
end


def checkExpectedFilesForCsAclUpdate()
  puts "[check] Checking hosts for expected files & directories..."

  validateExistsOrFail("#{csBaseDir}")
  validateExistsOrFail("#{csBaseDir}/#{timestamp}")
  validateExistsOrFail("#{csBaseDir}/#{timestamp}/commands.properties")

  puts "[check] ...Hosts contain the expected files & directories."
end


def calcMd5SumsForCsAclFiles()
  puts "[check] Calculating MD5 sums for CS ACL files..."

  calcMd5Sum("#{csBaseDir}/#{timestamp}/commands.properties")

  puts "[check] ...MD5 sum calculations complete."
end




#### CS ACL Update Process definitions -------------------------------------------------------------------------

def runCsAclUpdateProcess()
  puts "Running CS ACL script #{SCRIPT_VERSION} #{blue('update')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"

  checkExpectedFilesForCsAclUpdate()

  checkTomcatIsStopped()

  backupPrevCsAclDeployment()

  deployNewCsAcl()

  puts "CS ACL #{blue('update')} task run to completion.  Thank you for your cooperation citizen."
end


def backupPrevCsAclDeployment()
  puts "[backup] Backing up previous CS ACL deployments..."

  pathToCheck  = "#{$csConfDirHash[DEPLOYMENT_STYLE]}/commands.properties"
  pathToBackup = "#{$csConfDirHash[DEPLOYMENT_STYLE]}/*"
  backupDir    = "#{csBaseDir}/#{timestamp}/#{csBackupDir}"
  backupPrevDeployment(pathToCheck, pathToBackup, backupDir)

  puts "[backup] ...Previous CS ACL deployment backups complete."
end


def deployNewCsAcl()
  puts "[deploy] Removing previous CS ACL files & deploying new files..."

  #path of existing file
  commandsPropertiesFile = "#{$csConfDirHash[DEPLOYMENT_STYLE]}/commands.properties"

  #check for existing httpd conf dir & delete conf files they exists
  hostsWithCsAclDeploy = listHostsWithFile(commandsPropertiesFile)
  if hostsWithCsAclDeploy.any?
    puts "    Deleting current CS ACL deployments"
    executeCmdAsRoot("rm -f #{commandsPropertiesFile}", hostsWithCsAclDeploy)
  end

  #paths of new properties file to deploy
  newPropertiesFile = "#{csBaseDir}/#{timestamp}/commands.properties"

  puts "    Deploying new command.properties files"
  executeCmdAsRoot("cp #{newPropertiesFile} #{commandsPropertiesFile}", nil)

  puts "[deploy] ...New CS ACL deployments complete."
end




#### CS ACL Revert Process definitions -------------------------------------------------------------------------

def runCsAclRevertProcess()
  puts "Running CS ACL script #{SCRIPT_VERSION} #{magenta('revert')} task with the following parameters:"
  puts "  timestamp= #{timestamp}"
  puts ""
  puts "NOTE: This task will check for the existence of a #{csBackupDir} directory in the specified"
  puts "      timestamp directory, but it cannot recognize valid or invalid backup files.  Whatever is in,"
  puts "      or not in, the backup directory will be blindly copied to the CS conf directory."
  puts "      Please make sure the backup directory contains a valid backup."
  puts ""

  checkExpectedFilesForCsAclRevert()

  checkTomcatIsStopped()

  revertCsAclFromBackup()

  puts "CS ACL #{magenta('revert')} task run to completion.  The deployed CS ACL has been reverted from backup."
end


def checkExpectedFilesForCsAclRevert()
  puts "[check] Checking hosts for expected directories..."

  validateExistsOrFail("#{csBaseDir}")
  validateExistsOrFail("#{csBaseDir}/#{timestamp}")
  validateExistsOrFail("#{csBaseDir}/#{timestamp}/#{csBackupDir}")
  validateExistsOrFail("#{csBaseDir}/#{timestamp}/#{csBackupDir}/commands.properties")

  puts "[check] ...Hosts contain the expected directories."
end


def revertCsAclFromBackup()
  puts "[revert] Reverting CS ACL from backup..."

  #path of existing file
  commandsPropertiesFile = "#{$csConfDirHash[DEPLOYMENT_STYLE]}/commands.properties"

  #delete currently-deployed commands.properties
  hostsWithCsAclDeploy = listHostsWithFile(commandsPropertiesFile)
  if hostsWithCsAclDeploy.any?
    puts "    Deleting current CS ACL deployments"
    executeCmdAsRoot("rm -f #{commandsPropertiesFile}", hostsWithCsAclDeploy)
  end

  puts "    Deploying CS ACL from backup"

  #copy backup commands.properties to cs conf directory
  backupCommandsPropertiesFile = "#{csBaseDir}/#{timestamp}/#{csBackupDir}/commands.properties"
  executeCmdAsRoot("cp -p #{backupCommandsPropertiesFile} #{commandsPropertiesFile}", nil)

  puts "[revert] ...CS ACL revert complete."
end




#### Helper Function Definitions ----------------------------------------------------------------------------------

#Run the specified command on the specified hosts (array).
#If nil is specified for hosts, the command will be executed on all hosts.
#If outputLabel is specified, a short text message for each host (marked with
#the outputLabel value) is outputted to screen.
#Returns
#1) hash of results (output of command) for each host
#2) hash of exitCodes of the command run on each host
def executeCmd(command, hosts, outputLabel=nil)
  #if caller specified nil, execute for all hosts
  hosts = find_servers_for_task(current_task) if hosts.nil?

  #execute specified cmd using root privleges, sending the cached root password
  results   = Hash.new()
  exitCodes = Hash.new()
  begin
    exitCode="EXIT_CODE="
    #run specified cmd and explicitly output the exit code for the script to parse
    run("#{command}; echo #{exitCode}$?", :hosts=>hosts) do |channel, stream, out|
      channel.send_data("#{$rootPassword}\n")
      if stream==:out
        #strip whitespace from output
        sanitizedOut = out.strip
        results[channel[:host]] = [] unless results.key?(channel[:host])
        if sanitizedOut.include? exitCode
          #parse out exit code and save separately from result output
          exitCodes[channel[:host]] = sanitizedOut.split(exitCode).last.strip()
        elsif not sanitizedOut.empty?
          #Output from each host is not received in one go, but in chunks.
          #Save all the chunks received for a host as an array of strings.
          #Anybody who wants to make use of the saved output can use this later.
          #Due to the non-deterministic nature of the returned output different
          #runs of the script may result in slightly different chunk distribution
          #in the array even though the total output is identical.
          results[channel[:host]] << sanitizedOut
        end
      end
    end
  rescue Capistrano::RemoteError => error
    #output error from hosts in case cmd failed
    #(cmd completion order is non-deterministic, so we sort output by host ip)
    results.keys.sort_by(&String.natural_order).each {|key| puts "    [#{ERROR_RED}] #{key}> #{results[key]}"  }
    exit
  end

  #output saved results if requested
  #(cmd completion order is non-deterministic, so we sort output by host ip)
  results.keys.sort_by(&String.natural_order).each do|key|
    resultText = "#{results[key]}"
    puts "    [#{outputLabel}] #{key}> #{resultText}"
  end  unless outputLabel==nil

  return results, exitCodes
end


#Run the specified command on the specified hosts (array) using root privleges.
#This method will query the user to enter the password for the root account.
#If nil is specified for hosts, the command will be executed on all hosts.
#If outputLabel is specified, a short text message for each host (marked with
#the outputLabel value) is outputted to screen.
#Returns
#1) hash of results (output of command) for each host
#2) hash of exitCodes of the command run on each host
def executeCmdAsRoot(command, hosts, outputLabel=nil)
  #if caller specified nil, execute for all hosts
  hosts = find_servers_for_task(current_task) if hosts.nil?

  #query root password from user if it has not already been cached
  if $rootPassword.nil?
    puts "    This command requires root privileges.  Please enter the password for the root account of the target hosts."
    $rootPassword = fetch(:root_password, Capistrano::CLI.password_prompt("    Root password: "))
  else
    puts "    This command requires root privileges.  Re-using previously entered password."
  end

  #execute specified cmd using root privleges, sending the cached root password
  results   = Hash.new()
  exitCodes = Hash.new()
  default_run_options[:pty] = true  #necessary to send password thru channel; turn off again after block
  begin
    exitCode="EXIT_CODE="
    #run specified cmd and explicitly output the exit code for the script to parse
    run("su -c '#{command}; echo #{exitCode}$?'", :hosts=>hosts) do |channel, stream, out|
      channel.send_data("#{$rootPassword}\n")
      if stream==:out
        #strip put passwords & whitespace from output (send_data seems to repeat both a lot in the output)
        sanitizedOut = out.gsub($rootPassword, '')
        sanitizedOut = sanitizedOut.strip
        results[channel[:host]] = [] unless results.key?(channel[:host])
        if sanitizedOut.include? exitCode
          #parse out exit code and save separately from result output
          exitCodes[channel[:host]] = sanitizedOut.split(exitCode).last.strip()
        elsif not sanitizedOut.empty?
          #Output from each host is not received in one go, but in chunks.
          #Save all the chunks received for a host as an array of strings.
          #Anybody who wants to make use of the saved output can use this later.
          #Due to the non-deterministic nature of the returned output different
          #runs of the script may result in slightly different chunk distribution
          #in the array even though the total output is identical.
          results[channel[:host]] << sanitizedOut
        end
      end
    end
  rescue Capistrano::RemoteError => error
    #output error from hosts in case cmd failed
    #(cmd completion order is non-deterministic, so we sort output by host ip)
    results.keys.sort_by(&String.natural_order).each {|key| puts "    [#{ERROR_RED}] #{key}> #{results[key]}"  }
    exit
  end
  default_run_options[:pty] = false

  #output a portion of saved results if requested
  #(cmd completion order is non-deterministic, so we sort output by host ip)
  results.keys.sort_by(&String.natural_order).each do|key|
    #adjust amount to output try to avoid outputting the first element,
    #which generally contains the root 'Password:' prompt
    numElementsToOutput = results[key].length>2 ? 2 : 1
    resultText = "#{results[key].last(numElementsToOutput)}"
    #for [tomcat/httpd] status cmd, output custom msgs based off exit code instead
    resultText = translateExitCodeIntoText(exitCodes[key])  if outputLabel=='status'
    puts "    [#{outputLabel}] #{key}> #{resultText}"
  end  unless outputLabel==nil

  return results, exitCodes
end


#Assumes specified exitCode is for a tomcat/httpd status command,
#and translates it into the appropriate status text.
def translateExitCodeIntoText(statusCmdExitCode)
  if statusCmdExitCode==$httpdStatusCodeHash[:running]
    coloredStatus =  "#{green('running')}"
  elsif statusCmdExitCode==$httpdStatusCodeHash[:stopped]
    coloredStatus = "#{lightRed('stopped')}"
  else
    coloredStatus = "UNKNOWN: exitCode=#{statusCmdExitCode}"
  end

  return "[  #{coloredStatus}  ]"
end


#Runs md5sum on specified file/path.
def calcMd5Sum(filePath)
  run("md5sum #{filePath}")
end


#Checks whether the specified path (can be folder or file) exists on each host.
#Returns a hash with hostnames as keys and true/false as value depending on whether
#the specified file exists on that particular host.
def testFileExistanceOnHosts(path)
  results = Hash.new()

  invoke_command("test -e #{path} && echo -n 'true' || echo 'false'") do |channel, stream, out|
    results[channel[:host]] = (out == 'true')
  end

  puts "    Checking for '#{path}' on each host:"
  results.keys.each {|key| puts "      #{key}> #{results[key]? 'found' : 'not found'}"  }

  results
end

#Returns true if specified path (can be folder or file) exists on ALL hosts; false otherwise.
def remoteFileExists?(path)
  testFileExistanceOnHosts(path).values.all?
end

#Checks for the existence of specified path (can be directory or file).
#If it exists, just returns; otherwise, an error message is outputted and the script terminates.
def validateExistsOrFail(path)
  exists = remoteFileExists?(path)
  unless exists
    puts "    [#{ERROR_RED}] Directory/file '#{path}' could not be found on one or more hosts."
    exit
  end
end

#Returns all hosts that have the specified path (can be folder or file) as an array.
def listHostsWithFile(path)
  resultHash = testFileExistanceOnHosts(path)
  hostWithFileHash = resultHash.reject{|key, value| value==false}
  hostWithFileHash.keys
end

#Returns all hosts that do *not* have the specified path (can be folder or file) as an array.
def listHostsWithoutFile(path)
  resultHash = testFileExistanceOnHosts(path)
  hostWithFileHash = resultHash.reject{|key, value| value==true}
  hostWithFileHash.keys
end

#If specified pathToCheck exists on host, backs up specified pathToBackup to specified backupDir.
def backupPrevDeployment(pathToCheck, pathToBackup, backupDir)
  #common paths to be used below
  $backupDirPath = "#{backupDir}"
  softDeleteDirPath = "#{$backupDirPath}_DELETEME"

  #check status of various files on hosts
  hostsWithExistingBackup = listHostsWithFile($backupDirPath)
  hostsWithPreviousDeploy = listHostsWithFile(pathToCheck)
  hostsWithPreviousDeployAndExistingBackup = hostsWithPreviousDeploy & hostsWithExistingBackup

  #check for any existing backups (perhaps from previous script run); if it exists, soft-delete
  #(written with this if-guard so in the rare case a host has a backup folder but no existing
  # deployment, we leave the backup folder untouched since we cannot replace it w/ anything newer)
  if hostsWithPreviousDeployAndExistingBackup.any?
    puts "    Soft-deleting existing backups"
    run("mv #{$backupDirPath} #{softDeleteDirPath}", :hosts=>hostsWithPreviousDeployAndExistingBackup)
  end

  #backup any existing previous deployment to predetermined backup folder
  #(do nothing if host has no previous deployment)
  if hostsWithPreviousDeploy.any?
    puts "    Backing-up existing deployment"
    run("mkdir #{$backupDirPath}", :hosts=>hostsWithPreviousDeploy)
    executeCmdAsRoot("cp -Rp #{pathToBackup} #{$backupDirPath}", hostsWithPreviousDeploy)
  end

  #cleanup any soft-deletes on hosts we may have accumulated from above
  if hostsWithPreviousDeployAndExistingBackup.any?
    puts "    Cleaning up soft-deletes"
    executeCmdAsRoot("rm -Rf #{softDeleteDirPath}", hostsWithPreviousDeployAndExistingBackup)
  end
end


#Returns true if specified option hostUrl is one of the recognized
#KBI-integrated environments; otherwise, returns false.
def recognizedKbiIntegrationEnv?
  if exists? :hostUrl then
    return $metadataFileHash.keys.include? hostUrl
  else
    return false
  end
end


#Comparison function that returns strings in natural order
#[http://www.justskins.com/forums/natural-order-sort-101199.html]
def String.natural_order(nocase=false)
  proc do |str|
    i = true
    str = str.upcase if nocase
    str.gsub(/\s+/, '').split(/(\d+)/).map {|x| (i = !i) ? x.to_i : x}
  end
end


##Colorized text related definitions
def colorize(color_code, str)
  "\e[#{color_code}m#{str}\e[0m"
end
def bold(str) colorize(1, str) end
def red(str) colorize(31, str) end
def lightRed(str) colorize('1;31', str) end
def green(str) colorize(32, str) end
def blue(str) colorize(34, str) end
def magenta(str) colorize(35, str) end
def cyan(str) colorize(36, str) end
def gray(str) colorize(37, str) end
def lightCyan(str) colorize(96, str) end

ERROR_RED = red("ERROR")
