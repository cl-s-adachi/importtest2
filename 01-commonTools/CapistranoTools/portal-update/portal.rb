load 'blackbox.rb'


#### Target Host Definitions ----------------------------------------------------------------------------------

# Tama-Kenshou hosts
role :tamaken_ap1, "172.22.132.6"
role :tamaken_ap2, "172.22.132.14"
role :tamaken_ap4, "172.22.132.62"
set :tamaken_all, [:tamaken_ap1, :tamaken_ap2, :tamaken_ap4]

# Tama2-Kenshou hosts
role :tamaken2_ap1, "172.27.172.101"
role :tamaken2_ap2, "172.27.172.102"
set :tamaken2_all, [:tamaken2_ap1, :tamaken2_ap2]

# CS307verup-Kenshou hosts
role :verupken_ap1, "172.22.133.234"
role :verupken_ap2, "172.22.133.251"
set :verupken_all, [:verupken_ap1, :verupken_ap2]

# KCPS1-Tama-Release hosts
role :tamaRelease_ap1, "172.22.132.6"
role :tamaRelease_ap2, "172.22.132.14"
role :tamaRelease_ap3, "172.22.132.40"
role :tamaRelease_ap4, "172.22.132.62"
role :tamaRelease_ap5, "172.22.132.66"
set :tamaRelease_all, [:tamaRelease_ap1, :tamaRelease_ap2, :tamaRelease_ap3, :tamaRelease_ap4, :tamaRelease_ap5]

# KCPS1 mini environment hosts
role :minienv_ap1, "172.27.172.161"
set :minienv_ap_all, [:minienv_ap1]
role :minienv_rp1, "172.27.172.155"
set :minienv_rp_all, [:minienv_rp1]

# KCPS1 System-Test hosts
role :systemtest_ap1, "172.27.172.156"
role :systemtest_ap2, "172.27.172.165"
set :systemtest_ap_all, [:systemtest_ap1, :systemtest_ap2]

# ACS4.9-Tama-Release hosts
role :acs49tamaRelease_ap1, "172.22.132.7"
role :acs49tamaRelease_ap2, "172.22.132.8"
role :acs49tamaRelease_ap3, "172.22.132.24"
role :acs49tamaRelease_ap4, "172.22.132.26"
role :acs49tamaRelease_ap5, "172.22.132.27"
set :acs49tamaRelease_all, [:acs49tamaRelease_ap1, :acs49tamaRelease_ap2, :acs49tamaRelease_ap3, :acs49tamaRelease_ap4, :acs49tamaRelease_ap5]

# KCPS2-Oyama-Release hosts
role :oyamaRelease_ap1, "172.27.189.1"
role :oyamaRelease_ap2, "172.27.189.2"
role :oyamaRelease_ap3, "172.27.189.3"
role :oyamaRelease_ap4, "172.27.189.4"
set :oyamaRelease_ap_all, [:oyamaRelease_ap1, :oyamaRelease_ap2, :oyamaRelease_ap3, :oyamaRelease_ap4]
set :oyamaRelease_ap_spare, [:oyamaRelease_ap2, :oyamaRelease_ap3, :oyamaRelease_ap4]
role :oyamaRelease_rp1, "172.27.189.8"
role :oyamaRelease_rp2, "172.27.189.9"
role :oyamaRelease_rp3, "172.27.189.10"
role :oyamaRelease_rp4, "172.27.189.11"
set :oyamaRelease_rp_all, [:oyamaRelease_rp1, :oyamaRelease_rp2, :oyamaRelease_rp3, :oyamaRelease_rp4]
set :oyamaRelease_rp_spare, [:oyamaRelease_rp2, :oyamaRelease_rp3, :oyamaRelease_rp4]
role :oyamaRelease_dr1, "172.27.189.170"
role :oyamaRelease_dr2, "172.27.189.171"
set :oyamaRelease_dr_all, [:oyamaRelease_dr1, :oyamaRelease_dr2]

# KCPS2-Imaike-Release hosts
role :imaikeRelease_ap1, "172.27.191.1"
role :imaikeRelease_ap2, "172.27.191.2"
role :imaikeRelease_ap3, "172.27.191.3"
role :imaikeRelease_ap4, "172.27.191.4"
set :imaikeRelease_ap_all, [:imaikeRelease_ap1, :imaikeRelease_ap2, :imaikeRelease_ap3, :imaikeRelease_ap4]
set :imaikeRelease_ap_spare, [:imaikeRelease_ap2, :imaikeRelease_ap3, :imaikeRelease_ap4]
role :imaikeRelease_rp1, "172.27.191.8"
role :imaikeRelease_rp2, "172.27.191.9"
role :imaikeRelease_rp3, "172.27.191.10"
role :imaikeRelease_rp4, "172.27.191.11"
set :imaikeRelease_rp_all, [:imaikeRelease_rp1, :imaikeRelease_rp2, :imaikeRelease_rp3, :imaikeRelease_rp4]
set :imaikeRelease_rp_spare, [:imaikeRelease_rp2, :imaikeRelease_rp3, :imaikeRelease_rp4]
role :imaikeRelease_dr1, "172.27.191.170"
role :imaikeRelease_dr2, "172.27.191.171"
set :imaikeRelease_dr_all, [:imaikeRelease_dr1, :imaikeRelease_dr2]

# KCPS2-Imaike-Staging (giji) hosts
role :imaikeStaging_ap1, "172.17.173.11"
role :imaikeStaging_ap2, "172.17.173.12"
set :imaikeStaging_ap_all, [:imaikeStaging_ap1, :imaikeStaging_ap2]
role :imaikeStaging_rp1, "172.17.173.21"
role :imaikeStaging_rp2, "172.17.173.22"
set :imaikeStaging_rp_all, [:imaikeStaging_rp1, :imaikeStaging_rp2]



#### CommandLine Processing ---------------------------------------------------------------------------------------

#Extract the host argument from the cli command.
#All task-, option-, and option parameter-looking strings are ignored.
def getHostArg()
  #shift through cmdln args one by one looking for first non-task/option arg
  while true do
    arg=ARGV.shift
    if arg==nil then
      #exit script with error if no host parameter was specified
      puts "    [#{ERROR_RED}] A host argument was not specified in the command."
      puts "            Use 'cap portal:help' for appropriate format."
      exit
    elsif arg.end_with? ":help" then
      #help tasks do not require a host parameter, so skip processing
      host="<No host needed for help message>"
      break
    elsif arg.include? ':' or arg.start_with? '-' or arg.include? '=' then
      #skip all arg that are tasks or options
      next
    else
      #take the first non-task/option arg as the host parameter
      host=arg
      break
    end
  end

  #different processing required for a singular role and grouped role array
  if host.include? "_all" or host.include? "_spare" then
    returnVal = eval(host)   #return array associated with this symbol name
  else
    returnVal = host         #return symbol name as is
  end

  #Dynamically define a task with the same name as the host parameter.
  #Capistrano will consider any non-option args as a task and attempt
  #to execute it, so this will prevent unsightly "No such task" errors
  #from being outputted.
  task(host) do
    #no-op
  end

  return returnVal
end

#Save the host argument from the cli command for use below
set :host, getHostArg()



#### Capistrano Task Definitions ----------------------------------------------------------------------------------

##Tasks related to Portal deployments on AP hosts
namespace :portal do
  task :update, :roles=>host do
    runPortalUpdateProcess()
  end
  task :revert, :roles=>host do
    runPortalRevertProcess()
  end
  task :checkfile, :roles=>host do
    runPortalPreupdateCheckProcess()
  end
  task :help do
    printPortalHelpMessage()
  end

  #Tasks for controlling tomcat
  namespace :tomcat do
    task :start, :roles=>host do
      runTomcatStartCmd()
    end
    task :stop, :roles=>host do
      runTomcatStopCmd()
    end
    task :status, :roles=>host do
      runTomcatStatusCmd()
    end
    namespace :conf do
      task :update, :roles=>host do
        runTomcatConfUpdateProcess()
      end
      task :revert, :roles=>host do
        runTomcatConfRevertProcess()
      end
    end
    task :help do
      printTomcatHelpMessage()
    end
  end

  #Tasks related to Flyway
  namespace :flyway do
    task :down, :roles=>host do
      runFlywayDownProcess()
    end
    task :help do
      printFlywayHelpMessage()
    end
  end
end


##Tasks related to Reverse Proxy deployments on RP hosts
namespace :rpxy do
  namespace :proxy_conf do
    task :update, :roles=>host do
      runReverseProxyUpdateProcess($proxyConfFile)
    end
    task :revert, :roles=>host do
      runReverseProxyRevertProcess($proxyConfFile)
    end
    task :checkfile, :roles=>host do
      runReverseProxyPreupdateCheckProcess($proxyConfFile)
    end
  end
  namespace :proxy_admin_conf do
    task :update, :roles=>host do
      runReverseProxyUpdateProcess($proxyAdminConfFile)
    end
    task :revert, :roles=>host do
      runReverseProxyRevertProcess($proxyAdminConfFile)
    end
    task :checkfile, :roles=>host do
      runReverseProxyPreupdateCheckProcess($proxyAdminConfFile)
    end
  end
  namespace :proxy_api_conf do
    task :update, :roles=>host do
      runReverseProxyUpdateProcess($proxyApiConfFile)
    end
    task :revert, :roles=>host do
      runReverseProxyRevertProcess($proxyApiConfFile)
    end
    task :checkfile, :roles=>host do
      runReverseProxyPreupdateCheckProcess($proxyApiConfFile)
    end
  end
  namespace :proxy_api_admin_conf do
    task :update, :roles=>host do
      runReverseProxyUpdateProcess($proxyApiAdminConfFile)
    end
    task :revert, :roles=>host do
      runReverseProxyRevertProcess($proxyApiAdminConfFile)
    end
    task :checkfile, :roles=>host do
      runReverseProxyPreupdateCheckProcess($proxyApiAdminConfFile)
    end
  end
  task :help do
    printRpxyHelpMessage()
  end

  #Tasks for updating html messages
  namespace :html_msgs do
    task :update, :roles=>host do
      runRpxyHtmlMsgsUpdateProcess()
    end
    task :revert, :roles=>host do
      runRpxyHtmlMsgsRevertProcess()
    end
    task :checkfile, :roles=>host do
      runRpxyHtmlMsgsPreupdateCheckProcess()
    end
    task :help do
      printRpxyHtmlMsgsHelpMessage()
    end
  end

  #Tasks for controlling httpd
  namespace :httpd do
    task :start, :roles=>host do
      runHttpdStartCmd()
    end
    task :stop, :roles=>host do
      runHttpdStopCmd()
    end
    task :status, :roles=>host do
      runHttpdStatusCmd()
    end
    task :configtest, :roles=>host do
      runHttpdConfigTestCmd()
    end
    task :help do
      printHttpdHelpMessage()
    end
  end
end


##Tasks related to DR deployments on AP/DR hosts
namespace :dr do
  namespace :api do
    task :update, :roles=>host do
      runDrApiUpdateProcess()
    end
    task :revert, :roles=>host do
      runDrApiRevertProcess()
    end
    task :checkfile, :roles=>host do
      runDrApiPreupdateCheckProcess()
    end
    task :help do
      printDrApiHelpMessage()
    end
  end
  namespace :batch do
    task :update, :roles=>host do
      runDrBatchUpdateProcess()
    end
    task :revert, :roles=>host do
      runDrBatchRevertProcess()
    end
    task :checkfile, :roles=>host do
      runDrBatchPreupdateCheckProcess()
    end
    task :help do
      printDrBatchHelpMessage()
    end
  end
end


##Tasks related to CS deployments on AP hosts
namespace :cs do
  namespace :acl do
    task :update, :roles=>host do
      runCsAclUpdateProcess()
    end
    task :revert, :roles=>host do
      runCsAclRevertProcess()
    end
    task :checkfile, :roles=>host do
      runCsAclPreupdateCheckProcess()
    end
    task :help do
      printCsAclHelpMessage()
    end
  end
end





#### Help Definitions ----------------------------------------------------------------------------------o

def printPortalHelpMessage()
  puts "The expected command format to update one or more hosts is:"
  puts "  cap <hostname> portal:<task> -Sbuild=<buildname> -SentityId=<entityId> -ShostUrl=<hostUrl>"
  puts "The expected command format to revert/checkfile one or more hosts is:"
  puts "  cap <hostname> portal:<task> -Sbuild=<buildname>"
  puts "This command expects an Application (AP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <buildname> must specify an existing portal folder+WAR file on hosts to be updated/checkfiled:"
  puts "  ~/#{baseDir}/portal/<buildname>/<buildname>.war"
  puts "Or specify an existing portal folder with a backup of a previous deploy on hosts to be reverted:"
  puts "  ~/#{baseDir}/portal/<buildname>/backup_prevDeploy/"
  puts ""

  puts "The <entityId> & <hostUrl> values are used to configure security.xml:"
  puts "-all occurrences of the string 'ENTITY_ID_MASTER' are replaced with <entityId>"
  puts "-all occurrences of the string 'HOST_URL_MASTER' are replaced with <hostUrl>"
  puts "If there are no *_MASTER occurrences, then no replacement is performed (no warning is shown)."
  puts ""

  puts "For recognized KBI-integrated environments, the appropriate metadata file for that"
  puts "environment will also be deployed with a filename that matches the entityId for"
  puts "the same.  In this case, any user-specified <entityId> option will be ignored."
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_ap1 portal:update -Sbuild=isoroku-jenkins_YYYYMMDD-HHMM_XXXXX -SentityId=III -ShostUrl=UUU"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap2 portal:revert -Sbuild=isoroku-jenkins_YYYYMMDD-HHMM_XXXXX"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap_all portal:checkfile -Sbuild=isoroku-jenkins_YYYYMMDD-HHMM_XXXXX"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap portal:help"
end

def printTomcatHelpMessage()
  puts "The expected command format to send start/stop/status commands to tomcat is:"
  puts "  cap <hostname> portal:tomcat:<task>"
  puts "The expected command format to update/revert tomcat's configuration is:"
  puts "  cap <hostname> portal:tomcat:conf:<task> -Stimestamp=<timestamp>"
  puts "This command expects an Application (AP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <timestamp> must specify an existing tomcat sub-directory with conf file to be deployed in update:"
  puts "  ~/#{baseDir}/tomcat/<timestamp>/tomcat6.conf"
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_ap1 portal:tomcat:start"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap2 portal:tomcat:stop"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap_all portal:tomcat:status"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap3 portal:tomcat:conf:update -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap3 portal:tomcat:conf:revert -Stimestamp=YYYYMMDD"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap portal:tomcat:help"
end

def printFlywayHelpMessage()
  puts "The expected command format to change Flyway into down-mode is:"
  puts "  cap <hostname> portal:flyway:down"
  puts "This command expects an Application (AP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "Note though the flyway.xml file is edited to revert the database upon the next executing,"
  puts "the actual MySQL database tables are not reverted until tomact is started (Flyway executes"
  puts "upon portal startup."
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_ap1 portal:flyway:down"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap portal:flyway:help"
end

def printRpxyHelpMessage()
  puts "The expected command format to update the conf files of one or more reverse proxies is:"
  puts "  cap <hostname> rpxy:<conffile>:update -Stimestamp=<timestamp>"
  puts "The expected command format to revert the conf files of one or more reverse proxies is:"
  puts "  cap <hostname> rpxy:<conffile>:revert -Stimestamp=<timestamp>"
  puts "The expected command format to checkfile the conf files of one or more hosts is:"
  puts "  cap <hostname> rpxy:<conffile>:checkfile -Stimestamp=<timestamp>"
  puts "This command expects a Reverse Proxy (RP) server to be specified as the target server."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <conffile> must be one of:"
  puts "  - 'proxy_conf': updates the 'proxy.conf' file of httpd"
  puts "  - 'proxy_admin_conf': updates the 'proxy_admin.conf' file of httpd"
  puts "  - 'proxy_api_conf': updates the 'proxy_api.conf' file of httpd"
  puts "  - 'proxy_api_admin_conf': updates the 'proxy_api_admin.conf' file of httpd"
  puts ""

  puts "The <timestamp> must specify an existing rpxy sub-directory with conf files to be deployed in update:"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/proxy.conf"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/proxy_admin.conf"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/proxy_api.conf"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/proxy_api_admin.conf"
  puts "Or specify an existing rpxy folder with a backup of a previous deploy on hosts to be reverted:"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/backup_prevDeploy/..."
  puts "Each individual rpxy:<conffile>:update task creates its own backup sub-directory under the main"
  puts "backup_prevDeploy/ directory.  For safety, all files inside the httpd conf.d directory are backed"
  puts "up regardless of the specific update task executed.  Again for safety, only the specific <conffile>"
  puts "is copied from backup to the httpd conf.d directory during revert."
  puts ""

  puts "The update command also takes 2 optional values:"
  puts "  cap <hostname> rpxy:<conffile>:update -Stimestamp=<timestamp> -ShostIp=<hostIp> -SportalAppIp=<portalAppIp>"
  puts "Where <hostIp> is an IP value to use as HOST_IP in the conf files,"
  puts "and <portalAppIp> is an IP value to use as PORTAL_APP_IP in the conf files."
  puts "If both values are specified, the update task will insert the specified values into"
  puts "the deployed conf files (assumes conf files have HOST_IP and PORTAL_APP_IP placeholders)."
  puts "If both values are not specified, the update task auto-configures the deployed conf files"
  puts "by default, inheriting the host and portal app IP values from the existing deployment."
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_rp1 rpxy:proxy_conf:update -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp2 rpxy:proxy_admin_conf:revert -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp_all rpxy:proxy_api_conf:checkfile -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp1 rpxy:proxy_api_admin_conf:update -Stimestamp=YYYYMMDD -ShostIp=111.111.111.111 -SportalAppIp=222.222.222.222"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap rpxy:help"
end

def printRpxyHtmlMsgsHelpMessage()
  puts "The expected command format to update the html messages of one or more reverse proxies is:"
  puts "  cap <hostname> rpxy:html_msgs:update -Stimestamp=<timestamp>"
  puts "The expected command format to revert the html messages of one or more reverse proxies is:"
  puts "  cap <hostname> rpxy:html_msgs:revert -Stimestamp=<timestamp>"
  puts "The expected command format to checkfile the html messages of one or more hosts is:"
  puts "  cap <hostname> rpxy:html_msgs:checkfile -Stimestamp=<timestamp>"
  puts "This command expects a Reverse Proxy (RP) server to be specified as the target server."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <timestamp> must specify an existing rpxy sub-directory with html message files to be deployed in update:"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/createVolume.error.json.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/createVolume.error.xml.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/forbidden.json.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/forbidden.xml.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/general.error.json.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/general.error.xml.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/registerTemplate.error.json.asis"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/registerTemplate.error.xml.asis"
  puts "Or specify an existing rpxy folder with a backup of a previous deploy on hosts to be reverted:"
  puts "  ~/#{baseDir}/rpxy/<timestamp>/backup_prevDeploy/..."
  puts "The rpxy:html_msgs:update task creates its own backup sub-directory under the main directory:"
  puts "backup_prevDeploy/html_msgs/.  All files inside the html messages directory are backed up."
  puts "The complete directory contents (or lack thereof) are used to overwrite the contents of the"
  puts "html messages directory during a revert."
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_rp1 rpxy:html_msgs:update -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp2 rpxy:html_msgs:revert -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp_all rpxy:html_msgs:checkfile -Stimestamp=YYYYMMDD"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap rpxy:html_msgs:help"
end

def printHttpdHelpMessage()
  puts "The expected command format to send start/stop/status/configtest commands to httpd is:"
  puts "  cap <hostname> rpxy:httpd:<task>"
  puts "This command expects a Reverse Proxy (RP) to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_rp1 rpxy:httpd:start"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp2 rpxy:httpd:stop"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp_all rpxy:httpd:status"
  puts "Or:"
  puts "  $ cap imaikeStaging_rp_all rpxy:httpd:configtest"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap rpxy:httpd:help"
end

def printDrApiHelpMessage()
  puts "The expected command format to update/revert/checkfile DR API on one or more hosts is:"
  puts "  cap <hostname> dr:api:<task> -Sbuild=<buildname>"
  puts "This command expects an Application (AP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <buildname> must specify an existing DR API folder+WAR file on hosts to be updated/checkfiled:"
  puts "  ~/#{baseDir}/drApi/<buildname>/<buildname>.war"
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_ap1 dr:api:update -Sbuild=YYYYMMDD_drApi"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap2 dr:api:revert -Sbuild=YYYYMMDD_drApi"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap_all dr:api:checkfile -Sbuild=YYYYMMDD_drApi"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap portal:api:help"
end

def printDrBatchHelpMessage()
  puts "The expected command format to update/revert/checkfile DR Batch on one or more hosts is:"
  puts "  cap <hostname> dr:batch:<task> -Sbuild=<buildname>"
  puts "This command expects a Batch Server (BP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <buildname> must specify an existing DR Batch folder+ZIP file on hosts to be updated/reverted/checkfiled:"
  puts "  ~/#{baseDir}/drBatch/<buildname>/<buildname>.zip"
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_bp1 dr:batch:update -Sbuild=YYYYMMDD_drBatch"
  puts "Or:"
  puts "  $ cap imaikeStaging_bp2 dr:batch:revert -Sbuild=YYYYMMDD_drBatch"
  puts "Or:"
  puts "  $ cap imaikeStaging_bp_all dr:batch:checkfile -Sbuild=YYYYMMDD_drBatch"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap portal:batch:help"
end

def printCsAclHelpMessage()
  puts "The expected command format to update/revert/checkfile CS ACL on one or more hosts is:"
  puts "  cap <hostname> cs:acl:<task> -Stimestamp=<timestamp>"
  puts "This command expects an Application Server (AP) host to be specified as the target host."
  puts "Tasks and hostnames (including *_all hostname groups) are defined in the portal.rb file."
  puts ""

  puts "The <timestamp> must specify an existing CS ACL folder on hosts to be updated/reverted/checkfiled:"
  puts "  ~/#{baseDir}/cs/<timestamp>/commands.properties"
  puts ""

  puts "Example:"
  puts "  $ cap imaikeStaging_ap1 cs:acl:update -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap2 cs:acl:revert -Stimestamp=YYYYMMDD"
  puts "Or:"
  puts "  $ cap imaikeStaging_ap_all cs:acl:checkfile -Stimestamp=YYYYMMDD"
  puts ""

  puts "To show this help message again:"
  puts "  $ cap cs:acl:help"
end
