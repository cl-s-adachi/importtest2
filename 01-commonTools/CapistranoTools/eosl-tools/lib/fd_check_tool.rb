# coding: utf-8

def self.get_result(command, options={}, &block)
   result = ""
   out = ""

   # 実行結果を取得
   begin
      out = capture(command, options)
   rescue Error => ex
      msg = ex.to_s
      puts "************************"
      puts ex.to_s
      puts "************************"
      result = :ERR
      if block_given? then
        block.call result, out
      end
      return result
   end
   out.chomp!
   
end


namespace(:fd_check_tool) do
  desc 'fd数確認'
  task :"score", acs49_target do
      columns = ['check_fd']
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         score['check_fd'] = get_result("get_pid=`sudo service cloudstack-agent status | awk '{ print $3 }' | sed 's/)//'`; sudo ls -1 /proc/${get_pid}/fd | wc -l ;true" ,options)
        end
  end

end

