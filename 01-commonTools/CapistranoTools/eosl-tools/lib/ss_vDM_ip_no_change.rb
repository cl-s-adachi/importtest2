# coding: utf-8

target_ss_path = fetch(:target_ss_path, nil)

def check_options(opt)
   if opt.nil?
     puts("ERROR: paramter(\"target_ss_path\") is missing")
     exit 1
   end
end


namespace(:ss_umountcheck_tool) do
  desc 'KVM-SS評価'
  task :"score", acs49_target do
      # 引数チェック
      check_options(target_ss_path)
      target_ss_grep = ":/" + target_ss_path

      columns = ['umount']
      # ホストごとに処理
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         # check mount status
         score['umount'] = capture_unmatch(
            "sudo LANG=C df -hP -t nfs 2>/dev/null | grep #{target_ss_grep} | awk {'print $1;'} ; true", true, options)
        end
  end

  desc 'KVM-マウント状態確認'
  task :"mount_all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         ss_umountcheck_tool.KVM_mount_status();      log_ok "cloudstack_KVM_mount"
        end
  end

  desc 'Check KVM mount status'
  task :"KVM_mount_status", acs49_cmd_filter do
      c_sudo "df -hP -t nfs; true"
  end

  desc 'KVM-ターゲットのマウント状態確認'
  task :"mount_target", acs49_target do
      # 引数チェック
      check_options(target_ss_path)

      start_commands(:task_namespace ,Logger::INFO) do
         ss_umountcheck_tool.KVM_target_mount_status();      log_ok "cloudstack_KVM_target_mount"
        end
  end

  desc 'Check KVM mount status'
  task :"KVM_target_mount_status", acs49_cmd_filter do
      target_ss_grep = ":/" + target_ss_path

      c_sudo "df -hP -t nfs 2>/dev/null | grep #{target_ss_grep};true"
  end
end

