# coding: utf-8

target_ss_path = fetch(:target_ss_path, nil)

def check_options(opt)
   if opt.nil?
     puts("ERROR: paramter(\"target_ss_path\") is missing")
     exit 1
   end
end

namespace(:iso_attachcheck_tool) do
  desc 'KVM-ターゲットでISOをアタッチしている仮想マシン名を表示'
  task :"vm_name", acs49_target do
      # 引数チェック
      check_options(target_ss_path)

      start_commands(:task_namespace ,Logger::INFO) do
          iso_attachcheck_tool.KVM_VM_name();      log_ok "cloudstack_ISOattach_check"
        end
  end

  desc 'KVM - cloudstack_ISOattach_check'
  task :"KVM_VM_name", acs49_cmd_filter do

      c_run "for list in $(mount | grep #{target_ss_path} | grep template | awk '{print $3}'); do getPID=`lsof ${list} | awk '{print $2}' | grep -v PID` ; if [ \"`echo $?`\" = \"0\" ]; then getVMname=`ps -ef | grep ${getPID} 2>/dev/null  | grep qemu-kvm | sed -e 's/^.*\\si-*\\(.*\\)-VM\\s.*$/i-\\1-VM/'` ; if [ -n \"${getVMname}\" ]; then echo ${list}  ${getVMname} ; fi ; fi ; done"

  end
end

