# coding: utf-8

target_ss_path = fetch(:target_ss_path, nil)

def check_options(opt)
   if opt.nil?
     puts("ERROR: paramter(\"target_ss_path\") is missing")
     exit 1
   end
end

namespace(:ss_umount_tool) do
  desc 'KVM-ターゲットのセカンダリストレージのアンマウント'
  task :"ss_umount", acs49_target do
      # 引数チェック
      check_options(target_ss_path)

      start_commands(:task_namespace ,Logger::INFO) do
         ss_umount_tool.KVM_target_umount();      log_ok "cloudstack_KVM_target_umount"
        end
  end

  desc 'KVM - SecondaryStorage umount'
  task :"KVM_target_umount", acs49_cmd_filter do

      c_run "RESULT=0;for list in $(sudo mount | grep #{target_ss_path} | awk '{ print $3 }' | sed 's#/mnt/##'); do sudo virsh pool-destroy ${list} ; if [ \"`echo $?`\" != \"0\" ]; then RESULT=1 ; fi ; done ; [ \"${RESULT}\" == \"0\" ]"

  end
end

namespace(:ss_IPumountcheck_tool) do
  desc 'KVM-ターゲットのマウント状態確認'
  task :"mount_target", acs49_target do
      # 引数チェック
      check_options(target_ss_path)

      start_commands(:task_namespace ,Logger::INFO) do
         ss_IPumountcheck_tool.KVM_target_mount_status();      log_ok "cloudstack_KVM_target_mount"
        end
  end

  desc 'Check KVM mount status'
  task :"KVM_target_mount_status", acs49_cmd_filter do

      c_sudo "mount | grep #{target_ss_path};true"
  end
end
