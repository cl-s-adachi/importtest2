# coding: utf-8

set :upgrade_packages_base_path, '/home/ckkcl/shapeblue10'
set :openjdk, 'openjdk version "1.8.0_161"'
set :new_cloudstack_agent, 'cloudstack-agent-4.9.1.0-shapeblue10.el6.x86_64'
set :new_cloudstack_common, 'cloudstack-common-4.9.1.0-shapeblue10.el6.x86_64'
set :old_cloudstack_agent, 'cloudstack-agent-4.9.1.0-shapeblue0.el6.x86_64'
set :old_cloudstack_common, 'cloudstack-common-4.9.1.0-shapeblue0.el6.x86_64'

start_date = fetch(:start_date, nil)
repo = fetch(:repo, nil)

def check_options(opt)
   if opt.nil?
     puts("ERROR: paramter(\"start_date\" OR \"repo\") is missing")
     exit 1
   end
end

def check_repo(repo)
   if repo.include?("east") then
     $cs_repo_baseurl = "http://172.27.172.227"
   elsif repo.include?("west") then
     $cs_repo_baseurl = "http://172.27.173.227"
   else
     puts("ERROR:repository(\"#{repo}\") is missing")
     exit 1
   end
end

namespace(:kvm_upgrade_shapeblue10) do
  desc 'shapeblue10-アップグレード'
  task :"all", acs49_target do
      # 引数チェック
      check_options(start_date)
      check_options(repo)
      check_repo(repo)
      
      start_commands do
         kvm_upgrade_shapeblue10.stop_kvm_agent();                        log_ok "stop_kvm_agent"
         kvm_upgrade_shapeblue10.backup_kvm_agent_settings();             log_ok "backup_kvm_agent_settings"
         kvm_upgrade_shapeblue10.install_dependencies();                  log_ok "install_dependencies"
         kvm_upgrade_shapeblue10.install_cloudstack_agent_packages();     log_ok "install_cloudstack_agent_packages"
         kvm_upgrade_shapeblue10.overwrite_kvm_agent_settings();          log_ok "overwrite_kvm_agent_settings"
         kvm_upgrade_shapeblue10.cloudstack_agent_properties();           log_ok "cloudstack_agent_properties"
         kvm_upgrade_shapeblue10.cloudstack_agent_runlevel();             log_ok "cloudstack_agent_runlevel"
         kvm_upgrade_shapeblue10.start_cloudstak_agent();                 log_ok "start_cloudstak_agent"
      end
  end

  desc 'shapeblue10-Shut down the KVM agent'
  task :"stop_kvm_agent", acs49_cmd_filter do
      c_sudo "/etc/init.d/cloudstack-agent stop"
      # jsvcのプロセスが存在しない場合があるため失敗しても無視する
      c_sudo "killall jsvc; true"
  end

  desc 'shapeblue10-Back up the existing KVM agent settings'
  task :"backup_kvm_agent_settings", acs49_cmd_filter do
      c_sudo "mkdir -p #{upgrade_packages_base_path}"
      c_sudo "mkdir -p #{upgrade_packages_base_path}/backup/"
      c_sudo "cp -p /etc/init.d/cloudstack-agent #{upgrade_packages_base_path}/backup/_etc_init.d_cloudstack-agent_#{start_date}"
      c_sudo "cp -p /etc/logrotate.d/cloudstack-agent #{upgrade_packages_base_path}/backup/_etc_logrotate.d_cloudstack-agent_#{start_date}"
  end

  desc 'shapeblue10-Install dependencies'
  task :"install_dependencies", acs49_cmd_filter do
      c_sudo "yum -y --disablerepo=* --enablerepo=rhel69_dvd,rhel69_update install libuuid.i686 lksctp-tools pcsc-lite-libs"
  end

  desc 'shapeblue10-Install the new Cloudstack-agent packages'
  task :"install_cloudstack_agent_packages", acs49_cmd_filter do
      check_repo(repo)
      c_sudo "rpm -U --nodeps #{$cs_repo_baseurl}/SHAPEBLUE10/#{new_cloudstack_common}.rpm"
      c_sudo "rpm -U --nodeps #{$cs_repo_baseurl}/SHAPEBLUE10/#{new_cloudstack_agent}.rpm"
  end

  desc 'shapeblue10-Restore existing KVM agent configuration'
  task :"overwrite_kvm_agent_settings", acs49_cmd_filter do
      # ファイル上書きで実施
      c_sudo "cp -p #{upgrade_packages_base_path}/backup/_etc_init.d_cloudstack-agent_#{start_date} /etc/init.d/cloudstack-agent"
      c_sudo "cp -p #{upgrade_packages_base_path}/backup/_etc_logrotate.d_cloudstack-agent_#{start_date} /etc/logrotate.d/cloudstack-agent"
  end

  desc 'shapeblue10-Change the value of worker in /etc/cloudstack/agent/environment.properties.'
  task :"cloudstack_agent_properties", acs49_cmd_filter do
      # ファイル内の置換で実施
      c_sudo "sed -ie '/^workers/s/5/20/gi' /etc/cloudstack/agent/agent.properties"
  end

  desc 'shapeblue10-Restore service to Runlevel'
  task :"cloudstack_agent_runlevel", acs49_cmd_filter do
      c_sudo "chkconfig --level 2 cloudstack-agent off"
  end

  desc 'shapeblue10-Start the agent:'
  task :"start_cloudstak_agent", acs49_cmd_filter do
      c_sudo "date; /etc/init.d/cloudstack-agent start"
  end
end

namespace(:kvm_check_shapeblue10) do
# namespace(:kvm_check_shapeblue10)とnamespace(:kvm_check_rollback_shapeblue10)は処理が一部(agent_propertiesのworkの値)を除き同一な為、
# 編集する際はもう片方のnamespace内の記述もあわせて編集すること
  desc 'KVM-shapeblue10評価'
  task :"score", acs49_target do

      columns = ['javap','jsvcp','javav','agtv','agtcv','agtcnf','chkconfig']
      # ホストごとに処理
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         # java process
         score['javap'] = capture_match(
            "ps afxwww |grep -v grep | grep -v classpath |grep java; true", true, options)

         # jsvc process
         score['jsvcp'] = capture_match(
            "ps afxwww |grep -v grep |grep jsvc; true", true, options)

         # java version
         score['javav'] = capture_match(
            "java -version",
            #/#{Regexp.escape('openjdk version "1.8.0_161"')}/,
            %r|#{openjdk}|,
            options)

         # cloudstack-agent version
         score['agtv'] = capture_match(
            "rpm -q cloudstack-agent",
            %r|#{new_cloudstack_agent}|,
            options)

         # cloudstack-common version
         score['agtcv'] = capture_match(
            "rpm -q cloudstack-common",
            %r|#{new_cloudstack_common}|,
            options)

         # agent.properties settings
         score['agtcnf'] = capture_match(
            "cat /etc/cloudstack/agent/agent.properties |grep workers; true",
            "workers=20",
            options)

         # check config
         score['chkconfig'] = capture_match(
            "sudo chkconfig --list cloudstack-agent | grep '0:off' | grep '1:off' | grep '2:off' | grep '3:on' | grep '4:on' | grep '5:on' | grep '6:off'; true", true,
            options)
      end
  end

  desc 'KVM-shapeblue10状態確認'
  task :"all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         kvm_check_shapeblue10.java_process();         log_ok "java_process"
         kvm_check_shapeblue10.jsvc_process();         log_ok "jsvc_process"
         kvm_check_shapeblue10.agent_properties();     log_ok "agent_properties"
         kvm_check_shapeblue10.cloudstack_agent();     log_ok "cloudstack_agent"
      end
  end

  desc 'shapeblue10-Check java process'
  task :"java_process", acs49_cmd_filter do
      # grep行を含めない
      c_sudo "ps afxwww |grep -v grep |grep java; true"
  end

  desc 'shapeblue10-Check jsvc process'
  task :"jsvc_process", acs49_cmd_filter do
      # grep行を含めない
      c_sudo "ps afxwww |grep -v grep |grep jsvc; true"
  end

  desc 'shapeblue10-Confirm agent.properties settings'
  task :"agent_properties", acs49_cmd_filter do
      c_sudo "cat /etc/cloudstack/agent/agent.properties |grep workers; true"
  end

  desc 'shapeblue10-Check the cloudstack agent'
  task :"cloudstack_agent", acs49_cmd_filter do
      c_sudo "rpm -q cloudstack-agent"
      c_sudo "rpm -q cloudstack-common"
      c_sudo "chkconfig --list cloudstack-agent"
      c_sudo "service cloudstack-agent status; true"
  end
end

namespace(:kvm_rollback_shapeblue10) do

  desc 'KVM-shapeblue10切り戻し'
  task :"all", acs49_target do
      # 引数チェック
      check_options(start_date)
      check_options(repo)
      check_repo(repo)

      start_commands do
         kvm_rollback_shapeblue10.stop_kvm_agent();                        log_ok "stop_kvm_agent"
         kvm_rollback_shapeblue10.install_cloudstack_agent_oldpackages();  log_ok "install_cloudstack_agent_oldpackages"
         kvm_rollback_shapeblue10.overwrite_kvm_agent_settings();          log_ok "overwrite_kvm_agent_settings"
         kvm_rollback_shapeblue10.restore_cloudstack_agent_properties();   log_ok "restore_cloudstack_agent_properties"
         kvm_rollback_shapeblue10.cloudstack_agent_runlevel();             log_ok "cloudstack_agent_runlevel"
         kvm_rollback_shapeblue10.start_cloudstak_agent();                 log_ok "start_cloudstak_agent"
      end
  end

  desc 'shapeblue10-Stop the KVM agent'
  task :"stop_kvm_agent", acs49_cmd_filter do
      c_sudo "date; /etc/init.d/cloudstack-agent stop"
      # jsvcのプロセスが存在しない場合があるため失敗しても無視する
      c_sudo "killall jsvc; true"
  end

  desc 'shapeblue10-Install the Current Cloudstack-agent packages'
  task :"install_cloudstack_agent_oldpackages", acs49_cmd_filter do
      check_repo(repo)
      c_sudo "rpm -U --nodeps --oldpackage #{$cs_repo_baseurl}/ACS/#{old_cloudstack_common}.rpm"
      c_sudo "rpm -U --nodeps --oldpackage #{$cs_repo_baseurl}/ACS/#{old_cloudstack_agent}.rpm"
  end

  desc 'shapeblue10-Restore existing KVM agent configuration'
  task :"overwrite_kvm_agent_settings", acs49_cmd_filter do
      # ファイル上書きで実施
      c_sudo "cp -p #{upgrade_packages_base_path}/backup/_etc_init.d_cloudstack-agent_#{start_date} /etc/init.d/cloudstack-agent"
      c_sudo "cp -p #{upgrade_packages_base_path}/backup/_etc_logrotate.d_cloudstack-agent_#{start_date} /etc/logrotate.d/cloudstack-agent"
  end

  desc 'shapeblue10-Change the value of worker in /etc/cloudstack/agent/environment.properties.'
  task :"restore_cloudstack_agent_properties", acs49_cmd_filter do
      # ファイル内の置換で実施
      c_sudo "sed -ie '/^workers/s/20/5/gi' /etc/cloudstack/agent/agent.properties"
  end

  desc 'shapeblue10-Restore service to Runlevel'
  task :"cloudstack_agent_runlevel", acs49_cmd_filter do
      c_sudo "chkconfig --level 2 cloudstack-agent off"
  end

  desc 'shapeblue10-Start the agent:'
  task :"start_cloudstak_agent", acs49_cmd_filter do
      c_sudo "date; /etc/init.d/cloudstack-agent start"
  end

end

namespace(:kvm_check_rollback_shapeblue10) do
# namespace(:kvm_check_shapeblue10)とnamespace(:kvm_check_rollback_shapeblue10)は処理が一部(agent_propertiesのworkの値)を除き同一な為、
# 編集する際はもう片方のnamespace内の記述もあわせて編集すること

  desc 'KVM-shapeblue10切り戻し後評価'
  task :"score", acs49_target do

     columns = ['javap','jsvcp','javav','agtv','agtcv','agtcnf','chkconfig']
     # ホストごとに処理
     caputer_servers(columns) do |current_server, score|
        options = {:hosts => current_server.host}

        # java process
        score['javap'] = capture_match(
           "ps afxwww |grep -v grep | grep -v classpath |grep java; true", true, options)

        # jsvc process
        score['jsvcp'] = capture_match(
           "ps afxwww |grep -v grep |grep jsvc; true", true, options)

        # java version
        score['javav'] = capture_match(
           "java -version",
            %r|#{openjdk}|,
           options)

        # cloudstack-agent version
        score['agtv'] = capture_match(
           "rpm -q cloudstack-agent",
            %r|#{old_cloudstack_agent}|,
           options)

        # cloudstack-common version
        score['agtcv'] = capture_match(
           "rpm -q cloudstack-common",
            %r|#{old_cloudstack_common}|,
           options)

        # agent.properties settings
        score['agtcnf'] = capture_match(
           "cat /etc/cloudstack/agent/agent.properties |grep workers; true",
           "workers=5",
           options)

        # check config
        score['chkconfig'] = capture_match(
           "sudo chkconfig --list cloudstack-agent | grep '0:off' | grep '1:off' | grep '2:off' | grep '3:on' | grep '4:on' | grep '5:on' | grep '6:off'; true", true,
           options)
     end
  end

  desc 'KVM-shapeblue10切り戻し後状態確認'
  task :"all", acs49_target do
     start_commands(:task_namespace ,Logger::INFO) do
        kvm_check_rollback_shapeblue10.java_process();         log_ok "java_process"
        kvm_check_rollback_shapeblue10.jsvc_process();         log_ok "jsvc_process"
        kvm_check_rollback_shapeblue10.agent_properties();     log_ok "agent_properties"
        kvm_check_rollback_shapeblue10.cloudstack_agent();     log_ok "cloudstack_agent"
     end
  end

  desc 'shapeblue10-Check java process'
  task :"java_process", acs49_cmd_filter do
     # grep行を含めない
     c_sudo "ps afxwww |grep -v grep |grep java; true"
  end

  desc 'shapeblue10-Check jsvc process'
  task :"jsvc_process", acs49_cmd_filter do
     # grep行を含めない
     c_sudo "ps afxwww |grep -v grep |grep jsvc; true"
  end

  desc 'shapeblue10-Confirm agent.properties settings'
  task :"agent_properties", acs49_cmd_filter do
     c_sudo "cat /etc/cloudstack/agent/agent.properties |grep workers; true"
  end

  desc 'shapeblue10-Check the cloudstack agent'
  task :"cloudstack_agent", acs49_cmd_filter do
     c_sudo "rpm -q cloudstack-agent"
     c_sudo "rpm -q cloudstack-common"
     c_sudo "chkconfig --list cloudstack-agent"
     c_sudo "service cloudstack-agent status"
  end
end
