# coding: utf-8
##------------------------------------------------------------------------------
require "capistrano/ext/multistage"

## 環境毎の設定はconfig/deploy/*.rbに記述する
set :stages, %w(production staging develop rpx local local_rpx)
set :default_stage, :develop
# stage のロード処理が、最初のタスクを実行する直前にフックされているため
# stage 内の変数が使用できないタイミングがある。強制的に利用できるようにする
trigger :start

default_run_options[:pty]=true

##------------------------------------------------------------------------------
## 対象ホストの指定
## 指定方法：同梱のtargetsディレクトリ内のhost.*-*-*.rbファイルを、
##           Capfileと同じ階層に「hosts.rb」としてコピーしてください。
##           その後「cap コマンド名」でコマンドを実行します。
##           コピーしたhostsファイルが正しいか確認する場合は
##           「cap hostname」で取得したホスト名が一致するか確認してください。

load "hosts.rb"

#-------------------------------------------------------------------------------

## Common Valiables

## Commonly used tasks

desc 'ホスト名を表示します'
task :hostname, :roles=>[:targets] do
   run "echo $HOSTNAME"
end

desc 'ホストの時刻を表示します'
task :date, :roles=>[:targets] do
   run "date"
end

desc 'ホストの情報を確認します'
task :"hostinfo", :roles=>[:targets] do
   run "date; uname -n; id"
end
