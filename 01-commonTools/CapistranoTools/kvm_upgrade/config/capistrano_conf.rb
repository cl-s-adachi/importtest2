### Please specify the target host,account and password to use in Capistrano.
role :targets, *%w()
set :user, "root"
set :password, "Admin123ckk!"

### Filter
set :acs49_target, {:roles=>[:targets]}
set :acs49_cmd_filter, {:roles=>[:targets], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}

### KVM host upgrade settings --------------------------------------------------------------------------------
#dir info
set :src_dir, "upload/Meltdown_spectre"
set :dest_dir, "/home/ckkcl/KCPS1_Meltdown_spectre"
# venom info
set :venom_tar_file, {:name => "VENOM-upgrade.tar.gz", :md5 => "795bcc6569cc0f824bc70d573d481fd0"}
set :venom_patch1, {:name => "glusterfs-3.6.0.29-2.el6.x86_64.rpm", :md5 => "7a888852783ead7d4f682480c62ef7fa"}
set :venom_patch2, {:name => "glusterfs-api-3.6.0.29-2.el6.x86_64.rpm", :md5 => "6f82ab62ffc53ffb50d1e8e0fe2112ae"}
set :venom_patch3, {:name => "glusterfs-libs-3.6.0.29-2.el6.x86_64.rpm", :md5 => "91a0d9fd5999f8778cdc989a61d9f267"}
set :venom_patch4, {:name => "openssl-1.0.1e-30.el6_6.2.x86_64.rpm", :md5 => "77288f1243c4bf199fc4b6f744f13c89"}
set :venom_patch5, {:name => "qemu-img-0.12.1.2-3.448.el6.3.x86_64.rpm", :md5 => "0a327bffae9a34b2a6e2d85247cc1f04"}
set :venom_patch6, {:name => "qemu-kvm-0.12.1.2-3.448.el6.3.x86_64.rpm", :md5 => "c3f2f58f1e0a5d309ec1b6bb18b567f3"}
set :venom_patch7, {:name => "seabios-0.6.1.2-28.el6.x86_64.rpm", :md5 => "95d715948d4fbd530babaed553c6db11"}
set :venom_patch8, {:name => "snappy-1.1.0-1.el6.x86_64.rpm", :md5 => "6463c47dcf740298c4bdd77155774546"}
set :venom_patch9, {:name => "spice-server-0.12.4-11.el6.x86_64.rpm", :md5 => "e5f1030efb9b562d388273e400919aa9"}
set :venom_patch10, {:name => "usbredir-0.5.1-1.el6.x86_64.rpm", :md5 => "7cb6f55c76a162bed900061721893283"}
set :venom_patch11, {:name => "zlib-1.2.3-29.el6.x86_64.rpm", :md5 => "cfdbb548b83742ab844737e18fc1ea88"}
# qemu info
set :qemu_img, "qemu-img-0.12.1.2-2.503.el6_9.4.x86_64.rpm"
set :qemu_kvm, "qemu-kvm-0.12.1.2-2.503.el6_9.4.x86_64.rpm"
set :qemues_md5, {:qemu_img => "73cc90b160e525462c6359d93b8e2f47", :qemu_kvm => "d7b08355f38103bb0ce38ba00c8a9682"}
set :qemues_rename, {:qemu_img => "/usr/bin/qemu-img", :cloud_qemu_img => "/usr/bin/cloud-qemu-img"}
# repo file info
set :rhel69_repo_file_tama_oyama, {:name => "rhel69.repo_tama_oyama", :rename => "rhel69.repo", :md5 => "3887700c13db0a3bc362a33b332ff7e8"}
set :rhel69_repo_file_imike_osaka, {:name => "rhel69.repo_imike_osaka", :rename => "rhel69.repo", :md5 => "9ef1443a5f96ebcbe3bee65af3c0bc4b"}
# rhel,meltdownpath info
set :redhat_release69, "Red Hat Enterprise Linux Server release 6.9 (Santiago)"
set :uname69, "2.6.32-696.20.1.el6.x86_64"
set :spectre_meltdown_script, {:name => "spectre_meltdown.sh", :md5 => "d49c82b5b65b1668ff263aca8e217335"}
# daemon runlevel info
set :cloudstack_agent_run_level_off, "cloudstack-agent\t0:off\t1:off\t2:off\t3:off\t4:off\t5:off\t6:off"
set :cloudstack_agent_run_level_on, "cloudstack-agent\t0:off\t1:off\t2:off\t3:on\t4:on\t5:on\t6:off"
set :libvirtd_run_level_off, "libvirtd       \t0:off\t1:off\t2:off\t3:off\t4:off\t5:off\t6:off"
set :libvirtd_run_level_on, "libvirtd       \t0:off\t1:off\t2:off\t3:on\t4:on\t5:on\t6:off"
# agent log modification info
set :agent_symbolic_link, "/var/log/cloudstack/agent"
set :cloudstack_agent_logrotate, {:name => "cloudstack-agent", :md5 => "ae3bf10ca19bd83c501db4940c0d4e66"}
# yum repositories
set :yum_repos, "--enablerepo=rhel69_dvd,rhel69_update"

### log settings --------------------------------------------------------------------------------
set :log_dir, "./logs"
set :log_file, "#{log_dir}/capistrano_output.log"
#set :log_level, Capistrano::Logger::IMPORTANT
set :log_level, Capistrano::Logger::INFO
