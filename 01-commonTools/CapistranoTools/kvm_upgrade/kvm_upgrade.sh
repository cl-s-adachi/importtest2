#!/bin/bash

#
# config file location
# TODO: where should common or shared files put?
# location is current directory
#
scripts_path=$(dirname $0)/config/scripts.conf
if [ -f "$scripts_path" ]
then
  . $scripts_path
else
  echo -e "configuration error: $0 requires $scripts_path\n"
  exit 1
fi

capistrano_path=$(dirname $0)/config/capistrano_conf.rb
if [ ! -f "$capistrano_path" ]
then
  echo -e "configuration error: $0 requires $capistrano_path\n"
  exit 1
fi

# Confirm upgrade execution file
tool_execution=$(dirname $0)/KvmUpgradeExecute.txt
if [ ! -f "$tool_execution" ]
then
  echo -e "configuration error: $0 requires $tool_execution\n"
  exit 1
fi

# Confirm args
if [ -z "$1" ]; then
  usage
fi

## Setting parameters
# boolean 
migrate=false
upgrade1=false
upgrade2=false
migrate_back=false
premium_host_flag=false
roll_back_flag=false
error_handling_flag=false
# タグ変更(共通)
change_host_tag_ha="HA"
# タグ変更(VALUEクラスター)
change_host_tag="VALUE"
temp_host_tag="VALUE_TEMP"
temp_hahost_tag="HATEMP_V"
# タグ変更(PREMIUMクラスター)
change_host_premium_tag="PREMIUM"
temp_premium="PREMIUM_TEMP"
temp_premium_ha="HATEMP_P"
# ファイルorディレクトリー
capfile="config/capistrano_conf.rb"
hahost_lists="./ha_host_lists.txt"
valuehost_lists="./value_host_lists.txt"
evidence_dir="./logs"
upgrade_log_file="$evidence_dir/capistrano_output.log"

# args examination
while (( $# > 0 ))
do
  case "$1" in
    -*)
      if [[ "$1" =~ 'U1' ]]; then
        upgrade1=true
        if [[ "$3" =~ '-T' ]]; then taskname=$4 ;fi
      elif [[ "$1" =~ 'U2' ]]; then
        upgrade2=true
        if [[ "$3" =~ '-T' ]]; then taskname=$4 ;fi
      fi
      if $upgrade1 || $upgrade2; then hostname=`echo $2 | awk -F '.' '{print $1}'` ;fi
      break
      ;;
    *)
      break
      ;;
  esac
done

if ! $upgrade1 && ! $upgrade2; then
  while getopts m:r:h OPTS
  do
    case $OPTS in
      m)
        hostname=`echo $OPTARG | awk -F '.' '{print $1}'`
        migrate=true
      ;;
      r)
        hostname=`echo $OPTARG | awk -F '.' '{print $1}'`
        migrate_back=true
      ;;
      h)  usage;;
      *)  usage;;
    esac
  done
fi

# マイグレーション戻しのvmリスト
list_dir="./vmlist"
target_migrate_vms="$list_dir/${hostname}_vm_lists.txt"
# マイグレーションツールのログ
log_file="$evidence_dir/${hostname}_migration.log"

# 正しいホストが選択されているか確認。
check_line=`get_target_host | wc -l`
if [ $check_line -ne 1 ]; then
  echo "Check the existence of the specified host."
  echo "Also, is the setting in $scripts_path correct?"
  echo "If all is OK, please check the execution method below."
  echo "./kvm_upgrade.sh -h"
  echo ""
  exit 1
fi

if ! $migrate_back && ! $upgrade1 && ! $upgrade2; then
  ######################
  ## マイグレーション ##
  ######################
  get_target_host |
  while read \
    host_id  host_uuid host_name host_status host_resource_state host_hypervisor host_tag cluster_id cluster_uuid cluster_name
  do
    if [ "$host_hypervisor" != "KVM" ]; then
      log "  this Host is not KVM"
      exit 1
    elif [ "$host_status" != "Up" ]; then
      log "  this Host is not UP"
      exit 1
    fi
 
    log ""
    log "  ***************************"
    log "    target host information  "
    log "  ***************************"
    log "    id              = $host_id"
    log "    uuid            = $host_uuid"
    log "    name            = $host_name"
    log "    status          = $host_status"
    log "    resource_state  = $host_resource_state"
    log "    hypervisor      = $host_hypervisor"
    log "    tag             = $host_tag"
    log "    cluster_name    = $cluster_name"
    log "  --------------------------------------------------------"

    # 起動中のVMがcloudstack上とKVM上で一致しているか確認。はぐれVMの確認
    checkZombieInstance $hostname $host_uuid $cluster_id
    # プレミアムクラスターか確認
    echo 0 > $res_temp
    get_check_premium_cluster1 $cluster_id |
    while read tag
    do
      if [ $tag == "PREMIUM" ]; then
        echo "1" > $res_temp
        break
      elif [[ $tag =~ "@" ]]; then
        check_account=`echo $tag | awk -F '@' '{print $1}'`
        check_domain=`echo $tag | awk -F '@' '{print $2}'`
        check_domain=`echo $check_domain | awk -F '_' '{print $1}'`
        check_num=`get_check_premium_cluster2 $check_account $check_domain | wc -l`
        if [ $check_num -eq 1 ]; then
          echo "1" > $res_temp
          break
        else
          continue
        fi
      else
        continue
      fi
    done
    check_num1=$(cat $res_temp)
    check_num2=`get_ha_host $cluster_id $host_uuid | wc -l` # HAホストが存在するか確認
    check_account=`echo $host_tag | awk -F '@' '{print $1}'`
    check_domain=`echo $host_tag | awk -F '@' '{print $2}'`
    check_domain=`echo $check_domain | awk -F '_' '{print $1}'`
    check_num3=`get_check_premium_cluster2 $check_account $check_domain | wc -l`
    check_num4=`mysql --silent --skip-column-names $connect_string cloud -e "select t.tag from vm_instance v join host h on v.host_id=h.id join host_tags t on h.id = t.host_id where h.name like '%$hostname%' and h.removed is null and t.tag in ('HA','PREMIUM')" | wc -l`
    if [ $check_num1 -eq 1 ]; then
      # PREMIUMクラスター
      premium_host_flag=true
      if [ $check_num2 -eq 0 ] && [ $check_num3 -ne 0 ]; then
        log "  HA host is not exists or not status Up"
        log "  Please prepare HA host Then try again."
        exit 1
      elif [ $check_num4 -ne 0 ]; then
        log "  This host exists in the premium cluster."
        log "  The tag of this host is HA or PREMIUM."
        log "  Processing is terminated because VM exists on this host."
        exit 1
      elif [[ $host_tag =~ "@" ]]; then
        check_account=`echo $host_tag | awk -F '@' '{print $1}'`
        check_domain=`echo $host_tag | awk -F '@' '{print $2}'`
        check_num=`get_check_premium_cluster2 $check_account $check_domain | wc -l`
        if [ $check_num -eq 1 ]; then change_tag=${host_tag}_TEMP; fi
      elif [ $host_tag == "PREMIUM" ]; then
        change_tag=$temp_premium
      elif [ $host_tag == "HA" ]; then
        change_tag=$temp_premium_ha
      else
        log ""
        log "  Stop this script because hosttag is a character string that does not match account@domain, PREMIUM and HA."
        exit 1
      fi
      if [ ! -z $change_tag ]; then
        change_host_tag $host_uuid $change_tag
        if [ $? -eq 1 ]; then
          log "  failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
        # タグ変更後のホスト情報の表示
        get_check_host_tag $cluster_id $host_uuid |
        while read \
          h_ch_name h_ch_uuid h_ch_status h_ch_tag
        do
          log "  *******************************"
          log "    Changed $host_name's tag from $host_tag to $h_ch_tag "
          log "  *******************************"
          log "    name            = $h_ch_name"
          log "    uuid            = $h_ch_uuid"
          log "    status          = $h_ch_status"
          log "    tag             = $h_ch_tag"
          log "  --------------------------------------------------------"

          if [ "$h_ch_status" != "Up" ]; then
            log "    this Host is not Up"
            # タグ戻し
            tag_return $cluster_id $host_uuid
            exit 1
          fi
        done
        [[ $? -eq 1 ]] && exit 1
      fi
      if [ $check_num3 -eq 1 ]; then
        # 移行先HAホスト情報
        log ""
        log "  ***********************************"
        log "    Destination HA host information  "
        log "  ***********************************"
        get_ha_host $cluster_id $host_uuid |
        while read \
          ha_id ha_name ha_uuid ha_status ha_cluster_name ha_tag
        do
          if [ ! -z "$host_name" ]; then
            log "    id              = $ha_id"
            log "    name            = $ha_name"
            log "    uuid            = $ha_uuid"
            log "    status          = $ha_status"
            log "    cluster_name    = $ha_cluster_name"
            log "    tag             = $ha_tag"
            log "  --------------------------------------------------------"
            echo "$ha_name $ha_uuid" >> $hahost_lists
          fi
        done
        if [ ! -e $hahost_lists ]; then
          log "    do not exist."
          log "  --------------------------------------------------------"
          log "\\n  The target host does not exist."
          log "  Perform tag return."
          # タグ戻し
          tag_return $cluster_id $host_uuid
          exit 1
        fi
      fi
    else
      # VALUEクラスター
      if [ $host_tag == "VALUE" ]; then
        value_host_from_ha_uuid=`mysql ${connect_string} cloud -N -e "select h.uuid from host h join host_tags t on h.id = t.host_id join op_host_capacity hc on h.id=hc.host_id where h.cluster_id = $cluster_id and t.tag = 'HA' and h.status = 'Up' and h.uuid <> '$host_uuid' and h.removed is null order by hc.used_capacity asc" | head -1 | awk -F\. '{print $1}'` 
        change_host_tag $host_uuid $temp_host_tag
        if [ $? == 1 ]; then
          log "  failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
        # タグ変更後のホスト情報の表示
        get_check_host_tag $cluster_id $host_uuid |
        while read \
          h_ch_name h_ch_uuid h_ch_status h_ch_tag
        do
          log "  *******************************"
          log "    Changed $host_name's tag from $host_tag to $h_ch_tag "
          log "  *******************************"
          log "    name            = $h_ch_name"
          log "    uuid            = $h_ch_uuid"
          log "    status          = $h_ch_status"
          log "    tag             = $h_ch_tag"
          log "  --------------------------------------------------------"

          if [ "$h_ch_status" != "Up" ]; then
            log "    this Host is not Up"
            change_host_tag $host_uuid $change_host_tag
            exit 1
          fi
        done
        [[ $? -eq 1 ]] && exit 1
        log ""
        log "  Change one HA host to VALUE host..."
        change_host_tag $value_host_from_ha_uuid $change_host_tag
        if [ $? == 1 ]; then
          log "  failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
      elif [ $host_tag == "HA" ]; then
        change_host_tag $host_uuid $temp_hahost_tag
        if [ $? == 1 ]; then
          log "  failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
        # タグ変更後のホスト情報の表示
        get_check_host_tag $cluster_id $host_uuid |
        while read \
          h_ch_name h_ch_uuid h_ch_status h_ch_tag
        do
          log "  *******************************"
          log "    Changed $host_name's tag from $host_tag to $h_ch_tag "
          log "  *******************************"
          log "    name            = $h_ch_name"
          log "    uuid            = $h_ch_uuid"
          log "    status          = $h_ch_status"
          log "    tag             = $h_ch_tag"
          log "  --------------------------------------------------------"

          if [ "$h_ch_status" != "Up" ]; then
            log "    this Host is not Up"
            # タグ戻し
            tag_return $cluster_id $host_uuid
            exit 1
          fi
        done
      else
        if [ $host_tag != $temp_host_tag ] && [ $host_tag != $temp_hahost_tag ]; then
          log ""
          log "  Stop this script because hosttag is a character string that does not match VALUE and HA."
          exit 1
        fi
      fi
      if [ $? == 1 ] ; then exit 1;fi
      # 移行先VALUEホスト情報
      log ""
      log "  **************************************"
      log "    Destination VALUE host information  "
      log "  **************************************"
      rm -f $valuehost_lists
      get_value_host $cluster_id $host_uuid |
      while read \
        val_id val_name val_uuid val_status val_cluster_name val_tag
      do
        log "    id              = $val_id"
        log "    name            = $val_name"
        log "    uuid            = $val_uuid"
        log "    status          = $val_status"
        log "    cluster_name    = $val_cluster_name"
        log "    tag             = $val_tag"
        log "  --------------------------------------------------------"
        touch $valuehost_lists
        echo "$val_name $val_uuid" >> $valuehost_lists
      done
      if [ ! -e $valuehost_lists ]; then
        log "    do not exist."
        log "  --------------------------------------------------------"
        log "\\n  The target host does not exist."
        log "  Perform tag return."
        # タグ戻し
        tag_return $cluster_id $host_uuid
        exit 1
      fi
    fi
    if [ $? == 1 ] ; then exit 1;fi

    # マイグレーション戻しの対象VMリストの生成
    if [ ! -e $target_migrate_vms ]; then
      dir=/path/to/dir; [ ! -e $list_dir ] && mkdir -p $list_dir
      touch $target_migrate_vms
      get_migrate_vm $host_uuid $cluster_id |
      while read \
        vm_id vm_name vm_instance_name vm_uuid vm_state vm_type vm_host_name
      do
        echo "$vm_uuid" >> $target_migrate_vms
      done
    else
      get_migrate_vm $host_uuid $cluster_id |
      while read \
        vm_id vm_name vm_instance_name vm_uuid vm_state vm_type vm_host_name
      do
        check_line=`cat $target_migrate_vms | grep -w $vm_uuid | wc -l`
        if [ $check_line -eq 0 ]; then
          echo "$vm_uuid" >> $target_migrate_vms
        else
          break
        fi
      done
    fi
    get_migrate_vm $host_uuid $cluster_id |
    while read \
      vm_id vm_name vm_instance_name vm_uuid vm_state vm_type vm_host_name
    do
      # Confirm upgrade execution file
      if [ ! -f "$tool_execution" ]; then
        log ""
        log "  $tool_execution is not exist"
        log "  configuration error: $0 requires $tool_execution"
        break
      fi
      # スナップショットの有無確認
      do_check_snapshots $cluster_id $host_uuid
      if [ $? -eq 1 ]; then break; fi
      log ""
      purple Before
      log "  ***********************************"
      log "    migration target vm information  "
      log "  ***********************************"
      log "    name            = $vm_name"
      log "    instance_name   = $vm_instance_name"
      log "    uuid            = $vm_uuid"
      log "    state           = $vm_state"
      log "    hostname        = $vm_host_name"

      # KVMホスト内のゴミスナップショットの有無確認
      checkTrashSnapshot $cluster_id $host_uuid $vm_uuid
      if [ $? -eq 1 ]; then break; fi
      if [ "$vm_type" == "ConsoleProxy" ] || [ "$vm_type" == "SecondaryStorageVm" ] ; then
        log "  SSVM is not processed. vm_uuid:$vm_uuid"
      elif [ "$vm_type" == "DomainRouter" ] ; then
        log "  VR is not processed. vm_uuid:$vm_uuid"
      else
        cmd="listVirtualMachines"
        migratecmd="migrateVirtualMachine"
        if $premium_host_flag; then
          do_migrate_vm $vm_uuid $cmd $migratecmd
        else
          do_migrate_vm_val $vm_uuid $cmd $migratecmd
        fi
        return_code="$?"
        if [ $return_code -eq 0 ]; then
          continue
        elif [ $return_code -eq 1 ]; then
          break
        fi
      fi
    done
    # DBからVMの存在確認
    check_vm=`get_migrate_vm $host_uuid $cluster_id | wc -l`
    if [ $check_vm -eq 0 ]; then
      log ""
      log "  Migration of all VMs has been completed."
      log ""
      # 事前にmountされたプライマリーストレージの数を確認
      do_ps_count $host_uuid
      # メンテナンス投入
      do_prepareHostForMaintenance $host_uuid
    else
      log ""
      log "  Migration of all VMs has not been completed."
      log "  Please confirm status from the portal and logs."
      log ""
    fi
  done   
  rm -f $res_temp $hahost_lists $valuehost_lists $if_temp

elif ( $upgrade1 || $upgrade2 ) && [ -z $taskname ]; then
  ############################
  ## kvmホストのupgrade処理 ##
  ############################
  hostname=`echo $hostname | awk -F '.' '{print $1}'`
  check_count=`cat $capfile | grep -v "^#" | grep "role :targets" | grep "*%w(" | grep ")" | wc -l`
  if [ $check_count -eq 0 ]; then
    sed -i -e "1i role :targets, *%w($hostname)" $capfile
  else
    for row in `cat -n $capfile | grep -v "#role :targets" | grep "role :" | grep "*%w(" | grep ")" | cut -d$'\t' -f1`;
    do
      sed -i -e "${row} d" $capfile
      sed -i -e "${row} i role :targets, *%w($hostname)" $capfile
    done
  fi
  if $upgrade1; then
    # アップグレード前半
    do_kvm_upgrade1
  else
    # アップグレード後半
    do_kvm_upgrade2
  fi
elif ( $upgrade1 || $upgrade2 ) && [ ! -z $taskname ]; then
  ################################################
  ## kvmホストのupgrade処理(指定タスクより実行) ##
  ################################################
  hostname=`echo $hostname | awk -F '.' '{print $1}'`
  check_count=`cat $capfile | grep -v "^#" | grep "role :targets" | grep "*%w(" | grep ")" | wc -l`
  if [ $check_count -eq 0 ]; then
    sed -i -e "1i role :targets, *%w($hostname)" $capfile
  else
    for row in `cat -n $capfile | grep -v "#role :targets" | grep "role :" | grep "*%w(" | grep ")" | cut -d$'\t' -f1`;
    do
      sed -i -e "${row} d" $capfile
      sed -i -e "${row} i role :targets, *%w($hostname)" $capfile
    done
  fi
  if [ -f $upgrade_log_file ]; then mv $upgrade_log_file "${upgrade_log_file}_`date +'%Y%m%d%H%M%S'`.log"; fi
  clear
  if $upgrade1; then
    # アップグレード前半
    tasklist="./config/KvmUpgradeTaskListFirstHalf"
    ruby ./lib/cap/do_match_task.rb $tasklist $taskname
  else
    # アップグレード後半
    tasklist="./config/KvmUpgradeTaskListLatterHalf"
    ruby ./lib/cap/do_match_task.rb $tasklist $taskname
  fi
  # ログにタイムスタンプを付与
  if [ -f $upgrade_log_file ]; then mv $upgrade_log_file "$evidence_dir/${hostname}_`date +'%Y%m%d%H%M%S'`_upgrade.log"; fi
else
  # マイグレ戻しの処理
  hostname=$hostname

  get_target_host |
  while read \
    host_id  host_uuid host_name host_status host_resource_state host_hypervisor host_tag cluster_id cluster_uuid cluster_name
  do
    if [ "$host_hypervisor" != "KVM" ]; then
      log "  this Host is not KVM"
      exit 1
    fi
    log ""
    log "  ***************************"
    log "    target host information  "
    log "  ***************************"
    log "    id              = $host_id"
    log "    uuid            = $host_uuid"
    log "    name            = $host_name"
    log "    status          = $host_status"
    log "    resource_state  = $host_resource_state"
    log "    hypervisor      = $host_hypervisor"
    log "    tag             = $host_tag"
    log "    cluster_name    = $cluster_name"
    log "  --------------------------------------------------------"

    # メンテナンスモード解除
    do_cancelHostMaintenance $host_uuid

    # マウントされたプライマリーストレージの事前、事後の数量が一致しているか
    do_ps_count $host_uuid check

    # プレミアムクラスターか確認
    echo 0 > $res_temp
    get_check_premium_cluster1 $cluster_id |
    while read tag
    do
      if [ $tag == "PREMIUM" ]; then
        echo "1" > $res_temp
        break
      elif [[ $tag =~ "@" ]]; then
        check_account=`echo $tag | awk -F '@' '{print $1}'`
        check_domain=`echo $tag | awk -F '@' '{print $2}'`
        check_domain=`echo $check_domain | awk -F '_' '{print $1}'`
        check_num=`get_check_premium_cluster2 $check_account $check_domain | wc -l`
        if [ $check_num -eq 1 ]; then
          echo "1" > $res_temp
          break
        else
          continue
        fi
      else
        continue
      fi
    done
    check_num1=$(cat $res_temp)
    if [ $check_num1 -eq 1 ]; then
      premium_host_flag=true
      # タグ戻し
      return_flag=true
      # 対象ホストタグは「account@domain」or 「PREMIUM」or「HA」である場合はタグ変更しない
      if [[ $host_tag =~ "@" ]]; then
        check_account=`echo $host_tag | awk -F '@' '{print $1}'`
        check_domain=`echo $host_tag | awk -F '@' '{print $2}'`
        check_num=`get_check_premium_cluster2 $check_account $check_domain | wc -l`
        if [ $check_num -eq 1 ]; then return_flag=false; fi
      elif [ $host_tag == "PREMIUM" ] || [ $host_tag == "HA" ]; then
        log ""
        log "  Tag return complete."
        log "  Bye..."
        log ""
        exit 0
      fi
      # 対象ホストタグは「account@domain_TEMP」or 「PREMIUM_TEMP」or「HATEMP_P」である場合はタグ変更する
      if $return_flag; then
        if [[ $host_tag =~ "@" ]]; then
          return_tag=`echo $host_tag | awk -F '_' '{print $1}'`
        elif [ $host_tag == $temp_premium ]; then
          return_tag=$change_host_premium_tag
        elif [ $host_tag == $temp_premium_ha ]; then
          return_tag=$change_host_tag_ha
        fi
        change_host_tag $host_uuid $return_tag
        if [ $? == 1 ]; then
          log "failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
        # タグ変更後のホスト情報の表示
        get_check_host_tag $cluster_id $host_uuid |
        while read \
          h_ch_name h_ch_uuid h_ch_status h_ch_tag
        do
          log "  *******************************"
          log "    Changed $host_name's tag from $host_tag to $h_ch_tag "
          log "  *******************************"
          log "    name            = $h_ch_name"
          log "    uuid            = $h_ch_uuid"
          log "    status          = $h_ch_status"
          log "    tag             = $h_ch_tag"
          log "  --------------------------------------------------------"

          if [ "$h_ch_status" != "Up" ]; then
            log "    this Host is not Up"
            exit 1
          fi

          if [ $h_ch_tag == "HA" ] || [ $h_ch_tag == "PREMIUM" ]; then
            log ""
            log "  Tag return completed."
            log ""
            exit 1
          fi
        done
        [[ $? -eq 1 ]] && exit 1
      fi
      # VMリストの確認
      if [ ! -f $target_migrate_vms ]; then
        log "  $target_migrate_vms is not exists."
        log "  Please Create $target_migrate_vms and write target vm_uuid."
        log ""
        exit 1
      elif [ ! -s $target_migrate_vms ]; then
        log "  $target_migrate_vms is empty."
        log "  If there is a VM to return, write target vm_uuid in $target_migrate_vms."
        log ""
        exit 1
      fi
      # 対象のホストにlistファイルのvmがマイグレーション可能か確認
      while read vm_uuid
      do
        check_vm=`get_migrate_vm_roll_back $cluster_id $host_uuid $vm_uuid | wc -l`
        if [ $check_vm -eq 1 ]; then
          roll_back_flag=true
          break
        fi
      done < $target_migrate_vms

      if ! $roll_back_flag; then
        # 指定したホストにロールバック出来るVMがvm_listsファイルに存在しない。vmが起動していない、既にそのホストに存在、クラスターが異なる等考えられる。
        log ""
        log "  There is no list of VMs that can be restore."
        log "  bye..."
      fi
    else
      if [ $host_tag == $temp_host_tag ] || [ $host_tag == $temp_hahost_tag ]; then
        change_host_tag $host_uuid $change_host_tag_ha
        if [ $? == 1 ]; then
          log "  failed change host tag api. this script stopped. please check cloud-management status."
          exit 1
        fi
        # タグ変更後のホスト情報の表示
        get_check_host_tag $cluster_id $host_uuid |
        while read \
          h_ch_name h_ch_uuid h_ch_status h_ch_tag
        do
          log "  *******************************"
          log "    Changed $host_name's tag from $host_tag to $h_ch_tag "
          log "  *******************************"
          log "    name            = $h_ch_name"
          log "    uuid            = $h_ch_uuid"
          log "    status          = $h_ch_status"
          log "    tag             = $h_ch_tag"
          log "  --------------------------------------------------------"

          if [ "$h_ch_status" != "Up" ]; then
            log "  this Host is not Up"
            exit 1
          fi
        done
        log ""
        log "  Tag return completed."
        log ""
      else
        log ""
        log "  Perhaps, the tag has already been changed."
        log "  hostname:$hostname tag:$host_tag"
        log ""
      fi
      exit 0
    fi

    while read vm_uuid
    do
      # マイグレ戻し
      get_migrate_vm_roll_back $cluster_id $host_uuid $vm_uuid |
      while read \
        vm_name vm_instance_name vm_uuid vm_state vm_type vm_host_name
      do
        # Confirm upgrade execution file
        if [ ! -f "$tool_execution" ]; then
          log ""
          log "  $tool_execution is not exist"
          log "  configuration error: $0 requires $tool_execution\n"
          exit 1
        fi
        # スナップショットの有無確認
        sorcehost_uuid=`mysql ${connect_string} cloud -N -e "select h.uuid from vm_instance v join host h on v.host_id=h.id where v.uuid='$vm_uuid' and h.uuid <> '$host_uuid'"`
        do_check_snapshots $cluster_id $sorcehost_uuid
        if [ $? -eq 1 ]; then exit 1; fi
        log ""
        purple Before
        log "  ******************************* "
        log "    migration target vm information  "
        log "  ******************************* "
        log "    name            = $vm_name"
        log "    instance_name   = $vm_instance_name"
        log "    uuid            = $vm_uuid"
        log "    state           = $vm_state"
        log "    hostname        = $vm_host_name"

        # KVMホスト内のゴミスナップショットの有無確認
        checkTrashSnapshot $cluster_id $sorcehost_uuid $vm_uuid
        if [ $? -eq 1 ]; then break; fi
        if [ "$vm_type" == "ConsoleProxy" ] || [ "$vm_type" == "SecondaryStorageVm" ] ; then
          log "  SSVM is not processed. vm_uuid:$vm_uuid"
        elif [ "$vm_type" == "DomainRouter" ] ; then
          log "  VR is not processed. vm_uuid:$vm_uuid"
        else
          cmd="listVirtualMachines"
          migratecmd="migrateVirtualMachine"
          do_migrate_vm_roll_back $vm_uuid $cmd $migratecmd
          return_code="$?"
          if [ $return_code -eq 0 ]; then
            continue 2
          elif [ $return_code -eq 1 ]; then
            exit 1
          fi
        fi
      done
      [[ $? -eq 1 ]] && break
    done < $target_migrate_vms

    check_vm_count1=`get_migrate_vm $host_uuid $cluster_id | wc -l`
    check_vm_count2=`cat $target_migrate_vms | wc -l`
    if [ $check_vm_count1 == $check_vm_count2 ]; then
      log ""
      log "  Migration of all VMs has been completed."
      log ""
    else
      log ""
      log "  Migration of all VMs has not been completed."
      log "  Please confirm status from the portal and logs."
      log ""
    fi
  done   
  rm -f $res_temp $valuehost_lists
fi
# migrationlog rename
if $migrate; then
  if [ -f $log_file ]; then mv $log_file "$evidence_dir/${hostname}_`date +'%Y%m%d%H%M%S'`_migration_m.log"; fi
elif $migrate_back; then
  if [ -f $log_file ]; then mv $log_file "$evidence_dir/${hostname}_`date +'%Y%m%d%H%M%S'`_migration_r.log"; fi
fi
