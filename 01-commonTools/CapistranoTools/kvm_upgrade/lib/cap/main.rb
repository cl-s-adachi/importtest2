# coding: utf-8
##------------------------------------------------------------------------------
require "lib/cap/extension/multistage.rb"
require 'fileutils'
# Capistrano Custom Extensions
load 'lib/cap/extension/custom_methods.rb'
# Sub-file
load 'lib/cap/upgrade.rb'
load "lib/cap/common.rb"

## config/*.rbに記述する
set :default_stage, :capistrano_conf
# stage のロード処理が、最初のタスクを実行する直前にフックされているため
# stage 内の変数が使用できないタイミングがある。強制的に利用できるようにする
trigger :start
default_run_options[:pty] = true

namespace(:kvm_host_upgrade) do
  ## Valiables

  desc 'アップグレード1'
  task :"all_1", acs49_cmd_filter do
    start_commands2 do
      kvm_host.upgrade.dest_dir_check(); ps_ok footer("dest_dir_check")
      kvm_host.upgrade.upload_file_all(); ps_ok footer("upload_file_all")
      kvm_host.upgrade.check_md5sum(); ps_ok footer("check_md5sum")
      kvm_host.upgrade.cloudstack_agent_stop(); ps_ok footer("cloudstack_agent_stop")
      kvm_host.upgrade.cloudstack_agent_status_check_stopped(); ps_ok footer("cloudstack_agent_status_check_stopped")
      kvm_host.upgrade.libvirtd_stop(); ps_ok footer("libvirtd_stop")
      kvm_host.upgrade.libvirtd_status_check_stopped(); ps_ok footer("libvirtd_status_check_stopped")
      kvm_host.upgrade.runlevel_off(); ps_ok footer("runlevel_off")
      kvm_host.upgrade.runlevel_off_check(); ps_ok footer("runlevel_off_check")
      kvm_host.upgrade.put_rhel69_repo(); ps_ok footer("put_rhel69_repo")
      #kvm_host.upgrade.tmp_task(); ps_ok footer("tmp_task") # 本番環境(物理ホスト)で実施の際は本タスクはコメントアウトすること
      kvm_host.upgrade.do_venom_package(); ps_ok footer("do_venom_package")
      kvm_host.upgrade.del_matahari_package(); ps_ok footer("del_matahari_package")
      kvm_host.upgrade.del_matahari_package_check(); ps_ok footer("del_matahari_package_check")
      kvm_host.upgrade.rhel69_upgrade(); ps_ok footer("rhel69_upgrade")
      kvm_host.upgrade.os_stop(); ps_ok footer("os_stop")
    end
  end

  desc 'アップグレード2'
  task :"all_2", acs49_cmd_filter do
    start_commands2 do
      kvm_host.upgrade.os_check(); ps_ok footer("os_check")
      kvm_host.upgrade.meltdown_patch_check(); ps_ok footer("meltdown_patch_check")
      kvm_host.upgrade.qemu_rename(); ps_ok footer("qemu_rename")
      kvm_host.upgrade.qemu_rename_check(); ps_ok footer("qemu_rename_check")
      kvm_host.upgrade.qemu_install(); ps_ok footer("qemu_install")
      kvm_host.upgrade.qemu_package_check(); ps_ok footer("qemu_package_check")
      kvm_host.upgrade.modify_log_dir(); ps_ok footer("modify_log_dir")
      kvm_host.upgrade.libvirtd_start(); ps_ok footer("libvirtd_start")
      kvm_host.upgrade.libvirtd_status_check_running(); ps_ok footer("libvirtd_status_check_running")
      kvm_host.upgrade.cloudstack_agent_start(); ps_ok footer("cloudstack_agent_start")
      kvm_host.upgrade.cloudstack_agent_status_check_running(); ps_ok footer("cloudstack_agent_status_check_running")
      kvm_host.upgrade.modify_log_dir_check(); ps_ok footer("modify_log_dir_check")
      kvm_host.upgrade.runlevel_on; ps_ok footer("runlevel_on")
      kvm_host.upgrade.runlevel_on_check; ps_ok footer("runlevel_on_check")
    end
  end
end
