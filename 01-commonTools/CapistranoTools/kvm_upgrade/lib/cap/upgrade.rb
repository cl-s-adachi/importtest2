# coding: utf-8

# kvm6.9 Tasks required for upgrade

namespace(:kvm_host) do
  desc 'ファイル転送'
  task :upload_file, :roles => [:targets] do
    puts fetch(:src_path)
    puts fetch(:dest_path)
    do_upload_file(src_path, dest_path)
  end

  namespace(:upgrade) do
    desc 'アップロード先ディレクトリの確認・作成'
    task :dest_dir_check, :roles => [:targets] do
      unless skip.nil?
        header()
        # ディレクトリーの確認
        cmd = "mkdir -p #{dest_dir}"
        do_display(cmd)
      end
    end

    desc '各資材のアップロード'
    task :upload_file_all, :roles => [:targets] do
      unless skip.nil?
        header()
        east = ["ejo", "tky"]
        west = ["ckkoskc", "wjih"]
        dest_path = "#{dest_dir}/."
        src_qemues = {:qemu_img => src_dir + "/" + qemu_img, :qemu_kvm => src_dir + "/" + qemu_kvm}
        dest_qemues = {:qemu_img => dest_dir + "/" + qemu_img, :qemu_kvm => dest_dir + "/" + qemu_kvm}
        # qemuファイルのアップロード
        i = src_qemues.values.length-1
        for i in 0..i do
          do_upload_file(src_qemues.values[i], dest_path)
        end
        # spectre_meltdown.shファイルのアップロード
        src_spectre_meltdown_script = src_dir + "/" + spectre_meltdown_script[:name]
        do_upload_file(src_spectre_meltdown_script, dest_path) 
        # rhel69リポジトリファイルのアップロード
        current_host do |host|
          if east.any? { |e| host.include? e }
            src_rhel69_repo_file = src_dir + "/" + rhel69_repo_file_tama_oyama[:name] 
            do_upload_file(src_rhel69_repo_file, dest_path) 
          elsif west.any? { |w| host.include? w }
            src_rhel69_repo_file = src_dir + "/" + rhel69_repo_file_imike_osaka[:name] 
            do_upload_file(src_rhel69_repo_file, dest_path) 
          else
            puts output = "#{host}: upload rhel69.repo => #{red('NG')}"
            custom_logger;info(host, output)
            puts output = "host name is not mutch the following"
            custom_logger;info(host, output)
            puts output = "************************"
            custom_logger;info(host, output)
            puts east,west
            custom_logger;info(host, east.join("\n"))
            custom_logger;info(host, west.join("\n"))
            puts output = "************************"
            custom_logger;info(host, output)
            set_error_flag()
          end
        end
        # VENOMパッチ適応に必要なファイル群
        venomfiles = src_dir + "/" + venom_tar_file[:name]
        do_upload_file(venomfiles, dest_path) 
        cmd = "md5sum #{dest_dir}/#{venom_tar_file[:name]}"
        do_check(cmd, venom_tar_file[:md5])
        # logrotate.dに配置するファイル
        logrotate_file = src_dir + "/" + cloudstack_agent_logrotate[:name]
        do_upload_file(logrotate_file, dest_path) 
      end
    end

    desc 'MD5SUM 確認'
    task :check_md5sum, :roles => [:targets] do
      unless skip.nil?
        header()
        east = ["ejo", "tky"]
        west = ["ckkoskc", "wjih"]
        dest_qemues = {:qemu_img => dest_dir + "/" + qemu_img, :qemu_kvm => dest_dir + "/" + qemu_kvm}
        # qemuファイルのmd5確認
        i = dest_qemues.values.length-1
        for i in 0..i do
          cmd = "md5sum #{dest_qemues.values[i]}"
          do_check(cmd, qemues_md5.values[i])
        end
        # spectre_meltdown.shファイルのmd5確認
        cmd = "md5sum #{dest_dir}/#{spectre_meltdown_script[:name]}"
        do_check(cmd, spectre_meltdown_script[:md5])
        # rhel69リポジトリファイルのmd5確認
        current_host do |host|
          if east.any? { |e| host.include? e }
            cmd = "md5sum #{dest_dir}/#{rhel69_repo_file_tama_oyama[:name]}"
            capture_check(cmd, rhel69_repo_file_tama_oyama[:md5], host)
          elsif west.any? { |w| host.include? w }
            cmd = "md5sum #{dest_dir}/#{rhel69_repo_file_imike_osaka[:name]}"
            capture_check(cmd, rhel69_repo_file_imike_osaka[:md5], host)
          else
            puts output = "#{host}: md5sum rhel69.repo => #{red('NG')}"
            custom_logger;info(host, output)
            puts output = "host name is not mutch the following"
            custom_logger;info(host, output)
            puts output = "************************"
            custom_logger;info(host, output)
            puts east,west
            custom_logger;info(host, east.join("\n"))
            custom_logger;info(host, west.join("\n"))
            puts output = "************************"
            custom_logger;info(host, output)
            set_error_flag()
          end
        end
        # VENOMパッチの解凍
        venomfiles = dest_dir + "/" + venom_tar_file[:name]
        cmd1 = "tar xzvf #{venomfiles} -C  #{dest_dir}"
        do_display(cmd1)
        # VENOMパッチ適応に必要なファイル群のmd5確認
        venom_dir = venom_tar_file[:name]
        venom_dir.slice!(".tar.gz")
        array = [venom_patch1, venom_patch2, venom_patch3, venom_patch4, venom_patch5, venom_patch6, venom_patch7, venom_patch8,
                 venom_patch9, venom_patch10, venom_patch11]
        i = array.length-1
        for i in 0..i do
          cmd2 = "md5sum  #{dest_dir}/#{venom_dir}/#{array[i][:name]}"
          expeted = array[i][:md5]
          do_check(cmd2, expeted)
        end
        # logrotate.dに配置するファイルのmd5確認
        cmd = "md5sum  #{dest_dir}/#{cloudstack_agent_logrotate[:name]}"
        expeted = cloudstack_agent_logrotate[:md5]
        do_check(cmd, expeted)
      end
    end

    desc 'cloudstack-agentの停止'
    task :"cloudstack_agent_stop", :roles=>[:targets] do
       unless skip.nil?
         header()
         #sudo "/etc/init.d/cloudstack-agent stop;true"
         cmd1 = "/etc/init.d/cloudstack-agent status;true"
         cmd2 = "/etc/init.d/cloudstack-agent stop;true"
         expected = "cloudstack-agent is stopped"
         do_daemon_start_stop(cmd1, expected, cmd2)
       end
    end

    desc 'cloudstack-agentのステータス確認(停止)'
    task :"cloudstack_agent_status_check_stopped", :roles=>[:targets] do
      unless skip.nil?
        header()
        cmd = "/etc/init.d/cloudstack-agent status;true"
        expected = "cloudstack-agent is stopped"
        do_check(cmd, expected)
      end
    end

    desc 'libvirtの停止'
    task :"libvirtd_stop", :roles=>[:targets] do
      unless skip.nil?
        header()
        #sudo "/etc/init.d/libvirtd stop"
        cmd1 = "/etc/init.d/libvirtd status;true"
        cmd2 = "/etc/init.d/libvirtd stop"
        expected="libvirtd is stopped"
        do_daemon_start_stop(cmd1, expected, cmd2)
      end
    end

    desc 'libvirtのステータス確認'
    task :"libvirtd_status_check_stopped", :roles=>[:targets] do
      unless skip.nil?
        header()
        cmd ="/etc/init.d/libvirtd status;true"
        expected="libvirtd is stopped"
        do_check(cmd, expected)
      end
    end

    desc 'cloudstack-agent,libvirtdのランレベルを全てoffにする'
    task :runlevel_off, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd1 = "chkconfig cloudstack-agent off"
        cmd2 = "chkconfig libvirtd off"
        do_display(cmd1)
        do_display(cmd2)
      end
    end

    desc 'cloudstack-agent,libvirtdのランレベル変更確認(off)'
    task :runlevel_off_check, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd = "chkconfig cloudstack-agent --list"
        do_check(cmd, cloudstack_agent_run_level_off)
        cmd = "chkconfig libvirtd --list"
        do_check(cmd, libvirtd_run_level_off)
      end
    end

    desc 'rhel69リポジトリファイルを/etc/yum.repos.d/に配置・MD5SUM 確認'
    task :put_rhel69_repo, :roles => [:targets] do
      unless skip.nil?
        header()
        east = ["ejo", "tky"]
        west = ["ckkoskc", "wjih"]
        current_host do |host|
          if east.any? { |e| host.include? e }
            src_path = "#{dest_dir}/#{rhel69_repo_file_tama_oyama[:name]}"
            dest_path = "/etc/yum.repos.d/#{rhel69_repo_file_tama_oyama[:rename]}"
            cmd1 = "cp #{src_path} #{dest_path}"
            cmd2 = "md5sum /etc/yum.repos.d/#{rhel69_repo_file_tama_oyama[:rename]}"
            expected = rhel69_repo_file_tama_oyama[:md5] 
            capture_display(cmd1, host)
            capture_check(cmd2, expected, host)
          elsif west.any? { |w| host.include? w }
            src_path = "#{dest_dir}/#{rhel69_repo_file_imike_osaka[:name]}"
            dest_path = "/etc/yum.repos.d/#{rhel69_repo_file_imike_osaka[:rename]}"
            cmd1 = "cp #{src_path} #{dest_path}"
            cmd2 = "md5sum /etc/yum.repos.d/#{rhel69_repo_file_imike_osaka[:rename]}"
            expected = rhel69_repo_file_imike_osaka[:md5] 
            capture_display(cmd1, host)
            capture_check(cmd2, expected, host)
          else
            puts output = "#{host}: cp rhel69.repo /etc/yum.repos.d => #{red('NG')}"
            custom_logger;info(host, output)
            puts output = "host name is not mutch the following"
            custom_logger;info(host, output)
            puts output = "************************"
            custom_logger;info(host, output)
            puts east,west
            custom_logger;info(host, east.join("\n"))
            custom_logger;info(host, west.join("\n"))
            puts output = "************************"
            custom_logger;info(host, output)
            set_error_flag()
          end
        end
      end
    end

    desc 'NOS様にご用意して頂いた試験環境に必要なタスク。物理ホストで実施する際は本タスクはコメントアウトすること。'
    task :tmp_task, :roles => [:targets] do
      unless skip.nil?
        header()
        puts "検証環境には下記コマンドが必須。by NOS上野様"
        puts "yum -y update --disablerepo=* --enablerepo=package-base,package-update"
        puts "※理由"
        puts "検証用のお渡ししている環境では事前に上記コマンドを実施いただきパッケージ情報を商用に合わせていただく必要がございます。"
        puts "実行します。。。"
        cmd = "yum -y update --disablerepo=* --enablerepo=package-base,package-update"
        do_display(cmd)
      end
    end

    desc 'VENOMパッケージの適応'
    task :do_venom_package, :roles => [:targets] do
      unless skip.nil?
        header()
        # Premiumのvenom対応
        v_qemu_img = venom_patch5[:name]
        v_qemu_kvm = venom_patch6[:name]
        v_qemu_img.slice!(".rpm")
        v_qemu_kvm.slice!(".rpm")
        venom_dir = venom_tar_file[:name]
        venom_dir.slice!(".tar.gz")
        cmd1 ="rpm -qa | grep #{v_qemu_img};true"
        cmd2 ="rpm -qa | grep #{v_qemu_kvm};true"
        cmd3 = "yum -y localinstall #{dest_dir}/#{venom_dir}/*"
        c_do do
          current_host do |host|
            options = {:hosts => host}
            ans1 = capture("LANG=C #{cmd1}", options)
            ans2 = capture("LANG=C #{cmd2}", options)
            if ans1.empty? || ans2.empty?
              do_display(cmd3)
            else
              puts output = ("#{host}: #{cmd1} #{green('OK')}")
              custom_logger;info(host, output)
              puts output = "Expected value: #{v_qemu_img}"
              custom_logger;info(host ,output)
              puts output = "Result value:   #{ans1}"
              custom_logger;info(host ,output)
              puts output = ("#{host}: #{cmd2} #{green('OK')}")
              custom_logger;info(host, output)
              puts output = "Expected value: #{v_qemu_kvm}"
              custom_logger;info(host ,output)
              puts output = "Result value:   #{ans2}"
              custom_logger;info(host ,output)
              puts output = "Skip Task => #{cmd3}" 
              custom_logger;info(host, output)
            end
          end
        end
      end
    end

    desc 'yumパッケージ「matahari」の削除'
    task :del_matahari_package, :roles => [:targets] do
      unless skip.nil?
        header()
        # matahariのパッケージが入っている状態だと、rhel6.9へのアップグレードが失敗するため、削除。
        cmd = "yum -y remove matahari-*"
        do_display(cmd)
      end
    end

    desc 'yumパッケージ「matahari」の削除後確認'
    task :del_matahari_package_check, :roles => [:targets] do
      unless skip.nil?
        header()
        c_do do
          current_host do |host|
            cmd = "rpm -qa | grep matahari;true"
            options = {:hosts => host}
            ans = capture(cmd, options)
            if ans.empty?
              puts output = ("#{host}: #{cmd} => #{green('OK')}")
              custom_logger;info(host ,output)
              puts output = "Expected value: "
              custom_logger;info(host ,output)
              puts output = "Result value:   #{ans}"
              custom_logger;info(host ,output)
              puts output = "================================================================="
            else
              puts output = ("#{host}: #{cmd} => #{red('NG')}")
              custom_logger;info(host,output)
              puts output = "Expected value: "
              custom_logger;info(host ,output)
              puts output = "Result value:   #{ans}"
              custom_logger;info(host ,output)
              puts output = "================================================================="
              set_error_flag()
            end
          end
        end
      end
    end

    desc 'RHEL6.9へのアップグレード'
    task :rhel69_upgrade, :roles => [:targets] do
      unless skip.nil?
        header()
        # OSアップグレード
        cmd = "yum -y update --disablerepo=* #{yum_repos}"
        do_display(cmd)
      end
    end

    desc 'OS停止'
    task :os_stop, :roles => [:targets] do
      unless skip.nil?
        header()
        are_you_ok? "OSの停止をしてよろしいでしょうか? [y|n]: ";
        unless skip.nil?
          cmd = "shutdown -h now"
          do_display(cmd)
        end
      end
    end

    desc 'OS起動待ち'
    task :wait_ssh, :roles => [:targets] do
      unless skip.nil?
        header()
        catch :server_ok do
          begin
            timeout( 900 ) {
              begin
                cmd = "echo ssh connected"
                do_display(cmd)
                throw :server_ok
              rescue ConnectionError
                puts output = "try to connect..."
                current_host do |host|
                  custom_logger;info(host ,output)
                end
                sleep 15
                retry
              end
            }
          rescue Timeout::Error
            puts "",output = "Timed out"
            current_host do |host|
              custom_logger;error(host ,output)
            end
            set_error_flag()
          end
        end
      end
    end

    desc 'OSアップグレード確認'
    task :os_check, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd1 = "cat /etc/redhat-release"
        cmd2 = "uname -r"
        do_check(cmd1, redhat_release69)
        do_check(cmd2, uname69)
      end
    end

    desc '脆弱性パッチ適用確認'
    task :meltdown_patch_check, :roles => [:targets] do
      unless skip.nil?
        header()
        script_file = dest_dir + "/" + spectre_meltdown_script[:name]
        cmd1 = "chmod 755 #{script_file}"
        cmd2 = "#{script_file};true" 
        do_display(cmd1)
        do_display(cmd2)
      end
    end

    desc 'citrix提供qemu-imgをcloud-qemu-imgにリネーム'
    task :qemu_rename, :roles => [:targets] do
      unless skip.nil?
        header()
        img = qemues_rename[:qemu_img]
        cloud_img = qemues_rename[:cloud_qemu_img]
        cmd1 = "find /usr/bin -type f | grep #{cloud_img};true"
        cmd2 = "mv #{img} #{cloud_img}"
        c_do do
          current_host do |host|
            options = {:hosts => host}
            ans = capture("LANG=C #{cmd1}", options)
            if ans.empty?
              capture_display(cmd2, host)
            else
              puts output = ("#{host}: #{cmd1} #{green('OK')}")
              custom_logger;info(host, output)
              puts ans
              custom_logger;info(host, ans)
              puts output = "Skip Task => #{cmd2}" 
              custom_logger;info(host, output)
            end
          end
        end
      end
    end

    desc 'cloud-qemu-imgにリネームされたか確認'
    task :qemu_rename_check, :roles => [:targets] do
      unless skip.nil?
        header()
        cloud_img = qemues_rename[:cloud_qemu_img]
        cmd ="ls #{cloud_img}"
        do_check(cmd,cloud_img)
      end
    end

    desc 'RedHat純正qemu-img,qemu-kvmをインストール'
    task :qemu_install, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd = "yum -y downgrade #{dest_dir}/#{qemu_img} #{dest_dir}/#{qemu_kvm}"
        do_display(cmd)
      end
    end

    desc 'RedHat純正qemu-img,qemu-kvmパッケージ確認'
    task :qemu_package_check, :roles => [:targets] do
      unless skip.nil?
        header()
        qemu_img.slice!(".rpm")
        qemu_kvm.slice!(".rpm")
        cmd1 ="rpm -qa | grep #{qemu_img}"
        cmd2 ="rpm -qa | grep #{qemu_kvm}"
        do_check(cmd1, qemu_img)
        do_check(cmd2, qemu_kvm)
      end
    end

    desc 'ログディレクトリー修正作業'
    task :modify_log_dir, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd1 ="cd /var/log/cloud; mv agent agent_`date +'%Y%m%d%H%M'`"
        cmd2 ="cd /var/log/cloudstack; mv agent /var/log/cloud"
        cmd3 ="cd /var/log/cloudstack ; ln -s /var/log/cloud/agent ./"
        cmd4 ="mkdir -p /home/ckkcl/hookscript;mv /etc/logrotate.d/#{cloudstack_agent_logrotate[:name]} /home/ckkcl/hookscript/#{cloudstack_agent_logrotate[:name]}_`date +'%Y%m%d%H%M'`"
        cmd5 ="cp #{dest_dir}/#{cloudstack_agent_logrotate[:name]} /etc/logrotate.d"
        cmd6 ="find /var/log/cloudstack -type l;true"
        c_do do
          current_host do |host|
            options = {:hosts => host}
            ans = capture("LANG=C #{cmd6}", options)
            unless agent_symbolic_link == ans.chomp
              capture_display(cmd1, host)
              capture_display(cmd2, host)
              capture_display(cmd3, host)
              capture_display(cmd4, host)
              capture_display(cmd5, host)
              ans = capture("LANG=C #{cmd6}", options)
              if ans.empty?
                puts output = ("#{host}: #{cmd6} => #{red('NG')}")
                custom_logger;info(host,output)
                puts output = "Expected value: #{agent_symbolic_link}"
                custom_logger;info(host ,output)
                puts output = "Result value:   #{ans}"
                custom_logger;info(host ,output)
                puts output = "================================================================="
                set_error_flag()
              else
                puts output = ("#{host}: #{cmd6} #{green('OK')}")
                custom_logger;info(host,output)
                puts output = "Expected value: #{agent_symbolic_link}"
                custom_logger;info(host ,output)
                puts output = "Result value:   #{ans}"
                custom_logger;info(host ,output)
                puts output = "================================================================="
              end
            else
              puts output = ("#{host}: #{cmd6} #{green('OK')}")
              custom_logger;info(host, output)
              puts ans
              custom_logger;info(host, ans)
              puts output = "Skip Task\n=> #{cmd1}\n=> #{cmd2}\n=> #{cmd3}\n=> #{cmd4}\n=> #{cmd5}" 
              custom_logger;info(host, output)
            end
          end
        end
      end
    end

    desc 'libvirtを起動します'
    task :"libvirtd_start", :roles=>[:targets] do
      unless skip.nil?
        header()
        #sudo "/etc/init.d/libvirtd start"
        cmd1 = "/etc/init.d/libvirtd status;true"
        cmd2 = "/etc/init.d/libvirtd start"
        expected="libvirtd (pid  ) is running..."
        do_daemon_start_stop(cmd1, expected, cmd2)
      end
    end

    desc 'libvirtのステータス確認'
    task :"libvirtd_status_check_running", :roles=>[:targets] do
      unless skip.nil?
        header()
        cmd = "/etc/init.d/libvirtd status;true"
        expected="libvirtd (pid  ) is running..."
        do_check(cmd, expected)
      end
    end

    desc 'cloudstack-agentの起動'
    task :"cloudstack_agent_start", :roles=>[:targets] do
      unless skip.nil?
        header()
        #sudo "/etc/init.d/cloudstack-agent start"
        cmd1 = "/etc/init.d/cloudstack-agent status;true"
        cmd2 = "/etc/init.d/cloudstack-agent start;true"
        expected = "cloudstack-agent (pid  ) is running..."
        do_daemon_start_stop(cmd1, expected, cmd2)
      end
    end

    desc 'cloudstack-agentのステータス確認(起動)'
    task :"cloudstack_agent_status_check_running", :roles=>[:targets] do
      unless skip.nil?
        header()
        cmd = "/etc/init.d/cloudstack-agent status;true"
        expected = "cloudstack-agent (pid  ) is running..."
        do_check(cmd, expected)
      end
    end

    desc 'ログディレクトリー修正作業後確認'
    task :"modify_log_dir_check", :roles=>[:targets] do
      unless skip.nil?
        header()
        cmd1 ="find /var/log/cloud/agent/ -type f | grep -w '/var/log/cloud/agent/agent.log' | grep -v 'gz';true"
        cmd2 ="diff  /var/log/cloud/agent/agent.log  /var/log/cloudstack/agent/agent.log;true"
        cmd3 ="tail -5 /var/log/cloud/agent/agent.log;true"
        c_do do
          current_host do |host|
            options = {:hosts => host}
            ans1 = capture("LANG=C #{cmd1}", options)
            puts output = ("#{host}: #{cmd1}")
            custom_logger;info(host, output)
            puts ans1
            custom_logger;info(host, ans1)
            if ans1.empty?
              puts output = "「/var/log/cloud/agent/agent.log」 does not exist."
              custom_logger;info(host, output)
              puts output = "Skip Task\n=> #{cmd2}\n=> #{cmd3}" 
              custom_logger;info(host, output)
            else
              ans2 = capture("LANG=C #{cmd2}", options)
              ans3 = capture("LANG=C #{cmd3}", options)
              if ans2.empty?
                puts output = ("#{host}: #{cmd2} #{green('OK')}")
                custom_logger;info(host, output)
                puts output = "Expected value: "
                custom_logger;info(host ,output)
                puts output = "Result value:   #{ans2}"
                custom_logger;info(host ,output)
              else
                puts output = ("#{host}: #{cmd2} => #{red('NG')}")
                custom_logger;info(host,output)
                puts output = "Expected value: "
                custom_logger;info(host ,output)
                puts output = "Result value:   #{ans2}"
                custom_logger;info(host ,output)
              end
              puts output = ("#{host}: #{cmd3}")
              custom_logger;info(host, output)
              puts ans3
              custom_logger;info(host ,ans3)
            end
          end
        end
      end
    end

    desc 'cloudstack-agent,libvirtdのランレベルをonにする'
    task :runlevel_on, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd1 = "chkconfig --level 345 cloudstack-agent on"
        cmd2 = "chkconfig --level 345 libvirtd on"
        do_display(cmd1)
        do_display(cmd2)
      end
    end

    desc 'cloudstack-agent,libvirtdのランレベル変更確認(on)'
    task :runlevel_on_check, :roles => [:targets] do
      unless skip.nil?
        header()
        cmd = "chkconfig cloudstack-agent --list"
        do_check(cmd, cloudstack_agent_run_level_on)
        cmd = "chkconfig libvirtd --list"
        do_check(cmd, libvirtd_run_level_on)
      end
    end
  end

  def do_upload_file(src_path, dest_path)
    return if skip.nil?
    puts output = "uploading #{src_path} to #{dest_path}"
    current_host do |host|
      custom_logger;info(host ,output)
    end
    upload(src_path, dest_path, :via => :scp)
  end

  def copy_file(src_path, dest_path)
    puts "copying #{src_path} to #{dest_path}"
    c_sudo "cp #{src_path} #{dest_path}"
  end
end
