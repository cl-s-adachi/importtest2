# coding: utf-8

##------------------------------------------------------------------------------
## 対象ホストの指定
## 指定方法：./config/capistrano_conf.rbにホスト名を記載。
##           例：role :targets, *%w(ホスト名)
##             role :targets, *%w(tckktky4-pbhpvtestP)
##            「cap hostname」で取得したホスト名が一致するか確認してください。
##-------------------------------------------------------------------------------

## Commonly used tasks

desc 'ホスト名を表示します'
task :hostname, :roles=>[:targets] do
  run "echo $HOSTNAME"
end

desc 'ホストの時刻を表示します'
task :date, :roles=>[:targets] do
  run "date"
end

desc 'ホストの情報を確認します'
task :"hostinfo", :roles=>[:targets] do
  run "date; uname -n; id"
end

## Commonly used methods

def header(*str)
  if str[0].eql? nil
    puts "#{purple('START TASK')}: #{current_task.desc} Times of Day #{today}"
    output = "#{current_task.desc} Times of Day #{today}"
    custom_logger;info("#{purple('START TASK')}",output)
  else
    return "#{str[0]}\n" + "#{blue('END')}\n\n\n"
  end
end

def today()
  return Time.now().strftime("%Y/%m/%d %H:%M:%S")
end

def green(str)
  return "\e[32m" + str + "\e[0m"
end

def red(str)
  return "\e[31m" + str + "\e[0m"
end

def purple(str)
  return "\033[95m" + "\033[4m" + str + "\e[0m"
end

def blue(str)
  return "\033[96m" + "\033[4m" + str + "\e[0m"
end

def start_commands2(description=:task_namespace, log_level=Logger::IMPORTANT, &block)
  # ログレベルの変更
  log_level_scope log_level do
    if description == :task_namespace then
       description = current_task.fully_qualified_name
    end
    puts "Start  - #{description}" unless description.empty?
    block.call
    puts "Finish - #{description}" unless description.empty?

    # 実行サーバのOK/NG状況表示
    puts "================================================================="
    array_box = Array.new()
    result = :'Tasks completed successfully. = '
    check_error = current_task.namespace.roles[:targets]
    check_error.each do |role|
      if role.options[:error_task]
        array_box.push(role.host, role.options[:error_task])
        result = :'Tasks could not be completed normally. = '
      else
        array_box.push(role.host)
      end
    end
    array_box = array_box.uniq
    puts "#{result} #{array_box[0]}"
    if array_box[1]
      puts "#{red('NG_TASK')}: #{array_box[1]}"
      puts "================================================================="
      matchput(current_task.name, array_box[1].to_s)
    end
  end
end

def skip()
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    if srv.options[:has_error].nil? && srv.options[:error_task].nil? then
      next
    else
      return
    end
  end
end

def ps_ok(description)
  result = :OK
  check_error = current_task.namespace.roles[:targets]
  check_error.each do |role|
    options = role.options
    if options.has_key?(:has_error) && options.has_key?(:error_task)
      if options[:error_task].to_s.eql?(description.split("\n")[0].to_s)
        result = :NG
      else
        result = :SKIP
        return
      end
    end
  end
  puts "#{result} - #{description}"
end

def do_check(cmd, value)
  c_do do
    sudo("LANG=C #{cmd}") do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        if cmd.include?("md5sum")
          val = data.split()[0]
        else
          if value.include?("running")
            val = data.chomp!
            val = val.delete("0-9")
          else
            val = data.chomp!
          end
        end
        if val == value
          puts output = ("#{host_name}: #{cmd} => #{green('OK')}")
          custom_logger;info(host_name ,output)
          if cmd.include?("status")
            puts data
            custom_logger;info(host_name ,data)
          else
            puts output = "Expected value: #{value}"
            custom_logger;info(host_name ,output)
            puts output = "Result value:   #{val}"
            custom_logger;info(host_name ,output)
            puts output = "================================================================="
            custom_logger;info(host_name ,output)
          end
        else
          puts output = ("#{host_name}: #{cmd} => #{red('NG')}")
          custom_logger;info(host_name ,output)
          if cmd.include?("status")
            puts data
            custom_logger;info(host_name ,data)
          else
            puts output = "Expected value: #{value}"
            custom_logger;info(host_name ,output)
            puts output = "Result value:   #{val}"
            custom_logger;info(host_name ,output)
            puts output = "================================================================="
            custom_logger;info(host_name ,output)
          end
          set_error_flag()
        end
      elsif stream == :err
        puts "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
  end
end

def do_display(cmd)
  stream_err = Array.new()
  stream_err_display_tasks = ["tmp_task", "rhel69_upgrade", "del_matahari_package", "qemu_install", "do_venom_package"]
  current_host do |host|
    puts "#{host}: #{cmd}"
    custom_logger;info(host ,cmd)
  end
  c_do do
    sudo("LANG=C #{cmd}") do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        puts data
        custom_logger;info(host_name ,data)
      elsif stream == :err
        puts output = "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
        if stream_err_display_tasks.any? { |t| current_task.name.to_s.eql? t }
          output.strip!
          stream_err << output.split("\n")
        end
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
    unless stream_err.empty?
      puts "\n\n\n"
      puts "#{cmd} => There was an error."
      puts "Please check the results below."
      puts "************************"
      puts stream_err
      puts "************************"
      sleep 5
    end
  end
end

def do_daemon_start_stop(cmd1, expected, cmd2)
  c_do do
    sudo("LANG=C #{cmd1}") do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        if expected.include?("running")
          val = data.chomp!
          val = val.delete("0-9")
        else
          val = data.chomp!
        end
        if val == expected
          puts output = ("#{host_name}: #{cmd1} #{green('OK')}")
          custom_logger;info(host_name ,output)
          puts data
          custom_logger;info(host_name ,data)
          puts output = "Skip Task => #{cmd2}" 
          custom_logger;info(host_name ,output)
        else
          puts output = ("#{host_name}: #{cmd1} => #{data}") 
          custom_logger;info(host_name ,output)
          puts output = "Do Task => #{cmd2}"
          custom_logger;info(host_name ,output)
          capture_display(cmd2, host_name)
        end
      elsif stream == :err
        puts "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
  end
end

def capture_display(cmd, host)
  c_do do
    options = {:hosts => host}
    puts output = "#{host}: #{cmd}"
    custom_logger;info(host ,output)
    ans = capture("LANG=C #{cmd}", options)
    puts ans
    custom_logger;info(host, ans)
  end
end

def capture_check(cmd, expected, host)
  c_do do
    options = {:hosts => host}
    ans = capture("LANG=C #{cmd}", options)
    if cmd.include?("md5sum")
      ans = ans.split()[0]
    else
      if expected.include?("running")
        ans = ans.chomp!
        ans = ans.delete("0-9")
      else
        ans = ans.chomp!
      end
    end
    if ans == expected
      puts output = ("#{host}: #{cmd} => #{green('OK')}")
      custom_logger;info(host, output)
      if cmd.include?("status")
        puts ans
        custom_logger;info(host, ans)
      else
        puts output = "Expected value: #{expected}"
        custom_logger;info(host, output)
        puts output = "Result value:   #{ans}"
        custom_logger;info(host, output)
        puts output = "================================================================="
        custom_logger;info(host, output)
      end
    else
      puts output = ("#{host}: #{cmd} => #{red('NG')}")
      custom_logger;info(host, output)
      if cmd.include?("status")
        puts ans
        custom_logger;info(host ,ans)
      else
        puts output = "Expected value: #{expected}"
        custom_logger;info(host, output)
        puts output = "Result value:   #{ans}"
        custom_logger;info(host, output)
        puts output = "================================================================="
        custom_logger;info(host, output)
      end
      set_error_flag()
    end
  end
end

def are_you_ok?(question, description=[])
  check_error = current_task.namespace.roles[:targets]
  check_error.each do |role|
    options = role.options
    return if options.has_key?(:has_error) && options.has_key?(:error_task)
  end
  puts ""
  while true
    res = Capistrano::CLI.ui.ask question
    case res
    when /^[yY]\z/
      puts "You choose yes!"
      puts "Continue processing..."
      puts "************************"
      return true
    when /^[nN]\z/
      puts "You choose no!"
      puts "Skip the rest of the processing..."
      puts "************************"
      if description.empty?
        set_error_flag()
      else
        set_error_flag(description)
      end
      return false
    end
  end
end

def set_error_flag(description=[])
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    if srv.host
      #NGのあったサーバにフラグとエラータスク名を設定する
      srv.options[:has_error] = true
      if description.empty?
        srv.options[:error_task] = current_task.name
      else
        srv.options[:error_task] = description
      end
      return false
    end
  end
end

def current_host()
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    yield(srv.host)
  end
end

def custom_logger()
  if log_level.eql?(0)
    def info(*args);end
    alias :error :info
    return
  end
  FileUtils.mkdir_p(log_dir) unless FileTest.exist?(log_dir)
  @input = File.open(log_file,'a')
  @time = Time.now().strftime("%Y:%m:%d %H:%M:%S")
  def info(current_host, res)
    if res.empty?
      @input.puts " ** [out :: #{current_host}] #{res}"
    else
      res.strip!
      res = res.split("\n")
      len = res.length-1
      for i in 0..len do
        @input.puts " ** [out :: #{current_host}] #{res[i]}"
      end
    end
    @input.close
  end

  def error(current_host, res)
    if res.empty?
      @input.puts " ** [err :: #{current_host}] #{res}"
    else
      res.strip!
      res = res.split("\n")
      len = res.length-1
      for i in 0..len do
        @input.puts " ** [err :: #{current_host}] #{res[i]}"
      end
    end
    @input.close
  end
end

def matchput(all_task, ng_task)
  if all_task.eql?(:all_1)
    filename = "./config/KvmUpgradeTaskListFirstHalf"
  else
    filename = "./config/KvmUpgradeTaskListLatterHalf"
  end
  return unless File.exist?(filename)
  word = /#{ng_task}$/
  puts "下記タスクをご覧ください。"
  puts "赤色のタスクでNGとなり以降のタスクは未実行です。"
  puts "原因を確認し、下記タスクを個別に実行してください。"
  puts "**************************************************************************************"
  File.open(filename, "r") do |file|
    file.each_line do |line|
      unless line.match(/^#/)
        if line =~ word .. line =~ /_$/
          if line =~ word
            print "\033[91m" + "\033[1m" + line + "\e[0m"
          else
            print line
          end
        end
      end
    end
  end
  puts "**************************************************************************************"
end

alias :footer :header
