filename = ARGV[0]
word = /#{ARGV[1]}/
count = 0

def confirm()
  while true
    puts "[CONFIRM] 処理を開始しますか?"
    puts " 実行オプションを選択してください。"
    puts " 1) 処理を開始する。"
    puts " 2) 処理をスキップ。"
    puts " q) 処理を終了する。"
    print "上記オプションから希望する処理の番号を入力してください [1-2 or q]: "
    response = STDIN.gets.chomp
    case response
    when /^[1]\z/
      puts "\n1を選択したので処理を開始します。","\n"
      break
    when /^[2]\z/
      @skip = true
      puts "\n2を選択したので処理をスキップしました。"
      break
    when /^[qQ]\z/
      puts "\nqを選択したので処理を終了します。","\n"
      exit
    end
  end	
end

unless File.exist?(filename)
  puts "#{filename}が存在しません。"
  exit
end

File.open(filename, "r") do |file|
  file.each_line do |line|
    unless line.match(/^#/)
      if line =~ word .. line =~ /_$/
        description = line
        task = line.match(/=>*(.*)/).to_a.drop(1)
        @skip = false
        unless task.empty?
          tmp = task.join("").split(":").reverse
          s = Regexp.new(tmp[0])
          if s == word || count != 0
            cmd = task.to_s.strip
            puts "\n",description
            confirm()
            system(cmd) unless @skip
            count += 1
          else
            puts "正しいタスク名を引数に指定してください。"
            exit
          end
        end
      end
    end
  end
  if count == 0
    puts "正しいタスク名を引数に指定してください。"
    exit
  end
end
