# coding: utf-8

namespace(:local_test) do

  set :local_target, {:roles=>[:local]}
  set :local_cmd_filter, {:roles=>[:local], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}

  task :"score", local_target do

    columns = ['httpd', 'bind-utils']
    caputer_servers(columns) do |current_server, score|
      options = {:hosts => current_server.host}
      score['httpd'] = capture_match(
         "rpm -qa |grep httpd; true", /httpd\-3.*/, options)
      score['bind-utils'] = capture_match(
         "rpm -qa |grep bind-utils; true", /bind\-utils*/, options)

    end
  end

   task :"all", local_target do
      start_commands(:task_namespace ,Logger::IMPORTANT) do
#      start_commands(:task_namespace ,Logger::INFO) do
        local_test.check_proc_ok_1();       log_ok :check_proc_ok_1
        local_test.check_proc_ok_2();       log_ok :check_proc_ok_2
        local_test.check_proc_ok_3();       log_ok :check_proc_ok_3
        local_test.check_proc_ok_1();       log_ok :check_proc_ok_1
      end
   end

   task :check_proc_ok_1, local_cmd_filter do
      c_su "/etc/init.d/httpd status; true"
   end
   task :check_proc_ok_2, local_cmd_filter do
      c_su "/etc/init.d/httpd stop"
   end
   task :check_proc_ok_3, local_cmd_filter do
      c_su "/etc/init.d/httpd start"
   end
   task :check_proc_ng_all, local_cmd_filter do
      c_sudo "rpm -qa |grep aadadsaded"
   end
   task :check_proc_ok_and_ng, local_cmd_filter do
      c_sudo "rpm -qa |grep bind-utils"
   end

end
