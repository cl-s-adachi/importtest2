# coding: utf-8

namespace(:cloud_agent) do
  ## Valiables
  # show line logs count
  set :line_number, 10

  desc 'Agentを再起動します'
  task :"restart", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent restart"
  end

  desc 'Agentを停止します'
  task :"stop", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent stop"
  end

  desc 'Agentを起動します'
  task :"start", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent start"
  end

  desc 'Agentのバージョンを表示します'
  task :"version", :roles=>[:targets] do
    sudo "rpm -qa | grep cloud-agent-[0-9]; exit 0"
  end

  desc 'Agentのステータスを確認します'
  task :"status", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent status ; exit 0"
  end

  desc 'Agentのログを表示します'
  task :"cloud-agent-log", :roles=>[:targets] do
     sudo "tail -n #{line_number} /var/log/cloud/agent/agent.log"
  end
end
