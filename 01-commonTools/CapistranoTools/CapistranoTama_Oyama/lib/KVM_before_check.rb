# coding: utf-8
## Tasks

# CloudStack3:   /etc/cloud/agent
#                /etc/init.d/cloud-agent
# CloudStack4.9: /etc/cloudstack/agent
#                /etc/init.d/cloudstack-agent

set :upgrade_packages_base_path, '/root/acs4.9_upgrade_packages'
set :upgrade_backup_path, '/root/acs'

namespace(:kvm_check_acs37) do

   desc 'KVM-評価'
   task :"all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         kvm_check_acs37.agent_ps();                              log_ok "agent_ps"
         kvm_check_acs37.jsvc_no_ps();                            log_ok "jsvc_no_ps"
         kvm_check_acs37.libvirtd_ps();                           log_ok "libvirtd_ps"
         kvm_check_acs37.java_ver();                              log_ok "java_ver"
         kvm_check_acs37.openjdk_no_inst();                       log_ok "openjdk_no_inst"
         kvm_check_acs37.kvmpkg_no_inst();                        log_ok "kvmpkg_no_inst"
         kvm_check_acs37.cloud_packages();                        log_ok "cloud_packages"
         kvm_check_acs37.BrName();                                log_ok "BrName"
         kvm_check_acs37.snooping_check();                        log_ok "snooping_check"
      end
   end

   desc 'Agent ps status'
   task :"agent_ps", acs49_cmd_filter do
      c_sudo "/etc/init.d/cloud-agent status"
   end

   desc 'jsvc ps'
   task :"jsvc_no_ps", acs49_cmd_filter do
      c_sudo "ps -ef | grep jsvc | grep -v grep; true"
   end

   desc 'libvirtd ps'
   task :"libvirtd_ps", acs49_cmd_filter do
      c_sudo "/etc/init.d/libvirtd status; true"
   end

   desc 'Java version'
   task :"java_ver", acs49_cmd_filter do
      c_sudo "java -version; true"
   end

   desc 'OpenJDK check'
   task :"openjdk_no_inst", acs49_cmd_filter do
      c_sudo "rpm -qa python-argparse ipset | grep -e 'python-argparse' -e 'ipset'; true"
   end

   desc 'KVM package check'
   task :"kvmpkg_no_inst", acs49_cmd_filter do
      c_sudo "rpm -qa jakarta-commons-daemon jakarta-commons-daemon-jsvc bridge-utils ebtables ethtool libvirt libvirt-python | awk -v ORS=\" \" '{print}' | grep 'bridge-utils' | grep 'ebtables' | grep 'ethtool' | egrep \"libvirt-[0-9]\" | egrep \"libvirt-python-[0-9]\" | egrep \"jakarta-commons-daemon-[0-9]\" | egrep \"jakarta-commons-daemon-jsvc-[0-9]\"; true"
   end

   desc 'Cloud packages check'
   task :"cloud_packages", acs49_cmd_filter do
      c_sudo "rpm -qa | grep 'cloud' | awk -v ORS=\" \" '{print}' | grep 'cloud-deps' | grep 'cloud-agent-scripts' | grep 'cloud-core' | grep 'cloud-python' | grep 'cloud-agent-libs' | egrep \"cloud-agent-[0-9]\" | grep 'cloud-daemonize' | grep 'cloud-utils'; true"
   end

   desc 'Bridge name check'
   task :"BrName", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/br_name_check.sh", "/root", :via => :scp
      c_sudo "/root/br_name_check.sh"
      c_sudo "rm -f /root/br_name_check.sh"
   end

   desc 'Check the snooping setting'
   task :"snooping_check", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_check.sh", "/root", :via => :scp
      c_sudo "/root/snooping_check.sh"
      c_sudo "rm -f /root/snooping_check.sh"
   end

   desc 'KVM-評価'
   task :"score", acs49_target do
      columns = ['agent_ps','jsvc_no_ps','libvirtd_ps','java_ver','openjdk_no_inst','kvmpkg_no_inst','cloud_packages','BrName','snooping']
      # ホストごとに処理
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         # agent process
         score['agent_ps'] = capture_match(
            "/etc/init.d/cloud-agent status | egrep 'running|実行中'; true", true,
            options)

         # jsvc process
         score['jsvc_no_ps'] = capture_unmatch(
            "ps -ef | grep jsvc | grep -v grep; true", true,
            options)

         # libvirtd process
         score['libvirtd_ps'] = capture_match(
            "/etc/init.d/libvirtd status | egrep 'running|実行中'; true", true,
            options)

         # java version
         score['java_ver'] = capture_match(
            "java -version",
            /#{Regexp.escape('java version "1.6')}/,
            options)

         # rpm check
         score['openjdk_no_inst'] = capture_unmatch(
            "rpm -qa python-argparse ipset | grep -e 'python-argparse' -e 'ipset'; true", true,
            options)

         # package check
         score['kvmpkg_no_inst'] = capture_unmatch(
            "rpm -qa jakarta-commons-daemon jakarta-commons-daemon-jsvc bridge-utils ebtables ethtool libvirt libvirt-python | awk -v ORS=\" \" '{print}' | grep 'bridge-utils' | grep 'ebtables' | grep 'ethtool' | egrep \"libvirt-[0-9]\" | egrep \"libvirt-python-[0-9]\" | egrep \"jakarta-commons-daemon-[0-9]\" | egrep \"jakarta-commons-daemon-jsvc-[0-9]\"; true", true,
            options)

         # cloud package check
         score['cloud_packages'] = capture_match(
            "rpm -qa | grep 'cloud' | awk -v ORS=\" \" '{print}' | grep 'cloud-deps' | grep 'cloud-agent-scripts' | grep 'cloud-core' | grep 'cloud-python' | grep 'cloud-agent-libs' | egrep \"cloud-agent-[0-9]\" | grep 'cloud-daemonize' | grep 'cloud-utils'; true", true,
            options)

         # Bridge name check
         top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/br_name_check.sh", "/root", :via => :scp
         score['BrName'] = capture_match(
            "sudo /root/br_name_check.sh | grep OK; true", true, options)
            c_sudo "rm -f /root/br_name_check.sh"

         # multicast_snooping の値をチェックする
         top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_check.sh", "/root", :via => :scp
         score['snooping'] = capture_unmatch(
            "sudo /root/snooping_check.sh | grep NG; true", true, options)
            c_sudo "rm -f /root/snooping_check.sh"
      end
   end
end
