# coding: utf-8

set :pre_jdk_package, 'java-1.8.0-openjdk'
set :pre_jdk_path, '/usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java'
set :pre_upgrade_packages_base_path, '/root/acs4.9_upgrade_packages'

namespace(:openjdk18_upgrade_acs49) do
   ## Valiables
   set :pre_upload_base, './upload/'
   set :pre_upload_zip, 'acs49_upgrade.zip'

   desc '事前準備-アップグレード'
   task :"all", acs49_target do
      start_commands do
         openjdk18_upgrade_acs49.upload_file();       log_ok "upload_file"
         openjdk18_upgrade_acs49.ca_certificates();   log_ok "ca_certificates"
         openjdk18_upgrade_acs49.java8_jdk();         log_ok "java8_jdk"
         openjdk18_upgrade_acs49.alternatives_java(); log_ok "alternatives_java"
         openjdk18_upgrade_acs49.python_packages();   log_ok "python_packages"
      end
   end

   desc '必要ファイルのアップロード'
   task :"upload_file", acs49_cmd_filter do
      local = File.expand_path(Pathname.new(pre_upload_base).join(pre_upload_zip))
      remote = Pathname.new(user_home).join(pre_upload_zip)
      top.upload(local, user_home, :via=>:scp)
      c_sudo "unzip -o #{remote} && rm -f #{remote}"
      c_sudo "cp -f #{pre_upgrade_packages_base_path}/etc/yum.repos.d/acs.repo /etc/yum.repos.d/acs.repo"
   end

   desc 'CA証明書の追加'
   task :"ca_certificates", acs49_cmd_filter do
      c_sudo "yum -y #{yum_opts} #{yum_repos} update ca-certificates"
   end

   desc 'java8 JDKのインストール'
   task :"java8_jdk", acs49_cmd_filter do
      c_sudo "yum -y #{yum_opts} #{yum_repos} install #{pre_jdk_package}"
   end

   desc 'java8 JDKのAltenative登録'
   task :"alternatives_java", acs49_cmd_filter do
      c_sudo "alternatives --install /usr/bin/java java #{pre_jdk_path} 1600"
   end

   desc 'CloudStack Agentで必要なPythonパッケージのインストール'
   task :"python_packages", acs49_cmd_filter do
      c_sudo "yum -y #{yum_opts} #{yum_repos} install python-argparse"
      c_sudo "yum -y #{yum_opts} #{yum_repos} install ipset"
   end
end

namespace(:openjdk18_check_acs49) do

   desc '事前準備-評価'
   task :"score", acs49_target do

      columns = ['file', 'CA', 'JDK', 'pyarg', 'ipset']
      # ホストごとに処理
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         # 必要ファイル
         score['file'] = capture_unmatch(
            "ls -la #{pre_upgrade_packages_base_path}/etc/init.d/cloudstack-agent; true",
            /No such file or directory|そのようなファイルやディレクトリはありません/,
            options)

         # CA証明書
         score['CA'] = capture_match(
            "rpm -qa |grep ca-certificates; true",
            "ca-certificates-2015.2.6-65.0.1.el6_7.noarch",
            options)

         # java8 JDK
         jdk_disp = capture_match(
            "alternatives --display java",
            /#{Regexp.escape('link currently points to /usr/lib/jvm/jre-1.6.0-openjdk.x86_64/bin/java')}/,
            options)
         jdk_config = capture_match(
            "echo | alternatives --config java",
            /#{Regexp.escape('/usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java')}/,
            options)
         score['JDK'] = jdk_disp && jdk_config

         # Pythonパッケージ(python-argparse)
         score['pyarg'] = capture_match(
            "rpm -qa |grep python-argparse; true",
            "python-argparse-1.2.1-2.1.el6.noarch",
            options)

         # Pythonパッケージ(ipset)
         score['ipset'] = capture_match(
            "rpm -qa |grep ipset; true",
            "ipset-6.11-4.el6.x86_64",
            options)
      end
   end

   desc '事前準備-状態確認'
   task :"all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         openjdk18_check_acs49.upload_file();      log_ok "upload_file"
         openjdk18_check_acs49.ca_certificates();  log_ok "ca_certificates"
         openjdk18_check_acs49.java_version();     log_ok "java_version"
         openjdk18_check_acs49.java8_jdk();        log_ok "java8_jdk"
         openjdk18_check_acs49.python_packages();  log_ok "python_packages"
      end
   end

   desc '必要ファイルの確認'
   task :"upload_file", acs49_cmd_filter do
      c_sudo "ls -Rl #{pre_upgrade_packages_base_path}; true"
   end

   desc 'CA証明書の確認'
   task :"ca_certificates", acs49_cmd_filter do
      c_sudo "rpm -qa |grep ca-certificates; true"
   end

   desc 'java8 JDKの状態確認'
   task :"java8_jdk", acs49_cmd_filter do
      c_sudo "alternatives --display java"
      # echoでEnter入力待ちを回避する
      c_sudo "echo | alternatives --config java"
   end

   desc 'java バージョンの確認'
   task :"java_version", acs49_cmd_filter do
      c_sudo "java -version"
   end

   desc 'Pythonパッケージのインストール状態確認'
   task :"python_packages", acs49_cmd_filter do
      c_sudo "rpm -qa |grep python-argparse; true"
      c_sudo "rpm -qa |grep ipset; true"
   end
end

namespace(:openjdk18_cutback_acs49) do
   ## Valiables
   # uninstal packege definitions

   desc '事前準備-切り戻し'
   task :"all", acs49_target do
      start_commands do
         openjdk18_cutback_acs49.upload_file();       log_ok "upload_file"
         # プロテクトされていて削除できない。
         # 戻せるのが理想だが、戻せなければそのままでもよい
   #      openjdk18_cutback_acs49.ca_certificates();   log_ok "ca_certificates"
         openjdk18_cutback_acs49.alternatives_java(); log_ok "alternatives_java"
         # 2017/04/20 アンインストールはしない
         #openjdk18_cutback_acs49.java8_jdk();         log_ok "java8_jdk"
         openjdk18_cutback_acs49.python_packages();   log_ok "python_packages"
      end
   end

   desc '必要ファイルの削除'
   task :"upload_file", acs49_cmd_filter do
      c_sudo "rm -rf #{pre_upgrade_packages_base_path}; true"
      c_sudo "rm -rf /etc/yum.repos.d/acs.repo; true"
   end

   desc 'CA証明書の削除'
   task :"ca_certificates", acs49_cmd_filter do
      # 依存関係が多いので切り戻しはしない
      #sudo "yum -y #{yum_opts} erase ca-certificates"
   end

   desc 'java8 JDKのアンインストール'
   task :"java8_jdk", acs49_cmd_filter do
      c_sudo "yum -y #{yum_opts} erase #{pre_jdk_package}"
   end

   desc 'java8 JDKのAltenative削除'
   task :"alternatives_java", acs49_cmd_filter do
      c_sudo "alternatives --remove java #{pre_jdk_path}"
   end

   desc 'CloudStack Agentで必要なPythonパッケージのアンインストール'
   task :"python_packages", acs49_cmd_filter do
      c_sudo "yum -y #{yum_opts} erase python-argparse"
      c_sudo "yum -y #{yum_opts} erase ipset"
   end
end
