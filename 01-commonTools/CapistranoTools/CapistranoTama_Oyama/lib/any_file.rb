# coding: utf-8

# 使用する際はcapistranoのオプション -S を指定すること。指定したファイルが対象となる
# sampleでは-Sの変数fileに指定されたファイルを転送する
# sample command ) cap any_file:file-transport -S file="/tck2ejo2-vclweb04_vmstat.20150421.log"
namespace(:any_file) do
  desc '任意のファイル転送'
  task :"file-transport", roles=>[:target] do
    puts fetch(:file, "master")
    upload(file, user_home, :via=>:scp)
  end

  desc '任意のファイル転送'
  task :"file-transport-dl", roles=>[:target] do
    puts fetch(:file, "master")
    download(file, user_ckkcl, :via=>:scp)
  end

  desc '任意のパッケージを展開'
  task :"package-extract", :roles=>[:targets] do
    fetch(:file, "master")
    run "tar xzf #{file}"
  end
end
