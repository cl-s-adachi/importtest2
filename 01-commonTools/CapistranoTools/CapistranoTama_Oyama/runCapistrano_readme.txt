#########################################################################
#
# capistrano 実行スクリプト
# usage. runCapistrano.sh [-t rbfile-keyword] -e EXECOMMAND
# -t : 対象rbファイルの前方一致キーワード(ex:hosts.v-tckktky4) *未指定時は全rbファイル対象
# -e : capistranoのコマンド名(ex:cloud_agent:status)
# ex. ./runCapistrano.sh [-t hosts.v-tckktky4] -e cloud_agent:status
#
########################################################################

##############################################################################
■説明
・createHostRb.pyで出力されたファイルを利用すること。

・2014.2は古いバージョンなので、最新版で作成すること。
ファイル名が、「hosts.v-tckktky4-pEbsv01.rb」のような形式で出力されていればOK」

・2014.2の「hosts.tckktky4-pEbsv01.rb」でも問題ないですが、
VALUE/PREMIUMクラスタ単位での実行はできません。

##############################################################################
■対象範囲の選定

KCPSに特化した話ですが、-tオプションによって以下のような使い分けが可能

全て > VALUE/PREMIUM種別 > ゾーン
の粒度で実行単位を制御可能

以下に指定例を記載する.

1. 全ホストを対象
-t オプションを指定しない
./runCapistrano.sh -e cloud_agent:status

2. VALUEクラスタ全て
hosts.v-を指定
./runCapistrano.sh -t hosts.v- -e cloud_agent:status

3. PREMIUMクラスタ全て
# hosts.p-を指定
./runCapistrano.sh -t hosts.p- -e cloud_agent:status

4. 特定ゾーンのVALUEクラスタのみ. 例では東京ゾーン
# hosts.hosts.v-tckktky4を指定
./runCapistrano.sh -t hosts.hosts.v-tckktky4 -e cloud_agent:status

5. 特定ゾーンのPREMIUMクラスタのみ. 例では東京ゾーン
# hosts.hosts.v-tckktky4を指定
./runCapistrano.sh -t hosts.hosts.p-tckktky4 -e cloud_agent:status

ファイル名の前方一致なので、まだまだ用途使えますが、とりあえず上記のように利用可能
例えば
./runCapistrano.sh -t hosts.v-tckktky4-pEbsv0 -e cloud_agent:status
上記の指定にすると、tckktky4-pEbsv0*(01〜09)の実行が可能といった具合。

基本的に使うのは、1.〜5.いずれかのパターンだと思われます。

##############################################################################
■1つ1つ対話式で実行したい場合
-t オプションで指定した対象ファイルが大量にある場合、毎回y/nを入力するのも面倒なので、
機能をOFFにしていますが、スクリプト内の以下の変数をtrueに変更することにより、
1rbファイル(=1クラスタずつ）対話式で実行することが可能

isExecutionConfirm=false

##############################################################################
■出力イメージ

[INFO] target files
hosts.v-tckktky4-pEbsv01.rb     hosts.v-tckkwjih-pEbsv01.rb     hosts.v-tckkwjih-pEbsv02.rb
[INFO] execute command
cloud_agent:status
[CONFIRM] 処理を開始しますか？(y/n):y
[INFO]starting run **cap cloud_agent:status** ::::: ./target/hosts.v-tckktky4-pEbsv01.rb->./hosts.rb
  * 2014-02-18 15:38:41 executing `cloud_agent:status'
  * executing "sudo -p 'sudo password: ' /etc/init.d/cloud-agent status ; exit 0"
    servers: ["tckktky4-pbhpv010", "tckktky4-pbhpv001"]
connection failed for: tckktky4-pbhpv010 (Errno::EHOSTUNREACH: No route to host - connect(2))
[INFO] finished run **cap cloud_agent:status** ::::: ./target/hosts.v-tckktky4-pEbsv01.rb->./hosts.rb

[INFO]starting run **cap cloud_agent:status** ::::: ./target/hosts.v-tckkwjih-pEbsv01.rb->./hosts.rb
  * 2014-02-18 15:38:44 executing `cloud_agent:status'
  * executing "sudo -p 'sudo password: ' /etc/init.d/cloud-agent status ; exit 0"
    servers: ["tckkwjih-pbhpv013", "tckkwjih-pbhpv014"]
    [tckkwjih-pbhpv013] executing command
    [tckkwjih-pbhpv014] executing command
 ** [out :: tckkwjih-pbhpv014] cloud-agent (pid  1781) is running...
 ** [out :: tckkwjih-pbhpv013] cloud-agent (pid  15641) is running...
    command finished in 56ms
[INFO] finished run **cap cloud_agent:status** ::::: ./target/hosts.v-tckkwjih-pEbsv01.rb->./hosts.rb

[INFO]starting run **cap cloud_agent:status** ::::: ./target/hosts.v-tckkwjih-pEbsv02.rb->./hosts.rb
  * 2014-02-18 15:38:45 executing `cloud_agent:status'
  * executing "sudo -p 'sudo password: ' /etc/init.d/cloud-agent status ; exit 0"
    servers: ["tckkwjih-pbhpv021"]
    [tckkwjih-pbhpv021] executing command
 ** [out :: tckkwjih-pbhpv021] cloud-agent (pid  16210) is running...
    command finished in 30ms
[INFO] finished run **cap cloud_agent:status** ::::: ./target/hosts.v-tckkwjih-pEbsv02.rb->./hosts.rb

[INFO] finished all run!　★これが出れば終了です.

########################################