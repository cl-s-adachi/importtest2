##------------------------------------------------------------------------------
## 使用するアカウントとパスワードを指定してください。
## 一部のコマンドはroot権限、またはsudoでrootになれる権限が必要です。

set :user, "root"
set :password, "Admin123ckk!"
#set :password, "Creation@Line"


#ssh_options[:keys] = %w(/root/.ssh/pdsh.nonpass.kvmhost)
default_run_options[:pty]=true

##------------------------------------------------------------------------------
## 対象ホストの指定
## 指定方法：同梱のtargetsディレクトリ内のhost.*-*-*.rbファイルを、
##           Capfileと同じ階層に「hosts.rb」としてコピーしてください。
##           その後「cap コマンド名」でコマンドを実行します。
##           コピーしたhostsファイルが正しいか確認する場合は
##           「cap hostname」で取得したホスト名が一致するか確認してください。

load "hosts.rb"

#-------------------------------------------------------------------------------

## Valiables

# show line logs count
set :line_number, 10

# update packege definitions
set :new_package, 'CloudStack-PATCH_E-3.0.7-8-rhel6.2'
set :old_package, 'CloudStack-3.0.5-7-rhel6.2'
set :package_ext, '.tar.gz'
set :package_name, new_package
set :original_install_sh, 'install.sh'
set :tmp_custom_install_sh, 'tmp_install.sh'
set :custom_install_sh, 'install-custom.sh'
set :update_log, 'cloud-agent-update.log'
set :uninstall_log, 'cloud-agent-uninstall.log'
set :install_log, 'cloud-agent-install.log'
set :new_agent_file, 'cloud-agent'
set :agent_jar, 'cloud-agent.jar'
set :old_install_log, 'cloud-agent-305-install.log'
set :agent_pid, '`cat /var/run/cloud-agent.pid`'
user_home = '/root/'
user_home = "/home/#{user}/" if user != 'root'

# create-bridge definitions
set :vlanScriptPath, '/usr/lib64/cloud/agent/scripts/vm/network/vnet/modifyvlan.sh' # CS3.x
set :vlanPif, 'bond1'



## Tasks
desc 'ホスト名を表示します'
task :hostname, :roles=>[:targets] do
   run "echo $HOSTNAME"
end

desc 'ホストの時刻を表示します'
task :date, :roles=>[:targets] do
   run "date"
end

desc 'ホストの情報を確認します'
task :"hostinfo", :roles=>[:targets] do
   run "date; uname -n; id"
end

## agentへのアップロードするファイルを一括でscpする用に修正 須藤
## 2014.06.13 3.0.7 HostFix用に修正 須藤 ##
desc 'パッケージをアップロードします'
task :"package-upload", :roles=>[:targets] do
   # CloudStack-PATCH_E-3.0.7-8-rhel6.2.tar.gz	
   #file = package_name + package_ext
   #upload(file, user_home + file, :via=>:scp)
   # cloud-agent-3.0.5-7.el6.x86_64.tar.gz
   #file = old_package + package_ext
   #upload(file, user_home + file, :via=>:scp)
   # /etc/init.d/cloud-agent 
   #file = new_agent_file
   #upload(file, user_home + file, :via=>:scp)
   # cloud-agent,jar
   file = agent_jar 
   upload(file, user_home + file, :via=>:scp)
#   run "ls -l #{file}"
end

#desc '古いパッケージをアップロードします'
#task :"old_package-upload", :roles=>[:targets] do
#   file = old_package + package_ext
#   upload(file, user_home + file, :via=>:scp)
#   run "ls -l #{file}"
#end

desc 'パッケージを展開します'
task :"package-extract", :roles=>[:targets] do
   file = package_name + package_ext
   run "tar xzf #{file}"
   run "cd #{package_name}; sed -e \"s/yum update/yum update -y/g\" #{original_install_sh} > #{tmp_custom_install_sh}; ls -lhG"
   run "cd #{package_name}; sed -e \"s/yum remove/yum remove -y/g\" #{tmp_custom_install_sh} > #{custom_install_sh}; chmod +x #{custom_install_sh}; ls -lhG"
end

desc 'Agentをアップデートします'
task :"cloud-agent-update", :roles=>[:targets] do
   run "cd #{user_home}#{package_name}; #{sudo} yum clean all; date > #{update_log}; #{sudo} ./#{custom_install_sh}  --upgrade cloudstack >> #{update_log}; tail -n 1 #{update_log}"
end

desc 'Agentをインストールします'
task :"cloud-agent-install", :roles=>[:targets] do
   run "cd #{user_home}#{package_name}; #{sudo} yum clean all; date > #{install_log}; #{sudo} ./#{custom_install_sh} --install-agent >> #{install_log}"
end

#desc '古いパッケージを展開します'
#task :"old-package-extract", :roles=>[:targets] do
#   file = old_package + package_ext
#   run "tar xzf #{file}"
#   run "cd #{old_package}; sed -e \"s/yum update/yum update -y/g\" #{original_install_sh} > #{custom_install_sh}; chmod +x #{custom_install_sh}; ls -lhG"
#end

# 2014/3/13 aganeのダウングレードを追加
desc 'Agentをアンインストールします' 
task :"cloud-agent-uninstall", :roles=>[:targets] do
   # uninstall existing agent
   run "cd #{user_home}#{package_name}; #{sudo} yum clean all; date >> #{uninstall_log}; #{sudo} ./#{custom_install_sh} -r cdsk>> #{uninstall_log}"
   # delete rpm cache dir+contents
   run "#{sudo} rm -Rf /var/cache/yum/x86_64/6Server/cloud-temp"
end


desc '古いAgentをインストールします'
task :"cloud-old-agent-install", :roles=>[:targets] do
   # install older version of agent
   run "cd #{user_home}#{old_package}; #{sudo} yum clean all; date > #{install_log}; #{sudo} ./#{custom_install_sh} --install-agent >> #{old_install_log}"
   # revert agent.properties to the version saved from the previous install
   run "#{sudo} cp -p /etc/cloud/agent/agent.properties.rpmsave /etc/cloud/agent/agent.properties"
   # revert log4j-cloud to the version saved from the previous install
   run "#{sudo} cp -p /etc/cloud/agent/log4j-cloud.xml.rpmsave /etc/cloud/agent/log4j-cloud"
end

namespace(:cloud_agent) do
  # agentの再起動
  desc 'Agentを再起動します'
  task :"restart", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent restart"
  end

  desc 'Agentを停止します'
  task :"stop", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent stop"
  end

  desc 'Agentを起動します'
  task :"start", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent start"
  end

  desc 'Agentのバージョンを表示します'
  task :"version", :roles=>[:targets] do
    sudo "rpm -qa | grep cloud-agent-[0-9]; exit 0"
  end

  # agentのstatus確認
  desc 'Agentのステータスを確認します'
  task :"status", :roles=>[:targets] do
     sudo "/etc/init.d/cloud-agent status ; exit 0" 
  end

  desc 'Agentのログを表示します'
  task :"cloud-agent-log", :roles=>[:targets] do
     sudo "tail -n #{line_number} /var/log/cloud/agent/agent.log"
  end

end


namespace(:agent_conf) do
  ## 2013.04.26 snapshot対応でagentのファイルを置き換える処理

  desc 'agent.propertiesをバックアップ'
  task :"conf-backup", :roles=>[:targets] do
     sudo "/bin/cp -p /etc/cloud/agent/agent.properties /etc/cloud/agent/agent.properties.$(date +%Y%m%d)"
     sudo "/bin/ls -al /etc/cloud/agent/agent.properties*"
  end

  desc 'cmds.timeout=172800を追加'
  task :"update-cmds-timeout", :roles=>[:targets] do
     sudo "/bin/echo cmds.timeout=172800 >> /etc/cloud/agent/agent.properties"
     sudo "/usr/bin/tail -2 /etc/cloud/agent/agent.properties"
  end


  #
  # FILE=<バックアップファイル>
  # 
  # 　・FILEオプションを使用した場合は指定したファイルで /etc/cloud/agent/agent.properties を上書く
  # 　・FILEオプションが指定されなかった場合は /etc/cloud/agent/agent.properties.<実行日時のYYYYMMDD表現> で /etc/cloud/agent/agent.properties を上書く
  desc 'agent.propertiesをrollback'
  task :"conf-rollback", :roles=>[:targets] do
     backup_file = (ENV["FILE"] || "/etc/cloud/agent/agent.properties." + %x(date +%Y%m%d).chomp)

     sudo "/bin/cp -p #{backup_file} /etc/cloud/agent/agent.properties"
     sudo "/bin/ls -al /etc/cloud/agent/agent.properties*"
     sudo "/usr/bin/md5sum /etc/cloud/agent/agent.properties*"
  end

  desc 'cmds.timeout値を確認'
  task :"show-cmds-timeout", :roles=>[:targets] do
     sudo "/bin/grep cmds.timeout /etc/cloud/agent/agent.properties  ; exit 0"
  end

## 2013.07.05 file descriptor対応のため、cloud-agentファイルの置き換え処理を追加 ##
## cloud-agent-uploadとcloud-agent-backupを１つにまとめる　須藤 ##
#  desc 'cloud-agentファイルをアップロードします'
#  task :"cloud-agent-upload", :roles=>[:targets] do
#     file = new_agent_file
#     upload(file, user_home + file, :via=>:scp)
#  end

  desc 'cloud-agentの更新'
  task :"cloud-agent-change", :roles=>[:targets] do
     file = new_agent_file
     
     sudo "/bin/cp -p /etc/rc.d/init.d/cloud-agent /etc/rc.d/init.d/cloud-agent.$(date +%Y%m%d)"
     sudo "/bin/ls -al /etc/rc.d/init.d/cloud-agent*"
     sudo "/bin/rm -f /etc/rc.d/init.d/cloud-agent"
     
     sudo "/bin/cp -p #{file} /etc/rc.d/init.d/cloud-agent"
     sudo "/bin/cat /etc/rc.d/init.d/cloud-agent | grep ulimit"
  end

  desc 'cloud-agentのulimitを確認'
  task :"cloud-agent-ulimit-check", :roles=>[:targets] do
     sudo "/bin/cat /proc/#{agent_pid}/limits | grep open"
  end

  desc 'log4j-cloud.xmlをバックアップ'
  task :"log4j-cloud-backup", :roles=>[:targets] do
     sudo "/bin/cp -p /etc/cloud/agent/log4j-cloud.xml /etc/cloud/agent/log4j-cloud.xml.$(date +%Y%m%d)"
     sudo "/bin/ls -al /etc/cloud/agent/log4j-cloud.xml*"
  end



## 2014.04.16 cloud-agent.jarファイルの更新を追加 須藤##

#  desc 'jarファイルをアップロードします'
#  task :"agent-jar-upload", :roles=>[:targets] do
#     file = agent_jar 
#     upload(file, user_home + file, :via=>:scp)
#  end

  desc 'jarファイルの更新'
  task :"agent-jar-change", :roles=>[:targets] do
     file = agent_jar
     
     sudo "/bin/cp -p /usr/share/java/cloud-agent.jar /home/ckkcl/cloud-agent.jar.$(date +%Y%m%d)"
     sudo "/bin/ls -al /home/ckkcl/cloud-agent.jar*"

     sudo "/bin/cp -fp #{file} /usr/share/java/cloud-agent.jar"
     sudo "/bin/ls -al /usr/share/java/cloud-agent.jar"
  end
## 2014.06.13 cloud-agent.jarファイル切り戻し用 須藤 ##


 desc 'jarファイルのきり戻し'
  task :"agent-jar-re-change", :roles=>[:targets] do
     sudo "/bin/cp -p /home/ckkcl/cloud-agent.jar.$(date +%Y%m%d) /usr/share/java/cloud-agent.jar"
     sudo "/bin/ls -al /usr/share/java/cloud-agent.jar"
 end

end
  
  
##
namespace(:any_file) do
  # 使用する際はcapistranoのオプション -S を指定すること。指定したファイルが対象となる
  # sampleでは-Sの変数fileに指定されたファイルを転送する
  # sample command ) cap any_file:file-transport -S file="/tck2ejo2-vclweb04_vmstat.20150421.log"
  desc '任意のファイル転送'
  task :"file-transport", roles=>[:target] do
    puts fetch(:file, "master")
    upload(file, user_home, :via=>:scp)
  end
  
  desc '任意のパッケージを展開'
  task :"package-extract", :roles=>[:targets] do
    fetch(:file, "master")
    run "tar xzf #{file}"
  end
end


desc 'modifyvlan.shでホストのvlanを作成'
task :"create-bridge", :roles=>[:targets] do
  #puts "    vlanScriptPath=#{vlanScriptPath}"
  #puts "    vlanPif=#{vlanPif}"

  run "#{vlanScriptPath} -v 1900 -p #{vlanPif} -o add && "\
      "#{vlanScriptPath} -v 1899 -p #{vlanPif} -o add && "\
      "#{vlanScriptPath} -v 1898 -p #{vlanPif} -o add && "\
      "#{vlanScriptPath} -v 1897 -p #{vlanPif} -o add"

  puts "Task #{current_task.name} finished"
end

desc 'machine=rhel6.6.0のインスタンスがあるか確認'
task :"vm-check" , :roles=>[:targets] do
  run 'grep rhel6.6.0 /etc/libvirt/qemu/*xml | cut -d" " -f 1 | sed s/"\/etc\/libvirt\/qemu\/"//g  | sed s/\.xml://g'
end
