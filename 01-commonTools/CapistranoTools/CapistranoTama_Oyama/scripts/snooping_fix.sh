#!/bin/bash
if [ ! -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=cloudVirBr
elif [ -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ ! -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=brbond
else
	echo "ブリッジ名がひとつも存在しないか、\"cloudVirBr\" \"brbond\" 両方のブリッジ名が存在します。"
	exit 1
fi

brname=`ls /sys/class/net | grep "${br_name}"`
for line in ${brname}; do
	if [ ! -z "`cat /sys/class/net/${line}/bridge/multicast_snooping | grep \"0\"`" ]; then
		echo "Snooping setting == `cat /sys/class/net/${line}/bridge/multicast_snooping` ${line}"
	else
		echo "0" >/sys/class/net/${line}/bridge/multicast_snooping
		echo "Snooping setting == `cat /sys/class/net/${line}/bridge/multicast_snooping` ${line}"
	fi
done
