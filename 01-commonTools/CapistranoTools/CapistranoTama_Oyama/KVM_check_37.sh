#!/bin/bash
# 2017 CREATIONLINE INC. All rights reserved.
# 旧DB サーバアドレス
DB_SRV=ckktky4-vcldb00-tcsm001
#DB_SRV=172.22.132.93
#DB_SRV=172.22.132.95

# 新DB サーバアドレス
#DB_SRV=ckktky4-pcldb10
#DB_SRV=172.22.132.28
#DB_SRV=172.22.132.32

# 言語環境設定
export LANG=ja_JP.UTF-8

# hosts.rb がないか空の場合、エラーを出して終了。
if [ ! -f "hosts.rb" ]; then
	echo "hosts.rb がありません。"
	echo "Make_hosts_rb.sh で hosts.rb を生成して下さい。"
	exit 2
elif [ -z "`cat hosts.rb | grep 'pbhpv' 2>/dev/null`" ]; then
	echo "hosts.rb に host 情報がありません。"
	echo "Make_hosts_rb.sh で hosts.rb を生成して下さい。"
	exit 2
fi

if [ ! -z "`cat hosts.rb | grep 'pbhpv' 2>/dev/null`" ]; then
	# 対象ホストを hosts.rb より読み込む
	all_hosts=`cat hosts.rb | awk -F"(" '{print $2}' | awk -F")" '{print $1}' | sed 's/ /\n/g'`
	# KVM ホストの全台数
	all_kvm_host_num=`echo "${all_hosts}" | wc -l`
else
	echo "hosts.rb に host 情報がありません。"
	exit 2
fi

# 各ホストの agent.properties 格納先
DL_DIR="./agent_properties_file"
# agent.properties 格納先を初期化
rm -rf ${DL_DIR}
mkdir -p ${DL_DIR}

# NG log 保存先
NG_LOG=./NG_log
# NG log 保存先を初期化
rm -rf ${NG_LOG}
mkdir -p ${NG_LOG}

# 各既存ホストの /root/agent.properties を、各 KVM ホストの /tmp に移動。
cap develop agent_properties_check:mv_agent_properties
# 各ホストの、/etc/cloud/agent/agent.properties を /root 配下にコピー。
# agent.properties ファイル名の先頭に、各ホスト名をつける。
cap develop agent_properties_check:cp_agent_properties

# 各ホストから agent.properties を収集する
echo "==========【Start download \"agent.properties\"】=========="

for line in `echo ${all_hosts}`; do
        cap any_file:file-transport-dl -S file="/root/${line}_agent.properties" >/dev/null 2>&1

        if [ ! -f "${DL_DIR}/${line}_agent.properties" ]; then
                # 各ホストの agent.properties の DL に失敗したら、2 秒おきに 3 回リトライする。
                for ((i=0; i<3; i++)); do
                        if [ ! -f "${DL_DIR}/${line}_agent.properties" ]; then
                                echo "[ NG ]    \"${line}\" : リトライ"
                                sleep 2
                                cap any_file:file-transport-dl -S file="/root/${line}_agent.properties" >/dev/null 2>&1
                                continue
                        else
                                break
                        fi
                done

		# 1 ホストでも agent.properties のダウンロードに失敗したら処理を中断
                if [ ! -f "${DL_DIR}/${line}_agent.properties" ]; then
                        echo "[ Error ] \"${line}\" : \"agent.properties\" ダウンロード失敗" >>${NG_LOG}/agent_properties_DL_NG.log
                        echo "[ Error ] \"${line}\" : \"agent.properties\" ダウンロード失敗"
                else
                        echo "[ OK ]    \"${line}\""
                fi
        else
                echo "[ OK ]    \"${line}\""
        fi
done

# 1 ホストでも agent.properties のダウンロードに失敗したら処理を中断
if [ -f "${NG_LOG}/agent_properties_DL_NG.log" ]; then
        echo ""
        cat ${NG_LOG}/agent_properties_DL_NG.log
        echo ""
        echo "チェック処理を中断します。"
        echo ""
        exit 2
else
        echo ""
        echo "すべてのホストからのダウンロードに成功しました。"
        echo ""
fi

# 取得した agent.properties のホスト一覧をリスト化
(
	cd ${DL_DIR}
	ls *_agent.properties | awk -F"_" '{print $1}' >../atest.log
)

# 取得したダウンロード成功ホストのリストと、全ホストのリストを比較して、過不足がないかチェックする。
for all_host in `echo ${all_hosts}`; do
	if [ -z "`cat atest.log | grep ${all_host}`" ]; then
		echo "[ NG ] ${all_host}" >>${NG_LOG}/NG_get_agent_properties.log
		echo "[ NG ] ${all_host}"
	fi
done
rm -f ./atest.log

if [ -f ${NG_LOG}/NG_get_agent_properties.log ]; then
	echo ""
	echo "agent.properties を取得できなかった host があります。"
	echo ""
	cat ${NG_LOG}/NG_get_agent_properties.log
	exit 2
fi

echo ""

# ここから KVM Host check

echo "====================【Start host check】===================="

for line in `echo ${all_hosts}`; do
	# agent_properties.prm パラメータ参照項目
	for line2 in `cat agent_properties.prm`; do
		prm=`echo ${line2} | awk -F"=" '{print $1}'`
		val=`echo ${line2} | awk -F"=" '{print $2}'`
		host_val=`cat ${DL_DIR}/${line}_agent.properties | grep "${prm}=" | awk -F"=" '{print $2}'`

		# 値が空か設定そのものが存在しない場合 NG
		if [[ "${val}" =~ ^${host_val}$ ]]; then
			:
		else
			if [ -z ${host_val} ]; then
				echo "[ NG ] ${line} : ${line}_agent.properties にオペランド \"${prm}\" がありません。" >>${line}_agent_properties_NG.log
			else
				echo "[ NG ] ${line} : ${prm}=${host_val} => 正しい値 : ${val}" >>${line}_agent_properties_NG.log
			fi
		fi
	done

	# DB アクセスできない場合処理を中断
	name_db=`mysql -u root cloud -h ${DB_SRV} -t -e "select name from host where removed is null and hypervisor_type='KVM';" | grep "${line}" | awk '{print $2}'` >/dev/null 2>&1
	if [ -z ${name_db} ]; then
		echo "DB から host を参照できません。" >>${line}_agent_properties_NG.log
		echo "DB から host を参照できません。"
		exit 2
	fi

	# DB 参照項目チェック
	val_prm=("pod" "zone" "guid" "cluster" "LibvirtComputingResource.id")
	val_db=("pod_id" "data_center_id" "guid" "cluster_id" "id")
	for ((i=0; i<5; i++)); do
		el_prm=`cat ${DL_DIR}/${line}_agent.properties | grep "${val_prm[$i]}" | awk -F"=" '{print $2}'`
		el_db=`mysql -u root cloud -h ${DB_SRV} -t -e "select ${val_db[$i]} from host where removed is null and hypervisor_type='KVM' and name=\"${name_db}\";" | grep -v "${val_db[$i]}" | grep -v '+' | awk '{print $2}'`
		if [ ! -z "`echo ${el_db} | grep -e '-LibvirtComputingResource'`" ]; then
			el_db=`echo ${el_db} | awk -F"-Libvirt" '{print $1}'`
		fi
		if [[ "${el_prm}" =~ ^${el_db}$ ]]; then
			:
		else
			# 値が空か設定そのものが存在しない場合 NG
			if [ -z ${val_db[$i]} ]; then
				echo "[ NG ] ${line} : ${line}_agent.properties にオペランド \"${val_prm[$i]}\" がありません。" >>${line}_agent_properties_NG.log
			else
				echo "[ NG ] ${line} : ${val_prm[$i]} : DB の値 = ${el_db} => agent.properties の値 = ${el_prm}" >>${line}_agent_properties_NG.log
			fi
		fi
	done

	if [ -f ${line}_agent_properties_NG.log ]; then
		sed -i -e "1s/^/====================【${line}】====================\n/" ${line}_agent_properties_NG.log
		echo "" >>${line}_agent_properties_NG.log
	fi

	if [ ! -f ${line}_agent_properties_NG.log ]; then
		echo "[ OK ] ${line}"
	else
		echo "[ NG ] ${line}"
	fi

	# 0.5sec 間隔で流す。
	usleep 500000
done

echo ""

# NG がない場合、ファイルが存在しないのでエラーが出力される対策。
touch dummy_agent_properties_NG.log
# NG のみまとめて出力する。
ng_file=$(ls *agent_properties_NG.log)
if [ ! -z "`echo ${ng_file[@]}`" ]; then
	for ng in ${ng_file[@]}; do 
		cat ${ng} 
	done
fi
rm -f dummy_agent_properties_NG.log
mv *NG.log ${NG_LOG} >/dev/null 2>&1

# 全 KVM ホスト中何台が "OK" かを出力。
echo "[ OK ] KVM Host = \"$(expr ${all_kvm_host_num} - $(ls ${NG_LOG} | wc -l))/${all_kvm_host_num}\""
# NG ディレクトリが空の場合削除する。
if [ -z "`ls ./NG_log`" ]; then
	rm -rf ./NG_log
fi

# 各ホストから、/root 配下にコピーした agent.properties を削除。
# cap develop agent_properties_check:rm_agent_properties
exit 0
