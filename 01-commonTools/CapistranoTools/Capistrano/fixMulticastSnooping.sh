#!/bin/bash

basedir=`dirname $0`
temp=${basedir}/tmp
mkdir -p ${temp}


SCRIPT="`basename $0`"
DBHOST="172.22.132.84"

checkresult=${temp}/temp_snooping_check_fixMulticastSnooping.sh
ngresult=${temp}/temp_snooping_ng_fixMulticastSnooping.sh
fixresult=${temp}/temp_snooping_fix_fixMulticastSnooping.sh


cd ${basedir}

fqdn='cloud_platform.kddi.ne.jp'
hostname==`mysql -u root cloud -h ${DBHOST} -N -e "select name from host where removed is null and name like '$1%' ;" `

if [[ $1 =~ ${fqdn} ]] ; then
      echo "INFO :引数にするhost名をfqdn無しで指定してください。"
      exit 0
elif [ -z $1 ] ; then
      echo "INFO :host名をfqdn無しで指定してください。"
      exit 0
elif [ $# -gt 1 ] ; then 
      echo "INFO :引数を2つ以上指定しないでください。"
      exit 0 
elif [[ ${hostname} =~ $1 ]] ; then
# check multicast snooping
      rm -rf ${checkresult} ${ngresult} ${ngresult}
      echo "role :targets, *%w($1)" > hosts.rb

      date
      echo ""
      echo "##################################"
      echo "##MC_snoopingをチェックします。###"
      echo "##################################"
      echo ""     

      cap develop kvm_check_acs49:snooping_check
      cap develop kvm_check_acs49:snooping_check >> ${checkresult} 2>&1


# fix multicast snooping
      grep NG ${checkresult} | awk '{print $4}' | sed s/]//g | sort -u > ${ngresult}
      ./createHostsRbFromHostlist.sh ${ngresult} > hosts.rb
    if [ -s  ${ngresult} ] ;then 
      echo ""
      echo "##################################"
      echo "##MC_snoopingを修正します。#######"
      echo "##################################"
      echo "" 
     
      cap develop kvm_upgrade_acs49:snooping_fix > ${fixresult} 2>&1
      echo "done"
    else 
      echo ""
      echo "NGのブリッジはありません。ツールを終了します。"
      exit 0 
    fi

# recheck multicast snooping
      rm -rf ${checkresult} ${ngresult} ${ngresult}
      echo ""
      echo "##################################"
      echo "##MC_snoopingをチェックします。###"
      echo "##################################"
      echo "" 

      cap develop kvm_check_acs49:snooping_check 
      cap develop kvm_check_acs49:snooping_check >> ${checkresult} 2>&1 
 
      grep NG ${checkresult} | awk '{print $4}' | sed s/]//g | sort -u > ${ngresult}
    if [ ! -s ${ngresult} ] ; then
      echo ""
      echo "NGのブリッジはありません。ツールを終了します。"
      exit 0
    else 
      echo "NGのブリッジが残っています。ツールを終了します。"
      exit 0 
    fi

else
      echo "INFO: ホスト名が正しくない、または引数にするhost名をfqdn無しで指定してください。"
      exit 0
fi
