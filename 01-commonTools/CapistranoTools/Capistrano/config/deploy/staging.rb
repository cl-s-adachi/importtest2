# coding: utf-8
##------------------------------------------------------------------------------
set :stage, :develop #環境名

# 色付け
#require "capistrano_colors"

## 使用するアカウントとパスワードを指定してください。
## 一部のコマンドはroot権限、またはsudoでrootになれる権限が必要です。

set :user, "root"
set :password, "<REPLACE_PASSWORD>"
set :user_home, user=='root' ? '/root/' : "/home/#{user}/"
set :user_tckkcl, user=='tckkcl' ? '/root/' : "/home/tckkcl/common/capistrano/CapistranoTools/Capistrano/agent_properties_file/"

# HCAK: Net::SSH::AuthenticationFailedエラーとなる問題の解決
# http://qiita.com/takashibagura/items/33034c7529b553d2d0f9
#set :ssh_options, {
#   config: false,
#   port: '1000'
#}
ssh_options[:config] = false
ssh_options[:port] = "22"

#ssh_options[:keys] = %w(/root/.ssh/pdsh.nonpass.kvmhost)
#default_run_options[:pty]=true

# Filter
set :acs49_target, {:roles=>[:targets]}
set :acs49_cmd_filter, {:roles=>[:targets], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}

# TODO: ACS4.9のミニ環境の値であるため、変更必要。
set :management_ip_address, '10.193.5.247'
# TODO: 商用時にはIPアドレスが変更になる可能性あり。
set :cloudstack_repo_address, 'http://172.27.172.227/ACS'

# yum repositories
set :yum_repos, "--enablerepo=acs,rhel62_dvd,rhel63_dvd,rhel63_update"
set :yum_opts, "--debuglevel=1"

