# coding: utf-8
##------------------------------------------------------------------------------
set :stage, :local #環境名

# 色付け
#require "capistrano_colors"

## 使用するアカウントとパスワードを指定してください。
## 一部のコマンドはroot権限、またはsudoでrootになれる権限が必要です。

set :user, "nkmc"
set :password, "noiming"
set :user_home, user=='root' ? '/root/' : "/home/#{user}/"

## executeCmdAsRootでrootにsuする場合に使用するパスワード
$rootPassword = "noiming"

# HCAK: Net::SSH::AuthenticationFailedエラーとなる問題の解決
# http://qiita.com/takashibagura/items/33034c7529b553d2d0f9
set :ssh_options, {
   :config => false,
   :port => '22'
}

#ssh_options[:keys] = %w(/root/.ssh/pdsh.nonpass.kvmhost)
#default_run_options[:pty]=true

# Filter
set :acs49_target, {:roles=>[:local]}
set :acs49_cmd_filter, {:roles=>[:local], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}

# TODO: ACS4.9のミニ環境の値であるため、変更必要。
set :management_ip_address, '10.193.5.250'
# TODO: 商用時にはIPアドレスが変更になる可能性あり。
set :cloudstack_repo_address, '172.27.172.227'

# yum repositories
set :yum_repos, "--enablerepo=acs_f,rhel62_dvd,rhel63_dvd,rhel63_update"
set :yum_opts, "--debuglevel=1"
