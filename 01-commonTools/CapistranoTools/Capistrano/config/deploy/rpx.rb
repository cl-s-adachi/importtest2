# coding: utf-8
##------------------------------------------------------------------------------
set :stage, :rpx #環境名

# 色付け
#require "capistrano_colors"

## 使用するアカウントとパスワードを指定してください。
## 一部のコマンドはroot権限、またはsudoでrootになれる権限が必要です。

set :user, "ckkcl"
set :password, "CKK@6004"
set :user_home, user=='root' ? '/root/' : "/home/#{user}/"

## executeCmdAsRootでrootにsuする場合に使用するパスワード
$rootPassword = "Admin123ckk!"

# HCAK: Net::SSH::AuthenticationFailedエラーとなる問題の解決
# http://qiita.com/takashibagura/items/33034c7529b553d2d0f9
#set :ssh_options, {
#   config: false,
#   port: '1000'
#}
ssh_options[:config] = false
ssh_options[:port] = "22"

#ssh_options[:keys] = %w(/root/.ssh/pdsh.nonpass.kvmhost)
#default_run_options[:pty]=true

# Filter
set :acs49_target, {:roles=>[:targets]}
set :acs49_cmd_filter, {:roles=>[:targets], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}
