# coding: utf-8

# 処理実行結果をOK/NGの表形式で出力する。
# 呼び出し元は、ブロックに渡されるハッシュ変数scoreに
# キー名と結果(OK/NG等)を格納する。
#
# 以下のような出力が生成される
# Server            file CA JDK pyarg ipset
# tckktky4-pbhpv014  OK  NG  NG   NG    NG
# tckktky4-pbhpv010  OK  NG  NG   NG    NG
#
def self.caputer_servers(columns, &block)
  columns.unshift 'Server'
  # serversの中でhost名が一番長い要素を探す
  servers = find_servers_for_task(current_task)
  max_srv = servers.max_by {|server| server.host.length }
  svr_len = max_srv.host.length

  printHeader = true
  start_commands(:task_namespace, Logger::IMPORTANT) do
    servers.each do |current_server|

      score = {'Server' => current_server.host }
      block.call current_server, score

      # 各行、列の幅のリスト
      col_lens = []
      columns.each do |key|
         col_len = [key.length, score[key].to_s.length].max
         col_lens.push col_len
      end

      # ヘッダーの出力
      if ( printHeader )
         printHeader = false
         columns.each_with_index do |col, i|
            print col.ljust(col_lens[i] + 1)
         end
         puts  # 改行
      end
      # スコアの出力
      columns.each_with_index do |col, i|
         if ( i == 0 )
            print score[col].to_s.ljust(col_lens[i] + 1)
         else
            print score[col].to_s.center(col_lens[i] + 1)
         end
      end
      puts  # 改行

    end
  end
end

def self.start_commands(description=:task_namespace, log_level=Logger::IMPORTANT, &block)
   # ログレベルの変更
   log_level_scope log_level do
      if description == :task_namespace then
         description = current_task.fully_qualified_name
      end
      puts "Start  - #{description}" unless description.empty?
      block.call
      puts "Finish - #{description}" unless description.empty?

      # 実行サーバのOK/NG状況表示
      puts "================================================================="
        servers = find_servers_for_task(current_task)
        servers.each do |srv|
          result = srv.options[:has_error] ? :'Checking process completed successfully. = ' : :'Checking process completed successfully. = '
          puts "#{result} #{srv.host} #{srv.options[:error_task]}"
        end
      puts "================================================================="
   end
end

def self.log_level_scope(log_level=Logger::IMPORTANT, &block)
   begin
      # ログレベルの変更
      unless log_level.nil?
         old_level = logger.level
         logger.level = log_level
      end
      block.call
   ensure
      unless log_level.nil?
         # ログレベルを戻す
         logger.level = old_level
      end
   end
end

def self.capture_match(command, condition, options={}, &block)
   result = :UNKNOWN
   out = ""

   # 実行結果を取得
   begin
      out = capture(command, options)
   rescue Error => ex
      msg = ex.to_s
      puts "************************"
      puts ex.to_s
      puts "************************"
      result = :ERR
      if block_given? then
        block.call result, out
      end
      return result
   end
   out.chomp!

   # 条件の型により、マッチング方法を変更
   if condition.kind_of?(String) then
      # 文字列型は完全一致
      result = condition == out ? :OK : :NG
   elsif condition.kind_of?(Regexp) then
      # 正規表現型はマッチング
      result = condition =~ out ? :OK : :NG
   elsif condition.kind_of?(FalseClass) || condition.kind_of?(TrueClass) then
      # Boolean型は結果の有無
      if condition then
         # True 結果が取得できることが正
         # エラーメッセージでもTrueになる場合があるので注意
         result = out.empty? ? :NG : :OK
      else
         # False 結果が取得できないのが正
         result = out.empty? ? :OK : :NG
      end
   end

   if block_given? then
     block.call result, out
   end
   return result
end

def self.capture_unmatch(command, condition, options={}, &block)
   ret = :UNKNOWN
   out = ""
   capture_match(command, condition, options) do |result, output|
      out = output

      # 結果を反転させる
      if result == :OK then
         ret = :NG
      elsif result == :NG then
         ret = :OK
      else
         ret = result
      end
   end
   if block_given? then
     block.call ret, out
   end
   return ret
end

def self.log_ok(description)
   servers = find_servers_for_task(current_task)
   has_err = false
   all_err = true
   servers.each do |srv|
      if srv.options[:error_task].nil?
         all_err = false
      end
      if srv.options[:error_task] == description
         has_err = true
      end
   end

   if has_err
      result = :NG
   elsif all_err
      result = :SKIP
   else
      result = :OK
   end
   puts "#{result} - #{description}"
end

# 拡張runコマンド
# 正常ホストのみ処理続行が可能なフラグを設定する。
# 詳細はc_doを参照
def self.c_run(command)
   c_do do
      run command
   end
end

# 拡張sudoコマンド
# 正常ホストのみ処理続行が可能なフラグを設定する。
# 詳細はc_doを参照
def self.c_sudo(command)
   c_do do
      sudo command
   end
end

# 拡張suコマンド
# 正常ホストのみ処理続行が可能なフラグを設定する。
# 詳細はc_doを参照
def self.c_su(command)
   c_do do
      executeCmdAsRoot(command, nil)
    end
end

# 拡張実行コマンド
# 正常ホストのみ処理続行が可能なフラグを設定する。
# 異常のあったホストには
# options[:has_error] = true
# options[:error_task] = 異常が起きたタスク名
# が設定される。
#
# 本機能を使用する場合は、タスクの実行条件に以下を指定すること
# {:roles=>[:targets], :except=>{:has_error=>true}, :on_no_matching_servers=>:continue}
# 意味は以下の通り
# :roles 対象のロールまたはホスト
# :except=>{:has_error=>true} 事前のタスクで異常になったホストはタスクの対象外とする
# :on_no_matching_servers=>:continue タスクの実行対象のホストが0件であってもエラーにしない
#
def self.c_do(&block)
   begin
      block.call
   rescue Error => ex
      raise ex unless ex.to_s.include? ' on '

      msg = ex.to_s
      puts "************************"
      puts msg
      puts "************************"

      # TODO: CommandErrorならerr.hostsで取得できるかも
      ip = msg.slice(msg.rindex(' on ')+4 .. -1)
      ips = ip.split(',')

      servers = find_servers_for_task(current_task)
      servers.each do |srv|
         ips.each do |host|
            if srv.host == host
               # NGのあったサーバにフラグとエラータスク名を設定する
               srv.options[:has_error] = true
               srv.options[:error_task] = current_task.name
            end
         end
      end
   end
end

# 自前suコマンド
# Capistrano標準ではsudo/runしか用意されていないため
def executeCmdAsRoot(command, hosts, outputLabel=nil)
  #if caller specified nil, execute for all hosts
  hosts = find_servers_for_task(current_task) if hosts.nil?

  #query root password from user if it has not already been cached
  if $rootPassword.nil?
    puts "    This command requires root privileges.  Please enter the password for the root account of the target hosts."
    $rootPassword = fetch(:root_password, Capistrano::CLI.password_prompt("    Root password: "))
  else
    #puts "    This command requires root privileges.  Re-using previously entered password."
  end

  #execute specified cmd using root privleges, sending the cached root password
  results   = Hash.new()
  exitCodes = Hash.new()

  exitCode="EXIT_CODE="
  #run specified cmd and explicitly output the exit code for the script to parse
  run("su -c '#{command}; echo #{exitCode}$?'", :hosts=>hosts) do |channel, stream, out|
    channel.send_data("#{$rootPassword}\n")
    if stream==:out
      #strip put passwords & whitespace from output (send_data seems to repeat both a lot in the output)
      sanitizedOut = out.gsub($rootPassword, '')
      sanitizedOut = sanitizedOut.strip
      results[channel[:host]] = [] unless results.key?(channel[:host])
      if sanitizedOut.include? exitCode
        #parse out exit code and save separately from result output
        exitCodes[channel[:host]] = sanitizedOut.split(exitCode).last.strip()
        text = sanitizedOut.split(exitCode).first.strip()
        if not text.empty?
          results[channel[:host]] << text
        end
      elsif not sanitizedOut.empty?
        #Output from each host is not received in one go, but in chunks.
        #Save all the chunks received for a host as an array of strings.
        #Anybody who wants to make use of the saved output can use this later.
        #Due to the non-deterministic nature of the returned output different
        #runs of the script may result in slightly different chunk distribution
        #in the array even though the total output is identical.
#        if not sanitizedOut.include?('パスワード')
        if not sanitizedOut.strip.empty?
          results[channel[:host]] << sanitizedOut.strip()
        end
      end
    end
  end

  results.each do |host, outputs|
    outputs.shift # パスワード入力の出力
    logger.info "[#{host}] #{outputs.join('\n')}"
  end
  if (failed = exitCodes.select { |host, status| status != "0" }).any?
    message = "#{command.inspect} on #{failed.keys.join(',')}"
    error = CommandError.new("failed: #{message}")
    error.hosts = failed.keys.flatten
    raise error
  end
end

