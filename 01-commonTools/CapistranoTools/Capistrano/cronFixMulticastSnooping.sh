#!/bin/bash

basedir=`dirname $0`
temp=${basedir}/tmp
mkdir -p ${temp}

DBHOST="172.22.132.84"

INFO_LV="[INFO]"
WARN_LV="[INFO]"
TAG="[CloudStack]"
SCRIPT="`basename $0`"

target_dir=${basedir}/KVM_targets/
checkresult=${temp}/snooping_check
ngresult=${temp}/snooping_ng
fixresult=${temp}/snooping_fix

logger -t "${INFO_LV} ${TAG}" "Start ${SCRIPT}"

cd ${basedir}

# check multicast snooping
rm -rf ${checkresult}
dishosts=`mysql -u root cloud -h ${DBHOST} -N -e "select name from host where removed is null and status != 'Up' and type='Routing' and hypervisor_type='KVM';" `
for targetfile in `find ${target_dir}/*/*`
do
    cp ${targetfile} ${basedir}/hosts.rb
    for dishost in `echo ${dishosts} | sed s/.cloud_platform.kddi.ne.jp//g`
    do
        sed -i s/${dishost}//g hosts.rb
    done
    cap develop kvm_check_acs49:snooping_check >> ${checkresult}  2>&1
done

# fix multicast snooping
grep NG ${checkresult} | awk '{print $4}' | sed s/]//g | sort -u > ${ngresult}
 ./createHostsRbFromHostlist.sh ${ngresult} > hosts.rb

cap develop kvm_upgrade_acs49:snooping_fix > ${fixresult} 2>&1

for nghost in `cat ${ngresult}`
do
    logger -t "${INFO_LV} ${TAG}" "fixed multicast snooing:${nghost}"
done

logger -t "${INFO_LV} ${TAG}" "End ${SCRIPT}"
