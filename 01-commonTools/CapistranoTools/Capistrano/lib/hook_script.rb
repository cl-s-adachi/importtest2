# coding: utf-8

set :hookscript_cmd_filter, {:roles=>[:targets], :except=>{:has_error=>true, :skip_host=>true}, :on_no_matching_servers=>:continue}
set :dest_path, '/home/ckkcl'
set :hookscript_path, "#{dest_path}/hookscript"
set :sb_hook_md5, 'fd6b515c19a178a9f6d893d163993eb0'
set :cl_hook_md5, 'ac486d9402670cfd342f81fcf3ffbe18'
set :log_hook_md5, 'ee3526b97ccccbebc70ccd4bf8702b3d'
set :old_hook_rollback_md5, '85a34088d02daa032c282910da71945d'

namespace(:hookscript) do

  desc 'フックスクリプト配置評価'
  task :"score", acs49_target do

     columns = ['sbHook','clHook','hookLog','agtcnf','agtstats','agentBakFile']
     # ホストごとに処理
     caputer_servers(columns) do |current_server, score|
       options = {:hosts => current_server.host}

       # check SB hook
       score['sbHook'] = capture_match(
         "sudo find /etc/libvirt/hooks/qemu -type f | xargs md5sum | grep #{sb_hook_md5}; true", true, options)

       # check CL hook
       score['clHook'] = capture_match(
         "sudo find /etc/libvirt/hooks -type f | grep -w '/etc/libvirt/hooks/custom/all_CL-Custom-Hooks-Qemu.sh' | xargs md5sum | grep #{cl_hook_md5}; true", true, options)

       # check hookscript logrotate
       score['hookLog'] = capture_match(
         "sudo find /etc/logrotate.d -type f | grep libvirtd.qemu-hook | xargs md5sum | awk -F ' ' '{print $1}'; sudo find /var/log/libvirt -type d | grep -w '/var/log/libvirt/qemu-hook_old'; true", "#{log_hook_md5}\r\n/var/log/libvirt/qemu-hook_old", options)

       # check agent.properties 
       score['agtcnf'] = capture_match(
         "sudo cat /etc/cloudstack/agent/agent.properties | grep -w vm.migrate.speed=0 ;true", true, options)

       # check cloudstack-agent status 
       score['agtstats'] = capture_match(
         "sudo LANG=C /etc/init.d/cloudstack-agent status | grep 'cloudstack-agent (pid  [0-9]*) is running...' ;true", true, options)

       # check /etc/logrotate.d/loudstack-agent_*** bakupfile
       score['agentBakFile'] = capture_unmatch(
       "sudo find /etc/logrotate.d -type f | grep 'cloudstack-agent_[0-9]'; true", true, options)
    end
  end

  desc 'フックスクリプト配置'
  task :"all", acs49_target do
    start_commands(:task_namespace) do
      hookscript.upload_file();                           log_ok "upload_file"
      hookscript.put_hook_script();                       log_ok "put_hook_script"
      hookscript.put_hook_log_conf();                     log_ok "put_hook_log_conf"
      hookscript.update_agent_properties();               log_ok "update_agent_properties"
      hookscript.mv_cloudstack_agent_bakupfile();         log_ok "mv_cloudstack_agent_bakupfile"
    end
  end

  desc '資材配置・展開'
  task :"upload_file", hookscript_cmd_filter do
    top.upload "./upload/hookscript.tar.gz", "#{dest_path}", :via => :scp
    c_sudo "tar oxzvf #{dest_path}/hookscript.tar.gz -C #{dest_path}"
  end

  desc 'フックスクリプト配置'
  task :"put_hook_script", hookscript_cmd_filter do
    servers = find_servers_for_task(current_task)
    servers.each do |current_server|
      options = {:hosts => current_server.host}
      ans = capture("sudo find /etc/libvirt/hooks/qemu -type f | xargs md5sum | grep #{sb_hook_md5}; true", options)
      do_except_host(current_server.host) unless ans.empty?
    end
    c_sudo "cp -p /etc/libvirt/hooks/qemu #{hookscript_path}/qemu_`date +'%Y%m%d%H%M%S'`"; do_release_host()
    c_sudo "cp -p #{hookscript_path}/libvirtqemuhook.in /etc/libvirt/hooks/qemu"
    c_sudo "chmod 755 /etc/libvirt/hooks/qemu"
    c_sudo "mkdir -p /etc/libvirt/hooks/custom"
    c_sudo "cp -p #{hookscript_path}/all_CL-Custom-Hooks-Qemu.sh /etc/libvirt/hooks/custom/."
    c_sudo "chmod 755 /etc/libvirt/hooks/custom/all_CL-Custom-Hooks-Qemu.sh"
  end

  desc 'logrotate.d配下にlibvirtd.qemu-hookの配置・ログフォルダー作成'
  task :"put_hook_log_conf", hookscript_cmd_filter do
    c_sudo "mkdir -p /var/log/libvirt/qemu-hook_old"
    c_sudo "cp -p #{hookscript_path}/libvirtd.qemu-hook /etc/logrotate.d"
  end

  desc 'agent.propertiesにvm.migrate.speed=0を追記'
  task :"update_agent_properties", hookscript_cmd_filter do
    c_sudo "LANG=C /etc/init.d/cloudstack-agent stop; true"
    c_sudo "sed -i '/vm.migrate.speed/d' /etc/cloudstack/agent/agent.properties && echo 'vm.migrate.speed=0' >> /etc/cloudstack/agent/agent.properties;true"
    c_sudo "LANG=C /etc/init.d/cloudstack-agent start; true"
  end

  desc 'logrotate.d配下のcloudstack-agentバックアップファイル移動'
  task :"mv_cloudstack_agent_bakupfile", hookscript_cmd_filter do
    servers = find_servers_for_task(current_task)
    servers.each do |current_server|
      options = {:hosts => current_server.host}
      ans = capture("sudo find /etc/logrotate.d -type f | grep 'cloudstack-agent_[0-9]'; true", options)
      do_except_host(current_server.host) if ans.empty?
    end
    c_sudo "mv /etc/logrotate.d/cloudstack-agent_[0-9]* #{hookscript_path}"; do_release_host()
  end
end

namespace(:hookscript_rollback) do
  desc 'フックスクリプト切り戻し評価'
  task :"score", acs49_target do

     columns = ['sbHook','clHook','hookLog','agtcnf','agtstats']
     # ホストごとに処理
     caputer_servers(columns) do |current_server, score|
       options = {:hosts => current_server.host}

       # check SB hook
       score['sbHook'] = capture_match(
         "sudo find /etc/libvirt/hooks/qemu -type f | xargs md5sum | grep #{old_hook_rollback_md5}; true", true, options)

       # check CL hook
       score['clHook'] = capture_unmatch(
         "sudo find /etc/libvirt/hooks -type d | grep -w '/etc/libvirt/hooks/custom'; true", true, options)

       # check hookscript logrotate
       score['hookLog'] = capture_unmatch(
         "sudo find /etc/logrotate.d -type f | grep -w '/etc/logrotate.d/libvirtd.qemu-hook';sudo find /var/log/libvirt -type d | grep -w '/var/log/libvirt/qemu-hook_old' ;true", "/etc/logrotate.d/libvirtd.qemu-hook\r\n/var/log/libvirt/qemu-hook_old", options)

       # check agent.properties 
       score['agtcnf'] = capture_unmatch(
         "sudo cat /etc/cloudstack/agent/agent.properties | grep -w vm.migrate.speed=0 ;true", true, options)

       # check cloudstack-agent status 
       score['agtstats'] = capture_match(
         "sudo LANG=C /etc/init.d/cloudstack-agent status | grep 'cloudstack-agent (pid  [0-9]*) is running...' ;true", true, options)
    end
  end

  desc 'フックスクリプト切り戻し'
  task :"all", acs49_target do
    start_commands(:task_namespace) do
      hookscript_rollback.return_oldhook();                    log_ok "return_oldhook"
      hookscript_rollback.del_clhook();                        log_ok "del_clhook"
      hookscript_rollback.del_hook_log_conf();                 log_ok "del_hook_log_conf"
      hookscript_rollback.rollback_agent_properties();         log_ok "rollback_agent_properties"
    end
  end

  desc '旧フックスクリプト戻し'
  task :"return_oldhook", hookscript_cmd_filter do
    c_sudo "find #{hookscript_path} -type f | grep 'qemu_[0-9]' | xargs md5sum | grep #{old_hook_rollback_md5} | awk -F ' ' '{print $2}' | xargs -I{} mv {} /etc/libvirt/hooks/qemu"
  end

  desc 'CLフックスクリプト削除'
  task :"del_clhook", hookscript_cmd_filter do
    c_sudo "rm -rf /etc/libvirt/hooks/custom"
  end

  desc 'logrotate.d配下のlibvirtd.qemu-hook・ログフォルダー削除'
  task :"del_hook_log_conf", hookscript_cmd_filter do
    c_sudo "rm -f /etc/logrotate.d/libvirtd.qemu-hook && rm -rf /var/log/libvirt/qemu-hook_old"
  end

  desc 'agent.propertiesからvm.migrate.speed=0を削除'
  task :"rollback_agent_properties", hookscript_cmd_filter do
    c_sudo "LANG=C /etc/init.d/cloudstack-agent stop; true"
    c_sudo "sed -i '/vm.migrate.speed/d' /etc/cloudstack/agent/agent.properties;true"
    c_sudo "LANG=C /etc/init.d/cloudstack-agent start; true"
  end
end

# 条件と一致しないホストは処理できないようなフラグを設定する。
def do_except_host(except_host)
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    srv.options[:skip_host] = true if srv.host == except_host
  end
end

# スキップ対象ホストを正常ホストに戻し全ホスト処理続行可能とする。
def do_release_host()
  servers = current_task.namespace.roles[:targets]
  servers.each do |srv|
    srv.options.delete(:skip_host)
  end
end
