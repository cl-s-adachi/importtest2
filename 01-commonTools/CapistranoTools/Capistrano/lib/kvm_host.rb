# coding: utf-8

# 使用する際はcapistranoのオプション -S を指定すること。指定したファイルが対象となる
# sampleでは-Sの変数fileに指定されたファイルを転送する
# sample command ) 
#   cap <ENV_NAME> kvm_host:upload_file -S src_path=upload/acs4.9_upgrade_kvm_host/etc/cloudstack/agent/log4j-cloud.xml -S dest_path="/tmp" HOSTS=kcps-dev
#   cap <ENV_NAME> kvm_host:agent:check_md5sum -S hash="d8e8fca2dc0f896fd7cb4cb0031ba249" HOSTS=kcps-dev
#   cap <ENV_NAME> kvm_host:agent:backup_log4j_conf HOSTS=kcps-dev
#   cap <ENV_NAME> kvm_host:agent:rollback_log4j_conf HOSTS=kcps-dev
namespace(:kvm_host) do
  desc 'ファイル転送'
  task :upload_file, :roles => [:targets] do
    puts fetch(:src_path)
    puts fetch(:dest_path)
    do_upload_file(src_path, dest_path)
  end

  namespace(:agent) do
    set :log4j_conf, "/etc/cloudstack/agent/log4j-cloud.xml"
    set :agent_log, "/var/log/cloudstack/agent/agent.log"
    set :tmp_dir, "/tmp"

    desc 'log4j-cloud.xmlをアップロード'
    task :upload_log4j_conf, :roles => [:targets] do
      tmp_file_path = "#{tmp_dir}/log4j-cloud.xml"
      src_path = fetch(:src_path, nil)
      if src_path.nil?
        puts("ERROR: paramter(\"src_path\") is missing")
        exit 1
      end
        
      do_upload_file(src_path, tmp_file_path)
      c_sudo "mv #{tmp_file_path} #{log4j_conf}"
      c_sudo "chown root.root #{log4j_conf}"
      c_sudo "ls -al #{log4j_conf}"
    end

    desc 'log4j-cloud.xmlをバックアップ'
    task :backup_log4j_conf, :roles => [:targets] do
      backup_file = fetch(:backup_file, "#{log4j_conf}." + today())
      c_sudo "cp #{log4j_conf} #{backup_file}"
    end

    desc 'log4j-cloud.xmlをロールバック'
    task :rollback_log4j_conf, :roles => [:targets] do
      backup_file = fetch(:backup_file, "#{log4j_conf}." + today()) 
      c_sudo "cp #{backup_file} #{log4j_conf}"
    end

    desc 'MD5SUM 確認'
    task :check_md5sum, :roles => [:targets] do
      file = fetch(:check_file, log4j_conf)
      val_expected = fetch(:hash)

      run("md5sum #{file}") do |channel, stream, data|
        hostname = channel[:host] 
        if stream == :out
          val = data.split()[0]
          if val == val_expected
            puts("#{hostname}: #{green('OK')}")
          else
            puts("#{hostname}: #{red('NG')}")
          end
        else
          puts("#{hostname}: NG : \"#{val_expected}\" expected but \"#{val}\" returned.")
        end
      end
    end

    desc 'agent.log確認(DEBUG)'
    task :check_debug_log, :roles => [:targets] do
      count = fetch(:count, 100)
      grep_file("DEBUG", agent_log, count)
    end

    desc 'agent.log確認(INFO)'
    task :check_info_log, :roles => [:targets] do
      count = fetch(:count, 1)
      grep_file("INFO", agent_log, count)
    end

    desc 'grep実行'
    task :grep, :roles => [:targets] do
      word = fetch(:word, nil)
      file = fetch(:file, nil)
      if word.nil? || file.nil?
        puts("ERROR: paramter is missing")
        exit 1
      end  
      grep_file(word, file, nil)
    end

    desc 'tail実行'
    task :tail, :roles => [:targets] do
      file = fetch(:file, nil)
      count = fetch(:count, 10)
      if file.nil?
        puts("ERROR: paramter is missing")
        exit 1
      end  
        
      c_sudo("tail -#{count} #{file}")
    end

  end

  def grep_file(str, file_path, tail_count=nil)
    if tail_count.nil?
        cmd = "cat #{file_path}"
    else
        cmd = "tail -#{tail_count} #{file_path}"
    end

    run("#{cmd} | grep #{str} > /dev/null && echo \"OK\" || echo \"NG\"") do |channel, stream, data|
      hostname = channel[:host]
      if stream == :out
        val = data.split()[0]
        if val == "OK"
          puts("#{hostname}: #{green('OK')}")
        else
          puts("#{hostname}: #{red('NG')}")
        end
      else
        puts("#{hostname}: #{red('NG')}: Error occured while executing grep.")
      end
    end

  end

  def today()
    return Time.now().strftime("%Y%m%d")
  end

  def do_upload_file(src_path, dest_path)
    puts "uploading #{src_path} to #{dest_path}"
    upload(src_path, dest_path, :via => :scp)
  end

  def copy_file(src_path, dest_path)
    puts "copying #{src_path} to #{dest_path}"
    c_sudo "cp #{src_path} #{dest_path}"
  end

  def green(str)
    return "\e[32m" + str + "\e[0m"
  end

  def red(str)
    return "\e[31m" + str + "\e[0m"
  end

end
