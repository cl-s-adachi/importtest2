# coding: utf-8

## ACS4.9

namespace(:libvirtd) do
  ## Valiables
  # show line logs count
  set :line_number, 10

  desc 'libvirtを再起動します'
  task :"restart", :roles=>[:targets] do
     header()
     unless skip.nil?
       sudo "/etc/init.d/libvirtd restart"
     end
  end

  desc 'libvirtの停止'
  task :"stop", :roles=>[:targets] do
     header()
     unless skip.nil?
       #sudo "/etc/init.d/libvirtd stop"
       cmd = "/etc/init.d/libvirtd status;true"
       status = "stopped"
       task = "/etc/init.d/libvirtd stop"
       do_daemon_start_stop(cmd, status, task)
     end
  end

  desc 'libvirtを起動します'
  task :"start", :roles=>[:targets] do
     header()
     unless skip.nil?
       #sudo "/etc/init.d/libvirtd start"
       cmd = "/etc/init.d/libvirtd status;true"
       status = "running"
       task = "/etc/init.d/libvirtd start"
       do_daemon_start_stop(cmd, status, task)
     end
  end

  desc 'libvirtのバージョンを表示します'
  task :"version", :roles=>[:targets] do
     header()
     sudo "rpm -qa | grep libvirt; exit 0"
  end

  desc 'libvirtのステータスを確認します'
  task :"status", :roles=>[:targets] do
     header()
     sudo "/etc/init.d/libvirtd status;exit 0"
  end

  desc 'libvirtのステータス確認'
  task :"status_check_running", :roles=>[:targets] do
     header()
     unless skip.nil?
       cmd = "/etc/init.d/libvirtd status;true"
       do_check(cmd, "running")
     end
  end

  desc 'libvirtのステータス確認'
  task :"status_check_stopped", :roles=>[:targets] do
     header()
     unless skip.nil?
       cmd = "/etc/init.d/libvirtd status;true"
       do_check(cmd, "stopped")
     end
  end

  desc 'libvirtのログを表示します'
  task :"libvirt_log", :roles=>[:targets] do
     header()
     sudo "tail -n #{line_number} /var/log/libvirt/libvirtd.log"
  end
end
