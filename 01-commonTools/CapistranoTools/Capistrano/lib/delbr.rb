namespace(:delbr) do

   desc 'Remove bridge name cloudVirBr'
   task :"delbr_cloudVir", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/delbr_cloudVirBr.sh", "/root", :via => :scp
      c_sudo "/root/delbr_cloudVirBr.sh"
      c_sudo "rm -f /root/delbr_cloudVirBr.sh"
   end

   desc 'Remove bridge name cloudVirBr'
   task :"delbr_brbond", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/delbr_brbond1.sh", "/root", :via => :scp
      c_sudo "/root/delbr_brbond1.sh"
      c_sudo "rm -f /root/delbr_brbond1.sh"
   end
end
