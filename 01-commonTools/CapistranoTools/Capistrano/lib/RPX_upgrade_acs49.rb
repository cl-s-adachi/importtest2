# coding: utf-8

set :rpx_backup_path, '/root/acs'
set :rpx_upgrade_packages_base_path, '/root/acs4.9_rpx_packages'

namespace(:rpx_upgrade_acs49) do
   ## Valiables
   set :rpx_upload_base, './upload/'
   set :rpx_upload_zip, 'acs49_rpx.zip'
   set :rpx_upload_files_root, 'acs4.9_rpx_packages'

   desc 'リバースプロキシ-アップグレード'
   task :"all", acs49_target do
      start_commands do
         rpx_upgrade_acs49.upload_file();       log_ok "upload_file"
         rpx_upgrade_acs49.backup_confings();   log_ok "backup_confings"
         rpx_common_acs49.stop_service();       log_ok "stop_service"
         rpx_upgrade_acs49.rewrite_confings();  log_ok "rewrite_confings"
         rpx_common_acs49.start_service();      log_ok "start_service"
      end
   end

   ## Tasks
   desc '必要ファイルのアップロード'
   task :"upload_file", acs49_cmd_filter do
      local = File.expand_path(Pathname.new(rpx_upload_base).join(rpx_upload_zip))
      remote = Pathname.new(user_home).join(rpx_upload_zip)
      top.upload(local, user_home, :via=>:scp)
      c_run "unzip -o #{remote} && rm -f #{remote}"
      c_su "rsync -a #{rpx_upload_files_root} /root/"
      c_run "rm -rf #{rpx_upload_files_root}"
   end

   desc 'Backup configuration file'
   task :"backup_confings", acs49_cmd_filter do
      c_su "mkdir -p #{rpx_backup_path}/bu-etc-httpd-confd/"
      c_su "rsync -a /etc/httpd/conf.d/ #{rpx_backup_path}/bu-etc-httpd-confd"
   end

   desc 'Rewrite IP address for new AP server'
   task :"rewrite_confings", acs49_cmd_filter do
      # ホスト名($HOSTNAME)でコピー元ファイルを切り替えてファイルをコピー
      c_su "cp -f #{rpx_upgrade_packages_base_path}/$HOSTNAME/etc/httpd/conf.d/proxy_admin.conf /etc/httpd/conf.d/proxy_admin.conf"
      c_su "cp -f #{rpx_upgrade_packages_base_path}/$HOSTNAME/etc/httpd/conf.d/proxy.conf /etc/httpd/conf.d/proxy.conf"
   end
end

namespace(:rpx_check_acs49) do
   ## Valiables

   desc 'リバースプロキシ-確認'
   task :"all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         rpx_check_acs49.service_status();    log_ok "service_status"
         rpx_common_acs49.confirm_configs();  log_ok "confirm_configs"
         rpx_check_acs49.proxy_admin_config();log_ok "proxy_admin_config"
         rpx_check_acs49.proxy_config();      log_ok "proxy_config"
      end
   end

   ## Tasks
   desc 'Service Status'
   task :"service_status", acs49_cmd_filter do
      c_su "/etc/init.d/httpd status; true"
   end
   desc 'Cat proxy_admin.conf ip settings'
   task :"proxy_admin_config", acs49_cmd_filter do
      c_run "grep -E '^<VirtualHost\\s+|^ServerName\\s+|^ProxyPass\\s+|^ProxyPassReverse\\s+' /etc/httpd/conf.d/proxy_admin.conf"
   end
   desc 'Cat proxy.conf ip settings'
   task :"proxy_config", acs49_cmd_filter do
      c_run "grep -E '^<VirtualHost\\s+|^ServerName\\s+|^ProxyPass\\s+|^ProxyPassReverse\\s+' /etc/httpd/conf.d/proxy.conf"
   end
end

namespace(:rpx_cutback_acs49) do
   ## Valiables

   desc 'リバースプロキシ-切り戻し'
   task :"all", acs49_target do
      start_commands do
         rpx_cutback_acs49.upload_file();       log_ok "upload_file"
         rpx_common_acs49.stop_service();       log_ok "stop_service"
         rpx_cutback_acs49.rewrite_confings();  log_ok "rewrite_confings"
         rpx_common_acs49.start_service();      log_ok "start_service"
      end
   end

   ## Tasks
   desc 'アップロードファイルの削除'
   task :"upload_file", acs49_cmd_filter do
      c_su "rm -rf #{rpx_upgrade_packages_base_path}; true"
   end

   desc 'Rewrite IP address for old AP server'
   task :"rewrite_confings", acs49_cmd_filter do
      # 復元ファイルの属性・タイムスタンプを維持する
      c_su "cp -fp #{rpx_backup_path}/bu-etc-httpd-confd/proxy_admin.conf /etc/httpd/conf.d/proxy_admin.conf"
      c_su "cp -fp #{rpx_backup_path}/bu-etc-httpd-confd/proxy.conf /etc/httpd/conf.d/proxy.conf"
   end
end

namespace(:rpx_common_acs49) do

   desc 'Confirm configuration file'
   task :"confirm_configs", acs49_cmd_filter do
      c_su "ls -l /etc/httpd/conf.d/proxy_admin.conf"
      c_su "ls -l /etc/httpd/conf.d/proxy.conf"
   end

   desc 'Service Config Test'
   task :"service_configtest", acs49_cmd_filter do
      c_su "/etc/init.d/httpd configtest"
   end

   desc 'Stop Service'
   task :"stop_service", acs49_cmd_filter do
      # httpd statusはサービスが停止している場合、異常値を返す
      c_su "/etc/init.d/httpd status; true"
      c_su "/etc/init.d/httpd stop"
      c_su "/etc/init.d/httpd status; true"
   end

   desc 'Start Service'
   task :"start_service", acs49_cmd_filter do
      # httpd statusはサービスが停止している場合、異常値を返す
      c_su "/etc/init.d/httpd status; true"
      c_su "/etc/init.d/httpd start"
      c_su "/etc/init.d/httpd status; true"
   end
end
