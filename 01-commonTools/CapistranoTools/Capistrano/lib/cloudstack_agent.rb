# coding: utf-8

## ACS4.9

namespace(:cloudstack_agent) do
  ## Valiables
  # show line logs count
  set :line_number, 10

  desc 'Agentを再起動します'
  task :"restart", :roles=>[:targets] do
     sudo "/etc/init.d/cloudstack-agent restart"
  end

  desc 'Agentを停止します'
  task :"stop", :roles=>[:targets] do
     sudo "/etc/init.d/cloudstack-agent stop"
  end

  desc 'Agentを起動します'
  task :"start", :roles=>[:targets] do
     sudo "/etc/init.d/cloudstack-agent start"
  end

  desc 'Agentのバージョンを表示します'
  task :"version", :roles=>[:targets] do
    sudo "rpm -qa | grep cloudstack-agent-[0-9]; exit 0"
  end

  desc 'Agentのステータスを確認します'
  task :"status", :roles=>[:targets] do
     sudo "/etc/init.d/cloudstack-agent status ; exit 0"
  end

  desc 'Agentのログを表示します'
  task :"cloud-agent-log", :roles=>[:targets] do
     sudo "tail -n #{line_number} /var/log/cloudstack/agent/agent.log"
  end
end