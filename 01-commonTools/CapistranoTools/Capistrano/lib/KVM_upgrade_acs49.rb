# coding: utf-8
## Tasks

# CloudStack3:   /etc/cloud/agent
#                /etc/init.d/cloud-agent
# CloudStack4.9: /etc/cloudstack/agent
#                /etc/init.d/cloudstack-agent

set :upgrade_packages_base_path, '/root/acs4.9_upgrade_packages'
set :upgrade_backup_path, '/root/acs'

namespace(:kvm_upgrade_acs49) do
   ## Valiables

   desc 'アップグレード'
   task :"all", acs49_target do
      start_commands do
         kvm_upgrade_acs49.shutdown_kvm_agent();                    log_ok "shutdown_kvm_agent"
         kvm_upgrade_acs49.backup_kvm_agent_settings();             log_ok "backup_kvm_agent_settings"
         kvm_upgrade_acs49.install_dependencies();                  log_ok "install_dependencies"
         kvm_upgrade_acs49.set_openjdk_as_default();                log_ok "set_openjdk_as_default"
         kvm_upgrade_acs49.install_cloudstack_agent_packages();     log_ok "install_cloudstack_agent_packages"
         ##2018.03.19追記
         kvm_upgrade_acs49.snooping_fix();                          log_ok "snooping_fix"
         kvm_upgrade_acs49.patch_cloudstack_agent_initd();          log_ok "patch_cloudstack_agent_initd"
         kvm_upgrade_acs49.patch_cloudstack_agent_env_properties(); log_ok "patch_cloudstack_agent_env_properties"
         kvm_upgrade_acs49.patch_cloudstack_agent_properties();     log_ok "patch_cloudstack_agent_properties"
         kvm_upgrade_acs49.change_permissions_root_ssh();           log_ok "change_permissions_root_ssh"
         kvm_upgrade_acs49.start_cloudstak_agent();                 log_ok "start_cloudstak_agent"
      end
   end

   desc 'Shut down the KVM agent'
   task :"shutdown_kvm_agent", acs49_cmd_filter do
      c_sudo "/etc/init.d/cloud-agent stop"
      # jsvcのプロセスが存在しない場合があるため失敗しても無視する
      c_sudo "killall jsvc; true"
      c_sudo 'chkconfig cloud-agent off'
   end

   desc 'Back up the existing KVM agent settings'
   task :"backup_kvm_agent_settings", acs49_cmd_filter do
      c_sudo "mkdir -p #{upgrade_backup_path}"
      c_sudo "mkdir -p #{upgrade_backup_path}/bu-etc-init.d/"
      c_sudo "rsync -a /etc/init.d/cloud-agent #{upgrade_backup_path}/bu-etc-init.d/"
      c_sudo "rsync -a /etc/cloud/ #{upgrade_backup_path}/bu-etc-cloud"
      c_sudo "rsync -a /etc/cloud/agent/ #{upgrade_backup_path}/bu-etc-cloud-agent"
   end

   desc 'Install dependencies'
   task :"install_dependencies", acs49_cmd_filter do
      # c_sudo "yum -y #{yum_opts} #{yum_repos} install bridge-utils ebtables ethtool jakarta-commons-daemon jakarta-commons-daemon-jsvc libvirt libvirt-python qemu-img qemu-kvm vconfig"
      # qemu-img qemu-kvm はインストールしない。
      c_sudo "yum -y #{yum_opts} #{yum_repos} install bridge-utils ebtables ethtool jakarta-commons-daemon jakarta-commons-daemon-jsvc libvirt libvirt-python vconfig"
   end

   desc 'Set OpenJDK 1.8 as default'
   task :"set_openjdk_as_default", acs49_cmd_filter do
      c_sudo "/usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java"
   end

   desc 'Install the new Cloudstack-agent packages'
   task :"install_cloudstack_agent_packages", acs49_cmd_filter do

      # インストール済みかを確認
      ans = capture "rpm -q cloudstack-agent; true"
      if ans.include?("not installed") || ans.include?("インストールされていません") then
         # リトライ3回で取得を試みる。メッセージは簡略化する。
         c_sudo "wget --tries=3 --timeout=30 --no-verbose #{cloudstack_repo_address}/release.asc"
         c_sudo "rpm --import release.asc"
         # /etc/cloud.rpmsave があったら、/etc/cloud.rpmsave-old にリネームする。
         ans2 = capture "ls /etc/cloud.rpmsave; true"
         if ans2.include?("agent") then
            "sudo /bin/mv /etc/cloud.rpmsave /etc/cloud.rpmsave-old"
         end
         c_sudo "rpm -i --nodeps #{cloudstack_repo_address}/cloudstack-common-4.9.1.0-shapeblue0.el6.x86_64.rpm"
         c_sudo "rpm -i --nodeps #{cloudstack_repo_address}/cloudstack-agent-4.9.1.0-shapeblue0.el6.x86_64.rpm"
      else
         c_sudo "mv #{upgrade_backup_path}/rollback-cloudstackagent /etc/cloudstack/agent"
         c_sudo "/usr/bin/cloudstack-agent-upgrade"
      end
   end

   ##2018.03.19追記
   # multicast_snooping の値に "0" 以外が存在したら "0" に書き換える
   desc 'Fix snooping setting'
   task :"snooping_fix", acs49_cmd_filter do
      snooping_brbond = capture "cat /sys/class/net/brbond*/bridge/multicast_snooping | grep '1'; true"
         if snooping_brbond.include?("1") then
            top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_fix.sh", "/root", :via => :scp
            c_sudo "/root/snooping_fix.sh"
            c_sudo "rm -f /root/snooping_fix.sh"
         end
    end



   desc 'Patch the cloudstack-agent init.d script (/etc/init.d/cloudstack-agent) to stop searching for the java executable:'
   task :"patch_cloudstack_agent_initd", acs49_cmd_filter do
      # ファイル上書きで実施
      c_sudo "cp -f #{upgrade_packages_base_path}/etc/init.d/cloudstack-agent /etc/init.d/cloudstack-agent"
   end

   desc 'Verify that the following line exists in /etc/cloudstack/agent/environment.properties, and if not present add this:'
   task :"patch_cloudstack_agent_env_properties", acs49_cmd_filter do
      # ファイルへの追記で実施
      cmd = "cat /etc/cloudstack/agent/environment.properties |grep paths.script; true"
      c_sudo cmd
      out = capture cmd
      if out.length == 0 then
         c_sudo "echo paths.script=/usr/share/cloudstack-common >> /etc/cloudstack/agent/environment.properties"
      else
         puts "[OK] paths.script exists"
      end
   end

   desc 'Ensure that the host field in /etc/cloudstack/agent/agent.properties is pointed to the correct management server IP address.'
   #desc 'Update the “com.cloud.agent.resource.computing.LibvirtComputingResource” agent.properties  as follows:'
   task :"patch_cloudstack_agent_properties", acs49_cmd_filter do
      # ファイル変更で実施
      c_sudo "sed -i 's/^host=.*$/host=#{management_ip_address}/' /etc/cloudstack/agent/agent.properties"
      c_sudo "sed -i 's/com.cloud.agent.resource.computing.LibvirtComputingResource/com.cloud.hypervisor.kvm.resource.LibvirtComputingResource/' /etc/cloudstack/agent/agent.properties"
   end

   desc 'Change permissions on the /root/.ssh/ folder:'
   task :"change_permissions_root_ssh", acs49_cmd_filter do
      c_sudo "mkdir -p /root/.ssh/"
      c_sudo "chmod 0700 /root/.ssh/"
   end

   desc 'Start the agent:'
   task :"start_cloudstak_agent", acs49_cmd_filter do
      c_sudo "date; /etc/init.d/cloudstack-agent start"
   end
end

namespace(:kvm_check_acs49) do

   desc 'KVM-評価'
   task :"score", acs49_target do

      columns = ['javap','jsvcp','javav','agtcnf','dirp','agtlog','chkconfig','BrName','snooping']
      # ホストごとに処理
      caputer_servers(columns) do |current_server, score|
         options = {:hosts => current_server.host}

         # java process
         score['javap'] = capture_match(
            "ps afxwww |grep -v grep | grep -v classpath |grep java; true", true, options)

         # jsvc process
         score['jsvcp'] = capture_match(
            "ps afxwww |grep -v grep |grep jsvc; true", true, options)

         # java version
         score['javav'] = capture_match(
            "java -version",
            /#{Regexp.escape('openjdk version "1.8.0_77"')}/,
            options)

         # agent.properties settings
         score['agtcnf'] = capture_match(
            "cat /etc/cloudstack/agent/agent.properties |grep com.cloud.hypervisor.kvm.resource.LibvirtComputingResource; true",
            "resource=com.cloud.hypervisor.kvm.resource.LibvirtComputingResource",
            options)

         # directoryy permissions
         score['dirp'] = capture_match(
            "sudo ls -ld /root/.ssh/ | grep drwx------", true, options)

         # cloudstack agent logs
         score['agtlog'] = capture_unmatch(
            "sudo tail /var/log/cloudstack/agent/agent.log", /ERROR/, options)

         # check config
         score['chkconfig'] = capture_match(
            "sudo chkconfig --list cloudstack-agent | grep '0:off' | grep '1:off' | grep '2:off' | grep '3:on' | grep '4:on' | grep '5:on' | grep '6:off'; true", true,
            options)

         # Bridge name check
         top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/br_name_check.sh", "/root", :via => :scp
         score['BrName'] = capture_match(
            "sudo /root/br_name_check.sh | grep 'OK'; true", true, options)
            c_sudo "rm -f /root/br_name_check.sh"

         # multicast_snooping の値をチェックする
         top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_check.sh", "/root", :via => :scp
         score['snooping'] = capture_unmatch(
            "sudo /root/snooping_check.sh | grep 'NG'; true", true, options)
            c_sudo "rm -f /root/snooping_check.sh"
      end
   end

   desc 'ACSv4.9 KVM-状態確認'
   task :"all", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         kvm_check_acs49.java_process();         log_ok "java_process"
         kvm_check_acs49.jsvc_process();         log_ok "jsvc_process"
         kvm_check_acs49.java_version();         log_ok "java_version"
         kvm_check_acs49.agent_properties();     log_ok "agent_properties"
         kvm_check_acs49.permissions_root_ssh(); log_ok "permissions_root_ssh"
         kvm_check_acs49.agent_logs();           log_ok "agent_logs"
         kvm_check_acs49.cloudstack_agent();     log_ok "cloudstack_agent"
         kvm_check_acs49.snooping_check();       log_ok "snooping_check"
         kvm_check_acs49.br_name_check();        log_ok "br_name_check"
      end
   end

   desc 'CSv3.x KVM-状態確認'
   task :"before", acs49_target do
      start_commands(:task_namespace ,Logger::INFO) do
         kvm_check_acs49.java_process();         log_ok "java_process"
         kvm_check_acs49.jsvc_process();         log_ok "jsvc_process"
         kvm_check_acs49.java_version();         log_ok "java_version"
         kvm_check_acs49.cloud_agent();          log_ok "cloud_agent"
         kvm_check_acs49.snooping_check();       log_ok "snooping_check"
         kvm_check_acs49.br_name_check();        log_ok "br_name_check"
      end
   end

   desc 'Check java process'
   task :"java_process", acs49_cmd_filter do
      # grep行を含めない
      c_sudo "ps afxwww |grep -v grep |grep java; true"
   end

   desc 'Check jsvc process'
   task :"jsvc_process", acs49_cmd_filter do
      # grep行を含めない
      c_sudo "ps afxwww |grep -v grep |grep jsvc; true"
   end

   desc 'Check java version'
   task :"java_version", acs49_cmd_filter do
      c_sudo "java -version"
   end

   desc 'Confirm agent.properties settings'
   task :"agent_properties", acs49_cmd_filter do
      c_sudo "cat /etc/cloudstack/agent/agent.properties |grep LibvirtComputingResource; true"
   end

   desc 'Check directoryy permissions'
   task :"permissions_root_ssh", acs49_cmd_filter do
      # ディレクトリが存在し、パーミッションが0700であることを確認
      c_sudo "ls -ld /root/.ssh/ | grep drwx------"
   end

   desc 'Check the cloudstack agent logs'
   task :"agent_logs", acs49_cmd_filter do
      c_sudo "tail /var/log/cloudstack/agent/agent.log"
   end

   desc 'Check the cloudstack agent'
   task :"cloudstack_agent", acs49_cmd_filter do
      c_sudo "rpm -q cloudstack-agent"
      c_sudo "rpm -q cloudstack-common"
      c_sudo 'service cloudstack-agent status; true'
   end

   desc 'Check the cloud agent'
   task :"cloud_agent", acs49_cmd_filter do
      c_sudo 'service cloud-agent status; true'
   end

   desc 'BrName check'
    task :"br_name_check", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/br_name_check.sh", "/root", :via => :scp
      c_sudo "/root/br_name_check.sh"
      c_sudo "rm -f /root/br_name_check.sh"
    end
#   task :"brctl_show", acs49_cmd_filter do
#      c_sudo "brctl show"
#   end

   # multicast_snooping の値をチェックする
   desc 'Check the snooping setting'
   task :"snooping_check", acs49_cmd_filter do
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_check.sh", "/root", :via => :scp
      c_sudo "/root/snooping_check.sh"
      c_sudo "rm -f /root/snooping_check.sh"
   end
end

namespace(:kvm_cutback_acs49) do

   desc 'KVM-切り戻し'
   task :"all", acs49_target do
      start_commands do
         kvm_cutback_acs49.disable_cloudstack_agent();    log_ok "disable_cloudstack_agent"
         kvm_cutback_acs49.cutback_bridge();              log_ok "cutback_bridge"
         kvm_cutback_acs49.restore_agent_files();         log_ok "restore_agent_files"
         kvm_cutback_acs49.reset_openjdk_as_default();    log_ok "reset_openjdk_as_default"
         kvm_cutback_acs49.snooping_fix();                log_ok "snooping_fix"
         kvm_cutback_acs49.enable_cloudstack_agent();     log_ok "enable_cloudstack_agent"
      end
   end

   desc 'Stop and disable cloudstack-agent'
   task :"disable_cloudstack_agent", acs49_cmd_filter do
      # 既に停止している場合、エラーになるので無視する
      c_sudo 'service cloudstack-agent stop; true'
      # c_sudo 'chkconfig cloudstack-agent off; true'
   end

   # ブリッジネームを切り戻す
   desc 'Cutback bridge'
   task :"cutback_bridge", acs49_cmd_filter do
      # 商用環境の場合、/home/ckkcl にする。
      top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/cloud-agent-upgrade-cutback", "/root", :via => :scp
      c_sudo "/root/cloud-agent-upgrade-cutback"
      c_sudo "rm -f /root/cloud-agent-upgrade-cutback"
   end

   desc 'Archive /etc/cloudstack/agent files and restore the former CloudPlatform agent directory'
   task :"restore_agent_files", acs49_cmd_filter do
      c_sudo "rm -rf #{upgrade_backup_path}/rollback-cloudstackagent; true"
      # ACSのインストール途中で失敗した場合にフォルダがない可能性もあるので、エラーは無視する
      c_sudo "mv /etc/cloudstack/agent #{upgrade_backup_path}/rollback-cloudstackagent; true"
      # cp ではディレクトリがある場合、-fが効かないためrsyncで代用
      c_sudo "rsync -a #{upgrade_backup_path}/bu-etc-cloud/ /etc/cloud"
      # rpm -i cloudstack-agent-4.9.1.0-shapeblue0.el6.x86_64.rpmで生成されるCS3.xのバックアップフォルダを削除する
      # 今回実行しない
      # c_sudo "rm -rf /etc/cloud.rpmsave; true"
   end

   desc 'Reset OpenJDK 1.6 as default'
   task :"reset_openjdk_as_default", acs49_cmd_filter do
      c_sudo "/usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.6.0-openjdk.x86_64/bin/java"
   end

   # multicast_snooping の値に "0" 以外が存在したら "0" に書き換える
   desc 'Fix snooping setting'
   task :"snooping_fix", acs49_cmd_filter do
      snooping_cloudVir = capture "cat /sys/class/net/cloudVir*/bridge/multicast_snooping | grep '1'; true"
      if snooping_cloudVir.include?("1") then
         top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_fix.sh", "/root", :via => :scp
         c_sudo "/root/snooping_fix.sh"
         c_sudo "rm -f /root/snooping_fix.sh"
      end
   end

 

   ##2018.03.19追記 切り戻し用
   #multicast_snooping 暫定対応作業切り戻し(kvm_cutback_acs49:allタスクには入れない。)
   # multicast_snooping の値に "1" 以外が存在したら "1" に書き換える
   desc 'Fix snooping setting 1'
   task :"snooping_fix_1", acs49_cmd_filter do
      snooping_brbond = capture "cat /sys/class/net/brbond*/bridge/multicast_snooping | grep '0'; true"
         if snooping_brbond.include?("0") then
            top.upload "/home/ckkcl/common/capistrano/CapistranoTools/Capistrano/scripts/snooping_fix-rollback.sh", "/root", :via => :scp
            c_sudo "/root/snooping_fix-rollback.sh"
            c_sudo "rm -f /root/snooping_fix-rollback.sh"
         end
    end


   desc 'Re-enable and start cloud-agent'
   task :"enable_cloudstack_agent", acs49_cmd_filter do
      c_sudo 'chkconfig cloud-agent on'
      c_sudo 'service cloud-agent start'
   end
end
