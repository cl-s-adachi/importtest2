namespace(:agent_properties_check) do

   # Upgrade 前の agent.properties を各 KVM ホストの/etc/cloud/agent から、/root にコピー。同時にホスト名を prefix につける。
   desc 'Copy agent.properties'
   task :"cp_agent_properties", acs49_cmd_filter do
      c_sudo "cp -f /etc/cloud/agent/agent.properties /root/`hostname | awk -F\".\" '{print $1}'`_agent.properties"
   end
   
   # Upgrade 後の agent.properties を各 KVM ホストの/etc/cloud/agent から、/root にコピー。同時にホスト名を prefix につける。
   desc 'Copy agent.properties'
   task :"cp_agent_properties_49", acs49_cmd_filter do
      c_sudo "cp -f /etc/cloudstack/agent/agent.properties /root/`hostname | awk -F\".\" '{print $1}'`_agent.properties"
   end

   # agent.properties を各 KVM ホストから削除。
   desc 'Remove agent.properties'
   task :"rm_agent_properties", acs49_cmd_filter do       
      c_sudo "rm -f /root/`hostname | awk -F\".\" '{print $1}'`_agent.properties"
   end

   # agent.properties を /tmp に移動。
   desc 'Move agent.properties'
   task :"mv_agent_properties", acs49_cmd_filter do       
      c_sudo "/bin/mv /root/`hostname | awk -F\".\" '{print $1}'`_agent.properties /tmp"
   end
end
