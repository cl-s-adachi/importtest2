# coding: utf-8

##------------------------------------------------------------------------------
## 対象ホストの指定
## 指定方法：同梱のtargetsディレクトリ内のhost.*-*-*.rbファイルを、
##           Capfileと同じ階層に「hosts.rb」としてコピーしてください。
##           その後「cap コマンド名」でコマンドを実行します。
##           コピーしたhostsファイルが正しいか確認する場合は
##           「cap hostname」で取得したホスト名が一致するか確認してください。

#-------------------------------------------------------------------------------

## Commonly used tasks

desc 'ホスト名を表示します'
task :hostname, :roles=>[:targets] do
   run "echo $HOSTNAME"
end

desc 'ホストの時刻を表示します'
task :date, :roles=>[:targets] do
   run "date"
end

desc 'ホストの情報を確認します'
task :"hostinfo", :roles=>[:targets] do
   run "date; uname -n; id"
end

def header(*str)
  if str[0].eql? nil
    puts "#{purple('START TASK')}: #{current_task.desc} Times of Day #{today}"
    output = "#{current_task.desc} Times of Day #{today}"
    custom_logger;info("#{purple('START TASK')}",output)
  else
    custom_logger;info("#{blue('END')}","")
    return "#{str[0]}\n" + "#{blue('END')}\n\n\n"
  end
end

def today()
  return Time.now().strftime("%Y/%m/%d %H:%M:%S")
end

def green(str)
  return "\e[32m" + str + "\e[0m"
end

def red(str)
  return "\e[31m" + str + "\e[0m"
end

def purple(str)
  return "\033[95m" + "\033[4m" + str + "\e[0m"
end

def blue(str)
  return "\033[96m" + "\033[4m" + str + "\e[0m"
end

def skip()
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    if srv.options[:has_error].nil? && srv.options[:error_task].nil? then
      next
    else
      return
    end
  end
end

def do_check(cmd, value)
  c_do do
    sudo(cmd) do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        if cmd.include?("status") && value.include?("running")
          if data.include?("\345\256\237\350\241\214\344\270\255")
            val = value
          elsif data.include?("running")
            val = data.split()[4].gsub("...", "")
          else
            val = data
          end
        elsif cmd.include?("status") && value.include?("stopped")
          if data.include?("\345\201\234\346\255\242")
            val = value
          elsif data.include?("stopped")
            val = data.split()[2]
          else
            val = data
          end
        else
          if cmd.include?("redhat-release") || cmd.include?("chkconfig")
            val = data.chomp!
          else
            val = data.split()[0]
          end
        end
        if val == value
          puts output = ("#{host_name}: #{cmd} => #{green('OK')}")
          custom_logger;info(host_name ,output)
          if cmd.include?("status")
            puts data
            custom_logger;info(host_name ,data)
          else
            puts output = "Expected value: #{value}"
            custom_logger;info(host_name ,output)
            puts output = "Result value:   #{val}"
            custom_logger;info(host_name ,output)
            puts output = "================================================================="
            custom_logger;info(host_name ,output)
          end
        else
          puts output = ("#{host_name}: #{cmd} => #{red('NG')}")
          custom_logger;info(host_name ,output)
          if cmd.include?("status")
            puts data
            custom_logger;info(host_name ,data)
          else
            puts output = "Expected value: #{value}"
            custom_logger;info(host_name ,output)
            puts output = "Result value:   #{val}"
            custom_logger;info(host_name ,output)
            puts output = "================================================================="
            custom_logger;info(host_name ,output)
          end
          set_error_flag()
        end
      elsif stream == :err
        puts "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
  end
end

def do_display(cmd)
  stream_err = Array.new()
  stream_err_display_tasks = ["tmp_task", "rhel69_upgrade", "meltdown_patch", "qemu_install"]
  current_host do |host|
    puts "#{host}: #{cmd}"
    custom_logger;info(host ,cmd)
  end
  c_do do
    sudo(cmd) do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        puts data
        custom_logger;info(host_name ,data)
      elsif stream == :err
        puts output = "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
        if stream_err_display_tasks.any? { |t| current_task.name.to_s.eql? t }
          output.strip!
          stream_err << output.split("\n")
        end
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
    unless stream_err.empty?
      puts "\n\n\n"
      puts "#{cmd} => There was an error."
      puts "Please check the results below."
      puts "************************"
      puts stream_err
      puts "************************"
      sleep 5
    end
  end
end

def do_daemon_start_stop(cmd, status, task)
  c_do do
    sudo(cmd) do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        if status.include?("running")
          if data.include?("\345\256\237\350\241\214\344\270\255")
            val = status
          elsif data.include?("running")
            val = data.split()[4].gsub("...", "")
          else
            val = data
          end
        else
          if data.include?("\345\201\234\346\255\242")
            val = status
          elsif data.include?("stopped")
            val = data.split()[2]
          else
            val = data
          end
        end
        if val == status
          puts output = ("#{host_name}: #{cmd} #{green('OK')}")
          custom_logger;info(host_name ,output)
          puts data
          custom_logger;info(host_name ,data)
          puts output = "Skip Task => #{task}"
          custom_logger;info(host_name ,output)
        else
          puts output = ("#{host_name}: #{cmd} => #{data}")
          custom_logger;info(host_name ,output)
          puts output = "Do Task => #{task}"
          custom_logger;info(host_name ,output)
          do_display(task)
        end
      elsif stream == :err
        puts "#{red("#{stream}")}: #{data}"
        custom_logger;error(host_name ,data)
      else
        puts data
        custom_logger;info(host_name ,data)
      end
    end
  end
end

def do_dir_check(cmd, path, *args)
  count = 0
  begin
    sudo(cmd) do |channel, stream, data|
      host_name = channel[:host]
      if stream == :out
        val = data.split()[0]
        if val == path
          puts output = ("#{host_name}: #{cmd} => #{green('OK')}")
          custom_logger;info(host_name ,output)
          puts output = "Expected value: #{path}"
          custom_logger;info(host_name ,output)
          puts output = "Result value:   #{val}"
          custom_logger;info(host_name ,output)
          puts output = "================================================================="
          custom_logger;info(host_name ,output)
        else
          puts output = ("#{host_name}: #{cmd} => #{red('NG')}")
          custom_logger;info(host_name ,output)
          puts output = "Expected value: #{path}"
          custom_logger;info(host_name ,output)
          puts output = "Result value:   #{val}"
          custom_logger;info(host_name ,output)
          puts output = "================================================================="
          custom_logger;info(host_name ,output)
          set_error_flag()
        end
      end
    end
  rescue
    current_host do |host_name|
      puts output = "************************"
      custom_logger;info(host_name ,output)
      puts output = ("#{host_name}: #{path} is not exists.")
      custom_logger;info(host_name ,output)
      puts output = "It will create a directory in #{host_name}"
      custom_logger;info(host_name ,output)
      puts output = "************************"
      custom_logger;info(host_name ,output)
      cmd1 = "mkdir -p #{path}"
      do_display(cmd1)
    end
    if count <= 1
      count += 1
      retry
    elsif count == 2
      current_host do |host_name|
        puts output = "It could not make a directory"
        custom_logger;error(host_name ,output)
        puts output = "Skip the rest of the processing..."
        custom_logger;error(host_name ,output)
        puts output = "************************"
        custom_logger;error(host_name ,output)
        set_error_flag()
      end
    end
  end
end

def are_you_ok?()
  puts ""
  while true
    res = Capistrano::CLI.ui.ask "Is it correct? [y|n]: "
    case res
    when /^[yY]\z/
      puts "You choose yes!"
      puts "Continue processing..."
      puts "************************"
      return true
    when /^[nN]\z/
      puts "You choose no!"
      puts "Skip the rest of the processing..."
      puts "************************"
      set_error_flag()
      return false
    end
  end
end

def set_error_flag()
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    if srv.host
      #NGのあったサーバにフラグとエラータスク名を設定する
      srv.options[:has_error] = true
      srv.options[:error_task] = current_task.name
      return false
    end
  end
end

def current_host()
  servers = find_servers_for_task(current_task)
  servers.each do |srv|
    yield(srv.host)
  end
end

def custom_logger()
  if log_level.eql?(0)
    def info(*args);end
    alias :error :info
    return
  end
  FileUtils.mkdir_p(log_dir) unless FileTest.exist?(log_dir)
  @input = File.open(log_file,'a')
  @time = Time.now().strftime("%Y:%m:%d %H:%M:%S")
  def info(current_host, res)
    if res.empty?
      @input.puts " ** [out :: #{current_host}] #{res}"
    else
      res.strip!
      res = res.split("\n")
      len = res.length-1
      for i in 0..len do
        @input.puts " ** [out :: #{current_host}] #{res[i]}"
      end
    end
    @input.close
  end
  def error(current_host, res)
    if res.empty?
      @input.puts " ** [err :: #{current_host}] #{res}"
    else
      res.strip!
      res = res.split("\n")
      len = res.length-1
      for i in 0..len do
        @input.puts " ** [err :: #{current_host}] #{res[i]}"
      end
    end
    @input.close
  end
end

alias :footer :header