#!/bin/bash
# このスクリプトは、hosts.rb を生成するツールです。
# createHostRb.py を利用して target ファイルを作成。target ファイルを元にして hosts.rb を生成します。

# 現在の hosts.rb があればバックアップ
if [ -f hosts.rb ]; then
	mkdir -p ./hosts_rb_backup
	cp -p hosts.rb ./hosts_rb_backup/hosts.rb_`date "+%Y%m%d_%H%M%S"`
fi

# ./target ディレクトリ作成。
if [ ! -d ./target ]; then
	mkdir -p ./target
fi

# ./target に hosts.v-*.rb がなかったら、createHostRb.py を実行する。
if [ -z "`ls -l ./target | grep 'hosts.v-'`" ] && [ -z "`ls -l ./target | grep 'hosts.p-'`" ]; then
	(cd ./target; python ../../createHostsRb/createHostRb.py)
fi

# 既存の hosts.rb を削除。
rm -f hosts.rb

# ./target 配下の hosts.v-*.rb ファイルを参照して、hosts.rb ファイルを生成する。
target=`ls -l ./target | grep 'pEbsv' | awk '{print $9}'`
for line in ${target}; do
	# 空行、コメント行は除外、行の末尾に改行コードを追加し、hosts.rb を生成。
	cat ./target/${line} | sed '/^#/d' | sed 's/$/\n/g' | sed '/^$/d' >>hosts.rb
done

# 生成された hosts.rb を出力。
cat hosts.rb
