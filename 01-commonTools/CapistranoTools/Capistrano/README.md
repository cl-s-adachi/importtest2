# ACS4.9対応 OpenJDK/KVM/RPXアップグレード支援ツール

## runCapistrano.sh の使い方 
ACS4.9へのアップグレードを簡易に行えるようにしたスクリプトです。

> $ ./runCapistrano.sh [-t 対象ファイルキーワード] [-e 環境名] アップグレードコマンド ...

にてアップグレードコマンドを実行できます。  
コマンドをスペース区切りにすることで連続して実行が可能です。


コマンド実行例)
> $ ./runCapistrano.sh -d develop openjdk18_upgrade_acs49:all  
> $ ./runCapistrano.sh -t hosts.v-tckktky4 -d local hostname hostinfo


### -t 対象ファイルキーワード

処理したいホスト（ホストファイル単位）を絞り込みたい場合に指定します。  
ホストファイルは ** ./target/*.rb ** に配置しておきます。

本オプションを指定しない場合は、** ./hosts.rbが** 使用されます。

実行時の仕組みとしては、条件に一致したファイルを順次 ./hosts.rbに置き換えて、  
アップグレードコマンドを実行する動作となります。


#### 絞り込み
** createHostsRb\createHostRb.py ** を利用してホストファイルを作成しておくと、

* 全てのファイル
* VALUE/PREMIUM種別
* ゾーン

の粒度で実行単位を絞り込むことができます。  
※仕組みとしてはファイル名の前方一致となります。

##### 1. 全ホストを対象
-t オプションを指定しない
> ./runCapistrano.sh cloud_agent:status

##### 2. VALUEクラスタ全て
hosts.v-を指定
> ./runCapistrano.sh -t hosts.v- cloud_agent:status

##### 3. PREMIUMクラスタ全て
hosts.p-を指定
> ./runCapistrano.sh -t hosts.p- cloud_agent:status

##### 4. 特定ゾーンのVALUEクラスタのみ. 例では東京ゾーン
hosts.hosts.v-tckktky4を指定
> ./runCapistrano.sh -t hosts.hosts.v-tckktky4 cloud_agent:status

##### 5. 特定ゾーンのPREMIUMクラスタのみ. 例では東京ゾーン
hosts.hosts.v-tckktky4を指定
> ./runCapistrano.sh -t hosts.hosts.p-tckktky4 cloud_agent:status


### -e 環境名

環境名には、処理を実行するための環境情報を定義した、環境設定ファイルの主ファイル名を指定します。  
指定する環境設定ファイルは ** ./config/deploy/*.rb ** に配置しておきます。

環境名	|	用途
:-|:-
local	|	開発用（ローカルPC）
local_rpx	|	開発用リバースプロキシ（ローカルPC）
develop	|	アップグレード実施環境
rpx		|	リバースプロキシアップグレード実施環境

※これらのファイルはACS4.9アップグレード用に特化しています。

### アップグレードコマンド
アップグレードコマンドを指定することで、対象となるホストに  

* アップグレード
* 切り戻し
* 結果確認
* その他の操作

を行うことができます。

#### コマンド一覧

#####  JDKアップグレード(KVM事前作業用)
コマンド名	|	用途
:-|:-
openjdk18_upgrade_acs49:all	|	事前作業アップグレード
openjdk18_check_acs49:score	|	アップグレード後の結果確認
openjdk18_check_acs49:all		|	アップグレード後の詳細確認
openjdk18_cutback_acs49:all	|	事前作業の切り戻し

##### KVMアップグレード当日作業用
コマンド名	|	用途
:-|:-
kvm_upgrade_acs49:all	|	当日作業アップグレード
kvm_check_acs49:score	|	アップグレード後の結果確認
kvm_check_acs49:all		|	アップグレード後の詳細確認
kvm_cutback_acs49:all	|	当日作業の切り戻し

##### リバースプロキシ当日作業用
コマンド名	|	用途
:-|:-
rpx_upgrade_acs49:all	|	当日作業アップグレード
rpx_check_acs49:all		|	アップグレード後の詳細確認
rpx_cutback_acs49:all	|	当日作業の切り戻し

*:score機能はありません。

##### その他の操作
対象	|	コマンド名	|	用途
:-|:-|:-
全サーバ|	hostname	|	対象ホストの名称
全サーバ|	hostinfo	|	対象ホストの情報
ACS4.9	|	cloudstack_agent:status	|	cloudstack-agentの状態確認
ACS4.9	|	cloudstack_agent:start	|	cloudstack-agentの起動
ACS4.9	|	cloudstack_agent:stop	|	cloudstack-agentの停止
CS3.x	|	cloud_agent:status	|	cloud-agentの状態確認
CS3.x	|	cloud_agent:start	|	cloud-agentの起動
CS3.x	|	cloud_agent:stop	|	cloud-agentの停止


## アップグレード実行結果の見方

### *_upgrade_acs49:all
実行結果がすべてOKであること。  
ホストごとの実行結果がすべてOKであることを確認してください。

```
Start  - openjdk18_upgrade_acs49:all
OK - upload_file
OK - ca_certificates
OK - java8_jdk
OK - alternatives_java
OK - python_packages
Finish - openjdk18_upgrade_acs49:all
========================
OK tckktky4-pbhpv014.cloud_platform.kddi.ne.jp
OK tckktky4-pbhpv010.cloud_platform.kddi.ne.jp
========================
```

処理に異常が発生した場合は、** NG ** が出力されます。  
異常が発生したホストのみ、そこで処理が停止します。  
正常に処理できたホストは次の処理を実行します。

ホストの結果一覧には、NGになったホストと、NGになった処理名が表示されます。

```
Start  - kvm_upgrade_acs49:all
OK - shutdown_kvm_agent
OK - backup_kvm_agent_settings
OK - install_dependencies
OK - set_openjdk_as_default
OK - install_cloudstack_agent_packages
OK - patch_cloudstack_agent_initd
[OK] paths.script exists
OK - patch_cloudstack_agent_env_properties
OK - patch_cloudstack_agent_properties
************************
failed: "sh -c 'sudo -p '\\''sudo password: '\\'' chmod 0700 /root/.ssh/'" on tckktky4-pbhpv014.cloud_platform.kddi.ne.jp
************************
OK - change_permissions_root_ssh
OK - start_cloudstak_agent
Finish - kvm_upgrade_acs49:all
========================
NG tckktky4-pbhpv014.cloud_platform.kddi.ne.jp change_permissions_root_ssh
OK tckktky4-pbhpv010.cloud_platform.kddi.ne.jp
========================
```

また、すべてのホストが処理に失敗した場合は、** SKIP ** が表示されます。  
SKIPの処理は実行されていません。

```
Start  - kvm_upgrade_acs49:all
OK - shutdown_kvm_agent
OK - backup_kvm_agent_settings
OK - install_dependencies
OK - set_openjdk_as_default
************************
failed: "sh -c 'sudo -p '\\''sudo password: '\\'' mv /root/acs/rollback-cloudstackagent /etc/cloudstack/agent'" on tckktky4-pbhpv014.cloud_platform.kddi.ne.jp,tckktky4-pbhpv010.cloud_platform.kddi.ne.jp
************************
SKIP - install_cloudstack_agent_packages
SKIP - patch_cloudstack_agent_initd
SKIP - patch_cloudstack_agent_env_properties
SKIP - patch_cloudstack_agent_properties
SKIP - change_permissions_root_ssh
SKIP - start_cloudstak_agent
Finish - kvm_upgrade_acs49:all
========================
NG tckktky4-pbhpv014.cloud_platform.kddi.ne.jp install_cloudstack_agent_packages
NG tckktky4-pbhpv010.cloud_platform.kddi.ne.jp install_cloudstack_agent_packages
========================
```

### *_cutback_acs49:all
実行結果がすべてOKであること。  
ホストごとの実行結果がすべてOKであることを確認してください。  
詳細は、_upgrade:all と同様

```
Start  - kvm_cutback_acs49:all
OK - disable_cloudstack_agent
OK - restore_agent_files
OK - reset_openjdk_as_default
OK - enable_cloudstack_agent
Finish - kvm_cutback_acs49:all
========================
OK tckktky4-pbhpv014.cloud_platform.kddi.ne.jp
========================
```

### *_check_acs49:score
アップグレードが行われたホストの状況を簡易的に確認します。  
全ての項目がOKであれば、アップグレード処理は正常に実行されています。
アップグレード実行後に使用します。  

```
[ckkcl@tckktky4-vrhns01 Capistrano]$ cap develop openjdk18_check_acs49:score
Start  - openjdk18_check_acs49:score
Server                                      file CA JDK pyarg ipset
tckktky4-pbhpv014.cloud_platform.kddi.ne.jp  OK  OK  OK   OK    OK
tckktky4-pbhpv010.cloud_platform.kddi.ne.jp  OK  OK  OK   OK    OK
Finish - openjdk18_check_acs49:score
========================
OK tckktky4-pbhpv014.cloud_platform.kddi.ne.jp
OK tckktky4-pbhpv010.cloud_platform.kddi.ne.jp
========================
```

### *_check_acs49:all
アップグレードが行われたホストの状況を詳しく知りたいときに確認します。  

```
Start  - kvm_check_acs49:all
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] 3301 ?        Sl     3:50  \_ ../jre/bin/java -classpath ../jre/lib/rt.jar:../jre/lib/jsse.jar:../jre/lib/jce.jar:mail.jar:Framework.jar -Djava.library.path=. Framework.FrameworkManager
...
OK - java_process
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] 10816 ?        Ss     0:00 jsvc.exec -Djava.io.tmpdir=/usr/share/
...
OK - jsvc_process
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] openjdk version "1.8.0_77"
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] OpenJDK Runtime Environment (build 1.8.0_77-b03)
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] OpenJDK 64-Bit Server VM (build 25.77-b03, mixed mode)
OK - java_version
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] resource=com.cloud.hypervisor.kvm.resource.LibvirtComputingResource
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] LibvirtComputingResource.id=327
OK - agent_properties
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] drwx------ 2 root root 4096  4月  7 17:58 2017 /root/.ssh/
OK - permissions_root_ssh
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] 2017-04-07 18:33:52,212 INFO  [cloud.agent.Agent] (agentRequest-Handler-3:null) (logid:392d063a) Ready command is processed: agent id = 327
...
OK - agent_logs
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] cloudstack-agent-4.9.1.0-shapeblue0.el6.x86_64
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] cloudstack-common-4.9.1.0-shapeblue0.el6.x86_64
 ** [out :: tckktky4-pbhpv014.cloud_platform.kddi.ne.jp] cloudstack-agent (pid  10817) を実行中...
OK - cloudstack_agent
Finish - kvm_check_acs49:all
========================
OK tckktky4-pbhpv014.cloud_platform.kddi.ne.jp
========================
```


## 参考情報

### Capistrano直接実行
Capistranoでアップデートコマンドを直接実行したい場合は、以下のようにします。

> $ cap 環境名 アップグレードコマンド

対象ホストはhosts.rbに記述されたtargetsを対象とします。

ex)
> $ cap develop openjdk18_upgrade_acs49:all  
> $ cap local hostname



## KVMホストのAgent log4j設定更新
KVMホストの **/etc/cloudstack/agent/log4j-cloud.xml** を差し替えるためのスクリプトです。

### log4j-cloud.xmlアップロード
**/etc/cloudstack/agent/log4j-cloud.xml** をアップロードします。

* 実行例

    ```
    $ cap <環境名> kvm_host:agent:upload_log4j_conf -S src_path=<ファイルパス>
    ```

    * **src_path:** (必須)アップロードするファイル
* 実行例

    ```
    $ cap production kvm_host:agent:upload_log4j_conf -S src_path="upload/acs4.9_upgrade_kvm_host/etc/cloudstack/agent/log4j-cloud.xml"
    ```


### log4j-cloud.xmlバックアップ
**/etc/cloudstack/agent/log4j-cloud.xml** をバックアップします。

* 実行例

    ```
    $ cap <環境名> kvm_host:agent:backup_log4j_conf -S backup_file=<ファイルパス>
    ```

    * **backup_file:** (オプション)バックアップ先のファイル名。
    * **backup_file** が指定されない場合は */etc/cloudstack/agent/log4j-cloud.xml.YYYYMMDD* に保存される。(YYYYMMDDは実行時の日付)


### log4j-cloud.xmlロールバック
**/etc/cloudstack/agent/log4j-cloud.xml** をロールバックします。

* 実行例

    ```
    $ cap <環境名> kvm_host:agent:rollback_log4j_conf -S backup_file=<ファイルパス>
    ```

    * **backup_file:** (オプション)復元するファイル名。(YYYYMMDDは実行時の日付)



### DEBUG出力確認
**/var/log/cloudstack/agent/agent.log** に *DEBUG* の文字列が含まれているかチェックします。
デフォルトでは *agent.log* ファイルの最後 **100行** のみを対象にします。
**count** パラメータを指定することでgrepの対象行数を指定することができます。

* 実行方法

    ```
    $ cap <環境名> kvm_host:agent:check_debug_log
    ```

    * **count:** (オプション)tailする行数
* **agent.log** の最後の2行を対象にする場合

    ```
    $ cap <環境名> kvm_host:agent:check_debug_log -S count=2
    ```


### INFO出力確認
**/var/log/cloudstack/agent/agent.log** に **INFO** の文字列が含まれているかチェックします。
デフォルトでは **agent.log** ファイルの最後 **1行** のみを対象にします。
**count** パラメータを指定することでgrepの対象行数を指定することができます。

* 実行方法 

    ```
    $ cap <環境名> kvm_host:agent:check_info_log -S count=<tailする行数>
    ```

    * **count:** (オプション)tailする行数

* **agent.log** の最後の50行を対象にする場合

    ```
    $ cap <環境名> kvm_host:agent:check_info_log -S count=50
    ```


### 指定したファイルをtailする
指定したファイルに対してtailを実行します。


* 実行方法 

    ```
    $ cap <環境名> kvm_host:agent:tail -S file=<ファイルパス> -S count=<tailする行数>
    ```

    * **file:** (必須)tailする対象のファイル
    * **count:** (オプション)tailする行数

* **/var/log/cloudstack/agent/agent.log** の最後の50行を対象にする場合

    ```
    $ cap <環境名> kvm_host:agent:tail -S file="/var/log/cloudstack/agent/agent.log" -S count=50
    ```


### log4j-cloud.xmlのmd5sumを確認
**/etc/cloudstack/agent/log4j-cloud.xml** のmd5sumを確認する。


* 実行方法

    ```
    $ cap <環境名> kvm_host:agent:check_md5sum -S hash=<比較するmd5sum値>
    ```

    * **hash:** (必須)hashに設定された値と同じかどうかチェック

