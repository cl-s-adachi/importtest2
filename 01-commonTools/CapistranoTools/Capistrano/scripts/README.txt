このディレクトリにあるスクリプトは Capistrano で使用するので、削除しないこと。

cloud-agent-upgrade-cutback
ブリッジネームを cloudVirBr + (VLAN ID) にリネームします。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

cloudstack-agent-upgrade
ブリッジネームを brbond1- + (VLAN ID) にリネームします。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

delbr_brbond1.sh
brbond1 が存在していたら、消すツール。※ 今回使用しない。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

delbr_cloudVirBr.sh
cloudVirBr が存在していたら、消すツール。※ 今回使用しない。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

br_name_check.sh
CloudStack Ver に適したブリッジネームになっているかチェックするツール。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

snooping_check.sh
snooping の値をチェックするツール。
Capistrano で使用しない場合、Host に直接配置することで実行できます。

snooping_fix.sh
snooping の値が "0" の時 "1" に直すツール。
Capistrano で使用しない場合、Host に直接配置することで実行できます。
