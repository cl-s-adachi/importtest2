#!/bin/bash
# ブリッジ名を変更する時は、br_name の "" ←の中を書き換える。
br_name="brbond1-"
# ブリッジ名 + VLAN ID を切り出す。
br_name_vlan=`brctl show | grep "${br_name}" | awk '{print $1}'`

# 削除対象のブリッジ名が存在した時だけ、削除する処理。
if [ ! -z "${br_name_vlan}" ]; then
        for line in ${br_name_vlan}; do
                ip link set dev ${line} down
                brctl delbr "${line}"
        done
fi

# 削除処理しても削除対象のブリッジ名が残っていた時だけ、表示する。
if [ ! -z "`brctl show | grep ${br_name}`" ]; then
        echo "===============【`hostname | awk -F\".\" '{print $1}'`】==============="
        echo "削除対象のブリッジ名 \"${br_name}\" が残っています。"
        echo "`brctl show | grep ${br_name} | awk '{print $1}'`"
        echo ""
fi
