#!/bin/bash
# cloudVirBr があって、brbond がない場合、cloudVirBr を br_name に代入。
if [ ! -z "`brctl show | grep cloudVirBr`" ] && [ -z "`brctl show | grep brbond`" ]; then
	br_name='3.0.7_running'
# cloudVirBr がなくて、brbond がある場合、brbond を br_name に代入。
elif [ -z "`brctl show | grep cloudVirBr`" ] && [ ! -z "`brctl show | grep brbond`" ]; then
	br_name='4.9_running'
# cloudVirBr も brbond も両方ある場合、br_name は NG となる。
elif [ ! -z "`brctl show | grep cloudVirBr`" ] && [ ! -z "`brctl show | grep brbond`" ]; then
	br_name=NG
# cloudVirBr も brbond も両方ない場合、br_name は NONE となる。
elif [ -z "`brctl show | grep cloudVirBr`" ] && [ -z "`brctl show | grep brbond`" ]; then
	br_name=NONE
fi

# 現在の CloudStack の Ver 判別する。
if [ ! -z "`/etc/init.d/cloudstack-agent status 2>/dev/null | grep 'pid'`" ]; then
	cs_ver='4.9_running'
elif [ ! -z "`/etc/init.d/cloud-agent status 2>/dev/null | grep 'pid'`" ]; then
	cs_ver='3.0.7_running'
else
	if [ -d "/etc/cloudstack/agent" ]; then
		cs_ver="cloudstack-agent is not running."
	elif [ -d "/etc/cloud/agent" ]; then
		cs_ver="cloud-agent is not running."
	fi
fi

if [ "${br_name}" = "${cs_ver}" ]; then
	echo "[ OK ] : CS Status = \"${cs_ver}\""
elif [ "${br_name}" = "NG" ]; then
	echo "[ NG ] : CS Status = \"${cs_ver}\"（\"cloudVirBr\" と \"brbond1\" が両方存在しています。）"
elif [ "${br_name}" = "NONE" ] && [[ ${cs_ver} = "4.9_running" || ${cs_ver} = "3.0.7_running" ]]; then
	echo "[ OK ] : CS Status = \"${cs_ver}\""
elif [ "${br_name}" = "NONE" ] && [[ ${cs_ver} != "4.9_running" && ${cs_ver} != "3.0.7_running" ]]; then
	echo "[ NG ] : CS Status = \"${cs_ver}\""
elif [ "${br_name}" != "${cs_ver}" ]; then
	if [ ${br_name} = '4.9_running' ]; then
		br_name="brbond1-xxxx"
	elif [ ${br_name} = '3.0.7_running' ]; then
		br_name="cloudVirBr xxxx"
	fi
	echo "[ NG ] : CS Status = \"${cs_ver}\"（ブリッジ名 = \"${br_name}\"）"
fi
