#!/bin/bash
if [ ! -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=cloudVirBr
elif [ -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ ! -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=brbond
elif [ -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ -z "`ls /sys/class/net | grep brbond`" ]; then
	echo "Snooping setting == [ OK ]"
	echo "\"cloudVirBr\" \"brbond\" ともブリッジ名が存在しません。"
	exit 1
else
	echo "Snooping setting == [ NG ]"
	echo "\"cloudVirBr\" \"brbond\" 両方のブリッジ名が存在します。"
	exit 1
fi

brname=`brctl show | grep ${br_name} | awk '{print $1}'`
for line in ${brname}; do
	if [ ! -z "`cat /sys/class/net/${line}/bridge/multicast_snooping | grep \"0\"`" ]; then
		echo "Snooping setting == [ OK ] ${line}"
	else
		echo "Snooping setting == [ NG ] ${line}"
	fi
done
