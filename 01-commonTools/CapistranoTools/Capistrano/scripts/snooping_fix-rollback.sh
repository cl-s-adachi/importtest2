#!/bin/bash
if [ ! -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=cloudVirBr
elif [ -z "`ls /sys/class/net | grep cloudVirBr`" ] && [ ! -z "`ls /sys/class/net | grep brbond`" ]; then
	br_name=brbond
else
	echo "ブリッジ名がひとつも存在しないか、\"cloudVirBr\" \"brbond\" 両方のブリッジ名が存在します。"
	exit 1
fi

brname=`brctl show | grep ${br_name} | awk '{print $1}'`
for line in ${brname}; do
	if [ ! -z "`cat /sys/class/net/${line}/bridge/multicast_snooping | grep \"1\"`" ]; then
		echo "Snooping setting == `cat /sys/class/net/${line}/bridge/multicast_snooping` ${line}"
	else
		echo "1" >/sys/class/net/${line}/bridge/multicast_snooping
		echo "Snooping setting == `cat /sys/class/net/${line}/bridge/multicast_snooping` ${line}"
	fi
done
