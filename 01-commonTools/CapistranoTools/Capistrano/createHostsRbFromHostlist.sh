#!/bin/bash

# 第一引数に指定されたホスト名の一覧を、hosts.rbの形式に変換して出力する

if [ -z $1 ]
then
        echo "Description: This script convert hosts.rb style list from simple list of hostname."
        echo "Usage: ./`basename $0` simple-host.list > hosts.rb"
        exit 1
fi

HOSTS=`cat $1 |sed s/\\n/" "/g`
echo "role :targets, *%w("${HOSTS}")"
