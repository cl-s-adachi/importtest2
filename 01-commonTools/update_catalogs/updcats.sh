#!/bin/bash

readonly debug=1
readonly dbuser="root"
readonly dbhost="127.0.0.1"
readonly dbpass=""
readonly dbquery="SELECT subscriberName, assignedTo, usableServiceTemplateIds, usableIsoIds FROM Catalogs ORDER BY uuid\G"
readonly apipath="./kick_api.sh"
readonly outdir="./out_updcats/`date +%Y%m%d_%H%M%S`"

usage_exit() {
    echo "Usage: ${0} -a true|false -t id,id,... [-i or -e -u account@domainid,account@domainid,...]" 1>&2
    exit 1
}

debug_param_out() {
    if [ ${debug} -ne 0 ]; then
        echo "DEBUG: ${1} = ${2}"
    fi
}

# make output directory if not exist
mkdir -p "${outdir}"

#--------------------------------------------------------
# validate params
#--------------------------------------------------------
while getopts "a:t:ieu:h" OPT
do
    case $OPT in
        a) alloc=$OPTARG ;;
        t) tempids=$OPTARG ;;
        i) user_flag="includeaccounts" ;;
        e) user_flag="excludeaccounts" ;;
        u) userids=$OPTARG ;;
        h) usage_exit ;;
    esac
done

debug_param_out "alloc" ${alloc}
debug_param_out "tempids" ${tempids}
debug_param_out "user_flag" ${user_flag}
debug_param_out "userids" ${userids}

if [ -z ${alloc} ] || [ -z ${tempids} ]; then
    usage_exit
fi
if [ ${alloc} != "true" ] && [ ${alloc} != "false" ]; then
    usage_exit
fi
#if [ -z ${tempids} ]; then
#    usage_exit
#fi
if [ ! -z ${user_flag} ] && [ -z ${userids} ]; then
    usage_exit
fi
if [ -z ${user_flag} ] && [ ! -z ${userids} ]; then
    usage_exit
fi


#--------------------------------------------------------
# query selectable templateids before update
#--------------------------------------------------------
mysql -h ${dbhost} -u ${dbuser} -p${dbpass} -D portal -e"${dbquery}" > "${outdir}/before.txt"


#--------------------------------------------------------
# do updateCatalogs isoroku api command
#--------------------------------------------------------

# make templateid params
arg_tempids=""
for id in ${tempids//,/ }
do
    arg_tempids=${arg_tempids}"templateid=${id} "
done
debug_param_out "arg_tempids" "${arg_tempids}"

# make includeaccounts or excludeaccounts params
arg_userids=""
i=0
for id in ${userids//,/ }
do
    acc_info=(`echo ${id} | tr -s '@' ' '`)
    if [ ${#acc_info[@]} -ne 2 ]; then
        usage_exit
    fi
    arg_userids=${arg_userids}"${user_flag}[${i}].account=${acc_info[0]} ${user_flag}[${i}].domainid=${acc_info[1]} "
    i=$((++i))
done
debug_param_out "arg_userids" "${arg_userids}"

# do command
final_command="${apipath} command=updateCatalogs allocation=${alloc} ${arg_tempids} ${arg_userids}"
debug_param_out "final_command" "${final_command}"
$final_command


#--------------------------------------------------------
# query selectable templateids after update
#--------------------------------------------------------
mysql -h ${dbhost} -u ${dbuser} -p${dbpass} -D portal -e"${dbquery}" > "${outdir}/after.txt"

