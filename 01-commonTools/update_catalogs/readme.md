# updcat.sh

updcat.sh は、カタログの一括テンプレート割り当て、および解除を実行するスクリプトです。スクリプト内部では、portal API の updateCatalogs をコールしますので、kick_api.sh が必要になります。

## インストール

* 本スクリプト updcat.sh を kick_api.sh と同じディレクトリに配置します。
* updcat.sh に実行権限を与えます。
* kick_api.sh をテキストエディタで開き、portal_apis 配列に "updateCatalogs" コマンドを追加します。以下は例です。
```
portal_apis=("deployValueVirtualMachine" "deployPremiumVirtualMachine" \
             "startVirtualMachine"       "listPremiumVirtualMachines" \
             "changeServiceForVirtualMachine" "scaleVirtualMachine" \
             "listDistributionGroups"    "listPremiumHosts" \
             "addPremiumHosts"           "removePremiumHost" \
             "listZones"                 "queryExAsyncJobResult" \
             "getMaxNumberOfPremiumHosts" "updateCatalogs")
```

* updcat.sh をテキストエディタで開き、以下のパラメーターを指定します。
```
readonly debug=0
readonly dbuser="root"
readonly dbhost="127.0.0.1"
readonly dbpass="xxxxxx"
```
debug=1 で出力結果にデバッグ情報が追加されます。

dbuser,dbhost,dbpass は、MySQL のユーザー、ホスト、パスワードを指定します。

## 使い方

作業ディレクトリを updcat.sh が存在するディレクトリにし、以下のように実行します。
```
$ ./updcats.sh -a true|false -t id,id,... [-i or -e -u account@domainid,account@domainid,...]
```

### オプション

-a true|false (必須)

allocation を指定します。true で「テンプレートを割り当てる」false で「テンプレートの割り当てを解除する」

-t id,id,... (必須)

テンプレートID を指定します。複数指定する場合はカンマ区切りで指定します。

-i

一括更新の対象とするユーザーを指定する場合は、-i を指定します。このオプションを指定した場合は、-u を指定する必要があります。また、-e との同時指定はできません。

-e

一括更新の対象から除外するユーザーを指定する場合は、-e を指定します。このオプションを指定した場合は、-u を指定する必要があります。また、-i との同時指定はできません。

-u

対象のユーザーを指定します。指定フォーマットは、<アカウント名>@<ドメインID> であり、複数指定する場合はカンマ区切りで指定します。

## 出力

portal API の updateCatalogs コマンドの出力結果（更新されたカタログ数）がそのまま表示されます。また、out_updcats ディレクトリが作成され、さらにその配下に updcat.sh を実行した日時のディレクトリが作成されます。その中に、スクリプト実行前のカタログ情報（before.txt）と、スクリプト実行後のカタログ情報（after.txt）が出力されます。

* out_updcats
    * 20180326_084830
        * after.txt
        * before.txt

