#!/bin/sh

SITE=`uname -n | awk -F- '{print $1}'`

case "${SITE}" in
  ckktky4)
    DBVIP='ckktky4-pcldb10'
    APNIC='eth1'
    ;;
  
  ck2ejo2)
    DBVIP='ck2ejo2-pcldb00s'
    APNIC='eth1'
    ;;

  ck2wjih)
    DBVIP='ck2wjih-pcldb00s'
    APNIC='eth1'
    ;;

  tckkejo2)
    DBVIP='tckkejo2-vcldb00-tcsm001'
    APNIC='eth2'
    ;;
  
  tck2ejo2)
    DBVIP='tck2ejo2-pcldb00s'
    APNIC='eth1'
    ;;

  tck2wjih)
    DBVIP='tck2wjih-pcldb00s'
    APNIC='eth1'
    ;;

esac

