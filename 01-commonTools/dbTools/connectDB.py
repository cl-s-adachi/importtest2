#! /usr/bin/env python
# coding: utf-8
#
# connectDB.py
# mysqlDBに接続する（だけ）
#
# - usage
# ./connectDB.py
#
# - note
# customize variableに設定されたパラメータに沿って、DB接続を行います。
#
# - date
# 2013/10/01 new created
#
# - author
#  taira
#
# - ver
# 1.0 (new)

import MySQLdb
import sys
import time

###### customize variable ######
# db url parameter
dbHost = 'tckktky4-vcldb01'
dbUser = 'root'
dbName = 'cloud'
dbPasswd = ''

# connection number
connectionNum = 50

# connection time
connectTime = 10.0

###### customize variable ######

# sql sentence
execSQL = 'select ht.tag, h.name, h.uuid, h.status, h.removed FROM host as h INNER JOIN host_tags as ht ON h.id = ht.host_id WHERE h.removed IS NULL'

# connection list
newConnectList = []

# connect MYSQL
for var in range(0,connectionNum):
        connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
        cur = connect.cursor()
        cur.execute(execSQL)

        # connnection add list.
        # note:要素を使いまわしている(リストに格納しない)と、コネクション数が増えない
        newConnectList.append(connect)

# connection 指定時間コネクション接続
time.sleep(connectTime)

# MYSQL接続コネクション解放
for var in range(0,connectionNum):
        reConnect = newConnectList.pop()
        reCur = reConnect.cursor()

        # コネクション解放
        reConnect.close()
        reCur.close()

# 一応終了時のメッセージ
print ('finished script')