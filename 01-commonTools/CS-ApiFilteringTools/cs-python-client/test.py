#!/bin/env python
#-*- coding: utf-8 -*-
import CloudStack
import time
from urllib2 import HTTPError

# CL test env
#api    = "http://219.117.239.167/client/api"
#
## mgmt1, proxytestadmin
## proxytestadmin,password, proxytest
#apikey_domainadmin = "1xiuUPH8CLbB7rR-BvrHhsioSJTYR3nGdiS632pN4U2oyfO9q-5wNhlxfBrJnxM1ECMVkqQQxE2YaKgyRan1EA"
#secret_domainadmin = "DUWwTFygnL_oWHLjGUYz60uJKyEDRnxryGGbj-x7kb9yMbIPyazbPSsOh8swJGmXG5euSc79F-P_UAAogHtQbQ"
#
## proxytest, password, ROOT
#apikey_user = "6JlvS_AZGHC4wBlzHl6KFr6x9z8b36Ba1NZWDxEFpBu1Pw5kiOhyDbpzEmgi-DREUVPn_e-mafLBa45zfL6CIQ"
#secret_user = "eqwEfKYIjcmv0Ixf7mAcZITyFRfYs5u9Rs1H6QMVKjEOt8_YwK_rblOGiJu7f7ClDP_vvuKb7RnhyMtBTjvVTA"
#
#apikey_root = "084z00OgonY3iFZ_uIZupovluPQt7gTgsEvO0_6D_nUsM31YIX-873dr6O9swWtcMSko9bAF8-X9AQaQwikCYw"
#secret_root = "m5jNjbneRQnu5KEM2R3rA6kM9DC-ymDJlH5kGIoOHx3Hkkc2s8F-feeMxMco83InMRLyTeehOR_xkqp-Ouac3g"

# production
api    = "https://localhost:1443/client/api"
#test_user
apikey_user = "yCYVPSgFhXCy5n2S6K_WPIMUHia-mck_mRgve0W9ZqpA_aGW6HEm2kMx5Frs5BuIBQmtVxhIE2GA6BJU0nG1mQ"
secret_user = "r46iFb1HKJL-p3iQtmJaUVmipiFldNGppb2VsfAVUaIj2LPVWRc9258Z_8xjNbFyFIsbCO_rQqHGjNAUnrUI7A"

#test_domainadmin
apikey_domainadmin = "KInWot1XIx34_LdFZFIMWMbau-9NTbBNxZuxJVjQK3RdpS16ISzgfNs4zeFmsXouoJWSoYB3TCgOjeRBMy4J-Q"
secret_domainadmin = "JL2bx8yNRIBa-Khanx_nbVkM_OWSQQ1-erclIBgoWsN5Wbl2mftUo93tfst4UyKAsmpk1vi4RMp6q6wa05c4Dw"

#test_admin
apikey_root = "h2Ym7EXKltkh4mA5OXCdtrfgJ9pvLp8YM1sEcPkDnbZKmhSTkyESIR8CZcFF2O4i84SVMunWWeNzkOx7EVXzOw"
secret_root = "O877Nkb-nfWgokCuiSizXnoTqWql-jeNKZOITS_xUmsp6UoJ1sbuQ8sFOYqdM0-rDuuHE0NpMZBw6TyQ05aiQw"

admin = CloudStack.Client(api, apikey_domainadmin, secret_domainadmin)
user  = CloudStack.Client(api, apikey_user, secret_user)
root  = CloudStack.Client(api, apikey_root, secret_root)

commands = {'activateProject': {'id':'-1'},
            'addAccountToProject': {'projectid':'-1'},
            'addCluster': {'clustername':'-1', 'clustertype':'-1', 'hypervisor':'-1', 'zoneid':'-1'},
            'addExternalFirewall': {'password':'-1', 'url':'-1', 'username':'-1', 'zoneid':'-1'},
            'addExternalLoadBalancer': {'password':'-1', 'url':'-1', 'username':'-1', 'zoneid':'-1'},
            'addF5LoadBalancer': {'networkdevicetype': '-1', 'password':'-1', 'physicalnetworkid':'-1', 'url':'-1', 'username':'-1'},
            'addHost': {'hypervisor':'-1', 'password':'-1', 'url':'-1', 'username':'-1', 'zoneid':'-1', },
            'addNetscalerLoadBalancer': {'networkdevicetype':'-1', 'password':'-1', 'physicalnetworkid':'-1', 'url':'-1', 'username':'-1', },
            'addNetworkDevice': None,
            'addNetworkServiceProvider': {'name':'-1', 'physicalnetworkid':'-1', },
            'addSecondaryStorage': {'url':'-1', },
            'addSrxFirewall': {'networkdevicetype':'-1', 'password':'-1', 'physicalnetworkid':'-1', 'url':'-1', 'username':'-1', },
            'addSwift': {'url':'-1', },
            'addTrafficMonitor': {'url':'-1', 'zoneid':'-1', },
            'addTrafficType': {'physicalnetworkid':'-1', 'traffictype':'-1',},
            'addVpnUser': {'password':'-1', 'username':'-1',},
            'assignToLoadBalancerRule': {'id':'-1', 'virtualmachineids':'-1',},
            'assignVirtualMachine': {'account':'-1', 'domainid':'-1', 'virtualmachineid':'-1', },
            'associateIpAddress': None,
            'associateLun': {'iqn':'-1', 'name':'-1', },
            'attachIso': {'id':'-1', 'virtualmachineid':'-1', },
            'attachVolume': {'id':'-1', 'virtualmachineid':'-1', },
            'authorizeSecurityGroupEgress': None,
            'authorizeSecurityGroupIngress': None,
            'cancelHostMaintenance': {'id':'-1', },
            'cancelStorageMaintenance': {'id':'-1', },
            'changeServiceForRouter': {'id':'-1', 'serviceofferingid':'-1',},
            'changeServiceForVirtualMachine': {'id':'-1', 'serviceofferingid':'-1',},
            'configureF5LoadBalancer': {'lbdeviceid':'-1', },
            'configureNetscalerLoadBalancer': {'lbdeviceid':'-1', },
            'configureSrxFirewall': {'fwdeviceid':'-1', },
            'configureVirtualRouterElement': {'id':'-1','enabled':'-1', },
            'copyIso': {'id':'-1', 'destzoneid':'-1', 'sourcezoneid':'-1', },
            'copyTemplate': {'id':'-1', 'destzoneid':'-1', 'sourcezoneid':'-1', },
            'createAccount': {'accounttype':'-1', 'email':'-1', 'firstname':'-1', 'lastname':'-1', 'password':'-1', 'username':'-1', },
            'createDiskOffering': {'displaytext':'-1', 'name':'-1', },
            'createDomain': {'name':'-1', },
            'createFirewallRule': {'protocol':'-1', },
            'createInstanceGroup': {'name':'-1', },
            'createIpForwardingRule': {'ipaddressid':'-1', 'protocol':'-1', 'startport':'-1',},
            'createLBStickinessPolicy': {'lbruleid':'-1', 'methodname':'-1', 'name':'-1', },
            'createLoadBalancerRule': {'algorithm':'-1', 'name':'-1', 'privateport':'-1', 'publicport':'-1', },
            'createLunOnFiler': {'name':'-1', 'size':'-1' },
            'createNetwork': {'displaytext':'-1', 'name':'-1', 'networkofferingid':'-1', 'zoneid':'-1', },
            'createNetworkOffering': {'displaytext':'-1', 'guestiptype':'-1', 'name':'-1', 'supportedservices':'-1', 'traffictype':'-1', },
            'createPhysicalNetwork': {'name':'-1', 'zoneid':'-1', },
            'createPod': {'gateway':'-1', 'name':'-1', 'netmask':'-1', 'startip':'-1', 'zoneid':'-1', },
            'createPool': {'algorithm':'-1', 'name':'-1', },
            'createPortForwardingRule': {'ipaddressid':'-1', 'privateport':'-1', 'protocol':'-1', 'publicport':'-1', 'virtualmachineid':'-1', },
            'createProject': {'displaytext':'-1', 'name':'-1', },
            'createRemoteAccessVpn': {'publicipid':'-1', },
            'createSSHKeyPair': {'name':'-1', },
            'createSecurityGroup': {'name':'-1', },
            'createServiceOffering': {'cpunumber':'-1', 'cpuspeed':'-1', 'displaytext':'-1', 'memory':'-1', 'name':'-1', },
            'createSnapshot': {'volumeid':'-1', },
            'createSnapshotPolicy': {'intervaltype':'-1', 'maxsnaps':'-1', 'schedule':'-1', 'timezone':'-1', 'volumeid':'-1', },
            'createStorageNetworkIpRange': {'gateway':'-1', 'netmask':'-1', 'podid':'-1', 'startip':'-1', },
            'createStoragePool': {'name':'-1', 'url':'-1', 'zoneid':'-1', },
            'createTemplate': {'displaytext':'-1', 'name':'-1', 'ostypeid':'-1', },
            'createUser': {'account':'-1', 'email':'-1', 'firstname':'-1', 'lastname':'-1', 'password':'-1', 'username':'-1', },
            'createVirtualRouterElement': {'nspid':'-1', },
            'createVlanIpRange': {'startip':'-1', },
            'createVolume': {'name':'-1', },
            'createVolumeOnFiler': {'aggregatename':'-1', 'ipaddress':'-1', 'password':'-1', 'poolname':'-1', 'size':'-1', 'username':'-1', 'volumename':'-1', },
            'createZone': {'dns1':'-1', 'internaldns1':'-1', 'name':'-1', 'networktype':'-1', },
            'deleteAccount': {'id':'-1', },
            'deleteAccountFromProject': {'account':'-1', 'projectid':'-1', },
            'deleteCluster': {'id':'-1', },
            'deleteDiskOffering': {'id':'-1', },
            'deleteDomain': {'id':'-1', },
            'deleteExternalFirewall': {'id':'-1', },
            'deleteExternalLoadBalancer': {'id':'-1', },
            'deleteF5LoadBalancer': {'lbdeviceid':'-1', },
            'deleteFirewallRule': {'id':'-1', },
            'deleteHost': {'id':'-1', },
            'deleteInstanceGroup': {'id':'-1', },
            'deleteIpForwardingRule': {'id':'-1', },
            'deleteIso': {'id':'-1', },
            'deleteLBStickinessPolicy': {'id':'-1', },
            'deleteLoadBalancerRule': {'id':'-1', },
            'deleteNetscalerLoadBalancer': {'lbdeviceid':'-1', },
            'deleteNetwork': {'id':'-1', },
            'deleteNetworkDevice': {'id':'-1', },
            'deleteNetworkOffering': {'id':'-1', },
            'deleteNetworkServiceProvider': {'id':'-1', },
            'deletePhysicalNetwork': {'id':'-1', },
            'deletePod': {'id':'-1', },
            'deletePool': {'poolname':'-1', },
            'deletePortForwardingRule': {'id':'-1', },
            'deleteProject': {'id':'-1', },
            'deleteProjectInvitation': {'id':'-1', },
            'deleteRemoteAccessVpn': {'publicipid':'-1', },
            'deleteSSHKeyPair': {'name':'-1', },
            'deleteSecurityGroup': None,
            'deleteServiceOffering': {'id':'-1', },
            'deleteSnapshot': {'id':'-1', },
            'deleteSnapshotPolicies': None,
            'deleteSrxFirewall': {'fwdeviceid':'-1', },
            'deleteStorageNetworkIpRange': {'id':'-1', },
            'deleteStoragePool': {'id':'-1', },
            'deleteTemplate': {'id':'-1', },
            'deleteTrafficMonitor': {'id':'-1', },
            'deleteTrafficType': {'id':'-1', },
            'deleteUser': {'id':'-1', },
            'deleteVlanIpRange': {'id':'-1', },
            'deleteVolume': {'id':'-1', },
            'deleteZone': {'id':'-1', },
            'deployVirtualMachine': {'serviceofferingid':'-1', 'templateid':'-1', 'zoneid':'-1', },
            'destroyLunOnFiler': {'path':'-1', },
            'destroyRouter': {'id':'-1', 'serviceofferingid':'-1', },
            'destroySystemVm': {'id':'-1', },
            'destroyVirtualMachine': {'id':'-1', },
            'destroyVolumeOnFiler': {'aggregatename':'-1', 'ipaddress':'-1', 'volumename':'-1', },
            'detachIso': {'virtualmachineid':'-1', },
            'detachVolume': None,
            'disableAccount': {'lock':'-1', },
            'disableStaticNat': {'ipaddressid':'-1', },
            'disableUser': {'id':'-1', },
            'disassociateIpAddress': {'id':'-1', },
            'dissociateLun': {'iqn':'-1', 'path':'-1', },
            'enableAccount': None,
            'enableStaticNat': {'ipaddressid':'-1', 'virtualmachineid':'-1', },
            'enableStorageMaintenance': {'id':'-1', },
            'enableUser': {'id':'-1', },
            'extractIso': {'id':'-1', 'mode':'-1', },
            'extractTemplate': {'id':'-1', 'mode':'-1', },
            'extractVolume': {'id':'-1', 'mode':'-1', 'zoneid':'-1'},
            'generateUsageRecords': {'enddate':'-1', 'startdate':'-1', },
            'getCloudIdentifier': {'userid':'-1', },
            'getVMPassword': {'id':'-1', },
            'ldapConfig': {'hostname':'-1', 'queryfilter':'-1', 'searchbase':'-1', },
            'listAccounts': None,
            'listAlerts': None,
            'listAsyncJobs': None,
            'listCapabilities': None,
            'listCapacity': None,
            'listClusters': None,
            'listConfigurations': None,
            'listDiskOfferings': None,
            'listDomainChildren': None,
            'listDomains': None,
            'listEventTypes': None,
            'listEvents': None,
            'listExternalFirewalls': {'zoneid': '-1'},
            'listExternalLoadBalancers': None,
            'listF5LoadBalancerNetworks': {'lbdeviceid': '-1'},
            'listF5LoadBalancers': None,
            'listFirewallRules': None,
            'listHosts': None,
            'listHypervisorCapabilities': None,
            'listHypervisors': None,
            'listInstanceGroups': None,
            'listIpForwardingRules': None,
            'listIsoPermissions': {'id': '-1'},
            'listIsos': {'id': '-1'},
            'listLBStickinessPolicies': {'lbruleid': '-1'},
            'listLoadBalancerRuleInstances': {'id': '-1'},
            'listLoadBalancerRules': {'id': '-1'},
            'listLunsOnFiler': None,
            'listLunsOnFiler': {'poolname': '-1'},
            'listNetscalerLoadBalancerNetworks': {'lbdeviceid':'-1'},
            'listNetscalerLoadBalancers': None,
            'listNetworkDevice': None,
            'listNetworkOfferings': None,
            'listNetworkServiceProviders': None,
            'listNetworks': None,
            'listOsCategories': None,
            'listOsTypes': None,
            'listPhysicalNetworks': None,
            'listPods': None,
            'listPools': None,
            'listPortForwardingRules': None,
            'listProjectAccounts': {'projectid': '-1'},
            'listProjectInvitations': None,
            'listProjects': None,
            'listPublicIpAddresses': None,
            'listRemoteAccessVpns': {'publicipid': '-1'},
            'listResourceLimits': None,
            'listRouters': None,
            'listSSHKeyPairs': None,
            'listSecurityGroups': None,
            'listServiceOfferings': None,
            'listSnapshotPolicies': {'volumeid': '-1'},
            'listSnapshots': None,
            'listSrxFirewallNetworks': {'lbdeviceid': '-1'},
            'listSrxFirewalls': None,
            'listStorageNetworkIpRange': None,
            'listStoragePools': None,
            'listSupportedNetworkServices': None,
            'listSwifts': None,
            'listSystemVms': None,
            'listTemplatePermissions': {'id':'-1', },
            'listTemplates': {'templatefilter': '-1'},
            'listTrafficMonitors': {'traffictype': '-1', 'zoneid': '-1'},
            'listTrafficTypeImplementors': None,
            'listTrafficTypes': {'physicalnetworkid': '-1'},
            'listUsageRecords': {'enddate': '-1', 'startdate': '-1'},
            'listUsageTypes': None,
            'listUsers': None,
            'listVirtualMachines': None,
            'listVirtualRouterElements': None,
            'listVlanIpRanges': None,
            'listVolumes': None,
            'listVolumesOnFiler': {'poolname': '-1'},
            'listVpnUsers': None,
            'listZones': None,
            'listZones': None,
            'login': {'username':'-1', 'password':'-1', },
            'logout': None,
            'migrateSystemVm': {'hostid':'-1', 'virtualmachineid':'-1',},
            'migrateVirtualMachine': {'virtualmachineid':'-1', },
            'migrateVolume': {'storageid':'-1', 'volumeid':'-1', },
            'modifyPool': {'algorithm':'-1', 'poolname':'-1', },
            'prepareHostForMaintenance': {'id':'-1', },
            'prepareTemplate': {'templateid':'-1', 'zoneid':'-1', },
            'queryAsyncJobResult': {'jobid':'-1', },
            'rebootRouter': {'id':'-1', },
            'rebootSystemVm': {'id':'-1', },
            'rebootVirtualMachine': {'id':'-1', },
            'reconnectHost': {'id':'-1', },
            'recoverVirtualMachine': {'id':'-1', },
            'registerIso': {'displaytext':'-1', 'name':'-1', 'url':'-1', 'zoneid':'-1', },
            'registerSSHKeyPair': {'name':'-1', 'publickey':'-1', },
            'registerTemplate': {'displaytext':'-1', 'format':'-1', 'hypervisor':'-1', 'name':'-1', 'ostypeid':'-1', 'url':'-1', 'zoneid':'-1', },
            'registerUserKeys': {'id':'-1', },
            'removeFromLoadBalancerRule': {'id':'-1', 'virtualmachineids':'-1', },
            'removeVpnUser': {'username':'-1', },
            'resetPasswordForVirtualMachine': {'id':'-1', },
            'restartNetwork': {'id':'-1', },
            'restoreVirtualMachine': {'virtualmachineid':'-1', },
            'revokeSecurityGroupEgress': {'id':'-1', },
            'revokeSecurityGroupIngress': {'id':'-1', },
            'startRouter': {'id':'-1', },
            'startSystemVm': {'id':'-1', },
            'startVirtualMachine': {'id':'-1', },
            'stopRouter': {'id':'-1', },
            'stopSystemVm': {'id':'-1', },
            'stopVirtualMachine': {'id':'-1', },
            'suspendProject': {'id':'-1', },
            'updateAccount': {'newname':'-1', },
            'updateCluster': {'id':'-1', },
            'updateConfiguration': {'name':'-1', },
            'updateDiskOffering': {'id':'-1', },
            'updateDomain': {'id':'-1', },
            'updateHost': {'id':'-1', },
            'updateHostPassword': {'username':'-1', 'password':'-1', },
            'updateHypervisorCapabilities': None,
            'updateInstanceGroup': {'id':'-1', },
            'updateIso': {'id':'-1', },
            'updateIsoPermissions': {'id':'-1', },
            'updateLoadBalancerRule': {'id':'-1', },
            'updateNetwork': {'id':'-1', },
            'updateNetworkOffering': None,
            'updateNetworkServiceProvider': {'id':'-1', },
            'updatePhysicalNetwork': {'id':'-1', },
            'updatePod': {'id':'-1', },
            'updateProject': {'id':'-1', },
            'updateProjectInvitation': {'projectid':'-1', },
            'updateResourceCount': {'domainid':'-1', },
            'updateResourceLimit': {'resourcetype':'-1', },
            'updateServiceOffering': {'id':'-1', },
            'updateStorageNetworkIpRange': {'id':'-1', },
            'updateStoragePool': {'id':'-1', },
            'updateTemplate': {'id':'-1', },
            'updateTemplatePermissions': {'id':'-1', },
            'updateTrafficType': {'id':'-1', },
            'updateUser': {'id':'-1', },
            'updateVirtualMachine': {'id':'-1', },
            'updateZone': {'id':'-1', },
            'uploadCustomCertificate': {'certificate':'-1', 'domainsuffix':'-1', },
            }

commands = {
    #default:1
    'deleteZone': {'id':'-1', },
    #default:7
    'disableAccount': {'lock':'-1', },
#            'listLoadBalancerRuleInstances': {'id': '-1'},
#            'listTrafficMonitors': {'traffictype': '-1', 'zoneid': '1'},
            }

def execute(client):
    results = dict()
    for k in sorted(commands.keys()):
        v = commands[k]
        print k
        try:
            r = ""
            if v is None:
                r = getattr(client, k)()
            else:
                r = getattr(client, k)(v)
            #print "open -> %s" % r,
            #print " o | open",
            results[k] = "open"
        except HTTPError, e:
            #print " -> !! %d, %s !!" % (e.code, e.read())
            body = e.read()
            if e.code == 432:
                #print " x | filtrated by reverse proxy",
                results[k] = "filtrated by reverse proxy"
            elif body.find('The given command') >= 0:
                print " x | disabled by cloudstack property settings"
                results[k] = "disabled by CS property"
            else:
                #print " o | open(logical error) %d, %s" % (e.code, body),
                results[k] = "open"
        except RuntimeError, e:
            print " o | open %s" % e.__str__()
            results[k] = "open"
        finally:
            time.sleep(0.5)

            #print " |"

    return results

if __name__ == '__main__':

    print "execute user"
    user_results = execute(user)
    print "execute admin"
    admin_results = execute(admin)
    print "execute root"
    root_results = execute(root)
    print "=" * 120
    print "command, root, da, user"
    for k in sorted(commands.keys()):
        print "| %s | %s | %s | %s |" % (k, root_results[k], admin_results[k], user_results[k])
