#!/bin/sh

#########################################################
# showCommandsFor-cs-python-script.sh
# cs-python-script用commands配列生成スクリプト
# date:2014.1.21
# author:taira
#
# ・概要
# 　cs-python-scriptでは、test.pyのcommands配列に関数名と引数を定義している。
# 　これはスクリプトの作り上、CS APIの関数名と必須パラメータとなっている。
# 　CS APIの増減に伴い、commands配列の編集が必要になるが、
# 　必須パラメータを1つ1つ調べていたら大変なので、その処理を自動化する。
#
# ・事前準備
# 　api.txtに必須パラメータを調べたいAPI名を記載する(1行1API)
#   同一フォルダにkick.shが必要
# ・実行方法
#   ./printCommandsFor-cs-python-script.sh
#
# ・実行結果
#   out.txtに結果出力
#　 [必須項目有りのAPI]
#   'addIpToNic':{'nicid':'-1',}
#   [必須項目無しのAPI]
#   'addBigSwitchVnsDevice':None,
#　この形式は、test.pyのcommands配列の書式と同一になっている
#
#　・処理概要（備考）
#　　CloudStack4.2.1では導入されているCloudStackAPI(listApis)を使用しています。
#    本APIが無いバージョンおよびAPIの仕様変更が入ると動作しない可能性があります。
#
#　　ざっくり書くと、
#    listApis name=xxxxで実行
#    paramsタグ(パラメータ）でrequiredタグがtrue(必須パラメータ）のnameを取得。
#    やっつけでgrepでとってきている。
#    その後、それを実行結果に記載した書式で出力
#
#########################################################


filename="api.txt"
tmpfile="tmp.txt"
tmpfile2="tmp2.txt"
outputfile="out.txt"

cat ${filename}|while read line
do
  #api requredパラメータ取得
  ./kick.sh command=listApis name=${line} | grep "<params>" -A 6 | grep "<required>true</required>" -B 5 | grep "<name>" | sed -e "s/<name>/'/" | sed -e "s/<\/name>/':'-1', /" > ${tmpfile}

    # api名出力
    echo -n "'${line}':" >> ${outputfile}

    # ファイルサイズで、必須パラメータの有無チェック
    if [ -s ${tmpfile} ] ; then

      isRequired=0

      # 必須パラメータ精査
      cat ${tmpfile}|while read pLine
      do
        # 初回のみ"{"を出力"
        if [ ${isRequired} -eq 0 ] ; then
          echo -n "{" >> ${outputfile}
          isRequired=1
        fi
        # " 必須パラメータを出力"
        echo -n ${pLine} >> ${outputfile}
      done

      # "},"で閉じて終了
      echo "}," >> ${outputfile}

    else
      # 必須パラメータが無い場合は、"None,"を出力
      echo "None," >> ${outputfile}
    fi

    rm ${tmpfile}

done