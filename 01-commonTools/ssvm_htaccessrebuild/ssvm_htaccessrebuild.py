#!/usr/bin/python2
# -*- coding: utf-8 -*-
import os
import sys
import subprocess
import socket
import mysql.connector
import time
try:
	import config
except ImportError:
	print('config.pyがないため、スクリプトが動作できません。')
	sys.exit(1)

SshPassCommand = os.path.dirname(os.path.realpath(__file__)) + '/sshpass'
SsvmIdentityFile = os.path.dirname(os.path.realpath(__file__)) + '/id_rsa_ssvm'

class Ssvm:
	def __init__(self, ssvm_name):
		c = mysql.connector.connect(connection_timeout=3,  host=config.DbHost, user=config.DbUser, password=config.DbPassword, database='cloud')
		cur = c.cursor()
		cur.execute('select private_ip_address, public_ip_address,data_center_id from host where removed is NULL and type="SecondaryStorageVM" and name=%s', (ssvm_name, ))
		row = cur.fetchone()
		self.name = ssvm_name
		self.privateIpAddress = row[0]
		self.publicIpAddress = row[1]
		self.dataCenterId = row[2]
		cur.close()
		c.close()

	@staticmethod
	def getAllSsvms():
		c = mysql.connector.connect(connection_timeout=3,host=config.DbHost, user=config.DbUser, password=config.DbPassword, database='cloud')
		cur = c.cursor()
		cur.execute('select name from host where removed is NULL and type="SecondaryStorageVM"')
		rows = cur.fetchall()
		retlist = []
		for r in rows:
			retlist.append(Ssvm(r[0]))
		cur.close()
		c.close()
		return retlist

	def updateHtaccess(self, idfile, content):
		#SSVMへAPサーバ経由でポートフォワーディング
		cmdargs = ['sshpass', '-p', config.ApPassword, 'ssh', '-T', '-tL', '{0}:{1}:3922'.format(config.LocalPort, self.privateIpAddress), '-ostricthostkeychecking=no', '-oExitOnForwardFailure=yes','{0}@{1}'.format(config.ApUser, config.ApHost)]
		p_forwarding = subprocess.Popen(cmdargs, stdin=subprocess.PIPE)

		#ポートフォワーディングが完了するのを待つ
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		while True:
			if p_forwarding.poll():
				raise RuntimeError('SSVMへのポートフォワーディングに失敗しました。localhost:{0}が使用中でないか確認してください。'.format(config.LocalPort))
			res = sock.connect_ex(('127.0.0.1', config.LocalPort))
			if res == 0:
				break
			time.sleep(0.1)

		#.htaccessファイルを更新する
		cmdargs = ['ssh','-T', '-i', idfile, 'root@localhost', '-p', str(config.LocalPort), '-oLogLevel=ERROR', '-oUserKnownHostsFile=/dev/null', '-oBatchMode=yes', '-oStrictHostKeyChecking=no','cat > /var/www/html/copy/.htaccess']
		p = subprocess.Popen(cmdargs, stdin = subprocess.PIPE, stderr = subprocess.PIPE)
		err = p.communicate(content)[1]
		p_forwarding.terminate()
		p_forwarding.wait()
		if len(err) > 0:
			raise RuntimeError('.htaccessファイルを更新中にエラーが発生しました: {0}'.format(err))
		

def getZoneIdByZoneName(zone_name):
	c = mysql.connector.connect(host=config.DbHost, user=config.DbUser, password=config.DbPassword, database='cloud')
	cur = c.cursor()
	cur.execute('select id from data_center where name=%s', (zone_name,))
	row = cur.fetchone()
	if row is None:
		return None
	else:
		return row[0]
	cur.close()
	c.close()
		

def generateHtaccess(ssvm_list):
	strlist = []
	strlist.append('Options -Indexes')
	strlist.append('order deny,allow')
	strlist.append('deny from all')
	for s in ssvm_list:
		strlist.append('allow from {0}'.format(s.publicIpAddress))
	strlist.append('')
	return '\n'.join(strlist)

def main():
	global LocalPort
	global SsvmIdentityFile
	if not hasattr(config, 'LocalPort'):
		setattr(config, 'LocalPort', 10779)
	if len(sys.argv) < 2:
		print('使用法: {0} [-a|-z <ゾーン名>|-n <SSVM名>]'.format(sys.argv[0]))
		print('指定した条件に合致するSSVMの.htaccessファイルを更新します。')
		print('オプション:')
		print('-a           : すべてのSSVMを対象にする')
		print('-z <ゾーン名>: <ゾーン名>に存在するSSVMを対象にする')
		print('-n <SSVM名>  : <SSVM名>のSSVMを対象にする')
		print('-p <ポート番号>: ポートフォワーディングのローカルポート番号を <ポート番号> にする デフォルト:{0}'.format(config.LocalPort))
		sys.exit()

	if not os.path.exists(SsvmIdentityFile):
		print('SSVMの秘密鍵ファイルがありません。SSVMの秘密鍵ファイル名をid_rsa_ssvmとして、このスクリプトと同じディレクトリに配置してください。')
		print('SSVMの秘密鍵ファイルはAPサーバの /var/cloudstack/management/.ssh/id_rsa です。')
		sys.exit(1)
	try:
		ssvm_list = Ssvm.getAllSsvms()
	except mysql.connector.Error:
		print('SSVMの情報をDBから取得中にDBエラーが発生しました: {0}'.format(sys.exc_info()[1]))
		sys.exit(1)
	except Exception:
		print('SSVMの情報をDBから取得中に不明なエラーが発生しました: {0}'.format(sys.exc_info()[1]))
		sys.exit(1)
	try:
		htaccess = generateHtaccess(ssvm_list)
	except Exception:
		print('.htaccessファイルのデータを作成中にエラーが発生しました: {0}'.format(sys.exc_info()[1]))
		sys.exit(1)


	try:
		i = sys.argv.index('-p')
		try:
			config.LocalPort = int(sys.argv[i + 1])
		except IndexError:
			print('-p オプションが指定されましたが、ポート番号が指定されませんでした。')
			sys.exit(1)
	except ValueError:
		pass

	cond = False #対象SSVMの条件が指定されたか?
	if not cond:
		try:
			i = sys.argv.index('-n')
			try:
				vmname = sys.argv[i + 1]
			except IndexError:
				print('-nオプションが指定されましたが、SSVM名が指定されませんでした。')
				sys.exit(1)
			else:
				ssvm_list = [x for x in ssvm_list if x.name == vmname]
				cond = True
		except ValueError:
			pass
	if not cond:
		try:
			i = sys.argv.index('-z')
			try:
				zonename = sys.argv[i + 1]
			except IndexError:
				print('-zオプションが指定されましたが、ゾーン名が指定されませんでした。')
				sys.exit(1)
			zoneid = getZoneIdByZoneName(zonename)
			if zoneid is None:
				print('ゾーン {0} は存在しません'.format(zonename))
				sys.exit(1)
			else:
				ssvm_list = [x for x in ssvm_list if x.dataCenterId == zoneid]
				cond = True
		except ValueError:
			pass
	if '-a' in sys.argv: #全SSVMが対象
		cond = True

	if not cond:
		print('更新対象のSSVMの絞り込み条件を指定してください。すべてのSSVMを対象にするには-aオプションを指定してください。')
		sys.exit(1)

	if len(ssvm_list) == 0:
		print('.htaccessファイルの更新対象のSSVMが存在しないため、終了します。')
		sys.exit()

	for s in ssvm_list:
		try:
			s.updateHtaccess(os.path.dirname(sys.argv[0]) + '/id_rsa_ssvm', htaccess)
			print('{0} の.htaccessファイルを更新しました。'.format(s.name))
		except Exception:
			print('{0} の.htaccessファイルを更新中に不明なエラーが発生しました: {1}'.format(s.name, sys.exc_info()[1]))
			sys.exit(1)
main()
