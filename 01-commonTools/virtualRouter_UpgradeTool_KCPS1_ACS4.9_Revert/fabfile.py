# -*- coding: utf-8 -*-

# Python standard Libary
import sys, codecs, re
import os.path
import logging
import json
import time
import functools
from datetime import datetime, timedelta

# PyYaml
import yaml


# fabric
from fabric.api import local, run, execute
from fabric.api import env, settings
from fabric.decorators import *
from fabric.colors import *
from fabric.utils import puts
from fabric.api import *

# Exoscale cs
from cs import CloudStack

# Csv
import csv

## log settings
stream_log = logging.StreamHandler()
stream_log.setLevel(logging.DEBUG)
stream_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

file_log = logging.FileHandler(filename='operation.log')
file_log.setLevel(logging.INFO)
file_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

logging.getLogger().addHandler(file_log)
logging.getLogger().setLevel(logging.DEBUG)


KEY_VR_NAME="vr_name"
KEY_M_NUMBER="m_number"
KEY_ZONE="zone"
KEY_NET_GUEST_TYPE="guest_type" # Isolated|Shared
KEY_ID="id"
KEY_UUID="uuid"
KEY_CS_PRIVATE_IP="cs_private_ip"
KEY_NETWORK_ID = "network_id"
KEY_NETWORK_UUID = "network_uuid"
KEY_NETWORK_NAME = "network_name"

KEY_ZONE_CLUSTER_NAME="cluster_name"
KEY_ZONE_VCENTER_IP="vcenter_ip"
KEY_ZONE_VCENTER_USER="vcenter_user"
KEY_ZONE_VCENTER_PASS="vcenter_password"
KEY_ZONE_PORTFORWARD_HOST="portforward_host"
KEY_ZONE_PORTFORWARD_PORT="portforward_port"

@task
@runs_once
def setEnv(config='develop.yml'):
    '''
    設定ファイルの読み込み
    '''
    logging.getLogger().addHandler(stream_log)
    if not os.path.exists(config):
            logging.error(u'設定ファイルが存在しません。%s' % config)
            exit(1)

    try:
            # 設定ファイルの読み込み
            with open(config, 'r') as f:
                    conf = yaml.load(f)

            env.hosts        = []     # 動的にホスト設定する
            env.vr           = conf['vr']
            env.ap           = conf['ap']
            env.kvm_host     = conf['kvm_host']
            env.csApi        = conf['csApi']
            env.database = conf['database']
            env.networkOfferings = conf['networkOfferings']
            env.flags = conf['flags']
            env.vr_revert = conf['vr_revert']

            # ログディレクトリの作成
            if not os.path.exists('logs'):
                    os.mkdir('logs')

    except Exception as e:
            print e
            logging.error(u'設定ファイルの読み込みに失敗しました。%s' % config)
            exit(1)
    else:
            logging.info('config file loaded ... %s' % config)

@task
@runs_once
def isolatedRevert(filepath="specialDomains.txt"):
    '''
    isolated 仮想ルータの切戻し
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('isolated revert start.')
    startTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    # M番号
    mNumbers = _loadMNnumbers(filepath)
    if not mNumbers:
        logging.warn("No MNumber found in " + filepath + ".")
        exit(-1)

    execution_filepath = env.flags["executionFile"]
    # M番号単位で処理を行う（多重処理もM番号内で行う
    for mNumber in mNumbers:
        logging.info(u'[%s:] start ...' % mNumber)
        if not _checkExecutionFile(execution_filepath):
            logging.warn(red(execution_filepath + " not found. stop execution at MNumber: " + mNumber))
            break

        # M番号に紐づくルータの取得
#        datas = _getIsolatedRouterAndNetworkIds(mNumber)
        datas = _getRouterAndNetworkIdsForRevert(mNumber)
        if not datas:
            msg = u'[%s:] Isolated のルータが取得できませんでした。' % (mNumber)
            logging.error(msg)
            continue

        # ここからはfabricの並列処理の仕組みを無理やり使う
        # instaceのキー配列を作る
        vmids = map((lambda d: d['instance_name']), datas)
        # instanceのキー配列を、タスクの実行hostsとして設定する
        env.hosts = vmids
        # 紐づける値datasを渡して、並列実行タスクを呼び出す
        results = execute(do_isolated_revert, mNumber, datas)
        _printResults(startTime, 'isolated', mNumber, results)

    logging.info('isolated revert finish.')

##
# @parallelはWindowsでは動作しないためコメントアウトすること
# コメントアウトした場合は、直列実行となる
# pool_sizeで平行処理数を指定できる

@parallel(pool_size=10)
def do_isolated_revert(mNum, datas):
    logging.getLogger().addHandler(stream_log)
    logging.debug('do_isolated_revert.')
    res = {'status': 'NG', 'msg': ''}

    # タスクの実行ホスト(ここでは実在しないinstanceのキーが入っている)
    vr = env.host_string
    getMsg = functools.partial(_getFormatedMessage, mNum, vr)

    # キーに対応するルーター&ネットワーク情報を取り出し
    targets = filter((lambda d: d['instance_name']==vr), datas)
    if len(targets) != 1:
        msg = u'紐づく対象データがありませんでした。%s' % (vr)
        logging.error(getMsg(msg)); res['msg'] = msg
        _printResults(startTime, 'isolated', mNumber, res)
        return res

    # 1つのみ使用する
    data = targets[0]

    # HACK:デバッグ用にVRを限定
    # if data['network_name'] != 'sy-nkmc-vr-net':
    #         logging.debug(getMsg(u'拒否対象: %s' % data['network_name']))
    #         res['msg'] = "DEBUG!! stop proccess"
    #         return res

    logging.info(getMsg(u'router %s' % data))
    try:
            networkUuid = data['network_uuid']
            networkOfferingUuId = env.networkOfferings['isolated']['uuid']

            # 現在のネットワークの取得
            startTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            before_networks = _csListNetworks(networkUuid)
            if (not before_networks) or before_networks['count'] != 1:
                    msg = u'現在のネットワークが取得できませんでした。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            logging.debug(blue("before_networks" + str(before_networks)))

            # 現在のネットワークオファリングの確認
            before_network = before_networks['network'][0]
            if networkOfferingUuId == before_network['networkofferingid']:
                    msg = u'既にネットワークオファリングが更新されています。%s'
                    msg = msg % (before_network['networkofferingid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            logging.debug(blue("before_network" + str(before_network)))

            # 計測開始
            startTime = datetime.now()

            # 仮想ルータの破棄(VMWare)
#            result = _csDestroyRouter(data['instance_uuid'])
            vm_name = data['instance_name']
            logging.debug(red("Deleing VM name: " + vm_name))
            result = do_destroy_vm(mNum, vm_name)
# TODO: do_destroy_vm でリザルトを返すように実装する            
            if not result:
                    msg =u'ルーターの破棄に失敗しました。%s' % data['instance_uuid']
                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

# TODO: Test
            # 仮想ルータの破棄(旧環境)
            # DB上にはVRが存在するが実態はホスト上に無い(VRアップグレード時に削除されてしまっているため)
            if env.vr_revert['destroyOldVR']:
                result = _csDestroyRouter(data['instance_uuid'])
                if not result:
                    msg =u'ルーターの破棄に失敗しました。%s' % data['instance_uuid']
                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

            # ネットワークの更新
            result = _csUpdateNetwork(networkUuid, networkOfferingUuId)
            if not result:
                    msg = u'ネットワークの更新に失敗しました。%s, %s' % (networkUuid, networkOfferingUuId)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # ネットワークの取得
            networks = _csListNetworks(networkUuid)
            if (not networks) or networks['count'] != 1:
                    msg = u'ネットワークが取得できませんでした。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # ネットワークオファリング更新の確認
            network = networks['network'][0]
            if networkOfferingUuId != network['networkofferingid']:
                    msg = u'ネットワークオファリングが一致しませんでした。'
                    msg = msg % (networkOfferingUuId, network['networkofferingid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータの取得
            routers = _csListRouters(networkUuid)
            if (not routers) or routers['count'] != 1:
                    msg = u'ルーターが取得できませんでした。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータの状態確認
            router = routers['router'][0]
            res['vr_name'] = router['name']
#
# 切戻しの際はVMWareで動くとは限らないのでチェックはしない
#            if not _checkRouterOnVMWare(router):
#                    msg = u'ルーターがVMWare上で稼働していません。%s' % (router['hypervisor'])
#                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

            if not _checkRouterRunning(router):
                    msg = u'ルーターがRunningでありません。%s' % (router['state'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            if not _checkRouterNetwork(router, networkUuid):
                    msg = u'ルーターの稼働ネットワークIDが一致しません。%s' % (router['guestnetworkid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータ内の設定確認
            linkLocalIp = router['linklocalip']
            logging.info(getMsg("linkLocalIp: %s" % linkLocalIp))
            # VR内にログインするにはAPサーバ上での実行が必要
            #_runCommandsOnIsolated(linkLocalIp, res['vr_name'])
            _runCommandsOnIsolatedRevert(linkLocalIp, res['vr_name'], router)

            # 計測終了
            endTime = datetime.now()
            result = _checkExecutionTime(startTime, endTime, int(env.flags["executionThreshold"]))
            if result["status"]:
                logging.info(green("OK" + " Time: " + str(result["time"])))
            else:
                logging.warn(red("Excecution takes too long: [%s:%s]  Time: %s") % (
                    mNum,
                    router['name'],
                    str(result["time"])))
    except Exception as e:
            msg = str(e)
            logging.error(getMsg(msg))
            res['msg'] = msg
            res['status'] = 'NG'
    else:
            logging.info(getMsg('%s ... finish' % res['vr_name']))
            res['status'] = 'OK'
    finally:
            return res

@task
@runs_once
def sharedRevert(filepath="specialDomains.txt"):
    '''
    shared 仮想ルータの切戻し
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('shared revert start.')
    startTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    execution_filepath = env.flags["executionFile"]
    # M番号
    mNumbers = _loadMNnumbers(filepath)
    if not mNumbers:
        logging.warn("No MNumber found in " + filepath + ".")
        exit(-1)

    # M番号単位で処理を行う（多重処理もM番号内で行う
    for mNumber in mNumber.s:
        logging.info(u'[%s:] start ...' % mNumber)
        if not _checkExecutionFile(execution_filepath):
            logging.warn(red(execution_filepath + " not found. stop execution at MNumber: " + mNumber))
            break

        # M番号に紐づくルータの取得
#        datas = _getSharedRouterAndNetworkIds(mNumber)
        datas = _getRouterAndNetworkIdsForRevert(mNumber)
        if not datas:
            msg = u'[%s:] Shared のルータが取得できませんでした。' % (mNumber)
            logging.error(msg)
            continue

        # ここからはfabricの並列処理の仕組みを無理やり使う
        # instaceのキー配列を作る
        vmids = map((lambda d: d['instance_name']), datas)
        # instanceのキー配列を、タスクの実行hostsとして設定する
        env.hosts = vmids
        # 紐づける値datasを渡して、並列実行タスクを呼び出す
        results = execute(do_shared_revert, mNumber, datas)
        _printResults(startTime, 'shared', mNumber, results)

    logging.info('shared revert finish.')

##
# @parallelはWindowsでは動作しない
# pool_sizeで平行処理数を指定できる
@parallel(pool_size=15)
def do_shared_revert(mNum, datas):
    logging.getLogger().addHandler(stream_log)
    logging.debug('do_shared_revert.')
    res = {'status': 'NG', 'msg': ''}

    # タスクの実行ホスト(ここでは実在しないinstanceのキーが入っている)
    vr = env.host_string
    getMsg = functools.partial(_getFormatedMessage, mNum, vr)

    # キーに対応するルーター&ネットワーク情報を取り出し
    targets = filter((lambda d: d['instance_name']==vr), datas)
    if len(targets) != 1:
        msg = u'紐づく対象データがありませんでした。'
        logging.error(getMsg(msg)); res['msg'] = msg
        return res

    # 1つのみ使用する
    data = targets[0]

    # HACK:デバッグ用にVRを限定
    #if data['network_name'] != 'IntraM02_1':
    # if not 'IntraM02_1' in data['network_name'] :     # IntraM02_1*
    #         logging.debug(getMsg(u'拒否対象: %s' % data['network_name']))
    #         res['msg'] = "DEBUG!! stop proccess"
    #         return res

    logging.info(getMsg(u'router %s' % data))

    try:

            networkUuid = data['network_uuid']
            networkOfferingUuId = env.networkOfferings['shared']['uuid']

            # 現在のネットワークの取得
            before_networks = _csListNetworks(networkUuid)
            if (not before_networks) or before_networks['count'] != 1:
                    msg = u'現在のネットワークが取得できませんでした。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 現在のネットワークオファリングの確認
            before_network = before_networks['network'][0]
            if networkOfferingUuId == before_network['networkofferingid']:
                    msg = u'既にネットワークオファリングが更新されています。%s' 
                    msg = msg % (before_network['networkofferingid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 計測開始
            startTime = datetime.now()

            # 仮想ルータの破棄
#            result = _csDestroyRouter(data['instance_uuid'])
            logging.debug(red("VM name: " + env.vr_revert['vmPrefix'] + data['instance_name']))
            result = do_destroy_vm(mNum, data['instance_name'])
# TODO            
            if not result:
                    msg = u'ルーターの破棄に失敗しました。%s' % (data['instance_uuid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

# TODO: Test
            # 仮想ルータの破棄(旧環境)
            # DB上にはVRが存在するが実態はホスト上に無い(VRアップグレード時に削除されてしまっているため)
            if env.vr_revert['destroyOldVR']:
                result = _csDestroyRouter(data['instance_uuid'])
                if not result:
                    msg =u'ルーターの破棄に失敗しました。%s' % data['instance_uuid']
                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

            # ネットワークオファリングの更新
            updatedNomally = _updateAndCheckNetworkId(data['network_id'])
            if not updatedNomally:
                    msg = u'ネットワークオファリングIDが更新されませんでした。%s' % (data['network_id'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # ネットワークの再起動
            result = _csRestartNetwork(networkUuid)
            if not result:
                    msg = u'ネットワークの再起動に失敗しました。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータの取得
            routers = _csListRouters(networkUuid)
            if (not routers) or routers['count'] != 1:
                    msg = u'ルーターが取得できませんでした。%s' % (networkUuid)
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータの状態確認
            router = routers['router'][0]
            res['vr_name'] = router['name']
# TODO: 切戻し時にはVMWareの確認は不要？ 
#            if not _checkRouterOnVMWare(router):
#                    msg = u'ルーターがVMWare上で稼働していません。%s' % (router['hypervisor'])
#                    logging.error(getMsg(msg)); res['msg'] = msg
#                    return res

            if not _checkRouterRunning(router):
                    msg = u'ルーターがRunningでありません。%s' % (router['state'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            if not _checkRouterNetwork(router, networkUuid):
                    msg = u'ルーターの稼働ネットワークIDが一致しません。%s' % (router['guestnetworkid'])
                    logging.error(getMsg(msg)); res['msg'] = msg
                    return res

            # 仮想ルータ内の設定確認
            linkLocalIp = router['linklocalip']
            logging.info(getMsg("linkLocalIp: %s" % linkLocalIp))
            _runCommandsOnSharedRevert(linkLocalIp, res['vr_name'], router)

            # 計測終了
            endTime = datetime.now()
            result = _checkExecutionTime(startTime, endTime, int(env.flags["executionThreshold"]))
            if result["status"]:
                logging.info(green("OK" + " Time: " + str(result["time"])))
            else:
                logging.warn(red("Excecution takes too long: [%s:%s]  Time: %s") % (
                    mNum,
                    router['name'],
                    str(result["time"])))
    except Exception as e:
            msg = str(e)
            logging.error(getMsg(msg))
            res['msg'] = msg
            res['status'] = 'NG'
    else:
            logging.info(getMsg('%s ... finish' % res['vr_name']))
            res['status'] = 'OK'
    finally:
            return res

def _loadMNnumbers(filepath):
    '''
    M番号ファイルの読み込み
    '''
    logging.getLogger().addHandler(stream_log)
    logging.info('load mNumber file. %s' % filepath)

    if not os.path.exists(filepath): return False
    try:
            with open(filepath) as f:
                    lines = f.readlines()
    except:
            logging.error(u'M番号ファイルを開けません。%s' % filepath)
            exit(1)

    mFiles = []
    for line in lines:
            # 空行、コメント行を無視する(=先頭文字Mの行を有効とする)
            if line[0:1] == 'M':
                    # チェックフラグで動作を変更
                    if env.flags['checkMNumbers']:
                            if not re.match(r'^M[0-9]{8}$', line):
                                    logging.error(u'M番号はMで始まる9桁の数値を指定してください。%s' % line)
                                    return False
                    mFiles.append(line.rstrip())

    return mFiles

def _getFormatedMessage(mNum, vr, msg):
    '''
    ログファイル用メッセージの生成
    '''
    return "[%s:%s] %s" % (mNum, vr, msg)

def _printResults(startTime, vrType, mNumber, results):
    '''
    結果の出力
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('_printResults.')

    endTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    path = 'results.log'
    with open(path, 'a') as f:
            f = codecs.lookup('utf_8')[-1](f)
            f.write("[%s] %s, %s\n" % (startTime, vrType, mNumber))
            for k, v in results.items():
                    msg = "%s %s -> %s\t%s" % (v['status'], k, v.get('vr_name',''), v['msg'])
                    print msg
                    f.write("%s\n" % msg.replace('\r', ''))
            f.write("[%s] ... finish\n" % (endTime))

def _getIsolatedRouterAndNetworkIds(mNumber):
    '''
    Isloatedのルータとネットワークを取得
    '''
    sql =     "SELECT" \
                    " v.id, v.uuid, nw.id, nw.uuid, v.name, nw.name" \
                    " FROM vm_instance v" \
                    " INNER JOIN nics n ON v.id = n.instance_id" \
                    " INNER JOIN networks nw ON n.network_id = nw.id" \
                    " INNER JOIN domain d ON nw.domain_id = d.id" \
                    " WHERE" \
                    " v.removed IS NULL" \
                    " AND v.vm_type = 'DomainRouter'" \
                    " AND nw.name IS NOT NULL" \
                    " AND nw.guest_type = 'Isolated'" \
                    " AND d.name = '%s'" % (mNumber)

    queryResults = local(_createMySQLCommand(sql), capture=True)

    ids = []
    if queryResults:
            for result in _nlSplit(queryResults):
                    resultList = result.split('\t')
                    ids.append({ 'instance_id':     resultList[0],
                                             'instance_uuid': resultList[1],
                                             'network_id':        resultList[2],
                                             'network_uuid':    resultList[3],
                                             'instance_name': resultList[4],
                                             'network_name':    resultList[5] })
    return ids

def _getSharedRouterAndNetworkIds(mNumber):
    '''
    Sharedのルータとネットワークを取得
    '''
    sql =     "SELECT" \
                    " v.id, v.uuid, nw.id, nw.uuid, v.name, nw.name" \
                    " FROM vm_instance v" \
                    " INNER JOIN nics n ON v.id = n.instance_id" \
                    " INNER JOIN networks nw ON n.network_id = nw.id" \
                    " INNER JOIN domain_network_ref dnr ON nw.id = dnr.network_id" \
                    " INNER JOIN domain d ON dnr.domain_id = d.id" \
                    " WHERE" \
                    " v.removed IS NULL" \
                    " AND v.vm_type = 'DomainRouter'" \
                    " AND nw.guest_type = 'Shared'" \
                    " AND d.name = '%s' " % (mNumber)

    queryResults = local(_createMySQLCommand(sql), capture=True)

    ids = []
    if queryResults:
            for result in _nlSplit(queryResults):
                    resultList = result.split('\t')
                    ids.append({ 'instance_id':     resultList[0],
                                             'instance_uuid': resultList[1],
                                             'network_id':        resultList[2],
                                             'network_uuid':    resultList[3],
                                             'instance_name': resultList[4],
                                             'network_name':    resultList[5] })
    return ids

def _getRouterAndNetworkIdsForRevert(mNumber):
    '''
    Shared/Isolatedのルータとネットワークを取得(Revert用)

    :param str nMumber: M番号
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _getRouterAndNetworkIdsForRevert.')

    ids = []
    for conf in env.vr_config:
        if not conf[KEY_M_NUMBER] == mNumber:
            continue

        vr_name = conf[KEY_VR_NAME]
        logging.debug("VR Name: " + green(conf[KEY_VR_NAME]))
        zone_found_flag = False
        password = None
        for zc in env.zone_config:
            if conf[KEY_ZONE] == zc[KEY_ZONE_CLUSTER_NAME]:
                host = zc[KEY_ZONE_PORTFORWARD_HOST]
                port = zc[KEY_ZONE_PORTFORWARD_PORT]
                user = zc[KEY_ZONE_VCENTER_USER]
                password = zc[KEY_ZONE_VCENTER_PASS]
                ids.append({ 'instance_id':   conf[KEY_VR_NAME], # TODO: IDは必要?
                             'instance_uuid': conf[KEY_UUID],
                             'network_id':    conf[KEY_NETWORK_ID],
                             'network_uuid':  conf[KEY_NETWORK_UUID],
                             'instance_name': conf[KEY_VR_NAME],
                             'network_name':  conf[KEY_NETWORK_NAME],})
                zone_found_flag = True
                break

        if not zone_found_flag:
            logging.error(red("Zone(" + conf[KEY_ZONE] + ") が見つかりません: VM Name: " + vr_name))
            continue
  
    return ids

def _updateAndCheckNetworkId(networkId):
    '''
    ネットワークのネットワークオファリングの更新と確認(Shared)
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _updateAndCheckNetworkId.')
    # 対象NWをDBより確認
    beforeNetworkOfferingId = _getNetworkOfferingId(networkId)
    if not beforeNetworkOfferingId:
        return False

    # 対象ネットワークへupdate。(Sharedのみ)
    networkOfferingId = env.networkOfferings['shared']['id']
    _updateNetwork(networkId, networkOfferingId)

    # 対象NWをDBより確認
    afterNetworkOfferingId = _getNetworkOfferingId(networkId)
    if not afterNetworkOfferingId:
            return False

    # オファリングが変更されていることを確認
    if beforeNetworkOfferingId != afterNetworkOfferingId:
            return True
    else:
            return False

def _checkRouterOnVMWare(router):
    '''
    ネットワークオファリング更新後の確認(HyperVisor)
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _checkRouterOnVMWare.')
    # 更新後はVMWare上に起動しているはず
    return router['hypervisor'] == 'VMware'

def _checkRouterOnKVM(router):
    '''
    VRがKVM上にあるかどうかをチェックする。
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _checkRouterOnKVM.')
    # KVM上にあるか確認
    return router['hypervisor'] == 'KVM'

def _checkRouterRunning(router):
    '''
    ネットワークオファリング更新後の確認(Running)
    '''
    # 起動していることを確認
    return router['state'] == 'Running'

def _checkRouterNetwork(router, restartedNetworkId):
    '''
    ネットワークオファリング更新後の確認(Network)
    '''
    # ネットワークIDの一致を確認
    return router['guestnetworkid'] == restartedNetworkId

def _getNetworkOfferingId(networkId):
    '''
    Sharedネットワークの確認。
    ネットワークオファリングIDを返す
    '''
    sql = 'SELECT network_offering_id FROM networks WHERE id=%s' % (networkId)
    queryResults = local(_createMySQLCommand(sql), capture=True)

    networkOfferingId = None
    for result in _nlSplit(queryResults):
            resultList = result.split('\t')
            networkOfferingId = resultList[0]

    return networkOfferingId

def _updateNetwork(networkId, networkOfferingId):
    '''
    Sharedネットワークの更新
    '''
    sql = 'UPDATE networks SET network_offering_id=%s WHERE id=%s' % (networkOfferingId, networkId)
    queryResults = local(_createMySQLCommand(sql), capture=True)
    # 戻り値は取れない
    return True

def _createMySQLCommand(query):
    '''
    MySQL実行コマンド文字列の生成
    mysqlクライアントがインストールされ、PATHが通っている必要あり
    '''
    if env.database['password']:
            command = "mysql -h %(host)s --port=%(port)s --user=%(user)s --password=%(password)s --skip-column-names -U cloud -e \"%(query)s\""
    else:
            command = "mysql -h %(host)s --port=%(port)s --user=%(user)s --skip-column-names -U cloud -e \"%(query)s\""

    return command % {
            'host': env.database['host'],
            'port': env.database['port'],
            'user': env.database['user'],
            'password': env.database['password'],
            'query': query }

def _runCommandsOnIsolatedRevert(linklocalIp, hostName, router):
    '''
    Isolated VR内部 確認用コマンドの実行

    :param str linklocalIp: VRのLink Local IP
    :param str hostName: VR名
    :param dict router: 仮想ルータ情報
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _runCommandsOnIsolatedRevert: hostname: ' + hostName + ' linklocalIp: ' + linklocalIp)

    isKvmHost = True if router['hypervisor']  == "KVM" else False
    if isKvmHost:
        # APサーバ -> KMV host からsshを実行
        params = _getVrConnectEnvKVM(router["hostname"], env.kvm_host, env.ap)
        ap_run = functools.partial(_sshCmdKVMHost, linklocalIp) 
    else:
        # バッチサーバからAPサーバへsshし、APサーバ上からVRへssh実行
        params = _getVrConnectEnv(env.ap['host'], env.ap)
        ap_run = functools.partial(_sshCmd, linklocalIp)

    wLog = functools.partial(_writeLog, hostName)

    # 接続先の設定
    with settings(**params):
        with settings(warn_only=True):
            if isKvmHost:
                put(env.vr['key_filename_kvm_local'], env.vr['key_filename_kvm'], mode=0600)

            cmd = 'date; hostname'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/cloudstack-release'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep dns|egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps -ef |egrep cloud |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep apache |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep haproxy |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

#             cmd = "ping -c 3 `netstat -rn|egrep '^0.0.0.0' |awk '{print $2}'`"
#             wLog(cmd, ap_run(cmd))

            cmd = 'netstat -rn'
            wLog(cmd, ap_run(cmd))

            cmd = 'iptables -L -n'
            wLog(cmd, ap_run(cmd))

            cmd = 'mount'
            wLog(cmd, ap_run(cmd))

            cmd = 'df'
            wLog(cmd, ap_run(cmd))

            if isKvmHost:
                run("rm " + env.vr['key_filename_kvm'])
                    
def _runCommandsOnSharedRevert(linklocalIp, hostName, router):
    '''
    Shared VR内部 確認用コマンドの実行(VR切り戻し用)
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _runCommandsOnSharedRevert: hostname: ' + hostName + ' linklocalIp: ' + linklocalIp)

    isKvmHost = True if router['hypervisor']  == "KVM" else False
    if isKvmHost:
        # APサーバ -> KMV host からsshを実行
        params = _getVrConnectEnvKVM(router["hostname"], env.kvm_host, env.ap)
        ap_run = functools.partial(_sshCmdKVMHost, linklocalIp)
    else:
        # バッチサーバからAPサーバへsshし、APサーバ上からVRへssh実行
        params = _getVrConnectEnv(env.ap['host'], env.ap)
        ap_run = functools.partial(_sshCmd, linklocalIp)

    """
    if env.flags['execOnAp']:
            # APサーバからVRへ直接sshを実行
            params = _getVrConnectEnv(linklocalIp, env.vr)
            ap_run = functools.partial(run)
    else:
            # バッチサーバからAPサーバへsshし、APサーバ上からVRへssh実行
            params = _getVrConnectEnv(env.ap['host'], env.ap)
            ap_run = functools.partial(_sshCmd, linklocalIp)
    """

    wLog = functools.partial(_writeLog, hostName)

    # 接続先の設定
    with settings(**params):
        with settings(warn_only=True):
            if isKvmHost:
                put(env.vr['key_filename_kvm_local'], env.vr['key_filename_kvm'], mode=0600)

            cmd = 'date; hostname'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/cloudstack-release'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep dns|egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps -ef |egrep cloud |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep apache |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'netstat -rn'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/dnsmasq.d/cloud.conf'
            wLog(cmd, ap_run(cmd))

            cmd = 'mount'
            wLog(cmd, ap_run(cmd))

            if isKvmHost:
                run("rm " + env.vr['key_filename_kvm'])


def _getVrConnectEnv(host, envTarget):
    '''
    fabricの接続先を切り替えるための一時的なEnv作成
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _getVrConnectEnv.')
    params = {'host_string': host,
                        'port': envTarget.get('port', None),
                        'user': envTarget.get('user', None),
                        'password': envTarget.get('password', None),
                        'key_filename': envTarget.get('key_filename', None),
                        'disable_known_hosts': True
                        }
    return params


def _getVrConnectEnvKVM(kvm_host, envTarget, envGateway):
    '''
    KVM上のVR用のfabricの接続先を切り替えるための一時的なEnv作成

    :param str host: KVMホスト名
    :param list envTarget:
    :param list envGateway:
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _getVrConnectEnvKVM.')
    params = {'host_string': kvm_host,
              'port': envTarget.get('port', None),
              'user': envTarget.get('user', None),
              'password': envTarget.get('password', None),
              'disable_known_hosts': True,
              'gateway': "%s@%s:%s" % (
                             envGateway.get('user', None),
                             envGateway.get('host', None),
                             envGateway.get('port', None))
             }
    return params

def _sshCmd(linklocalip, cmd):
    '''
    踏み台からVR内部 確認用コマンドの実行
    http://stackoverflow.com/questions/12641514/switch-to-different-user-using-fabric/12648391#12648391
    http://stackoverflow.com/questions/19663476/su-root-with-python-fabric
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _sshCmd.')

    ssh_cmd = "ssh %s -l %s -i %s -p %s -q -o \"StrictHostKeyChecking=no\" \"%s\" "
    ssh_cmd = ssh_cmd % (
            linklocalip,
            env.vr['user'],
            env.vr['key_filename'],
            env.vr['port'],
            cmd)
    return run(ssh_cmd)

def _sshCmdKVMHost(linklocalip, cmd):
    '''
    踏み台 -> KVM HostからVR内部 確認用コマンドの実行
    http://stackoverflow.com/questions/12641514/switch-to-different-user-using-fabric/12648391#12648391
    http://stackoverflow.com/questions/19663476/su-root-with-python-fabric
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _sshCmdKVMHost.')

    ssh_cmd = "ssh %s -l %s -i %s -p %s -q -o \"StrictHostKeyChecking=no\" \"%s\" "
    ssh_cmd = ssh_cmd % (
            linklocalip,
            env.vr['user'],
            env.vr['key_filename_kvm'],
            env.vr['port'],
            cmd)
    logging.debug(yellow("ssh command: " + ssh_cmd))
    return run(ssh_cmd)

def _writeLog(host, cmd, ouput):
    '''
    コマンド実行ログの出力
    '''
    path = 'logs/%s.txt' % host
    with open(path, 'a') as f:
            f.write("\n[%s]\n" % cmd)
            f.write("%s\n" % ouput)

def _csDestroyRouter(instanceUuid):
    '''
    ルータを破棄する
    '''
    cs = _connectCloudStack()
    # ルータの破棄
    jobresult = cs.destroyRouter(id=instanceUuid)
    # jobの実行確認
    return _csQueryAsyncJobResult(jobresult['jobid'], 20)

def _csUpdateNetwork(networkUuid, offeringUuid):
    '''
    ルーターのネットワークを更新する
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _csUpdateNetwork.')

    cs = _connectCloudStack()
    # ネットワークの更新
    jobresult = cs.updateNetwork(id=networkUuid, networkofferingid=offeringUuid)
    # jobの実行確認
    return _csQueryAsyncJobResult(jobresult['jobid'], 40)

def _csRestartNetwork(networkUuid):
    '''
    ルータ所属のネットワークを再起動する
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _csRestartNetwork.')

    cs = _connectCloudStack()
    # ネットワークの再起動
    jobresult = cs.restartNetwork(id=networkUuid, cleanup=True)
    # jobの実行確認
    return _csQueryAsyncJobResult(jobresult['jobid'], 60)

def _csListNetworks(networkUuid):
    '''
    ルータのネットワーク情報を取得する
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _csListNetworks.')

    cs = _connectCloudStack()
    return cs.listNetworks(id=networkUuid, listall=True)

def _csListRouters(networkUuid):
    '''
    ルータの情報を取得する
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _connectCloudStack.')

    cs = _connectCloudStack()
    routers = cs.listRouters(networkid=networkUuid, listall=True)
    return routers

def _csGetRouter(routerName):
    '''
    ルータの情報を取得する

    :param str routerName: ルータのVM名
    '''
    cs = _connectCloudStack()
    router = cs.listRouters(name=routerName, listall=True)
    return router

def _csQueryAsyncJobResult(jobid, interval=10, retryCount=30):
    '''
    非同期APIの実行結果を問い合わせる
    応答が0の場合は、interval秒待機し、再度問い合わせる。
    応答が2の場合は、エラーとして例外を発生させる。
    上記以外の応答は、結果を読み込み戻り値として返す。
    interval * retryCount の秒数をすぎるとタイム・アウトとして例外を発生させる
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _csQueryAsyncJobResult.')

    cs = _connectCloudStack()
    count = 1
    result = {'jobstatus': 0}

    while result['jobstatus'] == 0:
            time.sleep(interval)
            result = cs.queryAsyncJobResult(jobid=jobid)
            if result['jobstatus'] == 2:
                    raise Exception('async job error. %s jobid: %s' % (result['jobresult'], jobid))
            count += 1
            if count > retryCount:
                    raise Exception('async job timeout. jobid: %s' % jobid)
            logging.debug("jobstatus %s looping... jobid:%s" % (result['jobstatus'], result['jobid']))

    return result

def _connectCloudStack():
    '''
    CloudStackオブジェクトの作成
    '''
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _connectCloudStack.')

    # Root Admin
    cs = CloudStack(endpoint=env.csApi['endpoint'], key=env.csApi['key'], secret=env.csApi['secret'])
    return cs

def _nlSplit(strval):
    '''
    改行で文字列を分割
    実行OSによってMySQLクライアントの改行コードが違い、適切に分割できないため
    '''
    if env.flags['isLinux']:
            return strval.split('\n')
    else:     # Windows
            return strval.split('\r\n')

def _checkExecutionFile(filepath):
    """
    指定したファイルが存在するかチェックする。

    :param str filepath: チェックするファイルのパス
    :rtype: bool
    :return: True - ファイルが存在する, False - ファイルが存在しない
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Checking file: " + filepath)
    return os.path.isfile(filepath)

def _checkExecutionTime(startTime, endTime, threshold):
    """
    時間内に処理できているかチェックする。

    :param datetime startTime: 処理開始時間
    :param datetime endTime: 処理終了時間
    :param int 閾値: 閾値(分)
    :rtype: bool
    :return: True - 問題なし, False - 閾値を超えている
    """
    logging.getLogger().addHandler(stream_log)

    threshold = timedelta(minutes=threshold)
    time = endTime - startTime

    logging.info("threashold: " + str(threshold))
    if time >= threshold:
#        logging.warn(red("Excecution takes: " + str(time)))
        return {"status": False, "time": time}
    else:
        return {"status": True, "time": time}


##############################
## Revert Tool
##############################
def _vrHostMapping():
    """
    ルータとホストのMappingを取得する

    :rtype:  list
    :return: VRとホスト名を含むリスト
      含まれるキー名
        vm_name
        link_local_ip
        hostname
        state
        hypervisor
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _vrHostMapping')

    cs = _connectCloudStack()
    routers = cs.listRouters(listall=True)
    
    # VMのStateがStoppedの場合、hostnameはNullになる
    vr_list = map(lambda d: {"vm_name": d['name'], 
                             "link_local_ip": d.get('linklocalip'), 
                             "hostname": d.get('hostname'),
                             "domain": d['domain'],
                             "state": d['state'],
                             "hypervisor": d.get('hypervisor')}, routers["router"])

    return vr_list

@task
def load_config(acs49vrlist="acs49VRList.txt",zone_config_file="zone_config.txt"):
    """
    VR切戻し用設定ファイル(acs49vrlist, zone_config.txt)を読み込む

    :param str acs49vrlist: 切り戻し対象のVR情報を含む設定ファイル
    :param str zone_config_file: Zone毎のvCenter情報)を含む設定ファイル
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Execute load_config.")

    vr_config = []
    f = open(acs49vrlist, "r")
    reader = csv.reader(f)
#     header = next(reader)
    for row in reader:
        if len(row) > 0:
            vr_config.append(_load_vr_config(row))

    f.close()

    env.vr_config = vr_config

    zone_config = []
    f = open(zone_config_file, "r")
    reader = csv.reader(f)

    for row in reader:
        zone_config.append(_load_zone_config(row))
    f.close()

    env.zone_config = zone_config
    env.vr_host_mapping = _vrHostMapping()

@task
def vr_check(vr_name):
    """
    指定したVRの状態をチェックする。

    :param str vr_name: コピー先のVR名
    """
    routers = _csGetRouter(vr_name)
    if (not routers) or routers['count'] != 1:
        logging.error(red("ルーターが取得できませんでした。%s'" % (vr_name)))

    print(yellow(str(routers)))

    router = routers['router'][0]
    nw_type = _getNetworkType(router)
    if router['hypervisor'] == "VMware":
        if nw_type == "Shared":
            _runCommandsOnSharedRevert(router["linklocalip"], router['name'], router)
        elif nw_type == "Isolated":
            _runCommandsOnIsolatedRevert(router["linklocalip"], router['name'], router)
    elif router['hypervisor'] == "KVM":
        if nw_type == "Shared":
            _runCommandsOnSharedRevert(router["linklocalip"], router['name'], router)
        elif nw_type == "Isolated":
            _runCommandsOnIsolatedRevert(router["linklocalip"], router['name'], router)

def _load_vr_config(row):
    """
    読み込んだVR設定をパースする。

    :param str row: 設定ファイルから読み込んだカンマ区切りのVR情報
    :rtype: dict
    :return VR情報を含むdictオブジェクト
    """
    print("row: " + str(row))

    val = {KEY_VR_NAME:row[0].strip(),
           KEY_M_NUMBER:row[1].strip(),
           KEY_ZONE:row[2].strip(),
           KEY_NET_GUEST_TYPE:row[3].strip(),
#           KEY_ID:row[4].strip(),
           KEY_UUID:row[4].strip(),
           KEY_CS_PRIVATE_IP:row[5].strip(),
           KEY_NETWORK_ID:row[6].strip(),
           KEY_NETWORK_UUID:row[7].strip(),
           KEY_NETWORK_NAME:row[8].strip()}
    return val

def _load_zone_config(row):
    """
    読み込んだゾーン設定をパースする。

    :param str row: 設定ファイルから読み込んだカンマ区切りのゾーン情報
    :rtype: dict
    :return ゾーン情報を含むdictオブジェクト
    """
    val = {KEY_ZONE_CLUSTER_NAME:row[0].strip(), 
           KEY_ZONE_VCENTER_IP:row[1].strip(), 
           KEY_ZONE_PORTFORWARD_HOST:row[2].strip(), 
           KEY_ZONE_PORTFORWARD_PORT:row[3].strip(),
           KEY_ZONE_VCENTER_USER:row[4].strip(),
           KEY_ZONE_VCENTER_PASS:row[5].strip()}
    return val

def do_destroy_vm(mNumber, vm_name):
    """
    指定したVMを削除する。(HostはVMWare)

    :param str mNumber: M番号
    :param str vm_name: 削除するVM名
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Executing do_destroy_vm: MNumber: " + mNumber + " VM Name: " + vm_name)

    vm_list = _get_vr_info(mNumber, vm_name)

    res = None
    for vm in vm_list:
        vr_name = vm[KEY_VR_NAME]
        logging.info("VM Name: " + green(vm[KEY_VR_NAME]))
        zc = _get_vcenter_info(vm[KEY_ZONE])

        if zc is None:
            logging.error(red("Zone(" + vm[KEY_ZONE] + ") 設定が見つかりません: VM Name: " + vr_name))
            continue
        else:
            res = _destroy_vm_on_vmware(zc[KEY_ZONE_VCENTER_USER], zc[KEY_ZONE_VCENTER_PASS],
                                  zc[KEY_ZONE_PORTFORWARD_HOST], zc[KEY_ZONE_PORTFORWARD_PORT],
                                  env.vr_revert["pyvmomi-community-samples-path"], env.vr_revert['vmPrefix'] + vr_name)
            #TODO: Test
            # 同一名のVRは一台しかないのでループから抜ける
            break

    return res

def _get_vr_info(mNumber, vm_name):
    """
    VRの情報を取得する。

    :param str mNumber: M番号
    :param str vm_name: VM名
    :rtype list
    :return VM情報
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Executing _get_vr_info.")

    val = []
    for conf in env.vr_config:
        if mNumber == conf[KEY_M_NUMBER] and vm_name == conf[KEY_VR_NAME]:
            logging.debug("Found VM for: " + green(conf[KEY_M_NUMBER]) + " VM Name: " + green(conf[KEY_VR_NAME]))
            val.append(conf)

    return val

def _get_vcenter_info(zone):
    """
    設定ファイルからゾーン情報を取得する。

    :param str zone: ゾーン名
    :rtype dict
    :return ゾーン情報
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Executing _get_vcenter_info.")

    for zc in env.zone_config:
        if zone == zc[KEY_ZONE_CLUSTER_NAME]:
            return zc

    logging.error(red("Zone(" + zone + ") が見つかりません"))

    return None

def _destroy_vm_on_vmware(user, password, host, port, pyvmomi_community_samples_path, vm_name):
    """
    VMWare上のVMを削除する。

    :param str user: vCenterユーザ
    :param str password: vCenterパスワード
    :param str host: vCenterホスト名またはIP
    :param str port: vCenterポート番号
    :param str pyvmomi_community_samples_path: pyvmomi communit samples がインストールされているパス
    :param str vm_name: 削除するVM名
    :rtype None
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Executing _destroy_vm_on_vmware.")

    command = "python %s/destroy_vm.py -s %s -o %s -u %s -p %s -v %s" % (
            pyvmomi_community_samples_path,
            host,
            str(port),
            user,
            password,
            vm_name)
    res = None
    try:
        logging.info(green("deleting vm: " + vm_name))
        logging.debug("Executing command: " + command)
        if local(command).failed:
            logging.error(red("destroy_vm.py command failed."))
        else:
            logging.info(green("destroy_vm.py command success"))
            # CloudStack APIの成功ステータスを返す
            res = {"jobstatus": 1}
    except Exception as e:
        print(e)
    finally:
        return res

def dump_vr_config():
    """VR Configの設定内容を表示する"""

    for v in env.vr_config:
        print(yellow("VM Name: " + v[KEY_VR_NAME] + " -------------------------------------------------"))
        print("  VM Name: %s\n  MNumber: %s \n  Zone: %s\n  Guest Type: %s" + 
              "\n  UUID: %s\n  CS Private IP: %s" +
              "\n  Network ID: %s\n  Network UUID: %s\n  Network Name: %s") % (
                v[KEY_VR_NAME],
                v[KEY_M_NUMBER],
                v[KEY_ZONE],
                v[KEY_NET_GUEST_TYPE],
                v[KEY_UUID],
                v[KEY_CS_PRIVATE_IP],
                v[KEY_NETWORK_ID],
                v[KEY_NETWORK_UUID],
                v[KEY_NETWORK_NAME])

def dump_zone_config():
    """Zone Configの設定内容を表示する"""

    for v in env.zone_config:
        print(yellow("Zone Name: " + v[KEY_ZONE_CLUSTER_NAME] + " -------------------------------------------------"))
        print("  Zone Name: %s \n  vCenter IP: %s" +  
              "\n  vCenter User: %s \n  vCenter Pass: %s" + 
              "\n  Portforwarding Host: %s \n  Portforwarding Port: %s") % (
                 v[KEY_ZONE_CLUSTER_NAME],
                 v[KEY_ZONE_VCENTER_IP],
                 v[KEY_ZONE_VCENTER_USER],
                 v[KEY_ZONE_VCENTER_PASS],
                 v[KEY_ZONE_PORTFORWARD_HOST],
                 v[KEY_ZONE_PORTFORWARD_PORT])


#
# TODO: このチェック方法が正しいのか不明
#
def _getNetworkType(router):
    for nic in router["nic"]:
        if nic["traffictype"] == "Guest":
            return nic["type"]

    return None

if __name__ == '__main__':
    # メイン処理
    # デバッグ用途
    setEnv()
    networkUuid = 'd3297343-80bb-4fdb-8912-036f605c2d0a'
    print "networks"
    print _csListNetworks(networkUuid)
    print "routers"
    print _csListRouters(networkUuid)
