# ACS4.9対応 VR切戻し支援ツール

## 使い方 
仮想ルータの切戻し(ACS4.9 -> ACP307)を簡易に行えるようにしたスクリプトです。

```
fab setEnv[:config=環境設定ファイル] load_config[:acs49vrlist=削除対象VR一覧ファイル,zone_config_file=ゾーン情報設定ファイル] アップグレード種別[:filepath=M番号ファイル]
```

にてアップグレードコマンドを実行できます。  
アップグレード種別は ** sharedRevert ** または ** isolatedRevert ** を指定できます。  

** コマンド実行例) **
> $ fab setEnv:config=revert.yml load_config:acs49vrlist=acs49VRList.txt,zone_config_file=zone_config.txt isolatedRevert:filepath=specialDomains.txt

### アップグレード種別

アップグレード種別は

* sharedRevert
* isolatedRevert

を指定できます。  
※重要顧客はiisolatedRevertを指R定してください。

#### :filepath M番号ファイル

アップグレード対象を、仮想ルータを使用しているドメイン（M番号）のファイルで指定します。  

ファイルの書式は以下のとおりです。

	## M numbers (domains) listed here will be EXCLUDED from VR restart operations by the k-vrouteradm script.
	## Each M number should be listed on a line by itself.
	## Blank lines and comments (lines beginning with '#') are ignored.

	M00000001
	M00000002
	#M00000111

ファイル未指定の場合は、** specialDomains.txt ** のファイルを読み込み使用します。


### :config 環境設定ファイル

環境設定ファイルには、実行に必要な各種接続先情報やアップグレードするネットワークオファリングの情報をYAML形式で指定します。

記述例)
```
vr:
  user: root
  key_filename: /var/cloudstack/management/.ssh/id_rsa
  port: 3922
ap:
  host: 172.27.172.155
  user: tckkcl
  password: ***
  port: 22
csApi:
  endpoint: http://172.27.172.155:80/client/api
  key: ***
  secret: ***
database:
  host: 172.27.172.163
  user: root
  password:
  port: 3306
networkOfferings:
  isolated:
    name: DefaultIsolatedNetworkOfferingWithSourceNatService(VR_200)
    id: 53
    uuid: c9b07ee9-5f2d-48cd-b4ed-74b0f227113c
  shared:
    name: m128_DefaultSharedNetworkOffering(VR_1000)
    id: 57
    uuid: 64b71e9c-b2f5-4f45-b1d4-034a87ff1231
flags:
  checkMNumbers: False
  isLinux: True
  execOnAp: True
vr_revert:
  pyvmomi-community-samples-path: ${HOME}/workspace/pyvmomi-community-samples/samples/
  vmPrefix: acs49_
```

ファイル未指定の場合は、 ** develop.yml ** のファイルを読み込み使用します。

#### 設定項目の説明

ファイルの書式は以下のとおりです。

分類	|項目名	|	説明	| 設定例
:-|:-|:-|:-
vr	|		|仮想ルータのSSH接続	|
		|user	|ユーザ名	|root
		|key_filename	|秘密鍵パス	|/root/.ssh/id_rsa.cloud
		|key_filename_kvm	|KVM上のVRにログインするために使用する秘密鍵のKVMホスト上のパス(一時的に鍵を置いておく)	|/home/ckkcl/id_rsa.cloud
		|key_filename_kvm_local	|KVM上のVRにログインするために使用する秘密鍵のfabric実行ホスト上のパス	|/home/ckkcl/id_rsa.cloud
		|port	|ポート	|3922
ap  |     |APサーバのSSH接続  |
    |host |ホスト  |...
    |user |ユーザ名 |tckkcl
    |password |パスワード  |...
    |port |ポート  |22
csApi	|		|CloudStackのAPI接続	|
		|endpoint	|ベースURL	|http://localhost:43325/client/api
		|key	|実行ユーザーキー	|...
		|secret	|秘密キー	|...
database|		|CloudStackのデータベース接続	|
		|host	|ホスト	|...
		|user	|ユーザ名	|root
		|password	|パスワード	|未指定時はパスワードなしで接続
		|port	|ポート	|3306
networkOfferings	|	|アップグレード設定	|
		|isolated	|Isolatedの更新情報	|
		|shared		|Sharedの更新情報	|
		|*:name		|更新後のネットワークオファリング名称	|m128_DefaultSharedNetworkOffering(VR_1000)
		|*:id		|更新後のネットワークオファリングID		|57
		|*:uuid		|更新後のネットワークオファリングUUID	|64b71e9c-b2f5-4f45-b1d4-034a87ff1231
flags	|		|処理制御のためのフラグ	|
		|checkMNumbers	|M番号の数値部分のチェックを行う	|True
    |isLinux        |Linuxでの実行フラグ(Falseを指定するとWindows実行) |True
    |execOnAp       |VRへのSSHログインをAPサーバ上から行うフラグ(Falseを指定すると別サーバからAPサーバへSSHログインした後、APサーバからVRにSSHを行う) |True
vr_revert	|		|切戻し処理用フラグ	|
		|pyvmomi-community-samples-path |pyvmomi-community-samples-pathのインストール先のパス|
    |vmPrefix |vCenterから削除するVM名のprefix | _acs49
    |destroyOldVR |VR(VMWare上の)削除時にACP307環境のVR破棄処理を実行するか| False


## アップグレード実行結果の見方

アップグレード結果は以下に出力されます。

* コンソール出力
* 結果ログ(results.log)
* 仮想ルータチェックログ(logs/VR-**.txt)
* 操作ログ(operaion.log)


### コンソール出力

処理実行の途中経過を出力します。処理自体に進捗があるかを確認します。  
また、結果ログ、操作ログの内容も出力されます。

```
... (大量の出力が行われるため、内容については記載しません) ...
```

### 結果ログ

処理の結果をわかりやすく出力します。

* 処理の開始時刻と終了時刻
* shared/isolated
* M番号
* 処理成否(OK/NG)
* アップグレード前後の仮想ルータ名
* エラー原因
を出力します

** results.log ** に追記出力されます。

```
[2017/04/17 21:45:08] isolated, M00000002
NG r-7380-VM -> 	DEBUG!! stop proccess
OK r-7797-VM -> r-7798-VM	
[2017/04/17 21:47:49] ... finish
[2017/04/17 21:52:24] shared, M00000002
OK r-7784-VM -> r-7800-VM	
OK r-7793-VM -> r-7809-VM	
OK r-7796-VM -> r-7812-VM	
OK r-7785-VM -> r-7801-VM	
OK r-7792-VM -> r-7808-VM	
OK r-7788-VM -> r-7804-VM	
OK r-7795-VM -> r-7811-VM	
OK r-7787-VM -> r-7803-VM	
OK r-7789-VM -> r-7805-VM	
OK r-7783-VM -> r-7799-VM	
OK r-7786-VM -> r-7802-VM	
OK r-7790-VM -> r-7806-VM	
OK r-7791-VM -> r-7807-VM	
OK r-7794-VM -> r-7810-VM	
[2017/04/17 22:28:14] ... finish
```

### 仮想ルータチェックログ

アップグレード後の仮想ルータのチェック結果を出力します。  

出力内容は以下の通りです。

* 実行コマンド
* ログイン日時
* ホスト名
* cat /etc/cloudstack-release の結果
* iptables -L -n の結果(isolatedの場合のみ)

** logs/アップグレード後のVR名.txt ** に追記出力されます。
```

[date; hostname]
2017年  4月 19日 水曜日 14:58:14 JST
rpx20.cl.local

[cat /etc/cloudstack-release]
Cloudstack Release 4.6.0 Mon Dec 19 09:59:35 UTC 2016

[iptables -L -n]
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           state RELATED,ESTABLISHED 
ACCEPT     icmp --  0.0.0.0/0            0.0.0.0/0           
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           
ACCEPT     tcp  --  0.0.0.0/0            0.0.0.0/0           state NEW tcp dpt:22 
REJECT     all  --  0.0.0.0/0            0.0.0.0/0           reject-with icmp-host-prohibited 

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
REJECT     all  --  0.0.0.0/0            0.0.0.0/0           reject-with icmp-host-prohibited 

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
```

### 操作ログ

実行した処理内容や、処理対象の詳細を出力します。

* 実行時刻
* ログレベル(INFO/ERROR)
* M番号
* アップグレード前の仮想ルータ名
* 処理内容の詳細

を出力します

** operaion.log ** に追記出力されます。
```
2017-04-17 21:45:08,042 INFO: config file loaded ... develop.yml
2017-04-17 21:45:08,043 INFO: isolated upgrade start ...
2017-04-17 21:45:08,043 INFO: [M00000002:] start ...
2017-04-17 21:45:08,408 INFO: [M00000002:r-7797-VM] router {'instance_uuid': '37ad70c8-22a5-4f74-8b37-15b1f27ee8b5', 'instance_name': 'r-7797-VM', 'network_uuid': 'd3297343-80bb-4fdb-8912-036f605c2d0a', 'network_id': '513', 'instance_id': '7797', 'network_name': 'sy-nkmc-vr-net'}
2017-04-17 21:47:49,516 INFO: [M00000002:r-7797-VM] linkLocalIp: 198.19.5.187
2017-04-17 21:47:49,517 INFO: [M00000002:r-7797-VM] r-7798-VM ... finish
2017-04-17 21:52:13,844 ERROR: 設定ファイルが存在しません。
2017-04-17 21:52:24,285 INFO: config file loaded ... revers.yml
2017-04-17 21:52:24,286 INFO: shared upgrade start.
2017-04-17 21:52:24,288 INFO: [M00000002:] start ...
2017-04-17 21:52:24,657 INFO: [M00000002:r-7783-VM] router {'instance_uuid': '203ef027-0977-4cdb-9181-5a9f04eb57b8', 'instance_name': 'r-7783-VM', 'network_uuid': 'd47c1d0e-f505-41a6-b946-582dc8aaff90', 'network_id': '469', 'instance_id': '7783', 'network_name': 'IntraM02_13'}
2017-04-17 21:55:06,650 INFO: [M00000002:r-7783-VM] linkLocalIp: 198.19.5.167
2017-04-17 21:55:06,651 INFO: [M00000002:r-7783-VM] r-7799-VM ... finish
2017-04-17 21:55:06,653 INFO: [M00000002:r-7784-VM] router {'instance_uuid': 'c59632a8-49e3-4725-ae11-02800a8429d8', 'instance_name': 'r-7784-VM', 'network_uuid': '804979d1-11af-47de-8ddf-91b5d3f346d2', 'network_id': '470', 'instance_id': '7784', 'network_name': 'IntraM02_14'}
2017-04-17 21:57:48,681 INFO: [M00000002:r-7784-VM] linkLocalIp: 198.19.5.110
2017-04-17 21:57:48,683 INFO: [M00000002:r-7784-VM] r-7800-VM ... finish
2017-04-17 21:57:48,684 INFO: [M00000002:r-7785-VM] router {'instance_uuid': '6a9c7d77-9684-41eb-b46f-d0764def9a7a', 'instance_name': 'r-7785-VM', 'network_uuid': 'a61ffe0e-00da-4591-a44b-57a56f485f9b', 'network_id': '471', 'instance_id': '7785', 'network_name': 'IntraM02_15'}
2017-04-17 22:00:10,691 INFO: [M00000002:r-7785-VM] linkLocalIp: 198.19.5.125
2017-04-17 22:00:10,693 INFO: [M00000002:r-7785-VM] r-7801-VM ... finish
2017-04-17 22:00:10,694 INFO: [M00000002:r-7786-VM] router {'instance_uuid': '5a3f0a7c-322a-403f-b3b4-80863ff1a1c6', 'instance_name': 'r-7786-VM', 'network_uuid': 'ad91eefb-3a9c-4cf4-837a-fef6d3b7a448', 'network_id': '465', 'instance_id': '7786', 'network_name': 'IntraM02_9'}
2017-04-17 22:02:32,869 INFO: [M00000002:r-7786-VM] linkLocalIp: 198.19.5.153
2017-04-17 22:02:32,871 INFO: [M00000002:r-7786-VM] r-7802-VM ... finish
2017-04-17 22:02:32,871 INFO: [M00000002:r-7787-VM] router {'instance_uuid': '3392f27d-b8d4-416b-a088-082631f6466d', 'instance_name': 'r-7787-VM', 'network_uuid': '73de31f1-dd62-45f5-9cf9-7e4518f87097', 'network_id': '464', 'instance_id': '7787', 'network_name': 'IntraM02_8'}
2017-04-17 22:05:15,165 INFO: [M00000002:r-7787-VM] linkLocalIp: 198.19.5.55
2017-04-17 22:05:15,167 INFO: [M00000002:r-7787-VM] r-7803-VM ... finish
.....
2017-04-17 22:23:10,428 INFO: [M00000002:r-7795-VM] router {'instance_uuid': '6fbcb9f0-f07e-4dc6-aa1e-2ea6d7015220', 'instance_name': 'r-7795-VM', 'network_uuid': '50afb99a-2594-4fec-827e-bac8bcca8473', 'network_id': '215', 'instance_id': '7795', 'network_name': 'IntraFrontSegment'}
2017-04-17 22:25:32,655 INFO: [M00000002:r-7795-VM] linkLocalIp: 198.19.5.181
2017-04-17 22:25:32,657 INFO: [M00000002:r-7795-VM] r-7811-VM ... finish
2017-04-17 22:25:32,657 INFO: [M00000002:r-7796-VM] router {'instance_uuid': 'b847ec08-1acc-401f-a7d8-5e43e9c85085', 'instance_name': 'r-7796-VM', 'network_uuid': '1eb43795-3859-47c8-81f9-ad3429ea91aa', 'network_id': '217', 'instance_id': '7796', 'network_name': 'BackSegment'}
2017-04-17 22:28:14,598 INFO: [M00000002:r-7796-VM] linkLocalIp: 198.19.5.91
2017-04-17 22:28:14,598 INFO: [M00000002:r-7796-VM] r-7812-VM ... finish
2017-04-17 22:28:14,611 INFO: shared upgrade finish.
```



## 参考情報

### 実行可能処理の確認

以下で実行できる処理名(タスク)が表示されます。

> $ fab --list

```
Available commands:

    isolated  isolated 仮想ルータのアップグレード
    setEnv    設定ファイルの読み込み
    shared    shared 仮想ルータのアップグレード
```

### 並列実行

処理はM番号単位で順次行われますが、M番号に紐付くVRに対しては並列でアップグレード処理が実行されます。  

並列実行数は

* isolated=5
* shared=15

となっています。変更を行いたいときはソースの ** pool_size ** を変更してください。

```
@parallel(pool_size=5)
def do_isolated(mNum, datas):
```

```
@parallel(pool_size=15)
def do_shared(mNum, datas):
```

※Windows環境で実行する場合は、並列処理は利用できません。@parallel の行をコメントアウトしてください。

