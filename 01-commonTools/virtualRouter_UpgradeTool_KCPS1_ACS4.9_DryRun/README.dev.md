# Python+Fabric環境構築手順

## ミニ環境(商用環境同様)向け

インターネットへの接続ができない環境へ依存ファイルをデプロイするために、必要ファイルをパッケージする。

### インストール用パッケージを準備する

インターネットへの接続が可能な、別環境でインストール用パッケージを作成する。  
[オフライン環境にPythonのパッケージをpipで持ち込みたい](http://qiita.com/ryozi_tn/items/d08edf86dba5e7806da8)


パッケージ用ディレクトリを作成し、pipのダウンロード
```
$ mkdir pip-fabric-installer
$ cd pip-fabric-installer
$ wget https://bootstrap.pypa.io/get-pip.py
```

requirements.txtを記述する
```
$ vi requirements.txt
fabric==1.13.1
cs==1.1.0
pyyaml==3.12
requests==2.13.0
paramiko==2.1.1
cryptography==1.7.2
cffi==1.9.1
pycparser==2.17
enum34==1.1.6
idna==2.2
ipaddress==1.0.18
pyasn1==0.2.2
setuptools==28.8.0
six==1.10.0
wheel
```

pipファイルをダウンロードする
```
$ python get-pip.py --download .src --requirement requirements.txt --no-binary :all:
```

ダウンロード結果
```
$ ll .src/
合計 6752
-rw-r--r-- 1 root root  217579  4月 21 15:20 2017 Fabric-1.13.1.tar.gz
-rw-r--r-- 1 root root  253011  4月 21 15:20 2017 PyYAML-3.12.tar.gz
-rw-r--r-- 1 root root   12700  4月 21 15:21 2017 appdirs-1.4.3.tar.gz
-rw-r--r-- 1 root root   70508  4月 21 15:21 2017 argparse-1.4.0.tar.gz
-rw-r--r-- 1 root root  418131  4月 21 15:21 2017 cffi-1.10.0.tar.gz
-rw-r--r-- 1 root root  420867  4月 21 15:21 2017 cryptography-1.7.2.tar.gz
-rw-r--r-- 1 root root    6159  4月 21 15:20 2017 cs-1.1.0.tar.gz
-rw-r--r-- 1 root root   40048  4月 21 15:21 2017 enum34-1.1.6.tar.gz
-rw-r--r-- 1 root root  130211  4月 21 15:21 2017 idna-2.5.tar.gz
-rw-r--r-- 1 root root   32475  4月 21 15:21 2017 ipaddress-1.0.18.tar.gz
-rw-r--r-- 1 root root    2114  4月 21 15:21 2017 ordereddict-1.1.tar.gz
-rw-r--r-- 1 root root   44706  4月 21 15:21 2017 packaging-16.8.tar.gz
-rw-r--r-- 1 root root 1196454  4月 21 15:21 2017 paramiko-2.1.1.tar.gz
-rw-r--r-- 1 root root 1197370  4月 21 15:20 2017 pip-9.0.1.tar.gz
-rw-r--r-- 1 root root   94862  4月 21 15:21 2017 pyasn1-0.2.3.tar.gz
-rw-r--r-- 1 root root  231163  4月 21 15:21 2017 pycparser-2.17.tar.gz
-rw-r--r-- 1 root root 1232522  4月 21 15:21 2017 pyparsing-2.2.0.tar.gz
-rw-r--r-- 1 root root  557508  4月 21 15:21 2017 requests-2.13.0.tar.gz
-rw-r--r-- 1 root root  624263  4月 21 15:21 2017 setuptools-35.0.1.zip
-rw-r--r-- 1 root root   29630  4月 21 15:21 2017 six-1.10.0.tar.gz
-rw-r--r-- 1 root root   54143  4月 21 15:21 2017 wheel-0.29.0.tar.gz
```

tarでまとめてパッケージにする
```
cd ../
tar czf pip-fabric-installer.tar.gz pip-fabric-installer
```


### 踏み台サーバ(tckktky4-vrhns01)へのインストール

ビルドに必要なパッケージをインストールする。

* gcc
* libffi-devel
* python-devel
* openssl-devel

ローカルリポジトリからインストール。

```
$ su
# yum install gcc libffi-devel python-devel openssl-devel

Installed:
  openssl-devel.x86_64 0:1.0.1e-30.el6_6.4

Dependency Installed:
  keyutils-libs-devel.x86_64 0:1.4-4.el6            krb5-devel.x86_64 0:1.10.3-10.el6_4.6
  libcom_err-devel.x86_64 0:1.41.12-14.el6_4.4      libselinux-devel.x86_64 0:2.0.94-5.3.el6_4.1
  libsepol-devel.x86_64 0:2.0.41-4.el6              samba-winbind.x86_64 0:3.6.9-151.el6_4.1
  zlib-devel.x86_64 0:1.2.3-29.el6

Updated:
  gcc.x86_64 0:4.4.7-3.el6                        libsmbclient.x86_64 0:3.6.9-151.el6_4.1
  python-devel.x86_64 0:2.6.6-37.el6_4

Dependency Updated:
  cpp.x86_64 0:4.4.7-3.el6                        e2fsprogs.x86_64 0:1.41.12-14.el6_4.4
  e2fsprogs-libs.x86_64 0:1.41.12-14.el6_4.4      expat.x86_64 0:2.0.1-11.el6_2
  gcc-c++.x86_64 0:4.4.7-3.el6                    gcc-gfortran.x86_64 0:4.4.7-3.el6
  keyutils.x86_64 0:1.4-4.el6                     keyutils-libs.x86_64 0:1.4-4.el6
  krb5-libs.x86_64 0:1.10.3-10.el6_4.6            krb5-workstation.x86_64 0:1.10.3-10.el6_4.6
  libcom_err.x86_64 0:1.41.12-14.el6_4.4          libgcc.i686 0:4.4.7-3.el6
  libgcc.x86_64 0:4.4.7-3.el6                     libgfortran.x86_64 0:4.4.7-3.el6
  libgomp.x86_64 0:4.4.7-3.el6                    libselinux.x86_64 0:2.0.94-5.3.el6_4.1
  libselinux-python.x86_64 0:2.0.94-5.3.el6_4.1   libselinux-utils.x86_64 0:2.0.94-5.3.el6_4.1
  libss.x86_64 0:1.41.12-14.el6_4.4               libstdc++.x86_64 0:4.4.7-3.el6
  libstdc++-devel.x86_64 0:4.4.7-3.el6            libtalloc.x86_64 0:2.0.7-2.el6
  libtdb.x86_64 0:1.2.10-1.el6                    python.x86_64 0:2.6.6-37.el6_4
  python-libs.x86_64 0:2.6.6-37.el6_4             samba-client.x86_64 0:3.6.9-151.el6_4.1
  samba-common.x86_64 0:3.6.9-151.el6_4.1         samba-winbind-clients.x86_64 0:3.6.9-151.el6_4.1
  zlib.x86_64 0:1.2.3-29.el6

Complete!
```

ミニ環境ではローカルリポジトリの対応が間に合わなかったので、libffi-devel はrpmからインストールした。

* https://www.rpmfind.net/linux/RPM/centos/6.8/x86_64/Packages/libffi-devel-3.0.5-3.2.el6.x86_64.html  
* ftp://fr2.rpmfind.net/linux/centos/6.8/os/x86_64/Packages/libffi-devel-3.0.5-3.2.el6.x86_64.rpm

```
# rpm -i libffi-devel-3.0.5-3.2.el6.x86_64.rpm
warning: libffi-devel-3.0.5-3.2.el6.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID c105b9de: NOKEY
```

実行時のmysqlクライアントも使用するのインストールする  
※商用環境の踏み台サーバにはインストール済みのため実行不要
```
# yum install mysql

Installed products updated.

Installed:
  mysql.x86_64 0:5.1.73-7.el6

Dependency Updated:
  mysql-libs.x86_64 0:5.1.73-7.el6

Complete!
```


fabric用のパッケージを展開する

```
$ tar xzf pip-fabric-installer.tar.gz
$ cd pip-fabric-installer
```

fabricおよび依存関係をインストールする

```
$ su
# python get-pip.py --no-index --find-links=.src --requirement requirements.txt --install-option="--install-scripts=/usr/local/bin"
```

```
DEPRECATION: Python 2.6 is no longer supported by the Python core team, please upgrade your Python. A future version of pip will drop support for Python 2.6
Collecting pip
Collecting wheel
Collecting fabric==1.13.1 (from -r requirements.txt (line 1))
Collecting cs==1.1.0 (from -r requirements.txt (line 2))
Collecting pyyaml==3.12 (from -r requirements.txt (line 3))
Collecting paramiko==2.1.1 (from -r requirements.txt (line 4))
Collecting cryptography==1.7.2 (from -r requirements.txt (line 5))
Collecting cffi==1.9.1 (from -r requirements.txt (line 6))
Collecting pycparser==2.17 (from -r requirements.txt (line 7))
Collecting enum34==1.1.6 (from -r requirements.txt (line 8))
Collecting idna==2.2 (from -r requirements.txt (line 9))
Collecting ipaddress==1.0.18 (from -r requirements.txt (line 10))
Collecting pyasn1==0.2.2 (from -r requirements.txt (line 11))
Collecting setuptools==28.8.0 (from -r requirements.txt (line 12))
Collecting six==1.10.0 (from -r requirements.txt (line 13))
Requirement already up-to-date: argparse in /usr/lib/python2.6/site-packages/argparse-1.4.0-py2.6.egg (from wheel)
Collecting requests (from cs==1.1.0->-r requirements.txt (line 2))
Collecting ordereddict (from enum34==1.1.6->-r requirements.txt (line 8))
Installing collected packages: pip, wheel, idna, pyasn1, six, setuptools, ordereddict, enum34, ipaddress, pycparser, cffi, cryptography, paramiko, fabric, requests, cs, pyyaml
  Running setup.py install for pip ... done
  Running setup.py install for wheel ... done
  Running setup.py install for idna ... done
  Running setup.py install for pyasn1 ... done
  Running setup.py install for six ... done
  Found existing installation: setuptools 0.6rc11
    DEPRECATION: Uninstalling a distutils installed project (setuptools) has been deprecated and will be removed in a future version. This is due to the fact that uninstalling a distutils project will only partially uninstall the project.
    Uninstalling setuptools-0.6rc11:
      Successfully uninstalled setuptools-0.6rc11
  Running setup.py install for setuptools ... done
  Running setup.py install for ordereddict ... done
  Running setup.py install for enum34 ... done
  Running setup.py install for ipaddress ... done
  Running setup.py install for pycparser ... done
  Running setup.py install for cffi ... done
  Running setup.py install for cryptography ... done
  Found existing installation: paramiko 1.7.5
    Uninstalling paramiko-1.7.5:
      Successfully uninstalled paramiko-1.7.5
  Running setup.py install for paramiko ... done
  Running setup.py install for fabric ... done
  Found existing installation: requests 2.11.1
    Uninstalling requests-2.11.1:
      Successfully uninstalled requests-2.11.1
  Running setup.py install for requests ... done
  Running setup.py install for cs ... done
  Running setup.py install for pyyaml ... done
Successfully installed cffi-1.9.1 cryptography-1.7.2 cs-1.1.0 enum34-1.1.6 fabric-1.13.1 idna-2.2 ipaddress-1.0.18 ordereddict-1.1 paramiko-2.1.1 pip-9.0.1 pyasn1-0.2.2 pycparser-2.17 pyyaml-3.12 requests-2.13.0 setuptools-28.8.0 six-1.10.0 wheel-0.29.0
```

インストール先は

* lib ... /usr/lib/python2.6/site-packages/
* bin ... /usr/local/bin

/usr/local/bin には以下がインストールされる
```
$ ll /usr/local/bin/
total 32
-rwxr-xr-x 1 root root 356 May 10 15:52 cs
-rwxr-xr-x 1 root root 403 May 10 15:37 easy_install
-rwxr-xr-x 1 root root 411 May 10 15:37 easy_install-2.6
-rwxr-xr-x 1 root root 373 May 10 15:51 fab
-rwxr-xr-x 1 root root 281 May 10 15:37 pip
-rwxr-xr-x 1 root root 283 May 10 15:37 pip2
-rwxr-xr-x 1 root root 287 May 10 15:37 pip2.6
-rwxr-xr-x 1 root root 294 May 10 15:37 wheel
```

動作確認（ヘルプがでること）
```
$ pip
```

fabfileのバージョンを確認
```
$ fab --version
Fabric 1.13.1
Paramiko 2.1.1
```
