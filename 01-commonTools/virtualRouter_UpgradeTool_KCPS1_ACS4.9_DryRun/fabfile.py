# -*- coding: utf-8 -*-

# Python standard Libary
import sys, codecs, re
import os.path
import logging
import json
import time
import functools
from datetime import datetime, timedelta

# PyYaml
import yaml

# fabric
from fabric.api import local, run, execute
from fabric.api import env, settings
from fabric.decorators import *
from fabric.colors import *

# Exoscale cs
from cs import CloudStack

## log settings
stream_log = logging.StreamHandler()
stream_log.setLevel(logging.DEBUG)
stream_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

file_log = logging.FileHandler(filename='operation.log')
file_log.setLevel(logging.INFO)
file_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

logging.getLogger().addHandler(file_log)
logging.getLogger().setLevel(logging.DEBUG)

@task
@runs_once
def setEnv(config='develop.yml'):
    '''
    設定ファイルの読み込み
    '''
    logging.getLogger().addHandler(stream_log)
    if not os.path.exists(config):
        logging.error(u'設定ファイルが存在しません。%s' % config)
        exit(1)

    try:
        # 設定ファイルの読み込み
        with open(config, 'r') as f:
            conf = yaml.load(f)

        env.hosts    = []   # 動的にホスト設定する
        env.vr       = conf['vr']
        env.ap       = conf['ap']
        env.csApi    = conf['csApi']
        env.database = conf['database']
        env.networkOfferings = conf['networkOfferings']
        env.flags = conf['flags']

        # ログディレクトリの作成
        if not os.path.exists('logs'):
            os.mkdir('logs')

    except Exception as e:
        print e
        logging.error(u'設定ファイルの読み込みに失敗しました。%s' % config)
        exit(1)
    else:
        logging.info('config file loaded ... %s' % config)

@task
@runs_once
def isolatedDryRun(filepath="specialDomains.txt"):
    '''
    isolated 仮想ルータのアップグレード
    '''
    logging.getLogger().addHandler(stream_log)
    logging.info('DRY-run: isolated upgrade start.')
    startTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    # M番号
    mNumbers = _loadMNnumbers(filepath)
    if not mNumbers:
        exit(-1)
    else:
        print("mNumbers: " + str(mNumbers))

    execution_filepath = env.flags["executionFile"]
    # M番号単位で処理を行う（多重処理もM番号内で行う
    for mNumber in mNumbers:
        logging.info(u'[%s:] start ...' % mNumber)
        if not _checkExecutionFile(execution_filepath):
            logging.warn(red(execution_filepath + " not found. stop execution at MNumber: " + mNumber))
            break

        # M番号に紐づくルータの取得
        datas = _getIsolatedRouterAndNetworkIds(mNumber)
        if not datas:
            msg = u'[%s:] Isolated のルータが取得できませんでした。' % (mNumber)
            logging.error(msg)
            continue

        # ここからはfabricの並列処理の仕組みを無理やり使う
        # instaceのキー配列を作る
        vmids = map((lambda d: d['instance_name']), datas)
        # instanceのキー配列を、タスクの実行hostsとして設定する
        env.hosts = vmids
        # 紐づける値datasを渡して、並列実行タスクを呼び出す
        results = execute(do_isolated, mNumber, datas)
        _printResults(startTime, 'isolated', mNumber, results)

    logging.info('DRY-run: isolated upgrade finish.')

##
# @parallelはWindowsでは動作しないためコメントアウトすること
# コメントアウトした場合は、直列実行となる
# pool_sizeで平行処理数を指定できる

@parallel(pool_size=10)
def do_isolated(mNum, datas):
    res = {'status': 'NG', 'msg': ''}

    # タスクの実行ホスト(ここでは実在しないinstanceのキーが入っている)
    vr = env.host_string
    getMsg = functools.partial(_getFormatedMessage, mNum, vr)

    # キーに対応するルーター&ネットワーク情報を取り出し
    targets = filter((lambda d: d['instance_name']==vr), datas)
    if len(targets) != 1:
        msg = u'紐づく対象データがありませんでした。%s' % (vr)
        logging.error(getMsg(msg)); res['msg'] = msg
        return res

    # 1つのみ使用する
    data = targets[0]

    # HACK:デバッグ用にVRを限定
    # if data['network_name'] != 'sy-nkmc-vr-net':
    #     logging.debug(getMsg(u'拒否対象: %s' % data['network_name']))
    #     res['msg'] = "DEBUG!! stop proccess"
    #     return res

    logging.info(getMsg(u'router %s' % data))

    try:
        networkUuid = data['network_uuid']
        networkOfferingUuId = env.networkOfferings['isolated']['uuid']

        # 現在のネットワークの取得
        _logDryRun("Checking current network: networkUuid=" + networkUuid)
        before_networks = _csListNetworks(networkUuid)
        if (not before_networks) or before_networks['count'] != 1:
            msg = u'現在のネットワークが取得できませんでした。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # 現在のネットワークオファリングの確認
        before_network = before_networks['network'][0]
        _logDryRun("Checking current network offering: networkUuid=" + before_network['networkofferingid'])
        if networkOfferingUuId == before_network['networkofferingid']:
            msg = u'既にネットワークオファリングが更新されています。%s'
            msg = msg % (before_network['networkofferingid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # 計測開始
        startTime = datetime.now()

        # 仮想ルータの破棄
        result = _csDestroyRouter(data['instance_uuid'])
        if not result:
            msg =u'ルーターの破棄に失敗しました。%s' % data['instance_uuid']
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # ネットワークの更新
        result = _csUpdateNetwork(networkUuid, networkOfferingUuId)
        if not result:
            msg = u'ネットワークの更新に失敗しました。%s, %s' % (networkUuid, networkOfferingUuId)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # ネットワークの取得
        networks = _csListNetworks(networkUuid)
        _logDryRun("networks count: " + str(networks['count']))
        if (not networks) or networks['count'] != 1:
            msg = u'ネットワークが取得できませんでした。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res


        # ネットワークオファリング更新の確認
        network = networks['network'][0]
        """
# Note: dry-runではオファリングは更新されないのでコメントアウト
        if networkOfferingUuId != network['networkofferingid']:
            msg = u'ネットワークオファリングが一致しませんでした。'
            msg = msg % (networkOfferingUuId, network['networkofferingid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # 仮想ルータの取得
        routers = _csListRouters(networkUuid)
        if (not routers) or routers['count'] != 1:
            msg = u'ルーターが取得できませんでした。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # 仮想ルータの状態確認
        router = routers['router'][0]
        res['vr_name'] = router['name']
        _logDryRun("Checking router status: VM name: " + router['name'])
        """
        if not _checkRouterOnVMWare(router):
            msg = u'ルーターがVMWare上で稼働していません。%s' % (router['hypervisor'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        if not _checkRouterRunning(router):
            msg = u'ルーターがRunningでありません。%s' % (router['state'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        if not _checkRouterNetwork(router, networkUuid):
            msg = u'ルーターの稼働ネットワークIDが一致しません。%s' % (router['guestnetworkid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # 仮想ルータ内の設定確認
        linkLocalIp = router['linklocalip']
        logging.info(getMsg("linkLocalIp: %s" % linkLocalIp))
        # VR内にログインするにはAPサーバ上での実行が必要
        _runCommandsOnIsolated(linkLocalIp, res['vr_name'])

        # 計測終了
        endTime = datetime.now()
        result = _checkExecutionTime(startTime, endTime, int(env.flags["executionThreshold"]))
        if result["status"]:
            logging.info(green("OK" + " Time: " + str(result["time"])))
        else:
            logging.warn(red("Excecution takes too long: [%s:%s]  Time: %s") % (
                mNum,
                router['name'],
                str(result["time"])))
    except Exception as e:
        msg = str(e)
        logging.error(getMsg(msg))
        res['msg'] = msg
        res['status'] = 'NG'
    else:
        logging.info(getMsg('%s ... finish' % res['vr_name']))
        res['status'] = 'OK'
    finally:
        return res

@task
@runs_once
def sharedDryRun(filepath="specialDomains.txt"):
    '''
    shared 仮想ルータのアップグレード
    '''
    logging.getLogger().addHandler(stream_log)
    logging.info('DRY-run: shared upgrade start.')
    startTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    # M番号
    mNumbers = _loadMNnumbers(filepath)
    if not mNumbers:
        exit(-1)

    execution_filepath = env.flags["executionFile"]
    # M番号単位で処理を行う（多重処理もM番号内で行う
    for mNumber in mNumbers:
        logging.info(u'[%s:] start ...' % mNumber)
        if not _checkExecutionFile(execution_filepath):
            logging.warn(red(execution_filepath + " not found. stop execution at MNumber: " + mNumber))
            break

        # M番号に紐づくルータの取得
        datas = _getSharedRouterAndNetworkIds(mNumber)
        if not datas:
            msg = u'[%s:] Shared のルータが取得できませんでした。' % (mNumber)
            logging.error(msg)
            continue

        # ここからはfabricの並列処理の仕組みを無理やり使う
        # instaceのキー配列を作る
        vmids = map((lambda d: d['instance_name']), datas)
        # instanceのキー配列を、タスクの実行hostsとして設定する
        env.hosts = vmids
        # 紐づける値datasを渡して、並列実行タスクを呼び出す
        results = execute(do_shared, mNumber, datas)
        _printResults(startTime, 'shared', mNumber, results)

    logging.info('DRY-run: shared upgrade finish.')


##
# @parallelはWindowsでは動作しない
# pool_sizeで平行処理数を指定できる

@parallel(pool_size=15)
def do_shared(mNum, datas):
    res = {'status': 'NG', 'msg': ''}

    # タスクの実行ホスト(ここでは実在しないinstanceのキーが入っている)
    vr = env.host_string
    getMsg = functools.partial(_getFormatedMessage, mNum, vr)

    # キーに対応するルーター&ネットワーク情報を取り出し
    targets = filter((lambda d: d['instance_name']==vr), datas)
    if len(targets) != 1:
        msg = u'紐づく対象データがありませんでした。'
        logging.error(getMsg(msg)); res['msg'] = msg
        return res

    # 1つのみ使用する
    data = targets[0]

    # HACK:デバッグ用にVRを限定
    #if data['network_name'] != 'IntraM02_1':
    # if not 'IntraM02_1' in data['network_name'] :   # IntraM02_1*
    #     logging.debug(getMsg(u'拒否対象: %s' % data['network_name']))
    #     res['msg'] = "DEBUG!! stop proccess"
    #     return res

    logging.info(getMsg(u'router %s' % data))

    try:
        networkUuid = data['network_uuid']
        networkOfferingUuId = env.networkOfferings['shared']['uuid']

        # 現在のネットワークの取得
        _logDryRun("Checking current network: networkUuid=" + networkUuid)
        before_networks = _csListNetworks(networkUuid)
        if (not before_networks) or before_networks['count'] != 1:
            msg = u'現在のネットワークが取得できませんでした。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # 現在のネットワークオファリングの確認
        before_network = before_networks['network'][0]
        _logDryRun("Checking current network offering: networkUuid=" + before_network['networkofferingid'])
        if networkOfferingUuId == before_network['networkofferingid']:
            msg = u'既にネットワークオファリングが更新されています。%s' 
            msg = msg % (before_network['networkofferingid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # 計測開始
        startTime = datetime.now()

        # 仮想ルータの破棄
        result = _csDestroyRouter(data['instance_uuid'])
        if not result:
            msg = u'ルーターの破棄に失敗しました。%s' % (data['instance_uuid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        # ネットワークオファリングの更新
        updatedNomally = _updateAndCheckNetworkId(data['network_id'])
        """
        Dry-runなのでチェックなし
        if not updatedNomally:
            msg = u'ネットワークオファリングIDが更新されませんでした。%s' % (data['network_id'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # ネットワークの再起動
        result = _csRestartNetwork(networkUuid)
        """
        if not result:
            msg = u'ネットワークの再起動に失敗しました。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # 仮想ルータの取得
        routers = _csListRouters(networkUuid)
        """
        if (not routers) or routers['count'] != 1:
            msg = u'ルーターが取得できませんでした。%s' % (networkUuid)
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # 仮想ルータの状態確認
        router = routers['router'][0]
        res['vr_name'] = router['name']
        _logDryRun("Checking router status: VM name: " + router['name'])
        """
        if not _checkRouterOnVMWare(router):
            msg = u'ルーターがVMWare上で稼働していません。%s' % (router['hypervisor'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        if not _checkRouterRunning(router):
            msg = u'ルーターがRunningでありません。%s' % (router['state'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res

        if not _checkRouterNetwork(router, networkUuid):
            msg = u'ルーターの稼働ネットワークIDが一致しません。%s' % (router['guestnetworkid'])
            logging.error(getMsg(msg)); res['msg'] = msg
            return res
        """

        # 仮想ルータ内の設定確認
        linkLocalIp = router['linklocalip']
        logging.info(getMsg("linkLocalIp: %s" % linkLocalIp))
        _runCommandsOnShared(linkLocalIp, res['vr_name'])

        # 計測終了
        endTime = datetime.now()
        result = _checkExecutionTime(startTime, endTime, int(env.flags["executionThreshold"]))
        if result["status"]:
            logging.info(green("OK" + " Time: " + str(result["time"])))
        else:
            logging.warn(red("Excecution takes too long: [%s:%s]  Time: %s") % (
                mNum,
                router['name'],
                str(result["time"])))
    except Exception as e:
        msg = str(e)
        logging.error(getMsg(msg))
        res['msg'] = msg
        res['status'] = 'NG'
    else:
        logging.info(getMsg('%s ... finish' % res['vr_name']))
        res['status'] = 'OK'
    finally:
        return res

def _loadMNnumbers(filepath):
    '''
    M番号ファイルの読み込み
    '''
    logging.getLogger().addHandler(stream_log)
    logging.info('load mNumber file. %s' % filepath)
    _logDryRun('load mNumber file. %s' % filepath)

    if not os.path.exists(filepath): return False
    try:
        with open(filepath) as f:
            lines = f.readlines()
    except:
        logging.error(u'M番号ファイルを開けません。%s' % filepath)
        exit(1)

    mFiles = []
    for line in lines:
        # 空行、コメント行を無視する(=先頭文字Mの行を有効とする)
        if line[0:1] == 'M':
            # チェックフラグで動作を変更
            if env.flags['checkMNumbers']:
                if not re.match(r'^M[0-9]{8}$', line):
                    logging.error(u'M番号はMで始まる9桁の数値を指定してください。%s' % line)
                    return False
            _logDryRun('mNumber: %s' % line)
            mFiles.append(line.rstrip())

    return mFiles

def _getFormatedMessage(mNum, vr, msg):
    '''
    ログファイル用メッセージの生成
    '''
    return "[%s:%s] %s" % (mNum, vr, msg)

def _printResults(startTime, vrType, mNumber, results):
    '''
    結果の出力
    '''
    endTime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    path = 'results.log'
    with open(path, 'a') as f:
        f = codecs.lookup('utf_8')[-1](f)
        f.write("[%s] %s, %s\n" % (startTime, vrType, mNumber))
        for k, v in results.items():
            msg = "%s %s -> %s\t%s" % (v['status'], k, v.get('vr_name',''), v['msg'])
            print msg
            f.write("%s\n" % msg.replace('\r', ''))
        f.write("[%s] ... finish\n" % (endTime))

def _getIsolatedRouterAndNetworkIds(mNumber):
    '''
    Isloatedのルータとネットワークを取得
    '''
    sql =   "SELECT" \
            " v.id, v.uuid, nw.id, nw.uuid, v.name, nw.name" \
            " FROM vm_instance v" \
            " INNER JOIN nics n ON v.id = n.instance_id" \
            " INNER JOIN networks nw ON n.network_id = nw.id" \
            " INNER JOIN domain d ON nw.domain_id = d.id" \
            " WHERE" \
            " v.removed IS NULL" \
            " AND v.vm_type = 'DomainRouter'" \
            " AND nw.name IS NOT NULL" \
            " AND nw.guest_type = 'Isolated'" \
            " AND d.name = '%s'" % (mNumber)

    queryResults = local(_createMySQLCommand(sql), capture=True)

    ids = []
    if queryResults:
        for result in _nlSplit(queryResults):
            resultList = result.split('\t')
            ids.append({ 'instance_id':   resultList[0],
                         'instance_uuid': resultList[1],
                         'network_id':    resultList[2],
                         'network_uuid':  resultList[3],
                         'instance_name': resultList[4],
                         'network_name':  resultList[5] })
    return ids

def _getSharedRouterAndNetworkIds(mNumber):
    '''
    Sharedのルータとネットワークを取得
    '''
    sql =   "SELECT" \
            " v.id, v.uuid, nw.id, nw.uuid, v.name, nw.name" \
            " FROM vm_instance v" \
            " INNER JOIN nics n ON v.id = n.instance_id" \
            " INNER JOIN networks nw ON n.network_id = nw.id" \
            " INNER JOIN domain_network_ref dnr ON nw.id = dnr.network_id" \
            " INNER JOIN domain d ON dnr.domain_id = d.id" \
            " WHERE" \
            " v.removed IS NULL" \
            " AND v.vm_type = 'DomainRouter'" \
            " AND nw.guest_type = 'Shared'" \
            " AND d.name = '%s' " % (mNumber)

    queryResults = local(_createMySQLCommand(sql), capture=True)

    ids = []
    if queryResults:
        for result in _nlSplit(queryResults):
            resultList = result.split('\t')
            ids.append({ 'instance_id':   resultList[0],
                         'instance_uuid': resultList[1],
                         'network_id':    resultList[2],
                         'network_uuid':  resultList[3],
                         'instance_name': resultList[4],
                         'network_name':  resultList[5] })
    return ids

def _updateAndCheckNetworkId(networkId):
    '''
    ネットワークのネットワークオファリングの更新と確認(Shared)
    '''
    # 対象NWをDBより確認
    beforeNetworkOfferingId = _getNetworkOfferingId(networkId)
    _logDryRun("Executing _getNetworkOfferingId: networkId: %s" %
                (networkId))
    if not beforeNetworkOfferingId:
        return False

    # 対象ネットワークへupdate。(Sharedのみ)
    networkOfferingId = env.networkOfferings['shared']['id']
#    _updateNetwork(networkId, networkOfferingId)
    _logDryRun("Executing _updateNetwork: networkId: %s networkOfferingId: %s" %
                (networkId,
                 networkOfferingId))

    # 対象NWをDBより確認
    _logDryRun("Executing _getNetworkOfferingId: networkId: %s" %
                (networkId))
    afterNetworkOfferingId = _getNetworkOfferingId(networkId)
    if not afterNetworkOfferingId:
        return False

# TODO: dry-runの時値は変更されない
    # オファリングが変更されていることを確認
    if beforeNetworkOfferingId != afterNetworkOfferingId:
        return True
    else:
        return False

def _checkRouterOnVMWare(router):
    '''
    ネットワークオファリング更新後の確認(HyperVisor)
    '''
    # 更新後はVMWare上に起動しているはず
    _logDryRun("Executing _checkRouterOnVMWare(router): " + router['hypervisor'])
    return router['hypervisor'] == 'VMware'

def _checkRouterRunning(router):
    '''
    ネットワークオファリング更新後の確認(Running)
    '''
    # 起動していることを確認
    _logDryRun("Executing _checkRouterRunning: " + router['state'])
    return router['state'] == 'Running'

def _checkRouterNetwork(router, restartedNetworkId):
    '''
    ネットワークオファリング更新後の確認(Network)
    '''
    # ネットワークIDの一致を確認
    _logDryRun("Executing _checkRouterNetwork(router, %s): guestnetworkid: %s" %
                (router['guestnetworkid'],
                 restartedNetworkId))
    return router['guestnetworkid'] == restartedNetworkId

def _getNetworkOfferingId(networkId):
    '''
    Sharedネットワークの確認。
    ネットワークオファリングIDを返す
    '''
    sql = 'SELECT network_offering_id FROM networks WHERE id=%s' % (networkId)
    queryResults = local(_createMySQLCommand(sql), capture=True)

    networkOfferingId = None
    for result in _nlSplit(queryResults):
        resultList = result.split('\t')
        networkOfferingId = resultList[0]

    return networkOfferingId


def _updateNetwork(networkId, networkOfferingId):
    '''
    Sharedネットワークの更新
    '''
    sql = 'UPDATE networks SET network_offering_id=%s WHERE id=%s' % (networkOfferingId, networkId)
    query = _createMySQLCommand(sql)
    _logryRun("Executing SQL: %s" %
                (query))
#    queryResults = local(_createMySQLCommand(sql), capture=True)
    # 戻り値は取れない
    return True

def _createMySQLCommand(query):
    '''
    MySQL実行コマンド文字列の生成
    mysqlクライアントがインストールされ、PATHが通っている必要あり
    '''
    if env.database['password']:
        command = "mysql -h %(host)s --port=%(port)s --user=%(user)s --password=%(password)s --skip-column-names -U cloud -e \"%(query)s\""
    else:
        command = "mysql -h %(host)s --port=%(port)s --user=%(user)s --skip-column-names -U cloud -e \"%(query)s\""

    return command % {
        'host': env.database['host'],
        'port': env.database['port'],
        'user': env.database['user'],
        'password': env.database['password'],
        'query': query }

def _runCommandsOnIsolated(linklocalIp, hostName):
    '''
    Isolated VR内部 確認用コマンドの実行
    '''
    _logDryRun("_runCommandsOnIsolated(%s, %s)" %
                (linklocalIp,
                 hostName))
    if env.flags['execOnAp']:
        # APサーバからVRへ直接sshを実行
        params = _getVrConnectEnv(linklocalIp, env.vr)
        ap_run = functools.partial(run)
    else:
        # バッチサーバからAPサーバへsshし、APサーバ上からVRへssh実行
        params = _getVrConnectEnv(env.ap['host'], env.ap)
        ap_run = functools.partial(_sshCmd, linklocalIp)

    wLog = functools.partial(_writeLog, hostName)

    # 接続先の設定
    with settings(**params):
        with settings(warn_only=True):
            cmd = 'date; hostname'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/cloudstack-release'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep dns|egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps -ef |egrep cloud |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep apache |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep haproxy |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

#           cmd = "ping -c 3 `netstat -rn|egrep '^0.0.0.0' |awk '{print $2}'`"
#           wLog(cmd, ap_run(cmd))


            cmd = 'netstat -rn'
            wLog(cmd, ap_run(cmd))

            cmd = 'iptables -L -n'
            wLog(cmd, ap_run(cmd))

            cmd = 'mount'
            wLog(cmd, ap_run(cmd))

            cmd = 'df'
            wLog(cmd, ap_run(cmd))
            

def _runCommandsOnShared(linklocalIp, hostName):
    '''
    Shared VR内部 確認用コマンドの実行
    '''
    _logDryRun("_runCommandsOnShared(%s, %s)" %
                (linklocalIp,
                 hostName))
    if env.flags['execOnAp']:
        # APサーバからVRへ直接sshを実行
        params = _getVrConnectEnv(linklocalIp, env.vr)
# TODO
#        ap_run = functools.partial(run)
    else:
        # バッチサーバからAPサーバへsshし、APサーバ上からVRへssh実行
        params = _getVrConnectEnv(env.ap['host'], env.ap)
        ap_run = functools.partial(_sshCmd, linklocalIp)

    wLog = functools.partial(_writeLog, hostName)

    # 接続先の設定
    with settings(**params):
        with settings(warn_only=True):
            cmd = 'date; hostname'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/cloudstack-release'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep dns|egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps -ef |egrep cloud |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'ps afxwww |egrep apache |egrep -v egrep'
            wLog(cmd, ap_run(cmd))

            cmd = 'netstat -rn'
            wLog(cmd, ap_run(cmd))

            cmd = 'cat /etc/dnsmasq.d/cloud.conf'
            wLog(cmd, ap_run(cmd))

            cmd = 'mount'
            wLog(cmd, ap_run(cmd))


def _getVrConnectEnv(host, envTarget):
    '''
    fabricの接続先を切り替えるための一時的なEnv作成
    '''
    params = {'host_string': host,
              'port': envTarget.get('port', None),
              'user': envTarget.get('user', None),
              'password': envTarget.get('password', None),
              'key_filename': envTarget.get('key_filename', None),
              'disable_known_hosts': True
              }
    return params

def _sshCmd(linklocalip, cmd):
    '''
    踏み台からVR内部 確認用コマンドの実行
    http://stackoverflow.com/questions/12641514/switch-to-different-user-using-fabric/12648391#12648391
    http://stackoverflow.com/questions/19663476/su-root-with-python-fabric
    '''
    ssh_cmd = "ssh %s -l %s -i %s -p %s -q -o \"StrictHostKeyChecking=no\" \"%s\" "
    ssh_cmd = ssh_cmd % (
        linklocalip,
        env.vr['user'],
        env.vr['key_filename'],
        env.vr['port'],
        cmd)
#    return run(ssh_cmd)
    return ssh_cmd

def _writeLog(host, cmd, output):
    '''
    コマンド実行ログの出力
    '''
    _logDryRun("Excecuting Command: Host: %s  Command: %s SSH Command: %s)" %
                 (host,
                  cmd,
                  output))
    path = 'logs/%s.txt' % host
    with open(path, 'a') as f:
        f.write("\n[%s]\n" % cmd)
        f.write("%s\n" % output)

def _csDestroyRouter(instanceUuid):
    '''
    ルータを破棄する
    '''
#    cs = _connectCloudStack()

    # ルータの破棄
#    jobresult = cs.destroyRouter(id=instanceUuid)
    _logDryRun("Executing cs.destroyRouter(id=%s))" %
                        (instanceUuid))

    # jobの実行確認
#    return _csQueryAsyncJobResult(jobresult['jobid'], 20)
    return True

def _csUpdateNetwork(networkUuid, offeringUuid):
    '''
    ルーターのネットワークを更新する
    '''
#    cs = _connectCloudStack()

    # ネットワークの更新
#    jobresult = cs.updateNetwork(id=networkUuid, networkofferingid=offeringUuid)
    _logDryRun("Executing cs.updateNetwork(id=%s, networkofferingid=%s)" %
                        (networkUuid,
                         offeringUuid))
    # jobの実行確認
#    return _csQueryAsyncJobResult(jobresult['jobid'], 40)
    return True

def _csRestartNetwork(networkUuid):
    '''
    ルータ所属のネットワークを再起動する
    '''
#    cs = _connectCloudStack()

    # ネットワークの再起動
#    jobresult = cs.restartNetwork(id=networkUuid, cleanup=True)
    _logDryRun("Executing cs.restartNetwork(id=%s, cleanup=True)" % 
                        (networkUuid))
    # jobの実行確認
#    return _csQueryAsyncJobResult(jobresult['jobid'], 60)
    return True

def _csListNetworks(networkUuid):
    '''
    ルータのネットワーク情報を取得する
    '''
    cs = _connectCloudStack()
    _logDryRun("Executing cs.listNetworks(%s, listall=True)" % (networkUuid))
    return cs.listNetworks(id=networkUuid, listall=True)

def _csListRouters(networkUuid):
    '''
    ルータの情報を取得する
    '''
    cs = _connectCloudStack()
    routers = cs.listRouters(networkid=networkUuid, listall=True)
    return routers

def _csQueryAsyncJobResult(jobid, interval=10, retryCount=30):
    '''
    非同期APIの実行結果を問い合わせる
    応答が0の場合は、interval秒待機し、再度問い合わせる。
    応答が2の場合は、エラーとして例外を発生させる。
    上記以外の応答は、結果を読み込み戻り値として返す。
    interval * retryCount の秒数をすぎるとタイム・アウトとして例外を発生させる
    '''
    cs = _connectCloudStack()
    count = 1
    result = {'jobstatus': 0}

    while result['jobstatus'] == 0:
        time.sleep(interval)
        result = cs.queryAsyncJobResult(jobid=jobid)
        if result['jobstatus'] == 2:
            raise Exception('async job error. %s jobid: %s' % (result['jobresult'], jobid))
        count += 1
        if count > retryCount:
            raise Exception('async job timeout. jobid: %s' % jobid)
        logging.debug("jobstatus %s looping... jobid:%s" % (result['jobstatus'], result['jobid']))

#    return result
    return {'jobstatus': 0}

def _connectCloudStack():
    '''
    CloudStackオブジェクトの作成
    '''
    # Root Admin
    cs = CloudStack(endpoint=env.csApi['endpoint'], key=env.csApi['key'], secret=env.csApi['secret'])
    return cs

def _nlSplit(strval):
    '''
    改行で文字列を分割
    実行OSによってMySQLクライアントの改行コードが違い、適切に分割できないため
    '''
    if env.flags['isLinux']:
        return strval.split('\n')
    else:   # Windows
        return strval.split('\r\n')

def _checkExecutionFile(filepath):
    """
    指定したファイルが存在するかチェックする。

    :param str filepath: チェックするファイルのパス
    :rtype: bool
    :return: True - ファイルが存在する, False - ファイルが存在しない
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug("Checking file: " + filepath)
    return os.path.isfile(filepath)

def _checkExecutionTime(startTime, endTime, threshold):
    """
    時間内に処理できているかチェックする。

    :param datetime startTime: 処理開始時間
    :param datetime endTime: 処理終了時間
    :param int 閾値: 閾値(分)
    :rtype: bool
    :return: True - 問題なし, False - 閾値を超えている
    """
    logging.getLogger().addHandler(stream_log)

    threshold = timedelta(minutes=threshold)
    time = endTime - startTime

    logging.info("threashold: " + str(threshold))
    if time >= threshold:
#        logging.warn(red("Excecution takes: " + str(time)))
        return {"status": False, "time": time}
    else:
        return {"status": True, "time": time}

def _logDryRun(msg):
    logging.info(magenta("DRY-run: " + msg))


if __name__ == '__main__':
    # メイン処理
    # デバッグ用途
    setEnv()
    networkUuid = 'd3297343-80bb-4fdb-8912-036f605c2d0a'
    print "networks"
    print _csListNetworks(networkUuid)
    print "routers"
    print _csListRouters(networkUuid)

