#!/bin/bash
#
# # Copyright(c) 2013 CREATIONLINE,INC. All Right Reserved.
#
# @(#) This script is to change template extraction configuration.
#
#
# Configuration file:
#  -${SCRIPT_DIR}/changeTemplateExtraction.conf
# Log directory:
#  -${LOG_DIR}
# Temporary directory:
#  -${TMP_DIR}
# Files:
#  -${LOG_DIR}/changeTemplateExtraction_<YYYYMMDD_hhmmss>.log     : log file 
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/template_format_list.txt         : contains template information.
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/iso_format_list.txt              : contains iso information.
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/template_<domainid>_<account>.xml: contains xml response(Template) from API Server.
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/iso_<domainid>_<account>.xml     : contains xml response(ISO) from API Server.
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/template_<id>.xml                : contains xml response(Template) for updating extraction from API Server.
#  -${TMP_DIR}/<YYYYMMDD_hhmmss>/iso_<id>.xml                     : contains xml response(ISO) for updating extraction from API Server.
#
#set -u
readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIR=$(cd $(dirname $0) ; pwd)
readonly EXEC_DATE=$(date +"%Y%m%d_%H%M%S")


info(){
  echo "INFO: $*" | tee -a ${LOG_FILE}
}

info_d(){
  info "$(date +'%Y/%m/%d %H:%M:%d') $*"
}

error(){
  echo "ERROR: $*" | tee -a ${LOG_FILE}
}

warn(){
  echo "WARN: $*" | tee -a ${LOG_FILE}
}

debug(){
  [ ${DEBUG_FLAG} -eq 1 ] && echo "DEBUG: $*" | tee -a ${LOG_FILE}
}

log(){
  echo "$*" >> ${LOG_FILE}
}

usage(){
  cat <<EOF
please input <domain id> <account id> <yes/no>
  usage: ${SCRIPT_NAME} -d <Domain ID> -a <Account ID> -f <yes/no>
  exit this script. bye...
EOF
}

#
# Initialization
#
init_script(){
  #
  # changeTemplateExtraction.conf must be loaded first because it contains variables.
  # 
  if [ -f ${SCRIPT_DIR}/changeTemplateExtraction.conf ] ; then
    . ${SCRIPT_DIR}/changeTemplateExtraction.conf
  else
    echo "ERROR: ${SCRIPT_NAME} requires changeTemplateExtraction.conf in the same directory."
    echo "INFO: $(date +'%Y/%m/%d %H:%M:%d') exit ${SCRIPT_NAME}"
    exit 1
  fi

  # Create log directory if not exist.
  if [ ! -d ${LOG_DIR} ] ; then
    echo "INFO: creating log directory."
    mkdir -p ${LOG_DIR}
    touch ${LOG_FILE}
    if [ $? -ne 0 ] ; then
      echo "ERROR: cannot create log file."
      echo "ERROR:  please check the current directory or file permission."
      echo "INFO: $(date +'%Y/%m/%d %H:%M:%d') exit ${SCRIPT_NAME}"
      exit 1
    fi
  fi

  # Create tmp directory if not exist.
  if [ ! -d ${TMP_DIR} ] ; then
    echo "INFO: creating tmp directory."
    mkdir -p ${TMP_DIR}
    if [ $? -ne 0 ] ; then
      error "ERROR: cannot create tmp directory."
      echo "INFO: $(date +'%Y/%m/%d %H:%M:%d') exit ${SCRIPT_NAME}"
      exit 1
    fi
  fi


  if [ -f ${SCRIPT_DIR}/cs-script.conf ] ; then
    . ${SCRIPT_DIR}/cs-script.conf
  else
    error "${SCRIPT_NAME} requires cs-script.conf in the same directory."
    info_d "exit ${SCRIPT_NAME}"
    exit 1
  fi

}

#
# Cleaning temprary files
#
clean_tmp_data(){
  if [ ${REMOVE_TMP_FLAG:=0} -eq 0 ] ; then
    return 0
  fi

  if [ -d "${TMP_DIR}" ] ; then
#    echo "${TMP_DIR}" | grep "^/tmp/tmp" > /dev/null
#    if [ $? -eq 0 ] ; then
      info "removing ${TMP_DIR}"
      rm -rf ${TMP_DIR}
#    else
#      error "cannot delete ${TMP_DIR}"
#    fi
  fi

}

#
# Get domain id
#
# $1 -> domain name (e.g. "ROOT", "M00000001")
#
get_domain_id(){
  local domain_name=$1

  if [ -z "${domain_name}" ] ; then
    error "domain name is empty."
    return 1
  fi

  if [ ${domain_name} == "ROOT" ] ; then
    local command="listDomains"
  else
    local command="listDomainChildren"
  fi

  domain_id=$(execute_command command=${command} name=${domain_name} 2> /dev/null | grep "<id>" | sed -e 's/\s*<\/\?id>//g')

  echo ${domain_id}

  if [ -z "${domain_id}" ] ; then
    warn "${domain_name} not found."
    return 1
  else
    return 0
  fi
}

#
# Format list of Templates/ISOs.
# 
#  $1 -> xml file which contains listTemplates response from API Server.
#  $2 -> output file
#
format_list(){
  local src_file=$1
  local out_file=$2

  debug "src_file: ${src_file}"
  debug "out_file: ${out_file}"

  if [ ! -f "${src_file}" ] ; then
    error "${src_file} not found."
    return 1
  fi

  grep "^<listtemplatesresponse .*>" ${src_file} > /dev/null
  if [ $? -eq 0 ] ; then
    type="template"
  else
    type="iso"
  fi
  debug "xml type : ${type}"


  declare -a a_id
  declare -a a_name
  declare -a a_ext

  IFS_ORIG=${IFS}
  IFS=$'\n'

  count=0
  for i in $(echo "cat /list${type}sresponse/${type}/id/text()" | xmllint --shell ${src_file} | sed -e '0~2n;d') ; do
    a_id[$count]=$i
#  for i in $(grep "<id>" ${src_file}) ; do
#    a_id[$count]=$(echo $i | sed -e 's/\s*<\/\?id>//g')
#    a_id[$count]=$(echo $i | sed -e 's|\s*<id>\(.*\)</id>|\1|g')
    count=$[ count + 1]
  done

  count=0
  for i in $(echo "cat /list${type}sresponse/${type}/name/text()" | xmllint --shell ${src_file} | sed -e '0~2n;d') ; do
    a_name[$count]=$i
#  for i in $(grep "<name>" ${src_file}) ; do
#    a_name[$count]=$(echo $i | sed -e 's/\s*<\/\?name>//g')
#    a_name[$count]=$(echo $i | sed -e 's|\s*<name>\(.*\)</name>|\1|g')
    count=$[ count + 1]
  done

  count=0
  for i in $(echo "cat /list${type}sresponse/${type}/isextractable/text()" | xmllint --shell ${src_file} | sed -e '0~2n;d') ; do
    a_ext[$count]=$i
#  for i in $(grep "<isextractable>" ${src_file}) ; do
#    a_ext[$count]=$(echo $i | sed -e 's|\s*<isextractable>\(.*\)</isextractable>|\1|g')
    count=$[ count + 1]
  done

  for (( i=0 ; i<${#a_id[@]} ; i++ )) ; do
    echo ${a_id[$i]} ${a_name[$i]} ${a_ext[$i]} >> ${out_file}
  done

  IFS=${IFS_ORIG}
}


#
# Get template id for given domain and account
#
# $1 -> domain id
# $2 -> account name
#
get_template(){
  local domain_id=$1
  local account=$2
  local response_file=${TMP_DIR}/template_${domain_id}_${account}.xml

  execute_command command=listTemplates templatefilter=self domainid=${domain_id} account=${account} 2> /dev/null 1> ${response_file}
#  template_list=$(grep "<id>" ${response_file} | sed -e 's/\s*<\/\?id>//g' | uniq)
 template_list=$( echo 'cat /listtemplatesresponse/template/id/text()' | xmllint --shell ${response_file} | sed -e '0~2n;d' | uniq)

  format_list ${response_file} ${FORMATED_TEMPLATE_LIST}


  if [ -z "${template_list}" ] ; then
    info "template for ${account} not found."
    return 1
  else
    return 0
  fi
}

#
# Get Iso id for given domain and account
#
# $1 -> domain id
# $2 -> account name
#
get_iso(){
  local domain_id=$1
  local account=$2
  local response_file=${TMP_DIR}/iso_${domain_id}_${account}.xml


  execute_command command=listIsos templatefilter=self domainid=${domain_id} account=${account} 2> /dev/null 1> ${response_file}
  # get ISO owned by system user.
  # CloudStack API returned ISO which is owned by system for some reasons.
  system_iso_id=$(echo 'cat /listisosresponse/iso/id[../account="system" and ../domain="ROOT"]/text()' | xmllint --shell ${response_file} | sed -e '0~2n;d')
  # template_list=$(echo 'cat /listisosresponse/iso/id[../account!="system" and ../domain!="ROOT"]/text()' | xmllint --shell ${response_file} | sed -e '0~2n;d')
  template_list=$(grep "<id>" ${response_file} | sed -e 's/\s*<\/\?id>//g' | uniq)

  format_list ${response_file} ${FORMATED_ISO_LIST}
  
  # remove system iso from file
  if [ ! -z "${system_iso_id}" ] ; then
    for i in ${system_iso_id} ; do
      debug "excluding ${i} from list."
      sed -i -e "/${i}.*/d" ${FORMATED_ISO_LIST}
    done
  fi

  if [ -z "${template_list}" ] ; then
    info "ISO for ${account} not found."
    return 1
  else
    return 0
  fi
}


# Modify ISO extraction for given ISO
#
# $1 -> File which contains ISO id
# $2 -> true/false
#
change_extraction(){
  local type=${1,,}
  local flag=${2,,}
  local list=$3

  if [ $# -ne 3 ] ; then
    error "invalid parameters: $*"
    return 1
  fi

  if [ "${type}" != "template" -a "${type}" != "iso" ] ; then
    error "invalid type: ${type}"
    return 1
  fi
  
  if [ "${type}" = "template" ] ; then
    command="updateTemplatePermissions"
  elif [ "${type}" = "iso" ] ; then
    command="updateIsoPermissions"
  else
    error "invalid parameter value: ${type}"
    return 1
  fi

  if [ "${flag}" != "true" -a "${flag}" != "false" ] ; then
    error "invalid flag: ${flag}"
    return 1
  fi

  if [ ! -f "${list}" ] ; then
    error "cannot find ${list}."
    return 1
  fi


  while read str ; do
    t_id=$(echo ${str} | awk '{print $1}')
    debug "updating extraction for ${t_id}."
    
    result_file="${TMP_DIR}/${type}_${t_id}_result.xml"
    debug "result file: ${result_file}"

    execute_command command=${command} id=${t_id} isextractable=${flag} 2> /dev/null 1> ${result_file}
    if [ ! -f "${result_file}" ] ; then
      error "result file for id ${t_id} not found."
      continue
    fi

    result=$(grep "<success>" ${result_file} | sed -e 's/\s*<\/\?success>//g' -e 's/true/success/g')
    info "result ${t_id}: ${result}"
  done < ${list}

  return 0
}

#
# LIst all Template/ISO.
#
list_all(){
  debug "executing list_all."

  get_template ${domain_id} ${account}
  template_result=$?
  if [ ${template_result} -eq 0 ] ; then
    echo "Templates ------------------------------------------------------------"
    echo ""
    cat ${FORMATED_TEMPLATE_LIST}
    echo ""
  fi

  get_iso ${domain_id} ${account}
  iso_result=$?
  if [ ${iso_result} -eq 0 ] ; then
    echo "ISOs -----------------------------------------------------------------"
    echo ""
    cat ${FORMATED_ISO_LIST}
    echo ""
  fi

  debug "list_all finished."
}

#
# Check input value(yes/no).
#
# return:
#  0 -> "yes"
#  1 -> "no"
#  2 -> value which is not "yes" nor "no"
#
check_confirmation(){
  local k_input=${1,,}

  if [ "${k_input}" = 'y' ] || [ "${k_input}" = 'yes' ] ; then
    return 0
  elif [ "${k_input}" = 'n' ] || [ "${k_input}" = 'no' ] ; then
    return 1
  else
    return 2
  fi
}




#
# Main
#
echo "INFO: $(date +'%Y/%m/%d %H:%M:%d') start executing ${SCRIPT_NAME}"
init_script


while getopts ":a:d:f:lv" flag ; do
  case $flag in
    a)
       account=${OPTARG}
       debug "account: $account"
       ;;
    d)
       domain_name=${OPTARG}
       debug "domain name: $domain_name"
       ;;
    f)
       extract_flag=${OPTARG}
       debug "extract flag: $extract_flag"
       ;;
    l)
       list_flag=1
       debug "extract flag: $extract_flag"
       ;;
    v)
       DEBUG_FLAG=1
       debug "extract flag: $extract_flag"
       ;;
    *)
       error "unsupported option."
       usage
       exit 1
       ;;
  esac
done

if [ -z "${domain_name}" -o -z "${account}" ] ; then
  usage
  exit 1
fi

if [ -z "${list_flag}" -a -z "${extract_flag}" ] ; then
  usage
  exit 1
fi

if [ ! -z "${extract_flag}" ] ; then
  if [ ${extract_flag,,} != "yes" -a ${extract_flag,,} != "no" ] ; then
    error "${extract_flag} is invalid value for -f option."
    info_d "exit ${SCRIPT_NAME}"
    exit 1
  fi
fi

#
# Get domain id from domain name.
#
domain_id=$(get_domain_id ${domain_name})
if [ $? -ne 0 ] ; then
  error "Failed to get domain id for ${domain_name}."
  info_d "exit ${SCRIPT_NAME}"
  exit 1
fi
debug "domain id: ${domain_id}"


#
# List all templates and ISOs which is owned by given account and quit script.
#
if [ ${list_flag:=0} -eq 1 ] ; then
  info "listing templates and ISOs."
  echo ""
  list_all
  echo ""
  info_d "${SCRIPT_NAME} finished."
  exit 0
fi


#
# Get Template information.
#
get_template ${domain_id} ${account}
if [ $? -ne 0 ] ; then
  error "Failed to get template list."
  info_d "exit ${SCRIPT_NAME}"
  exit 1
fi

#
# Get ISO information.
#
get_iso ${domain_id} ${account}
if [ $? -ne 0 ] ; then
  error "Failed to get ISO list."
  info_d "exit ${SCRIPT_NAME}"
  exit 1
fi


#
# Modify Template/ISO extraction value.
#
info "following templates' and ISOs'configuration will be modified."
echo ""
echo "Templates ------------------------------------------------------------"
echo ""
cat ${FORMATED_TEMPLATE_LIST}
echo ""
echo "ISOs -----------------------------------------------------------------"
echo ""
cat ${FORMATED_ISO_LIST}
echo ""

while true ; do
  echo "Please confirm: [no/yes]"
  read k_input

  check_confirmation ${k_input}
  result=$?
  if [ ${result} -eq 0 ] ; then
    if [ "${extract_flag,,}" = "yes" ] ; then
      change_extraction "template" "true" "${FORMATED_TEMPLATE_LIST}"
      change_extraction "iso" "true" "${FORMATED_ISO_LIST}"
    elif [ "${extract_flag,,}" = "no" ] ; then
      change_extraction "template" "false" "${FORMATED_TEMPLATE_LIST}"
      change_extraction "iso" "false" "${FORMATED_ISO_LIST}"
    else
      error "${extract_flag} is invalid value for -f option."
      info "exit ${SCRIPT_NAME}"
      exit 1
    fi

    break
  elif [ ${result} -eq 1 ] ; then
    info "Template/ISO configuration is not modified."
    break  
  else
    echo "wrong input value: ${k_input}"
  fi
done

clean_tmp_data

info_d "${SCRIPT_NAME} finished."

exit 0

