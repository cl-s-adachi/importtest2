#!/bin/bash

readonly CURRENT=$(cd $(dirname $0) && pwd)
readonly SQLDIR="${CURRENT}/sql"
readonly OUTDIR="${CURRENT}/output"

# check argument (sqlfile)
if [ -z $1 ] ; then
  readonly SQL_FILES=$(ls -1 ${SQLDIR})
else
  if [ -f "${SQLDIR}/$1" ] ; then
    readonly SQL_FILES="$1"
  else
    echo "$1 not found"
    exit 1
  fi
fi

# open config.txt
. "${CURRENT}/config.txt"

# make output directory if not exist
mkdir -p "${OUTDIR}"

# do transaction
for sql in ${SQL_FILES} ; do
  # execute command to db1
  echo "execute ${sql} to ${DB1_ID}..."
  mysql ${MYSQL_DB1_OPTS} < "${SQLDIR}/${sql}" | sed -e "${SED_OPTS}" > "${OUTDIR}/${sql%.*}-${DB1_ID}.txt"

  # execute command to db2
  echo "execute ${sql} to ${DB2_ID}..."
  mysql ${MYSQL_DB2_OPTS} < "${SQLDIR}/${sql}" | sed -e "${SED_OPTS}" > "${OUTDIR}/${sql%.*}-${DB2_ID}.txt"

  # compare db1 output with db2 output
  echo "compare ${DB1_ID} output with ${DB2_ID} output..."
  diff ${DIFF_OPTS} "${OUTDIR}/${sql%.*}-${DB1_ID}.txt" "${OUTDIR}/${sql%.*}-${DB2_ID}.txt" | tee "${OUTDIR}/${sql%.*}-diff.txt"

  sleep 5
  echo ""
done

