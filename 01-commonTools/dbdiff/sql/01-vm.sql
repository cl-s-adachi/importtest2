# check1
SELECT COUNT(*) AS vm_count FROM vm_instance WHERE removed is NULL;

SELECT '===================================================================';

# check2
SELECT id, name, instance_name, state, created, removed from vm_instance WHERE removed is NULL ORDER BY id;

SELECT '===================================================================';

# check3
SELECT i.id, i.name, i.ha_enabled, i.hypervisor_type, i.state, i.created, i.removed,
    h.name AS host, p.name AS pod, z.name AS zone,
    /*t.name AS template,*/
    (CASE WHEN t.name = 'SystemVM Template (vSphere)' THEN 'systemvm-vmware-4.6' ELSE (
     CASE WHEN t.name = 'SystemVM Template (KVM)'     THEN 'systemvm-kvm-4.6'
          ELSE t.name   END) END) AS template,
    s.name AS service_offering, a.account_name AS account, d.name AS domain
    FROM vm_instance AS i
        LEFT JOIN host          AS h ON i.host_id = h.id
        LEFT JOIN host_pod_ref  AS p ON h.pod_id = p.id
        LEFT JOIN data_center   AS z ON i.data_center_id = z.id
        LEFT JOIN vm_template   AS t ON i.vm_template_id = t.id
        LEFT JOIN disk_offering AS s ON i.service_offering_id = s.id
        LEFT JOIN account       AS a on i.account_id = a.id
        LEFT JOIN domain        AS d on i.domain_id = d.id
    WHERE i.ha_enabled = 1 AND i.removed is NULL
    ORDER BY i.id\G
