\! echo ""
\! echo ===Number of valid records for each table===
\! echo ""
SELECT COUNT(*) AS account_count FROM account WHERE removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS domain_count FROM domain WHERE removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS vm_count FROM vm_instance WHERE removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS vm_template_count FROM vm_template v
    INNER JOIN template_zone_ref z 
    ON  v.id=z.template_id
    where z.removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS volumes_count FROM volumes WHERE state = 'Ready' AND removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS snapshots_count FROM snapshots WHERE status = 'BackedUp' and removed is NULL;
SELECT '===================================================================';
SELECT COUNT(*) AS networks_count FROM networks WHERE removed is NULL;
