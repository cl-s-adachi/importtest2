# check1
SELECT COUNT(*) AS domain_count FROM domain WHERE removed IS NULL;

SELECT '===================================================================';

# check2
SELECT id, name, state, removed
    FROM domain
    WHERE removed IS NULL
    ORDER BY id;
