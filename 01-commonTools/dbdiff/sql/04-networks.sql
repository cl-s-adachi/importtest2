# check1
SELECT COUNT(*) AS networks_count
    FROM networks AS n
        LEFT JOIN vlan              AS vl ON n.id = vl.network_id AND n.physical_network_id = vl.physical_network_id
        LEFT JOIN data_center       AS z  ON n.data_center_id = z.id
        LEFT JOIN network_offerings AS no ON n.network_offering_id = no.id
    WHERE n.removed is NULL
    ORDER BY n.id;

SELECT '===================================================================';

# check2
SELECT n.id, n.name, n.created, n.gateway, n.cidr, n.guest_type, n.removed,
    (CASE WHEN NOT vl.vlan_id LIKE 'vlan://%' THEN
     CONCAT('vlan://', vl.vlan_id) ELSE
     vl.vlan_id END) AS vlan_id,
    z.name AS zone_name, no.name AS nw_offering_name
    FROM networks AS n
        LEFT JOIN vlan              AS vl ON n.id = vl.network_id AND n.physical_network_id = vl.physical_network_id
        LEFT JOIN data_center       AS z  ON n.data_center_id = z.id
        LEFT JOIN network_offerings AS no ON n.network_offering_id = no.id
    WHERE n.removed is NULL
    ORDER BY n.id\G
