# check1
SELECT COUNT(*) AS snapshots_count FROM snapshots WHERE status = 'BackedUp' and removed is NULL;

SELECT '===================================================================';

# check2
SELECT s.id, s.name, s.size, s.status, s.created, s.removed, s.hypervisor_type, s.backup_snap_id,
    z.name AS zone_name, a.account_name AS account, d.name AS domain
    FROM snapshots AS s
        LEFT JOIN account       AS a ON s.account_id = a.id
        LEFT JOIN domain        AS d ON s.domain_id = d.id
        LEFT JOIN data_center   AS z ON s.data_center_id = z.id
    WHERE s.status = 'BackedUp' AND s.removed is NULL
    ORDER BY s.id\G
