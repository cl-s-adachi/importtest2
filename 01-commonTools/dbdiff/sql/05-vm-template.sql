# check1
SELECT COUNT(*) AS vm_template_count FROM vm_template t
    LEFT JOIN template_zone_ref z
    ON  t.id=z.template_id
    where z.removed is NULL;

SELECT '===================================================================';

# check2
SELECT t.id, t.name, t.created, t.hypervisor_type, t.format,
    a.account_name AS account
    FROM vm_template AS t
        LEFT JOIN template_zone_ref z ON  t.id=z.template_id
        LEFT JOIN account AS a ON t.account_id = a.id
    WHERE z.removed is NULL
    ORDER BY t.id\G
