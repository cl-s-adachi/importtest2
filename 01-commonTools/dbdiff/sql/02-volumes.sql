# check1
SELECT COUNT(*) AS volumes_count FROM volumes WHERE state = 'Ready' and removed is NULL;

SELECT '===================================================================';

# check2
SELECT v.id, v.name, v.size, v.state, v.created, v.removed, v.volume_type,
    /*t.name AS template,*/
    (CASE WHEN t.name = 'SystemVM Template (vSphere)' THEN 'systemvm-vmware-4.6' ELSE (
     CASE WHEN t.name = 'SystemVM Template (KVM)'     THEN 'systemvm-kvm-4.6'
          ELSE t.name   END) END) AS template,
    a.account_name AS account, d.name AS domain,
    i.name AS instance_name, sp.name AS storage_name, do.name AS disk_offering_name,
    p.name AS pod_name, z.name AS zone_name
    FROM volumes AS v
        LEFT JOIN account       AS a  on v.account_id = a.id
        LEFT JOIN domain        AS d  on v.domain_id = d.id
        LEFT JOIN vm_instance   AS i  ON v.instance_id = i.id
        LEFT JOIN vm_template   AS t  ON v.template_id = t.id
        LEFT JOIN storage_pool  AS sp ON v.pool_id = sp.id
        LEFT JOIN disk_offering AS do ON v.disk_offering_id = do.id
        LEFT JOIN host_pod_ref  AS p  ON v.pod_id = p.id
        LEFT JOIN data_center   AS z  ON v.data_center_id = z.id
    WHERE v.state = 'Ready' AND v.removed is NULL
    ORDER BY v.id\G
