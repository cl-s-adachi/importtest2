# check1
SELECT COUNT(*) AS account_count FROM account WHERE removed is NULL;

SELECT '===================================================================';

# check2
SELECT a.id, a.account_name, d.name AS domain_name, a.removed
    FROM account AS a
        LEFT JOIN domain AS d ON a.domain_id = d.id
    WHERE a.removed is NULL
    ORDER BY a.id;
