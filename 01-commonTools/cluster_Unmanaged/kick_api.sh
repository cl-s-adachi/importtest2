#!/bin/bash

# please set your host
# address="http://localhost:8096"

address="http://ckktky4-vclweb01:8080"
#address="http://localhost:8080"
#address="http://ckktky4-vclweb02:8080"

# please set your api key
api_key="jPEEgFXy_XWxS-OC6o0nOh__c7CVx0OsySB-f_74wLjIUVqzvX3gmU1IAzLaZDgaosI_MdJwhQD45qdm7qxORw"
# please set your secret key
secret_key="MTzP1egX2GqCYo-v0VdJj0MKJx37-ktjlApda1L91ckakTwscnowtnHAi7qCINuRc98YGADYRrg6zd_jVLcNiQ"

api_path="/client/api?"


if [ $# -lt 1 ]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
elif [[ $1 != "command="* ]]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
elif [ $1 == "command=" ]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
fi


# echo " asako 1 $@"

data_array=("$@" "apikey=${api_key}")
# echo " asako 2 ${data_array[@]}"

temp1=$(echo -n ${data_array[@]} | \
  tr " " "\n" | \
  sort -fd -t'=' | \
  perl -pe's/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg'| \
  tr "A-Z" "a-z" | \
  tr "\n" "&" )
# echo " asako 3 sorted ${temp1[@]}"

signature=$(echo -n ${temp1[@]})
signature=${signature%&}
# echo " asako 4 $signature"

signature=$(echo -n $signature | \
  openssl sha1 -binary -hmac $secret_key | \
  openssl base64 )

# echo " asako 5 signature=$signature"
signature=$(echo -n $signature | \
  perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')
# echo " asako 6 urlencoded signature=$signature"


url=${address}${api_path}$(echo -n $@ | tr " " "&")"&"apikey=$api_key"&"signature=$signature
echo " SEND URL: $url"


curl ${url} | xmllint --format -
