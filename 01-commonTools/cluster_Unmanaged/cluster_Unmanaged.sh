#!/bin/bash
# Copyright 2017 CREATIONLINE, INC.

#trap 'read -p "$BASH_COMMAND"' DEBUG

rm -rf /tmp/*Tmp.log
export LANG=ja_JP.utf8
mkdir -p ./logs
. ./prm/zone.prm
. ./prm/hypervisor.prm

if [ -z "`cat ./prm/hypervisor.prm | egrep -i VMware`" ]; then
	echo ""
	echo "		VMware 以外のハイパーバイザーには対応していません。"
	echo ""
	exit 2
fi

(
	cd ./logs
	if [ -e ./updateCluster.log ]; then
		cp --suffix=_`date +%Y%m%d%H%M%S` -f updateCluster.log updateCluster.log
		rm -rf updateCluster.log
	fi

	if [ -e ./updateCluster.log ]; then
		cp --suffix=_`date +%Y%m%d%H%M%S` -f updateCluster.log updateCluster.log
		rm -rf updateCluster.log
	fi
)

if [ ! -e ./kick_api.sh ]; then
	echo ""
	echo "		kick_api.sh を cluster_Unmanaged.sh と同じディレクトリに配置して下さい。"
	echo ""
	exit 2
fi

while :
do
	while :
	do
		while :
		do
			while :
			do
				echo
				echo "        非管理対象化／管理対象化する Zone を選択して下さい。"
				echo

				menu="${ZONE_1_NAME} ${ZONE_2_NAME} ${ZONE_3_NAME} ${ZONE_4_NAME} ${ZONE_5_NAME} 中止する"

				select ftest in ${menu}
				do
					case ${ftest} in
						${ZONE_1_NAME} )
							zone_id=${ZONE_1_ID}
							zone_name_j=${ZONE_1_NAME}
							break 2
						;;
						${ZONE_2_NAME} )
							zone_id=${ZONE_2_ID}
							zone_name_j=${ZONE_2_NAME}
							break 2
						;;
						${ZONE_3_NAME} )
							zone_id=${ZONE_3_ID}
							zone_name_j=${ZONE_3_NAME}
							break 2
						;;
						${ZONE_4_NAME} )
							zone_id=${ZONE_4_ID}
							zone_name_j=${ZONE_4_NAME}
							break 2
						;;
						${ZONE_5_NAME} )
							zone_id=${ZONE_5_ID}
							zone_name_j=${ZONE_5_NAME}
							break 2
						;;
						中止する )
							echo
							echo "        中止しました。"
							echo
							exit 1
						;;
						* )
							echo
							echo "        数字キーで選択して下さい。"
							echo
							continue 2
						;;
					esac
				done
			done

			./kick_api.sh command=listClusters hypervisor=${HYPERVISOR} zoneid=${zone_id} | grep '<id>' | head -n 1 | tail -n 1 | awk -F">" '{print $2}' | awk -F"<" '{print $1}' >/tmp/OneClusterIdTmp.log

			cluster_id=`cat /tmp/OneClusterIdTmp.log`

			./kick_api.sh command=listZones id=${zone_id} | grep "<name>" | awk -F">" '{print $2}' | awk -F"<" '{print $1}' >/tmp/zoneNameTmp.log
			zone_name=`cat /tmp/zoneNameTmp.log`

			./kick_api.sh command=listZones id=${zone_id} >/tmp/invalid_checkTmp.log

			if [ -z "`echo ${zone_id}`" ]; then
				echo ""
				echo "==================== 設定エラー ===================="
				echo ""
				echo "./prm/zone.prm の \"${zone_name_j}\" にゾーンID が設定されていません。"
				echo "./prm/zone.prm の `cat ./prm/zone.prm | grep -A 1 ${zone_name_j} | tail -n 1` に ID を追加修正して下さい。"
				echo ""
				echo "	↓ ↓ ↓ ./prm/zone.prm ↓ ↓ ↓"
				echo ""
				cat ./prm/zone.prm | grep -A 1 ${zone_name_j}
				echo ""
				echo "===================================================="
				echo ""
				rm -rf /tmp/*Tmp.log
				exit 2
			elif [ ! -z "`cat /tmp/invalid_checkTmp.log | grep invalid`" ]; then
				echo ""
				echo "==================== 設定エラー ===================="
				echo ""
				echo "./prm/zone.prm の \"${zone_name_j}\" に設定されているゾーンID \"${zone_id}\" が、環境内に存在しません。"
				echo "./prm/zone.prm の `cat ./prm/zone.prm | grep -A 1 ${zone_name_j} | tail -n 1` を正しいゾーンID に修正して下さい。"
				echo ""
				echo "	↓ ↓ ↓ ./prm/zone.prm ↓ ↓ ↓"
				echo ""
				cat ./prm/zone.prm | grep -A 1 ${zone_name_j}
				echo ""
				echo "===================================================="
				echo ""
				rm -rf /tmp/*Tmp.log
				exit 2
			fi

			{
				echo -n "ゾーン名："
				echo -n "`./kick_api.sh command=listZones id=${zone_id} | grep '<name>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}'`"
				echo "（`echo ${zone_name_j}`）"
				echo -n "ゾーン ID："
				echo "`./kick_api.sh command=listZones id=${zone_id} | grep '<id>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}'`"
				echo ""
				echo "ゾーン内 1 クラスター："
				echo -n "\"`./kick_api.sh command=listClusters id=${cluster_id} | grep -m 1 '<name>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}' | awk -F"/" '{print $3}'`\" = " >/tmp/clusterName1Tmp.log
				./kick_api.sh command=listClusters id=${cluster_id} | grep -m 1 '<managedstate>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}' >/tmp/clusterStateTmp.log
				state=`cat /tmp/clusterStateTmp.log`
			} >/tmp/clusterNameTmp.log

			state=`cat /tmp/clusterStateTmp.log`

			if [ ${state} = "Managed" ]; then
				state="管理対象"
			elif [ ${state} = "Unmanaged" ]; then
				state="非管理対象"
			elif [ ${state} = "PrepareUnmanaged" ]; then
				state="非管理対象化準備中"
			fi

			{
				echo "現在 \"${state}\""
				echo ""
			} >>/tmp/clusterName1Tmp.log

			echo ""
			echo "【 Step 1 】"
			echo ""
			echo "\"${zone_name_j}\" ゾーンから \"`cat ./prm/hypervisor.prm | grep "HYPERVISOR" | awk -F'=' '{print $2}'`\" クラスター 1 基を抽出し、管理対象化／非管理対象化します。"
			echo ""
			echo "========== 対象ゾーンおよびクラスターの現在状態 =========="
			echo ""
			echo "`cat /tmp/clusterNameTmp.log`"
			echo "`cat /tmp/clusterName1Tmp.log`"
			echo ""
			echo "=========================================================="
			echo ""
			echo ""

			while :
			do
				menu="非管理対象化する 管理対象化する ゾーン選択に戻る この処理をスキップして、全クラスター設定に進む 中止する"
				select ftest in ${menu}
				do
					case ${ftest} in
					非管理対象化する )
						ftest="Unmanaged"
						break 4
					;;
					管理対象化する )
						ftest="Managed"
						break 4
					;;
					ゾーン選択に戻る )
						rm -rf /tmp/*Tmp.log
						continue 4
					;;
					この処理をスキップして、全クラスター設定に進む )
						ftest=
						break 4
					;;
					中止する )
						echo
						echo "                  中止しました。"
						echo
						rm -rf /tmp/*Tmp.log
						exit 1
					;;
					* )
						echo
						echo "        数字キーで選択して下さい。"
						echo
						continue 2
					;;
					esac
				done
			done
		done
	done

	{
		one_cluster=`cat /tmp/OneClusterIdTmp.log`
		if [ -z "`echo ${ftest}`" ]; then
			date
			echo `./kick_api.sh command=listClusters id=${one_cluster}`
			echo ""
			echo "【 Step 1 】はスキップしました。"
			echo ""
		elif [ ! -z "`echo ${ftest}`" ]; then
			date
			./kick_api.sh command=updateCluster id=${one_cluster} managedstate="${ftest}"
			sleep 1
			echo `./kick_api.sh command=listClusters id=${one_cluster}`
			echo ""
		fi
	} >>./logs/updateCluster.log

	./kick_api.sh command=listClusters id=${one_cluster}

	./kick_api.sh command=listClusters hypervisor=${HYPERVISOR} zoneid=${zone_id} | grep '<id>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}' >/tmp/AllClusterIdsTmp.log

	for i in $(cat /tmp/AllClusterIdsTmp.log); do
		./kick_api.sh command=listClusters id=$i >/tmp/CStateNameTmp.log
		if [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'VMware'`" ]; then
			cat /tmp/CStateNameTmp.log | grep "<name>" \
				| awk -F">" '{print $2}' \
				| awk -F"<" '{print $1}' \
				| awk -F"/" '{print $3}' >/tmp/CStateName1Tmp.log
		elif [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'KVM'`" ]; then
			cat /tmp/CStateNameTmp.log | grep "<name>" \
				| awk -F">" '{print $2}' \
				| awk -F"<" '{print $1}' >/tmp/CStateName1Tmp.log
		else
			cat /tmp/CStateNameTmp.log | grep "<name>" \
				| awk -F">" '{print $2}' \
				| awk -F"<" '{print $1}' >/tmp/CStateName1Tmp.log
		fi

		cat /tmp/CStateNameTmp.log | grep "<managedstate>" \
			| awk -F">" '{print $2}' \
			| awk -F"<" '{print $1}' >/tmp/clstateTmp.log
		clstate=`cat /tmp/clstateTmp.log`

		if [ ${clstate} = "Managed" ]; then
			clstate="管理対象"
			echo ""
			echo "\"`cat /tmp/CStateName1Tmp.log`\" ＝ 現在 \"${clstate}\"" >>/tmp/AllClusterStateTmp.log
			echo ""
		elif [ ${clstate} = "Unmanaged" ]; then
			clstate="非管理対象"
			echo ""
			echo "\"`cat /tmp/CStateName1Tmp.log`\" ＝ 現在 \"${clstate}\"" >>/tmp/AllClusterStateTmp.log
			echo ""
		elif [ ${clstate} = "PrepareUnmanaged" ]; then
			clstate="非管理対象化準備中"
			echo ""
			echo "\"`cat /tmp/CStateName1Tmp.log`\" ＝ 現在 \"${clstate}\"" >>/tmp/AllClusterStateTmp.log
			echo ""
		fi
	done

	{
		echo -n "ゾーン ID："
		echo "`./kick_api.sh command=listZones id=${zone_id} | grep '<id>' | awk -F">" '{print $2}' | awk -F"<" '{print $1}'`"
		echo ""
	} >>/tmp/zoneidTmp.log

	echo ""
	echo "【 Step 2 】"
	echo ""
	echo "\"${zone_name_j}\" ゾーン配下すべての \"`cat ./prm/hypervisor.prm | grep "HYPERVISOR" | awk -F'=' '{print $2}'`\" クラスターを、管理対象化／非管理対象化します。"
	echo ""
	echo "========== 対象ゾーンおよびクラスターの現在状態 =========="
	echo ""
	echo -n "ゾーン："
	echo -n "${zone_name}"
	echo "（${zone_name_j}）"
	cat /tmp/zoneidTmp.log
	echo "ゾーン内全クラスター："
	cat /tmp/AllClusterStateTmp.log
	echo ""
	echo "=========================================================="
	echo ""

	while :
	do
		menu="すべての対象クラスターを非管理対象化 すべての対象クラスターを管理対象化 ゾーン選択に戻る 中止する"
		select ftest in ${menu}
		do
			case ${ftest} in
			すべての対象クラスターを非管理対象化 )
				ftest="Unmanaged"
				break 3 
			;;
			すべての対象クラスターを管理対象化 )
				ftest="Managed"
				break 3
			;;
			ゾーン選択に戻る )
				rm -rf /tmp/*Tmp.log
				continue 4
			;;
			中止する )
				echo
				echo "			中止しました。"
				echo
				rm -rf /tmp/*Tmp.log
				exit 0
			;;
			* )
				echo
				echo "        数字キーで選択して下さい。"
				echo
				continue 2
			;;
			esac
		done
	done
done

for i in $(cat /tmp/AllClusterIdsTmp.log); do
./kick_api.sh command=listClusters id=$i >/tmp/listClusterTmp.log
	cat /tmp/listClusterTmp.log \
	| grep -m 1 "<managedstate>" \
	| awk -F">" '{print $2}' \
	| awk -F"<" '{print $1}' >/tmp/CStateTmp.log
	ssstate=`cat /tmp/CStateTmp.log`
	echo ${ssstate}

if [ "${ftest}" = "${ssstate}" ]; then
	if [ "Managed" = "${ssstate}" ]; then
		ssstate="管理対象"
	elif [ "Unmanaged" = "${ssstate}" ]; then
		ssstate="非管理対象"
	elif [ "PrepareUnmanaged" = "${ssstate}" ]; then
		ssstate="非管理対象化準備中"
	fi
	./kick_api.sh command=listClusters id=$i >/tmp/CStateNameTmp.log
	if [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'VMware'`" ]; then
		cat /tmp/CStateNameTmp.log | grep "<name>" \
			| awk -F">" '{print $2}' \
			| awk -F"<" '{print $1}' \
			| awk -F"/" '{print $3}' >/tmp/clusterNameTmp.log
	elif [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'KVM'`" ]; then
		cat /tmp/CStateNameTmp.log | grep "<name>" \
			| awk -F">" '{print $2}' \
			| awk -F"<" '{print $1}' >/tmp/clusterNameTmp.log
	else
		:
	fi
	cluster_name=`cat /tmp/clusterNameTmp.log`
	date >>./logs/updateCluster.log
	./kick_api.sh command=listClusters id=$i >>./logs/updateCluster.log
	./kick_api.sh command=listClusters id=$i
	echo ""
	echo "		\"${cluster_name}\" は既に \"${ssstate}\" です。" >>./logs/updateCluster.log
	echo "		\"${cluster_name}\" は既に \"${ssstate}\" です。"
	echo "" >>./logs/updateCluster.log
	echo ""
	echo "			変更しません。" >>./logs/updateCluster.log
	echo "			変更しません。"
	echo "" >>./logs/updateCluster.log
	echo ""
else
	if [ "Managed" = "${ssstate}" ]; then
		ssstate2="非管理対象"
	elif [ "Unmanaged" = "${ssstate}" ]; then
		ssstate2="管理対象"
	elif [ "PrepareUnmanaged" = "${ssstate}" ]; then
		ssstate2="非管理対象"
	else
		:
	fi
	if [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'VMware'`" ]; then
		cat /tmp/CStateNameTmp.log | grep "<name>" \
			| awk -F">" '{print $2}' \
			| awk -F"<" '{print $1}' \
			| awk -F"/" '{print $3}' >/tmp/clusterNameTmp.log
	elif [ ! -z "`cat ./prm/hypervisor.prm | egrep -i 'KVM'`" ]; then
		cat /tmp/CStateNameTmp.log | grep "<name>" \
			| awk -F">" '{print $2}' \
			| awk -F"<" '{print $1}' >/tmp/clusterNameTmp.log
	else
		:
	fi
	cluster_name=`cat /tmp/clusterNameTmp.log`
	echo ""
	echo "		\"${cluster_name}\" を \"${ssstate2}\" に切り替えます。"
	echo ""
	date >>./logs/updateCluster.log
	./kick_api.sh command=updateCluster id=$i managedstate=${ftest} >>./logs/updateCluster.log
	./kick_api.sh command=listClusters id=$i
	echo "" >>./logs/updateCluster.log
	echo ""
fi
usleep 1500000
done

echo ""
echo ""
echo ""
echo ""
echo "cat ./logs/updateCluster.log"
echo ""
cat ./logs/updateCluster.log

echo ""
echo "			終了しました。"
echo ""

rm -rf /tmp/*Tmp.log
exit 0
