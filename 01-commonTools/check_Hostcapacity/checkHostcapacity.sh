#!/bin/sh

DBVIP="ckktky4-pcldb10"

echo "====================【Capacity Check VR only host】===================="
echo ""


hostname=`mysql -u root cloud -h $DBVIP -s -N -e 'select host.name from host inner join host_tags on host.id=host_tags.host_id where host.removed is NULL and host.type="Routing" and host_tags.tag="VR" order by host.cluster_id;'`

for i in $hostname ; do

ZONENAME=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<zonename>' |sed -e 's/ //g' | sed -e 's/<zonename>//g'  | sed -e 's/<\/zonename>//g'`
CLUSTERNAME=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<clustername>' |sed -e 's/ //g' | sed -e 's/<clustername>//g'  | sed -e 's/<\/clustername>//g'`
CS_HOSTNAME=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<name>' |sed -e 's/ //g' | sed -e 's/<name>//g'  | sed -e 's/<\/name>//g'`
HOSTTAG=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<hosttags>' |sed -e 's/ //g' | sed -e 's/<hosttags>//g'  | sed -e 's/<\/hosttags>//g'`
HYPERVISOR=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<hypervisor>' |sed -e 's/ //g' | sed -e 's/<hypervisor>//g'  | sed -e 's/<\/hypervisor>//g'`
STATE=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<state>' |sed -e 's/ //g' | sed -e 's/<state>//g'  | sed -e 's/<\/state>//g'`
CPUALLCATED=`./kick_api.sh command=listHosts  2>/dev/null name=$i | grep '<cpuallocated>' |sed -e 's/ //g' | sed -e 's/<cpuallocated>//g'  | sed -e 's/<\/cpuallocated>//g' |sed -e 's/%//g' `

threshold=85
result=`echo "$CPUALLCATED > $threshold" |bc `

capacity_100=100
result2=`echo "$CPUALLCATED > $capacity_100" |bc `

   if [ $result2 -eq "1" ] ; then
echo "【$CS_HOSTNAME($HOSTTAG)】WARN: CPU割り当て率が100%を超過しました。現在のCPU割り当て率は$CPUALLCATED% 【$ZONENAME $CLUSTERNAME】"
   elif [ $result -eq "1" ] ; then
echo "【$CS_HOSTNAME($HOSTTAG)】WARN: CPU割り当て率が100%に達しそうです。現在のCPU割り当て率は$CPUALLCATED% 【$ZONENAME $CLUSTERNAME】"
   else
echo "【$CS_HOSTNAME($HOSTTAG)】INFO: CPU割り当て率に余裕があります。現在のCPU割り当て率は$CPUALLCATED% 【$ZONENAME $CLUSTERNAME】"
   fi

sleep 1
done ;

echo ""
echo "====================【Capacity Check VR only host】===================="

