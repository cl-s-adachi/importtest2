# -*- coding: utf-8 -*-

# Python standard Libary
import sys
import os.path
import logging

# Exoscale cs
from cs import CloudStack

# KCPS
from cs_common import *


def cs_list_host(hostname):
    """
    ホスト情報を取得する

    :param str hostname:
    """
    cs = connectCloudStack()
    return cs.listHosts(name=hostname, listall=True)

