# -*- coding: utf-8 -*-

# Python standard Libary
import sys, codecs, re
import os.path
import logging
import json
import time
import functools
from datetime import datetime, timedelta

# PyYaml
import yaml

# fabric
from fabric.api import local, run, execute, put
from fabric.api import env, settings
from fabric.decorators import *
from fabric.context_managers import *
from fabric.colors import *
from fabric.utils import puts


# Exoscale cs
from cs import CloudStack

#  KCPS
#from cs_common import *
import cs_common
import cs_domain
#import config
from util import *


def dump_routers(routers):
    if not routers:
        logging.warning(yellow('No router information given.'))
        return

    for r in routers['router']:
        print(green('Name: %s | ID: %s | Link Local IP: %s | State: %s | Hostname: %s | Hypervisor: %s' %
                (r['name'], r['id'], r['linklocalip'], r['state'], r['hostname'], r['hypervisor'])))

def cs_get_routers(m_num = None, networkUuid = None, vr_name = None):
    """
    ルータの情報を取得する

    :param str m_num: M番号
    :param str networkUuid: Network UUID
    :return :
    """
    cs = cs_common.connectCloudStack()

    if m_num:
        domain_id = cs_domain.get_id(m_num)
        if not domain_id:
            logging.info('Domain not found: ' + m_num)
            return None
        result = cs.listRouters(domainid = domain_id, listall = True)
    elif networkUuid:
        result = cs.listRouters(networkid = networkUuid, listall = True)
    elif vr_name:
        result = cs.listRouters(name = vr_name, listall = True)
    else:
        logging.warn('Wrong number of argument.')
        return None


    if env.debug:
        dump_json(result)

    return result

def cs_get_router(routerName):
    """
    ルータの情報を取得する

    :param str routerName: ルータのVM名
    """
    cs = cs_common.connectCloudStack()
    router = cs.listRouters(name = routerName, listall = True)

    return router

def get_network_type(router):
    """
    ネットワークがSharedかIsolatedかを返す

    :param list rotuer: ルータ情報
    """
    for nic in router['nic']:
        if nic['traffictype'] == 'Guest':
            return nic['type']

    return None

def get_vr_link_local_ip(vr_name):
    cs = cs_common.connectCloudStack()
    result = cs.listRouters(name=vr_name, listall=True)

    if env.debug:
        dump_json(result)

    if not result or len(result) == 0:
        return None

    return result['router'][0]['linklocalip']

def get_link_local_ip_list(**kwarg):
    """
    VRのLink Local IPを取得する

    :param dict **kwarg: 以下のいずれかのキーと値を含むkwarg
        m_num
        vr_name
        network_uuid
    """
    m_num = kwarg.get('m_num', None)
    vr_name = kwarg.get('vr_name', None)
    network_uuid = kwarg.get('network_uuid', None)

    link_local_ip = []
    if network_uuid:
        for nw in network_uuid:
            result = cs_get_routers(None, nw['network_uuid'])
            if not result:
                logging.warn(yellow('[%s:%s] Router not found.' % (nw['m_num'], nw['network_uuid'])))
                continue

            print(green(str(result)))
            link_local_ip += map(lambda d: d['linklocalip'], result['router'])
    elif m_num:
        result = cs_get_routers(m_num)
        if not result:
            logging.warn(yellow('[%s] Domain not found.' % (m_num)))
            return None
        link_local_ip += map(lambda d: d['linklocalip'], result['router'])
    elif vr_name:
        ip = get_vr_link_local_ip(vr_name)
        if not ip:
            logging.warn(yellow('[%s] Router not found.' % (vr_name)))
            return None

        link_local_ip = [ip]
    else:
        logging.warn('Argument error: m_num or vr_name must be set.')
        return None

    return link_local_ip


