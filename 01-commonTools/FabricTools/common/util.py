# -*- coding: utf-8 -*-
from fabric.api import env
from fabric.api import put
from fabric.api import run
from fabric.api import settings
from fabric.colors import *
import json
import logging
import distutils.util



def dump_json(json_dict):
    print(json.dumps(json_dict, indent=2))


def get_formated_message(m_num, vr_name, msg):
    """
    ログファイル用メッセージの生成

    :param str m_num: M番号
    :param str vr_name: VR名
    :param str msg: メッセージ
    :return str: フォーマット済のメッセージ
    """
    return '[%s:%s] %s' % (m_num, vr_name, msg)


def create_mysql_command(query):
    """
    MySQL実行コマンド文字列の生成
    mysqlクライアントがインストールされ、PATHが通っている必要あり
    """
    if env.database['password']:
        command = 'mysql -h %(host)s --port=%(port)s --user=%(user)s --password=%(password)s --skip-column-names -U %(db_name) -e \'%(query)s\''
    else:
        command = 'mysql -h %(host)s --port=%(port)s --user=%(user)s --skip-column-names -U %(db_name)s -e \'%(query)s\''

    return command % {
        'host': env.database['host'],
        'port': env.database['port'],
        'user': env.database['user'],
        'db_name': env.database['name'],
        'password': env.database['password'],
        'query': query }


# TODO: Error処理を修正する。一部ファイルのみアップロードに失敗した場合等。
def copy_file(targetHost, files, envParam):
    """
    ssh gatewayを介してファイルコピーする。

    param: str targetHost: ファイルのコピー先
    param: list envTarget: コピーするファイル情報(dict)を含むList
            src_file
            dst_file
            mode
            md5sum
    param: list envParam: "with settings" で使用する parameter一覧
    """
    logging.debug('Execute _do_copy_file. target host: %s' % (targetHost))
    res = True

    with settings(**envParam):
        for f in files:
            put(f["src_file"], f["dst_file"], mode=f["mode"])
            src_file_md5sum = f.get("md5sum", None)

            # ymlファイルに md5sum が設定されている場合のみ値を比較する
            if src_file_md5sum:
                cmd_res = run("md5sum %s " % (f["dst_file"]))
                if cmd_res.succeeded:
                    dst_file_md5sum = cmd_res.split(" ")[0]
                    logging.debug("md5sum: " + dst_file_md5sum)
                    if not dst_file_md5sum == src_file_md5sum:
                        res = False
                        logging.error(red("md5sum is different: src file: %s  dst file: %s" % (
                                            src_file_md5sum,
                                            dst_file_md5sum)))
                    else:
                        logging.info(green("md5sum is OK: %s" % (f["dst_file"])))
                else:
                    res = False
                    logging.error("md5sum command failed")
    return res



def exec_command(command, targetHost, envParam):
    """ 
    ssh gatewayを介してコマンドを実行する。
        
    param: str targetHost: ファイルのコピー先
    param: list envTarget: コピーするファイル情報
            src_file 
            dst_file
            mode
            md5sum
    param: list envParam: "with settings" で使用する parameter一覧
    """
    logging.debug('Execute exec_command. target host: %s' % (targetHost))

    with settings(**envParam):
        cmd_result = run(command)


def str_to_bool(flag):
    """
    文字列をboolに変換する。
    """
    try:
        flag = distutils.util.strtobool(flag)
    except Exception:
        flag = False

    return flag
