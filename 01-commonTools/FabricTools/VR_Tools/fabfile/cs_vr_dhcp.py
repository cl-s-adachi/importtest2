# -*- coding: utf-8 -*-

from fabric.api import local, run, env, settings
#from fabric.utils import puts
from fabric.colors import *
from fabric.api import *
from fabric.decorators import task, runs_once


######################3
# Python standard Libary
import codecs
import csv
import functools
import logging
import json
import os.path
import re
import sys
import time
from datetime import *
from time import *


# Exoscale cs
from cs import CloudStackException

# KCPS
#import common
#from common.cs_vr import *
from common import cs_vr
from common import config
from common import util


# DHCPインストール済のVR名を記載したファイル
DHCP_INSTALLED_VR_FILE='dhcp_installed_vr.txt'
DHCP_INSTALL_LOG_FILE='dhcp_install_results.log'

@task
def dhcp_check_lease_file(install_flag = False):
    """
    VR内の/etc/dhcphosts.txtと/var/lib/misc/dnsmasq.leasesの件数が同じか比較する。

    """
    router_info = _get_target_routers_from_db()

    if not router_info:
        logging.warning(yellow("No router found."))
        exit(1)

    routers_running = router_info.get('router_running')

    env.hosts = router_info.get('linklocalip_list')
    results = execute(_dhcp_check_lease_file, routers_running)

    result_all = results.values()
    result_ok = filter(lambda d: d['status'] == 'OK', result_all)
    result_ng = filter(lambda d: d['status'] == 'NG', result_all)

    ng_flag = True if len(result_ng) else False
    if not ng_flag:
        logging.info(blue("All VR has dhcp files installed"))

    _print_dhcp_check_lease_results(result_all)

    # NGのみ/var/log/messagesに出力する
    _logger_check_lease_ng_results(result_ng)

    if util.str_to_bool(install_flag) and ng_flag:
        # DHCP関連ファイル未配布のVRにファイルを配布する
        logging.info(yellow('deploy dhcp files'))
        linklocalip_list_ng = map(lambda d: d['linklocalip'], result_ng)
        print(linklocalip_list_ng) 
        routers_not_installed = filter(lambda d: d['linklocalip'] in linklocalip_list_ng , routers_running)
        print(routers_not_installed)
        
        env.hosts = linklocalip_list_ng
        results = execute(_dhcp_exec_all, routers_not_installed)
        _print_results(results)


def _logger_check_lease_ng_results(results, msg = 'dnsmasq lease file check error'):
    """
    dnsmasql leaseファイルチェクNGのものをログ出力する
    """
    for v in results:
        msg = '[%s:%s][%s] %s %s' % (v.get('m_num', ''), v.get('vr_name', ''), v.get('linklocalip', ''), v.get('status', 'Status Unknown'), 'Check dnsmasq files failed.')
        #TODO Loggerの出力フォーマット確認
        local('logger -t "%s" "%s"' % ("[INFO] [KCPS-DHCP-CHECK]", msg))


@parallel(pool_size=10)
def _dhcp_check_lease_file(routers):
    """
    VR内の/etc/dhcphosts.txtと/var/lib/misc/dnsmasq.leasesの件数が同じか比較する。
    """
    logging.info('_dhcp_check_lease_file')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    # _dhcp_check_lease_fileのみlinklocalip,m_num,vr_nameをレスポンスに追加
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num, 'linklocalip': env.host_string}

    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    # DHCP file install check
    # TODO
    dhcp_installed = _check_dhcp_install()
    if dhcp_installed:
        msg = 'DHCP file is installed.'
        logging.info(green(get_msg(msg)))
    else:
        msg = 'DHCP file is not installed.'
        logging.info(yellow(get_msg(msg)))
        res['msg'] = msg
        res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        return res

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        try:
            dhcphosts_file = '/etc/dhcphosts.txt'
            dnsmasql_file = '/var/lib/misc/dnsmasq.leases'

            # /etc/dhcphosts.txt存在可否
            cmd_res = run("test -f %s" % (dhcphosts_file))
            if cmd_res.return_code == 1:
                # /etc/dhcphosts.txtは存在しない場合もあるのので status=OK
                res['status'] = 'OK'
                msg = '"%s" does not exist.' % (dhcphosts_file)
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                logging.info(yellow(get_msg(msg)))
                return res
            
            # 設定ファイル行数比較
            #command = 'wc -l %s %s' % (dhcphosts_file, dnsmasql_file)
            command = 'wc -l %s' % (dhcphosts_file)
            # test -f /etc/dhcphosts.txt && wc -l /etc/dhcphosts.txt /var/lib/misc/dnsmasq.leases  || echo "NG"
            cmd_res1 = run(command)
            if cmd_res1.failed:
                # TODO
                # コマンド実行に失敗しているのでException投げたほうが良いか？
                res['status'] = 'NG'
                msg = '"%s" failed.' % (command)
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                logging.error(red(get_msg(msg)))
                return res

            command = 'wc -l %s' % (dnsmasql_file)
            cmd_res2 = run(command)
            if cmd_res2.failed:
                # TODO
                # コマンド実行に失敗しているのでException投げたほうが良いか？
                res['status'] = 'NG'
                msg = '"%s" failed.' % (command)
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                logging.error(red(get_msg(msg)))
                return res

        
            # TODO: check
            #ret_line = cmd_res.split('\n')
            #dhcphost_cnt = ret_line[0].split(" ")[0]
            dhcphost_cnt = cmd_res1.split(" ")[0]
            #dnsmasq_lease_cnt = ret_line[1].split(" ")[0]
            dnsmasq_lease_cnt = cmd_res2.split(" ")[0]
            if not dhcphost_cnt == dnsmasq_lease_cnt:
                msg = 'Number of lines does not match: dhcphost.txt(%s) : dnsmasq.leases(%s)' % (dhcphost_cnt, dnsmasq_lease_cnt)
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                logging.warn(yellow(get_msg(msg)))
                return res
        except Exception as e:
            msg = 'Failed to check dnsmasq lease file. %s' % (e)
            res['status'] = 'NG'
            res['msg'] = msg
            logging.error(red(get_msg(msg)))
            res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        else:
            msg = 'Successfuly check dnsmasq lease file.'
            res['status'] = 'OK'
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        finally:
            return res


@task
def dhcp_check_install(m_num = None, vr_name = None, target_file = None):
    """
    VRにDHCP関連ファイルが配置されているかチェックする

    :param str m_num: M番号
    :param str vr_name: VR名
    :param str target_file: チェック対象のネットワーク一覧を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')
    results = execute(_dhcp_check_install_parallel, routers_running)
    _print_results(results)


@parallel(pool_size=10)
def _dhcp_check_install_parallel(routers):
    """
    DHCP moduleがインストールされているか確認する

    :param list rotuers:

    """
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    dhcp_installed = _check_dhcp_install()

    if dhcp_installed:
        msg = get_msg('DHCP file is installed')
        logging.info(green(msg))
        res['status'] = 'OK'
        res['msg'] = msg
    else:
        msg = get_msg('DHCP file is not installed')
        logging.info(yellow(msg))
        res['msg'] = msg

    res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    return res


def _check_dhcp_install():
    """
    DHCP関連ファイルがインストールされているかチェックする。

    :return bool: True - インストール済。 False - 未インストール
    """
    res = True
    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        # TODO: md5sumの値を比較するよう修正
        for f in env.vr['files_dhcp']:
            dst_file = f['dst_file']
                
            # fileが存在するか確認
            cmd_res = run("test -f %s" % (dst_file))
            if cmd_res.return_code == 1:
                # ファイルが存在しないのでエラー
                logging.error(red('[%s] %s not found' % (
                                   env.host_string,
                                   dst_file)))
                return False

            src_file_md5sum = f.get('md5sum', None)
            command = 'md5sum %s' % (dst_file)
            cmd_res = run(command)
            if cmd_res.succeeded:
                dst_file_md5sum = cmd_res.split(" ")[0]
                logging.debug('md5sum: ' + dst_file_md5sum)

            if not dst_file_md5sum == src_file_md5sum:
                res = False
                logging.error(red('[%s] md5sum is different: %s : src file: %s  dst file: %s' % (
                                   env.host_string,
                                   f['dst_file'],
                                   src_file_md5sum,
                                   dst_file_md5sum)))
            else:
                logging.info(green('[%s] md5sum is OK: %s' % (env.host_string, f['dst_file'])))

    return res


def _dhcp_check_install_from_file(vr_name):
    """
    DHCPスクリプトがインストール済かチェックする。

    :param str vr_name: VR名
    :return bool: True: インストール済み False: 未インストール
    """
    vol_created = cs_vr.get_volume_created(vr_name)
    if env.dhcp_install_vr:
        data = filter(lambda d: d['vr_name'] == vr_name and d['created'] == vol_created, env.dhcp_install_vr)
        if data:
            return True

    return False


@task
def dhcp_exec_all(m_num = None, vr_name = None, target_file = None):
    """
    ファイルをVRにコピーしスクリプトを実行する。

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    """
    results = execute(_exec_command, routers_running, "cp -p /opt/cloud/bin/cs/CsDhcp.py /opt/cloud/bin/cs/CsDhcp.py.bk")
    _print_results(results)
    results = execute(_dhcp_upload_files, routers_running)
    _print_results(results)
    results = execute(_exec_command, routers_running, "/root/modVRdhcp.sh")
    _print_results(results)
    results = execute(_exec_command, routers_running, "/root/checkVRdhcp.sh")
    _print_results(results)
    """
    results = execute(_dhcp_exec_all, routers_running)
    _print_results(results)


@parallel(pool_size=10)
def _dhcp_exec_all(routers):
    logging.info('_upload_cs_dhcp')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    try:
        results = _exec_command(routers, "cp -p /opt/cloud/bin/cs/CsDhcp.py /opt/cloud/bin/cs/CsDhcp.py.bk")
        print(blue(results))
        if results['status'] == 'NG':
            return results

        results = _dhcp_upload_files(routers)
        if results['status'] == 'NG':
            return results

        results = _exec_command(routers, "/root/modVRdhcp.sh")
        if results['status'] == 'NG':
            return results

        results = _exec_command(routers, "/root/checkVRdhcp.sh")
        if results['status'] == 'NG':
            return results
    except Exception as e:
        msg = str(e)
        logging.error(get_msg(msg))
        res['msg'] = msg
        res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        res['status'] = 'NG'
    else:
        msg = get_msg('finish')
        logging.info(msg)
        res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        res['msg'] = msg
        res['status'] = 'OK'
    finally:
        return res


@task
def dhcp_upload_files(m_num = None, vr_name = None, target_file = None):
    """
    DHCP関連ファイルをVRにコピーする。

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    results = execute(_dhcp_upload_files, routers_running)
    _print_results(results)


@parallel(pool_size=10)
def _dhcp_upload_files(routers):
    """
    ファイルをVRにコピーする。
    """
    logging.info('_upload_cs_dhcp')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    cp_res = util.copy_file(vr_name, env.vr['files_dhcp'], params)

    # TODO 
    if cp_res:
        msg = get_msg('Successfuly upload files.')
        res['status'] = 'OK'
        res['msg'] = msg
    else:
        msg = get_msg('Failed to upload some files.')
        res['msg'] = msg

    res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    return res


@task
def get_system_log_files(m_num = None, vr_name = None, target_file = None):
    """
    VRからシステム関連ログファイル取得する。

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    backup_dir = 'backup_vr_system_log/' + datetime.now().strftime('%Y%m%d_%H%M')
    results = execute(_get_files, routers_running, ['/var/log/dnsmasq.log'], backup_dir)
    _print_results(results)


@task
def get_dhcp_files(m_num = None, vr_name = None, target_file = None):
    """
    DHCP関連ファイルをVRからコピーする

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    backup_dir = 'backup/' + datetime.now().strftime('%Y%m%d_%H%M')
    results = execute(_get_files, routers_running, ['/var/lib/misc/dnsmasq.leases', '/etc/dhcphosts.txt'], backup_dir)
    _print_results(results)


@parallel(pool_size=10)
def _get_files(routers, file_list, backup_dir = 'backup/'):
    """
    ファイルをVRにコピーする。
    """
    logging.info('_get_files')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        if backup_dir and backup_dir[-1:] != '/':
            backup_dir += '/'

        backup_dir += '%(host)s/%(path)s'
        try:
            for f in file_list:
                get(f, backup_dir)
        except Exception as e:
            msg = get_msg(e)
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        else:
            msg = get_msg('Successfuly download files.')
            res['status'] = 'OK'
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        finally:
            return res


@task
def dhcp_rollback_files(m_num = None, vr_name = None, target_file = None):
    """
    DHCP関連ファイルをRollbackする。

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    results = execute(_dhcp_rollback_files, routers_running)
    _print_results(results)


@parallel(pool_size=10)
def _dhcp_rollback_files(routers):
    """
    DHCP関連ファイルをRollbackする。
    """
    logging.info('_exec_command')
    
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)
        
    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    success_flag = True
    # TODO
    with settings(**params):
        command = "cp -p /opt/cloud/bin/cs/CsDhcp.py.bk /opt/cloud/bin/cs/CsDhcp.py"
        cmd_res = run(command)
        if cmd_res.failed:
            success_flag = False
            msg = get_msg('"%s" failed.' % (command))
            logging.error(red(msg))

        command = "cp -p /root/dhcpbackup/dhcphosts.txt.bk /etc/dhcphosts.txt"
        cmd_res = run(command)
        if cmd_res.failed:
            success_flag = False
            msg = get_msg('"%s" failed.' % (command))
            logging.error(red(msg))

        command = "cp -p /root/dhcpbackup/dnsmasq.leases.bk /var/lib/misc/dnsmasq.leases"
        cmd_res = run(command)
        if cmd_res.failed:
            success_flag = False
            msg = get_msg('"%s" failed.' % (command))
            logging.error(red(msg))

        if success_flag:
            msg = get_msg('rollback succeeded.')
            res['status'] = 'OK'
            res['msg'] = msg
            logging.info(green(msg))
        else:
            msg = get_msg('rollback failed.')
            res['msg'] = msg
            logging.error(red(msg))

    res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    return res


@parallel(pool_size=10)
def _exec_command(routers, command):
    """
    引数で実行したコマンドを実行する
    """
    logging.info('_exec_command')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        cmd_res = run(command)
        if cmd_res.succeeded:
            msg = get_msg('"%s" succeeded.' % (command))
            res['status'] = 'OK'
            res['msg'] = msg
            logging.info(green(msg))
        else:
            msg = get_msg('"%s" failed.' % (command))
            res['msg'] = msg
            logging.error(red(msg))

    res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    return res


@task
def dnsmasq(m_num = None, vr_name = None, target_file = None, cmd_param = 'status'):
    """
    /etc/init.d/dnsmasqを実行する。

    :param str m_num: 対象M番号
    :param str vr_name: VR名
    :param str target_file: 対象VR名を含むファイル
    :param str cmd_param: /etc/init.d/dnsmasq に渡す引数。デフォルトは "status"。
    """
    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')
    results = execute(_exec_command, routers_running, '/etc/init.d/dnsmasq %s' % (cmd_param))
    _print_results(results)


@task
def check_file(filename, m_num = None, vr_name = None, target_file = None):
    if not m_num and not vr_name and not target_file:
        logging.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    router_info = _get_target_routers(routers)

    routers_running = router_info.get('routers_running')
    env.hosts = router_info.get('linklocalip_list')

    results = execute(_check_file, routers_running, filename)
    #TODO: リザルト表示


@parallel(pool_size=10)
def _check_file(routers, filename):
    logging.info('_check_file')

    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    util.exec_command("ls -al %s" % (filename), vr_name, params) 


def _load_target(target_file):
    """
    targetファイルをロードする

    :param str target_file: Targetファイルのパス
    :return list: 以下の値を含むdictを含むlist
        vr_name
    """
    logging.info('load target file. %s' % target_file)
    if not target_file:
        logging.error("argument target_file not set.")

    if not os.path.exists(target_file):
        logging.error("target file not found. %s" % target_file)
        return False

    targets = []
    try:
        f = open(target_file, "r")
        reader = csv.reader(f)
        for row in reader:
            if len(row) > 0:
                if row[0][0:1] == '#':
                    # 先頭行が "#" はコメントとみなす
                    continue

                targets.append({'vr_name': row[0]})

        f.close()
    except Exception as e:
        print(red(e.message))
        logging.error('設定ファイルを開けません。%s' % target_file)
        return False

    return targets


def _get_routers(**kwargs):
    """
    仮想ルータ一覧を取得する。

    :param dict **kwargs: 引数に取るキーは以下。複数指定された場合は target_file が優先して使用される。
        m_num: M番号に紐づく仮想ルータを返す。
        vr_name: 仮想ルータ名から仮想ルータ情報を取得する。
        target_file: M番号、Network UUID一覧を含むファイル名。指定されたNetwork UUIDに紐づく仮想ルータ情報を取得する。
    """
    target_file = kwargs['target_file']
    m_num = kwargs['m_num']
    vr_name = kwargs['vr_name']

    routers = []
    if target_file:
        vr_list = _load_target(target_file)
        if not vr_list:
            return None

        for vr_name in vr_list:
            try:
                router = cs_vr.cs_get_routers(None, None, vr_name.get('vr_name'))
                if router:
                    routers += router['router']
                else:
                    logging.info('[%s] Router not found' % (vr_name))
            except CloudStackException as e:
                logging.error(red('[%s] %s' % (vr_name,
                                                  e.args[2].get('errortext', ""))))
    else:
        routers = cs_vr.cs_get_routers(m_num, None, vr_name).get('router', None)

    return routers


def _get_target_routers(routers):
    """
    処理対象のルータ情報を取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    router_info = {}
    routers_stopped = filter(lambda d: d.get('linklocalip', None) == None, routers)
    for vr in routers_stopped:
        logging.info(yellow('[%s:%s] Linked Local IP not set. State: %s' % (vr['domain'], vr['name'], vr['state'])))
        #TODO: ログ出力

    router_info['routers_stopped'] = routers_stopped
    
    routers_running = filter(lambda d: d.get('linklocalip', None) != None, routers)
    linklocalip_list = map(lambda d: d['linklocalip'], routers_running)

    router_info['routers_stopped'] = routers_stopped
    router_info['routers_running'] = routers_running
    router_info['linklocalip_list'] = linklocalip_list

    return router_info

@task
def dump_vr_from_db():
    """
    操作対象となるVRをDBから取得し表示する。
    """
    data = _get_target_routers_from_db()

    for d in data['router_running']:
       print('%s\t%s\t%s\t%s' % (d['name'], d['linklocalip'],d['domain'],d['state']))

    for d in data['router_stopped']:
       print('%s\t%s\t%s\t%s' % (d['name'], d['linklocalip'],d['domain'],d['state']))


def _get_target_routers_from_db():
    """
    処理対象のルータ情報をCSDBから取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    sql = "SELECT vm.name, vm.private_ip_address, d.name, vm.state" \
          " FROM nics AS ni" \
          " LEFT JOIN vm_instance AS vm ON ni.instance_id=vm.id" \
          " LEFT JOIN networks AS net ON ni.network_id=net.id" \
          " LEFT JOIN domain AS d ON vm.domain_id=d.id" \
          " LEFT JOIN host AS h ON vm.host_id=h.id" \
          " LEFT JOIN host AS hl ON vm.last_host_id=hl.id" \
          " WHERE net.removed IS NULL AND vm.removed IS NULL AND vm.type LIKE \"DomainRouter\" AND net.guest_type IS NOT NULL" \
          " ORDER BY vm.created;"
      
    queryResults = local(util.create_mysql_command(sql), capture=True)

    router_info = {}
    ids = []
    if queryResults:
        for result in queryResults.split('\n'):
            resultList = result.split('\t')
            ids.append({ 'name':     resultList[0],
                         'linklocalip': resultList[1],
                         'domain':       resultList[2],
                         'state':       resultList[3]})
    # linklocalipが "169.254" で始まるものはKVMのVRで古くもう存在しないが
    # CSDB上にゴミとして残っているので結果に含めない
    router_info['router_running'] = filter(lambda d: d['state'] == "Running" and not d['linklocalip'].startswith('169.254.'), ids)
    router_info['router_stopped'] = filter(lambda d: d['state'] == "Stopped", ids)
    router_info['linklocalip_list'] = map(lambda d: d['linklocalip'], router_info['router_running'])

    return router_info


def _print_results(results, log_file = DHCP_INSTALL_LOG_FILE):
    """
    結果をファイルに出力する

    :param list results: 結果を含むlist
      status
      start_time
      end_time
      msg
    """
    path = env.log_dir + '/' + log_file
    with open(path, 'a') as f:
        f = codecs.lookup('utf_8')[-1](f)
        for k, v in results.items():
            msg = '%s [%s] %s \t%s' % (v.get('end_time', ''), k, v.get('status', 'Status Unknown'), v.get('msg', ''))
            f.write('%s\n' % msg.replace('\r', ''))


def _print_dhcp_check_lease_results(results, log_file = DHCP_INSTALL_LOG_FILE):
    """
    dnsmasql lease fileチェック結果をファイルに出力する

    :param list results: 結果を含むlist
      m_num
      vr_name
      linklocalip
      status
      start_time
      end_time
      msg
    """
    path = env.log_dir + '/' + log_file
    with open(path, 'a') as f:
        f = codecs.lookup('utf_8')[-1](f)
        for v in results:
            msg = '%s [%s:%s][%s] %s \t%s' % (v.get('end_time', ''), v.get('m_num'), v.get('vr_name', ''), v.get('linklocalip', ''), v.get('status', 'Status Unknown'), v.get('msg', ''))
            f.write('%s\n' % msg.replace('\r', ''))


@task
def get_cs_dhcp_py_md5sum():
    filepath = '/opt/cloud/bin/cs/CsDhcp.py'
    md5sum = _get_dhcp_file_md5sum(filepath)
    print('%s: %s' % (filepath, md5sum))

def _get_dhcp_file_md5sum(filepath):
    """
    設定ファイルから CsDhcp.py のmd5sum値を取得する。
    """
    file_name = '/opt/cloud/bin/cs/CsDhcp.py'
    data = filter(lambda d: d['dst_file'] == file_name, env.vr['files_dhcp'])
    if not data:
        logger.error('md5sum not found for %s' % (file_name))
        return None
    md5sum = data[0]['md5sum']
    return md5sum


