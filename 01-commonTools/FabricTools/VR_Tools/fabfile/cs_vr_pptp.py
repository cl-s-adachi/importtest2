# -*- coding: utf-8 -*-

from fabric.api import local, run, env, settings
from fabric.utils import puts
from fabric.colors import *
from fabric.api import *
from fabric.decorators import task, runs_once


######################3
# Python standard Libary
import codecs
import csv
import functools
import logging
import json
import os.path
import re
import sys
import time
from datetime import *
from time import *

# Exoscale cs
from cs import CloudStackException

# KCPS
#import common
#from common.cs_vr import *
from common import cs_vr
from common import config
from common import util


# PPTPモジュールインストール済のVR名を記載したファイル
PPTP_INSTALLED_VR_FILE='pptp_installed_vr.txt'


@task
def pptp_check_install(m_num = None, vr_name = None, target_file = None):
    """
    VRにPPTPモジュールがインストールされているかチェックする

    :param str vr_name: VR名
    :param str version: dnsmasqのバージョン。
    :param str target_file: チェック対象のネットワーク一覧を含むファイル
    """
    if not m_num and not vr_name and not target_file:
        loggeing.warn('Argument error: m_num or vr_name must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    link_local_ip = map(lambda d: str(d['linklocalip']), routers)

    env.hosts = link_local_ip
    results = execute(_pptp_check_install_parallel, routers)
    _print_results(results)


@parallel(pool_size=10)
def _pptp_check_install_parallel(routers):
    """
    PPTP moduleがインストールされているか確認する

    :param list rotuers:

    """
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    pptp_installed = _check_pptp_install()

    if pptp_installed:
        msg = get_msg('PPTP module is installed')
        logging.info(green(msg))
        res['status'] = 'OK'
        res['msg'] = msg
    else:
        msg = get_msg('PPTP module is not installed')
        logging.info(yellow(msg))
        res['msg'] = msg

    res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    return res


def _check_pptp_install():
    """
    PPTPモジュールがインストールされているかチェックする。

    :return bool: True - インストール済。 False - 未インストール
    """
    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        command = 'lsmod | grep pptp'
        cmd_res = run(command)

        if cmd_res.return_code == 0:
            return True
        else:
            return False


@task
def pptp_install(m_num=None, vr_name=None, target_file = None):
    """
    PPTPモジュールをインストールする

    :param str m_num:
    :param str vr_name:
    :param str target_file:
    """
    if not m_num and not vr_name and not target_file:
        loggeing.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers = _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
       logging.warning(yellow("No router found."))
       exit(1)

    link_local_ip = map(lambda d: d['linklocalip'], routers)

    if not link_local_ip:
        logging.warn('VR Not found.')
        exit(1)

    env.hosts = link_local_ip
    env.pptp_install_vr = _load_pptp_installed(PPTP_INSTALLED_VR_FILE)

    results = execute(_pptp_install_module, routers)
    _print_results(results)


@parallel(pool_size=10)
def _pptp_install_module(routers):
    """
    PPTPモジュールをインストールする

    :param str m_num:
    :param str vr_name:
    :param str target_file:
    """
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    if env.pptp_install_vr:
        if vr_name in env.pptp_install_vr:
            msg = get_msg('PPTP module already installed')
            logging.info(msg)
            res['status'] = 'OK'
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            return res

        """
        for val in env.pptp_install_vr:
        for val in env.pptp_install_vr:
            if val == vr_name:
                msg = get_msg('PPTP module already installed')
                logging.info(msg)
                res['status'] = 'OK'
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                return res
        """

    mod_list = ['ip_nat_pptp',
                'ip_conntrack_pptp']
    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        res['start_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        for mod in mod_list:
            cmd = 'modprobe %s' % mod
            cmd_result = run(cmd)
            if cmd_result.failed:
                msg = get_msg('Faied to run "%s"' % (cmd))
                logging.error(msg)
                res['msg'] = msg
                res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                return res

        cmd = 'grep -E "^nf_conntrack_pptp" /etc/modules > /dev/null || echo -E "nf_conntrack_pptp" >> /etc/modules ; grep -E "^nf_nat_pptp" /etc/modules > /dev/null || echo -e "nf_nat_pptp" >> /etc/modules'
        cmd_result = run(cmd)
        if cmd_result.failed:
            msg = get_msg('Faied to run "%s"' % (cmd))
            logging.error(msg)
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            return res

        msg = get_msg('PPTP module installation finished')
        logging.info(msg)
        res['status'] = 'OK'
        res['msg'] = msg
        res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        _add_pptp_installed(PPTP_INSTALLED_VR_FILE, vr_name)

    return res


def _load_target(target_file):
    """
    targetファイルをロードする

    :param str target_file: Targetファイルのパス
    :return list: 以下の値を含むdictを含むlist
        m_num:
        network_uuid:
    """
    logging.info('load target file. %s' % target_file)
    if not target_file:
        logging.error("argument target_file not set.")

    if not os.path.exists(target_file):
        logging.error("target file not found. %s" % target_file)
        return False

    targets = []
    try:
        f = open(target_file, "r")
        reader = csv.reader(f)
        for row in reader:
            if len(row) > 0:
                if row[0][0:1] == '#':
                    # 先頭行が "#" はコメントとみなす
                    continue

                targets.append({"m_num": row[0],
                                "network_uuid": row[1]})

        f.close()
    except Exception as e:
        print(red(e.message))
        logging.error('設定ファイルを開けません。%s' % target_file)
        return False

    return targets


def _add_pptp_installed(file_path, vr_name):
    """
    PPTPモジュールロード済みのVR一覧にVRを追加する

    :param str file_path: VR名を含むファイルパス
    """
    logging.info('update file. %s' % file_path)

    if not file_path:
        logging.error("argument file_path not set.")
        return False

    try:
        f = open(file_path, 'a+')
        f.write(vr_name + '\n')
        f.close()
    except Exception as e:
        print(red(e.message))
        logging.error('failed to update file. %s' % target_file)
        return False

    return True


def _load_pptp_installed(file_path):
    """
    PPTPモジュールロード済みのVR一覧を取得する

    :param str file_path: VR名を含むファイルパス
    """
    logging.info('load file. %s' % file_path)

    if not file_path:
        logging.error("argument file_path not set.")
        return False

    if not os.path.exists(file_path):
        logging.warn("file not found. %s" % file_path)
        return False

    data = None
    try:
        f = open(file_path, "r")
        data = f.readlines()

        f.close()
    except Exception as e:
        print(red(e.message))
        logging.error('failed to read file. %s' % target_file)
        return False

    if data:
        data = filter(lambda d: d[0:1] != '#', data)
        for i, val in enumerate(data):
            data[i] = val.strip()

    return data


def _get_routers(**kwargs):
    """
    仮想ルータ一覧を取得する。

    :param dict **kwargs: 引数に取るキーは以下。複数指定された場合は target_file が優先して使用される。
        m_num: M番号に紐づく仮想ルータを返す。
        vr_name: 仮想ルータ名から仮想ルータ情報を取得する。
        target_file: M番号、Network UUID一覧を含むファイル名。指定されたNetwork UUIDに紐づく仮想ルータ情報を取得する。
    """
    target_file = kwargs['target_file']
    m_num = kwargs['m_num']
    vr_name = kwargs['vr_name']

    routers = []
    if target_file:
        nw_uuid_list = _load_target(target_file)
        if not nw_uuid_list:
            return None

        for nw_uuid in nw_uuid_list:
            try:
                router = cs_vr.cs_get_routers(None, nw_uuid['network_uuid'], None)
                if router:
                    routers += router['router']
            except CloudStackException as e:
                logging.error(red('[%s:%s] %s' % (nw_uuid['m_num'],
                                                  nw_uuid['network_uuid'],
                                                  e.args[2].get('errortext', ""))))
    else:
        routers = cs_vr.cs_get_routers(m_num, None, vr_name).get('router', None)

    return routers


def _print_results(results):
    """
    結果をファイルに出力する

    :param list results: 結果を含むlist
      status
      start_time
      end_time
      msg
    """
    path = env.log_dir + "/pptp_install_results.log"
    with open(path, 'a') as f:
        f = codecs.lookup('utf_8')[-1](f)
        for k, v in results.items():
            msg = '%s [%s] %s \t%s' % (v.get('end_time', ''), k, v.get('status', 'Status Unknown'), v.get('msg', ''))
            f.write('%s\n' % msg.replace('\r', ''))


