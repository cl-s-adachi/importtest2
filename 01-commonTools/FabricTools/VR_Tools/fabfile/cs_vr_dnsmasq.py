# -*- coding: utf-8 -*-

from fabric.api import local, run, env
from fabric.api import env, settings
from fabric.utils import puts
from fabric.colors import *
from fabric.api import *
from fabric.decorators import task, runs_once, parallel


######################3
# Python standard Libary
import codecs
import csv
import functools
import logging
import json
import os.path
import re
import sys
import time
from datetime import *
from time import *

# KCPS
from common import cs_vr
from common import config
from common import util


@task
def dnsmasq_check_version(version, m_num = None, vr_name = None, target_file = None):
    """
    指定したVRのdnsmasqのバージョンをチェックする

    :param str vr_name: VR名
    :param str version: dnsmasqのバージョン。
    """
    if not m_num and not vr_name and not target_file:
        loggeing.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers =  _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    linklocalip_list = map(lambda d: d['linklocalip'], routers)

    env.hosts = linklocalip_list
    results = execute(_dnsmasq_check_version, version, routers)
    _print_results(results)


@parallel(pool_size=10)
def _dnsmasq_check_version(version, routers):
    """
    dnsmasqのバージョンをチェックする

    :param str version: dnsmasqのバージョン。
    :param list routers: rotuer情報を含むlist
    """
    vr = filter(lambda d: str(d['linklocalip']) == env.host_string, routers)[0]
    m_num = vr.get('m_num', vr.get('domain', None))
    vr_name = vr.get('vr_name', vr.get('name', None))
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    with settings(**params):
        res['start_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        cmd_res = run('dpkg -l | grep "dnsmasq"')
        if cmd_res.failed:
            msg = get_msg('failed to get dnsmasq version.')
            logging.error(red(msg))
            res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            return res

        version_map = {}
        for val in cmd_res.splitlines():
            parsed_val = val.split()
            version_map[parsed_val[1]] =  parsed_val[2]

        msg_list = ''
        status = True
        for k, v in version_map.items():
            if v == version:
                msg = get_msg('OK %s version: %s' % (k, v))
                logging.info(green(msg))
                msg_list += '\n\t' + msg
                status = status and True
            else:
                msg = get_msg('NG %s version: %s should be %s' % (k, v, version))
                logging.error(red(msg))
                msg_list += '\n\t' + msg
                status = status and False

        res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        res['status'] = 'OK' if status else 'NG'
        res['msg'] = msg_list
        return res


@task
def dnsmasq_show_version(m_num = None, vr_name = None, target_file = None):
    """
    dnsmasqのバージョンを表示する

    :param str m_num: M番号
    :param str vr_name:VR名 
    """
    if not m_num and not vr_name and not target_file:
        loggeing.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers =  _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    linklocalip_list = map(lambda d: d['linklocalip'], routers)

    env.hosts = linklocalip_list
    results = execute(_dnsmasq_show_version, routers)
    _print_results(results)


@parallel(pool_size=10)
def _dnsmasq_show_version(routers):
    """
    dnsmasqのバージョンを表示する

    :param list routers: VR情報
    """

    vr = filter(lambda d: str(d['linklocalip']) == env.host_string, routers)[0]
    m_num = vr.get('m_num', vr.get('domain', None))
    vr_name = vr.get('vr_name', vr.get('name', None))
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    with settings(**params):
        cmd_res = run('dpkg -l | grep "dnsmasq"')
        res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        if cmd_res.failed:
            msg = get_msg('failed to get dnsmasq version.')
            logging.error(msg)
            return res

        msg = get_msg(cmd_res)
        logging.info(green(msg))
        res['msg'] = msg
        res['status'] = 'OK'
        return res


@task
def dnsmasq_update(m_num=None, vr_name=None, target_file = None, version=None):
    """
    dnsmasqをアップデートする

    :param str m_num: M番号 
    :param str vr_name: VR名
    :param str version: バージョン
    """
    if not m_num and not vr_name and not target_file:
        loggeing.warn('Argument error: m_num or vr_name or target_file must be set.')
        exit(1)

    routers =  _get_routers(m_num = m_num, vr_name = vr_name, target_file = target_file)

    if not routers:
        logging.warning(yellow("No router found."))
        exit(1)

    linklocalip_list = map(lambda d: d['linklocalip'], routers)

    env.hosts = linklocalip_list
    pkg_list = [{'name': 'dnsmasq', 'version': version},
                {'name': 'dnsmasq-base', 'version': version},
                {'name': 'dnsmasq-utils', 'version': version}]
    results = execute(update_package, routers, pkg_list)
    _print_results(results)


def update_package(routers, pkg_list):
    """
    Packageを更新する

    :param list routers: VR情報を含むlist
    :param list pkg_list: 更新するパッケージとバージョンを含むdictを含むList。
      'name': パッケージ名
      'version': バージョン。バージョン指定無しの場合は設定しない。
    """
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['vr_name']
    m_num = router['m_num']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    pkg_names = ''
    for pkg in pkg_list:
        name = pkg['name']
        version = pkg.get('version', None)
        if version:
            name = '%s=%s' % (name, version)

        pkg_names = pkg_names + ' ' + name

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    with settings(**params):
        res['start_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        cmd_result = run('apt-get update > /dev/null')
        if cmd_result.failed:
            logging.warn(get_msg('Faied to run "apt-get update"'))

        command = 'apt-get install -y %s' % (pkg_names)
        cmd_result = run(command)
        msg = get_msg('Faied to run "%s"' % (command))
        if cmd_result.failed:
            msg = get_msg('Faied to run "%s"' % (command))
            logging.error(msg)
            res['msg'] = msg
            res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            return res

        msg = get_msg('dnsmasq update finished.')
        res['status'] = 'OK'
        res['msg'] = msg
        res['end_time'] = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        return res


def _get_routers(**kwargs):
    """
    仮想ルータ一覧を取得する。

    :param dict **kwargs: 引数に取るキーは以下。複数指定された場合は target_file が優先して使用される。
        m_num: M番号に紐づく仮想ルータを返す。
        vr_name: 仮想ルータ名から仮想ルータ情報を取得する。
        target_file: M番号、Network UUID一覧を含むファイル名。指定されたNetwork UUIDに紐づく仮想ルータ情報を取得する。
    """
    target_file = kwargs['target_file']
    m_num = kwargs['m_num']
    vr_name = kwargs['vr_name']

    routers = []
    if target_file:
        routers = _load_target(target_file)
    else:
        routers = cs_vr.cs_get_routers(m_num, None, vr_name).get('router', None)

    return routers


def _print_results(results):
    """
    結果をファイルに出力する

    :param str start_time: 開始時間
    :param str end_time: 終了時間
    :param str m_num: M番号
    :param list results: 結果を含むlist
    """
    path = env.log_dir + "/dnsmasq_update_results.log"
    with open(path, 'a') as f:
        f = codecs.lookup('utf_8')[-1](f)
        for k, v in results.items():
            msg = '%s [%s] %s \t%s' % (v.get('end_time', ''), k, v['status'], v['msg'])
            f.write('%s\n' % msg.replace('\r', ''))


def _load_target(target_file):
    """
    targetファイルをロードする

    :param str target_file: Targetファイルのパス
    :return list: 以下の値を含むdictを含むlist
        vr_name:
        linklocalip:
        m_num:
    """
    logging.info('load target file. %s' % target_file)
    if not target_file:
        logging.error("argument target_file not set.")

    if not os.path.exists(target_file):
        logging.error("target_file not found. %s" + target_file)
        return False

    targets = []
    try:
        f = open(target_file, "r")
        reader = csv.reader(f)
        for row in reader:
            if len(row) > 0:
                if row[0][0:1] == '#':
                    # 先頭行が "#" はコメントとみなす
                    continue

                targets.append({'vr_name': row[0]})

        f.close()
    except Exception as e:
        print(red(e.message))
        logging.error('設定ファイルを開けません。%s' % target_file)
        return False

    for vr in targets:
        router = cs_vr.cs_get_router(vr['vr_name']).get('router', None)

        if not router:
            logging.warning(yellow('[%s] Link Local IP not found.' % vr['vr_name']))
            targets.remove(vr)
            continue

        if len(router) == 1:
            vr['linklocalip'] = str(router[0]['linklocalip'])
            vr['m_num'] = str(router[0]['domain'])
        elif len(router) > 1:
            logging.warning(yellow('[%s] More than one VR found.' % vr['vr_name']))
            targets.remove(vr)

    return targets

