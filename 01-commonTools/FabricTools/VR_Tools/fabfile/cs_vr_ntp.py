# -*- coding: utf-8 -*-

from fabric.api import local, run, env, settings
#from fabric.utils import puts
from fabric.colors import *
from fabric.api import *
from fabric.decorators import task, runs_once


######################3
# Python standard Libary
import codecs
import csv
import functools
import logging
import json
import os.path
import re
import sys
import time
from datetime import *
from time import *


# Exoscale cs
from cs import CloudStackException

# KCPS
#import common
#from common.cs_vr import *
from common import cs_vr
from common import config
from common import util


@task
def ntp_stop(target = "All"):
    
    if target == "All":
        router_info = _get_all_routers_from_db()
    elif target == "Isolate":
        router_info = _get_isolate_routers_from_db()
    elif target == "Shared":
        router_info = _get_shared_routers_from_db()
    elif target[0:2] == "r-":
        router_info = _get_router_from_db(target)

    if not router_info:
        logging.warning(yellow("No router found."))
        exit(1)

    routers_running = router_info.get('router_running')
    env.hosts = router_info.get('linklocalip_list')

    results = execute(_exec_command, routers_running, "service ntp stop")

@parallel(pool_size=10)
def _exec_command(routers, command):
    """
    引数で実行したコマンドを実行する
    """
    logging.info('_exec_command') 
    router = filter(lambda d: d['linklocalip'] == env.host_string, routers)[0]
    vr_name = router['name']
    m_num = router['domain']
    res = {'status': 'NG', 'msg': '', 'vr_name': vr_name, 'm_num': m_num}
    get_msg = functools.partial(util.get_formated_message, m_num, vr_name)

    res['start_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    params = config.getConnectEnvGW(env.host_string, env.vr, env.sshGW)
    params['warn_only'] = True
    with settings(**params):
        cmd_res = run(command)
        if cmd_res.succeeded:
            msg = get_msg('"%s" succeeded.' % (command))
            res['status'] = 'OK'
            res['msg'] = msg
            logging.info(green(msg))
        else:
            msg = get_msg('"%s" failed.' % (command))
            res['msg'] = msg
            logging.error(red(msg))

    res['end_time'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    return res


    return flag

def dump_vr_from_db():
    """
    操作対象となるVRをDBから取得し表示する。
    """
    data = _get_target_routers_from_db()

    for d in data['router_running']:
       print('%s\t%s\t%s\t%s' % (d['name'], d['linklocalip'],d['domain'],d['state']))

    for d in data['router_stopped']:
       print('%s\t%s\t%s\t%s' % (d['name'], d['linklocalip'],d['domain'],d['state']))


def _get_all_routers_from_db():
    """
    全てのルータ情報をCSDBから取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    sql = "SELECT vm.name, vm.private_ip_address, d.name, vm.state" \
          " FROM nics AS ni" \
          " LEFT JOIN vm_instance AS vm ON ni.instance_id=vm.id" \
          " LEFT JOIN networks AS net ON ni.network_id=net.id" \
          " LEFT JOIN domain AS d ON vm.domain_id=d.id" \
          " LEFT JOIN host AS h ON vm.host_id=h.id" \
          " LEFT JOIN host AS hl ON vm.last_host_id=hl.id" \
          " WHERE net.removed IS NULL AND vm.removed IS NULL AND vm.type LIKE \"DomainRouter\" AND net.guest_type IS NOT NULL" \
          " ORDER BY vm.created;"
      
    queryResults = local(util.create_mysql_command(sql), capture=True)

    router_info = {}
    ids = []
    if queryResults:
        for result in queryResults.split('\n'):
            resultList = result.split('\t')
            ids.append({ 'name':     resultList[0],
                         'linklocalip': resultList[1],
                         'domain':       resultList[2],
                         'state':       resultList[3]})
    # linklocalipが "169.254" で始まるものはKVMのVRで古くもう存在しないが
    # CSDB上にゴミとして残っているので結果に含めない
    router_info['router_running'] = filter(lambda d: d['state'] == "Running" and not d['linklocalip'].startswith('169.254.'), ids)
    router_info['router_stopped'] = filter(lambda d: d['state'] == "Stopped", ids)
    router_info['linklocalip_list'] = map(lambda d: d['linklocalip'], router_info['router_running'])

    return router_info

def _get_isolate_routers_from_db():
    """
    IsolateNetworkのルータ情報をCSDBから取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    sql = "SELECT vm.name, vm.private_ip_address, d.name, vm.state" \
          " FROM nics AS ni" \
          " LEFT JOIN vm_instance AS vm ON ni.instance_id=vm.id" \
          " LEFT JOIN networks AS net ON ni.network_id=net.id" \
          " LEFT JOIN domain AS d ON vm.domain_id=d.id" \
          " LEFT JOIN host AS h ON vm.host_id=h.id" \
          " LEFT JOIN host AS hl ON vm.last_host_id=hl.id" \
          " WHERE net.removed IS NULL AND vm.removed IS NULL AND vm.type LIKE \"DomainRouter\" AND net.guest_type LIKE \"Isolated\"" \
          " ORDER BY vm.created;"
      
    queryResults = local(util.create_mysql_command(sql), capture=True)

    router_info = {}
    ids = []
    if queryResults:
        for result in queryResults.split('\n'):
            resultList = result.split('\t')
            ids.append({ 'name':     resultList[0],
                         'linklocalip': resultList[1],
                         'domain':       resultList[2],
                         'state':       resultList[3]})
    # linklocalipが "169.254" で始まるものはKVMのVRで古くもう存在しないが
    # CSDB上にゴミとして残っているので結果に含めない
    router_info['router_running'] = filter(lambda d: d['state'] == "Running" and not d['linklocalip'].startswith('169.254.'), ids)
    router_info['router_stopped'] = filter(lambda d: d['state'] == "Stopped", ids)
    router_info['linklocalip_list'] = map(lambda d: d['linklocalip'], router_info['router_running'])

    return router_info

def _get_shared_routers_from_db():
    """
    SharedNetworkのルータ情報をCSDBから取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    sql = "SELECT vm.name, vm.private_ip_address, d.name, vm.state" \
          " FROM nics AS ni" \
          " LEFT JOIN vm_instance AS vm ON ni.instance_id=vm.id" \
          " LEFT JOIN networks AS net ON ni.network_id=net.id" \
          " LEFT JOIN domain AS d ON vm.domain_id=d.id" \
          " LEFT JOIN host AS h ON vm.host_id=h.id" \
          " LEFT JOIN host AS hl ON vm.last_host_id=hl.id" \
          " WHERE net.removed IS NULL AND vm.removed IS NULL AND vm.type LIKE \"DomainRouter\" AND net.guest_type LIKE \"Shared\"" \
          " ORDER BY vm.created;"
      
    queryResults = local(util.create_mysql_command(sql), capture=True)

    router_info = {}
    ids = []
    if queryResults:
        for result in queryResults.split('\n'):
            resultList = result.split('\t')
            ids.append({ 'name':     resultList[0],
                         'linklocalip': resultList[1],
                         'domain':       resultList[2],
                         'state':       resultList[3]})
    # linklocalipが "169.254" で始まるものはKVMのVRで古くもう存在しないが
    # CSDB上にゴミとして残っているので結果に含めない
    router_info['router_running'] = filter(lambda d: d['state'] == "Running" and not d['linklocalip'].startswith('169.254.'), ids)
    router_info['router_stopped'] = filter(lambda d: d['state'] == "Stopped", ids)
    router_info['linklocalip_list'] = map(lambda d: d['linklocalip'], router_info['router_running'])

    return router_info

def _get_router_from_db(router_name):
    """
    仮想ルータ名からルータ情報をCSDBから取得する。
    起動中と停止中のVRを別のlistに追加し、起動中のVRのLinked Local IPを返す。

    :param list routers: 設定ファイルから読み込んだRouter情報
    :param list results: 結果(dict)を含むlist。dictのキーは以下
      routers_stopped
      routers_running
      linklocalip_list
    """
    sql = "SELECT vm.name, vm.private_ip_address, d.name, vm.state" \
          " FROM nics AS ni" \
          " LEFT JOIN vm_instance AS vm ON ni.instance_id=vm.id" \
          " LEFT JOIN networks AS net ON ni.network_id=net.id" \
          " LEFT JOIN domain AS d ON vm.domain_id=d.id" \
          " LEFT JOIN host AS h ON vm.host_id=h.id" \
          " LEFT JOIN host AS hl ON vm.last_host_id=hl.id" \
          " WHERE net.removed IS NULL AND vm.removed IS NULL AND vm.type LIKE \"DomainRouter\" AND vm.name LIKE \"" + router_name + "\""  \
          " ORDER BY vm.created;"

    queryResults = local(util.create_mysql_command(sql), capture=True)

    router_info = {}
    ids = []
    if queryResults:
        for result in queryResults.split('\n'):
            resultList = result.split('\t')
            ids.append({ 'name':     resultList[0],
                         'linklocalip': resultList[1],
                         'domain':       resultList[2],
                         'state':       resultList[3]})
    # linklocalipが "169.254" で始まるものはKVMのVRで古くもう存在しないが
    # CSDB上にゴミとして残っているので結果に含めない
    router_info['router_running'] = filter(lambda d: d['state'] == "Running" and not d['linklocalip'].startswith('169.254.'), ids)
    router_info['router_stopped'] = filter(lambda d: d['state'] == "Stopped", ids)
    router_info['linklocalip_list'] = map(lambda d: d['linklocalip'], router_info['router_running'])

    return router_info
