# ACS4.9対応 VRツール

## PPTPモジュールインストール

### pptp_check_install

PPTPモジュールがインストールされているかチェックする。

```
$ fab set_env:config=[環境設定ファイル] pptp_check_install:target_file=[ターゲットファイル]
```

* [環境設定ファイル]: 設定ファイル
* [ターゲットファイル]: M番号とNetwork UUIDを含むcsvファイル

        M00000001,861c0e4a-6a61-4be1-83b1-ecdf5dcd0be2

* 実行例

        $ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
        $ fab set_env:config=conf/production.yml pptp_check_install:target_file=target/pptp_production.txt


### pptp_install

PPTPモジュールをインストールする。

```
$ fab set_env:config=[環境設定ファイル] pptp_install:target_file=[ターゲットファイル]
```

* [環境設定ファイル]: 設定ファイル
* [ターゲットファイル]: M番号とNetwork UUIDを含むcsvファイル
* 実行例

        $ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
        $ fab set_env:config=conf/production.yml pptp_install:target_file=target/pptp_production.txt



## dnsmasq設定ファイルアップデート


### dhcp_exec_all

以下のファイルをVRにコピーしスクリプトを実行する。

* /root/modVRdhcp.sh
* /root/checkVRdhcp.sh
* /opt/cloud/bin/cs/CsDhcp.py
* **実行方法**

        $ fab -w set_env:config=[環境設定ファイル] dhcp_exec_all:target_file=[ターゲットファイル]

    * **環境設定ファイル:** 環境毎の設定ファイル
    * **ターゲットファイ:** 対象のVR名一覧を含むファイル
    * ファイルの内容の例

                r-7379-VM
                r-7810-VM
                r-10249-VM
                r-10522-VM
                r-10523-VM

* **実行例**

        $ fab -w set_env:config=conf/production.yml dhcp_exec_all:target_file=target/dhcp_production.txt


### dump_vr_from_db

VRの一覧をDBから取得し表示する。
**VR名, LinkLocalIP, Domain, VM State** が出力される。

* **実行方法**


        $ fab -w set_env:config=[環境設定ファイル] dump_vr_from_db

    * **環境設定ファイル:** 環境毎の設定ファイル

* **実行例**

                $ fab  set_env:config=conf/production.yml dump_vr_from_db

    * **出力結果の例**


                r-7379-VM       198.19.5.50     ROOT    Running
                r-7810-VM       198.19.5.102    ROOT    Running
                r-10499-VM      198.19.5.41     ROOT    Running
                r-10959-VM      198.19.5.83     ROOT    Running
                r-10960-VM      198.19.5.182    ROOT    Running
                r-11146-VM      198.19.5.31     ROOT    Running
                r-11223-VM      198.19.5.116    M00000002       Running
                r-11227-VM      198.19.5.167    M00000001       Running
                r-11275-VM      198.19.5.194    ROOT    Running
                r-10249-VM      198.19.5.110    M99993333       Stopped



### get_system_log_files

VRから **/var/log/dnsmasq.log** をコピーする。
コピーされたファイルはカレントディレクトリ内の以下に保存される。

* **backup_vr_system_log/YYYYMMDD_hhmm/<LINK_LOCAL_IP>/**
* **実行方法**

        $ fab -w set_env:config=[環境設定ファイル] get_system_log_files:target_file=target/dhcp_production.txt

    * **環境設定ファイル:** 環境毎の設定ファイル
    * **ターゲットファイル:** 対象のVR名一覧を含むファイル


#### 対象VR一覧を作成しdnsmasq.logを取得する方法

**商用環境での実行手順**

1. **課金バッチサーバ** にログイン
1. **ckk_opeユーザ** になりディレクトリ移動
 
        $ su - 
        # su - ckk_ope
        $ cd /home/ckk_ope/scripts/CL_Tools/FabricTools/VR_Tools/

1. **dump_vr_from_db** を実行し最新のVR一覧を **target/dhcp/production_vr_list.txt** として保存

        $ mkdir target/dhcp/
        $ fab set_env:config=conf/production.yml dump_vr_from_db | awk {'print $1'} | grep 'VM' > target/dhcp/production_vr_list.txt

1. **get_system_log_files** を実行

        $ fab set_env:config=conf/production.yml get_system_log_files:target_file=target/dhcp/production_vr_list.txt

1. 上記を実行すると以下のディレクトリが作成され、各VRのdnsmasq.logが保存される(ディレクトリ名はVRのLink Local IP)
    * **backup_vr_system_log/YYYYMMDD_hhmm/<Link_Local_IP>/dnsmasq.log**


### dnsmasq

**/etc/init.d/dnsmasq** を実行する。

* **実行方法**

        $ fab -w set_env:config=[環境設定ファイル] dnsmasq:target_file=[ターゲットファイル],cmd_param=<コマンド引数>

    * **環境設定ファイル:** 環境毎の設定ファイル
    * **コマンド引数:** **/etc/init.d/dnsmasq** への引数(e.g. **start stop restart status**)
    * **ターゲットファイル:** 対象のVR名一覧を含むファイル

* **実行例**

        $ fab -w set_env:config=conf/production.yml dnsmasq:cmd_param=status,target_file=target/dhcp_production.txt



## dnsmasqアップデート


### dnsmasq_update

Dnsmasqをアップデートする。

* **実行方法**

        $ fab set_env:config=[環境設定ファイル] dnsmasq_update:target_file=[ターゲットファイル]

    * **環境設定ファイル]:** 設定ファイル
    * **ターゲットファイル:** VR名を含むcsvファイル

                r-11200-VM
                r-11227-VM

* 実行例

        $ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
        $ fab set_env:config=conf/production.yml dnsmasq_update:target_file=target/dnsmasq_production.txt



