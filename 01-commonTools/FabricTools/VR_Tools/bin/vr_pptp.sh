#!/bin/bash
#
# Script executed by cron.
# This script install PPTP modules in Virtual Rotuer.
#
VR_TOOL_PATH="/home/ckk_ope/scripts/CLTool/FabricTools/VR_Tools"
export PYTHONPATH="${VR_TOOL_PATH}/../"

cd ${VR_TOOL_PATH}
/usr/local/bin/fab set_env:config=${VR_TOOL_PATH}/conf/production.yml pptp_install:target_file=${VR_TOOL_PATH}/target/pptp_production.txt
