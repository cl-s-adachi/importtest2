#!/bin/bash
#
# Script executed by cron.
# This script check/install Dnsmasql related files in Virtual Rotuer.
#
VR_TOOL_PATH="/home/ckk_ope/scripts/CLTool/FabricTools/VR_Tools"
export PYTHONPATH="${VR_TOOL_PATH}/../"

cd ${VR_TOOL_PATH}
#/usr/local/bin/fab -w set_env:config=${VR_TOOL_PATH}/conf/production.yml dhcp_check_lease_file
/usr/local/bin/fab -w set_env:config=${VR_TOOL_PATH}/conf/production.yml dhcp_check_lease_file:install_flag=True
