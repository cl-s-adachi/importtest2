#!/bin/bash
#
# Script executed by cron.
# This script check/install Dnsmasql related files in Virtual Rotuer.
#
VR_TOOL_PATH="/home/ckk_ope/scripts/CLTool/FabricTools/VR_Tools"
#VR_TOOL_CONF=${VR_TOOL_PATH}/conf/staging.yml
VR_TOOL_CONF=${VR_TOOL_PATH}/conf/production.yml
export PYTHONPATH="${VR_TOOL_PATH}/../"

cd ${VR_TOOL_PATH}
/usr/local/bin/fab set_env:config=${VR_TOOL_CONF} ntp_stop
#/usr/local/bin/fab set_env:config=${VR_TOOL_CONF} ntp_stop:target=Isolate
#/usr/local/bin/fab set_env:config=${VR_TOOL_CONF} ntp_stop:target=Shared
