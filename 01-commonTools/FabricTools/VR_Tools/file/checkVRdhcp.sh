#!/bin/bash

grep "876000h" /etc/dhcphosts.txt > /dev/null
if [ $? -ne 0 ] ;
then 
  echo "dhcphosts.txt NG"
fi

hostscnt=`cat /etc/dhcphosts.txt | wc -l`
dnsmasqcnt=`cat /var/lib/misc/dnsmasq.leases | wc -l`

if [ ${hostscnt} -ne ${dnsmasqcnt} ] ; then 
  echo "dnsmasq.leases NG"
fi


