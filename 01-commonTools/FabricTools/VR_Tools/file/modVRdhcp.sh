#!/bin/bash

# make bakcup directory
BKDIR="/root/dhcpbackup"

#if [ -e ${BKDIR} ] ; then
#   exit 0
#fi

mkdir -p ${BKDIR}

# dhcphosts
cp -p /etc/dhcphosts.txt ${BKDIR}/dhcphosts.txt.bk
cp -p /etc/dhcphosts.txt ${BKDIR}/dhcphosts.txt.bk_`date +%Y%m%d_%H%m`
#awk -F, '{print $1 "," $2 "," $3 "," "876000h"}' ${BKDIR}/dhcphosts.txt.bk > /etc/dhcphosts.txt
awk -F, '{if($2~/^set/){print $1 "," $2 "," $3 "," $4 "," "876000h"}else{print $1 "," $2 "," $3 "," "876000h"}}' ${BKDIR}/dhcphosts.txt.bk > /etc/dhcphosts.txt

# leases file
cp -p /var/lib/misc/dnsmasq.leases ${BKDIR}/dnsmasq.leases.bk
cp -p /var/lib/misc/dnsmasq.leases ${BKDIR}/dnsmasq.leases.bk_`date +%Y%m%d_%H%m`
TIMESTAMP=`date --date "100 years" "+%s"`
#awk -F, '{print '"${TIMESTAMP}"',$1,$2,$3,"*"}' /etc/dhcphosts.txt > /var/lib/misc/dnsmasq.leases
awk -F, '{if($2~/^set/){print '"${TIMESTAMP}"',$1,$3,$4,"*"}else{print '"${TIMESTAMP}"',$1,$2,$3,"*"}}' /etc/dhcphosts.txt > /var/lib/misc/dnsmasq.leases

# service restart
/etc/init.d/dnsmasq restart

