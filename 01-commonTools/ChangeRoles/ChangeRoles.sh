#!/bin/bash

IP=ckktky4-pcldb10
KICK="/home/ckkcl/scripts"
USER=`mysql -u root cloud -h ${IP} -Nse "select uuid from roles where name='User';"`
DOMAIN=`mysql -u root cloud -h ${IP} -Nse "select uuid from roles where name='Domain Admin';"`
RESOURCE=`mysql -u root cloud -h ${IP} -Nse "select uuid from roles where name='Resource Admin';"`

API_USER="createTags
deleteTags
listAutoScaleVmGroups
listTags
updatePortForwardingRule"

API_DOMAIN="addIpToNic
addNicToVirtualMachine
createTags
createVMSnapshot
deleteTags
deleteVMSnapshot
listAutoScaleVmGroups
listNics
listTags
listVMSnapshot
removeIpFromNic
removeNicFromVirtualMachine
resizeVolume
revertToVMSnapshot
updatePortForwardingRule"

API_RESOURCE="addIpToNic
addNicToVirtualMachine
createTags
createVMSnapshot
deleteTags
deleteVMSnapshot
listAutoScaleVmGroups
listNics
listTags
listVMSnapshot
removeIpFromNic
removeNicFromVirtualMachine
resizeVolume
revertToVMSnapshot
updatePortForwardingRule"

DELETE_API="extractVolume
uploadVolume"


for i in `echo "$API_USER"`
do
	$KICK/kick_api.sh \
	command=createRolePermission \
	rule=$i \
	permission=allow \
	roleid=$USER 2>/dev/null | egrep "<createrolepermissionresponse cloud-stack-version=\"4.9.1.0\">|<rule>" >> ./check_add_role.txt
done

for i in `echo "$API_DOMAIN"`
do
	$KICK/kick_api.sh \
	command=createRolePermission \
	rule=$i \
	permission=allow \
	roleid=$DOMAIN 2>/dev/null | egrep "<createrolepermissionresponse cloud-stack-version=\"4.9.1.0\">|<rule>" >> ./check_add_role.txt
done

for i in `echo "$API_RESOURCE"`
do
	$KICK/kick_api.sh \
	command=createRolePermission \
	rule=$i \
	permission=allow \
	roleid=$RESOURCE 2>/dev/null | egrep "<createrolepermissionresponse cloud-stack-version=\"4.9.1.0\">|<rule>" >> ./check_add_role.txt
done

for i in `echo "$DELETE_API"`
do
	DELETE_RESOURCE=`mysql -u root cloud -h ${IP} -Nse "select uuid from role_permissions where rule='$i' and role_id=2;"`
	$KICK/kick_api.sh \
	command=deleteRolePermission \
	id=$DELETE_RESOURCE 2>/dev/null | egrep "<deleterolepermissionresponse cloud-stack-version=\"4.9.1.0\">|true" >> ./check_del_role.txt
done

for i in `echo "$DELETE_API"`
do
	DELETE_DOMAIN=`mysql -u root cloud -h ${IP} -Nse "select uuid from role_permissions where rule='$i' and role_id=3;"`
	$KICK/kick_api.sh \
	command=deleteRolePermission \
	id=$DELETE_DOMAIN 2>/dev/null | egrep "<deleterolepermissionresponse cloud-stack-version=\"4.9.1.0\">|true" >> ./check_del_role.txt
done

