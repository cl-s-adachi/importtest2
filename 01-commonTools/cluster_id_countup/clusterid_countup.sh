#!/bin/bash
# Copyright(c) 2013 CREATIONLINE,INC. All Right Reserved.

export LANG=C

#
# check config file path and set environment values
#
cs_script_conf_path=$(dirname $0)/cs-script.conf
if [ -f "$cs_script_conf_path" ] && [ -r "$cs_script_conf_path" ]
then
  . "$cs_script_conf_path"
else
  echo -e ""
  echo -e "configuration error: $0 requires $cs_script_conf_path at the same directory"
  echo -e ""
  exit 1
fi


function check_write_permission()
{
  if [ -f "${1}" ]
  then
    if [ -w "${1}" ]
    then
      return 0
    else
      return 1
    fi
  else
    touch "${1}" 2>/dev/null
    return $?
  fi
}

function exit_proc()
{
  if [ -n "$res_temp" ] && [ -f "$res_temp" ]
  then
    rm -f $res_temp
  fi
  if [ -n $1 ]
  then
    exit $1
  else
    exit
  fi
}

#
# check script parameter
#
if [ "$#" == 3 ]
then
  cluster_name=$1
  cluster_podname=$2
  cluster_zonename=$3
else
  echo -e ""
  echo -e "this script repeat addCluster and deleteCluster with specified parameters to count up cloud.cluster.id"
  echo -e "usage: $0 <dummy cluster name> <existed pod name> <existed zone name>"
  echo -e ""
  exit_proc
fi

#
# check write permisson of temporary file
#
tempfile_base=$(basename $0)
tempfile_base=${tempfile_base%.sh}
res_temp=res_command_$tempfile_base    # temporary file cloudstack response to parse

check_write_permission $res_temp
if [[ "$?" != 0 ]]
then
  echo -e ""
  echo -e "cannot create temporary file this script uses."
  echo -e "  please check the current directory permission"
  echo -e ""
  exit_proc
fi

#
# check database connection information
#
if [ -z "$db_server" ] || [ -z "$db_user" ]
then
  echo -e ""
  echo -e "not found database connection information"
  echo -e "  exit this script. bye..."
  echo -e ""
  exit 1
fi

if [ -n "$db_passwd" ]
then
  connect_str="-h $db_server -u $db_user -p${db_passwd}"
else
  connect_str="-h $db_server -u $db_user"
fi

mysql $connect_str cloud -e "select 1" > /dev/null 2>&1
if [[ "$?" != 0 ]]
then
  echo -e ""
  echo -e "cannot connect cloud database"
  echo -e "  please check database connection information of $cs_script_conf_path"
  echo -e "  exit this script. bye..."
  echo -e ""
  exit 1
fi



#
# main
#
execute_command command=listZones \
  listall=true \
  name=$cluster_zonename > $res_temp

errcode=$(pickup_entity errorcode < $res_temp)
errtext=$(pickup_entity errortext < $res_temp)
if [ -n "$errcode" ] || [ -n "$errtext" ]
then
  echo -e ""
  echo -e "failed to list Zone"
  echo -e "  error code[$errcode]"
  echo -e "  error text[$errtext]"
  echo -e ""
  exit_proc
fi
cnt=$(pickup_entity count < $res_temp)

if [ -z "$cnt" ] || [ $cnt -eq 0 ]
then
  echo -e ""
  echo -e "not found input zone name: ${3}"
  echo -e "  exit this script. bye..."
  echo -e ""
  exit_proc
fi
cluster_zonename=$(pickup_entity id < $res_temp)
cluster_zoneid=$(pickup_entity id < $res_temp)

execute_command command=listPods \
  listall=true \
  name=$cluster_podname \
  zoneid=$cluster_zoneid> $res_temp

errcode=$(pickup_entity errorcode < $res_temp)
errtext=$(pickup_entity errortext < $res_temp)
if [ -n "$errcode" ] || [ -n "$errtext" ]
then
  echo -e ""
  echo -e "failed to list Pod"
  echo -e "  error code[$errcode]"
  echo -e "  error text[$errtext]"
  echo -e ""
  exit_proc
fi
cnt=$(pickup_entity count < $res_temp)

if [ -z "$cnt" ] || [ $cnt -eq 0 ]
then
  echo -e ""
  echo -e "not found input pod name: ${3}"
  echo -e "  exit this script. bye..."
  echo -e ""
fi
cluster_podname=$(pickup_entity name < $res_temp)
cluster_podid=$(pickup_entity id < $res_temp)


execute_command command=listClusters \
  listall=true \
  name=$cluster_name > $res_temp

errcode=$(pickup_entity errorcode < $res_temp)
errtext=$(pickup_entity errortext < $res_temp)
if [ -n "$errcode" ] || [ -n "$errtext" ]
then
  echo -e ""
  echo -e "failed to list Cluster"
  echo -e "  error code[$errcode]"
  echo -e "  error text[$errtext]"
  echo -e ""
  exit_proc
fi

cnt=$(pickup_entity count < $res_temp)

if [ -n "$cnt" ] && [ $cnt -ne 0 ]
then
  echo -e ""
  echo -e "already exists input Cluster name: ${1}"
  echo -e "  exit this script. bye..."
  echo -e ""
  exit_proc
fi

current_id=$(mysql --silent --skip-column-names $connect_str cloud \
  -e "select max(id) from cluster")

if [ $current_id -gt 127 ]
then
  echo -e ""
  echo -e "already reached cluster id to $increment_id"
  exit_proc
fi
echo -e ""
echo -e "now cloud.cluster.id = $current_id"
echo -e ""


#
# output api parameters for addCluster
#
cluster_hypervisor="KVM"
cluster_clustertype="CloudManaged"

echo -e "add Cluster $name information"
echo -e "  name          = $cluster_name"
echo -e "  hypervisor    = $cluster_hypervisor"
echo -e "  cluster type  = $cluster_clustertype"
echo -e "  pod name      = $cluster_podname"
echo -e "  pod id        = $cluster_podid"
echo -e "  zone name     = $cluster_zonename"
echo -e "  zone id       = $cluster_zoneid"

echo -e ""
echo -e "start increment of cluster id"

for i in $(seq $current_id 126)
do
  
  execute_command command=addCluster \
    clustername=$cluster_name \
    clustertype=$cluster_clustertype \
    hypervisor=$cluster_hypervisor \
    podid=$cluster_podid \
    zoneid=$cluster_zoneid \
    > $res_temp

  errcode=$(pickup_entity errorcode < $res_temp)
  errtext=$(pickup_entity errortext < $res_temp)
  if [ -n "$errcode" ] || [ -n "$errtext" ]
  then
    echo -e ""
    echo -e "failed to add Cluster"
    echo -e "  error code[$errcode]"
    echo -e "  error text[$errtext]"
    echo -e ""
    exit_proc
  fi

  delete_cluster_id=$(pickup_entity id < $res_temp)

  execute_command command=deleteCluster \
    id=$delete_cluster_id \
    > $res_temp

  errcode=$(pickup_entity errorcode < $res_temp)
  errtext=$(pickup_entity errortext < $res_temp)
  if [ -n "$errcode" ] || [ -n "$errtext" ]
  then
    echo -e ""
    echo -e "failed to delete Cluster"
    echo -e "  error code[$errcode]"
    echo -e "  error text[$errtext]"
    echo -e ""
    exit_proc
  fi

  print_id=$(mysql --silent --skip-column-names $connect_str cloud \
    -e "select max(id) from cluster")
  echo -ne " $print_id, "

done

current_id=$(mysql --silent --skip-column-names $connect_str cloud \
  -e "select max(id) from cluster")

if [ $current_id -ge 127 ]
then
  echo -e ""
  echo -e "finished cluster id increment"
  echo -e "now cloud.cluster.id = $current_id"
  echo -e ""
else
  echo -e ""
  echo -e "failed cluster id increment"
  echo -e "now cloud.cluster.id = $current_id"
  echo -e ""
fi

exit_proc
