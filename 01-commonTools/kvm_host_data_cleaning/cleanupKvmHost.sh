#!/bin/bash


SLEEP_TIME=10
DELIMITER=","
DELETE_BRIDGE_FILE="delete_bridge.txt"
DELETE_BRIDGE_PATH="/root/"
PRIMARY_DIR=("/ckkejo2-vnx5700_01/ckkejo2-pEbsv01Fast01" "/ckkejo2-vnx5700_01/ckkejo2-pEbsv01Middle01" "/ckkejo2-vnx5700_01/ckkejo2-pEbsv02Fast01" "/ckkejo2-vnx5700_01/ckkejo2-pEbsv02Middle01")


#check
if [ $# -eq 1 ]; then
  if [ \( $1 -ge 1 -a $1 -le 16 \) -o $1 -eq 99 ]; then
      echo "HostCreanup.sh START"
      /bin/date
      /bin/uname -n
  else
      echo "Please specify only the value of 1 to 14 arguments."
      exit 1
  fi
else
  echo "Please specify only the value of 1 to 14 arguments."
  exit 1
fi

#---------------------------------------------------------------------------
# functions START


function CheckVirshList(){
  # 01.check virsh list
  echo ""
  echo "01.check virsh list"
  /usr/bin/virsh list --all
}


function DeleteVirsh(){
  # 02.delete virsh
  echo ""
  echo "02.delete virsh"
  virshs=(`virsh list --all | awk '{ print $2","$3; }'`)
  for ((virsh = 2; virsh+1 < ${#virshs[*]}; virsh++))
  do
    virsh_name=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $1 }'`)
    virsh_state=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $2 }'`)
    if test $virsh_state = "running"; then
        echo "virsh destroy" $virsh_name
#       /usr/bin/virsh destroy $virsh_name
    fi
  done
}


function CheckVirshSnapshot(){
  # 03.check virsh snapshot
  echo ""
  echo "03.check virsh snapshot"
    virshs=(`virsh list --all | awk '{ print $2","$3; }'`)
  for ((virsh = 2; virsh+1 < ${#virshs[*]}; virsh++))
  do
    virsh_name=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $1 }'`)
    echo "virsh snapshot-list" $virsh_name
    /usr/bin/virsh snapshot-list $virsh_name
  done
}


function DeleteVirshSnapshot(){
  # 04.delete virsh snapshot
  echo ""
  echo "04.delete virsh snapshot"
    virshs=(`virsh list --all | awk '{ print $2","$3; }'`)
  for ((virsh = 2; virsh+1 < ${#virshs[*]}; virsh++))
  do
    virsh_name=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $1 }'`)
    snapshots=(`virsh snapshot-list $virsh_name | awk '{ print $1 }'`)
    for ((num = 2; num < ${#snapshots[*]}; num++))
    do
        echo "virsh snapshot-delete" $virsh_name ${snapshots[$num]}
#        /usr/bin/virsh snapshot-delete $virsh_name ${snapshots[$num]}
    done
  done
}


function UndefineVirsh(){
  # 05.undefine virsh
  echo ""
  echo "05.undefine virsh"
  virshs=(`virsh list --all | awk '{ print $2; }'`)
  for ((virsh = 1; virsh < ${#virshs[*]}; virsh++))
  do
    echo "virsh undefine" ${virshs[$virsh]}
#    /usr/bin/virsh undefine ${virshs[$virsh]}
  done
}


function CheckVirshPool(){
  # 06.check virsh pool
  echo ""
  echo "06.check virsh pool"
  /usr/bin/virsh pool-list --all
}


function StopVirshPool(){
  # 07.stop virsh pool
  echo ""
  echo "07.stop virsh pool"
  virshs=(`virsh pool-list --all | awk '{ print $1","$2; }'`)
  for ((virsh = 2; virsh+1 < ${#virshs[*]}; virsh++))
  do
    virsh_pool_name=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $1 }'`)
    virsh_pool_state=(`echo ${virshs[$virsh]} | awk -F"$DELIMITER" '{ print $2 }'`)
    if test $virsh_pool_state = "active"; then
        echo "virsh pool-destroy" $virsh_pool_name
#       /usr/bin/virsh pool-destroy $virsh_pool_name
    fi
  done
}


function UndefineVirshPool(){
  # 08.undefine virsh pool
  echo ""
  echo "08.undefine virsh pool"
  virshs=(`virsh pool-list --all | awk '{ print $1; }'`)
  for ((virsh = 2; virsh < ${#virshs[*]}; virsh++))
  do
    echo "virsh pool-undefine" ${virshs[$virsh]}
#    /usr/bin/virsh pool-undefine ${virshs[$virsh]}
  done
}


function AgentStop(){
  # 09.agent stop
  echo ""
  echo "09.agent stop"
  /etc/init.d/cloud-agent status
  chk_agent_status=(`/etc/init.d/cloud-agent status | grep "running"`)
  if test -n $chk_agent_status; then
      echo "Since the cloud-agent is running, I will stop the cloud-agent"
#      /etc/init.d/cloud-agent stop
  fi
}


function CheckMountPrimary(){
  # 10.check mount primary
  echo ""
  echo "10.check mount primary"
  /bin/mount
}


function UmountPrimary(){
  # 11.umount primary
  echo ""
  echo "11.umount primary"
  for ((num = 0; num < ${#PRIMARY_DIR[*]}; num++))
  do
    mount_dir=(`/bin/mount | grep ${PRIMARY_DIR[$num]} | awk '{ print $3 }'`)
    if test ${#mount_dir} -gt 0; then
        echo "umount" $mount_dir
#        /bin/umount $mount_dir
    fi
  done
}


function CheckCloudBridge(){
  # 12.check cloud bridge
  echo ""
  echo "12.check cloud bridge"
  /usr/sbin/brctl show
}


function DownCloudBridge(){
  # 13.down cloud bridge + get delete_bridge.txt
  echo ""
  echo "13.down cloud bridge + get delete_bridge.txt"
  bridge_info=(`/usr/sbin/brctl show | grep cloudVirBr | awk '{ print $1","$4; }'`)
  for ((num = 0; num < ${#bridge_info[*]}; num++))
  do
    bridge_name=(`echo ${bridge_info[$num]} | awk -F"$DELIMITER" '{ print $1 }'`)
    bridge_eth=(`echo ${bridge_info[$num]} | awk -F"$DELIMITER" '{ print $2 }'`)
    echo $bridge_eth >> $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE
    echo "ifconfig" $bridge_name "down"
#    /sbin/ifconfig $bridge_name down
  done
}


function DeletecloudBridge(){
  # 14.delete cloud bridge
  echo ""
  echo "14.delete cloud bridge"
  bridge_name=(`/usr/sbin/brctl show | grep cloudVirBr | awk '{ print $1; }'`)
  for ((num = 0; num < ${#bridge_name[*]}; num++))
  do
    echo "brctl delbr" ${bridge_name[$num]}
#    /usr/sbin/brctl delbr ${bridge_name[$num]}
  done
}


function DownInterfaceCloudBridge(){
  # 15.down interface cloud bridge. use delete_bridge.txt
  echo ""
  echo "15.down interface cloud bridge. use delete_bridge.txt"
  if test -e $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE; then
      while read del_bri
      do
        echo "ifconfig" ${del_bri} "down"
#        /sbin/ifconfig ${del_bri} down
      done < $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE
  else
      echo $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE "not found."
      echo "Skip this process."
  fi
}


function DeleteInterfaceCloudBridge(){
  # 16.delete interface cloud bridge. use+delete delete_bridge.txt
  echo ""
  echo "16.delete interface cloud bridge. use+delete delete_bridge.txt"
  if test -e $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE; then
      while read del_bri
      do
        echo "vconfig rem" ${del_bri}
#        /sbin/vconfig rem ${del_bri}
      done < $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE
      rm -f $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE
  else
      echo $DELETE_BRIDGE_PATH$DELETE_BRIDGE_FILE "not found."
      echo "Skip this process."
  fi
}

function CheckEnd(){
  # 99.E N D
  echo ""
  echo "HostCreanup.sh E N D"
  /bin/date
  /bin/uname -n
}

# functions E N D
#---------------------------------------------------------------------------


case $1 in
  1)  CheckVirshList;;
  2)  DeleteVirsh;;
  3)  CheckVirshSnapshot;;
  4)  DeleteVirshSnapshot;;
  5)  UndefineVirsh;;
  6)  CheckVirshPool;;
  7)  StopVirshPool;;
  8)  UndefineVirshPool;;
  9)  AgentStop;;
  10)  CheckMountPrimary;;
  11)  UmountPrimary;;
  12) CheckCloudBridge;;
  13) DownCloudBridge;;
  14) DeletecloudBridge;;
  15) DownInterfaceCloudBridge;;
  16) DeleteInterfaceCloudBridge;;
  99)
      CheckVirshList
      DeleteVirsh
        sleep $SLEEP_TIME
      CheckVirshSnapshot
      DeleteVirshSnapshot
        sleep $SLEEP_TIME
      UndefineVirsh
        sleep $SLEEP_TIME
      CheckVirshPool
      StopVirshPool
        sleep $SLEEP_TIME
      UndefineVirshPool
        sleep $SLEEP_TIME
      AgentStop
        sleep $SLEEP_TIME
      CheckMountPrimary
      UmountPrimary
        sleep $SLEEP_TIME
      CheckCloudBridge
      DownCloudBridge
        sleep $SLEEP_TIME
      DeletecloudBridge
        sleep $SLEEP_TIME
      DownInterfaceCloudBridge
        sleep $SLEEP_TIME
      DeleteInterfaceCloudBridge
        sleep $SLEEP_TIME
      CheckVirshList
      CheckVirshPool
      CheckMountPrimary
      CheckCloudBridge
      CheckEnd
      ;;
  *)  echo "Alert";;
esac
