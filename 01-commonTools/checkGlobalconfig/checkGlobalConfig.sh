#!/bin/sh

DBVIP="ckktky4-pcldb10"


date;
echo "====================【Start Global Configuration Check】===================="
echo ""

SQL=`mysql -u root cloud -h $DBVIP -s -N -e "select name from configuration ;"`
for i in $SQL ; do


##設定したGlobal設定値tempファイル作成
      NAME=`mysql -u root cloud -h $DBVIP -s -N -e "select name from configuration where name='$i' ; "`
      VALUE=`mysql -u root cloud -h $DBVIP -s -N -e "select value from configuration where name='$i' ; "`

      echo $NAME'|'$VALUE |tee -a global_config.log > global_config.tmp


##設計値をtempファイルに出力
      cat GlobalConfig_list | grep "^$i|" |tee -a design_global_config.log > design_global_config.tmp
      result=`cat global_config.tmp`
      result2=`cat design_global_config.tmp`
      
      now_config=`cat global_config.tmp  |cut -d '|' -f 2`
      design_value=`cat design_global_config.tmp |cut -d '|' -f 2`
      
     if [[ $result = $result2 ]] ; then
       echo -e "Global設定名 $i: 設計値が適用されています。"
       echo -e "設計値: $design_value"
       echo -e "現在適用値: $now_config"
       echo -e ""
    
    elif [  -z $now_config ] ; then
       echo -e "Global設定名 $i: 設計値が適用されています(値が空)。" 
       echo -e "設計値: $design_value"
       echo -e "現在適用値: ※blank"
       echo -e ""
    
    else 
       echo -e "\e[31mGlobal 設定名 $i: 設計値を再度確認してください。\e[m" | tee -a Recheck_global_config_`date +%Y%m%d_%H%M`.log
       echo -e "\e[31m 設計値: $design_value \e[m" | tee -a Recheck_global_config_`date +%Y%m%d_%H%M`.log
       echo -e "\e[31m 現在適用値: $now_config \e[m"   | tee -a Recheck_global_config_`date +%Y%m%d_%H%M`.log
       echo -e ""  | tee -a Recheck_global_config_`date +%Y%m%d_%H%M`.log
     
     fi
      sleep 1;

rm -f global_config.tmp design_global_config.tmp design_global_config.log global_config.log

done;

echo ""
echo "====================【Done Global Configuration Check】===================="
date;
