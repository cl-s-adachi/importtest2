#!/bin/bash

BASEDIR=`dirname $0`
LOGDIR=${BASEDIR}/logs
DELETE_SNAPSHOT_LOG=${LOGDIR}/deleteSnapshot.log.`date '+%Y%m%d%H%M%S'`
DBHOST=ckktky4-vcldb00-tcsm001
APISCRIPT=${BASEDIR}/kick_api.sh
snapuuid=$1

if [ -z ${snapuuid} ] ;then
	echo $"Usage: $0 SnapshotUuid"
	exit
fi

mkdir -p ${LOGDIR}

deleteBackingUpSnapshot(){

		echo "deleting:"${snapuuid}
		mysql -u root cloud -h ${DBHOST} -e "UPDATE snapshots SET status='BackedUp' WHERE status = 'BackingUp' AND uuid=\"${snapuuid}\";" 
		${APISCRIPT} command=deleteSnapshot id=${snapuuid} >> ${DELETE_SNAPSHOT_LOG} 

		echo "done"
}

deleteBackingUpSnapshot
