use cloud;
select ms.name as management_server,cl.uuid,cl.name as cluster,count(*) as host_count from host join cluster cl on cl.id = host.cluster_id join mshost ms on ms.msid = host.mgmt_server_id where host.hypervisor_type='VMware' and host.removed is NULL group by cl.name order by ms.name;
