#!/bin/bash

SQLFILE=$1
if [ -z "${SQLFILE}" ] ;then
	echo $"Usage: $0 SQLFILE"
	exit
fi

DBHOST=ckktky4-pcldb10

BASEDIR=`dirname $0`
LOGDIR=${BASEDIR}/logs
SQL_LOG_FILE=${LOGDIR}/${SQLFILE}.`date '+%Y%m%d%H%M%S'`

mkdir -p `dirname ${SQL_LOG_FILE}`

echo "HOST: "${DBHOST} | tee -a ${SQL_LOG_FILE}
echo "mysql> "`cat ${SQLFILE}` | tee -a ${SQL_LOG_FILE}

mysql -u root -h ${DBHOST} -t -e "source ${SQLFILE}" | tee -a ${SQL_LOG_FILE}


