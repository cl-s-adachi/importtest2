#!/bin/bash

BASEDIR=`dirname $0`
LOGDIR=${BASEDIR}/logs
ERROR_SNAPSHOT_FILE=${LOGDIR}/ERROR_SNAPSHOT.`date '+%Y%m%d%H%M%S'`
DELETE_SNAPSHOT_LOG=${LOGDIR}/deleteSnapshot.log.`date '+%Y%m%d%H%M%S'`
DBHOST=ckktky4-vcldb00-tcsm001
APISCRIPT=${BASEDIR}/kick_api.sh

mkdir -p ${LOGDIR}

getErrorSnapshotList(){
	mysql -u root cloud -h ${DBHOST} -e 'select id,uuid,name from snapshots where removed is null and status="ERROR";' > ${ERROR_SNAPSHOT_FILE} 

	snapid=(`awk '{print $1}' ${ERROR_SNAPSHOT_FILE} | grep -v id | tr  "\n" " "`)
	snapuuid=(`awk '{print $2}' ${ERROR_SNAPSHOT_FILE} | grep -v uuid | tr "\n" " "`)
	snapname=(`awk '{print $3}' ${ERROR_SNAPSHOT_FILE} | grep -v name | tr  "\n" " "`)
}

deleteErrorSnapshot(){
	ErrorSnapshotCount=${#snapid[@]}
	i=0
	while [ ${i} -lt ${ErrorSnapshotCount} ]	
	do
		echo "deleting:"${snapid[i]}
		mysql -u root cloud -h ${DBHOST} -e "UPDATE snapshots SET status='BackedUp' WHERE status != 'BackedUp' AND id=${snapid[i]};" 
	
		${APISCRIPT} command=deleteSnapshot id=${snapuuid[i]} >> ${DELETE_SNAPSHOT_LOG} 
		sleep 5

		echo "done"
		i=$((i + 1))
	done

}

getErrorSnapshotList
deleteErrorSnapshot
