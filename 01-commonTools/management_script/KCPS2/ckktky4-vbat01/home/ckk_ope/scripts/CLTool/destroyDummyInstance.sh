﻿#!/bin/sh

#ver=KCPS2

opt_flg="a"

while getopts :s: OPT
do
 case ${OPT} in
  "s" ) source="${OPTARG}";;
  *) echo $"Usage: $0 { -s SourceSnapshot }"
     exit ;;
 esac
done
shift $((OPTIND - 1))

if [ -z ${source} ] ;then
   echo $"Usage: $0 { -s SourceSnapshot }"
   exit
fi

BASEDIR=`dirname $0`

KICK_API=${BASEDIR}/./kick_api.sh
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`

TMPDIR=${BASEDIR}/tmp
TMPFILE="${TMPDIR}/drjob"

mkdir -p ${TMPDIR}

INFO_LV="[INFO]"
WARN_LV="[INFO]"
TAG="[CloudStack]"
SCRIPT="`basename $0`"

echo "${INFO_LV} ${TAG}" "Start ${SCRIPT}"

BASEDIR=`dirname $0`
DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`

MRDBHOST=`echo ${DBHOST} | sed s/[0-9][0-9]/03/g`

TMPDIR="${BASEDIR}/tmp"
mkdir -p ${TMPDIR}

name=${source}

 result=`mysql -u root cloud -h ${MRDBHOST} -P 3307 -N -e "select account.account_name,data_center.uuid,domain.name  from snapshots,account,domain,data_center where snapshots.account_id=account.id and account.domain_id=domain.id and snapshots.data_center_id=data_center.id and snapshots.name='${name}'"`
  if [ "x${result}" != "x" ] ; then
     acName=`echo ${result} | cut -d" " -f1`
     dcUuid=`echo ${result} | cut -d" " -f2`
     domName=`echo ${result} | cut -d" " -f3`
     dcIdR=""
     DCcount=0

        while :
        do
         result=`mysql -u root cloud -h ${DBHOST} -N -e "select vm.uuid, vm.name, dc.id from cloud.vm_instance as vm join cloud.vm_template  as temp  on vm.vm_template_id = temp.id  join cloud.account as ac on ac.id =  vm.account_id join cloud.data_center as dc on dc.id = vm.data_center_id  join cloud.domain as do on ac.domain_id=do.id where vm.name like 'v-site-recovery-%' and vm.removed is null and temp.name like 'DR_Template' and ac.account_name='${acName}' and do.name='${domName}' ${dcIdR};"`
         if [ "x${result}" != "x" ] ; then
            vmUuid=`echo ${result} | cut -d" " -f1`
            vmName=`echo ${result} | cut -d" " -f2`
            dcId=`echo ${result} | cut -d" " -f3`

            ${KICK_API} command=destroyVirtualMachine id=${vmUuid} | grep job
            echo "Destroyed Instance:${vmName}"

            dcIdR+="and NOT vm.data_center_id=${dcId} "
            DCcount=$(( DCcount + 1 ))

         else
            echo "Not found of dummy instance."
            break

         fi
      done
  else
  echo "Not found of snapshot."
 fi
echo "${INFO_LV} ${TAG}" "End ${SCRIPT}"