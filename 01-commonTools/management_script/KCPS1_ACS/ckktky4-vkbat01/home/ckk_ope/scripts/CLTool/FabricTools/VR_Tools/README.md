# ACS4.9対応 VRツール

## PPTPモジュールインストール

### pptp_check_install

PPTPモジュールがインストールされているかチェックする。

```
fab set_env:config=[環境設定ファイル] pptp_check_install:target_file=[ターゲットファイル]
```

* [環境設定ファイル]: 設定ファイル
* [ターゲットファイル]: M番号とNetwork UUIDを含むcsvファイル
    
    ```
    M00000001,861c0e4a-6a61-4be1-83b1-ecdf5dcd0be2
    ```

* 実行例

    ```
    $ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
    $ fab set_env:config=conf/production.yml pptp_check_install:target_file=target/pptp_production.txt
    ```
     

### pptp_install

PPTPモジュールをインストールする

```
$ fab set_env:config=[環境設定ファイル] pptp_install:target_file=[ターゲットファイル]
```

* [環境設定ファイル]: 設定ファイル
* [ターゲットファイル]: M番号とNetwork UUIDを含むcsvファイル
* 実行例

    ```
    $ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
    $ fab set_env:config=conf/production.yml pptp_install:target_file=target/pptp_production.txt
    ```


## dnsmasqアップデート


### dnsmasq_update

Dnsmasqをアップデートする。

```
$ fab set_env:config=[環境設定ファイル] dnsmasq_update:target_file=[ターゲットファイル]
```

* [環境設定ファイル]: 設定ファイル
* [ターゲットファイル]: VR名を含むcsvファイル

    ```
    r-11200-VM
    r-11227-VM
    ```

* 実行例

```
$ export PYTHONPATH="/home/ckk_ope/scripts/CLTool/FabricTools/"
$ fab set_env:config=conf/production.yml dnsmasq_update:target_file=target/dnsmasq_production.txt
```



