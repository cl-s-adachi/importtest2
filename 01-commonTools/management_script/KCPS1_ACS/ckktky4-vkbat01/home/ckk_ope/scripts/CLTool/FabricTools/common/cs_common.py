# -*- coding: utf-8 -*-

# Python standard Libary
import sys, codecs, re
import os.path
import logging
import json
import time
import functools
from datetime import datetime, timedelta

# PyYaml
import yaml

# fabric
from fabric.api import local, run, execute, put
from fabric.api import env, settings
from fabric.decorators import *
from fabric.colors import *
from fabric.utils import puts


# Exoscale cs
from cs import CloudStack


def connectCloudStack():
    """
    CloudStackオブジェクトの作成
    """
    # Root Admin
    cs = CloudStack(endpoint=env.csApi['endpoint'], key=env.csApi['key'], secret=env.csApi['secret'])
    return cs

