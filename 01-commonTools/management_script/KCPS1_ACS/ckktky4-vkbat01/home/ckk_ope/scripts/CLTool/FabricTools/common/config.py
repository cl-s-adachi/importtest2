# -*- coding: utf-8 -*-

# Python standard Libary
import sys, codecs, re
import os.path
import logging
import functools
import distutils.util

# PyYaml
import yaml

# fabric
from fabric.api import local, run, execute, put
from fabric.api import env, settings
from fabric.decorators import *
from fabric.colors import *
from fabric.utils import puts

## log settings

stream_log = logging.StreamHandler()
stream_log.setLevel(logging.DEBUG)
stream_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

"""
file_log = logging.FileHandler(filename='operation.log')
file_log.setLevel(logging.INFO)
file_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

logging.getLogger().addHandler(file_log)
logging.getLogger().setLevel(logging.INFO)
"""


@task
@runs_once
def set_env(config):
    """
    設定ファイルの読み込み
    """
    logging.getLogger().addHandler(stream_log)
    if not os.path.exists(config):
        logging.error(u'設定ファイルが存在しません。%s' % config)
        exit(1)

    try:
        # 設定ファイルの読み込み
        with open(config, 'r') as f:
            conf = yaml.load(f)

        env.hosts    = []   # 動的にホスト設定する
        env.vr       = conf['vr']
        env.ap       = conf['ap']
        env.csApi    = conf['csApi']
        env.database = conf['database']
        env.sshGW = conf['sshGW']
        try:
            debug = distutils.util.strtobool(env.debug)
        except Exception as e:
            logging.warn('env.debug value is wrong: ' + e.message)
            debug = False

        env.debug = debug

        if not conf.has_key('log'):
            log_dir = 'logs'
            log_filename = 'operation.log'
        else:
            log_dir = conf['log'].get('dir', 'logs')
            log_filename =  log_dir + '/' + conf['log'].get('filename', 'operation.log')

        env.log_dir = log_dir

        # ログディレクトリの作成
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)

        global file_log
        file_log = logging.FileHandler(filename = log_filename)
        file_log.setLevel(logging.INFO)
        file_log.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))

        logging.getLogger().addHandler(file_log)
        logging.getLogger().setLevel(logging.INFO)
    except Exception as e:
        print e
        logging.error(u'設定ファイルの読み込みに失敗しました。%s' % config)
        exit(1)
    else:
        logging.info('config file loaded ... %s' % config)

def getConnectEnv(host, envTarget):
    """
    fabricの接続先を切り替えるための一時的なEnv作成
    """
    params = {'host_string': host,
              'port': envTarget.get('port', 22),
              'user': envTarget.get('user', None),
              'password': envTarget.get('password', None),
              'key_filename': envTarget.get('key_filename', None),
              'disable_known_hosts': True
              }
    return params

def getConnectEnvGW(host, envTarget, envGateway):
    """
    Gatewayを介してホスト接続するため一時的なEnv作成

    :param str host: 接続先ホスト名
    :param list envTarget: 以下のkeyを含むdict
        user
        port
        key_filename
    :param list envGateway: 以下のkeyを含むdict
        user
        host
        port
        password
    """
    logging.getLogger().addHandler(stream_log)
    logging.debug('Execute _getVrConnectEnvGW.')
    # ssh gatewayを設定
    # env.passwords は user@host:port 形式で指定しなければならない(GW IP/Hostnameだけではだめ)
    env.passwords['%s@%s:%s' % (envGateway.get('user', None),
                                envGateway.get('host', None),
                                envGateway.get('port', None))] = envGateway.get('password', env.password)
    params = {'host_string': host,
              'port': envTarget.get('port', 22),
              'user': envTarget.get('user', None),
              'key_filename': envTarget.get('key_filename', None),
              'disable_known_hosts': True,
              'gateway': '%s@%s:%s' % (
                             envGateway.get('user', None),
                             envGateway.get('host', None),
                             envGateway.get('port', None))
             }
    return params

