# -*- coding: utf-8 -*-
import json


def dump_json(json_dict):
    print(json.dumps(json_dict, indent=2))


def get_formated_message(m_num, vr_name, msg):
    """
    ログファイル用メッセージの生成

    :param str m_num: M番号
    :param str vr_name: VR名
    :param str msg: メッセージ
    :return str: フォーマット済のメッセージ
    """
    return '[%s:%s] %s' % (m_num, vr_name, msg)


def create_mysql_command(query):
    """
    MySQL実行コマンド文字列の生成
    mysqlクライアントがインストールされ、PATHが通っている必要あり
    """
    if env.database['password']:
        command = 'mysql -h %(host)s --port=%(port)s --user=%(user)s --password=%(password)s --skip-column-names -U cloud -e \'%(query)s\''
    else:
        command = 'mysql -h %(host)s --port=%(port)s --user=%(user)s --skip-column-names -U cloud -e \'%(query)s\''

    return command % {
        'host': env.database['host'],
        'port': env.database['port'],
        'user': env.database['user'],
        'password': env.database['password'],
        'query': query }
