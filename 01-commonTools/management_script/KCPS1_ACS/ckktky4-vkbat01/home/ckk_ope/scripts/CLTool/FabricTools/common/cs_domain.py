# -*- coding: utf-8 -*-

# Python standard Libary
import sys
import codecs
import re
import os.path
import logging
import json
import time
import functools
from datetime import datetime, timedelta

# Fabric
from fabric.api import env

# Exoscale cs
from cs import CloudStack

# KCPS
#from common import util
import cs_common
import util


def get_id(m_num):
    """
    M番号に紐づくIDを取得する

    :param str m_num: 検索するM番号
    :return dict: ドメインID
    """
    result = get_domain(m_num)

    if not result:
        return None

    if env.debug:
        util.dump_json(result)

    domain_id = result['id']
    return domain_id

def get_domain(m_num = None, uuid = None):
    """
    M番号に紐づくDomain情報取得

    :param str m_num: 検索するM番号
    :return dict: M番号のドメイン情報
    """
    cs = cs_common.connectCloudStack()
    if m_num:
        result = cs.listDomains(name = m_num, listall = True)
    elif uuid:
        result = cs.listDomains(id = uuid, listall = True)
    if env.debug:
        util.dump_json(result)

    if not result:
        return None

    return result['domain'][0]

