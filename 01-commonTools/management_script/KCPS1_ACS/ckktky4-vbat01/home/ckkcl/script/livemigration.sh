#!/bin/sh

#############################################################################################
#
# This script to do the live-migration.
#
# * This script have to arrangement to same as the directory of kick_api.sh.
#
# Last Up-date : 13 February 2015
#
#############################################################################################

# This section to enter a subject instance. Then, this value is checking.

clear
echo
echo " This script will do the live-migration."
echo
echo -n " Please enter a subject instance. : "
read VAL1

VHID=`./kick_api.sh command=listVirtualMachines name="$VAL1" listall=true | grep -B 1 "<name>$VAL1</name>"  | sed -n -e 1p | sed -e 's/<id>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`

CHECK=`echo "$VHID" | grep "-" | wc -l`
if [ $CHECK -eq 0 ]; then
 VHID=`./kick_api.sh command=listRouters name="$VAL1" listall=true | grep -B 8 "<name>" | grep "<id>" | sed -e 's/<id>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`
 CHECK=`echo "$VHID" | grep "-" | wc -l`
 if [ $CHECK -eq 0 ]; then
  VHID=`./kick_api.sh command=listSystemVms name="$VAL1" listall=true | grep -B 8 "<name>" | grep "<id>" | sed -e 's/<id>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`
  CHECK=`echo "$VHID" | grep "-" | wc -l`
  if [ $CHECK -eq 0 ]; then
   clear
   echo
   echo " Could not find an instance from the system. If you have to do this again, please re-execute this script. Then, re-enter a subject instance."
   echo
   exit 1
  else
   CMD=listSystemVms
   MIGRATECMD=migrateSystemVm
  fi
 else
  CMD=listRouters
  MIGRATECMD=migrateSystemVm
 fi
else
 CMD=listVirtualMachines
 MIGRATECMD=migrateVirtualMachine
fi

INSN=`./kick_api.sh command="$CMD" id="$VHID" listall=true`

STATCHK=`echo "$INSN" | grep "<state>Running</state>" | wc -l`
if [ $STATCHK -eq 0 ]; then
 clear
 echo
 echo " This instance is not running. If you have to do this again, please re-execute this script. Then, re-enter a running instance to subject."
 echo
 exit 1
fi

# This section to acquire a cluster information of the subject instance.

CRHOST=`echo "$INSN" | grep "<hostname>" | sed -e 's/<hostname>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`
CLID=`./kick_api.sh command=listHosts name="$CRHOST" list=true | grep "<clusterid>" | sed -e 's/<clusterid>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`

# This section to acquire the hosts information of identical clusterid.

SUBJHOSTS=`./kick_api.sh command=listHosts clusterid="$CLID"`
CHSUJHOST=`echo "$SUBJHOSTS" | grep "<name>" | sed  -e 's/ //g' | sed -e 's/<name>//g' | awk -F"<" '{print $1}' | wc -l`
if [ $CHSUJHOST -eq 1 ]; then
 clear
 echo
 echo " This instance does not have the subject hosts for live-migration. please confirm the configuration of the subject instance."
 echo
 exit 1
fi

HOSTSTMP=`echo "$SUBJHOSTS" | grep "<name>" | sed  -e 's/ //g' | sed -e 's/<name>//g' | awk -F"<" '{print $1}' | sed '/'$CRHOST'/d'`


# This section to create the hosts list.

NUM=0
rm -f ./host.lst

echo "$HOSTSTMP" | while read SUBJECT
do
 HOSTINFO=`./kick_api.sh command=listHosts name="$SUBJECT" list=true`
 STA=`echo "$HOSTINFO" | grep "<state>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<state>//g' | awk -F"<" '{print $1}'`
 RCSTA=`echo "$HOSTINFO" | grep "<resourcestate>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<resourcestate>//g' | awk -F"<" '{print $1}'`
 HTAG=`echo "$HOSTINFO" | grep "<hosttags>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<hosttags>//g' | awk -F"<" '{print $1}'`
 CPUUSE=`echo "$HOSTINFO" | grep "<cpuused>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<cpuused>//g' | awk -F"<" '{print $1}'`
 CAPAMEM=`echo "$HOSTINFO" | grep "<memorytotal>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<memorytotal>//g' | awk -F"<" '{print $1}'`
 MEMALO=`echo "$HOSTINFO" | grep "<memoryallocated>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<memoryallocated>//g' | awk -F"<" '{print $1}'`
 MEMUSE=`echo "$HOSTINFO" | grep "<memoryused>" | sed -n -e 1p | sed -e 's/ //g' | sed -e 's/<memoryused>//g' | awk -F"<" '{print $1}'`
 NUM=`expr $NUM + 1`
 echo " $NUM". [Host_Name] "$SUBJECT" [Status/ResourceState] "$STA"/"$RCSTA" [Host_Tages] "$HTAG" [CPU_Used] "$CPUUSE" [Memory_Used] "$MEMUSE" Byte / "$MEMALO" Byte / "$CAPAMEM" Byte"" >> ./host.lst
done

# This section to enter a subject hosts. Then, this value is checking.

clear
echo
echo " $VAL1 is running on the $CRHOST now."
echo
echo " Please find a number of the subject host from the list below. Then, enter that number."
echo
echo " If you want to cancel the live-migration. Please enter 0."
echo
cat ./host.lst
echo
echo -n " Please enter a number of subject host. : "
read VAL2

if [ $VAL2 -eq 0 ]; then
 clear
 echo
 echo " This script canceled the live-migration."
 echo
 rm -f ./host.lst
 exit 0
fi

COUNT=`cat host.lst | wc -l`
if [ $VAL2 -le $COUNT ]; then
 clear
 echo
 echo " The live-migration will commence. Please wait few minutes."
 echo
 sleep 3
else
 clear
 echo
 echo " You wrong selected number of the host. If you have to do this again, please re-execute this script."
 echo
 rm -f ./host.lst
 exit 1
fi

# This section do the live-migration commence.

SHOS=`cat host.lst | sed -n -e "$VAL2"p | awk '{ print $3 }'`


HOSTID=`./kick_api.sh command=listHosts name="$SHOS" | grep -B 1 "<name>$SHOS</name>" | sed -n -e 1p | sed -e 's/<id>//g' | sed -e 's/ //g' | awk -F"<" '{print $1}'`

# This section to kick live-migration command.

JOBID=`./kick_api.sh command="$MIGRATECMD" virtualmachineid="$VHID" hostid="$HOSTID" | grep "<jobid>" | sed -e 's/ //g' | sed -e 's/<jobid>//g' | awk -F"<" '{print $1}'`


# This section to check the live-migration status.

TIMEOUT=1

while [ $TIMEOUT -le 30 ]
do
 clear
 echo
 echo " Checking......."
 sleep 1
 JOBSTA=`./kick_api.sh command=queryAsyncJobResult jobid="$JOBID" | grep "<jobstatus>" | sed -e 's/ //g' | sed -e 's/<jobstatus>//g' | awk -F"<" '{print $1}'`
 if [ $JOBSTA -eq 1 ]; then
  LVMGSTA=`./kick_api.sh command="$CMD" id="$VHID" listall=true | grep "<hostname>$SHOS</hostname>" | wc -l`
  if [ $LVMGSTA -eq 0 ]; then
   clear
   echo
   echo " The live-migration is in-progress...."
   echo
   TIMEOUT=`expr $TIMEOUT + 1`
   sleep 10
  else
   clear
   echo
   echo " The live-migration was successful."
   echo
   rm -f ./host.lst
   exit 0
  fi
 else
  if [ $JOBSTA -eq 0 ]; then
   clear
   echo
   echo " The live-migration is in-progress...."
   echo
   TIMEOUT=`expr $TIMEOUT + 1`
   sleep 10
  else
   JOBERR=`./kick_api.sh command=queryAsyncJobResult jobid="$JOBID" | grep "<errortext>" | sed -e 's/  //g' | sed -e 's/<errortext>//g' | awk -F"<" '{print $1}'`
   clear
   echo
   echo " <Error Information>: $JOBERR."
   echo
   echo " The live-migration was failure. Please confirm status from the portal and logs."
   echo
   rm -f ./host.lst
   exit 1
  fi
 fi
done

clear
echo
echo " The live-migration status check was time-out. Please confirm status from the portal and logs."
echo
rm -f ./host.lst

exit 1
