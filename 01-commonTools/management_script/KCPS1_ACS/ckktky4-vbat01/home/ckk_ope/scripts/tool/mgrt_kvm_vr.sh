#!/bin/bash -e
#
# Copyright(c) 2015 CREATIONLINE,INC. All Right Reserved.
#
scriptDir=$(cd $(dirname $0); pwd)
errPrefix="\033[0;31mError:\033[0;39m"
okPrefix="\033[0;32mSuccess:\033[0;39m"

kickApiPath="$scriptDir/kick_api.sh"
logFilePath="$scriptDir/.mgrtkvr.log"
vlanScriptPath="/usr/lib64/cloud/agent/scripts/vm/network/vnet/modifyvlan.sh" # CS3.x
#vlanScriptPath="/usr/lib64/cloud/common/scripts/vm/network/vnet/modifyvlan.sh" # CS4.x
vlanPif="bond1"
jobConfirmationCount=12
jobConfirmationWaitTime=5s
debug=false
rootPW="Admin123ckk!"

usage() {
  echo "Usage: $0 -r <VR name> -h <host name>"
}

log() {
  if [ $# -eq 1 -o $debug = "true" ] ; then
    echo -e "[`date "+%Y/%m/%d %H:%M:%S"`] $1$2" >> $logFilePath
  fi
}


#------------------------------------------------------------------------
# [Step 1] Arguments check
#------------------------------------------------------------------------
if [ $# -ne 4 ] ; then
  usage
  exit 1
fi
while getopts "h:r:" OPTION
do
  case $OPTION in
    h) hostName="$OPTARG" ;;
    r) vrName="$OPTARG" ;;
    ?) usage
       exit 1 ;;
  esac
done
log "<Command and args> $0 $*"


#------------------------------------------------------------------------
# [Step 2] Get VR ID from VR name using listRouters command.
#------------------------------------------------------------------------
res=`$kickApiPath command=listRouters listall=true name=$vrName`
log "raw data of listRouters" "\n$res"
vrInfo=`echo -e "$res" | awk -F"[<>]" '/^    <id>/{print $3} /^    <hostid>/{print $3} /^    <hostname>/{print $3}'`
set -- $vrInfo
vrId=$1; vrHostId=$2; vrHostName=$3;
if [ -z "$vrId" ] ; then
  echo -e "$errPrefix No virtual router found."
  log "No virtual router found."
  log "exit code 2"
  exit 2
fi
echo "The virtual router info: name=$vrName, host=$vrHostName"
log "VR name=$vrName, id=$vrId, hostid=$vrHostId, hostname=$vrHostName"


#------------------------------------------------------------------------
# [Step 3] Find host live-migration enabled.
#------------------------------------------------------------------------
# CloudStack version 4.x
#res=`$kickApiPath command=findHostsForMigration virtualmachineid=$vrId`
#log "raw data of findHostsForMigration" "\n$res"
#
# CloudStack version 3.0.x  Method 1:listHosts with virtualmachineid parameter.
#res=`$kickApiPath command=listHosts type=Routing virtualmachineid=$vrId`
#log "raw data of listHosts" "\n$res"

# CloudStack version 3.0.x  Method 2:listHosts to get a cluster ID then listHosts to get hosts on the cluster.
clusterId=`$kickApiPath command=listHosts id=$vrHostId | awk -F"[<>]" '/<clusterid>/{print $3}'`
log "The cluster ID the virtual router is running on is $clusterId"
if [ -z "$clusterId" ] ; then
  echo -e "$errPrefix Cluster ID wasn't acquired from the Host ID the virtual router is running."
  log "Cluster ID wasn't acquired from the Host ID the virtual router is running."
  exit 3
fi
res=`$kickApiPath command=listHosts type=Routing clusterid=$clusterId`
log "raw data of listHosts" "\n$res"

hostInfo=`echo -e "$res" | egrep -A 15 -B 1 "<name>$hostName" | awk -F"[<>]" '/<id>/{print $3} /<ipaddress>/{print $3} /<hypervisor>/{print $3}'`
if [ -z "$hostInfo" ] ; then
  echo -e "$errPrefix '$hostName' isn't migratable host."
  log "'$hostName' isn't migratable host."
  log "exit code 3"
  exit 3
fi
set -- $hostInfo
hostId=$1; hostIp=$2; hostHv=$3
echo "The destination host info: name=$hostName, ip=$hostIp, hypervisor=$hostHv"
log "Host name=$hostName, id=$hostId, ip=$hostIp hypervisor=$hostHv"
if [ "$hostHv" != "KVM" ] ; then
  echo -e "$errPrefix The hypervisor of the host isn't KVM but $hostHV"
  log "The hypervisor of the host isn't KVM but $hostHV"
  exit 3
fi


#------------------------------------------------------------------------
# [Step 4] Add vlan on the host using modifyvlan.sh script.
#------------------------------------------------------------------------
cmd="$vlanScriptPath -v 1900 -p $vlanPif -o add &&
$vlanScriptPath -v 1899 -p $vlanPif -o add &&
$vlanScriptPath -v 1898 -p $vlanPif -o add &&
$vlanScriptPath -v 1897 -p $vlanPif -o add"
PR='(#|\\$) $'

res=`expect -c "
 set timeout 20
 spawn ssh root@$hostName \"$cmd\"
 while (1) {
    expect timeout { break } \"(yes/no)?\" { sleep 1;send \"yes\r\" } \"word: \" { sleep 1;send \"$rootPW\r\" } -re \"$PR\" { sleep 1;send \"\r\";break }
}"`

if [ $? -ne 0 ] ; then
  echo -e "$errPrefix Returned error response from the host."
  log "Response from the host" "\n$res"
  log "exit code 4"
  exit 4
fi
echo "Added vlan on the host."
log "Added vlan on the host."


#------------------------------------------------------------------------
# [Step 5] Migrate virtual router to the host.
#------------------------------------------------------------------------
res=`$kickApiPath command=migrateSystemVm hostid=$hostId virtualmachineid=$vrId`
log "raw data of migrateSystemVm" "\n$res"
jobid=`echo -e "$res" | awk -F"[<>]" '/<jobid>/{print $3}'`
log "Job id=$jobid"
for i in `seq 1 $jobConfirmationCount`
do
  echo "Waiting to finish the migration...($i/$jobConfirmationCount)"
  sleep $jobConfirmationWaitTime
  res=`$kickApiPath command=queryAsyncJobResult jobid=$jobid`
  log "raw data of queryAsyncJobResult ($i/$jobConfirmationCount)" "\n$res"
  jobInfo=`echo -e "$res" | awk -F"[<>]" '/<jobstatus>/{print $3} /<jobresultcode>/{print $3} /<errortext>/{print $3}'`
  _IFS=$IFS; IFS=$'\n'
  set -- $jobInfo
  IFS=$_IFS
  jobState=$1; jobResultCode=$2; jobErrText=$3
  if [ "$jobState" != "0" ] ; then
    if [ "$jobResultCode" != "0" ] ; then
      echo -e "$errPrefix $jobErrText"
      log "Job error: $jobErrText"
      log "exit code 5"
      exit 5
    else
      echo -e "$okPrefix The migration of the router is finished."
      log "The migration of the router is finished."
      log "exit code 0"
      exit 0
    fi
  fi
done
echo -e "$errPrefix Timeout. Please confirm to finish the migration by yourself."
log "Timeout"
log "exit code 6"
exit 6
