#!/bin/bash

# please set your host
# address="http://localhost:8096"
# address="http://172.22.132.6:8080"
#address="http://172.22.132.14:8080"
address="http://172.22.132.7:8080"


# please set your api key
api_key="Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA"


# please set your secret key
secret_key="aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw"




api_path="/client/api?"


if [ $# -lt 1 ]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
elif [[ $1 != "command="* ]]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
elif [ $1 == "command=" ]; then
 echo "usage: $0 command=... paramter=... parameter=..."; exit;
fi


# echo " asako 1 $@"

data_array=("$@" "apikey=${api_key}")
# echo " asako 2 ${data_array[@]}"

temp1=$(echo -n ${data_array[@]} | \
  tr " " "\n" | \
  sort -fd -t'=' | \
  perl -pe's/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg'| \
  tr "A-Z" "a-z" | \
  tr "\n" "&" )
# echo " asako 3 sorted ${temp1[@]}"

signature=$(echo -n ${temp1[@]})
signature=${signature%&}
# echo " asako 4 $signature"

signature=$(echo -n $signature | \
  openssl sha1 -binary -hmac $secret_key | \
  openssl base64 )

# echo " asako 5 signature=$signature"
signature=$(echo -n $signature | \
  perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')
# echo " asako 6 urlencoded signature=$signature"


url=${address}${api_path}$(echo -n $@ | tr " " "&")"&"apikey=$api_key"&"signature=$signature
echo " SEND URL: $url"


curl ${url} | xmllint --format -
