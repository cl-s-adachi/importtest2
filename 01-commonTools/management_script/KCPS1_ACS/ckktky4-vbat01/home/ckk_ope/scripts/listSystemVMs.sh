#!/bin/sh

## This script is counting SSVM of each zones
## This script have to arrangement to same as the directory of kick_api.sh.
echo "-------------------------------------------------"
echo " Checking the number of SecondaryStorageVM(SSVM) "
echo "-------------------------------------------------"

# default counts of SSVM


#case $1 in
#     -s)  DEFAULT_SSVM=$2 ;;
#      *)  DEFAULT_SSVM=6 ;; 
#esac

DEFAULT_SSVM=6
LOGGING=0

while getopts "s:l:" OPT
do
	case $OPT in
	s) DEFAULT_SSVM=${OPTARG} ;;
	l) LOGGING=1 ;;
	
        esac
done

ZONES=`./kick_api.sh command=listZones 2>/dev/null |grep "<name>" |sed -e 's/ //g' | sed -e 's/<name>//g'  | sed -e 's/<\/name>//g'`
for ZONE in $ZONES
do
ZONEID=`./kick_api.sh command=listZones name="$ZONE" 2>/dev/null |grep "<id>" |sed -e 's/ //g' | sed -e 's/<id>//g'  | sed -e 's/<\/id>//'`
SSVM=`./kick_api.sh command=listSystemVms zoneid="$ZONEID" systemvmtype=secondarystoragevm listall=true 2>/dev/null | grep "<name>" | sed -e 's/<name>//g' | sed -e 's/ //g' | sed -e 's/<\/name>//g'`
SSVM_COUNT=`echo "$SSVM" | wc -l`
  echo "$ZONE "
  if [ $SSVM_COUNT -eq   $DEFAULT_SSVM ]; then
    MESSAGE="INFO $ZONE SSVMの数は正常(基準値)です。"
  elif [ $SSVM_COUNT -gt $DEFAULT_SSVM ]; then
    MESSAGE="WARNING $ZONE SSVMの数は基準値より多いです。"
  elif [ $SSVM_COUNT -lt $DEFAULT_SSVM ]; then    
    MESSAGE="INFO $ZONE SSVMの数は基準値より少ないです。"
  fi
  echo " SSVM :"$SSVM" "
  echo " count:"$SSVM_COUNT" "
  if [ $LOGGING -eq 1 ]; then
    logger -i $MESSAGE
  else
    echo $MESSAGE
  fi
done
