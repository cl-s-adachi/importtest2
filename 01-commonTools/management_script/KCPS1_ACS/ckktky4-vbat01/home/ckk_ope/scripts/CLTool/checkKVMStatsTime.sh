#!/bin/bash


BASEDIR=`dirname $0`

KICK_API=${BASEDIR}/./kick_api.sh
SSHLOGIN=${BASEDIR}/tool/SshTool/SshHost.sh

execCommandToHost(){
echo ${SSHLOGIN} ${HOST}
        expect -c "
set timeout 10
spawn ${SSHLOGIN} ${HOST}
expect \"Are you sure you want to continue connecting (yes/no)?\" {
   send \"Yes\n\"
   expect \"root@*pbhpv* ~]# \"
   send \"${CMD}\n\"
}  \"root@*pbhpv* ~]# \" {
   send \"${CMD}\n\"
}
expect \"root@*pbhpv* ~]# \"
send \"exit \n\"
expect \"*@*\"
send \"exit \n\"
"

}

HOST="$1"

###################ACS4.9######################
#CMD="grep PingRoutingWithNwGroupsCommand /var/log/cloud/agent/agent.log | grep -v GMT |tail -n 5"
CMD="grep PingRoutingWithNwGroupsCommand /var/log/cloudstack/agent/agent.log | grep -v GMT |tail -n 5"

###############################################

execCommandToHost
