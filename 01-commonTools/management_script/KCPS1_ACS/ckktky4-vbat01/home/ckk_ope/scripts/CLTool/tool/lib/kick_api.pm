#CloudStack APIを実行する
package kick_api;
use LWP::Simple;
require "env.pl";

$ENV{'PATH'} = '/usr/bin:/usr/local/bin';

sub api{
my $command = shift;

#API URL
our $ADDRESS;
our $API_KEY;
our $SECRET_KEY;

my $api_path="/client/api?";

$command = $command . " " . "apikey=$API_KEY";

my @para= split(/\s/,$command);
my @para2;
my $signature;
my $num=0;

foreach $i (sort @para){
$i=~s/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg;
$i=~tr/A-Z/a-z/;
$para2[$num]=$i;
$num++;
}

$signature = join("&",@para2);
my $sign = `echo -n "$signature"|openssl sha1 -binary -hmac $SECRET_KEY|openssl base64`;
chomp $sign;
$sign =~s/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg;
$command =~tr/" "/"\&"/;
my $url=$ADDRESS . $api_path . "&" . $command . "&signature=". $sign;

#print "$url\n";

my $html = get("$url") or die "Couldn't get it!";
return $html;
}
