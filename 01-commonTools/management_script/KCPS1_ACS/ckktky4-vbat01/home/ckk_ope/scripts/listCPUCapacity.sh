#!/bin/bash

# Copyright(c) 2012 CREATIONLINE,INC. All Right Reserved.
# version 1.1
# 2012/07/26 y-arai	update systemVM hostname logic

# please set your host
# arai-san's environement
#address="http://ckktky4-vclweb01:8080"
#address="http://ckktky4-vclweb02:8080"
#address="http://ckktky4-vclweb11:8080"
address="http://172.22.132.7:8080"


# please set your api key
# this is api@ROOT@vclweb
#api_key="jPEEgFXy_XWxS-OC6o0nOh__c7CVx0OsySB-f_74wLjIUVqzvX3gmU1IAzLaZDgaosI_MdJwhQD45qdm7qxORw"
api_key="Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA"


# please set your secret key
# this is api@ROOT@vclweb
secret_key="aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw"

# export LC_ALL='C'

api_path="/client/api?"

tempfile1=temp1
tempfile2=temp2

#
# usage: exeute_command command=... paramter=... parameter=...
# return: command output (xml format)
#
function execute_command()
{
  local api_path="/client/api?"
## echo " debug print 1 $@"

  local data_array=("$@" "apikey=${api_key}")
## echo " debug print 2 ${data_array[@]}"

  local temp1=$(echo -n ${data_array[@]} | \
    tr " " "\n" | \
    sort -fd -t'=' | \
    perl -pe's/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg'| \
    tr "A-Z" "a-z" | \
    tr "\n" "&" )
## echo " debug print 3 sorted ${temp1[@]}"

  local signature=$(echo -n ${temp1[@]})
  signature=${signature%&}
## echo " debug print 4 $signature"

  signature=$(echo -n $signature | \
    openssl sha1 -binary -hmac $secret_key | \
    openssl base64 )
## echo " debug print 5 signature=$signature"

  signature=$(echo -n $signature | \
    perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')
# echo " debug print 6 urlencoded signature=$signature"

  local url=${address}${api_path}$(echo -n $@ | tr " " "&")"&"apikey=$api_key"&"signature=$signature
## echo " SEND URL: $url"

  curl -s ${url} | xmllint --format -
}


declare -a cluster_list
declare -a host_list
declare -a cpuallocated_list
function get_host_list()
{
  execute_command command=listHosts listall=true type=Routing details=capacity > $tempfile1 

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listhostsresponse/host/clustername/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  cluster_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listhostsresponse/host/name/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  host_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listhostsresponse/host/cpuallocated/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  cpuallocated_list=($strings)
}



# exec clusterCpuUsed
  get_host_list
  # echo "debug count=${#host_list[@]} ${cluster_list[@]} ${cpuallocated_list[@]}"
  #
  # delimiter of each line is tab
  #
  echo -e "cluster name\thost name\tcpuallocated"
  for ((i=0; i<${#host_list[@]}; i++))
  do
    echo -e "${cluster_list[$i]}\t${host_list[$i]}\t${cpuallocated_list[$i]}"
  done | \
  sort -t$'\t' -k1,2 | \
  awk 'BEGIN {FS="\t"; OFS="\t"; sum=0; cluster=""; host_cnt = 0;} \
      {if(cluster=="") {sum+=$3; cluster=$1; host_cnt++; {print $1, $2, $3}} \
       else if (cluster==$1) {sum+=$3; host_cnt++;       {print $1, $2, $3}} \
       else { \
           {print "--- " cluster, "average:", (sum/host_cnt)"%"} \
            sum=$3; cluster=$1; host_cnt= 1;             {print $1, $2, $3}}} \
      END  {print "--- " cluster, "average:", (sum/host_cnt)"%"}'


  rm $tempfile1 $tempfile2
