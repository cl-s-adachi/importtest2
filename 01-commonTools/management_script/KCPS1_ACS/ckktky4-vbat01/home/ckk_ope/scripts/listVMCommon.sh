#!/bin/bash

# Copyright(c) 2012 CREATIONLINE,INC. All Right Reserved.
# version 1.1
# 2012/07/26 y-arai	update systemVM hostname logic

# please set your host
# arai-san's environement
#address="http://ckktky4-vclweb01:8080"
#address="http://ckktky4-vclweb02:8080"
#address="http://ckktky4-vclweb11:8080"
#address="http://ckktky4-vclweb11:8080"
#address="http://ckktky4-vclweb11:8080"
address="http://172.22.132.7:8080"


# please set your api key
# this is api@ROOT@vclweb
api_key="Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA"
# please set your secret key
# this is api@ROOT@vclweb
secret_key="aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw"


# export LC_ALL='C'

api_path="/client/api?"

tempfile1=temp1
tempfile2=temp2
touch $tempfile1
chmod 666 $tempfile1
touch $tempfile2
chmod 666 $tempfile2

# display help
if [ $# -ne 1 ]; then
  echo -e "Invalid option. please specify list option"
  echo -e "usage: $0 [VM|SystemVM]"
  exit 1
fi

#
# usage: exeute_command command=... paramter=... parameter=...
# return: command output (xml format)
#
function execute_command()
{
  local api_path="/client/api?"
## echo " debug print 1 $@"

  local data_array=("$@" "apikey=${api_key}")
## echo " debug print 2 ${data_array[@]}"

  local temp1=$(echo -n ${data_array[@]} | \
    tr " " "\n" | \
    sort -fd -t'=' | \
    perl -pe's/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg'| \
    tr "A-Z" "a-z" | \
    tr "\n" "&" )
## echo " debug print 3 sorted ${temp1[@]}"

  local signature=$(echo -n ${temp1[@]})
  signature=${signature%&}
## echo " debug print 4 $signature"

  signature=$(echo -n $signature | \
    openssl sha1 -binary -hmac $secret_key | \
    openssl base64 )
## echo " debug print 5 signature=$signature"

  signature=$(echo -n $signature | \
    perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')
# echo " debug print 6 urlencoded signature=$signature"

  local url=${address}${api_path}$(echo -n $@ | tr " " "&")"&"apikey=$api_key"&"signature=$signature
## echo " SEND URL: $url"

  curl -s ${url} | xmllint --format -
}


#
# usage: read_dom < xml file
# return: raw strings of entity and content
#
function read_dom()
{
  local IFS=\>
  read -d \< entity content
}


#
# usage: pickup_entity entity < entyty file created by read_dom function
# return: raw content string of the indicated entity
#
function pickup_entity()
{
  # check dom elements
  while read_dom
  do
    if [[ $entity = ${1} ]]
    then
      echo $content
      exit
    fi
  done
}


#
# usage: check_null string 
# return: N/A string id specieid string is null or empty
#
function check_null()
{
 if [ -z "$1" ]
 then
   echo "N/A"
 else
   echo "$1"
 fi
 exit
}


declare -a id_list
declare -a name_list
declare -a publicip_list
declare -a guestipaddress_list
declare -a guestnetworkid_list
declare -a vlan_list
declare -a account_list
declare -a domain_list
declare -a zonename_list
function get_router_list()
{
  execute_command command=listRouters listall=true > $tempfile1 

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/id/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  id_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/name/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  name_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/zonename/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  zonename_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/guestnetworkid/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  guestnetworkid_list=($strings)
}



declare -a id_list
declare -a name_list
declare -a systemvmtype_list
declare -a privateip_list
declare -a linklocalip_list
declare -a publicip_list
declare -a zonename_list
declare -a hostname_list
function get_systemvm_list()
{
  execute_command command=listSystemVms  > $tempfile1 

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listsystemvmsresponse/systemvm/id/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  id_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listsystemvmsresponse/systemvm/name/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  name_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listsystemvmsresponse/systemvm/systemvmtype/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  systemvmtype_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listsystemvmsresponse/systemvm/zonename/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  zonename_list=($strings)

#  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
#    cat /listsystemvmsresponse/systemvm/hostname/text()
    #quit
#EOL
#  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
#  hostname_list=($strings)
}


declare -a id_list
declare -a name_list
declare -a displayname_list
declare -a state_list
declare -a zonename_list
declare -a domain_list
declare -a account_list
declare -a hostid_list
declare -a hostname_list
function get_vm_list()
{
  execute_command command=listVirtualMachines listall=true > $tempfile1 

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/id/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  id_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/name/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  name_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/displayname/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  displayname_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/state/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  state_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/zonename/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  zonename_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/domain/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  domain_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/account/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  account_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listvirtualmachinesresponse/virtualmachine/hostid/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  hostid_list=($strings)
}

##
case "$1" in
routers)

  get_router_list

  # echo "debug count=${#id_list[@]} ${id_list[@]}"
  # echo "debug count=${#name_list[@]} ${name_list[@]}"
  # echo "debug count=${#guestnetworkid_list[@]} ${guestnetworkid_list[@]}"

  #
  # delimiter of each line is tab
  #
  echo -e "virtualrouter_uuid\tvirtualrouter_instance_name\tpublic_ip\tguest_ipaddress\tguestnetworkid_uuid\tvlan\taccount_name\tdomain_name\tzone_name"

  for ((i=0; i<${#id_list[@]}; i++))
  do

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity publicip)
    publicip_list[$i]=$(check_null $str)
    
    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity guestipaddress)
    guestipaddress_list[$i]=$(check_null $str)

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity account)
    account_list[$i]=$(check_null $str)

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity domain)
    domain_list[$i]=$(check_null $str)

    str=$(execute_command command=listNetworks listall=true id=${guestnetworkid_list[$i]} | \
    pickup_entity vlan)
    vlan_list[$i]=$(check_null $str)

    str=$(execute_command command=listNetworks listall=true id=${guestnetworkid_list[$i]} | \
    pickup_entity domain)
    domain_list[$i]=$(check_null $str)

    echo -e "${id_list[$i]}\t${name_list[$i]}\t${publicip_list[$i]}\t${guestipaddress_list[$i]}\t${guestnetworkid_list[$i]}\t${vlan_list[$i]}\t${account_list[$i]}\t${domain_list[$i]}\t${zonename_list[$i]}"
  done
  
  rm $tempfile1 $tempfile2
  ;;


SystemVM)
  get_systemvm_list

  # echo "debug count=${#id_list[@]} ${id_list[@]}"
  # echo "debug count=${#name_list[@]} ${name_list[@]}"

  #
  # delimiter of each line is tab
  #
  echo -e "systemvm_uuid\tsystemvm_instance_name\tsystemvm_type\tprivate_ip\tlinklocal_ip\tpublic_ip\tzone\thostname"

  for ((i=0; i<${#id_list[@]}; i++))
  do

    str=$(execute_command command=listSystemVms id=${id_list[$i]} | \
    pickup_entity privateip)
    privateip_list[$i]=$(check_null $str)

    str=$(execute_command command=listSystemVms id=${id_list[$i]} | \
    pickup_entity linklocalip)
    linklocalip_list[$i]=$(check_null $str)

    str=$(execute_command command=listSystemVms id=${id_list[$i]} | \
    pickup_entity publicip)
    publicip_list[$i]=$(check_null $str)

    str=$(execute_command command=listSystemVms id=${id_list[$i]} | \
    pickup_entity hostname)
    hostname_list[$i]=$(check_null $str)

    echo -e "${id_list[$i]}\t${name_list[$i]}\t${systemvmtype_list[$i]}\t${privateip_list[$i]}\t${linklocalip_list[$i]}\t${publicip_list[$i]}\t${zonename_list[$i]}\t${hostname_list[$i]}"
  done
  rm $tempfile1 $tempfile2
  ;;


VM)
  get_vm_list
  #
  # delimiter of each line is tab
  #
  echo -e "tvm_displayname\t\tstate\taccount\tdomain\tzone\thostname"

  for ((i=0; i<${#id_list[@]}; i++))
  do

    if [ ${hostid_list[$i]} != 0 ]; then
      hostname_list[$i]=$(execute_command command=listHosts id=${hostid_list[$i]} | \
      pickup_entity name)
    else
      hostname_list[$i]="[N/A]\t"
    fi
    echo -e "${displayname_list[$i]}\t\t${state_list[$i]}\t${account_list[$i]}\t${domain_list[$i]}\t${zonename_list[$i]}\t${hostname_list[$i]}"
  done
  rm $tempfile1 $tempfile2
  ;;

*)
  execute_command command=listRouters listall=true
  execute_command command=listSystemVms
  echo -e "\n\nInvalid option. please specify list option"
  echo -e "usage: $0 [VM|SystemVM]"
  ;;

esac
