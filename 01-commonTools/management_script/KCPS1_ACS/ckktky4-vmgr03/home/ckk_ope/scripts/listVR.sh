#!/bin/bash

# Copyright(c) 2012 CREATIONLINE,INC. All Right Reserved.

# please set your host
# arai-san's environement
#address="http://ckktky4-vclweb01:8080"
#address="http://ckktky4-vclweb02:8080"
#address="http://ckktky4-vclweb11:8080"
address="http://172.22.132.7:8080"


# please set your api key
# this is api@ROOT@vclweb
api_key="Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA"

# please set your secret key
# this is api@ROOT@vclweb
secret_key="aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw"

# export LC_ALL='C'

api_path="/client/api?"

tempfile1=temp1
tempfile2=temp2
touch $tempfile1
chmod 666 $tempfile1
touch $tempfile2
chmod 666 $tempfile2

#
# usage: exeute_command command=... paramter=... parameter=...
# return: command output (xml format)
#
function execute_command()
{
  local api_path="/client/api?"
## echo " debug print 1 $@"

  local data_array=("$@" "apikey=${api_key}")
## echo " debug print 2 ${data_array[@]}"

  local temp1=$(echo -n ${data_array[@]} | \
    tr " " "\n" | \
    sort -fd -t'=' | \
    perl -pe's/([^-_.~A-Za-z0-9=\s])/sprintf("%%%02X", ord($1))/seg'| \
    tr "A-Z" "a-z" | \
    tr "\n" "&" )
## echo " debug print 3 sorted ${temp1[@]}"

  local signature=$(echo -n ${temp1[@]})
  signature=${signature%&}
## echo " debug print 4 $signature"

  signature=$(echo -n $signature | \
    openssl sha1 -binary -hmac $secret_key | \
    openssl base64 )
## echo " debug print 5 signature=$signature"

  signature=$(echo -n $signature | \
    perl -pe's/([^-_.~A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg')
# echo " debug print 6 urlencoded signature=$signature"

  local url=${address}${api_path}$(echo -n $@ | tr " " "&")"&"apikey=$api_key"&"signature=$signature
## echo " SEND URL: $url"

  curl -s ${url} | xmllint --format -
}


#
# usage: read_dom < xml file
# return: raw strings of entity and content
#
function read_dom()
{
  local IFS=\>
  read -d \< entity content
}


#
# usage: pickup_entity entity < entyty file created by read_dom function
# return: raw content string of the indicated entity
#
function pickup_entity()
{
  # check dom elements
  while read_dom
  do
    if [[ $entity = ${1} ]]
    then
      echo $content
      exit
    fi
  done
}


#
# usage: check_null string 
# return: N/A string id specieid string is null or empty
#
function check_null()
{
 if [ -z "$1" ]
 then
   echo "[N\A]\t"
 else
   echo "$1"
 fi
 exit
}


declare -a id_list
declare -a name_list
declare -a publicip_list
declare -a guestipaddress_list
declare -a guestnetworkid_list
declare -a vlan_list
declare -a account_list
declare -a domain_list
declare -a hostname_list
function get_router_list()
{
  execute_command command=listRouters listall=true > $tempfile1 

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/id/text()
    quit
EOL

  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  id_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/name/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  name_list=($strings)

  xmllint --shell $tempfile1 2>/dev/null > $tempfile2 <<EOL
    cat /listroutersresponse/router/guestnetworkid/text()
    quit
EOL
  strings=$(cat $tempfile2 | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  guestnetworkid_list=($strings)
}


##
  get_router_list

#  echo "debug count=${#id_list[@]} ${id_list[@]}"
#  echo "debug count=${#name_list[@]} ${name_list[@]}"
#  echo "debug count=${#guestnetworkid_list[@]} ${guestnetworkid_list[@]}"
  #
  # delitter of each line is tab
  #
  echo -e "virtualrouter\tpublic_ip\tguest_ipaddress\tvlan\taccount_name\tdomain_name\thostname"

  for ((i=0; i<${#id_list[@]}; i++))
  do

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity publicip)
    publicip_list[$i]=$(check_null $str)
    
    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity guestipaddress)
    guestipaddress_list[$i]=$(check_null $str)

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity account)
    account_list[$i]=$(check_null $str)

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity domain)
    domain_list[$i]=$(check_null $str)

    str=$(execute_command command=listNetworks listall=true id=${guestnetworkid_list[$i]} | \
    pickup_entity vlan)
    vlan_list[$i]=$(check_null $str)

    str=$(execute_command command=listNetworks listall=true id=${guestnetworkid_list[$i]} | \
    pickup_entity domain)
    domain_list[$i]=$(check_null $str)

    str=$(execute_command command=listRouters listall=true id=${id_list[$i]} | \
    pickup_entity hostname)
    hostname_list[$i]=$(check_null $str)

    echo -e "${name_list[$i]}\t${publicip_list[$i]}\t${guestipaddress_list[$i]}\t${vlan_list[$i]}\t${account_list[$i]}\t${domain_list[$i]}\t${hostname_list[$i]}"
  done
  
rm $tempfile1 $tempfile2
