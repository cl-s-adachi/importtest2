#!/bin/bash

# Copyright(c) 2012 CREATIONLINE,INC. All Right Reserved.

# please set your host
#address="http://ckktky4-vclweb01:8080"
#address="http://ckktky4-vclweb11:8080"
address="http://172.22.132.7:8080"

# please set your api key
# this is api@ROOT@vclweb

api_key="Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA"


# please set your secret key
# this is api@ROOT@vclweb
secret_key="aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw"


# common-functions file location
# TODO: where should common or shared files put?
# location is current directory
#
common_function_path=$(dirname $0)/common-functions
echo $common_function_path
if [ -e "$common_function_path" ]; then
  . "$common_function_path"
else
  echo -e "please put common-functions file at appropriate directory"
  exit 1
fi

# turn on/off (true/false) debug parameter.  if debug on, output debug print
#debug=true
debug=false

# export LC_ALL='C'

tempfile_base=`date '+%Y%m%d%H%M%S'`$(basename $0)
tempfile_base=${tempfile_base%.sh}
res_temp=res_command_$tempfile_base    # temporary file cloudstack response to parse
tempfile=temp2_$tempfile_base          # temporary file cloudstack response to parse

#
# pollAsyncJob function parameter
# retry:                 retry count of issuing queryAsyncJobResult
# interval:              interval time of issuing queryAsyncJobResult
# output temporary file: output temprary file from queryAsyncJobResult
#
max_retry_pollAsyncJob="3"
interval_pollAsyncJob="4"                                      # unit is second
queryAsyncJobRawXmlResponse=res_queryAsyncJob_$tempfile_base   # output file query job response



#
# the following paramters are internal use
#
# router info columns definition
router_name="0"
router_uuid="1"
router_zone_name="2"
router_zone_uuid="3"
router_domain_name="4"
router_domain_uuid="5"
router_account_name="6"
router_original_state="7"
router_current_state="8"
publicip="9"
vlan="10"
guestnetwork_name="11"
guestnetwork_uuid="12"
guestnetwork_domain_name="13"
guestnetwork_domain_uuid="14"
guestnetwork_zone_name="15"
guestnetwork_zone_uuid="16"
router_host_name="17"
router_host_uuid="18"
router_hypervisor="19"
r_async_command="20"
r_async_jobid="21"
r_column_cnt=22

# network info columns definition
n_network_name="0"
n_network_uuid="1"
n_network_state="2"
n_network_domain_name="3"
n_network_domain_uuid="4"
n_network_zone_name="5"
n_network_zone_uuid="6"
n_vlan="7"
n_current_state="8"
n_async_command="9"
n_async_jobid="10"
n_column_cnt=11


#
# usage: r_get i  entity
# return: entity string of router information
#
function r_get()
{
  eval echo -e "\$routers_${1}_${2}"
}

#
# usage: r_set i  entity value of router information
# return: None
#
function r_set()
{
  if [ -z "$1" ] || [ -z "$2" ]
  then
    echo -e "error r_set: $1 $2 $3"
    return
  fi
  if [ -n "$3" ]
  then
    eval "routers_${1}_${2}=\${3}"
  else
    eval "routers_${1}_${2}=\"\""
  fi
  # r_get $1 $2
}

#
# usage: n_get i  entity
# return: entity string of network information
#
function n_get()
{
  eval echo -e "\$networks_${1}_${2}"
}

#
# usage: n_set i  entity value of network information
# return: None
#
function n_set()
{
  if [ -z "$1" ] || [ -z "$2" ]
  then
    echo -e "error n_set: $1 $2 $3"
    return
  fi
  if [ -n "$3" ]
  then
    eval "networks_${1}_${2}=\${3}"
  else
    eval "networks_${1}_${2}=\"\""
  fi
  # n_get $1 $2
}

#
# usage: rr_get i  entity
# return: entity string of restart router information
#
function rr_get()
{
  eval echo -e "\$restart_routers_${1}_${2}"
}

#
# usage: rr_set i  entity value of restart router information
# return: None
#
function rr_set()
{
  if [ -z "$1" ] || [ -z "$2" ]
  then
    echo -e "error rr_set: $1 $2 $3"
    return
  fi
  if [ -n "$3" ]
  then
    eval "restart_routers_${1}_${2}=\${3}"
  else
    eval "restart_routers_${1}_${2}=\"\""
  fi
  # rr_get $1 $2
}
#
# usage: wait_r_asyncjobs to_status
# wait for router's async job
# ASYNC_JOB_PENDING/ASYNC_JOB_SUCCESS/ASYNC_JOB_FAILURE definition at common-functions
#
function wait_r_asyncjobs()
{
  for ((i=0; i< $r_cnt; i++)); do
    local jobid=$(r_get $i $r_async_jobid)
    if [ -n "$jobid" ]; then
      pollAsyncJob $jobid $interval_pollAsyncJob $max_retry_pollAsyncJob

      jobstatus=$(pickup_entity jobstatus < $queryAsyncJobRawXmlResponse)
      if [ "$jobstatus" == "$ASYNC_JOB_SUCCESS" ]; then
        r_set $i $r_async_command
        r_set $i $r_async_jobid
        r_set $i $router_current_state $1
        rm -f $queryAsyncJobRawXmlResponse
      elif [ "$jobstatus" = "$ASYNC_JOB_FAILURE" ]; then
        r_set $i $router_current_state "async_job_failure"
        if $debug; then echo -e "\nresponse detail of queryAsyncJobResult"; fi
        if $debug; then cat $queryAsyncJobRawXmlResponse; fi
      fi
    fi
  done
}

#
# usage: wait_n_asyncjobs to_status
# wait for network's async job
# ASYNC_JOB_PENDING/ASYNC_JOB_SUCCESS/ASYNC_JOB_FAILURE definition at common-functions
#
function wait_n_asyncjobs()
{
  for ((i=0; i< $n_cnt; i++)); do
    local jobid=$(n_get $i $n_async_jobid)
    if [ -n "$jobid" ]; then
      pollAsyncJob $jobid $interval_pollAsyncJob $max_retry_pollAsyncJob

      jobstatus=$(pickup_entity jobstatus < $queryAsyncJobRawXmlResponse)
      if [ "$jobstatus" == "$ASYNC_JOB_SUCCESS" ]; then
        n_set $i $n_async_command
        n_set $i $n_async_jobid
        n_set $i $n_current_state $1
        rm -f $queryAsyncJobRawXmlResponse
      elif [ "$jobstatus" = "$ASYNC_JOB_FAILURE" ]; then
        n_set $i $n_current_state "async_job_failure"
        if $debug; then echo -e "\nresponse detail of queryAsyncJobResult"; fi
        if $debug; then cat $queryAsyncJobRawXmlResponse; fi
      fi
    fi
  done
}

#
# usage: wait_rr_asyncjobs to_status
# wait for router's async job, target router array is created after restart network
# ASYNC_JOB_PENDING/ASYNC_JOB_SUCCESS/ASYNC_JOB_FAILURE definition at common-functions
#
function wait_rr_asyncjobs()
{
  for ((i=0; i< $rr_cnt; i++)); do
    local jobid=$(rr_get $i $r_async_jobid)
    if [ -n "$jobid" ]; then
      pollAsyncJob $jobid $interval_pollAsyncJob $max_retry_pollAsyncJob

      jobstatus=$(pickup_entity jobstatus < $queryAsyncJobRawXmlResponse)
      if [ "$jobstatus" == "$ASYNC_JOB_SUCCESS" ]; then
        rr_set $i $r_async_command
        rr_set $i $r_async_jobid
        rr_set $i $router_current_state $1
        rm -f $queryAsyncJobRawXmlResponse
      elif [ "$jobstatus" = "$ASYNC_JOB_FAILURE" ]; then
        rr_set $i $router_current_state "async_job_failure"
        if $debug; then echo -e "\nresponse detail of queryAsyncJobResult"; fi
        if $debug; then cat $queryAsyncJobRawXmlResponse; fi
      fi
    fi
  done
}

#
# usage: get_r_guestnetwork_information uuid
# return: None
#
function get_r_guestnetwork_information()
{
  # listNetworks
  execute_command command=listNetworks listall=true id=$1 > $res_temp
  str=$(pickup_entity vlan < $res_temp)
  [ -n "$str" ] && r_set $i $vlan $str

  str=$(pickup_entity name < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_name $str

  str=$(pickup_entity domainid < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_domain_uuid $str

  str=$(pickup_entity domain < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_domain_name $str

  str=$(pickup_entity zoneid < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_zone_uuid $str

  str=$(pickup_entity zonename < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_zone_name $str
}

#
# usage: get_n_guestnetwork_information uuid
# return: None
#
function get_r_network_information()
{
  # listNetworks
  execute_command command=listNetworks listall=true id=$1 > $res_temp
  str=$(pickup_entity vlan < $res_temp)
  [ -n "$str" ] && n_set $i $n_vlan $str

  str=$(pickup_entity name < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_name $str

  str=$(pickup_entity domainid < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_domain_uuid $str

  str=$(pickup_entity domain < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_domain_name $str

  str=$(pickup_entity zoneid < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_zone_uuid $str

  str=$(pickup_entity zonename < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_zone_name $str

  str=$(pickup_entity state < $res_temp)
  [ -n "$str" ] && n_set $i $n_network_state $str
}



#
# usage: $0 <virtual router instance name> <virtual router instance name> ...
##
## main procedure
##
#
# check input parameters
#
if [ $# -lt 1 ]; then
 echo "usage: $0 <virtual router instance name> <virtual router instance name> ..."
 exit
fi

#
# filter duplicated router name
#
routers=$(echo -e "${@}" | tr " " "\n" | sort | uniq | tr "\n" " ")

#
# check router status
#
declare -i r_cnt=0
declare -i i=0
for router in $routers
do
  r_set $i $router_name $router

  # listRouters
  execute_command command=listRouters listall=true name=$router > $res_temp
  str=$(pickup_entity state < $res_temp)
  # when cannot get state of router, we decide this router doesn't exist
  if [ -z "$str" ]; then
    echo -e "Not exist router instance name=$router"
    continue;
  fi
  r_set $i $router_original_state $str

  str=$(pickup_entity account < $res_temp)
  r_set $i $router_account_name $str

  str=$(pickup_entity domainid < $res_temp)
  r_set $i $router_domain_uuid $str

  str=$(pickup_entity domain < $res_temp)
  r_set $i $router_domain_name $str

  str=$(pickup_entity zoneid < $res_temp)
  r_set $i $router_zone_uuid $str

  str=$(pickup_entity zonename < $res_temp)
  r_set $i $router_zone_name $str

  str=$(pickup_entity guestnetworkid < $res_temp)
  [ -n "$str" ] && r_set $i $guestnetwork_uuid $str

  str=$(pickup_entity hostname < $res_temp)
  [ -n "$str" ] && r_set $i $router_host_name $str

  str=$(pickup_entity hostid < $res_temp)
  [ -n "$str" ] && r_set $i $router_host_uuid $str

  str=$(pickup_entity publicip < $res_temp)
  [ -n "$str" ] && r_set $i $publicip $str

  # after 2013.01.23 meeting, pubic network id shouldn't check
  # str=$(pickup_entity publicnetworkid < $res_temp)

  #
  # both router and nic chunk have id entity in listroutersresponse
  # we have to inidicate xpath in order to get router id
  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/id/text()
  quit
EOL

##################unyo_script_kenshou#####################  
#  str=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  str=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##########################################################
  
  r_set $i $router_uuid $str

  uuid=$(r_get $i $guestnetwork_uuid)
  if [ -n "$uuid" ]; then
    # listNetworks
    get_r_guestnetwork_information $uuid
  fi

  # listPublicIpAddresses
  t_vlan=$(r_get $i $vlan)
  t_publicip=$(r_get $i $publicip)
  if [ -z "$t_vlan" ] && [ -n "$t_publicip" ]; then
    if $debug; then
      echo "get guest network id from publicip of router because vlan is null"
    fi

    execute_command command=listPublicIpAddresses listall=true ipaddress=$t_publicip > $res_temp
    str=$(pickup_entity associatednetworkid < $res_temp)
    [ -n "$str" ] && r_set $i $guestnetwork_uuid $str
    uuid=$str
    if [ -n "$uuid" ]; then
      # listNetworks
      get_r_guestnetwork_information $uuid
    fi
  fi

  # if the router isn't stopped, we can get uuid
  t_host_uuid=$(r_get $i $router_host_uuid)
  # if [ -n "$t_host_uuid" ]; then
    # listHosts
  #  execute_command command=listHosts listall=true id=$t_host_uuid > $res_temp
  #  str=$(pickup_entity hypervisor < $res_temp)
  #  r_set $i $router_hypervisor $str
  # else
  #   echo -e "we cannot decide hypervisor of instance name=$(r_get $i $router_name) state=$(r_get $i $router_original_state) account=$(r_get $i $router_account_name) domain=$(r_get $i $router_domain_name) zone=$(r_get $i $router_zone_name)"
  #  echo -e "is this router stopped?"
  # fi

  echo -e "Send Stop command to instance name=$router"
  echo -e "    uuid         =$(r_get $i $router_uuid)"
  echo -e "    state        =$(r_get $i $router_original_state)"

  # send stop command to virtual router
  if [ $(r_get $i $router_original_state) == "Running" ]; then

    execute_command command=stopRouter id=$(r_get $i $router_uuid) > $res_temp
    errcode=$(pickup_entity errorcode < $res_temp)
    errtext=$(pickup_entity errortext < $res_temp)
    if [ -n "$errcode" ] || [ -n "$errtext" ]; then
      echo "stopRouter is failed: error code[$errcode] error text[$errtext]"
      echo -e "exit this script. bye..."
      exit 1
    else
      jobid=$(pickup_entity jobid < $res_temp)
      r_set $i $r_async_command "stopRouter"
      r_set $i $r_async_jobid    $jobid
      r_set $i $router_current_state "Stopping"
      echo -e "Sent Stop command to instance name=$router"
    fi
  elif [ $(r_get $i $router_original_state) == "Stopped" ]; then
    echo -e "already Stopped the router: instance name=$router"
    r_set $i $router_current_state "Stopped"
  elif [ $(r_get $i $router_original_state) == "Stopping" ]; then
    echo -e "already Stopping the router: instance name=$router"
    r_set $i $router_current_state "Stopping"
  else
    echo -e "No Expected status of the routes: instance name=$router"
    echo -e "skip this router..."
    continue
  fi
  echo -e "    uuid         =$(r_get $i $router_uuid)"
  echo -e "    state        =$(r_get $i $router_original_state)"
  echo -e "    current state=$(r_get $i $router_current_state)"

  ((r_cnt++))
  ((i++))
done


#
# check router count
#
if [ $r_cnt -le 0 ]; then
  echo -e "\ndon't exist indicated routers\n"
  echo -e "exit this script. bye..."
  exit 1
fi

#
# wait for finishing async job of stopRouter
#
echo -e "\nwait for finishing async job of stopRouter\n"
while true; do
  all_done=true
  wait_r_asyncjobs "Stopped"
  for ((i=0; i< $r_cnt; i++)); do
    if [ $(r_get $i $router_current_state) == "async_job_failure" ]; then
      echo -e "\n"
      echo -e "Failure stopRouter command"
      echo -e "    name         =$(r_get $i $router_name)"
      echo -e "    uuid         =$(r_get $i $router_uuid)"
      echo -e "exit this script. bye..."
      exit 1
    elif [ $(r_get $i $router_current_state) == "Stopping" ]; then
      all_done=false
      continue
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done

echo -e "\ncheck current status of all virtual router\n"
for router in "$@"; do
  # listRouters
  execute_command command=listRouters listall=true name=$router > $res_temp
  state=$(cat $res_temp | pickup_entity state)
  echo -e  "instance name=$router state=$state\n"
done

#
# destroy virtual router
#
# time_of_destroyRouter=$(date +%s)
time_of_destroyRouter=$(date +%Y%m%d%H%M%S)
for ((i=0; i< $r_cnt; i++)); do
  echo -e "Send destroy command to instance name=$(r_get $i $router_name)"
  echo -e "    uuid         =$(r_get $i $router_uuid)"
  echo -e "    current state=$(r_get $i $router_current_state)"

  if [ $(r_get $i $router_current_state) == "Stopped" ]; then

    execute_command command=destroyRouter id=$(r_get $i $router_uuid) > $res_temp
    # for test
    # send stopRouter behalf of destroyRouter
    # execute_command command=stopRouter id=$(r_get $i $router_uuid) > $res_temp
    errcode=$(pickup_entity errorcode < $res_temp)
    errtext=$(pickup_entity errortext < $res_temp)
    if [ -n "$errcode" ] || [ -n "$errtext" ]; then
      echo "destroyRouter is failed: error code[$errcode] error text[$errtext]"
      echo -e "exit this script. bye..."
      exit 1
    else
      jobid=$(pickup_entity jobid < $res_temp)
      r_set $i $r_async_command "destroyRouter"
      r_set $i $r_async_jobid    $jobid
      r_set $i $router_current_state "Destroying"
      echo -e "Sent destroy command to instance name=$(r_get $i $router_name)"
    fi
  else
    echo "Non accepted state of router to send destroy command"
    r_set $i $router_current_state $(r_get $i $router_original_state)

  fi
  echo -e "    uuid         =$(r_get $i $router_uuid)"
  echo -e "    current state=$(r_get $i $router_current_state)"
done


#
# wait for finishing async job of destroyRouter
#
echo -e "\nwait for finishing async job of destroyRouter\n"
while true; do
  all_done=true
  wait_r_asyncjobs "Destroyed"
  for ((i=0; i< $r_cnt; i++)); do
    if [ $(r_get ${i} $router_current_state) == "async_job_failure" ]; then
      echo -e "\n"
      echo -e "Failure destroyRouter command"
      echo -e "    name         =$(r_get $i $router_name)"
      echo -e "    uuid         =$(r_get $i $router_uuid)"
      echo -e "exit this script. bye..."
      exit 1
    elif [ $(r_get ${i} $router_current_state) == "Destroying" ]; then
      all_done=false
      continue
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done


if $debug; then
  for ((i=0; i< $r_cnt; i++)); do
    for ((j=0; j< $r_column_cnt; j++)); do
      echo "routers [${i}][${j}] = $(r_get ${i} ${j})"
    done
  done
fi


#
# filter duplicated network id
#
declare -a network_uuids
for ((i=0; i< $r_cnt; i++)); do
  network_uuids+=($(r_get $i $guestnetwork_uuid))
done
network_uuids=$(echo -e "${network_uuids[@]}" | tr " " "\n" | sort | uniq | tr "\n" " ")

#
# restart network
#
declare -i n_cnt=0
i=0
for network_uuid in $network_uuids
do
  n_set $i $n_network_uuid $network_uuid

  # listNetwork
  get_r_network_information $network_uuid

  echo -e "Send restart command to network"
  echo -e "    uuid         =$network_uuid"
  echo -e "    name         =$(n_get $i $n_network_name)"
  echo -e "    domain       =$(n_get $i $n_network_domain_name)"
  echo -e "    zone         =$(n_get $i $n_network_zone_name)"
  echo -e "    state        =$(n_get $i $n_network_state)"

  if [ $(n_get $i $n_network_state) == "Setup" ] || \
     [ $(n_get $i $n_network_state) == "Implemented" ]
  then
    # restartNetwork
    execute_command command=restartNetwork id=$network_uuid cleanup=false > $res_temp
    errcode=$(pickup_entity errorcode < $res_temp)
    errtext=$(pickup_entity errortext < $res_temp)
    if [ -n "$errcode" ] || [ -n "$errtext" ]; then
      echo "restartNetwork is failed: error code[$errcode] error text[$errtext]"
      echo -e "exit this script. bye..."
      exit 1
    else
      jobid=$(pickup_entity jobid < $res_temp)
      n_set $i $n_async_command "restartNetwork"
      n_set $i $n_async_jobid    $jobid
      n_set $i $n_current_state  "Restarting"
      echo -e "Sent restart command to network"
      echo -e "    uuid         =$network_uuid"
      echo -e "    name         =$(n_get $i $n_network_name)"
      echo -e "    domain       =$(n_get $i $n_network_domain_name)"
      echo -e "    zone         =$(n_get $i $n_network_zone_name)"
    fi
  else
    echo -e "No Expected status of the network: instance name=$(n_get $i $n_network_name)"
    echo -e "skip this network..."
    continue
  fi
  ((n_cnt++))
  ((i++))
done

if $debug; then
  for ((i=0; i< $n_cnt; i++)); do
    for ((j=0; j< $n_column_cnt; j++)); do
      echo "networks [${i}][${j}] = $(n_get ${i} ${j})"
    done
  done
fi

#
# wait for finishing async job of restartNetwork
#
echo -e "\nwait for finishing async job of restartNetwork\n"
while true; do
  all_done=true
  wait_n_asyncjobs "Restarted"
  for ((i=0; i< $n_cnt; i++)); do
    if [ $(n_get $i $n_current_state) == "async_job_failure" ]; then
      echo -e "\n"
      echo -e "Failure restartNetwork command"
      echo -e "    uuid         =$(n_get $i $n_network_uuid)"
      echo -e "    name         =$(n_get $i $n_network_name)"
      echo -e "    domain       =$(n_get $i $n_network_domain_name)"
      echo -e "    zone         =$(n_get $i $n_network_zone_name)"
      # echo -e "exit this script. bye..."
      # exit 1
    elif [ $(n_get ${i} $n_current_state) == "Restarting" ]; then
      all_done=false
      continue
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done

#
# collect restart target virtual routers
#
declare -i rr_cnt
rr_cnt=0

for ((i=0; i< $n_cnt; i++)); do

  # listRouters
  # execute_command command=listRouters listall=true vlan=$(n_get $i $n_vlan) zoneid=$(n_get $i $n_network_zone_uuid) > $res_temp
  execute_command command=listRouters listall=true networkid=$(n_get $i $n_network_uuid) > $res_temp
  cnt=$(pickup_entity count < $res_temp)

  # we expect the only one router return with vlan and zone parameter
  # however, regarding VMware environment, possibly return plural routers even when narrows with (vlan and zone) or (networkid)
  # so, needed loop procedure for plural return of listRouters.
  # The CURRENT SCOPE is skipping stop/start router when listRouters return one more router.

  # when cannot get count of list router response, we decide this router doesn't exist
  if [ -z "$cnt" ]; then
    echo -e "Not exist router instance on networkid=$(n_get $i $n_network_uuid)"
    echo -e "    uuid         =$(n_get $i $n_network_uuid)"
    echo -e "    name         =$(n_get $i $n_network_name)"
    echo -e "    vlan         =$(n_get $i $n_vlan)"
    echo -e "    zone         =$(n_get $i $n_network_zone_name)"
    continue
  fi
  if [[ $cnt -gt 1 ]]; then
    echo -e "listRouters returns plural return; router count=$cnt"
  fi

  # the following xml parse is the countermeasure against plural return of listRouters command.
  # is it redundant?
  declare -a router_uuid_list
  declare -a router_name_list
  declare -a router_state_list
  declare -a account_name_list
  declare -a domain_uuid_list
  declare -a domain_name_list
  declare -a zone_uuid_list
  declare -a zone_name_list
  declare -a host_name_list
  declare -a host_uuid_list
  declare -a hypervisor_list
  declare -a created_list

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/id/text()
  quit
EOL

##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################

  router_uuid_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/name/text()
  quit
EOL
##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")

##############################################
  router_name_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/state/text()
  quit
EOL
##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  router_state_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/account/text()
  quit
EOL

##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  account_name_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/domainid/text()
  quit
EOL

##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  domain_uuid_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/domain/text()
  quit
EOL
##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  domain_name_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/zoneid/text()
  quit
EOL
##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  zone_uuid_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/zonename/text()
  quit
EOL
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
  zone_name_list=($strings)

  xmllint --shell $res_temp 2>/dev/null > $tempfile <<EOL
  cat /listroutersresponse/router/created/text()
  quit
EOL
##############unyo_script_kenshou#############
#  strings=$(cat $tempfile | sed -n /[0-9a-zA-z]/p | tr "\n" " ")
  strings=$(cat $tempfile | sed -n /[0-9a-zA-Z]/p | tr "\n" " ")
##############################################
  created_list=($strings)

  # get host information
  host_name_list[$j]="N/A"
  host_uuid_list[$j]="N/A"
  hypervisor_list[$j]="N/A"
  for ((j=0; j<$cnt; j++))
  do
    if [ "${router_state_list[$j]}" == "Stopped" ] || \
       [ "${router_state_list[$j]}" == "Stopping" ]
    then
      # echo -e "we cannot decide hypervisor because of router state is stopped"
      if $debug; then
        echo -e "    instance name=${router_name_list[$j]}"
        echo -e "    uuid         =${router_uuid_list[$j]}"
        echo -e "    state        =${router_state_list[$j]}"
        echo -e "    account      =${account_name_list[$j]}"
        echo -e "    domain       =${domain_name_list[$j]}"
        echo -e "    zone         =${zone_name_list[$j]}"
        echo -e "is this router stopped?"
      fi
      # according to current schema, don't skip stopped router after restart network
      # continue

    else
      # when the router is runing state
      # get host information of the router

      # listRouters
      execute_command command=listRouters listall=true id=${router_uuid_list[$j]} > $res_temp
      str=$(pickup_entity hostname < $res_temp)
      [ -n "$str" ] && host_name_list[$j]=$str

      str=$(pickup_entity hostid < $res_temp)
      [ -n "$str" ] && host_uuid_list[$j]=$str

      # if [ -n "$str" ]; then
        # listHosts
      #   execute_command command=listHosts listall=true id=$str > $res_temp
      #   str=$(pickup_entity hypervisor < $res_temp)
      #   [ -n "$str" ] && hypervisor_list[$j]=$str
      # fi

    fi

    echo -e "fetched router from network uuid"
    echo -e "    network uuid =$(n_get $i $n_network_uuid)"
    echo -e "    instance name=${router_name_list[$j]}"
    echo -e "    uuid         =${router_uuid_list[$j]}"
    echo -e "    state        =${router_state_list[$j]}"
    echo -e "    account      =${account_name_list[$j]}"
    echo -e "    domain       =${domain_name_list[$j]}"
    echo -e "    zone         =${zone_name_list[$j]}"
    echo -e "    host name    =${host_uuid_list[$j]}"
    echo -e "    host uuid    =${host_name_list[$j]}"
    # echo -e "    hypervisor   =${hypervisor_list[$j]}"

    # date commands is verious edition. So, compare simple text
    # t_created_date=$(date -f %Y-%m-%dT%H:%M:%S ${created_list[$j]} +%s)
    t_created_date=$(echo ${created_list[$j]} | tr -d "-"  | tr -d "T" | tr -d ":"  | cut -d "+" -f 1)
    echo -e "    destroy date string        =$time_of_destroyRouter"
    echo -e "    router created date string =$t_created_date"

    if [[ $t_created_date -gt $time_of_destroyRouter ]]; then
      rr_set $rr_cnt $router_uuid              ${router_uuid_list[$j]}
      rr_set $rr_cnt $router_name              ${router_name_list[$j]}
      rr_set $rr_cnt $router_original_state    ${router_state_list[$j]}
      rr_set $rr_cnt $router_account_name      ${account_name_list[$j]}
      rr_set $rr_cnt $router_domain_name       ${domain_name_list[$j]}
      rr_set $rr_cnt $router_domain_uuid       ${domain_uuid_list[$j]}
      rr_set $rr_cnt $router_zone_name         ${zone_name_list[$j]}
      rr_set $rr_cnt $router_zone_uuid         ${zone_uuid_list[$j]}
      rr_set $rr_cnt $guestnetwork_uuid        $(n_get $i $n_network_uuid)
      rr_set $rr_cnt $guestnetwork_name        $(n_get $i $n_network_name)
      rr_set $rr_cnt $guestnetwork_domain_name $(n_get $i $n_network_domain_uuid)
      rr_set $rr_cnt $guestnetwork_domain_uuid $(n_get $i $n_network_domain_name)
      rr_set $rr_cnt $guestnetwork_zone_name   $(n_get $i $n_network_zone_uuid)
      rr_set $rr_cnt $guestnetwork_zone_uuid   $(n_get $i $n_network_zone_name)
      if [ "${host_uuid_list[0]}" != "N/A" ]; then
        rr_set $rr_cnt $router_host_uuid  ${host_uuid_list[$j]}
        rr_set $rr_cnt $router_host_name  ${host_name_list[$j]}
        # rr_set $rr_cnt $router_hypervisor ${hypervisor_list[$j]}
      fi
      ((rr_cnt++))
    fi
  done

  if $debug; then
    echo -e "listRouters loop exit!!"
  fi

done

#
# check router count
#
if [ $rr_cnt -le 0 ]; then
  echo -e "\ndon't exist any routers that should stop and start after restart network\n"
  echo -e "exit this script. bye..."
  exit 1
fi

if $debug; then
  echo -e "routers after restarting network"
  for ((i=0; i< $rr_cnt; i++)); do
    for ((j=0; j< $r_column_cnt; j++)); do
      echo "after restart network: routers [${i}][${j}] = $(rr_get ${i} ${j})"
    done
  done
fi

#
# check state of restart target router
#
echo -e "\nwait for finishing router starting or stopping after restart network\n"
while true; do
  all_done=true
  for ((i=0; i< $rr_cnt; i++)); do
    if [ $(rr_get $i $router_original_state) == "Running" ]
    then
      rr_set $i  "Running"
    elif [ $(rr_get $i $router_original_state) == "Stopped" ]
    then
      rr_set $i $router_current_state "Stopped"
    else
      # listRouters
      execute_command command=listRouters listall=true id=$(rr_get $i $router_uuid) > $res_temp
      str=$(pickup_entity state < $res_temp)
      rr_set $i $router_current_state $str
      all_done=false
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done

#
# start virtual router
#
for ((i=0; i< $rr_cnt; i++)); do
  echo -e "Send start command to instance name=$(rr_get $i $router_name)"
  echo -e "    uuid         =$(rr_get $i $router_uuid)"
  echo -e "    current state=$(rr_get $i $router_current_state)"

  if [ $(rr_get $i $router_current_state) == "Stopped" ]
  then

    # startRouter
    execute_command command=startRouter id=$(rr_get $i $router_uuid) > $res_temp
    errcode=$(pickup_entity errorcode < $res_temp)
    errtext=$(pickup_entity errortext < $res_temp)
    if [ -n "$errcode" ] || [ -n "$errtext" ]; then
      echo "startRouter is failed: error code[$errcode] error text[$errtext]"
      # echo -e "exit this script. bye..."
      # exit 1
    else
      jobid=$(pickup_entity jobid < $res_temp)
      rr_set $i $r_async_command      "startRouter"
      rr_set $i $r_async_jobid        $jobid
      rr_set $i $router_current_state "Starting"
      echo -e "Sent start command to instance name=$(rr_get $i $router_name)"
    fi
  else
    echo "Non accepted state of router to send start command"
    rr_set $i $router_current_state $(rr_get $i $router_original_state)

  fi
  echo -e "    uuid         =$(rr_get $i $router_uuid)"
  echo -e "    current state=$(rr_get $i $router_current_state)"
done


#
# wait for finishing async job of startRouter
#
echo -e "\nwait for finishing async job of startRouter\n"
while true; do
  all_done=true
  wait_rr_asyncjobs "Running"
  for ((i=0; i< $rr_cnt; i++)); do
    if [ $(rr_get $i $router_current_state) == "async_job_failure" ] &&
       [ $(rr_get $i $r_async_command)      == "startRouter" ] &&
       [ -n $(rr_get $i $r_async_job) ]
    then
      echo -e "\n"
      echo -e "Failure startRouter command"
      echo -e "    name         =$(rr_get $i $router_name)"
      echo -e "    uuid         =$(rr_get $i $router_uuid)"
      # echo -e "exit this script. bye..."
      # exit 1
    elif [ $(rr_get $i $router_current_state) == "Starting" ]; then
      all_done=false
      continue
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done

#
# reboot virtual router
#
for ((i=0; i< $rr_cnt; i++)); do
  echo -e "Send reboot command to instance name=$(rr_get $i $router_name)"
  echo -e "    uuid         =$(rr_get $i $router_uuid)"
  echo -e "    current state=$(rr_get $i $router_current_state)"

  if [ $(rr_get $i $router_current_state) == "Running" ]
  then
     if [ $(rr_get $i $router_original_state) == "Stopped" ] ||
        [ $(rr_get $i $router_original_state) == "Stopping" ]
     then

        # rebootRouter
        execute_command command=rebootRouter id=$(rr_get $i $router_uuid) > $res_temp
        errcode=$(pickup_entity errorcode < $res_temp)
        errtext=$(pickup_entity errortext < $res_temp)
        if [ -n "$errcode" ] || [ -n "$errtext" ]; then
          echo "startRouter is failed: error code[$errcode] error text[$errtext]"
          # echo -e "exit this script. bye..."
          # exit 1
        else
          jobid=$(pickup_entity jobid < $res_temp)
          rr_set $i $r_async_command      "rebootRouter"
          rr_set $i $r_async_jobid        $jobid
          rr_set $i $router_current_state "Rebooting"
          echo -e "Sent reboot command to instance name=$(rr_get $i $router_name)"
      fi
    fi
  else
    echo "Non accepted state of router to send reboot command"
    rr_set $i $router_current_state $(rr_get $i $router_original_state)

  fi
  echo -e "    uuid         =$(rr_get $i $router_uuid)"
  echo -e "    current state=$(rr_get $i $router_current_state)"
done


#
# wait for finishing async job of startRouter
#
echo -e "\nwait for finishing async job of startRouter\n"
while true; do
  all_done=true
  wait_rr_asyncjobs "Running"
  for ((i=0; i< $rr_cnt; i++)); do
    if [ $(rr_get $i $router_current_state) == "async_job_failure" ] &&
       [ $(rr_get $i $r_async_command)      == "rebootRouter" ] &&
       [ -n $(rr_get $i $r_async_job) ]
    then
      echo -e "\n"
      echo -e "Failure rebootRouter command"
      echo -e "    name         =$(rr_get $i $router_name)"
      echo -e "    uuid         =$(rr_get $i $router_uuid)"
      # echo -e "exit this script. bye..."
      # exit 1
    elif [ $(rr_get $i $router_current_state) == "Rebooting" ]; then
      all_done=false
      continue
    fi
  done
  if [ $all_done == true ]; then echo -e "all done...\n"; break; fi
done


echo -e "\ncheck current status of all virtual router\n"
for ((i=0; i< $rr_cnt; i++)); do
  # listRouters
  execute_command command=listRouters listall=true name=$(rr_get $i $router_name) > $res_temp
  state=$(cat $res_temp | pickup_entity state)
    echo -e  "instance name=$(rr_get $i $router_name)  state=$state"
  if [ $(rr_get $i $router_current_state) == "async_job_failure" ]; then
    echo -e  "    command failed; command=$(rr_get $i $r_async_command)"
  fi
  echo -e "\n"
done

echo -e "\ncompleted all processes!!"
echo -e "  stop indicated routers"
echo -e "  destroy indicated routers"
echo -e "  restart networks"
echo -e "  start routers linked with restarted network when the router state is stopped"
echo -e "  reboot routers linked with restarted network when the router state is stopped"

#
# remove temporay files
#
if [ -e "$queryAsyncJobRawXmlResponse" ]; then rm -f $queryAsyncJobRawXmlResponse; fi
rm -f $res_temp $tempfile

