#!/bin/bash

printUsage(){
	echo "Usage: $0 -v [ uuid of destination vm] -h [uuid of destination host]" 
	exit 1
}

execCommandToHost(){
       	expect -c "
		# login to host
		set timeout 5
		spawn ssh -l root ${DEST_HOST} 
		expect \"Are you sure you want to continue connecting (yes/no)?\" {
			send \"Yes\n\"

			expect \"password:\" {
				send \"Admin123ckk!\n\"
			}
		}

		expect \"password:\" {
			send \"Admin123ckk!\n\"
		}

		# check virsh entry
		expect \"root@*pbhpv* ~]# \" {
			send \"${COMMAND}\n\"
		}

		# exit
		expect \"root@*pbhpv* ~]# \" {
			send \"exit \n\"
		}
	"
}

while getopts v: OPT
do
	case ${OPT} in
                v ) VMID=${OPTARG}
                   ;;
		\? ) printUsage ;;
	esac
done

if [ -z ${VMID} ] ; then
	printUsage
fi

echo "Getting information of target from DB.."

DBHOST=`./getDBInfo.sh StandbyName`
HYPERVISOR=`mysql -u root cloud -h ${DBHOST} -e "select hypervisor_type from vm_instance where uuid='${VMID}'\G" | tail -1 | awk '{print $2}'`

DEST_HOST=`mysql -u root cloud -h ${DBHOST} -N -e "select host.name from vm_instance,host where vm_instance.uuid='${VMID}' and vm_instance.host_id=host.id;" | awk -F\. '{print $1}'` 
INSTANCE_NAME=`mysql -u root cloud -h ${DBHOST} -N -e "select instance_name from vm_instance where uuid='${VMID}'"`

#MIGRATE_SPEED=8796093022207
##20180813 This value was changed to 1024MiB from 8796093022207 MiB
#MIGRATE_SPEED=1024
##20181030 This value was changed to 128MiB from 1024 MiB
MIGRATE_SPEED=128

echo "---------INFORMATIOIN-----------"
echo "Hypervisor type:  ${HYPERVISOR}" 
echo "Destination host: ${DEST_HOST}"
echo "--------------------------------"
echo 

if [ ${HYPERVISOR} != "KVM" ]; then
	echo "END: Destination host is not a KVM. Exit0"
	exit 0
fi

echo "Destination host is KVM host. set migrate speed to ..."
COMMAND="virsh migrate-setspeed ${INSTANCE_NAME} ${MIGRATE_SPEED}"
execCommandToHost > /dev/null
