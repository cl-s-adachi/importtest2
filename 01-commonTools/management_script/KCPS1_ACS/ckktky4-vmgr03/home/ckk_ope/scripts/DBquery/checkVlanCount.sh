#!/bin/sh

BASEDIR=`dirname $0`
#DBHOST=`${BASEDIR}/getDBInfo.sh StandbyName`
DBHOST=ckktky4-pcldb10
TMPDIR="${BASEDIR}/tmp"
TMPFILE="${TMPDIR}/vlancount"

mkdir -p ${TMPDIR}

INFO_LV="[INFO]"
WARN_LV="[INFO]"
TAG="[CloudStack]"
SCRIPT="`basename $0`"
THRESHOLD=4

logger -t "${INFO_LV} ${TAG}" "Start ${SCRIPT}"

mysql -u root cloud -h ${DBHOST} -N -e 'select networks.id,vlan.vlan_id from networks,user_ip_address,vlan where networks.removed is null and networks.name="PublicFrontSegment" and networks.id=user_ip_address.network_id and user_ip_address.vlan_db_id=vlan.id order by  networks.domain_id;' | sort -u | awk '{print $1}' | uniq -cd | sort > ${TMPFILE}

while read line
do
 # item[0] vlan count
 # item[1] network id
 item=(${line})
 if [ ${THRESHOLD} -le ${item[0]} ] ;then
#   logger -t "${WARN_LV} ${TAG}" "The number of VLANs in this network is large.:domain/${item[1]},network/${item[2]},VLAN Count/${item[0]}"
   DomainName=`mysql -u root cloud -h ${DBHOST} -N -e "select domain.name from networks,domain where networks.domain_id=domain.id and networks.id=${item[1]}"`
   echo "${WARN_LV} ${TAG}" "The number of VLANs in this network is large.:domain/${DomainName},network/${item[1]},VLANs/${item[0]}"
 fi
done < ${TMPFILE}

logger -t "${INFO_LV} ${TAG}" "End ${SCRIPT}"

