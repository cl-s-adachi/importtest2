#################################
"kloud-cloudstack files" repository readme.

[history]
date:2015/12/18 author: toyama comment:Added the explanation of project.
date:2013/09/13 author: staira comment:Added branch and tags rules.
date：2013/09/12 author: t-yamaguchi comment:new created
#################################

■ブランチ・タグルール（重要）
○ブランチ
商用提供（リリース）されたものは、そのタイミングで必ずブランチをつけること。
ブランチの目的は、どの資産が、いつ、商用適用(顧客リリース)されているかを把握するため。
ブランチ名には、必ずrevision番号をつけること。
また、いくつかのtoolsが格納されているため、どのtoolsが、何のために提供されたのかが分かるような名称にすること。
以下は、isorokuのブランチ名(参考)
branch_tamaRelease_jenkins8783_rev1348_dbSchema1.9

一般的に使うブランチとはちょっと異なる部分がありますが、商用リリース後にその断面の資産に対して、更新が発生する可能性が
否定できないため、リリース単位でブランチをきります。

○タグ
原則使用不可とします。

○参考
リリース・ブランチに関する参考URL。
http://www.nulab.co.jp/kousei/chapter2/06.html#ブランチ

■ディレクトリ階層　説明

○ files/..
「作業時に必要となるスクリプト以外のファイルを保管する。　例：システムVMテンプレートファイルなど」
「【files/..】配下は用途別にフォルダを作成し、ファイルは用途別に配置すること。」

○ commonTools/
汎用的に使えそうな便利なツール群配置

 - Capistrano/..
   Capistranoで作成/更新したスクリプト・定義ファイル置き場。

 - kvm_host_data_cleaningno/..
   KVMホストのデータクリーニング用資材置き場

○ projectTools/
プロジェクト固有のツール群配置

 - bcp/..
   bcpプロジェクトで作成したスクリプト置き場。
      ただし、遠山さん作成分のスクリプトは除く。
      (2017/7/26)
      ACS4.9対応に伴い改修が必要となった為、別リポジトリで管理するように変更。
      リポジトリ「kcps_bcp」で管理。


 - ClouStack-GUI-masking-patch/..
      「CSのGUIマスキング用」スクリプトを保管する。」

 - cleanup/..
      KCPS2環境のクリーンアップスクリプト群
      [see wiki ](https://bitbucket.org/creationline/kloud-cloudstack-files/wiki/cleanup%20scripts)