#!/bin/bash
# Copyright(c) 2015 CREATIONLINE,INC. All Right Reserved.
#
# This script processes each specified host one-by-one, looking for VMs
# that have not yet had the VENOM patch applied (un-venomed VMs).  These
# un-venomed VMs are the stopped+started via the CloudStack API one-by-one.
# All successfully restarted VMs are outputted to an output file,
# and all restart failures are outputted to another output file
# (output files are saved to timestamped output directory with each run).
#
# usage:
# $ ./restartUnvenomedVms.sh [-h|--allHosts] [-v|-allVms] <host.lst>
#
# The <host.list> is the list of host to be processed.  1 host per line,
# each host must be reachable via ssh directly.  Comments lines allowed.
# The -h | --allHosts option can used to auto-confirm processing of all hosts.
# The -h | --allVms option can used to auto-confirm processing of all VMs.
#
# requirements:
# - this script must be located on an AP server
# - the root & password variables below must be set to values that
#   can be used to log into all the target hosts from this AP server
# - the cs-script.conf & common-functions library files must be
#   in the same directory as this script to run
# - the api_key & secret_key variables in cs-script.conf must
#   be set to values that allow admin access to the CloudStack
#   API on this AP server




###
# flags
###
preconfirmHostProcessing=true #true: asks user before processing host; false: batch-processes all hosts
preconfirmVmProcessing=true #true: asks user before processing vm; false: batch-processes all vm

###
# host login parameters
###
root="root"
password="Admin123ckk!"

###
# temporary files
###
rawXmlResponse=temp_apiResponseBuffer  #used to save intermediate results between operations
parsedXmlResponse=temp_parseBuffer     #should be automatically deleted once script is done

#####
# Returns timestamp for now in barest format, suitable for dir names
# usage:  timestamp_dir
# return: current date+time in "YYYYMMDDHHMMSS" format
#####
function timestamp_dir() {
  date +"%Y%m%d%H%M%S"
}

###
# internal
###
scriptVer="1.2"
scriptName=`basename $0`
logDir="logs"
logFile="$logDir/restartUnvenomedVms.log"  #script log is concat-ed to with each run
outputDir="output/$(timestamp_dir)"        #new timestamped output dir created with each run
vmRestartSuccessListFile="$outputDir/vmRestartSuccessList.txt"  #file for each run written to timestamped dir
vmRestartFailureListFile="$outputDir/vmRestartFailureList.txt"  #file for each run written to timestamped dir
defaultSessionTimeout="120"

###
# cmdln arguments
###
hostFile=''

###
## "constants" used throughout the script
###
RUNNING='Running'  #vm state




#####
# Outputs given string to cli and a timestamped+b/w version to logFile
# usage:  print <string to output>
# return: none
#####
function print()
{
  #set IFS to non-space char so multi-spaces will not be trimmed
  IFS='^'
  #output string to both stdout and log
  #(output to log is timestamped and stripped of any terminal colors)
  echo -e $1
  echo -e "$(timestamp): $1" | removeTerminalColorCodes | removeTerminalRestCodes >> $logFile
  #reset IFS back to space to not interfere with normal functioning
  unset IFS
}

#####
# Returns timestamp for now
# usage:  timestamp
# return: current date+time in "YYYY-MM-DD_HH:MM:SS" format
#####
function timestamp() {
  date +"%Y-%m-%d_%H:%M:%S"
}

#####
# Creates the log directory if it does not already exist.
# usage:  makeLogDirIfNotExists
# return: none
#####
function makeLogDirIfNotExists() {
  mkdir -p $logDir
}

#####
# Creates the output directory if it does not already exist.
# usage:  makeOutputDirIfNotExists
# return: none
#####
function makeOutputDirIfNotExists() {
  mkdir -p $outputDir
}

#####
# Initializes output files $vmRestartSuccessListFile & $vmRestartFailureListFile
# with comment headers describing the data that the file will contain.
# usage:  writeOutputFileHeaders
# return: none
# output: writes comment headers to each output file
#####
function writeOutputFileHeaders() {
  echo -e "# <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp> <restartEndTimestamp>" > $vmRestartSuccessListFile
  echo -e "# <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp> <restartFailTimestamp>" > $vmRestartFailureListFile
}


#####
# Returns contents of specified file with all comments (lines starting with '#') removed
# usage:  readFileRemovingComments <filename>
# return: string of contents of file, excluding all comments
#####
function readFileRemovingComments() {
  cat $1 | removeComments | replaceNewlinesWithSpaces
}

#####
# Strips out all comments (lines starting with '#') from input string.
# Meant to be used as part of piped command.
# usage:  ... | removeComments
# return: input string with all comments removed
#####
function removeComments() {
  sed '/^#/ d'
}

#####
# Replace all newline characteres in input string with space characters.
# Meant to be used as part of piped command.
# usage:  ... | replaceNewlinesWithSpaces
# return: input string with all newlines changed to spaces
#####
function replaceNewlinesWithSpaces() {
  tr '\n' ' '
}

#####
# Strips all non-ascii characters from input string.
# Meant to be used as part of piped command.
# usage:  ... | removeNonAsciiChars
# return: input string with all non-ascii characters removed
#####
function removeNonAsciiChars() {
  tr -cd '[:print:]'
}

#####
# Strips out all terminal color codes from input string.
# Meant to be used as part of piped command.
# [http://www.commandlinefu.com/commands/view/3584/remove-color-codes-special-characters-with-sed]
# usage:  ... | removeTerminalColorCodes
# return: input string with all terminal color codes removed
#####
function removeTerminalColorCodes() {
  sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
}

#####
# Strips out all terminal reset codes from input string.
# Meant to be used as part of piped command.
# usage:  ... | removeTerminalRestCodes
# return: input string with all terminal reset codes removed
#####
function removeTerminalRestCodes() {
  sed -r "s/\x1B\(B//g"
}

#####
# Strips $endOfOutputSym from the end of input string
# Meant to be used as part of piped command.
# usage:  ... | removeEndOfOutputMarker
# return: input string with any ending $endOfOutputSym removed
#####
function removeEndOfOutputMarker() {
  sed "s/$endOfOutputSym$//"
}


#####
# Returns array of name of VMs on specified host that have not applied the venom patch.
# This list is created by ssh-ing into the specified host and directly querying
# information for VMs on the host using virsh.
# usage:  listUnVenomedVmsForHost <hostname/url/ip>
# return: string that is space-delimited list of names of un-venomed VMs on the host
#
# The cmd conglomerate defined by $listUnvenomedVmsCmd is a huge command starting with
# 'virsh list' and piped through many filters to end up with a space-delimited list
# of names of VMs that have not have the venom patch applied yet.  Each part of the
# cmd conglomerate works as follows:
#
#         for vm in...done : Original KDDI virsh cmd that outputs all VMs on the host,
#                            one line of data per VM, with non-venomed VMs postfixed
#                            with a '(deleted)' string.
#         grep '(deleted)' : Extract only the non-venomed VM lines.
#        awk '{print \$1}' : Extract only the VM names from each line.
#                     sort : Sort the VM names.
#    awk '{print}' ORS=' ' : Change all line breaks into spaces.
#   echo '$endOfOutputSym' : Mark end of output with special marker recognized by script.
#
# The only difference between the $listUnvenomedVmsCmd cmd below and what you would type
# into a terminal are the escaping of special characters to make the string work in
# this script and the use of the $endOfOutputSym var defined by this script.
#####
rootPromptOfHosts="*\# "
endOfOutputSym='END_OF_OUTPUT'
listUnvenomedVmsCmd="for vm in \`virsh list | awk '{print \$2}' |egrep ^i-\`; do echo -n \"\${vm} \" ; lsof -p \`ps ax |egrep \${vm} | egrep -v egrep |awk '{print \$1}'\` | egrep '/usr/libexec/qemu-kvm'; done | grep '(deleted)' | awk '{print \$1}' | sort | awk '{print}' ORS=' '; echo '$endOfOutputSym'"
function listUnVenomedVmsForHost() {
  local host=$1
  local rawResult=$(expect -c '
    #exp_internal 1  #expect debug flag
    log_user 0
    #set timeout $defaultSessionTimeout

    #ssh into host as root
    spawn ssh -q -o StrictHostKeyChecking=no '"$root@$host"'
    expect {
      timeout { send_user {'"\n$host> ...Failed to get password prompt\n"'}; exit 1 }
      eof { send_user {'"\n$host> ...SSH failure for $host\n"'}; exit 1 }
      '"*assword: "'
    }
    send '"$password\r"'
    expect {
      timeout { send_user {'"\n$host> ...Login failed. Password incorrect.\n"'}; exit 1 }
      '"$rootPromptOfHosts"'
    }
    
    #input cmd conglomerate that will output non-venom-ed vms for this host & exec
    send { '"$listUnvenomedVmsCmd"' }
    send '"\r"'
    expect {
      timeout { send_user {'"\n$host> ...Virsh command timed out.\n"'}; exit 1 }
      #look for the end of the inputted cmd
      { '"*echo '$endOfOutputSym'"' }
    }
    #set expect parser to line immediately after inputted cmd
    expect '"\r\n"$'

    #wait for all output; use $endOfOutputSym to identify the end of the cmd output
    expect '"*$endOfOutputSym\r\n"$'
    
    #print the cmd output from the host so this script can save it to var
    puts $expect_out(0,string)

    send { '"exit\r"' }
  ')

  #clean up the raw output of the cmd and return space-delimited list of vm names
  echo $rawResult | removeNonAsciiChars | removeEndOfOutputMarker
}

#####
# Returns true if parameter is any recognized variation of yes; false otherwise
# usage:  isYes <string>
# return: true if specified string is an recognized variation of yes; false otherwise
#####
function isYes() {
  [ $1 == "yes" ] || [ $1 == "Yes" ] || [ $1 == "y" ] || [ $1 == "Y" ]
}

#####
# Returns true if parameter is any recognized variation of exit; false otherwise
# usage:  isExit <string>
# return: true if specified string is an recognized variation of exit; false otherwise
#####
function isExit() {
  [ $1 == "exit" ] || [ $1 == "Exit" ] || [ $1 == "e" ] || [ $1 == "E" ]
}


#####
# Sends listVirtualMachines request to cs api, filtering by specified keyword
# usage:  getVmWithKeyword <vmName>
# return: raw xml response from cs
# output: raw xml response from cs to rawXmlResponse file
#####
function getVmWithKeyword() {
  local vmName=$1
  execute_command command=listVirtualMachines keyword=${vmName} listall=true > $rawXmlResponse

  echo ${rawXmlResponse}
}

#####
# Sends stopVirtualMachine request to cs api with specified vm id value
# usage:  stopVmWithId <vmId>
# return: raw xml response from cs
# output: raw xml response from cs to rawXmlResponse file
#####
function stopVmWithId()
{
  local vmUuid=$1
  execute_command command=stopVirtualMachine id=${vmUuid} > $rawXmlResponse
  
  echo ${rawXmlResponse}
}

#####
# Sends startVirtualMachine request to cs api with specified vm id value
# usage:  startVmWithId <vmId>
# return: raw xml response from cs
# output: raw xml response from cs to rawXmlResponse file
#####
function startVmWithId()
{
  local vmUuid=$1
  execute_command command=startVirtualMachine id=${vmUuid} > $rawXmlResponse
  
  echo ${rawXmlResponse}
}


#####
# Returns number of VMs listed in $vmRestartSuccessListFile file
# usage:  numSuccesses
# return: number of VMs in $vmRestartSuccessListFile file
#####
function numSuccesses() {
  cat $vmRestartSuccessListFile | removeComments | wc -l
}

#####
# Returns number of VMs listed in $vmRestartFailureListFile file
# usage:  numFailures
# return: number of VMs in $vmRestartFailureListFile file
#####
function numFailures() {
  cat $vmRestartFailureListFile | removeComments | wc -l
}


#####
# Writes the specified vm name/displayname/uuid/restartBeginTimestamp to the $vmRestartSuccessListFile file.
# The restartEndTimestamp is assumed to be the moment this function is called.
# The $vmRestartSuccessListFile output file is assumed to have the following format
#
# <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp> <restartEndTimestamp>
#
# with one line per successfully restarted VM.
# usage:  recordRestartSuccess <vmId> <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp>
# return: none
# output: single line for successfully restarted VM to $vmRestartSuccessListFile file
#####
function recordRestartSuccess()
{
  #assume the moment this function was called as the end timestamp
  local restartEndTimestamp=$(timestamp)
  
  local vm=$1
  local vmDisplayName=$2
  local vmUuid=$3
  local restartBeginTimestamp=$4

  echo -e "$vm $vmDisplayName $vmUuid $restartBeginTimestamp $restartEndTimestamp" >> $vmRestartSuccessListFile
}

#####
# Writes the specified vm name/displayname/uuid/restartBeginTimestamp to the $vmRestartFailureListFile file.
# The restartFailTimestamp is assumed to be the moment this function is called.
# The $vmRestartFailureListFile output file is assumed to have the following format
#
# <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp> <restartFailTimestamp>
#
# with one line per restart VM failure.
# usage:  recordRestartFailure <vmId> <vmName> <vmDisplayName> <vmUuid> <restartBeginTimestamp>
# return: none
# output: single line for failed VM restart to $vmRestartFailureListFile file
#####
function recordRestartFailure()
{
  #assume the moment this function was called as the failure timestamp
  local restartFailTimestamp=$(timestamp)
  
  local vm=$1
  local vmDisplayName=$2
  local vmUuid=$3
  local restartBeginTimestamp=$4

  echo -e "$vm $vmDisplayName $vmUuid $restartBeginTimestamp $restartFailTimestamp" >> $vmRestartFailureListFile
}








#############################################
##
## main procedure
##
#############################################
print "Running $scriptName ($scriptVer)"


## Setup ---------------------------------------------------------------

#check for required conf include file
csScriptConfFile="cs-script.conf"
if [ -f "${csScriptConfFile}" ]; then
  source ${csScriptConfFile}
else
  print "error: $scriptName requires the following support files in the same directory to function: ${csScriptConfFile}"
  print "       Aborting script run.\n\n"
  exit 1
fi

#process all cmdln options and parameter
while [ "$#" -gt 0 ]; do
  case "$1" in
    -h|--allHosts) preconfirmHostProcessing=false; shift 1;;
    -v|--allVms)   preconfirmVmProcessing=false;   shift 1;;
    
    -vh) preconfirmHostProcessing=false; preconfirmVmProcessing=false; shift 1;;
    -hv) preconfirmHostProcessing=false; preconfirmVmProcessing=false; shift 1;;

    -*) print "error: Unknown option: $1"; print "       Aborting script run.\n\n" >&2; exit 1;;
    *)  hostFile=${hostFile:-$1}; shift 1;;  #the neat ${:-} construction makes sure only
  esac                                       #the first param is treated as the hostFile
done
if ! $preconfirmHostProcessing ; then
  print " - batch processing all Hosts"
fi
if ! $preconfirmVmProcessing ; then
  print " - batch processing all VMs"
fi

print ""

#check required cmdln param
if [ -z $hostFile ]; then
  print "error: Host file argument required."
  print "       Aborting script run.\n\n"
  exit 1
elif [ ! -f $hostFile ]; then
  print "error: Specified host file could not be found: $hostFile"
  print "       Valid host file argument required."
  print "       Aborting script run.\n\n"
  exit 1
fi

#script-related initializations
makeLogDirIfNotExists
makeOutputDirIfNotExists
writeOutputFileHeaders
  
#read host list from file
hostList=$(readFileRemovingComments $hostFile)
print "Host data read from: $hostFile"
print "Will check the following hosts for un-venom-ed VMs:"
print "[$hostList]"


## Process Hosts+VMs -------------------------------------------------------

#process each host in order, examining/start+stopping VMs on each host as needed
for host in $hostList;
do
  print ""
  
  #confirm host processing with user if necessary
  selection='y'  #default to batch processing all hosts
  if $preconfirmHostProcessing; then
    print "$host> Do you want to process host $host? (y)es (n)o (e)xit"
    read selection
  fi
  
  #at each host, there is the option to: y) process it, n) skip it, e) force exit script
  if $(isYes $selection); then
    #ssh into host & get un-venomed vm names
    print "$host> SSH-ing into host..."
    unVenomedVmList=$(listUnVenomedVmsForHost $host)
    print "$host> ...Retrieved VM data from host."
    print "$host> Found the following un-venomed VMs:"
    print "$host> [$unVenomedVmList]"
  elif $(isExit $selection); then
    print "exit: Aborting script run and exiting."
    print "      Un-venom-ed VMs may still exist.\n\n"
    exit 1
  else
    print "$host> Skipping host $host."
    continue
  fi

  ## Process VMs on host ------------------------------------------------

  #output friendly message for hosts with no un-venomed VMs
  if [[ -z $unVenomedVmList ]]; then
    print "$host> No VMs need to be restarted on this host."
  fi

  #once we have the un-venomed VM list for the host, we can stop+start each VM
  for vm in $unVenomedVmList;
  do
    print "$host>"

    #find uuid (and other attributes) for VM
    print "$host> $vm> Querying VM information..."
    getVmResponse=$(getVmWithKeyword $vm)
    vmUuid=$(pickup_entity id < $getVmResponse)
    vmState=$(pickup_entity state < $getVmResponse)
    vmDisplayName=$(pickup_entity displayname < $getVmResponse)
    
    #double-check VM still exists (guards against the rare case the
    #VM was deleted between the time of the virsh list cmd and here)
    if [ -z $vmUuid ]; then
      print "$host> $vm> ...Current VM information not found; skip this VM."
      continue
    fi
    
    #output most current VM info
    print "$host> $vm> ...Retrieved VM from CloudStack:"
    print "$host> $vm> ${txtbld}$vmDisplayName${txtrst} ($vmUuid) $vmState"
    
    #double-check the VM is still running
    if [ $vmState == $RUNNING ]; then    
      #confirm vm processing with user if necessary
      selection='y'  #default to batch processing all hosts
      if $preconfirmVmProcessing; then
        print "$host> $vm> Do you want to restart VM $vm? (y)es (n)o (e)xit"
        read selection
      fi
    
      #at each host, there is the option to: y) process it, n) skip it, e) force exit script
      if $(isYes $selection); then
        #initiate VM stop...
        print "$host> $vm> Stopping $vmDisplayName..."
        restartBeginTimestamp=$(timestamp)
        stopVmWithIdResponse=$(stopVmWithId $vmUuid)
          #check for api errors
          checkForAndExitOnCsApiError $stopVmWithIdResponse
        #...get stopVM job id...
        jobId=$(pickup_entity jobid < $stopVmWithIdResponse)
        print "$host> $vm>   job in progress (jobId=$jobId)"
        #...and wait for VM to stop
        pollAsyncJobToCompletion $jobId
        jobStatus=$(pickup_entity jobstatus < ${queryAsyncJobRawXmlResponse})
        if [ $jobStatus = $ASYNC_JOB_SUCCESS ]; then
          print "$host> $vm> ...Stop vm result: OK"
        else
          print "$host> $vm> ...Stop vm result: ${bldred}FAILED${txtrst}"
          recordRestartFailure $vm $vmDisplayName $vmUuid $restartBeginTimestamp
          print "$host> $vm> Skip start for this VM."
          continue  #skip start op for this VM and proceed to next VM
        fi
        
        #initiate VM start...
        print "$host> $vm> Starting $vmDisplayName..."
        startVmWithIdResponse=$(startVmWithId $vmUuid)
          #check for api errors
          checkForAndExitOnCsApiError $startVmWithIdResponse
        #...get startVM job id...
        jobId=$(pickup_entity jobid < $startVmWithIdResponse)
        print "$host> $vm>   job in progress (jobId=$jobId)"
        #...and wait for VM to start
        pollAsyncJobToCompletion $jobId
        jobStatus=$(pickup_entity jobstatus < ${queryAsyncJobRawXmlResponse})
        if [ $jobStatus = $ASYNC_JOB_SUCCESS ]; then
          print "$host> $vm> ...${txtbld}Start vm result: ${bldgrn}OK${txtrst}"
          recordRestartSuccess $vm $vmDisplayName $vmUuid $restartBeginTimestamp
        else
          print "$host> $vm> ...${txtbld}Start vm result: ${bldred}FAILED${txtrst}"
          recordRestartFailure $vm $vmDisplayName $vmUuid $restartBeginTimestamp
          continue
        fi
      elif $(isExit $selection); then
        print "exit: Aborting script run and exiting."
        print "      Un-venom-ed VMs may still exist.\n\n"
        exit 1
      else
        print "$host> $vm> Skip restart for VM $vm."
        continue
      fi  #if $(isYes $selection)
    else
      print "$host> $vm> Skip non-running VM $vmDisplayName." 
      continue
    fi  #if [ $vmState == $RUNNING ]
  done  #for vm in $unVenomedVmList;
done  #for host in $hostList;


# Cleanup ---------------------------------------------------------------

#delete any temp files we created as part of processing
rm -f $rawXmlResponse $queryAsyncJobRawXmlResponse $parsedXmlResponse


# Done! ------------------------------------------------------------------
print ""
print "##################################"
print ""
print "$scriptName has completed ${bldblu}successfully${txtrst}."
print "Successfully restarted VMs listed in file: $vmRestartSuccessListFile ($(numSuccesses) VMs)"
print "VM restart fails have been listed in file: $vmRestartFailureListFile ($(numFailures) VMs)"
print "Thank you for your cooperation citizen."
print ""
print "##################################\n\n"
