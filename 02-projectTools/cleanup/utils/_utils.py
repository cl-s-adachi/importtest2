#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
small utility functions
"""

import datetime, json, logging, sys, time
import iso8601

logger = logging.getLogger(__name__)


def check_privilege_of_keys(api):
    """
    api: cloudstack object
    """
    try:
        api.listSystemVms({"page": "1", "pagesize": "1"})
    except Exception:
        # maybe, keys don't have ROOT admin user privilege,
        #  the following error occures
        # urllib2.HTTPError: HTTP Error 432:
        sys.exit("""
            ERROR!!: Cannot connect to cloudstack with authentication keys.
            Please check api/secret key. ROOT admin user's keys are required
        """)
    return


def wait_async_job(api, description, jobid):
    """ wait for finishing async job

    Args:
        description(str): the api name for success or failure message.
        jobid(str): job id.
    """
    while True:
        res = api.queryAsyncJobResult({"jobid": jobid})
        # job has succeeded
        if res["jobstatus"] == 1:
            print("""
                {0} async job done: jobid {1}.
            """.format(description, res["jobid"]))
            logger.debug("return queryAsyncJobResult")
            logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))
            break
        # job has failed
        elif res["jobstatus"] == 2:
            print("""
                {0} async job failed: jobid {1}.
                {2}
            """.format(description, res["jobid"], json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
            logger.debug("return queryAsyncJobResult")
            logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))
            return None
        # job is still pending
        elif res["jobstatus"] == 0:
            print("""
                {0} async job pending: jobid {1}
            """.format(description, res["jobid"]))
            logger.debug("return queryAsyncJobResult")
            logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))
            time.sleep(5)

    return res


def check_count_value(res):
    """ check count value of list api response

    Args:
        res(json): cloudstack api response
    Returns:
        None: if count is zero
        res: otherwise
    """
    if "count" not in res or res["count"] <= 0:
        return None
    else:
        return res


def sizeof_format(number, suffix='B'):
    for unit in ["" "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(number) < 1024:
            return "%3d%s%s" % (number, unit, suffix)
        number /= 1024
    return "%3d%s%s" % (number, "Yi", suffix)


def check_created_date(created_str, days):
    """
    created of cloudstack object format is 2015-12-08T21:48:16+0900
    check if the days past

    Returns:
        True if older than days ago
        False if less than days ago
    """
    created = iso8601.parse_date(created_str)
    created = created.replace(tzinfo=None)
    now = datetime.datetime.now()
    gap = now - created
    if gap.days > days:
        return True
    else:
        return False


def decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode("utf-8")
        elif isinstance(item, list):
            item = decode_list(item)
        elif isinstance(item, dict):
            item = decode_dict(item)
        rv.append(item)
    return rv


def decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode("utf-8")
        if isinstance(value, unicode):
            value = value.encode("utf-8")
        elif isinstance(value, list):
            value = decode_list(value)
        elif isinstance(value, dict):
            value = decode_dict(value)
        rv[key] = value
    return rv
