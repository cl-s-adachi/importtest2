#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
manageme output filename, google spreadsheet title
"""

import datetime, logging, os
import iso8601

logger = logging.getLogger(__name__)


class OutputFilenameGenerator(object):
    def __init__(self, site):
        """initializes 
        Args:
            site(str): site string such as "jp2-east" or "jp2-west"
        """
        self.site = site
        self.time = datetime.datetime.now()

    def generate_objects_filename(self, objects_file_dir, filename=None):
        """
        generates file name
        file format is date_<site>_<cyclic number[0-9]>.txt

        Args:
            object_file_dir(str): directory the objects file stored.
            filename(str): filename is specfied, this method do nothing.
        Returns:
            filename(str): filename to store the cloudstack objects list
        """
        # set default filename that is dateformat + .txt
        if not filename:
            if not os.path.isdir(objects_file_dir):
                print("objects file directroy doesn't exist.")
                print("create diretory {0}".format(objects_file_dir))
                os.mkdir(objects_file_dir)

            prefix = self.time
            filename = prefix.strftime("%Y%m%d%H%M%S%f")
            filename += "_"
            filename += self.site
            filename += "_"

            # check sufffix number for a new filename
            cyclic = "0"
            if os.listdir(objects_file_dir):
                prev_generation = sorted(os.listdir(objects_file_dir))

                # file format date_site_cyclic number[0-9].txt
                number = prev_generation[-1].split("_")[-1].split(".")[0]
                if number is not None and number.isdigit():
                    number = int(number)
                    if number < 9: cyclic = str(number+1)
                if len(prev_generation) > 9:
                    try:
                        os.remove(os.path.join(objects_file_dir, prev_generation[0]))
                    except:
                        print("""
                            ERROR!! cannot delete the oldest file {0}.
                        """.format(prev_generation[0]))

            filename += cyclic
            filename += ".txt"

        filename = os.path.join(objects_file_dir, filename)

        return filename

    def generate_worksheet_title(self):
        """
         edit worksheet title
         worksheet title format "site %Y%m%d %H:%M:%S"

        Returns:
            title(str): worksheet tilte string
        """
        return "{0} {1}".format(self.site, self.time.strftime("%Y%m%d %H:%M:%S"))

    def get_oldest_worksheet(self, worksheets):
        """analyzes the worksheet titles and return the latest worksheet
        Args:
           worksheet(list): list of GoogleWorksheet
        Returns:
           worksheet(GoogleWorksheet): the oldest worksheet
        """
        list_of_dictionary = []
        for worksheet in worksheets:
            # worksheet title format site date(%Y%m%d %H:%M:%S)
            # see, generate_worksheet_title() method
            l = worksheet.title.split()
            d = {}
            d["created"] = iso8601.parse_date(" ".join(l[1:]))
            d["created"] = d["created"].replace(tzinfo=None)
            d["worksheet"] = worksheet
            list_of_dictionary.append(d)
        s = sorted(list_of_dictionary, key=lambda k: k["created"], reverse=False)
        if s:
            d = s[0]
            return d["worksheet"]
        else:
            return None
