#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Using gspread library instead of Google gdata
some methods i want are on testing. and gdata isn't easy to use!!
  gdata cannot handles a blank row.
  gdata doesn't provide deleting worksheetn function.
  gdata doesn't read the first row by get_line_feed method.
  gdata believes the first row is column defenition.
  gdata believes the first column is row defenition.
  when need all empty cells you are no choice excpet CellQuery.
  etc,...

  Gspread is very very slow when insert rows.

access to Google Spreadsheet with service account authentication.
the following parameters in properties file;
    service account
    credential file name

"""
import logging, string
import gspread
from oauth2client.client import SignedJwtAssertionCredentials

logger = logging.getLogger(__name__)


class GoogleSpreadsheetClient(object):
    """authorizes google drive"""
    def __init__(self, account, credential):
        """authorizes google drive
        Args:
            account(str): google service account that have read/write permission to the spreadsheet.
            credential(str): credential file that is downloadd by google developer console.
        """
        self.client = None

        with open(credential) as f:
            private_key = f.read()

            # for scopes, see https://developers.google.com/drive/web/scopes
            self.credentials = SignedJwtAssertionCredentials(
                  service_account_name = account,
                  private_key          = private_key,
                  scope                = ["https://spreadsheets.google.com/feeds"])

            self.client = gspread.authorize(self.credentials)

    def get_client(self):
        return self.client

class GoogleWorksheet(object):
    def __init__(self, worksheet):
        self.row_counter = 1
        self.worksheet = worksheet

        self.range_format = "A{0}:{1}{0}"

    def add_row(self, values):
        """add a row.

        Args:
           values(list of str): cell values of a row
        """
        if self.row_counter > self.worksheet.row_count:
            logger.error("Reached the max row. cannot write row no more")
            raise IOError("Reached the max row. cannot write row no more")

        f = self.range_format.format(
                self.row_counter,
                string.uppercase[self.worksheet.col_count-1])

        cell_list = self.worksheet.range(f)

        for i, value in enumerate(values):
            cell_list[i].value = value  
        self.worksheet.update_cells(cell_list)
        self.row_counter += 1


if __name__ == "__main__":
    import sys, traceback
    from Properties import Properties

    logging.basicConfig(stream=sys.stderr, level=logging.ERROR)
    logger.setLevel(logging.DEBUG)

    properties = Properties("jp2-west")

    client = GoogleSpreadsheetClient(properties.google["service_account"], properties.google["credential_file"]).get_client()
    spreadsheet = client.open("cleanupTest")
    worksheets = client.open("cleanupTest").worksheets()
    print(worksheets)
