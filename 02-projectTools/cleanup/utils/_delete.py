#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
deletes virtual machine, volume, snapshot, template.

when delete virtualmachines, checks the attachedvolumes
 and attached cvolume at the same time.
"""

# if true, enable debug logging
DEBUG = False
SKIP_DELETE = False

import json, logging, time
import _utils

logger = logging.getLogger(__name__)


def delete_virtualmachine(api, uuid):
    """
    remove virtual machine and relevant objects
    """

    volumes = api.listVolumes(
        {"listall": "true", "virtualmachineid": uuid}
    )
    logger.debug("return listVolumes")
    logger.debug(json.dumps(volumes, sort_keys=True, indent=4, ensure_ascii=False))

    if _utils.check_count_value(volumes) is None:
        print("""
            Not found the virtual machine {0}.
        """.format(uuid))
        return

    # not yet care the following api for the specified virtual machine.
    # all linked objects automatically are removed when the virutal machine is removed.
    # then, empty PortForwardingRule or LoadBalancerRule remains.
    #   deletePortForwardingRule
    #   removeFromLoadBalancerRule

    vm = _destroy_virtualMachine(api, uuid)

    for volume in volumes["volume"]:
        if volume["type"] == "ROOT":
            continue

        logger.debug("delete target volume")
        logger.debug(json.dumps(volume, sort_keys=True, indent=4, ensure_ascii=False))

        print("delete attached volume {0} of vm {1}.".format(volume["name"], vm["name"]))
        i = 0
        while i < 20:
            res = delete_volume(
                api,
                volume["id"]
            )
            if "success" in res and res["success"] == "true":
                break
            print("retry deleting attached volume {0} of vm {1}.".format(volume["name"], vm["name"]))
            time.sleep(30)
            i += 1
    return


def _destroy_virtualMachine(api, uuid):
    if SKIP_DELETE:
        print("return empty because skip destroyVirtualMachine API")
        return {}

    res = api.destroyVirtualMachine({"id": uuid})
    logger.debug("return destroyVirtualMachine")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if "errorcode" in res:
        print("""
            destroyVirtualMachine returns error
            {0}
        """.format(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
        return res

    vm = _utils.wait_async_job(api, "destroyVirtualMachine", res["jobid"])
    if vm is None:
        # destroyVirtualMachine failed. Nothing to do next.
        return

    vm = vm["jobresult"]["virtualmachine"]
    logger.debug("return queryAsyncJobResult")
    logger.debug(json.dumps(vm, sort_keys=True, indent=4, ensure_ascii=False))
    print("""
        destroyed virtual machine {0} of {1} {2}.
    """.format(vm["name"], vm["account"], vm["domain"]))

    return vm


def delete_volume(api, uuid):

    if SKIP_DELETE:
        print("return empty because skip deleteVolume API")
        return {}

    res = api.deleteVolume({"id": uuid})
    logger.debug("return api.deleteVolume")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if "errorcode" in res:
        print("""
            deleteVolume returns error
            {0}
        """.format(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
        return res

    if "success" in res:
        if res["success"] == "true":
            print("""
                deleted volume {0}.
            """.format(uuid))
            return res

    print("failed deleting volume whose uuid is {0}.".format(uuid))
    print("   {0}".format(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
    return res


def delete_snapshot(api, uuid):

    if SKIP_DELETE:
        print("return empty because skip deleteSnapshot API")
        return {}

    res = api.deleteSnapshot({"id": uuid})
    logger.debug("return api.deleteSnapshot")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if "errorcode" in res:
        print("""
            deleteSnapshot returns error
            {0}
        """.format(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
        return res

    # by cloudstack api document, deleteSnapshot is sync api
    # however, response returns jobid. it means deleteSnapshot is asyn job?
    res = _utils.wait_async_job(api, "deleteSnapshot", res["jobid"])
    logger.debug("return queryAsyncJobResult")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))
    if res is None:
        # snapshot failed. Nothing to do next.
        return

    print("""
        deleted snapshot {0}.
    """.format(uuid))

    return res


def delete_template(api, uuid):

    if SKIP_DELETE:
        print("return empty because skip deleteTemplate API")
        return {}

    res = api.deleteTemplate({"id": uuid})
    logger.debug("return api.deleteTemplate")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if "errorcode" in res:
        print("""
            deleteTemplate returns error
            {0}
        """.format(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False)))
        return res

    # by cloudstack api document, deleteTemplate is sync api
    # however, response returns jobid. it means deleteTemplate is asyn job?
    res = _utils.wait_async_job(api, "deleteTemplate", res["jobid"])
    logger.debug("return queryAsyncJobResult")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))
    if res is None:
        # template failed. Nothing to do next.
        return

    print("""
        deleted template {0}.
    """.format(uuid))

    return res
