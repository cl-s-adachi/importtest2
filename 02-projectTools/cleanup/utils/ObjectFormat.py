#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
manageme cloudstack object output format.
"""


class ObjectFormat(object):
    def __init__(self):
        # for object name
        self.category = ""

        # pickup key from cloudstack object such as virtual machine, volume and so on
        self.pickup_keys = ""

        # stdout header format
        self.header = ""

        # stdout line format
        # formatter key must be cloudstack object key.
        # if cloudstackResponse["id"] is mapped to "{id:32}"
        self.line_format = ""

    def get_category(self):
        return self.category

    def get_keys(self):
        return self.pickup_keys

    def get_header(self):
        return self.header

    def get_line_format(self):
        return self.line_format


class VirtualMachineFormat(ObjectFormat):
    def __init__(self):
        self.category = "[virtual machines]"
        self.pickup_keys = [
            "zonename",
            "domain",
            "account",
            "id",
            "name",
            "state",
            "serviceofferingname",
            "templatename",
            "created"]
        self.header = [
            "zonename",
            "domain",
            "account",
            "uuid",
            "name",
            "vmstate",
            "service offering name",
            "template name",
            "created date"]
        self.line_format = "  {zonename:10} {domain:9} {account:9} {id:36} {name:32} {state:8} {serviceofferingname:12} {templatename:10} {created:25}\n"

    def get_uuid_indexOfList(self):
        return 3

    def get_uuid_indexOfColumn(self):
        return 4


class VolumeFormat(ObjectFormat):
    def __init__(self):
        self.category = "[volumes]"
        self.pickup_keys = [
            "zonename",
            "domain",
            "account",
            "id",
            "name",
            "type",
            "size",
            "created"]
        self.header = [
            "zonename",
            "domain",
            "account",
            "uuid",
            "name",
            "volume type",
            "size",
            "created"]
        self.line_format = "  {zonename:10} {domain:9} {account:9} {id:36} {name:32} {type:8} {size:6} {created:25}\n"

    def get_uuid_indexOfList(self):
        return 3

    def get_uuid_indexOfColumn(self):
        return 4


class SnapshotFormat(ObjectFormat):
    def __init__(self):
        self.category = "[snapshots]"
        self.pickup_keys = [
            "domain",
            "account",
            "id",
            "name",
            "intervaltype",
            "state",
            "created"]
        self.header = [
            "domain",
            "account",
            "uuid",
            "name",
            "interval type",
            "state",
            "created date"]
        self.line_format = "  {domain:9} {account:9} {id:36} {name:72} {intervaltype:8} {state} {created:25}\n"

    def get_uuid_indexOfList(self):
        return 2

    def get_uuid_indexOfColumn(self):
        return 3


class TemplateFormat(ObjectFormat):
    def __init__(self):
        self.category = "[templates]"
        self.pickup_keys = [
            "zonename",
            "domain",
            "account",
            "id",
            "name",
            "ostypename",
            "status",
            "size",
            "created"]
        self.header = [
            "zonename",
            "domain",
            "account",
            "uuid",
            "name",
            "os type",
            "status",
            "size",
            "created date"]
        self.line_format = "  {zonename:10} {domain:9} {account:9} {id:36} {name:32} {ostypename:38} {status:18} {size:6} {created:25}\n"

    def get_uuid_indexOfList(self):
        return 3

    def get_uuid_indexOfColumn(self):
        return 4
