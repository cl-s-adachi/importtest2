#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
the properties specidied by siet name
"""

import ConfigParser, json, os, sys, traceback
import _utils

PROPERTIES_FILE = "cleanup.properties"


class Properties(object):
    def __init__(self, site=None):

        filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), PROPERTIES_FILE)

        try:
            config = ConfigParser.ConfigParser()
            config.read(filename)

            self.objects_file_dir = config.get("objects_file_directory", "DIR")

            self.evacuate_days_ago = int(config.get("evacuate", "DAYS_AGO"))
            if self.evacuate_days_ago < 1:
                raise ConfigParser.Error("Set days ago value greater than 1")

            self.google = {}
            self.google["service_account"] = config.get("google_spreadsheet", "SERVICE_ACCOUNT")

            credential_file = os.path.join(
                 os.path.dirname(os.path.realpath(__file__)),
                 config.get("google_spreadsheet", "CREDENTIAL_FILE"))
            if not os.path.isfile(credential_file):
                raise ConfigParser.Error("Not found file {0}".format(credential_file))

            self.google["credential_file"]   = credential_file
            self.google["spreadsheet_title"] = config.get("google_spreadsheet", "TITLE")

            self.sites               = [x.strip() for x in config.get("sites", "SITES").split(",")]

            if site:
                self.site            = site
                self.url             = config.get(site, "URL")
                self.api_key         = config.get(site, "API_KEY")
                self.secret_key      = config.get(site, "SECRET_KEY")
                self.accounts        = json.loads(config.get(site, "ACCOUNTS"), object_hook=_utils.decode_dict)
                evacuees_file = os.path.join(
                    os.path.dirname(os.path.realpath(__file__)),
                    config.get(site, "EVACUEES_FILE"))
                if not os.path.isfile(evacuees_file):
                    raise ConfigParser.Error("Not found file {0}".format(evacuees_file))
                self.evacuees_file  = evacuees_file
        except Exception:
            traceback.print_exc()
            if site:
                print("""
                cannot get site information of {0}.""".format(site))
            sys.exit("""
                please check if {0} file exists or setting is correct.
            """.format(filename))
