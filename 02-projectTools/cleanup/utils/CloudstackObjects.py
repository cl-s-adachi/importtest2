#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""CloudstackObjcts

keep cloudstack objects' uuid
  for evacuees
  and
  for deleting
"""
import logging
from ObjectFormat import VirtualMachineFormat, VolumeFormat, SnapshotFormat, TemplateFormat

logger = logging.getLogger(__name__)


class CloudstackObjects(object):
    """CloudstackObjects class"""
    def __init__(self):

        self.virtualMachineFormat = VirtualMachineFormat()
        self.volumeFormat = VolumeFormat()
        self.snapshotFormat = SnapshotFormat()
        self.templateFormat = TemplateFormat()
        self.arrays = {
            self.virtualMachineFormat.get_category(): [],
            self.volumeFormat.get_category(): [],
            self.snapshotFormat.get_category(): [],
            self.templateFormat.get_category(): [],
        }

    def get_virtualMachines(self):
        """return the list of virtual machine uuid"""
        return self.arrays[self.virtualMachineFormat.get_category()]

    def get_volumes(self):
        """return the list of volume uuid"""
        return self.arrays[self.volumeFormat.get_category()]

    def get_snapshots(self):
        """return the list of snapshot uuid"""
        return self.arrays[self.snapshotFormat.get_category()]

    def get_templates(self):
        """return the list of template uuid"""
        return self.arrays[self.templateFormat.get_category()]

    def _set_values(self, readNow, header, row):
        if readNow == self.virtualMachineFormat.get_category():
            uuid =  row[self.virtualMachineFormat.get_uuid_indexOfList()]
            if uuid != "uuid":
                self.arrays[readNow].append(uuid)
        elif readNow == self.volumeFormat.get_category():
            uuid = row[self.volumeFormat.get_uuid_indexOfList()]
            if uuid != "uuid":
                self.arrays[readNow].append(uuid)
        elif readNow == self.snapshotFormat.get_category():
            uuid =  row[self.snapshotFormat.get_uuid_indexOfList()]
            if uuid != "uuid":
                self.arrays[readNow].append(uuid)
        elif readNow == self.templateFormat.get_category():
            uuid = row[self.templateFormat.get_uuid_indexOfList()]
            if uuid != "uuid":
                self.arrays[readNow].append(uuid)

        elif header == self.virtualMachineFormat.get_category():
                readNow = self.virtualMachineFormat.get_category()
        elif header == self.volumeFormat.get_category():
                readNow = self.volumeFormat.get_category()
        elif header == self.snapshotFormat.get_category():
                readNow = self.snapshotFormat.get_category()
        elif header == self.templateFormat.get_category():
                readNow = self.templateFormat.get_category()

        return readNow


class EvacuateObjects(CloudstackObjects):
    """keep evacuee objects by reading the evacuate file"""

    def __init__(self, evacuees_file):
        """fetches evacuee uuids from the specified file
        Args:
           evacuee_file: filename that described evacuees
        """
        CloudstackObjects.__init__(self)

        with open(evacuees_file, "r") as f:
            readNow = None
            for line in f.read().splitlines():

                cols = line.split()

                # blank line?
                if not cols:
                    readNow = None
                    continue

                readNow = self._set_values(readNow, line, cols)


class DeleteObjects(CloudstackObjects):
    """keep delete objects by reading the google worksheet"""

    def __init__(self, worksheet):
        CloudstackObjects.__init__(self)
        """fetches evacuee uuids from the specified worksheet
        Args:
           worksheet: Gspread.Worksheet object
        """
        logger.debug("worksheet title is {0}".format(worksheet.title))

        readNow = None
        for row in worksheet.get_all_values():
            # blank line?
            if not row:
                readNow = None
                continue
            if not row[0]:
                readNow = None
                continue

            readNow = self._set_values(readNow, row[0], row)


if __name__ == "__main__":
    import sys
    from Properties import Properties
    properties = Properties("jp2-west")

    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    logger.setLevel(logging.DEBUG)
   
    e = EvacuateObjects(properties.evacuees_file)
    print "virtual machines"
    print e.get_virtualMachines()
    print "\n"
    print "volumes"
    print e.get_volumes()
    print "\n"
    print "snapshots"
    print e.get_snapshots()
    print "\n"
    print "templates"
    print e.get_templates()
    print "\n"

    from Gspread import GoogleSpreadsheetClient

    client = GoogleSpreadsheetClient(properties.google["service_account"], properties.google["credential_file"]).get_client()
    worksheet = client.open("cleanupTest").worksheet("jp2-west 20151217 11:25:22")
    d = DeleteObjects(worksheet)
    print "delete virtual machines"
    print d.get_virtualMachines()
    print "\n"
    print "delete volumes"
    print d.get_volumes()
    print "\n"
    print "delete snapshots"
    print d.get_snapshots()
    print "\n"
    print "delete templates"
    print d.get_templates()
    print "\n"
