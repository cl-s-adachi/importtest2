#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GoogleSpreadsheet

access to Google Spreadsheet with service account authentication.
the following parameters in properties file;
    service account
    credential file name
    spreadsheet title

google spreadsheet can be controlled by illegible key or id such as "13RquqbO883TQ_OyMJAa-3zBAUos-y4zdIOGAl7m146w" or "od6"
GoogleSpreadsheet class can accesss by title of the each object.

"""
import logging
import gdata.spreadsheets.client
from oauth2client.client import SignedJwtAssertionCredentials

from ObjectFormat        import VirtualMachineFormat, VolumeFormat, SnapshotFormat, TemplateFormat

logger = logging.getLogger(__name__)

MAX_ROW = 1000

class GoogleSpreadsheetClient(object):
    """authorizes google drive"""
    def __init__(self, account, credential):
        """authorizes google drive
        Args:
            account(str): google service account that have read/write permission to the spreadsheet.
            credential(str): credential file that is downloadd by google admin console.
        """

        with open(credential) as f:
            private_key = f.read()

            # for scopes, see https://developers.google.com/drive/web/scopes
            self.credentials = SignedJwtAssertionCredentials(
                  service_account_name = account,
                  private_key          = private_key,
                  scope                = ["https://spreadsheets.google.com/feeds"])

            self.client = gdata.spreadsheets.client.SpreadsheetsClient()
            auth_token = gdata.gauth.OAuth2TokenFromCredentials(self.credentials)
            auth_token.authorize(self.client)

    def list_spreadsheets(self):
        """fetches spreadsheet's titles
        Yields:
           str: that spreadsheet title text
        """
        for entry in self.client.get_spreadsheets().entry:
            yield entry.title.text

    def get_spreadsheet(self, title):
        """fetches spreadsheet's title
        Args:
           title(str): spreadsheet title
        Returns:
           GoogleSpreadsheet: GoogleSpreadsheet object
        Raises:
           NameError: not found the spreadsheet
        """
        for entry in self.client.get_spreadsheets().entry:
            if entry.title.text == title:
                key = entry.get_id().split("/")[-1]
                logger.debug("in getSpreadsheet @ GoogleSpreadsheetClient")
                logger.debug(key)
                return GoogleSpreadsheet(title, self.client, key)

        raise NameError(title)


class GoogleSpreadsheet(object):
    """adds or reads worksheet under the GoogleSpreadsheet"""
    def __init__(self, title, client, spreadsheet_key):
        """initialize spreadsheet title, authentication info, spreadsheet key
        Args:
            title(str): google spreadsheet title
            client(GoogleSpreadsheetClient): authorized information
            spreadsheet_key(str): google spreadsheet key
        """
        self.client          = client
        self.spreadsheet_key = spreadsheet_key
        self.title           = title

    def get_worksheet(self, title):
        """fetches worksheet's title
        Args:
           title(str): worksheet title
        Returns:
           GoogleWorksheet: GoogleWorksheet object
        Raises:
           NameError: not found the worksheet
        """
        for entry in self.client.get_worksheets(self.spreadsheet_key).entry:
            if entry.title.text == title:
                worksheet_id = entry.get_worksheet_id()
                return GoogleWorksheet(title, self.client, self.spreadsheet_key, worksheet_id)
        raise NameError(title)

    def get_worksheets(self):
        """gets all worksheets under the spreadsheet.
        Returns:
           list of GoogleWorksheet object
        """
        res = []
        feed = self.client.get_worksheets(self.spreadsheet_key)
        for entry in feed.entry:
            res.append(
                GoogleWorksheet(
                    entry.title.text,
                    self.client,
                    self.spreadsheet_key,
                    entry.get_worksheet_id()))
        logger.debug(res)
        return res

    def get_worksheets_filter_by_site(self, site):
        """gets worksheet whose title including the specified site string.
        Args:
           site(str): site string such as "jp2-east" or "jp-2west"
        Returns:
           list of filtered GoogleWorksheet object
        """
        res = []
        for worksheet in self.get_worksheets():
            if site in worksheet.title:
                res.append(worksheet)
        return res

    def add_worksheet(self, title, rows=MAX_ROW, cols=None):
        """adds a worksheet.
        Args:
           title(str): worksheet title
           rows(int or str): The number of rows this worksheet should start with.
                             defaut is 1000.
           cols(int or str): The number of cols this worksheet should start with.
                             defaut is the max value of outputFormat objects' header length.
        Returns:
           GoogleWorksheet: GoogleWorksheet object
        """
        if not cols:
            cols = max([len(VirtualMachineFormat().get_header()),
                        len(VolumeFormat().get_header()),
                        len(SnapshotFormat().get_header()),
                        len(TemplateFormat().get_header())])
   
        entry = self.client.add_worksheet(self.spreadsheet_key,
                                          title,
                                          rows,
                                          cols)
        logger.debug("in add_worksheet @ GoogleSpreadsheet")
        logger.debug(self.spreadsheet_key)
        logger.debug(entry.get_worksheet_id())
        return GoogleWorksheet(title, self.client, self.spreadsheet_key, entry.get_worksheet_id())

    def delete_worksheet(self, worksheet):
        """deletes a worksheet.
        Args:
           worksheet(GoogleWorksheet): worksheet object
        """
        # entry = self.client.get_worksheet(self.spreadsheet_key, worksheet.worksheet_id)
        # entry.delete()


class GoogleWorksheet(object):
    """adds or reads rows under the GoogleWorksheet"""
    def __init__(self, title, client, spreadsheet_key, worksheet_id):
        """initialize worksheet title, authentication info, spreadsheet key, worksheet id
        Args:
            title(str): google spreadsheet title
            client(GoogleSpreadsheetClient): authorized information
            spreadsheet_key(str): google spreadsheet key
            worksheet_id(str): google worksheet id
        """
        self.client          = client
        self.spreadsheet_key = spreadsheet_key
        self.worksheet_id    = worksheet_id
        self.title           = title
        self.end_row         = 1

    def get_rows_as_list(self):
        """fetches rows.
        gdata library handles the first row as column definition.
        get_list_feed method doesn't return the first row anytime.

        Yields:
           row(list): the list of cell text in a row.
        """
        list_feed = self.client.get_list_feed(self.spreadsheet_key, self.worksheet_id)
        for entry in list_feed.entry:
            row = []
            logger.debug("=== LINE ===")
            logger.debug(entry.content)
            # logger.debug(entry.id)
            logger.debug(entry.title)
            logger.debug(entry)
            row.append(entry.title.text)
            if entry.content is not None and entry.content.text is not None:
                # sample content text is ...
                #     _cokwr: M00000001, _cpzh4: kenshou01, _cre1l: f26506f4-f6e4-40d5-96f6-aff3ff3bc176, _chk2m: cl-suto-test, _ciyn3: CentOS 6.0 (64-bit), _ckd7g: Download Complete, _clrrx: 100TiB, _cyevm: 2015-09-29T10:39:44+0900
                #
                # note: a value might include ":" such as "2015-09-29T10:39:44+0900"
                #       a value might include "," such as "Large2(4vCPU,Mem16GB)"
                #       a value might not include ", "
                for x in entry.content.text.split(", "):
                    y = x.split()
                    row.append(y[1].strip())
            logger.debug(row)
            yield row

    def get_rows_as_dictionary(self):
        """fetches rows.
        gdata library handles the first row as column definition.
        get_list_feed method doesn't return the first row anytime.

        Not used now.

        Yields:
           row(dict): the dictonary of colum name and cell text in a row.
        """
        list_feed = self.client.get_list_feed(self.spreadsheet_key, self.worksheet_id)
        for entry in list_feed.entry:
            yield entry.to_dict()

    def add_header(self, header):
        """add a row.
        Args:
           header(list of str): adds a row with header text list
                         for example,
                         ["a", "b", "c"] is inserted as A1: "a", "B1" "b", "C1": "c"
        Raises:
           IOError: reached max row.
        """
        # row is list string for header
        if self.end_row >= MAX_ROW:
            logger.error("Reached max row. cannot write row no more")
            raise IOError("Reached max row. cannot write row no more")

        cell_query = gdata.spreadsheets.client.CellQuery(
            min_row=self.end_row,
            max_row=self.end_row,
            min_col=1,
            max_col=len(header),
            return_empty=True)
        cells = self.client.GetCells(self.spreadsheet_key, self.worksheet_id, q=cell_query)
        batch = gdata.spreadsheets.data.BuildBatchCellsUpdate(self.spreadsheet_key, self.worksheet_id)

        for cell, s in zip(cells.entry, header):
            cell.cell.input_value = s
            batch.add_batch_entry(cell, cell.id.text, batch_id_string=cell.title.text, operation_string="update")

        self.client.batch(batch, force=True)
        self.end_row += 1

    def add_row_with_list(self, values):
        """add a row.

        google drive API doesn't accept cell control except for adding or updating text of cell.
        gdata cannot controll cell attribute such as bold font or validation

        therefore, add_row_with_list is same as add_header.

        Args:
           values(list of str): adds a row with header text list
        Raises:
           IOError: reached max row.
        """
        self.add_header(values)

    def add_row_with_dictionary(self, row):
        """add a row.
        Not used now.

        Args:
           row(dict): the dictonary of colum name and cell text in a row.
        Raises:
           IOError: cannot insert a row
        """
        # row is dict parameter

        logger.debug("in add_row_with_dictionary @ GoogleWorksheet")
        logger.debug(self.spreadsheet_key)
        logger.debug(self.worksheet_id)

        entry = gdata.spreadsheets.data.ListEntry()
        entry.from_dict(row)
        rc = self.client.add_list_entry(entry, self.spreadsheet_key, self.worksheet_id)
        if not isinstance(rc, gdata.spreadsheets.data.ListEntry):
            raise IOError()


if __name__ == "__main__":
    import sys
    from Properties import Properties

    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    logger.setLevel(logging.DEBUG)

    properties = Properties("jp2-west")

    client = GoogleSpreadsheetClient(properties.google["service_account"], properties.google["credential_file"])
    spreadsheet = client.get_spreadsheet("cleanupTest")
    worksheets = spreadsheet.get_worksheets()
    worksheet = spreadsheet.get_worksheet("ant")
    # worksheet = client.get_spreadsheet("cleanupTest").get_worksheet("ant")
    # for r in worksheet.get_rows_as_dictionary():
    #     print r
    #     print r.values()
    # worksheet.add_row_with_dictionary({"a": "AA", "b": "BB", "c": "CC", "D": "DD"})
    # worksheet = client.get_spreadsheet("cleanupTest").get_worksheet("jp2-west 20151215 17:01:55")
    # for r in worksheet.get_rows_as_list():
    #     print r
    worksheet = spreadsheet.add_worksheet("DDDle")
    spreadsheet.delete_worksheet(worksheet)
