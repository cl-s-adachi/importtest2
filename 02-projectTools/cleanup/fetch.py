#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
fetch specified accountn objects of cloudstack.
"""

# if true, enable debug logging
DEBUG = False

MAX_ROW = 1000

import argparse, json, logging, os, sys, traceback

lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "utils")
sys.path.append(lib_path)

import _utils
from CloudstackObjects       import EvacuateObjects
from Gspread                 import GoogleSpreadsheetClient, GoogleWorksheet
from ObjectFormat            import VirtualMachineFormat, VolumeFormat, SnapshotFormat, TemplateFormat
from OutputFilenameGenerator import OutputFilenameGenerator
from Properties              import Properties
from SignedAPICall           import CloudStack
from Tee                     import Tee


logger = logging.getLogger(__name__)


def _helper_output_description(fp, worksheet, description):
    fp.write(description)
    fp.write("\n")
    if worksheet is not None:
        worksheet.add_row([description])


def _helper_output_header(fp, worksheet, print_format, pickup_keys, header):
    d = dict((k, v) for k, v in zip(pickup_keys, header))
    fp.write(print_format.format(**d))
    if worksheet is not None:
        worksheet.add_row(header)


def _helper_output_line(fp, worksheet, evacuees, days_ago, print_format, pickup_keys, data):
    if worksheet is not None:

        # when created key is not returned, maybe, it's on creating.
        if "created" not in data:
            return
        # check the days-ago filter
        # days-ago value is defined in properties file
        if _utils.check_created_date(data["created"], days_ago):
        # if True:
            # if object is old object, print out it to stdout and text file, and store Google spreadsheet
            d = dict((k, v) for k, v in data.items() if k in pickup_keys)

            # is this data an evacuee?
            if d["id"] in evacuees:
                return

            fp.write(print_format.format(**d))

            worksheet.add_row([d[key] for key in pickup_keys])

    else:
        d = dict((k, v) for k, v in data.items() if k in pickup_keys)
        fp.write(print_format.format(**d))


def _list_virtualMachines(api, fp, worksheet, evacuees, days_ago, account, need_header):
    """
    output listVirtualMachines response to file and google spreadsheet
    """
    res = api.listVirtualMachines(
        {"listall": "true", "account": account["name"], "domainid": account["domainid"]}
    )

    logger.debug("return listVirtualMachines")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if _utils.check_count_value(res) is None:
        _helper_output_description(
            fp, worksheet,
            "No virtual machines on {0} of {1}".format(account["name"], account["domain"]))
        return None

    virtualMachineFormat = VirtualMachineFormat()

    if need_header:
        # output row header at the first call
        _helper_output_header(
            fp, worksheet,
            virtualMachineFormat.get_line_format(),
            virtualMachineFormat.get_keys(),
            virtualMachineFormat.get_header())

    for virtualMachine in res["virtualmachine"]:
        _helper_output_line(
            fp, worksheet,
            evacuees,
            days_ago,
            virtualMachineFormat.get_line_format(),
            virtualMachineFormat.get_keys(),
            virtualMachine)

    return res


def _list_volumes(api, fp, worksheet, evacuees, days_ago, account, need_header):
    """
    return listVolumes response
    """
    res = api.listVolumes(
        {"listall": "true", "account": account["name"], "domainid": account["domainid"]}
    )

    logger.debug("return listVolumes")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if _utils.check_count_value(res) is None:
        _helper_output_description(
            fp, worksheet,
            "No volumes on {0} of {1}".format(account["name"], account["domain"]))
        return None

    volumeFormat = VolumeFormat()

    if need_header:
        # output row header at the first call
        _helper_output_header(
            fp, worksheet,
            volumeFormat.get_line_format(),
            volumeFormat.get_keys(),
            volumeFormat.get_header())

    for volume in res["volume"]:

        if "virtualmachineid" in volume:
            continue
        # adjust columns for printing
        volume["size"] = _utils.sizeof_format(volume["size"])

        _helper_output_line(
            fp, worksheet,
            evacuees,
            days_ago,
            volumeFormat.get_line_format(),
            volumeFormat.get_keys(),
            volume)

    return res


def _list_snapshots(api, fp, worksheet, evacuees, days_ago, account, need_header):
    """
    return listSnapshots
    """
    res = api.listSnapshots(
        {"listall": "true", "account": account["name"], "domainid": account["domainid"]}
    )

    logger.debug("return listSnapshots")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if _utils.check_count_value(res) is None:
        _helper_output_description(
            fp, worksheet,
            "No snapshots on {0} of {1}".format(account["name"], account["domain"]))
        return None

    snapshotFormat = SnapshotFormat()

    if need_header:
        # output row header at the first call
        _helper_output_header(
            fp, worksheet,
            snapshotFormat.get_line_format(),
            snapshotFormat.get_keys(),
            snapshotFormat.get_header())

    for snapshot in res["snapshot"]:
        # adjust columns for printing
        if "domain" not in snapshot:  snapshot["domain"] = "N/A"
        if "account" not in snapshot: snapshot["account"] = "N/A"

        _helper_output_line(
            fp, worksheet,
            evacuees,
            days_ago,
            snapshotFormat.get_line_format(),
            snapshotFormat.get_keys(),
            snapshot)

    return res


def _list_templates(api, fp, worksheet, evacuees, days_ago, account, need_header):
    """
    return listTemplates response
    """
    res = api.listTemplates(
        {"listall": "true", "account": account["name"], "domainid": account["domainid"], "templatefilter": "self"}
    )

    logger.debug("return listTemplates")
    logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

    if _utils.check_count_value(res) is None:
        _helper_output_description(
            fp, worksheet,
            "No templates on {0} of {1}".format(account["name"], account["domain"]))
        return None

    templateFormat = TemplateFormat()

    if need_header:
        # output row header at the first call
        _helper_output_header(
            fp, worksheet,
            templateFormat.get_line_format(),
            templateFormat.get_keys(),
            templateFormat.get_header())

    for template in res["template"]:
        # adjust columns for printing
        if "domain" not in template:  template["domain"] = "N/A"
        if "account" not in template: template["account"] = "N/A"
        if "size" not in template:
            template["size"] = "N/A"
        else:
            template["size"] = _utils.sizeof_format(template["size"])

        _helper_output_line(
            fp, worksheet,
            evacuees,
            days_ago,
            templateFormat.get_line_format(),
            templateFormat.get_keys(),
            template)

    return res


def _issue_list_api(api, filename, worksheet, evacuees, days_ago, accounts):
    """
    issue list api of cloudstack objects
    """

    fp = Tee(filename, "w")
    try:
        _helper_output_description(fp, worksheet, VirtualMachineFormat().get_category())
        need_header = True
        for account in accounts:
            _list_virtualMachines(
                api,
                fp,
                worksheet,
                evacuees.get_virtualMachines(),
                days_ago,
                account,
                need_header)
            need_header = False
        _helper_output_description(fp, worksheet, "")
        fp.flush()

        _helper_output_description(fp, worksheet, VolumeFormat().get_category())
        need_header = True
        for account in accounts:
            _list_volumes(
                api,
                fp,
                worksheet,
                evacuees.get_volumes(),
                days_ago,
                account,
                need_header)
            need_header = False
        _helper_output_description(fp, worksheet, "")
        fp.flush()

        _helper_output_description(fp, worksheet, SnapshotFormat().get_category())
        need_header = True
        for account in accounts:
            _list_snapshots(
                api,
                fp,
                worksheet,
                evacuees.get_snapshots(),
                days_ago,
                account,
                need_header)
            need_header = False
        _helper_output_description(fp, worksheet, "")
        fp.flush()

        _helper_output_description(fp, worksheet, TemplateFormat().get_category())
        need_header = True
        for account in accounts:
            _list_templates(
                api,
                fp,
                worksheet,
                evacuees.get_templates(),
                days_ago,
                account,
                need_header)
            need_header = False
        _helper_output_description(fp, worksheet, "")
        fp.flush()

    except:
        traceback.print_exc()
        sys.exit("""
            FAILURE!!  during issuing cloudstack api.
            exit this script.
              bye, bye...
        """)

    fp.close()
    return


def _fetch_objects(properties, filename):

    api = CloudStack(properties.url, properties.api_key, properties.secret_key)

    #
    # user keys privilege check
    #
    _utils.check_privilege_of_keys(api)

    accounts = []
    for fetch_target in properties.accounts:
        account = fetch_target["account"].lower()
        domain  = fetch_target["domain"].lower()
        logger.debug("target account {0} {1}".format(account, domain))

        #
        # check if the account exists?
        #
        res = api.listAccounts(
            {"listall": "true", "name": account}
        )
        logger.debug("return listAccounts")
        logger.debug(json.dumps(res, sort_keys=True, indent=4, ensure_ascii=False))

        if _utils.check_count_value(res) is None:
            sys.exit("""
                Not found the account {0}.
            """.format(account, domain))

        account = None
        for a in res["account"]:
            if a["domain"].lower() == domain:
                account = a
        if account is None:
            sys.exit("""
                Not found the account {0} with domain {1}.
            """.format(account, domain))

        accounts.append(account)

    # output the file of objects that are proposed for deletion
    generator = OutputFilenameGenerator(properties.site)
    if filename is None:
        outputfilename = generator.generate_objects_filename(properties.objects_file_dir)
    else:
        outputfilename = filename

    #
    # if worksheet_off is true, output only text file.
    #
    worksheet = None
    if filename is None:
        # authorize Google account
        client = GoogleSpreadsheetClient(properties.google["service_account"], properties.google["credential_file"]).get_client()
        spreadsheet = client.open(properties.google["spreadsheet_title"])
        worksheet = spreadsheet.add_worksheet(generator.generate_worksheet_title(),
                                              MAX_ROW,
                                              max([
                                                  len(VirtualMachineFormat().get_header()),
                                                  len(VolumeFormat().get_header()),
                                                  len(SnapshotFormat().get_header()),
                                                  len(TemplateFormat().get_header()),
                                              ]))

        worksheet = GoogleWorksheet(worksheet)
    #
    # fetch evacuees
    #
    evacuateObjects = EvacuateObjects(properties.evacuees_file)

    #
    # send listObjets cloudstack api and restore.
    #
    _issue_list_api(
        api,
        outputfilename,
        worksheet,
        evacuateObjects,
        properties.evacuate_days_ago,
        accounts)


def main():
    """
    main function
    """
    if DEBUG:
        logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
        logging.getLogger("GoogleSpreadsheet").setLevel(logging.INFO)
        logger.setLevel(logging.DEBUG)

    #
    # command line option analysis
    #
    description = """
Fetches cloudstack objects with specified site name.
Stores the list of cloudstack objects(virtual machine, volume, snapshot, template) that are created 7-days-ago.
Outputs the list to a text file and Google worksheet. The output destinations are defined in properties file.
"""
    parser = argparse.ArgumentParser(
        description = description,
        usage = "%(prog)s site"
    )
    parser.add_argument(
        "site",
        action = "store",
        type   = str,
        nargs  = 1,
        help   = "Target cloudstack site. (specify jp2-east or jp2-west string.)"
    )
    parser.add_argument(
        "--worksheet-off",
        action = "store",
        dest   = "filename",
        nargs  = 1,
        help   = "Outputs only text file to the specified file. Ormits to output Google worksheet. Stores all current cloudstack objects including evacuee deifiniton objects."
    )
    args = parser.parse_args()
    site = args.site[0].lower()
    if args.filename:
        filename = args.filename[0]
    else:
        filename = args.filename

    properties = Properties(site)

    # fetch cloudstack objects and store data
    _fetch_objects(properties, filename)


if __name__ == "__main__":
    main()
