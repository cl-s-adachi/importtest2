#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
cleanup cloudstack objects.
  GoogleSpreadsheet indicates the objects should be deleted.
  read it. delete them.
"""

# if true, enable debug logging
DEBUG = False

# thread count
MAX_THREAD = 5


import argparse, logging, os, sys, threading, time

lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "utils")
sys.path.append(lib_path)

import _utils, _delete
from CloudstackObjects import DeleteObjects
from Gspread           import GoogleSpreadsheetClient
from OutputFilenameGenerator import OutputFilenameGenerator
from Properties        import Properties
from SignedAPICall     import CloudStack


logger = logging.getLogger(__name__)


def _cleanup_objects(properties):

    api = CloudStack(properties.url, properties.api_key, properties.secret_key)

    #
    # user keys privilege check
    #
    _utils.check_privilege_of_keys(api)

    # authorize for Goggle account
    client = GoogleSpreadsheetClient(properties.google["service_account"], properties.google["credential_file"]).get_client()
    spreadsheet = client.open(properties.google["spreadsheet_title"])
    worksheets = []
    for w in spreadsheet.worksheets():
        if properties.site in w.title:
            worksheets.append(w)

    # output the file of objects that are proposed for deletion
    generator = OutputFilenameGenerator(properties.site)
    worksheet = generator.get_oldest_worksheet(worksheets)
    if not worksheet:
        sys.exit("""
            not found the worksheet named "[site] [date] [time]"
            exit this script.
              bye, bye...
        """)
    print("delete list worksheet is {0} in {1}".format(worksheet.title, spreadsheet.title))

    #
    # get delete target list
    #
    deleteObjects = DeleteObjects(worksheet)
    logger.debug("delete virtual machines")
    logger.debug(deleteObjects.get_virtualMachines())
    logger.debug("\n")
    logger.debug("delete volumes")
    logger.debug(deleteObjects.get_volumes())
    logger.debug("\n")
    logger.debug("delete snapshots")
    logger.debug(deleteObjects.get_snapshots())
    logger.debug("\n")
    logger.debug("delete templates")
    logger.debug(deleteObjects.get_templates())

    #
    # delete objects
    #
    threads = []
    for uuid in deleteObjects.get_virtualMachines():
        print("delete virtual machine uuid = {0}".format(uuid))
        t = threading.Thread(target=_delete.delete_virtualmachine, args = (api, uuid))
        threads.append(t)
    for uuid in deleteObjects.get_volumes():
        print("delete volume uuid = {0}".format(uuid))
        t = threading.Thread(target=_delete.delete_volume, args = (api, uuid))
        threads.append(t)
    for uuid in deleteObjects.get_snapshots():
        print("delete snapshot uuid = {0}".format(uuid))
        t = threading.Thread(target=_delete.delete_snapshot, args = (api, uuid))
        threads.append(t)
    for uuid in deleteObjects.get_templates():
        print("delete template uuid = {0}".format(uuid))
        t = threading.Thread(target=_delete.delete_template, args = (api, uuid))
        threads.append(t)

    current = []
    while threads:
        for i in range(len(current), MAX_THREAD):
            if threads:
                thread = threads.pop()
            else:
                break
            thread.start()
            current.append(thread)
            time.sleep(5)

        current = [x for x in current if x.isAlive()]
        logger.debug("who is still alive?")
        logger.debug(current)
        if threads:
            time.sleep(5)
        else:
            for thread in current:
                thread.join()
    print("""
        deleteing objects done.
    """)

    spreadsheet.del_worksheet(worksheet)
    print("""
        deleted the worksheet, [{0}]. 
    """.format(worksheet.title))

    print("""
        all done!!!
    """)


def main():
    """
    main function
    """
    if DEBUG:
        logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
        logger.setLevel(logging.DEBUG)

    #
    # command line option analysis
    #
    description = """
Deletes cloudstack objects with specified site name.
This scirpt reads Google Spreadsheet and issues delete cloudstack API.
The Google Spreadsheet was created by fetch.py script.
"""
    parser = argparse.ArgumentParser(
        description = description,
        usage = "%(prog)s site"
    )
    parser.add_argument(
        "site",
        action = "store",
        type   = str,
        nargs  = 1,
        help   = "Target cloudstack site. (specify jp2-east or jp2-west string.)"
    )
    args = parser.parse_args()
    site = args.site[0].lower()
    properties = Properties(site)

    # destroy and remove cloudstack objects
    _cleanup_objects(properties)


if __name__ == "__main__":
    main()
