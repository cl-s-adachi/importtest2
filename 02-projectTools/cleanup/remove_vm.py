#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
delete virtual machine on cloudstack.
delete volume, snapshot, template.
"""

# if true, enable debug logging
DEBUG = False


import argparse, json, logging, os, sys, time, traceback

lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "utils")
sys.path.append(lib_path)

import _delete, _utils
from SignedAPICall import CloudStack


logger = logging.getLogger(__name__)


def _delete_vm(properties, vm_uuid):

    api = CloudStack(properties.url, properties.api_key, properties.secret_key)

    #
    # user keys privilege check
    #
    try:
        _utils.check_privilege_of_keys(api)
    except:
        sys.exit("""
            please set api / secret key with admin privilege.
            exit this script.
              bye, bye...
        """)

    _delete.delete_virtualmachine(api, vm_uuid)


def main():
    """
    main function
    """
    from Properties import Properties

    if DEBUG:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    #
    # command line option analysis
    #
    description = "removed virtual machine"
    parser = argparse.ArgumentParser(
        description = description,
        usage = "%(prog)s site vm_uuid"
    )
    parser.add_argument(
        "site",
        action = "store",
        type   = str,
        nargs  = 1,
        help   = "target cloudstack site. (specify jp2-east or jp2-west string.)"
    )
    parser.add_argument(
        "vm_uuid",
        action = "store",
        type   = str,
        nargs  = 1,
        help   = "virtual machine uuid"
    )
    args = parser.parse_args()
    site    = args.site[0].lower()
    vm_uuid = args.vm_uuid[0]
    properties = Properties(site)

    # fetch cloudstack objects and store data
    _delete_vm(properties, vm_uuid)


if __name__ == "__main__":
    main()
