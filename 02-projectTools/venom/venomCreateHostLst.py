#! /usr/bin/env python
# coding: utf-8
#
# venomCreateHostLst.py
#
# - usage:
#   ./venomCreateHostLst.py
#   python venomCreateHostLst.py
#
# - author: thsu from staira

# import libs
import MySQLdb
import sys
import os

import urllib
import urllib2
import commands


############################## Settings ##############################

# db server
#dbHost = 'tckktky4-pcldb01'
dbHost = 'tckkejo2-vcldb01'
dbName = 'cloud'
dbUser = 'root'
dbPasswd = ''

# output file related
lowestHaHostMarker = ' HA_HOST_WITH_LOWEST_ALLOCATION:'

# verbosity
debug = True
#debug = False


##################### Definitions/Implementations #####################

#Definitions for coloring terminal output
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Return the ids and names of all enabled clusters for specified hypervisor.
#
# param[in]	hypervisor               : Hypervisor of clusters to return
# param[out] List with 2 inner lists : [[clusterIds], [clusterNames]]
def getClusters(hypervisor):

	#target all KVM clusters
	selectClusterSql = "SELECT id,uuid,name,allocation_state,hypervisor_type FROM cluster WHERE removed is NULL AND hypervisor_type='"+hypervisor+"'"

	#retrieve cluster info
	connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
	cur = connect.cursor()
	cur.execute(selectClusterSql)

	print ""
	print "Enabled KVM Clusters:"
	print '   No', 'id', 'uuid', 'name', 'allocation_state' ,'hypervisor_type'

	idList =[];
	nameList = [];
	rows = int(cur.rowcount)
	for x in range(0, rows):
		row = cur.fetchone()
		print "   ", x, str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4])

		#return all enabled clusters
		if ("Enabled" == row[3]):
                	idList.append(str(row[0]));
                	nameList.append(str(row[2]));
		else:
			print "Cluster: %s not Enabled" % str(row[3])

	#mysql cleanup
	cur.close()
	connect.close()

	return (idList, nameList);
	


# Output specially formatted host.lst files.
# The contents of a host.lst file are of the format:
#   valueHost1 valueHost2 ... valueHostX haHost1 haHost2 ... haHostX HA_HOST_WITH_LOWEST_ALLOCATION:haHost1
# "HA_HOST_WITH_LOWEST_ALLOCATION:" names the first host starting the ha host portion of the list.
# Both value and ha hosts are ordered (separately) by "most likely to accept a live migration"-ness,
# which is defined by ordering in increasing order the *greater* of either cpu_alloc or mem_alloc
# percentages.  The value host list portion is sorted within itself; the ha host list portion
# is sorted within itself; the 2 lists are concatenated to form the final list.
#
# param[in]	clusteridList	: list of cluster ids to process
# param[in]	clusternameList	: list of names for each id in clusteridList (same ordering)
def outputHostLstFiles(clusteridList, clusternameList):
	print ""
	print "Creating Host LST file for each cluster with active hosts:"
	print ""

	for index, clusterid in enumerate(clusteridList):
		clustername = clusternameList[index]
		print 'Creating %s (clusterId=%s) lst file...' % (clustername, clusterid)

		#get all value hosts in cluster
		valueHostnameList = getHosts(clusterid, "VALUE")
		#get all ha hosts in cluster
		haHostnameList = getHosts(clusterid, "HA")

		#get host list array reverse(To run from the high-usage at the time of commercial application host.)
                valueHostnameList.reverse()
                haHostnameList.reverse()

		#dont output a file for any cluster without value nor ha hosts
		if (0==len(valueHostnameList) and 0==len(haHostnameList)):
			print bcolors.WARNING+"[WARN]"+bcolors.ENDC+" Skip file creation: no hosts in cluster %s" % clustername
			print ""
			continue

		writeLine = ""
		#post-pend via special marker all concat-ed ha hostnames if >1 exist
                if (0 != len(haHostnameList)):
                        writeLine += " ".join(haHostnameList) + " "
		#concat all value hostnames with a space delimiter if >1 exist
		if (0 != len(valueHostnameList)):
			writeLine += " ".join(valueHostnameList) + " "
		#post-pend via special marker all concat-ed first save destination ha hostname if >1 exist
		if (0 != len(haHostnameList)):
			writeLine += lowestHaHostMarker + haHostnameList[-1]
		writeLine = writeLine.strip()

		#output to file with cluster name
		filename = "hosts.v-" + clustername + ".lst"
		fp = open(filename, 'w+')
		fp.write(writeLine)
		fp.close()

		print bcolors.OKGREEN+"[OK]"+bcolors.ENDC+" Created "+bcolors.BOLD+"%s" % filename + bcolors.ENDC
		print ""

	print "Host LST files created."



# Return list of hosts in the specified clusterid with the specified host tag
# Hosts are ordered by "most likely to accept a live migration"-ness,
# which is defined by ordering in increasing order the *greater* of
# either cpu_alloc or mem_alloc percentages.
#
# param[in]	clusterid : id of cluster whose hosts to return
# param[in]	tag	      : host tag of hosts to return
# param[out] List     : [hostNames]
def getHosts(clusterid, tag):
	#target all existing hosts in specified cluster with specified host tag
	# - the op_host_capacity table is used to retrieve cpu/memory capacities,
	#   which are used to calculate mem_alloc & cpu_alloc for each host
	# - hosts are sorted first by the largest of its mem_alloc/cpu_alloc.
	#   this is done so hosts with a high alloc of one or both values is de-prioritized:
	#   e.g. if hostA[cpu:90%, mem:90%] and hostB[cpu:99%, mem:1%], then order hostA
	#        before hostB even though the total alloc% of hostA is larger than hostB
	#        b/c hostB has the larger single allocation (cpu:99%)
	selectHostSql = "SELECT h.id, h.uuid, h.name, h.status, ht.tag, cpuCapacity.used_capacity/cpuCapacity.total_capacity as cpu_alloc, memCapacity.used_capacity/memCapacity.total_capacity as mem_alloc FROM host as h INNER JOIN host_tags as ht ON h.id=ht.host_id LEFT JOIN op_host_capacity as cpuCapacity ON (h.id=cpuCapacity.host_id and cpuCapacity.capacity_type=1 and cpuCapacity.capacity_state='Enabled') LEFT JOIN op_host_capacity as memCapacity ON (h.id=memCapacity.host_id and memCapacity.capacity_type=0 and memCapacity.capacity_state='Enabled') WHERE h.cluster_id=" + clusterid + " and tag='" + tag + "' and h.removed is NULL ORDER BY GREATEST(cpu_alloc, mem_alloc), name;"

	#retrieve host info
	connect = MySQLdb.connect(dbHost, dbUser, dbPasswd, dbName)
	cur = connect.cursor()
	cur.execute(selectHostSql)

	#output names of all hosts in cluster to file
	hostnameList=[]
	rows = int(cur.rowcount)
	for host in range(0, rows):
		row = cur.fetchone()

		#collect all hosts, but output their state for informational purposes
		hostname = str(row[2])
		hostnameList.append(hostname)
		if ("Up" == str(row[3])):
			if (debug):
				print "   Include host: %s (%s)" % (hostname, str(row[4]))
		else:
			print "   Include host: %s (%s) " % (hostname, str(row[4])) + bcolors.WARNING + "State not Up" + bcolors.ENDC

	#mysql cleanup
	cur.close()
	connect.close()
	
	return hostnameList





############################## Main ##############################
# Start here:

ver = '1.4'
scriptName = os.path.basename(__file__)

print "Running %s %s with hard-coded parameters:" % (scriptName, ver)
print "   dbHost= %s" % dbHost
print "   dbName= %s" % dbName
print "   dbUser= %s" % dbUser
print "   dbPass= %s" % dbPasswd

#get cluster list
kvmClusters = getClusters("KVM")
if (debug):
	print "kvmClusters= %s" % str(kvmClusters)
clusterIds = kvmClusters[0]
clusterNames = kvmClusters[1]

#null guard
if (0==len(clusterIds) or 0==len(clusterNames)):
        print "No clusters found: exiting"
        sys.exit()

#create lst files from host data in each cluster
outputHostLstFiles(clusterIds, clusterNames)

print "Bye!"
print ""
