#!/bin/bash

# Copyright(c) 2015 CREATIONLINE,INC. All Right Reserved.

###############
# variable start
##########

###
# flg
###
venom_upgrade_flg=true #true:upgrade false:downgrade

###
# api parameters
###
# please set your host
#address="http://localhost:8080"
address="http://tckkejo2-vclweb01:8080"
# please set your api key. this is api@ROOT@vclweb
api_key="T03d-Bb-CwdZAuZdRpU1UDd5TKhDCN3Rc7hq9vyaL9mhw3LIVYIWT8Fo5mjyLVzX1PT8AP0oC_vooI3_c08ksQ"
# please set your secret key. this is api@ROOT@vclweb
secret_key="31gC1Yj6TLlGYP22OY5735Emh7E_KVmmSG8QKF1C4NvapcjtKpXHuZLAUTDqY10PFTq0gcZoTGKJ6M5T3BWi6Q"

###
# temporary 
###
tempfile_base=$(basename $0)
tempfile_base=${tempfile_base%.sh}
res_temp=res_command_$tempfile_base    # temporary file cloudstack response to parse

###
# pollAsyncJob function parameter
# retry:                 retry count of issuing queryAsyncJobResult
# interval:              interval time of issuing queryAsyncJobResult
# output temporary file: output temprary file from queryAsyncJobResult
###
max_retry_pollAsyncJob="120"
interval_pollAsyncJob="15"                                      # unit is second
queryAsyncJobRawXmlResponse=res_queryAsyncJob_$tempfile_base   # output file query job response

###
# function skip_check variable
###
skip_check_file_dir="./"
skip_check_file="skip_host_list.lst"

###
# function kvm_host_venom_update and kvm_host_venom_roll_back variable
###
# common
username="root"
password="Admin123ckk!"
default_session_timeout="120"
host_base_dir="/root/VENOM"
venom_result_base_temp="venom_result_temp_"
interval_host_state_chk="5"
max_retry_host_state_chk="120"
# kvm_host_venom_update variable
upgrade_evidence_base_file="venom_upgrade_"
upgrade_dir="VENOM-upgrade"
# kvm_host_venom_roll_back variable
downgrade_evidence_base_file="venom_downgrade_"
downgrade_dir="VENOM-downgrade"

###
# main process
###
if $venom_upgrade_flg; then yum_cmd="upgraded"
else yum_cmd="downgraded"; fi
vms_on_finished_hosts_list_log="vms_on_${yum_cmd}_hosts_list.log"
venom_successful_host_list_log="venom_successful_host_list.log"
venom_failed_host_list_log="venom_failed_host_list.log"
target_cluster_file_dir="./target"
evidence_base_dir="./logs"
migrate_success_vm_list_log="migrate_success_vm_list.log"
migrate_fail_vm_list_log="migrate_fail_vm_list.log"
priority_host=""
change_host_tag="VALUE"
temp_host_tag="TEMP"
first_priority_host_delimiter="HA_HOST_WITH_LOWEST_ALLOCATION:"

###
# debug 
###
debug=true
script_ver="1.0"

##########
# variable end
###############

##########
# function
#########
#
# common-functions file location
# TODO: where should common or shared files put?
# location is current directory
#
common_function_path=$(dirname $0)/common-functions
if [ -e "$common_function_path" ]; then
  . "$common_function_path"
else
  echo -e "please put common-functions file at appropriate directory"
  exit 1
fi


######
#
# usage: 選択したhostがskip対象か確認
# parameters: $1:host名
# return: 対象外:0 対象:1
#
######
function skip_check ()
{
  # skipリストファイルの存在確認
  if [ ! -e $skip_check_file_dir/$skip_check_file ]; then
    echo -e "["$skip_check_file_dir"/"$skip_check_file"] does not found."
  fi
  
  # リストファイルに引数と同名の値が存在するか確認
  host_list=$(cat $skip_check_file_dir"/"$skip_check_file)
  host_array=(`echo $host_list`)

  # host分処理を繰り返す
  for ha in "${host_array[@]}"; do
    if [ $ha = $1 ]; then
      echo -e "$1 is skip target."
      return 1
    fi
  done

  return 0
}


#####
#
# usage: hostタグを変更
# parameters: $1:hostid $2:変更後のタグ
# return: 成功:0 失敗:1
#
#####
function change_host_tag ()
{
  execute_command command=listHosts listall=true id="$1" > $res_temp
  host_tag_before=$(cat $res_temp | pickup_entity hosttags)
  
  echo -e "  [start]change host tag hostid:$1"
  execute_command command=updateHost id=$1 hosttags=$2 > $res_temp
  execute_command command=listHosts listall=true id=$1 > $res_temp
  host_tag_after=$(cat $res_temp | pickup_entity hosttags)
  if [ "$host_tag_after" == "$2" ]; then
    echo -e "  [e n d]changed tag [$host_tag_before]->[$host_tag_after]"
    return 0
  elif [ "$host_tag_after" != "$2" ]; then
    echo -e "  [e n d]failed changetag [$host_tag_before]->[$host_tag_after]"
    return 1
  fi
}


#####
#
# usage: 指定ホスト配列から特定hostタグを持つホストのタグ変更
# parameters: $1:host_listの変数名 $2:変更したいタグ $3:変更後のタグ
# parameters: $1はデリミタ空白でnameを配列で持つ変数名を指定
# parameters: $2で指定したタグを持つホストタグを変更
# return: 成功:0 失敗:1
#
#####
function loop_change_host_tag ()
{
  # paramters確認
  if [ "x${1}" = "x" ]; then
    echo -e ${1}
    echo -e "change host list not found."
    return 0
  fi
  if [ "x${2}" = "x" -o "x${3}" = "x" ]; then
    echo -e "changed host tag not selected."
    return 1
  fi

  target_host_tag=$2
  host_new_tag=$3
  change_tag_fail_flg=false

  # 関節参照で配列取得
  host_list_variable_name=$1
  eval host_list=\"\${$host_list_variable_name[@]}\"
  host_array=(`echo $host_list`)

  for host_name in "${host_array[@]}"; do
    # listHostsでhost情報取得
    execute_command command=listHosts listall=true name=$host_name > $res_temp
    host_id=$(cat $res_temp | pickup_entity id)
    host_tag=$(cat $res_temp | pickup_entity hosttags)
    host_state=$(cat $res_temp | pickup_entity state)
    if [ "xUp" = "x${host_state}" ]; then
      if [ $target_host_tag = $host_tag ]; then
        echo -e "change host tag hostname:$host_name"
        change_host_tag $host_id $host_new_tag
        if [ 1 = $? ]; then
          echo -e "failed change host tag api. this script stopped. please check cloud-management status."
          echo "hostname:$host_name hostid:$host_id [failed]change_host_tag" >> $evidence_dir/$venom_failed_host_list_log
           return 1
        fi
      fi
    else
      # 対象ホストがUpでない場合
      echo -e "  $host_name state is not Up. This host is skip."
      echo "hostname:$host_name hostid:$host_id [failed]host_state_not_Up" >> $evidence_dir/$venom_failed_host_list_log
      continue
    fi
  done
}


#####
#
# usage: 引数で指定したhostにsshを行い、venom対応を実施する
# parameters: $1:host名
# return: 成功:0 事前確認失敗:1 upgrade失敗:2
#
#####
function kvm_host_venom_update ()
{
  # log設定
  hostname=$(echo $1 | cut -d . -f 1)
  evidence_file=$evidence_dir/$upgrade_evidence_base_file$hostname.log
  iptables_cmd="iptables -nvL | grep 'tcp spt:2049'"
  iptables_chk_cmd="iptables -nvL | grep 'tcp spt:2049' | wc -l"
  iptables_set_cmd="iptables -I INPUT 1 -p tcp --sport 2049 -j ACCEPT"
  iptables_save_msg="OK"
  iptables_save_cmd="service iptables save"
  ip_result_before_msg="iptables_cmd:result:"
  ip_result_after_msg=":count"
  ip_result_exec_before_match_msg=$ip_result_before_msg"0"$ip_result_after_msg
  ip_result_exec_after_match_msg=$ip_result_before_msg"1"$ip_result_after_msg
  echo $ip_result_match_msg

  # hostへ接続
  echo -e "  [start]ssh host $1"
  expect -c"
    log_file "$evidence_file"
    set timeout $default_session_timeout
    spawn ssh -l $username $hostname

    expect {
      default {exit 1}
      \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\n\"
        expect \"$username@$hostname's password:\"
        send \"$password\n\"
      } \"$username@$hostname's password:\" {
        send \"$password\n\"
      }
    }

    expect {
      default {exit 1}
      \"\[$username@$hostname\" {
        send \"date\n\"
        send \"$iptables_cmd\n\"
        send \"$iptables_chk_cmd\n\"
        send \"ip_result_before=\`$iptables_chk_cmd\`\n\"
        send \"cmd_msg_before="$ip_result_before_msg"\\\$ip_result_before"$ip_result_after_msg"\n\"
        send \"echo \\\$cmd_msg_before\n\"
        set timeout 2
      }
    }
    expect {
      \"$ip_result_exec_before_match_msg\" {
        send \"$iptables_set_cmd\n\"
        send \"$iptables_save_cmd\n\"
        expect {
          default {exit 2}
          \"$iptables_save_msg\" {
            send \"$iptables_cmd\n\"
            send \"$iptables_chk_cmd\n\"
            send \"ip_result_after=\`$iptables_chk_cmd\`\n\"
            send \"cmd_msg_after="$ip_result_before_msg"\\\$ip_result_after"$ip_result_after_msg"\n\"
            send \"echo \\\$cmd_msg_after\n\"
            expect {
              default {exit 2}
              \"$ip_result_exec_after_match_msg\" {
                set timeout $default_session_timeout
              }
            }
          }
        }
      } timeout { set timeout $default_session_timeout }
    }

    expect {
      default {exit 2}
      \"\[$username@$hostname\" {
        send \"date\n\"
        send \"/etc/init.d/cloud-agent status\n\"
        expect {
          default {exit 2}
          \"cloud-agent (pid\" {
            send \"date\n\"
            send \"/etc/init.d/cloud-agent stop\n\"
            expect {
              default {exit 2}
              \"\[  OK  \]\" {
                send \"date\n\"
              }
            }
          }
        }
      }
    }

    expect {
      default {exit 1}
      \"\[$username@$hostname\" {
        send \"cd $host_base_dir/$upgrade_dir\n\"
      }
    }
    expect {
      default {exit 1}
      \"$upgrade_dir\]#\" {
        send \"ls -l\n\"
        send \"ls -l *rpm | wc -l\n\"
      }
    }
    expect {
      default {exit 1}
      \"11\" {
        send \"date\n\"
        send \"yum -y localinstall *\n\"
      }
    }

    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"/etc/init.d/libvirtd status\n\"
        expect {
          default {exit 2}
          \"libvirtd (pid\" {
            send \"date\n\"
            send \"/etc/init.d/libvirtd stop\n\"
            expect {
              default {exit 2}
              \"\[  OK  \]\" {
                send \"date\n\"
                send \"/etc/init.d/libvirtd start\n\"
                expect {
                  default {exit 2}
                  \"\[  OK  \]\" {
                    send \"date\n\"
                    send \"/etc/init.d/libvirtd status\n\"
                  }
                }
              }
            }
          }
        }
      }
    }
    expect {
      default {exit 2}
      \"libvirtd (pid\" {
        send \"date\n\"
        send \"/etc/init.d/cloud-agent start\n\"
        expect {
          default {exit 2}
          \"\[  OK  \]\" {
            send \"date\n\"
            send \"/etc/init.d/cloud-agent status\n\"
            expect {
              default {exit 2}
              \"cloud-agent (pid\" { send \"date\n\" }
            }
          }
        }
      }
    }

    send \"exit\n\"
    interact
  "
  result=$?
  echo "venom patch process result=$result"
  echo $result > $venom_result_temp
  echo -e "  [e n d]ssh host $1"

  # check host status
  echo "  Waiting agent status Up..."
  retry_num=0
  execute_command command=listHosts listall=true name=$1 > $res_temp
  host_state=$(cat $res_temp | pickup_entity state)
  while [ "Up" != $host_state -a $max_retry_host_state_chk -gt $retry_num ]
  do
    retry_num=$((retry_num+1))
    echo "    retry_num=$retry_num, host_state=$host_state, next_check $interval_host_state_chk second..."
    sleep $interval_host_state_chk
    execute_command command=listHosts listall=true name=$1 > $res_temp
    host_state=$(cat $res_temp | pickup_entity state)
  done
  echo "    Finish agent state check. host_state=$host_state"
  if [ "Up" != $host_state -a $max_retry_host_state_chk -le $retry_num ]; then
    echo "  Restart agent not Up status. check $hostname agent log."
    result=2
    echo $result > $venom_result_temp
  fi
}


#####
#
# usage: 引数で指定したhostにsshを行い、venom対応の切り戻しを実施する
# parameters: $1:host名
# return: 成功:0 事前確認失敗:1 切り戻し失敗:2
#
#####
function kvm_host_venom_roll_back ()
{
  # downgrage対象パッケージを変数として指定
  qemu_img="qemu-img-0.12.1.2-3.209.el6.4.x86_64.rpm"
  qemu_kvm="qemu-kvm-0.12.1.2-3.209.el6.4.x86_64.rpm"
  seabios="seabios-0.6.1.2-8.el6.x86_64.rpm"
  spice_server="spice-server-0.8.2-5.el6.x86_64.rpm"
  zlib="zlib-1.2.3-27.el6.x86_64.rpm"
  openssl="openssl-1.0.0-20.el6_2.5.x86_64.rpm"
  # removed対象パッケージを変数として指定(version指定のパッケージ名では削除不可の為、パッケージ名のみ指定)
  glusterfs="glusterfs"
  glusterfs_api="glusterfs-api"
  glusterfs_libs="glusterfs-libs"
  snappy="snappy"
  usbredir="usbredir"

  # log設定
  hostname=$(echo $1 | cut -d . -f 1)
  evidence_file=$evidence_dir/$downgrade_evidence_base_file$hostname.log

  # hostへ接続
  echo -e "  [start]ssh host $1"
  expect -c"
    log_file "$evidence_file"
    set timeout $default_session_timeout
    spawn ssh -l $username $hostname

    expect {
      default {exit 1}
      \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\n\"
        expect \"$username@$hostname's password:\"
        send \"$password\n\"
      } \"$username@$hostname's password:\" {
        send \"$password\n\"
      }
    }

    expect {
      default {exit 2}
      \"\[$username@$hostname\" {
        send \"date\n\"
        send \"/etc/init.d/cloud-agent status\n\"
        expect {
          default {exit 2}
          \"cloud-agent (pid\" {
            send \"date\n\"
            send \"/etc/init.d/cloud-agent stop\n\"
            expect {
              default {exit 2}
              \"\[  OK  \]\" {
                send \"date\n\"
              }
            }
          }
        }
      }
    }

    expect {
      default {exit 1}
      \"\[$username@$hostname\" {
        send \"cd $host_base_dir/$downgrade_dir\n\"
      }
    }
    expect {
      default {exit 1}
      \"$downgrade_dir\]#\" {
        send \"ls -l\n\"
        send \"ls -l *rpm | wc -l\n\"
      }
    }
    expect {
      default {exit 1}
      \"6\" {
        send \"date\n\"
        send \"yum -y  downgrade $qemu_img $qemu_kvm\n\"
      }
    }

    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"yum -y  downgrade $seabios\n\"
      }
    }
    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"yum -y  downgrade $spice_server\n\"
      }
    }
    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"yum -y  downgrade $zlib\n\"
      }
    }
    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"yum -y remove $glusterfs $usbredir $snappy $glusterfs_libs\n\"
      }
    }
    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"yum -y  downgrade $openssl\n\"
      }
    }
    expect {
      default {exit 2}
      \"Complete!\" {
        send \"date\n\"
        send \"/etc/init.d/libvirtd status\n\"
        expect {
          default {exit 2}
          \"libvirtd (pid\" {
            send \"date\n\"
            send \"/etc/init.d/libvirtd stop\n\"
            expect {
              default {exit 2}
              \"\[  OK  \]\" {
                send \"date\n\"
                send \"/etc/init.d/libvirtd start\n\"
                expect {
                  default {exit 2}
                  \"\[  OK  \]\" {
                    send \"date\n\"
                    send \"/etc/init.d/libvirtd status\n\"
                  }
                }
              }
            }
          }
        }
      }
    }
    expect {
      default {exit 2}
      \"libvirtd (pid\" {
        send \"date\n\"
        send \"/etc/init.d/cloud-agent start\n\"
        expect {
          default {exit 2}
          \"\[  OK  \]\" {
            send \"date\n\"
            send \"/etc/init.d/cloud-agent status\n\"
            expect {
              default {exit 2}
              \"cloud-agent (pid\" { send \"date\n\" }
            }
          }
        }
      }
    }

    send \"exit\n\"
    interact
  "
  result=$?
  echo "venom patch process result=$result"
  echo $result > $venom_result_temp
  echo -e "  [e n d]ssh host $1"

  # check host status
  echo "  Waiting agent status Up..."
  retry_num=0
  execute_command command=listHosts listall=true name=$1 > $res_temp
  host_state=$(cat $res_temp | pickup_entity state)
  while [ "Up" != $host_state -a $max_retry_host_state_chk -gt $retry_num ]
  do
    retry_num=$((retry_num+1))
    echo "    retry_num=$retry_num, host_state=$host_state, next_check $interval_host_state_chk second..."
    sleep $interval_host_state_chk
    execute_command command=listHosts listall=true name=$1 > $res_temp
    host_state=$(cat $res_temp | pickup_entity state)
  done
  echo "    Finish agent state check. host_state=$host_state"
  if [ "Up" != $host_state -a $max_retry_host_state_chk -le $retry_num ]; then
    echo "  Restart agent not Up status. check $hostname agent log."
    result=2
    echo $result > $venom_result_temp
  fi
}


#####
#
# usage: このscriptで作成された一時ファイルを削除する
# return: 成功:0 失敗:1
#
#####
function delete_temp_files ()
{
  if [ -f $res_temp ]; then rm $res_temp; fi
  if [ 0 -lt "`ls ${venom_result_base_temp}* 2> /dev/null | wc -l`" ]; then
    temp_host_log_list=$(ls ${venom_result_base_temp}*)
    tmep_host_log_array=(`echo $temp_host_log_list`)
    if [ $(echo ${#tmep_host_log_array[@]}) -gt 0 ]; then
      rm ${venom_result_base_temp}*
    fi
  fi
}



#############################################
##
## main procedure
##
#############################################
script_name=`basename $0`
echo -e "This script is [$script_name ver $script_ver]"
success_host_count=0
venom_host_count=0

#####
# script variable check
#####
if [ true != $venom_upgrade_flg -a false != $venom_upgrade_flg ]; then
  echo -e "this script parameter [venom_upgrade_flg] must be true or false. stopping script."
  exit
fi


#####
# hosts.*ファイルの存在確認
###
if [ "$(ls ${target_cluster_file_dir}/hosts.*)" = '' ]; then
    echo -e "target cluster hosts file does not found. target file ["${target_cluster_file_dir}"/hosts.*)]"
    exit 1
fi

###
# create evidence base directry
###
if [ ! -e ${evidence_base_dir} ]; then
    echo -e "["${evidence_base_dir}"] does not found. This folder create."
    mkdir ${evidence_base_dir}
fi
script_start_time=$(date +%Y%m%d%H%M%S)


######
# check target cluster file 
######
# targetフォルダに存在するhosts.*ファイルを数える
target_cluster_list=$(ls ${target_cluster_file_dir}/hosts.*)
target_cluster_array=(`echo $target_cluster_list`)

# hosts.*ファイル分処理を繰り返す
for tca in "${target_cluster_array[@]}"; do

  echo -e "[action target cluster file name] $tca"

  # 設定
  tag_roll_back_flg=true
  target_cluster_file=$(echo $tca | awk -F$target_cluster_file_dir/ '{print $2;}')
  evidence_dir="${evidence_base_dir}/$target_cluster_file/$script_start_time"
  mkdir -p $evidence_dir

  ######
  # check target hosts file
  ######
  # hosts.*ファイルから対象ホストを設定
  host_list=$(cat $tca)
  target_host_list=$(echo $host_list | awk -F$first_priority_host_delimiter '{print $1;}')
  target_host_array=(`echo $target_host_list`)

  # ホストタグの変更
  loop_change_host_tag "target_host_array" $change_host_tag $temp_host_tag
  if [ 1 = $? ]; then
    exit
  fi

  # host分処理を繰り返す
  for tha in "${target_host_array[@]}"; do

    # host単位の初期設定
    echo -e "------------"
    echo -e "[start][action target host name] $tha"

    #####
    # ホストがskip対象か確認
    #####
    skip_check $tha
    if [ 1 = $? ]; then
      tag_roll_back_flg=false
      continue
    fi

    # listHostsでhost情報取得
    execute_command command=listHosts listall=true name=$tha > $res_temp
    host_id=$(cat $res_temp | pickup_entity id)
    host_state=$(cat $res_temp | pickup_entity state)


    # hostのstateがUp以外の場合skip対象とする
    if [ $host_state != "Up" ]; then
      tag_roll_back_flg=false
      continue
    fi
 
    #####
    # 作業継続の判断
    #####
    while true
    do
      echo -e ""
      echo -e "ホスト[$tha]の[$yum_cmd]を開始します。"
      echo -e "  実行オプションを選択してください。"
      echo -e " 1) venomスクリプトの実行"
      echo -e " 2) このホストの$yum_cmdをスキップし、venomスクリプトを実行。"
      echo -e " q) このスクリプトを停止する。"
      read -p "上記オプションから希望する処理の番号を入力してください [1-2 or q]: " input
      case "$input" in
        1)
          break
          ;;
        2)
          echo -e "selected 2. This host is skip."
          tag_roll_back_flg=false
          continue 2 # this continue is frcus host loop(tha)
          ;;
        q|Q)
          echo -e ""
          echo -e "  exit this script. bye..."
          exit
          ;;
        *)
          echo -e ""
          echo -e "invalid key input"
          ;;
      esac
    done
    venom_host_count=$((venom_host_count+1))



    #####
    # venom
    #####
    venom_result_temp="$venom_result_base_temp$tha.log"
    if $venom_upgrade_flg; then kvm_host_venom_update $tha
    else kvm_host_venom_roll_back $tha; fi
    venom_result=$(cat $venom_result_temp)
    if [ 1 = $venom_result ]; then
      # upgrade/downgrade事前処理失敗時
      echo "$tha failed venom advance work process. This host is skip."
      echo "hostname:$tha hostid:$host_id [failed]failed_yum_${yum_cmd}_advance_work" >> $evidence_dir/$venom_failed_host_list_log
      tag_roll_back_flg=false
      continue
    elif [ 2 = $venom_result ]; then
      # upgrade/downgrade処理失敗時
      echo "$tha failed venom process. This host is skip."
      echo "hostname:$tha hostid:$host_id [failed]failed_yum_${yum_cmd}" >> $evidence_dir/$venom_failed_host_list_log
      tag_roll_back_flg=false
      continue
    fi
    # upgrade/downgrade成功時
    echo "hostname:$tha hostid:$host_id" >> $evidence_dir/$venom_successful_host_list_log


    echo -e "[e n d][action target host name] $tha"
    echo -e "------------"
    success_host_count=$((success_host_count+1))

  done

  # ホストタグの戻し
  if $tag_roll_back_flg; then
    loop_change_host_tag "target_host_array" $temp_host_tag $change_host_tag
    if [ 1 = $? ]; then
      exit
    fi
  else
    echo -e "----- ATTENTION -----"
    echo -e "  処理の行われていないホストがある、またはパッチ適用に失敗した為、ホストタグの戻しは行いません。"
    echo -e "  ログを調査し、問題が発生していないか確認してください。"
    echo -e "---------------------"
  fi

done

# tempファイル削除
delete_temp_files

# finish message
echo -e "##################################"
echo -e ""
echo -e "finish this scritp. venom [$yum_cmd] process."
echo -e "result venom [$yum_cmd] success host num [$success_host_count/$venom_host_count]"
echo -e ""
echo -e "##################################"
