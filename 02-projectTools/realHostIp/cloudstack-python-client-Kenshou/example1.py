#!/usr/bin/python

import CloudStack

api = 'http://localhost:8080/client/api'
apikey = 'T03d-Bb-CwdZAuZdRpU1UDd5TKhDCN3Rc7hq9vyaL9mhw3LIVYIWT8Fo5mjyLVzX1PT8AP0oC_vooI3_c08ksQ'
secret = '31gC1Yj6TLlGYP22OY5735Emh7E_KVmmSG8QKF1C4NvapcjtKpXHuZLAUTDqY10PFTq0gcZoTGKJ6M5T3BWi6Q'

cloudstack = CloudStack.Client(api, apikey, secret)

vms = cloudstack.listVirtualMachines({
    'listall': 'true'
})

for vm in vms:
    print "%s %s %s" % (vm['id'], vm['name'], vm['state'])
