# drop dr user
drop user 'dr'@'%';
drop user 'dr'@'localhost';

# create dr user
GRANT USAGE ON *.* TO 'dr'@'%' IDENTIFIED BY 'ckkdrpass';
GRANT ALL PRIVILEGES ON `dr`.* TO 'dr'@'%';
GRANT USAGE ON *.* TO 'dr'@'localhost' IDENTIFIED BY 'ckkdrpass';
GRANT ALL PRIVILEGES ON `dr`.* TO 'dr'@'localhost';


flush privileges;

# check user info
show databases;
SELECT * FROM mysql.user;
