echo "#show tables#"
mysql -u root dr -e "show tables;"

echo "#desc partitions#"
mysql -u root dr -e "desc partitions;"

echo "#select * from partitions#"
mysql -u root dr -e "select * from partitions;"

echo "#desc partition_isilon#"
mysql -u root dr -e "desc partition_isilon;"

echo "#select * from partition_isilon#"
mysql -u root dr -e "select * from partition_isilon;"

echo "#desc partition_zones#"
mysql -u root dr -e "desc partition_zones;"

echo "#select * from partition_zones#"
mysql -u root dr -e "select * from partition_zones;"

echo "#desc recovery_isilon#"
mysql -u root dr -e "desc recovery_isilon;"

echo "#select * from  recovery_isilon#"
mysql -u root dr -e "select * from  recovery_isilon;"

echo "#select * from recovery_isilon#"
mysql -u root dr -e "select * from recovery_isilon\G"

echo "#desc recovery_manual_jobs#"
mysql -u root dr -e "desc recovery_manual_jobs;"

echo "#slect * from recovery_manual_jobs limit 3#"
mysql -u root dr -e "select * from recovery_manual_jobs limit 3\G"

echo "#desc recovery_auto_interval#"
mysql -u root dr -e "desc recovery_auto_interval;"

echo "#seldect * from recovery_auto_interval limit 3#"
mysql -u root dr -e "select * from recovery_auto_interval limit 3\G"
