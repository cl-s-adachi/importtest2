#!/bin/bash

######################################################
##KCPS2.5 DRDB更新##
######################################################

##########################################################################################################################################

###変数定義###
work_dir=/home/ckkcl/KCSPS2.5_DRDB_update_fixipaddr
#work_dir=/home/tckkcl/KCSPS2.5_DRDB_update_fixipaddr

resource=/home/ckkcl/KCSPS2.5_DRDB_update_fixipaddr/fixipaddr_DRDB_oyama.sql
#resource=/home/tckkcl/KCSPS2.5_DRDB_update_fixipaddr/fixipaddr_DRDB_oyama.sql


##############

###関数定義ファイル読み込み###
. cl-n.fnc

##############

##########################################################################################################################################
###実行###

echo "################"
echo "#DRDB 修正前   #"
echo "################"
mysql -u root dr -e "show tables;"

echo "#################"
echo "#DRDB修正 Start #"
echo "#################"

cd ${work_dir};

echo "-----作業ディレクトリ チェック-----"

judge "`pwd`" ${work_dir} work_directory

echo "-----.sql インポート-----"

mysql -u root < ${resource}

judge "$?" 0 import

echo "-----結果確認-----"

echo "################"
echo "#DRDB 修正後   #"
echo "################"

mysql -u root < ${check}


echo "################"
echo "#DRDB修正 End ##"
echo "################"
