#!/bin/bash

###関数定義###
function judge () {
if [ $1 = $2 ]
then
    echo "$3 is OK!"
else
    echo "$3 is NG!"
    echo "script STOP!"
    exit 0
fi
}

function md5sum_check (){
if [ "`md5sum  $1 | cut -d " " -f1`" = $2 ]
then
    echo "md5sum is OK!"
else
    echo "md5sum is NG!"
    echo "script STOP!"
    exit 0
fi
}

function uuid_check (){
if [[ "$1" =~ [a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12} ]]
then
  uuid=1
  echo "【cloud.volumes.uuid】=$1"
  echo "↑This is uuid!"
elif [ "$1" = "" ]
then
  echo ""
else
  echo "【cloud.volumes.uuid】=$1"
  echo "↑This is Not uuid!! You had better check uuid!!"
fi
}

##############

