#!/bin/bash
# recoveryDRserver.sh
# for creationline, inc.
# ver 1.0

# DB Server IP adress
DBIP=ck2ejo2-pcldb00

# Mirror DB Server IP adress
MirrorDBIP=10.195.0.20

# display time
time=720

while getopts f OPT
do
  case $OPT in
    "f" ) FLG_F="TRUE" ;;
  esac
done

update_auto_jobs() {
    mysql -u root -h $DBIP dr -e "UPDATE recovery_auto_interval SET auto_jobs_id=NULL where auto_jobs_id in (SELECT id FROM recovery_auto_jobs WHERE batch_server_id = $1 and state='Creating') \G";
    mysql -u root -h $DBIP dr -e "UPDATE recovery_auto_jobs SET state='Error', end_time=now() - interval '9' hour WHERE batch_server_id = $1 and state='Creating';";
}

update_manual_jobs() {
    mysql -u root -h $DBIP dr -e "UPDATE recovery_manual_jobs SET state='Error', end_time=now() - interval '9' hour WHERE batch_server_id = $1 and state='Creating';";
}

get_batch_server_id() {
    R=`mysql -u root -h $DBIP dr -N -B -e "select id from recovery_batch where state like 'ok' and removed is null and name like '$1';"`
    echo $R
}

get_batch_server_list() {
    R=`mysql -u root -h $DBIP dr -N -B -e "select name from recovery_batch where state like 'ok' and removed is null;"`
    echo $R
}

get_account_name() {
    R=`mysql -u root -h $DBIP cloud -N -B -e "select a.account_name,d.name from account as a, domain as d where a.domain_id=d.id and a.uuid = '$1';"`
    R=`echo $R |sed 's/ /@/'`
    echo $R
}

get_source_snapshot_name() {
    R=`mysql -u root -h $MirrorDBIP -P 3307 cloud -N -B -e "select name from snapshots where uuid = '$1';"`
    echo $R
}

get_batch_server_name() {
    R=`mysql -u root -h $DBIP dr -N -B -e "select name from recovery_batch where id = '$1';"`
    echo $R
}

TMP_FILE=tmp_jobs2

print_tmp_jobs() {
  cat $TMP_FILE | while read line
  do
    ID=`echo ${line} |awk '{print $1}'`
    A_UUID=`echo ${line} |awk '{print $2}'`
    A_NAME=`get_account_name $A_UUID`
    SS_UUID=`echo ${line} |awk '{print $3}'`
    SS_NAME=`get_source_snapshot_name $SS_UUID`;
    BATCH_SERVER_ID=`echo ${line} |awk '{print $4}'`
    BATCH_SERVER_NAME=`get_batch_server_name $BATCH_SERVER_ID`;
    STATE=`echo ${line} |awk '{print $5}'`
    STATEDETAILS=`echo ${line} |awk '{print $6}'`
    START_TIME=`echo ${line} |awk '{print $7" "$8}'`
    END_TIME=`echo ${line} |awk '{print $9" "$10}'`
    printf "$BATCH_SERVER_NAME  $A_NAME %-2s $SS_NAME%-6s $STATE %-2s $STATEDETAILS %-14s $START_TIME %-2s $END_TIME \n"
  done
}

print_jobs_interval() {
  cat $TMP_FILE | while read line
  do
    ID=`echo ${line} |awk '{print $1}'`
    AUTO_JOBS_ID=`echo ${line} |awk '{print $2}'`
    SV_TYPE=`echo ${line} |awk '{print $3}'`
    SV_UUID=`echo ${line} |awk '{print $4}'`
    A_UUID=`echo ${line} |awk '{print $2}'`
    A_NAME=`get_account_name $A_UUID`
    D_NAME=`echo ${line} |awk '{print $2}'`
    ZONE_UUID=`echo ${line} |awk '{print $2}'`
    CREATED=`echo ${line} |awk '{print $7" "$8}'`
    END_TIME=`echo ${line} |awk '{print $9" "$10}'`
    printf "$BATCH_SERVER_NAME  $A_NAME %-2s $SS_NAME%-6s $STATE %-2s $STATEDETAILS %-14s $START_TIME %-2s $END_TIME \n"
  done
}

print_update_auto_jobs() {
    mysql -u root -h $DBIP dr -N -B -e "select id,account_uuid,source_snapshot_uuid,batch_server_id,state,statedetails,start_time + interval '9' hour,end_time + interval '9' hour from recovery_auto_jobs where batch_server_id = $1 and state='Creating';" > $TMP_FILE;
    print_tmp_jobs;
    mysql -u root -h $DBIP dr -N -B -e "select * from recovery_auto_interval where auto_jobs_id in (select id from recovery_auto_jobs where batch_server_id = $1 and state='Creating');" > $TMP_FILE;
    cat $TMP_FILE
}

print_update_manual_jobs() {
    mysql -u root -h $DBIP dr -N -B -e "select id,account_uuid,source_snapshot_uuid,batch_server_id,state,statedetails,start_time + interval '9' hour,end_time + interval '9' hour from recovery_manual_jobs where batch_server_id = $1 and state='Creating';" > $TMP_FILE;
    print_tmp_jobs;
}

if [ $# -ne 1 ]; then
  echo "Batch server list:"
  BLIST=`get_batch_server_list`;
  for BNAME in $BLIST
    do 
       echo " $BNAME"
    done
  exit 0;
fi

BID=`get_batch_server_id $1`;
if [ "x$BID" = "x" ]; then
  echo "Batch Server: '$1' is not found"
  exit 1;
fi

printf "[ Manual Recovery Jobs (in Progress)]\n"
print_update_manual_jobs $BID;
printf "[ Auto Recovery Jobs (in Progress)]\n"
print_update_auto_jobs $BID;

echo -n "Batch Server $1 Cancel Jobs: [y/n]?  "
read val1
case "$val1" in
    y | yes | Y | YES ) ;;
    *       ) rm -f $TMP_FILE; exit 0;;
esac

echo "[ Cancel Jobs ]"
echo -n "Manual Recovery Jobs (in Progress).."
update_manual_jobs $BID;
sleep 0.5; 
echo "  done."

echo -n "Auto Recovery Jobs (in Progress).."
update_auto_jobs $BID;
sleep 0.5; 
echo "  done."

rm -f $TMP_FILE;
exit 0;
