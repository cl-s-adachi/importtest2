#!/bin/bash
# deleteTemporaryResource.sh
# for creationline, inc.
# ver 1.0

# DB Server IP adress

DBIP="ck2ejo2-pcldb00s"
# Mirror DB Server IP adress
MirrorDBIP="10.195.0.20"

LOGFILE=deleteTemporaryResource.log
# ファイルの出力先
SSfileName="/home/ckk_ope/scripts/excluding_snapshots_list.txt"
VolFileName="/home/ckk_ope/scripts/excluding_volumes_list.txt"

#ログスタート関数
start_log() {
  DATE=`/bin/date +"%Y%m%d-%H%M"`
  echo "------------------------------" >> $LOGFILE
  echo "StartTime:$DATE" >> $LOGFILE
  echo -n "Host: " >> $LOGFILE
  uname -n >> $LOGFILE
  echo -n "Command: " >> $LOGFILE
  echo "$0 $@" >> $LOGFILE
}
#ログエンド	関数
end_log() {
  DATE=`/bin/date +"%Y%m%d-%H%M"`
  echo "EndTime:$DATE" >> $LOGFILE
  echo "------------------------------" >> $LOGFILE
}

clean_tmpfile() {
	rm -f tmp_result tmp_m_TS_list tmp_a_TS_list tmp_m_TV_list tmp_a_TV_list tmp_d_TS_list tmp_d_TV_list
}

get_batch_server_id() {
  R=`mysql -u root -h $DBIP dr -N -B -e "select id from recovery_batch where state like 'ok' and removed is null and name like '$1';"`
  echo $R
}

get_batch_server_list() {
  R=`mysql -u root -h $DBIP dr -N -B -e "select name from recovery_batch where state like 'ok' and removed is null;"`
  echo $R
}

start_log $@


#####  jobがエラー かつ、uuidで値が入っているゴミの削除
if [ $# -ne 1 ]; then
  echo "Batch server list:"
  BLIST=`get_batch_server_list`
  for BNAME in $BLIST
    do
       echo " $BNAME"
    done
  end_log
  exit 0
fi

BID=`get_batch_server_id $1`
if [ -z $BID ]; then
  echo "Not Found Batch server name."
  end_log
  exit 1
fi

# manual_jobs : state != Finishe
mysql -u root -h $DBIP dr -N -B -e "select temporary_snapshot_uuid from recovery_manual_jobs where batch_server_id = $BID and temporary_snapshot_uuid !='NULL' and state!='Finished';" > tmp_m_TS_list;
mysql -u root -h $DBIP dr -N -B -e "select temporary_volume_uuid from recovery_manual_jobs where batch_server_id = $BID and temporary_volume_uuid!='NULL' and state!='Finished';" > tmp_m_TV_list;

# auto_jobs : state != Finished
mysql -u root -h $DBIP dr -N -B -e "select temporary_snapshot_uuid from recovery_auto_jobs where batch_server_id = $BID and temporary_snapshot_uuid!='NULL' and state!='Finished';" > tmp_a_TS_list;
mysql -u root -h $DBIP dr -N -B -e "select temporary_volume_uuid from recovery_auto_jobs where batch_server_id = $BID and temporary_volume_uuid!='NULL' and state!='Finished';" > tmp_a_TV_list;

rm -f tmp_d_TS_list tmp_d_TV_list
touch tmp_d_TS_list tmp_d_TV_list

echo "Temporary Resource list:"
echo "Snapshots:"
cat tmp_m_TS_list tmp_a_TS_list | while read line
do
  TS_UUID=`echo $line | awk -F" " '{print $1}'`
  echo -n " $TS_UUID" >> $LOGFILE
  ./kick_api.sh command=listSnapshots id=${TS_UUID} > tmp_result
  cat tmp_result |grep "<id>"
  cat tmp_result |grep "<name>" |sed "s/<name>/  <name>/"
  cat tmp_result |grep "<account>" |sed "s/<account>/  <account>/"
  cat tmp_result |grep "<domain>" |sed "s/<domain>/  <domain>/"
  cat tmp_result |grep "<name>" >> /dev/null
  if [ $? == 0 ]; then
     echo $TS_UUID >> tmp_d_TS_list
  fi
done

echo "Volumes:"
cat tmp_m_TV_list tmp_a_TV_list | while read line
do
  TV_UUID=`echo $line | awk -F" " '{print $1}'`
  ./kick_api.sh command=listVolumes id=${TV_UUID} > tmp_result
  cat tmp_result >> $LOGFILE
  cat tmp_result |grep "<id>"
  cat tmp_result |grep "<name>" |sed "s/<name>/  <name>/"
  cat tmp_result |grep "<account>" |sed "s/<account>/  <account>/"
  cat tmp_result |grep "<domain>" |sed "s/<domain>/  <domain>/"
  cat tmp_result |grep "<attached>" |sed "s/<attached>/  <attached>/"
  cat tmp_result |grep "<name>" >> /dev/null
  if [ $? == 0 ]; then
     echo $TV_UUID >> tmp_d_TV_list
  fi
done

echo -n "Delete Temporary Resource: [y/n]?  "
read val1
case "$val1" in
    y | yes | Y | YES )


	echo "Snapshots List: " >> $LOGFILE
	cat tmp_d_TS_list >> $LOGFILE
	echo "Volume List: " >> $LOGFILE
	cat tmp_d_TV_list >> $LOGFILE

	echo "[ Delete Temporary Resource ]"
	echo "Snapshots (in Progress)"
	cat tmp_d_TS_list | while read line
	do
	  TS_UUID=`echo $line | awk -F" " '{print $1}'`
	  echo -n " $TS_UUID"

		./kick_api.sh command=deleteSnapshot id=${TS_UUID} > tmp_result
	#DRの課金関連テーブルに値をいれるため、データ保存
		echo ${TS_UUID} >> kakinSS

	  echo ".. done."
	  cat tmp_result >> $LOGFILE
	done

	echo "Volumes (in Progress)"
	cat tmp_d_TV_list | while read line
	do
	  TV_UUID=`echo $line | awk -F" " '{print $1}'`
	  echo -n " $TV_UUID"

	#DRの課金関連テーブルに値をいれるため、データ保存
		echo ${TV_UUID} >> kakinVOL
	  ./kick_api.sh command=listVolumes id=${TV_UUID} > tmp_result
	  cat tmp_result >> $LOGFILE
	  cat tmp_result |grep "<attached>" >> /dev/null
	  if [ $? == 0 ]; then
	      ./kick_api.sh command=detachVolume id=${TV_UUID} > tmp_result
	      cat tmp_result >> $LOGFILE
	      echo -n ".. detaching(sleep 10) "
	      sleep 10;
	  fi
	  ./kick_api.sh command=deleteVolume id=${TV_UUID} > tmp_result
	  echo ".. done."
	  cat tmp_result >> $LOGFILE
	done
;;
*       ) ;;
esac

#####################################
# kakinVOL kakinSS に保存したUUIDから、recovery_temporary_volumes, recovery_temporary_snapshotsに追加
#####################################
# kakinVOL を dr.recovery_temporary_volumes に insert
if [ -e kakinVOL ];
then
	cat kakinVOL | while read voluuid
	do
		mysql -u root -h $DBIP dr -N -B -e "update dr.recovery_temporary_volumes set removed = NOW() - interval '9' hour  where uuid = '${voluuid}'"
	done
	rm -f kakinVOL
fi
# kakinSS を  dr.recovery_temporary_snapshots に insert
if [ -e kakinSS ];
then
	cat kakinSS | while read ssuuid
	do
		mysql -u root -h $DBIP dr -N -B -e "update dr.recovery_temporary_snapshots set removed = NOW()  - interval '9' hour where uuid = '${ssuuid}'"
	done
	rm -f kakinSS
fi

clean_tmpfile


############# ########################
# temporary_ゴミROOTSS削除
############# ########################

# 表示
desplay_SS() {
	echo "Snapshot_uuid = ${1}"
	echo "Snapshot_name = ${2}"
}
desplay_VOL() {
	echo "Volume_uuid = ${1}"
	echo "Volume_name = ${2}"
}
desplay_acdom() {
	echo "account_name  = ${1}"
	echo "domain_name   = ${2}"
}


# スナップショット削除処理
# 引数1_account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
delete_ssApi() {

	./kick_api.sh command=deleteSnapshot id=$5 >> $LOGFILE 2>&1
	mysql -u root -h $DBIP dr -e "insert into recovery_temporary_snapshots(account_uuid, account_name, domain_name, zone_uuid, uuid, name , created, removed) values ('${1}', '${2}', '${3}', '${4}', '${5}', '${6}',  '${7}', NOW() - interval '9' hour);"
}

# volume削除処理
# 引数1account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
delete_volApi() {

	./kick_api.sh command=deleteVolume id=$5 >> $LOGFILE 2>&1
	mysql -u root -h $DBIP dr -e "insert into recovery_temporary_volumes(account_uuid, account_name, domain_name, zone_uuid, uuid, name , created, removed) values ('${1}', '${2}', '${3}', '${4}', '${5}', '${6}',  '${7}', NOW() - interval '9' hour );"
}

# ROOTボリュームの復元の時の削除対象一時スナップショットの取得  account_idは番号
get_cloud_SS() {
	local R=`mysql -u root -h $DBIP cloud -N -B -e "select  snap.uuid, vm.account_id,  vm.data_center_id,  snap.name, DATE_FORMAT(snap.created,'%Y/%m/%d-%k:%i:%s') from cloud.vm_instance as vm  join cloud.vm_template as temp on vm.vm_template_id = temp.id  join cloud.volumes as vol on vm.id = vol.instance_id  join cloud.snapshots as snap on vol.id = snap.volume_id  where vm.name like 'v-site%'  and vol.volume_type like 'ROOT'  and snap.status like 'BackedUp'  and temp.name like 'DR_Template' and snap.removed is null;"`
	echo "Get gomi_ROOT_temporary :   snap.uuid, vm.account_id,  vm.data_center_id,  snap.name, snap.created " >> $LOGFILE
	# SQLで返った値を各行ごとに成型
	local arry=( `echo $R` )
	for (( i=0; i < ${#arry[@]}; i++ ))
	do
		let local count=\(${i}+1\)%5

		if [ ${count} -eq 0 ]; then
			echo "${arry[$i]} "
			# account_uuidとして取得
		elif [ ${count} -eq 2 ]; then
			a=`mysql -u root -h $DBIP cloud -N -B -e "select  uuid from cloud.account where id = '${arry[$i]}';"`
			echo -n "${a} "
		elif [ ${count} -eq 3 ]; then
			a=`mysql -u root -h $DBIP cloud -N -B -e "select  uuid from cloud.data_center where id = '${arry[$i]}';"`
			echo -n "${a} "
		else
			echo -n "${arry[$i]} "
		fi
	done
}

# dr.recovery_auto_jobs 取得
#select  ac.id, dc.id
#from dr.recovery_auto_jobs jobs
#join cloud.account ac on  ac.uuid = jobs.account_uuid
#join cloud.data_center dc on dc.uuid = jobs.zone_uuid
#where jobs.end_time is null
get_recovery_auto_jobs() {
	R=`mysql -u root -h $DBIP dr -N -B -e "SELECT account_uuid, zone_uuid FROM dr.recovery_auto_jobs jobs where jobs.end_time is null;"`
	echo "Get recovery_auto_jobs_Table :  account_id, zone_id" >> $LOGFILE
	# SQLで返った値を各行ごとに成型
	local arry=( `echo $R` )
	for (( i=0; i < ${#arry[@]}; i++ ))
	do
		let local count=\(${i}+1\)%2

		if [ ${count} -eq 0 ]
		then
			echo "${arry[$i]} "

		else
			echo -n "${arry[$i]} "
		fi
	done
}

# dr.recovery_manual_jobs 取得
get_recovery_manual_jobs() {
	R=`mysql -u root -h $DBIP dr -N -B -e "SELECT account_uuid, zone_uuid FROM dr.recovery_manual_jobs jobs where jobs.end_time is null;"`
	echo "Get recovery_manual_jobs_Table :  account_id, zone_id" >> $LOGFILE
	# SQLで返った値を各行ごとに成型
	local arry=( `echo $R` )
	for (( i=0; i < ${#arry[@]}; i++ ))
	do
		let local count=\(${i}+1\)%2

		if [ ${count} -eq 0 ]
		then
			echo "${arry[$i]} "

		else
			echo -n "${arry[$i]} "
		fi
	done
}

# recoveryDRserver.sh
# source_snapshotsから一時ボリューム名を取得する関数
# 引数 $BID
get_date_sourceSSList() {
		echo "Get DATA temporary_Volume_name" >> $LOGFILE
	#auto, manual jobsにアクセス
	R=`mysql -u root -h $DBIP dr -N -B -e "select jobs.source_snapshot_uuid from dr.recovery_manual_jobs jobs  where jobs.batch_server_id = $BID and  jobs.state='Error';"`
	D=`mysql -u root -h $DBIP dr -N -B -e "select jobs.source_snapshot_uuid from dr.recovery_auto_jobs jobs  where jobs.batch_server_id = $BID and  jobs.state='Error';"`
	local arry=( `echo $R $D` )
	for (( i=0; i < ${#arry[@]}; i++ ))
	do
			# 元スナップショット名から、元ボリューム名を取得
			volname=`mysql -u root -h $MirrorDBIP -P 3307 cloud -N -B -e "select ss.name ssvol, vol.name volname from cloud.snapshots ss join cloud.volumes vol on ss.volume_id = vol.id where vol.volume_type = 'DATADISK'  and  ss.uuid = '${arry[$i]}' group by volname;"`
			#volname=`mysql -u root -h $MirrorDBIP -P 3307 cloud -N -B -e "select concat(substring(ss.name, 1,  LOCATE( vol.name, ss.name)-12), '_', vol.name) as groupvol from cloud.snapshots ss join cloud.volumes vol on ss.volume_id = vol.id where vol.volume_type = 'DATADISK'  and  ss.uuid = '${arry[$i]}' group by groupvol;"`
			# 元スナップショット名、元ボリューム名から削除対象の一時ボリューム名を取得
			local ssvollist=( `echo $volname` )
			if [ "${volname}" != "" ]
			then
				if [[ "$ssvollist[0]}" =~ ^detached_.* ]]; then
					vol=`echo  "${ssvollist[1]}"`
				else
					vol=`echo  "${ssvollist[0]}" | sed  -e "s/-M[0-9]\{8\}_${ssvollist[1]}_[0-9]\{14\}-*[0-9]\{,3\}$/_${ssvollist[1]}/"`
				fi
				if [ -e deleteVolList ];
				then
					cat deleteVolList |grep "${vol}" >> /dev/null
					if [ $? != 0 ]; then
						echo "${vol}" >> deleteVolList
					fi
				else
					echo "${vol}" >> deleteVolList
				fi
			fi
	done
}

get_data_nullsnapshotsjobs() {
	R=`mysql -u root -h $DBIP dr -N -B -e "select ac.uuid , ac.account_name, ac.domain_id, dc.uuid, ss.uuid, ss.name, DATE_FORMAT(ss.created ,'%Y/%m/%d-%k:%i:%s') from cloud.snapshots ss join cloud.volumes vol  on vol.id = ss.volume_id join cloud.account ac   on ac.id = ss.account_id join cloud.data_center dc on dc.id = ss.data_center_id where ss.status != 'Destroyed' and ( vol.uuid in ( select temporary_volume_uuid  from dr.recovery_auto_jobs where  state = 'Error' and ( volume_type is null or  volume_type != 'ROOT') and temporary_volume_uuid is not null and temporary_snapshot_uuid is null  ) or vol.uuid in ( select temporary_volume_uuid from dr.recovery_manual_jobs where  state = 'Error' and ( volume_type is null or  volume_type != 'ROOT') and temporary_volume_uuid is not null and temporary_snapshot_uuid is null  )) ;" `
	echo "Get recovery_manual_jobs_Table :  account_id, zone_id" >> $LOGFILE
	# SQLで返った値を各行ごとに成型
	local arry=( `echo $R` )
	for (( i=0; i < ${#arry[@]}; i++ ))
	do
		let local count=\(${i}+1\)%7

		if [ ${count} -eq 0 ]
		then
			echo "${arry[$i]} "

		else
			echo -n "${arry[$i]} "
		fi
	done
}


MNUM=""
get_MNumber() {
	MNUM=`mysql -u root -h $DBIP dr -N -B -e "select dom.name from cloud.domain dom join cloud.account ac on dom.id = ac.domain_id where ac.uuid = '${1}';"`
}
############# ROOT_SS 削除対象判断  ########################
echo "---------------------------" >> $LOGFILE
echo "gomi_temporary_delete_Check START" >> $LOGFILE
echo "temporary_ROOT_SS_DELETE Check START"  >> $LOGFILE
# job テーブルの取得
get_recovery_auto_jobs > jobsList
get_recovery_manual_jobs >> jobsList
# jobsList作成のため、ダミーデータ
echo "aa bb" >> jobsList

echo "temporary_ROOT_SS_DELETE jobs_Check START"  >> $LOGFILE
get_cloud_SS > rootSSList
cat rootSSList | while read snap_uuid account_id  data_center_id  snap_name  snap_created
do
	# jobs除外対象かどうか判断(account_uuid, zone_uuidが同じ場合)
	Count=0;
	while read jobs_ac jobs_zonevi
	do
		if [ "${account_id}" =  "${jobs_ac}" ];
		then
			let Count=${Count}+1
			echo "snapshots_exclude : ${snap_uuid}" >> $LOGFILE
			get_MNumber "${account_id}"
			echo "${MNUM}は処理中のため削除処理をスキップします。再度実行してください"
			echo "${MNUM} ${snap_uuid} ${snap_name}" >> ${SSfileName}
			break;
		fi
	done < jobsList
	# 上に等しいものが存在しないとき 削除対象SS
	if [ ${Count} -eq 0 ]
	then
		# dr.recovary_temporary_snapshotsにデータをいれるため、取得
		# delete_snapshotsの中身 ${snap_uuid} ${account_id} ${data_center_id} ${snap_name} ${account_name} ${domain_name}
		acdom=`mysql -u root -h $DBIP dr -N -B -e "SELECT ac.account_name, dom.name FROM cloud.account ac join cloud.domain dom on ac.domain_id = dom.id where ac.uuid = '${account_id}';"`
		echo "${snap_uuid} ${account_id} ${data_center_id} ${snap_name} ${acdom} ${snap_created}" >> delete_ROOTsnapshots;
	fi
done

echo "temporary_ROOT_SS_DELETE jobs_Check FINISH"  >> $LOGFILE
echo "temporary_ROOT_SS_DELETE Check FINISH"  >> $LOGFILE

# 一時ファイルが存在するかどうか確認
if [  -e rootSSList ];
then
	rm -f rootSSList
fi

############# DATA_SS,VOLUME 削除対象判断  ########################
echo "temporary_DATA_SS,VOLUME_DELETE Check START"  >> $LOGFILE
#recoveryDRserver.shの実行結果から復元一時VOLUME名が手に入る
get_date_sourceSSList ${BID}
# 一時ボリュームと一時スナップショットを一つずつ取得(7列)
# deleteVolListが存在するかどうか確認
if [  -e deleteVolList ];
then
	cat deleteVolList | while read volname
	do
	# ボリューム名から必要情報の取得
		base_data=`mysql -u root -h $DBIP dr -N -B -e "select  vol.account_id, vol.domain_id, dc.uuid, vol.uuid, vol.name, DATE_FORMAT(vol.created ,'%Y/%m/%d-%k:%i:%s'), snap.uuid, snap.name, DATE_FORMAT(snap.created ,'%Y/%m/%d-%k:%i:%s'), vol.instance_id from cloud.volumes vol left join cloud.snapshots as snap on vol.id = snap.volume_id  left join cloud.data_center dc on vol.data_center_id = dc.id where  vol.name = '${volname}'  and vol.volume_type = 'DATADISK';" `
	# 配列が一行分より多い場合、削除しないで、同じ名前だったことを表示
		arryList=( `echo $base_data` )
		acuuid=`mysql -u root -h $DBIP dr -N -B -e "SELECT ac.uuid FROM cloud.account ac  where ac.id = '${arryList[0]}';" `

		if [ ${#arryList[@]} -gt 10 ];
		then
			#ここに削除しない旨と、そのvolume名を表示
			errorvoluuid=`mysql -u root -h $DBIP cloud -N -B -e "SELECT vol.uuid from cloud.volumes vol where vol.name ='${volname}' and removed is null;" `
			if [ "${errorvoluuid}" != "" ]; then
				echo "NOT DELETE exist Same Volume name : ${arryList[4]}" >> $LOGFILE
				get_MNumber "${acuuid}"
				echo "${MNUM} ${volname}  -  削除対象外"
				echo "${MNUM} ${volname}  -  削除対象外" >> ${VolFileName}
				echo "  Volume_uuid = ${errorvoluuid}"
				echo "  Volume_uuid = ${errorvoluuid}"  >> ${VolFileName}
				echo "  Volume_uuid = ${errorvoluuid}"  >> $LOGFILE
			fi
			continue;
		else

	# vol.account_id, vol.domain_id, vol.data_center_id, vol.uuid, vol.name, vol.created, snap.uuid, snap.name, snap.created, instance_id
			if [ "${base_data}" != "" ];
			then
				#
				removed=`mysql -u root -h $DBIP dr -N -B -e "SELECT removed FROM cloud.volumes ac  where uuid = '${arryList[3]}';" `
				if [ "${removed}" = "NULL" ];
				then
					echo "${acuuid} ${arryList[1]} ${arryList[2]} ${arryList[3]} ${arryList[4]} ${arryList[5]} ${arryList[6]} ${arryList[7]} ${arryList[8]} ${arryList[9]} ${arryList[10]}" >> delete_SS_VOL
				fi
			fi
		fi
	done
	rm -f deleteVolList
fi


# delete_SS_VOLからjob確認
if [  -e delete_SS_VOL ];
then
	echo "temporary_DATA_SS,VOLUME_DELETE jobs_Check START"  >> $LOGFILE
# job テーブルの取得
	get_recovery_auto_jobs > jobsList
	get_recovery_manual_jobs >> jobsList
	echo "aa bb">> jobsList


	cat delete_SS_VOL | while read account_id domain_id data_center_id vol_uuid vol_name vol_created snap_uuid snap_name snap_created instance_id
	do
		Count=0;
		while read ${jobs_ac}
		do
			if [ "${account_id}" =  "${jobs_ac}" ];
			then
				let Count=${Count}+1
				echo "volumes_exclude : account=${account_id} volume_uuid=${vol_uuid} volume_name=${vol_name}" >> $LOGFILE
				get_MNumber "${account_id}"
				echo "${MNUM} ${vol_name}  -  削除対象外"
				echo  "${MNUM} ${vol_name}  -  削除対象外" >> ${VolFileName}

				#SSのがあれば判定
				if [ "${snap_uuid}" -ne "NULL"]
				then
					echo "snapshot_exclude : account=${account_id} snapshot_uuid=${snap_uuid} snapshot_name=${snap_name}" >> $LOGFILE
				fi
				break;
			fi
		done < jobsList
	# 上に等しいものが存在しないとき 削除対象VOL or SS
		if [ ${Count} -eq 0 ];
		then
			# dr.recovary_temporary_snapshotsにデータをいれるため、取得
			# delete_DATAsnapshotsの中身 ${snap_uuid} ${account_id} ${data_center_id} ${snap_name} ${account_name} ${domain_name} ${instance_id}
			acdom=`mysql -u root -h $DBIP dr -N -B -e "SELECT ac.account_name, dom.name FROM cloud.account ac join cloud.domain dom on ac.domain_id = dom.id where ac.uuid = '${account_id}';" `
			echo "${account_id} ${domain_id} ${data_center_id} ${vol_uuid} ${vol_name} ${vol_created} ${snap_uuid} ${snap_name} ${acdom} ${snap_created} ${instance_id}" >> delete_DATA
		fi
	done

	echo "temporary_DATA_SS,VOLUME_DELETE jobs_Check FINISH"  >> $LOGFILE
else # recoveryDRserver.shで取れてこなかった場合に来る分岐
	echo "not found temporary_delete_snapshots and temporary volume from DATA_VOLUME" >> $LOGFILE
fi

# deleteVolListが存在するかどうか確認
rm -f jobsList
if [ -e delete_SS_VOL ];
then
	rm -f delete_SS_VOL
fi


echo "temporary_DATA_SS,VOLUME_DELETE Check FINISH"  >> $LOGFILE

############# 対象削除開始  ########################
############  ROOTディスクからのゴミ一時SS削除 ######${snap_uuid} ${account_id} ${data_center_id} ${snap_name} ${acdom} ${snap_created}"
if [ -e delete_ROOTsnapshots ];
then
	echo "temporary_ROOTTEMPORARY_SS_DELETE START" >> $LOGFILE

	exec 3<&0 < delete_ROOTsnapshots
	lineCount=0
	while read  snap_uuid account_id data_center_id snap_name account_name domain_name snap_created
	do
		let lineCount=${lineCount}+1
		desplay_acdom ${account_name} ${domain_name}
		desplay_SS ${snap_uuid}  ${snap_name}
		echo -n "Delete Temporary Snapshots Resource: [y/n]? : "
		exec 0<&3 3<&-
		read val1
		exec 3<&0 < delete_ROOTsnapshots
		case "${val1}" in
		      "y" | "yes" | "Y" | "YES" )
		    # 引数1_account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
				delete_ssApi ${account_id} ${account_name} ${domain_name} ${data_center_id} ${snap_uuid} ${snap_name} ${snap_created}
		;;
		esac
		for (( i=0; i < ${lineCount}; i++ ))
			do
				read line
		done
	done
	exec 0<&3 3<&-

	rm -f delete_ROOTsnapshots
	echo "temporary_ROOTTEMPORARY_SS_DELETE END" >> $LOGFILE
else
	echo "NOT TARGET Delte ROOT_temporary_SS" >> $LOGFILE
fi

############  DATAディスクからのゴミ一時ボリューム, スナップショット削除 ######
if [ -e delete_DATA ];
then
	# "${account_id} ${domain_id} ${data_center_id} ${vol_uuid} ${vol_name} ${vol_created} ${snap_uuid} ${snap_name} ${acdom} ${snap_created} ${instance_id}"
	echo "temporary_DATATEMPORARY_SS_VOL_DELETE START" >> $LOGFILE
	exec 3<&0 < delete_DATA
	lineCount=0
	while read  account_id domain_id data_center_id vol_uuid vol_name vol_created snap_uuid snap_name account_name domain_name snap_created instance_id
	do
		let lineCount=${lineCount}+1
		desplay_acdom ${account_name} ${domain_name}
		desplay_VOL ${vol_uuid} ${vol_name}
		desplay_SS ${snap_uuid}  ${snap_name}
		echo -n "Delete Temporary Snapshots and volumes Resource: [y/n]? : "
		exec 0<&3 3<&-
		read val1
		exec 3<&0 < delete_DATA
		case "${val1}" in
			  "y" | "yes" | "Y" | "YES" )
				# 12${instance_id} からデタッチするかどうか決める
				if [ "${instance_id}" != "NULL" ] ;
				then
						./kick_api.sh command=detachVolume id=${vol_uuid} >> $LOGFILE
					echo -n ".. detaching(sleep 10) "
					sleep 10;
				fi

				#account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
				if [ "${vol_uuid}" != "NULL" ] ;
				then
					delete_volApi ${account_id} ${account_name} ${domain_name} ${data_center_id} ${vol_uuid} ${vol_name} ${vol_created}
				fi
				#SSはない可能性があるのでその判定をいれる
				if [ "${snap_uuid}" != "NULL" ] ;
				then
					# account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
					delete_ssApi ${account_id} ${account_name} ${domain_name} ${data_center_id} ${snap_uuid} ${snap_name} ${snap_created}
				fi
			;;
		esac
		for (( i=0; i < ${lineCount}; i++ ))
			do
				read line
			done
	done
	exec 0<&3 3<&-
	rm -f delete_DATA
	echo "temporary_DATATEMPORARY_SS_VOL_DELETE END" >> $LOGFILE
else
	echo "NOT TARGET DELETE DATATEMPORARY_SS_VOL" >> $LOGFILE
fi


############  DATAディスク 一時ボリュームは入力されているが、一時スナップショットがNULLの時削除 ######
get_data_nullsnapshotsjobs > nullsnapshotsList
# 対象データの取得
if [ -e nullsnapshotsList ];
then
echo "temporary_DATATEMPORARY_SS_DELETE START" >> $LOGFILE
exec 3<&0 < nullsnapshotsList
lineCount=0
while read ac_uuid  account_name domain_id dc_uuid snap_uuid snap_name snap_created
do
	domain_name=`mysql -u root -h $DBIP cloud -N -B -e "SELECT dom.name FROM cloud.domain dom  where dom.id = '${domain_id}';" `
	let lineCount=${lineCount}+1
	desplay_acdom ${account_name} ${domain_name}
	desplay_SS ${snap_uuid}  ${snap_name}
	echo -n "Delete Temporary Snapshots : [y/n]? : "
	exec 0<&3 3<&-
	read val1
	exec 3<&0 < nullsnapshotsList
	case "${val1}" in
			  "y" | "yes" | "Y" | "YES" )
				#SSはない可能性があるのでその判定をいれる
					# account_uuid, 2_account_name, 3_domain_name, 4_zone_uuid, 5_uuid, 6_name , 7_created, removed
					delete_ssApi ${ac_uuid} ${account_name} ${domain_name} ${dc_uuid} ${snap_uuid} ${snap_name} ${snap_created}
			;;
	esac
	for (( i=0; i < ${lineCount}; i++ ))
	do
		read line
	done
done
exec 0<&3 3<&-

rm -f nullsnapshotsList
echo "temporary_DATATEMPORARY_SS_DELETE START" >> $LOGFILE
else
	echo "NOT TARGET DELETE DATATEMPORARY_SS" >> $LOGFILE
fi

end_log
echo "Complete."
exit 0
