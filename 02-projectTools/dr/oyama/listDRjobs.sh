#!/bin/bash
# listDRjobs.sh
# for creationline, inc.
# ver 1.1

# DB Server IP adress
DBIP=ck2ejo2-pcldb00

# Mirror DB Server IP adress
MirrorDBIP=10.195.0.20

# display time
time=$1

# check parameter
if [ $# -lt 1 ]; then
    printf "引数に謝りがあります。引数として時間を指定してください。\n"; exit 1;
fi

echo " $time時間分の履歴を表示します"

get_account_name() {
    R=`mysql -u root -h $DBIP cloud -N -B -e "select a.account_name,d.name from account as a, domain as d where a.domain_id=d.id and a.uuid = '$1';"`
    R=`echo $R |sed 's/ /@/'`
    echo $R
}

get_source_snapshot_name() {
    R=`mysql -u root -h $MirrorDBIP -P 3307 cloud -N -B -e "select name from snapshots where uuid = '$1';"`
    echo $R
}

get_batch_server_name() {
    R=`mysql -u root -h $DBIP dr -N -B -e "select name from recovery_batch where id = '$1';"`
    echo $R
}

get_Partition() {
   if [ $1 -lt 3 ]; then
    R=$1
    echo $R
else
    R=`mysql -u root -h $DBIP dr -N -B -e "select partition_id from recovery_auto_interval where id = '$1';"`
    echo $R
fi
}

print_tmp_jobs() {
  cat tmp_jobs | while read line
  do
    ID=`echo ${line} |awk '{print $1}'`
    A_UUID=`echo ${line} |awk '{print $2}'`
    A_NAME=`get_account_name $A_UUID`
    SS_UUID=`echo ${line} |awk '{print $3}'`
    SS_NAME=`get_source_snapshot_name $SS_UUID`;
    BATCH_SERVER_ID=`echo ${line} |awk '{print $4}'`
    BATCH_SERVER_NAME=`get_batch_server_name $BATCH_SERVER_ID`;
    STATE=`echo ${line} |awk '{print $5}'`
    STATEDETAILS=`echo ${line} |awk '{print $6}'`
    PARTITION_ID=`echo ${line} |awk '{print $7}'`
    PARTITION=`get_Partition $PARTITION_ID`;
    START_TIME=`echo ${line} |awk '{print $8" "$9}'`
    END_TIME=`echo ${line} |awk '{print $10" "$11}'`
 printf "$BATCH_SERVER_NAME  $A_NAME %-2s $SS_NAME%-6s $STATE %-2s $STATEDETAILS %-14s $PARTITION %-5s $START_TIME %-2s $END_TIME \n"
  done
}

# fetch manual jobs in $time hour
mysql -u root -h $DBIP dr -N -B -e "select id,account_uuid,source_snapshot_uuid,batch_server_id,state,statedetails,partition_id,start_time + interval '9' hour,end_time + interval '9' hour from recovery_manual_jobs where start_time + interval '9' hour > now() - interval $time hour;" > tmp_jobs;

printf "[ Manual Recovery Jobs ]\n"
printf "BatchServer  Account %-14s SourceSnapshot %-45s State %-5s StateDetails %-10s Partition %-5s StartTime %-12s EndTime \n"
print_tmp_jobs;

# fetch auto jobs in $time hour
mysql -u root -h $DBIP dr -N -B -e "select id,account_uuid,source_snapshot_uuid,batch_server_id,state,statedetails,auto_interval_id,start_time + interval '9' hour,end_time + interval '9' hour from recovery_auto_jobs where start_time + interval '9' hour > now() - interval $time hour;" > tmp_jobs;
printf "[ Auto Recovery Jobs ]\n"
printf "BatchServer  Account %-14s SourceSnapshot %-45s State %-5s StateDetails %-10s Partition %-5s StartTime %-12s EndTime \n"
print_tmp_jobs;

rm -f tmp_jobs;