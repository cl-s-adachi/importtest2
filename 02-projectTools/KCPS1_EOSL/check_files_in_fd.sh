#!/bin/bash

# DBサーバのIPアドレス、もしくはホスト名
DB_HOST="172.22.132.84"


#######################################
# mysqlコマンドを実行する。
#
# グローバル変数:
#   DB_HOST
# 引数:
#   $1: SQL文
#       注意: ダブルクォーテーションは使わないこと
# 戻り値:
#   タブ区切りのクエリー結果
#   selectの場合、ヘッダーは表示させない。
#######################################
function exec_mysql() {
  local sql="$1"
  mysql -u root -h ${DB_HOST} cloud -N -e "${sql}"
}



#######################################
# main()関数
#######################################
function main() { 
    # ゾーン確認
    zones="$(exec_mysql 'select name from data_center where removed is null')"

    echo "以下のゾーンのシステムVMを確認します。"
    for zone in "${zones}"
    do
        echo "${zone}"
    done
    echo ""

    # 各ゾーンでシステムVMを確認する。
    for zone in ${zones}
    do
        echo "${zone}のシステムVMを確認します。"
        vms=$(exec_mysql "select private_ip_address from vm_instance where removed is null and type not in ('User', 'DomainRouter') and data_center_id = (select id from data_center where removed is null and name = '${zone}');")
        
        # 該当ゾーンの全てのシステムVMを確認する。
        for vm in ${vms}
        do
            # agentのPIDを取得
            uname=$(ssh -i /var/cloudstack/management/.ssh/id_rsa -o StrictHostKeyChecking=no -p 3922 ${vm} 'uname -n')
            agent_pid=$(ssh -i /var/cloudstack/management/.ssh/id_rsa -o StrictHostKeyChecking=no -p 3922 ${vm} 'ps -ef | grep cloud | grep -v grep')
            agent_pid=$(echo ${agent_pid} | awk '{ print $2}')

            # fd配下のファイル数確認
            cmd_for_files="ls -1 /proc/${agent_pid}/fd | wc -l"
            files=$(ssh -i /var/cloudstack/management/.ssh/id_rsa -o StrictHostKeyChecking=no -p 3922 ${vm} "${cmd_for_files}")

            # 確認結果を出力
            echo ${uname} ${files}
        done
        echo ""
    done
}

# main()関数を呼び出す。
main
