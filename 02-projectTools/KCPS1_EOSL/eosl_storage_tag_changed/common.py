#!/usr/bin/python
# -*- coding: UTF-8 -*-

r"""このモジュールは以下のツールが利用する関数を纏めたモジュールである。

* プライマリストレージタグ変更ツール(storage_tag_changed.py)

"""

import csv
import subprocess
import sys
import re

# テストコード実行時にはインポートしない。
try:
    import mysql.connector
except:
    pass

import config


def changed_ps_tag(ps_tag_list):
    r"""
    プライマリストレージのストレージタグ名を変更する。


    Parameters
    ----------
    ps_tag_list: list
        [ ['PRIMARYSTORAGE_NAME', 'PRIMARYSTORAGE_TAG'], ...]

    Returns
    -------
    変更が失敗したプライマリストレージ情報: list

    Notes
    -----
    リスト「ps_tag_list」の最初の要素はヘッダーの想定なので無視する。

    """

    # 登録の失敗したストレージ処理用変数
    pses_failed = []
    ps_failed = []

    # kick_api.sh実行時のパラメータ
    command = 'updateStoragePool'
    id = ''
    tags = ''

    # kick_api.sh実行結果保存用変数
    kick_api_result = ()


    # リスト「ps_tag_list」の各行を処理する。
    for row in ps_tag_list:

        print '次のプライマリストレージのストレージタグ名を変更します。'
        print row
        print ''

        # CSDBから必要な情報を取ってくる。
	psid = get_id_by_name('storage_pool', row[0])

        # kick_api.sh実行用パラメータを準備する。
        kick_api_params = [
            'command=' + command,
            'id=' + psid,
            'tags=' + row[1],
        ]

        # kick_api.sh実行
        print kick_api_params
        kick_api_result = exec_kick_api(kick_api_params)
        kick_api_result = kick_api_result[0].splitlines()
        print kick_api_result
        print ''

        # kick_api.sh実行結果確認
        for result_row in kick_api_result:
            # 実行結果に「errortext」という文字列があれば失敗とする。
            if 'errortext' in result_row:
                ps_failed = row
                ps_failed.append(result_row)
                pses_failed.append(ps_failed)

    return pses_failed


def check_columns(a_list):
    r"""
    リスト内のリストにNone、または空白のStringが存在するか、
    また、リスト内と各リストの要素数が同じか確認する。

    Parameters
    ----------
    a_list: list
        確認対象リスト

    Returns
    -------
    True: boolean
        確認で問題がない場合はTrueを返す。
    False: boolean
        該当のリスト内リストを返す。
    """

    first_row = True
    element_num = 0

    for row in a_list:
        if first_row == True:
            element_num = len(row)
            first_row = False
        else:
            for col in row:
                if col == None or col == '':
                    return row

    return True


def load_csv_as_list_gen_all(filename):
    r"""
    CSVファイルを読み込んでリストで返す。

    Parameters
    ----------
    filename: str
        ファイル名(フルパス可能)

    Yields
    ------
    row: list
        ファイルの各行を1行ずつ返す。

    """
    # ヘッダーを無視する為に使われる。
    first_row = True

    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            # リスト「row」は以下のようになっている想定。
            # [ ['PRIMARYSTORAGE_NAME', 'PRIMARYSTORAGE_TAG'], ...]

            # 最初の行はヘッダーなので無視する。
            if first_row == True:
                first_row = False
                continue

            # 空白行があったら無視する。
            # この分岐を追加したのは、CSVファイルの最後の行の後ろに改行コードがあると、空白行が追加される為。
            if row == []:
                continue
            yield row


def continue_or_stop(message):
    r"""
    ツールを終了するか確認する。

    質問にy以外の文字列を入力した場合はスクリプトを終了させる。

    Parameters
    ----------
    message: str
        質問内容

    Returns
    -------
    True: boolean
        質問にyを入力するとTrueを返す。
    False: boolean
        質問にy以外を入力するとFalseを返す。

    Notes
    -----
    Todo: y以外の場合はFalseを返す(この関数を使うスクリプトを修正する必要がある)。

    """

    answer = ''

    answer = raw_input(message + '[y/N] ')

    if answer == 'y':
        return True
    else:
        return False



def exec_mysql(params):
    r"""
    mysqlコマンドを実行します。

    Parameters
    ----------
    params: dict
        mysqlコマンド実行時に必要な引数

    Returns
    -------
    SQL文実行結果: list
        [ ['1行目1列目', '1行目2列目', ... ], ['2行目1列目', '2行目2列目', ... ], ... ]

    """

    operation = params['sql'].split()[0].lower()

    # selectとupdateと処理が若干違うので分岐を作る。
    if operation == 'select':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        result = cursor.fetchall()

        cursor.close()
        cnx.close()

        # tupleをlistに、Noneをstrの'NULL'にする。
        return manipulate_mysql_result(result)

    elif operation == 'update':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        cnx.commit()

        cursor.close()
        cnx.close()

        return True

    else:
        raise Exception('SQL文がSelect、もしくはUpdateではありません。指定されたのは: ' + operation)
        sys.exit(1)


def manipulate_mysql_result(raw_result):
    r"""
    mysql実行結果の各行をlistに変換し、Noneは'NULL'に変換する。

    Parameters
    ----------
    raw_result: list
        mysql.connectorでのfetchall()の結果

    Returns
    -------
    上記説明参照: result

    """

    result = []
    for i in raw_result:
        result.append(list(i))

    for i in range(len(result)):
        for j in range(len(result[i])):
            if result[i][j] == None:
                result[i][j] = 'NULL'
            if isinstance(result[i][j], int):
                result[i][j] = str(result[i][j])

    return result


def get_id_by_name(table,name):
    r"""
    DBの特定のテーブルで、名前(name)からID(id)を取得する。
    削除処理されたレコード(removed = NULL)は無視する。

    Parameters
    ----------
    table: str
        テーブル名
    name: str
        フィールド「name」に指定する名前

    Returns
    -------
    'UUID': str
        一致する結果が一個のみの場合に該当のUUIDを返す。
    False: boolean
        一致する結果が複数の場合に返す。
    'NULL': str
        結果がない場合に返す。
    """

    db_params = {
        'db_host' : config.DB_HOST,
        'db_user' : 'root',
        'database' : 'cloud',
        'sql' : 'select id from ' + table + ' where removed is null and name = "' + name + '";',
    }

    result = exec_mysql(db_params)

    if len(result) > 1:
        return False
    elif len(result) == 1 and len(result[0]):
        return result[0][0]
    else:
        return 'NULL'


def load_csv_as_list_gen_col(filename):
    r"""
    CSVファイルを読み込んでリストで返す。

    Parameters
    ----------
    filename: str
        ファイル名(フールパス可能)

    Yields
    ------
    row: list
        ファイルの各行を1行ずつ返す。

    """
    lines = []

    # ヘッダーを無視する為に使われる。
    first_row = True

    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            # 最初の行はヘッダーなので無視する。
            if first_row == True:
                first_row = False
                continue

            # 空白行があったら無視する。
            # この分岐を追加したのは、CSVファイルの最後の行の後ろに改行コードがあると、空白行が追加される為。
            if row == []:
                continue
            lines.append(row[0])
    return lines

def list_to_where_in(ids, double_quote=False):
    '''
    複数のID情報(数字)を持っている変数をwhere文節向けに変換する。


    Parameters
    ----------
    ids: list
        変換するIDの一覧

    Returns
    -------
    変換されたlist: str
        'ID1,ID2,ID3, ...'

    Notes
    -----
    文字列には対応させるには``double_quote``を``True``にする必要がある。

    '''
    result = ''

    if isinstance(ids[0], list):
        result = ','.join([x[0] for x in ids])
    elif isinstance(ids[0], str):
        result = ','.join(ids)

    if double_quote == False:
        return result
    else:
        result = re.sub(',', '","', result)
        result = re.sub(r'^', '"', result)
        result = re.sub(r'$', '"', result)
        return result



def get_sp_ids_by_names(sp_names):
    """
    プライマリストレージの名前でプライマリストレージ情報を取得する。

    Parameters
    ----------
    vm_names: list
        インスタンス名

    Returns
    -------
    exec_mysql()の戻り値: list
        [ ['プライマリストレージid'], ...]

    """

    sp_names_where_in = list_to_where_in(sp_names, True)

    db_params = {
        'db_host' : config.DB_HOST,
        'db_user' : 'root',
        'database' : 'cloud',
        'sql' : 'select id from storage_pool where removed is null and name in ( %(sp_names)s );' % {
            'sp_names' : sp_names_where_in,
            },
    }

    return exec_mysql(db_params)


def get_sp_info_a_by_ids_states(sp_ids):
    r"""
    指定されたプライマリストレージの名前、ID、タグ情報を取得する。

    Parameters
    ----------
    vm_ids: list
        インスタンスID(str)
    states: str or list
        インスタンスの状態(vm_instance.state)(str)

    Returns
    -------
    exec_mysql()の戻り値: list
        [ ['プライマリストレージ名', 'プライマリストレージID', 'プライマリストレージタグ名'], ...]

    """

    sp_ids_where_in = list_to_where_in(sp_ids)

    vms_db_params = {
        'db_host' : config.DB_HOST,
        'db_user' : 'root',
        'database' : 'cloud',
        'sql' : 'select sp.name,sp.id,st.name from storage_pool sp LEFT JOIN storage_tag_view st ON sp.id=st.pool_id where sp.id in ( %(sp_ids)s ) order by sp.name;' % {
            'sp_ids' : sp_ids_where_in,
        },
    }

    return exec_mysql(vms_db_params)


def search_in_list(listA, listB):
    r"""
    listBにないlistAの要素を洗い出す。

    Parameters
    ----------
    listA: list
    listB: list

    Returns
    -------
    listBにないlistAの要素: list

    """

    not_exist = []
    toggle = False

    for i in listA:
        toggle = False
        for j in listB:
            if i == j:
                toggle = True
                break
        if toggle == False:
            not_exist.append(i)

    return not_exist


def exec_kick_api(params):
    r"""
    kick_api.shを実行する。

    Parameters
    ----------
    params: list
        kick_api.shに指定するパラメータ
        ( 'パラメータ名=値', ... )

    Returns
    -------
    kick_api.shの実行結果: tuple
        ('レスポンス(XML)', 'curlコマンドの実行ステータス')

    """

    params.insert(0, config.KICK_API)
    command = params

    # コマンドを実行する。
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()

    # コマンド実行結果を回収する。
    p_stdout, p_stderr = p.communicate()

    return (p_stdout, p_stderr)

