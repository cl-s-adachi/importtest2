#!/usr/bin/python
# -*- coding: UTF-8 -*-

r"""プライマリストレージタグ変更ツール
"""

import argparse
import sys

import common
import pretty_table


def main(args):
    r"""
    main関数

    Parameters
    ----------

    args: namespace
        argparseモジュールで取得した、コマンド実行時に指定したパラメータ

    """
    line_a = '********************************************************************************'

    #コマンド実行時に指定したファイル内の情報を取得
    list_file = args.file_path

    print line_a
    print 'ファイルを読み込みます。'
    print ''
    pses_pre_load = common.load_csv_as_list_gen_all(list_file)
    
    #各行の列数を確認
    print '各行の列数を確認します。'
    print ''
    verify_csv = common.check_columns(pses_pre_load)
    if verify_csv != True:
        print '以下の行が正常ではありません。ツールを終了します。'
        print ','.join(verify_csv)
        print ''
        sys.exit(1)
    else:
        print '全ての行の列数が一致しています'
    print line_a
    print ''

    print line_a
    print 'ファイル内に記載されているプライストレージ名およびストレージタグ名は以下の通りです。'
    pses_sec_load = common.load_csv_as_list_gen_all(list_file)
    for i in sorted(pses_sec_load):
        print i
    print line_a
    print ''


    print line_a
    print 'ファイル内に記載されたプライマリストレージの最新情報を確認します。'
    sp_names=common.load_csv_as_list_gen_col(list_file)
    sp_names.sort()
    sps = common.get_sp_info_a_by_ids_states(common.get_sp_ids_by_names(sp_names))

    print '以下のプライマリストレージが確認されました。'
    sps_with_header = sps[:]
    sps_with_header.insert(0, ['storage_name', 'storage_id', 'storage_tag_name'])
    print pretty_table.create_table(sps_with_header)
    print ''

    sp_names_from_result = [x[0] for x in sps]
    sp_names_from_result.sort()
    sps_not_exist = common.search_in_list(sp_names, sp_names_from_result)

    if sps_not_exist == []:
        print 'ファイル内に記載された全てのプライマリストレージが見つかりました。'
    elif sps_not_exist == sp_names_from_result:
        print 'ファイル内に記載された全てのプライマリストレージが見つかりませんでした。'
        print 'ツールを終了します。'
        sys.exit(0)
    else:
        print '以下のプライマリストレージは見つかりませんでした。'
        for i in sps_not_exist:
            print i


    print line_a
    if common.continue_or_stop('上記プライマリストレージのストレージタグ名変更を開始しますか？ ') is False:
        print 'スクリプトを終了します。'
        sys.exit(0)

    print line_a
    print 'プライマリストレージのストレージタグ名変更を開始します。'


    pses_failed = common.changed_ps_tag(common.load_csv_as_list_gen_all(list_file))
    print 'プライマリストレージのストレージタグ名変更が完了しました。'
    print ''

    print line_a
    if pses_failed == []:
        print 'ファイル内に記載された全てのプライマリストレージのストレージタグ名の変更が成功しました。'
    else:
        print 'ファイル内に記載された全てのプライマリストレージの内、以下はストレージタグ名の変更が失敗しました。'
        for row in pses_failed:
            print row[4], row[-1]


    print line_a
    print ''
    print 'スクリプトを終了します。'

    sys.exit(0)



if __name__ == "__main__":
    r"""
    引数を確認してmain()関数を呼ぶ。
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('file_path', help='登録対象の一覧ファイル')
    args = parser.parse_args()

    main(args)

