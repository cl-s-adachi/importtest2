#!/usr/bin/python
# -*- coding: UTF-8 -*-

r"""listの要素を表で出力する為のモジュール

"""


def create_table(sourceList, lineChar='*'):
    r"""
    受けたリストを人間が読みやすい表(str)で返す。

    Parameters
    ----------
    sourceList: list
        1行あたり1要素、1列あたり1要素のlist。
        [ ['1行目1列目', '1行目2列目', ... ], ['2行目1列目', '2行目2列目', ... ], ... ]
        1行目はヘッダー(列名)として処理する。

    Returns
    -------
    表: str
        変換された表を返す。

    Notes
    -----

    * 1行目はヘッダー(列名)として処理する。
    * 元のlistは全ての行の列数が一致する必要があるが、この関数は各行の列数を検査しない。

    """

    # 列数、行数を取得する。
    numRows = len(sourceList)
    numCols = len(sourceList[0])


    # 各列の広さを記載するlistを定義する。
    lenCols = []
    for i in range(numCols):
        lenCols.append(0)


    # 各列の広さを測る。
    for i in range(numRows):
        for j in range(numCols):
            if len(str(sourceList[i][j])) > lenCols[j]:
                lenCols[j] = len(sourceList[i][j])


    # 表全体の広さを測る。
    totalWidth = 4 + ((numCols -1) * 3)
    for i in lenCols:
        totalWidth = totalWidth + i


    # リターンする表(str)を定義する。
    table = ''


    # 表の上側の線を書く。
    for i in range(totalWidth):
        table += lineChar
    table += '\n'


    # 列名を書く。
    table += lineChar

    for i in range(numCols):
        table += ' ' + sourceList[0][i]
        for j in range(lenCols[i] - len(sourceList[0][i])):
            table += ' '
        table += ' ' + lineChar

    table += '\n'

    for i in range(totalWidth):
        table = table + lineChar
    table = table + '\n'


    # 残りの表を書く。

    for k in range(numRows):
        if k == 0:
            continue

        table += lineChar

        for i in range(numCols):
            table += ' ' + str(sourceList[k][i])
            for j in range(lenCols[i] - len(str(sourceList[k][i]))):
                table += ' '
            table += ' ' + lineChar
        table += '\n'

    for i in range(totalWidth):
        table += lineChar
    table += '\n'


    # 結果を返す。
    return table
