#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""APサーバ及びバッチサーバに移行支援ツールをインストールするスクリプトです."""
from __future__ import print_function
import sys
import subprocess
import os
import traceback
import imp
def parse_args():
    """コマンドライン引数をパースし、グローバル変数に格納します.

    Parameters
    ---------
    None

    Returns
    -------
    dict
        ['configfile']: str
            設定ファイル名
        ['usage']: bool
            使用例を表示するかどうか
        ['dryrun']: bool
            ドライランするかどうか
        ['debugtools']: bool
            デバッグツールをインストールするかどうか
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    ret = {}
    ret['configfile'] = dir_path + '/install_config.py'
    ret['usage'] = False
    ret['dryrun'] = False
    ret['debugtools'] = False
    if len(sys.argv) < 1:
        ret['usage'] = True
        return
    i = 1
    while True:
        if i == len(sys.argv):
            break
        if sys.argv[i] == '--help':
            ret['usage'] = True
        elif sys.argv[i] == '-f':
            i += 1
            ret['configfile'] = sys.argv[i]
        elif sys.argv[i] == '--debug-tools':
            ret['debugtools'] = True
        else:
            raise RuntimeError('不明なオプション {0}'.format(sys.argv[i]))
        i += 1

    return ret

class RemoteCommandExecutor(object):
    """リモートホストを扱うコマンドを実行するためのクラスです.

    Attributes
    ----------
    host: str
        ホスト (IPアドレス)
    user: str
        ユーザ名
    password: str
        パスワード
    sshpass: str
        sshpassコマンドへのパス
    """

    def __init__(self, host, user, password, sshpass):
        """RemoteCommandExecutorクラスのインスタンスを作成します."""
        self.host = host
        self.user = user
        self.password = password
        self.sshpass = sshpass

    def rsync(self, srcpath, destpath):
        """rsyncコマンドを実行します.

        Parameters
        ----------
        srcpath: str
            コピー元ローカルファイル(ディレクトリ)のパス
        destpath: str
            コピー先リモートファイル(ディレクトリ)のパス

        Returns
        -------
        None
        """
        return subprocess.Popen([self.sshpass, '-p', self.password, 'rsync', '-e',
                                 'ssh -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no',
                                 '-avzp', srcpath,
                                 '{0}@{1}:{2}'.format(self.user, self.host, destpath)])

    def ssh(self, arglist):
        """sshコマンドを実行します.

        Parameters
        ----------
        arglist: list
            コマンドライン引数リスト (例: ['ls', '/etc/hogehoge'] )

        Returns
        -------
        None
        """
        return subprocess.Popen([self.sshpass, '-p', self.password, 'ssh',
                                 '-oUserKnownHostsFile=/dev/null', '-oStrictHostKeyChecking=no',
                                 '{0}@{1}'.format(self.user, self.host)] + arglist)


def main():
    """スクリプトのエントリポイントです."""
    args = parse_args()
    if args['usage']:
        print("使用法:{0} ".format(sys.argv[0]))
        print("")
        print("EOSL移行支援ツールをリモートホストにインストールします。")
        print('オプション:')
        print('  -f <ファイル名> 設定ファイルとして <ファイル名> を使用します。')
        return

    print('設定ファイルとして {0} を使用します。'.format(args['configfile']))

    try:
        install_config = imp.load_source('', args['configfile'])
    except Exception:
        traceback.print_exc()
        sys.exit('Error: インストール設定ファイル {0} をインポートできませんでした。'.format(args['configfile']))

    dir_path = os.path.dirname(os.path.realpath(__file__))
    sshpass = dir_path + '/../eosl_local_root/usr/local/bin/sshpass'
    batch_scripts_dir = dir_path + '/../batch_scripts'
    ap_batch_common_dir = dir_path + '/../ap_batch_common'
    mysql_dir = dir_path + '/../mysql'
    batch_remote_subdirname = 'eosl_migration_tools_bat_vr'
    batch_remote_destdir = install_config.BatchInstallDir + '/' + batch_remote_subdirname

    ap_scripts_dir = dir_path + '/../ap_scripts'
    ap_debug_scripts_dir = dir_path + '/../ap_debug_scripts'
    eosl_local_root_dir = dir_path + '/../eosl_local_root'
    ap_remote_subdirname = 'eosl_migration_tools_ap_vr'
    ap_remote_destdir = install_config.ApInstallDir + '/' + ap_remote_subdirname

    #バッチサーバにインストールする
    print('バッチサーバにインストールしています...')
    batch_executor = RemoteCommandExecutor(install_config.BatchHost, install_config.BatchUser,
                                           install_config.BatchPassword, sshpass)
    batch_executor.ssh(['mkdir', '-p', batch_remote_destdir]).wait()
    batch_executor.rsync(batch_scripts_dir + '/', batch_remote_destdir).wait()
    batch_executor.rsync(ap_batch_common_dir, batch_remote_destdir).wait()
    batch_executor.rsync(eosl_local_root_dir, batch_remote_destdir).wait()
    batch_executor.rsync(mysql_dir, batch_remote_destdir).wait()

    #APサーバにインストールする
    for aphost in install_config.ApHosts:
        print('APサーバ {0} にインストールしています...'.format(aphost))
        ap_executor = RemoteCommandExecutor(aphost, install_config.ApUser,
                                            install_config.ApPassword, sshpass)
        ap_executor.ssh(['mkdir', '-p', ap_remote_destdir]).wait()
        ap_executor.rsync(ap_scripts_dir + '/', ap_remote_destdir).wait()
        ap_executor.rsync(ap_batch_common_dir, ap_remote_destdir).wait()
        ap_executor.rsync(eosl_local_root_dir, ap_remote_destdir).wait()
        ap_executor.rsync(mysql_dir, ap_remote_destdir).wait()

    #デバッグツールをインストールする
    if args['debugtools']:
        print('APサーバにデバッグツールをインストールしています...')
        ap_executor.rsync(ap_debug_scripts_dir + '/', ap_remote_destdir).wait()

main()
