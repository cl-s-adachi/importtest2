#!/usr/bin/python2
# -*- coding:utf-8 -*-
import sys
import urllib2
import urllib
import hashlib
import hmac
import base64 
import traceback
import json
import time
import os
import datetime
import mysql.connector
import re

#vCenter Server TaskInfo

#taskMO: taskへのManagedObjectReference
#entityName: エンティティ名
#descriptionId: 操作の種類を表すID
#state: タスクの状態
#cancelable: タスクがキャンセル可能かどうか
#参照: http://pubs.vmware.com/vi30/sdk/ReferenceGuide/vim.TaskInfo.html
class VCSTaskInfo:
	#def __init__(self, task_mo):
		#self.descriptionId = re.search('<descriptionId>(.+?)<', xml_taskinfo).group(1)
		#self.entityName = re.search('<entityName>(.+?)<', xml_taskinfo).group(1)
		#self.taskMO = re.search('<task.*?>(.+?)<', xml_taskinfo).group(1)

	#TaskMOからTaskInfoを取得する
	#task_mo: TaskへのManagedObjectReference
	#vcsclient: API実行に使用するVCSClient
	@staticmethod
	def fromTaskMO(task_mo, vcsclient):
		self = VCSTaskInfo()
		self.taskMO = task_mo

                line_list = []
                line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
                line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
                line_list.append('  <S:Body>')
                line_list.append('   <RetrieveProperties xmlns="urn:vim25">')
                line_list.append('     <_this type="PropertyCollector">propertyCollector</_this>')
                line_list.append('       <specSet>')
                line_list.append('         <propSet><type>Task</type><pathSet>info</pathSet></propSet>')
                line_list.append('         <objectSet><obj type="Task">{0}</obj></objectSet>'.format(task_mo))
                line_list.append('       </specSet>')
                line_list.append('    </RetrieveProperties>')
                line_list.append('  </S:Body>')
                line_list.append('</S:Envelope>')
                envelope = ''.join(line_list)
                req = urllib2.Request('https://{host}/sdk/vimService'.format(host = vcsclient.host), data=envelope,
                headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(vcsclient.sessionId)})
                try:
                        resp = urllib2.urlopen(req, timeout = vcsclient.timeout)
                except urllib2.HTTPError as e:
                        print(e.read())
                        #raise
                        return #TODO: デバッグのため一時的に握りつぶしています

                content = resp.read()
		ensearch = re.search('<entityName>(.+?)</entityName>', content)
		self.entityName = None if ensearch is None else ensearch.group(1) 
		disearch = re.search('<descriptionId>(.+?)</descriptionId>', content)
		self.descriptionId = None if disearch is None else disearch.group(1)
		stsearch = re.search('<state>(.+?)</state>', content)
		self.state = None if stsearch is None else stsearch.group(1)
		csearch = re.search('<cancelable>(.+?)</cancelable>', content)
		self.cancelable = None if csearch is None else csearch.group(1)
		lsearch = re.search('<locked>(.+?)</locked>', content)
		self.locked = None if lsearch is None else lsearch.group(1)
		esearch = re.search('<entity>(.+?)</entity>', content)
		self.entity = None if esearch is None else esearch.group(1)
		nsearch = re.search('<name>(.+?)</name>', content)
		self.name = None if nsearch is None else nsearch.group(1)
		#print(self.descriptionId)
		qsearch = re.search('<queueTime>(.+?)</queueTime>', content)
                self.queueTime = None if qsearch is None else qsearch.group(1) #タスク要求時刻
                self.queueTimeDateTime = None
                if self.queueTime is not None:
                        self.queueTimeDateTime = datetime.datetime.strptime(self.queueTime, '%Y-%m-%dT%H:%M:%S.%fZ')
		return self

class VCSManagedEntity:
	@staticmethod
	def findVirtualMachineByUuid(uuid, vcsclient):
		self = VCSManagedEntity
		self.uuid = uuid

                line_list = []
                line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
                line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
                line_list.append('  <S:Body>')
                line_list.append('   <FindByUuid xmlns="urn:vim25">')
                line_list.append('     <_this type="SearchIndex">SearchIndex</_this>')
                line_list.append('       <uuid>{0}</uuid>'.format(uuid))
                line_list.append('       <vmSearch>true</vmSearch>')
                line_list.append('    </FindByUuid>')
                line_list.append('  </S:Body>')
                line_list.append('</S:Envelope>')
                envelope = ''.join(line_list)
                req = urllib2.Request('https://{host}/sdk/vimService'.format(host = vcsclient.host), data=envelope,
                headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(vcsclient.sessionId)})
                try:
                        resp = urllib2.urlopen(req, timeout = vcsclient.timeout)
                except urllib2.HTTPError as e:
                        print(e.read())
                        #raise
                        return #TODO: デバッグのため一時的に握りつぶしています

                content = resp.read()
		print(content)
		#print(self.descriptionId)
		return self
		

#vCenter Server Client
#
#host: ホスト名
#username: ユーザ名
#password: パスワード
#sessionId: セッションID
#timeout: タイムアウト(秒)
class VCSClient:
	def __init__(self, host, username, password):
		self.host = host
		self.username = username
		self.password = password
		self.timeout = 10

	#ログインしてセッションIDをセットする
	def login(self):
		line_list = []
		line_list.append('<?xml version="1.0" encoding="UTF-8"?>\n')
		line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">\n')
		line_list.append('  <S:Body>\n')
		line_list.append('    <Login xmlns="urn:vim25">\n')
		line_list.append('      <_this type="SessionManager">SessionManager</_this>\n')
		line_list.append('      <userName>{0}</userName>\n'.format(self.username))
		line_list.append('      <password>{0}</password>\n'.format(self.password))
		line_list.append('    </Login>\n')
		line_list.append('  </S:Body>\n')
		line_list.append('</S:Envelope>\n')
		login_envelope = ''.join(line_list)
		req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=login_envelope, headers={"Content-Type": "application/soap+xml"})
		resp = urllib2.urlopen(req, timeout = self.timeout)
		cookie_header = resp.info().getheader('Set-Cookie')
		reresult = re.search('vmware_soap_session="([\w\-]+)"', cookie_header)
		self.sessionId = reresult.group(1)
		return
	
	#Running状態のタスクを取得するTaskHistoryCollectorを作成する
	#returns: TaskHistoryCollectorのID
	#def createRunningTaskHistoryCollector(self):
	#	line_list = []
	#	line_list.append('<?xml version="1.0" encoding="UTF-8"?>\n')
	#	line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">\n')
	#	line_list.append('  <S:Body>\n')
	#	line_list.append('    <CreateCollectorForTasks xmlns="urn:vim25">\n')
	#	line_list.append('      <_this type="TaskManager">TaskManager</_this>\n')
	#	line_list.append('      <filter>\n')
	#	#line_list.append('      <state>success</state>\n')
	#	#line_list.append('	<state>running</state>\n')
	#	#line_list.append('      <state>queued</state>\n')
	#	line_list.append('      </filter>\n')
	#	line_list.append('    </CreateCollectorForTasks>\n')
	#	line_list.append('  </S:Body>\n')
	#	line_list.append('</S:Envelope>\n')
	#	envelope = ''.join(line_list)
	#	req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope, 
	#		headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
	#	resp = urllib2.urlopen(req, timeout = self.timeout)
	#	content = resp.read()
	#	reresult = re.search('<returnval type="TaskHistoryCollector">(.+?)<', content)
	#	return reresult.group(1)
	
	def resetCollector(self, rthc):
                #ResetConllector()
                line_list = []
                line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
                line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
                line_list.append('  <S:Body>')
                line_list.append('   <ResetCollector xmlns="urn:vim25">')
                line_list.append('     <_this type="TaskHistoryCollector">{0}</_this>'.format(rthc))
                line_list.append('    </ResetCollector>')
                line_list.append('  </S:Body>')
                line_list.append('</S:Envelope>')
                envelope = ''.join(line_list)
                req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
                        headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
                resp = urllib2.urlopen(req, timeout = self.timeout)

	#新しいタスクから取得する
	#rthc: TaskHistoryCollector ID
	#max_count: 最大取得数
	#def getPreviousTasks(self, rthc, max_count):
	#	line_list = []
	#	line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
	#	line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
	#	line_list.append('  <S:Body>')
	#	line_list.append('   <ReadPreviousTasks xmlns="urn:vim25">')
	#	line_list.append('     <_this type="TaskHistoryCollector">{0}</_this>'.format(rthc))
	#	line_list.append('      <maxCount>{0}</maxCount>'.format(max_count))
	#	line_list.append('    </ReadPreviousTasks>')
	#	line_list.append('  </S:Body>')
	#	line_list.append('</S:Envelope>')
	#	envelope = ''.join(line_list)
        #        req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
        #                headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
        #        resp = urllib2.urlopen(req, timeout = self.timeout)
        #        content = resp.read()
	#	reresults = re.finditer('<returnval>(.+?)</returnval>', content)
	#	
	#	retlist = []
	#	for r in reresults:
	#		retlist.append(VCSTaskInfo(r.group(1)))
	#	return retlist

	#最近のタスクを取得する
	def getRecentTasks(self):	
		line_list = []
                line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
                line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
                line_list.append('  <S:Body>')
                line_list.append('   <RetrieveProperties xmlns="urn:vim25">')
                line_list.append('     <_this type="PropertyCollector">propertyCollector</_this>')
		line_list.append('       <specSet>')
		line_list.append('         <propSet><type>TaskManager</type><pathSet>recentTask</pathSet></propSet>')
		line_list.append('         <objectSet><obj type="TaskManager">TaskManager</obj></objectSet>')
		line_list.append('       </specSet>')
                line_list.append('    </RetrieveProperties>')
                line_list.append('  </S:Body>')
                line_list.append('</S:Envelope>')
                envelope = ''.join(line_list)
                req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
                headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
		try:
	               	resp = urllib2.urlopen(req, timeout = self.timeout)
		except urllib2.HTTPError as e:
			print(e.read())
			#raise
			return #TODO: デバッグのため一時的に握りつぶしています

		content = resp.read()
		reresults = re.finditer('>(task-\\d+)<', content)
		
		retlist = []
		for r in reresults:
			retlist.append(VCSTaskInfo.fromTaskMO(r.group(1), self))
		return retlist

Usage = None
VCHost = None
VCUsername = None
VCPassword = None
def parse_args():
        global Usage
	global VCHost
	global VCUsername
	global VCPassword
#        global InstanceListFileName
        if len(sys.argv) < 2:
                Usage = True
                return
        i = 1
        while(1):
                if i == len(sys.argv):
                        break
		if sys.argv[i] == '--vchost':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--vchost オプションの後にvCenter Serverホスト名が指定されていません。')
			VCHost = sys.argv[i]
		elif sys.argv[i] == '--vcusername':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--vcusername オプションの後にvCenter Server ユーザ名が指定されていません。')
			VCUsername = sys.argv[i]
		elif sys.argv[i] == '--vcpassword':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--vcpassword オプションの後にvCenter Server パスワードが指定されていません。')
			VCPassword = sys.argv[i]
                else:
			raise RuntimeError('不明なオプション \'{0}\''.format(sys.argv[i]))
                i += 1

def main():
	try:
		parse_args()
	except:
		emsg = sys.exc_info()[1]
		print(emsg)
		#raise
		return
	if Usage:
		print("使用法: {0} --vchost <vCenter Server ホスト名> --vcusername <vCenter Server ユーザ名> --vcpassword <vCenter Server パスワード>".format(sys.argv[0]))
                print("")
                print("vCenter Serverで実行中のタスクを取得します。")
		return
	if VCHost is not None:
		vcsclient = VCSClient(VCHost, VCUsername, VCPassword)
		vcsclient.login()
	#VCSManagedEntity.findVirtualMachineByUuid('2c1c88e2bede4694ad73aa5030ee3ff4', vcsclient)
	VCSManagedEntity.findVirtualMachineByUuid('8169b345611947ea83a313cdd23e2d97', vcsclient)
	VCSManagedEntity.findVirtualMachineByUuid('a69ee612cf7c46539be067bd6e3a7373', vcsclient)
	#return #
	for t in vcsclient.getRecentTasks():
		print('taskMO={0}, cancelable={1}, descriptionId={2}, entityName={3}, state={4}, locked={5}, entity={6}, name={7}, queueTimeDateTime={8}'.format(t.taskMO, t.cancelable, t.descriptionId, t.entityName, t.state, t.locked, t.entity, t.name, t.queueTimeDateTime))


main()
