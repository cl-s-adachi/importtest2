#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""vCenter ServerのAPIを叩くためのクラスです."""
import sys
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)
import traceback
import time
import urllib2
import inspect
import re
from VCSTaskInfo import VCSTaskInfo
from ap_batch_common.VirtualMachine import VirtualMachine
from eosl_migrate_ap_common import CancellationError
from eosl_migrate_ap_common import MigrationCancellationError

#VCSClientで発生するエラー
class VCSClientError(Exception):
    """VCSClientで発生するすべての例外のスーパークラスです."""

    pass

#セッションが切れている場合のエラー
class VCSSessionExpiredError(VCSClientError):
    """セッションが切れている場合に発生するエラーです."""

    pass

#MOが存在しない場合のエラー
class VCSManagedObjectNotFoundError(VCSClientError):
    """Managed Object (MO) が見つからなかった場合のエラーです."""

    pass

class VCSClient:
    """vCenter Serverのライアントです.

    Attributes
    ----------
    host: str
       ホスト名 (IPアドレス)
    username: str
       ユーザ名
    password: str
       パスワード
    sessionId: str
       セッションID
    timeout: int
       タイムアウト(秒)
    """

    def __init__(self, host, username, password):
        """VCSClientのインスタンスを作成します.

        Parameters
        ---------- 
        host: str
            接続先vCenter Serverのホスト名 (IPアドレス)
        username: str
            ユーザ名
        password: str
            パスワード
        """
        self.host = host
        self.username = username
        self.password = password
        self.timeout = 10

    def login(self):
        """Login to vCenter Server and set self.sessionId.

        vCenter Serverにログインし、self.sessionIdをセットします.

        Returns
        -------
        None
        """
        line_list = []
        line_list.append('<?xml version="1.0" encoding="UTF-8"?>\n')
        line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">\n')
        line_list.append('  <S:Body>\n')
        line_list.append('    <Login xmlns="urn:vim25">\n')
        line_list.append('      <_this type="SessionManager">SessionManager</_this>\n')
        line_list.append('      <userName>{0}</userName>\n'.format(self.username))
        line_list.append('      <password>{0}</password>\n'.format(self.password))
        line_list.append('    </Login>\n')
        line_list.append('  </S:Body>\n')
        line_list.append('</S:Envelope>\n')
        login_envelope = ''.join(line_list)
        req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=login_envelope, headers={"Content-Type": "application/soap+xml"})
        resp = urllib2.urlopen(req, timeout = self.timeout)
        cookie_header = resp.info().getheader('Set-Cookie')
        reresult = re.search('vmware_soap_session="([\w\-]+)"', cookie_header)
        self.sessionId = reresult.group(1)
        return

    def login2(self, retry_count, fail_func, fail_func_arglist, retry_func, retry_func_arglist):
        """Same as login() but tries up to retry_count times.

        vCenter Serverにログインし、self.sessionIdをセットします.
        最大試行回数をセットすることができます.

        Parameters
        ----------
        retry_count: int
            最大試行回数
        fail_func: def
            接続に失敗した際に呼び出すメソッド.
        fail_func_arglist: list
            fail_funcに渡す引数リスト.
        retry_func: def
            再試行時に呼び出すメソッド.
        retry_func_arglist: list
            retry_funcに渡す引数リスト.

        Returns
        -------
        ログインに成功した場合はTrue.それ以外の場合はFalse.
        """
        login_flag = False
        for i in range(retry_count):
            try:
                self.login()
                return True
            except Exception:
                if fail_func is not None:
                    fail_func(sys.exc_info()[1], *fail_func_arglist)
                if i < retry_count - 1:
                    if retry_func is not None:
                        retry_func(*retry_func_arglist)
        return False

    def isSessionActive(self):
        """セッションがアクティブかどうか調べます.このメソッドは例外を発生することがあります.

        Returns
        -------
        セッションがアクティブな場合はTrue.そうでない場合はFalse.
        """
        try:
            line_list = []
            line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
            line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
            line_list.append('  <S:Body>')
            line_list.append('   <RetrieveProperties xmlns="urn:vim25">')
            line_list.append('     <_this type="PropertyCollector">propertyCollector</_this>')
            line_list.append('       <specSet>')
            line_list.append('         <propSet><type>TaskManager</type><pathSet>maxCollector</pathSet></propSet>')
            line_list.append('         <objectSet><obj type="TaskManager">TaskManager</obj></objectSet>')
            line_list.append('       </specSet>')
            line_list.append('    </RetrieveProperties>')
            line_list.append('  </S:Body>')
            line_list.append('</S:Envelope>')
            envelope = ''.join(line_list)
            req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
            headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
            try:
                resp = urllib2.urlopen(req, timeout = self.timeout)
            except urllib2.HTTPError as e:
                content = e.read()
                if e.code == 500 and 'NotAuthenticatedFault' in content:
                    raise VCSSessionExpiredError('セッションが切れています。再度ログインしてください。')
                raise VCSClientError('vCenterサーバからデータを取得中にエラーが発生しました: {0}'.format(content))
            return True
        except VCSSessionExpiredError:
            return False

    #最近のタスクを取得する
    def getRecentTasks(self):
        """Get vCenter tasks.

        vCenter Serverで最近のタスクを取得します.

        Returns
        -------
        retlist: list
            VCSTaskInfoのリスト.
        """
        line_list = []
        line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
        line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
        line_list.append('  <S:Body>')
        line_list.append('   <RetrieveProperties xmlns="urn:vim25">')
        line_list.append('     <_this type="PropertyCollector">propertyCollector</_this>')
        line_list.append('       <specSet>')
        line_list.append('         <propSet><type>TaskManager</type><pathSet>recentTask</pathSet></propSet>')
        line_list.append('         <objectSet><obj type="TaskManager">TaskManager</obj></objectSet>')
        line_list.append('       </specSet>')
        line_list.append('    </RetrieveProperties>')
        line_list.append('  </S:Body>')
        line_list.append('</S:Envelope>')
        envelope = ''.join(line_list)
        req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
        headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
        try:
                       resp = urllib2.urlopen(req, timeout = self.timeout)
        except urllib2.HTTPError as e:
            content = e.read()
            if e.code == 500 and 'NotAuthenticatedFault' in content:
                raise VCSSessionExpiredError('セッションが切れています。再度ログインしてください。')
            raise VCSClientError('vCenterサーバからデータを取得中にエラーが発生しました: {0}'.format(content))

        content = resp.read()
        reresults = re.finditer('>(task-\\d+)<', content)
        
        retlist = []
        for r in reresults:
            retlist.append(VCSTaskInfo.fromTaskMO(r.group(1), self))
        return retlist

    #タスクをキャンセルする
    #taskmo: TaskへのManagedObjectReferenceのID
    def cancelTask(self, taskmo):
        """Cancel vCenter task.

        指定したvCenterタスクをキャンセルします.

        Parameters
        ----------
        taskmo: str
            vCenter TaskのMO (task-xxxxxx)
        """
        line_list = []
        line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
        line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
        line_list.append('  <S:Body>')
        line_list.append('    <CancelTask xmlns="urn:vim25">')
        line_list.append('      <_this type="Task">{0}</_this>'.format(taskmo))
        line_list.append('    </CancelTask>')
        line_list.append('  </S:Body>')
        line_list.append('</S:Envelope>')
        envelope = ''.join(line_list)
        req = urllib2.Request('https://{host}/sdk/vimService'.format(host = self.host), data=envelope,
        headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(self.sessionId)})
        try:
                    resp = urllib2.urlopen(req, timeout = self.timeout)
        except urllib2.HTTPError as e:
            content = e.read()
            #print(VCSTaskInfo.fromTaskMO(taskmo, self).descriptionId)
            if 'NotSupportedFault' in content:
                raise RuntimeError('タスクをキャンセルできませんでした。既にタスクが完了している可能性があります。');
            if e.code == 500 and 'NotAuthenticatedFault' in content:
                raise VCSSessionExpiredError('セッションが切れています。再度ログインしてください。')
            if e.code == 500 and 'ManagedObjectNotFoundFault' in content:
                raise VCSManagedObjectNotFoundError('タスク {0} は見つかりませんでした。'.format(taskmo))
            raise VCSClientError('vCenterサーバからデータを取得中にエラーが発生しました: {0}'.format(content))
                
        content = resp.read()
        return

    #タスクをキャンセルする
    #taskmo: TaskへのManagedObjectReferenceのID
    #kwargs:
    #    ct_count: vCenterタスクのキャンセルを試みる回数。未指定の場合、3回
    #    ct_interval: vCenterタスクのキャンセルを試みる間隔。未指定の場合、10秒。
    #    ct_func_to_call_on_error: vCenterタスクのキャンセル時にエラーが発生した際に呼び出すメソッド。未指定の場合、lambda: None
    #    ct_arglist_to_pass_on_error: ↑に渡す引数のリスト。第一引数にはスタックトレースが入る。未指定の場合、[]
    def cancelTask2(self, taskmo, **kwargs):
        """Cancel vCenter task.

        vCenterタスクをキャンセルします.

        Parameters
        ----------
        taskmo: str
            キャンセルするタスクのMO (task-xxxxxx) .
        **kwargs:
            ct_count: int
                vCenterタスクのキャンセルを試みる回数. 未指定の場合、3回.
            ct_interval: int
                vCenterタスクのキャンセルを試みる間隔(秒).　未指定の場合, 10秒.
            ct_func_to_call_on_error: def
                vCenterタスクのキャンセル時にエラーが発生した場合に呼び出すメソッド.
            ct_arglist_to_pass_on_error: list
                ct_func_to_call_on_errorに渡す第二引数以降のリスト. 第一引数にはスタックトレースが入ります.未指定の場合[].
        """
        ct_count = kwargs.get('ct_count', 3)
        ct_interval = kwargs.get('ct_interval', 10)
        ct_func_to_call_on_error = kwargs.get('ct_func_to_call_on_error', lambda msg: None)
        ct_arglist_to_pass_on_error = kwargs.get('ct_arglist_to_pass_on_error', [])
        for i in range(ct_count):
            try:
                self.cancelTask(taskmo)
                return
            except:
                ct_func_to_call_on_error(traceback.format_exc(), *ct_arglist_to_pass_on_error)
            if i + 1 == ct_count:
                break
            time.sleep(ct_interval)

        raise CancellationError('{0}: タスク {1} をキャンセルできませんでした。'.format(inspect.currentframe().f_code.co_name, taskmo))
        
    def getTaskToCancelAttached(self, instance_name):
        """アタッチされているボリュームのマイグレーションを強制中止するときにキャンセルするべきタスクを取得します.

        Parameters
        ----------
        instance_name: str
            ボリュームがアタッチされているインスタンスのinstance_name (iから始まります) .

        Returns
        -------
        None
        """
        rtasks = self.getRecentTasks()
        for t in rtasks:
            if t.state == 'running' and t.cancelable == 'true' and t.descriptionId == 'VirtualMachine.relocate' and t.entityName == instance_name:
                return t
        return None

    #強制終了時にキャンセルすべきタスクを取得する (ボリュームがデタッチされているとき)
    #
    #base_time: 基準時刻
    #time_range: 基準時刻からの差
    #base_time +/- time_rangeにqueueされたタスクを検索します
    #
    #returns:
    #キャンセルすべきタスクのリスト ([])
    #このメソッドは2つ以上のTaskを返す場合があります。
    def getTasksToCancelDetached(self, base_time, time_range):
        """デタッチされているボリュームのマイグレーションを強制中止するときにキャンセルするべきタスクを取得します.

        base_time +/- time_rangeの間にqueueされたvCenterタスクを検索します.

        Parameters
        ----------
        base_time: datetime
            基準時刻 (タスクを検索する基準となる時刻)
        time_range: datetime
            基準時刻からの範囲

        Returns
        -------
        ret: list
            キャンセルするべきVCSTaskInfoのリスト.
        """
        rtasks = self.getRecentTasks()
        ret = []
        for t in rtasks:
            if t.state == 'running' and t.cancelable == 'true' and t.descriptionId == 'VirtualMachine.relocate':
                tdiff_abs = abs(t.queueTimeDateTime - base_time)
                if tdiff_abs <= time_range:
                    ret.append(t)    
        return ret

    def getTasksToCancelDetached2(self, base_time, time_range, **kwargs):
        """デタッチされているボリュームのマイグレーションの強制停止時にキャンセルすべきvCenterタスクを取得します.

        Parameters
        ----------
        base_time: datetime
            基準時刻 (タスクを検索する基準となる時刻)
        time_range: datetime
            基準時刻からの範囲
        **kwargs:
            gt_count: int
                vCenterタスクの検索を試みる回数.未指定の場合は3回.
            gt_interval: int
                vCenterタスクの検索を試みる間隔. 未指定の場合は10秒.
            gt_func_to_call_on_retry: def
                vCenterタスクの検索を再試行するときに呼び出すメソッド. 未指定の場合, lambda: None
            gt_arglist_to_pass_on_retry: list
                gt_func_to_call_on_retryに渡す引数リスト. 未指定の場合, []
            gt_func_to_call_on_error:
                 vCenterタスクの検索時にエラーが発生したときに呼び出すメソッド。未指定の場合、lambda:None
            gt_arglist_to_pass_on_error:
                 gt_func_to_call_on_errorに渡す引数リスト. なお、第一引数にはスタックトレースが渡される。未指定の場合、[]
        
        """
        gt_count = kwargs.get('gt_count', 3)
        gt_interval = kwargs.get('gt_interval', 10)
        gt_func_to_call_on_retry = kwargs.get('gt_func_to_call_on_retry', lambda: None)
        gt_arglist_to_pass_on_retry = kwargs.get('gt_arglist_to_pass_on_retry', [])
        gt_func_to_call_on_error = kwargs.get('gt_func_to_call_on_error', lambda: None)
        gt_arglist_to_pass_on_error = kwargs.get('gt_arglist_to_pass_on_error', [])
        rtasks = []
        for i in range(gt_count):
            try:
                rtasks = self.getTasksToCancelDetached(base_time, time_range)
            except:
                gt_func_to_call_on_error(traceback.format_exc(), *gt_arglist_to_pass_on_error)
            if len(rtasks) > 0:
                return rtasks
            gt_func_to_call_on_retry(*gt_arglist_to_pass_on_retry)
            time.sleep(gt_interval)

    def cancelAttachedVolumeMigrationTask(self, vol, connection, **kwargs):
        """アタッチされているボリュームのマイグレーションをキャンセルします.

        Parameters
        ----------
        vol: Volume
            マイグレーションをキャンセルするVolume.
        **kwargs:
            gt_count: int
                vCenterタスクの検索を試みる回数.未指定の場合は3回.
            gt_interval: int
                vCenterタスクの検索を試みる間隔. 未指定の場合は10秒.
            gt_func_to_call_on_retry: def
                vCenterタスクの検索を再試行するときに呼び出すメソッド. 未指定の場合, lambda: None
            gt_arglist_to_pass_on_retry: list
                gt_func_to_call_on_retryに渡す引数リスト.
            ct_count: int
                vCenterタスクのキャンセルを試みる回数. 未指定の場合, 3回.
            ct_interval: int
                vCenterタスクのキャンセルを試みる間隔. 未指定の場合, 10秒.

        Returns
        -------
        None
        """
        if vol.instanceId is None:
            raise RuntimeError('このメソッドでデタッチドボリュームのマイグレーションのキャンセルを行うことはできません。')

        #DB接続維持
        try:
            connection.ping(reconnect = True, attempts = 3, delay = 10)
        except:
            raise MigrationCancellationError('DBへのpingに失敗しました。スタックトレース: {0}'.format(traceback.format_exc()))

        #VM情報取得
        try:
            vm = VirtualMachine(connection, vol.instanceId)
        except:
            raise MigrationCancellationError('DBから仮想マシン {0} の情報を取得できませんでした。スタックトレース: {1}'.format(vol.instanceId, traceback.format_exc()))

        #キャンセル対象タスク取得
        gt_count = kwargs.get('gt_count', 3)
        gt_interval = kwargs.get('gt_interval', 10)
        gt_func_to_call_on_retry = kwargs.get('gt_func_to_call_on_retry', lambda: None)
        gt_arglist_to_pass_on_retry = kwargs.get('gt_arglist_to_pass_on_retry', [])
        task_to_cancel = None
        for i in range(gt_count):
            try:
                task_to_cancel = self.getTaskToCancelAttached(vm.instanceName)
            except:
                pass
            if task_to_cancel is not None:
                break
            gt_func_to_call_on_retry(*gt_arglist_to_pass_on_retry)
            time.sleep(gt_interval)
        if task_to_cancel is None:
            raise MigrationCancellationError('キャンセル対象のvCenterタスクを取得できませんでした。')

        #キャンセル
        ct_count = kwargs.get('ct_count', 3)
        ct_interval = kwargs.get('ct_interval', 10)
        for i in range(ct_count):
            try:
                self.cancelTask(task_to_cancel.taskMO)
                return
            except:
                pass
            time.sleep(ct_interval)
        raise MigrationCancellationError('vCenterタスクのキャンセルに失敗しました。既にマイグレーションが完了している可能性があります。')
