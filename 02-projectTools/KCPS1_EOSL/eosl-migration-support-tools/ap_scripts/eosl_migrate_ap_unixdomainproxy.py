#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import sys
import urllib2
import urllib
import hashlib
import hmac
import base64 
import traceback
import json
import time
import os
import datetime
import subprocess
import threading
import socket
import select
import signal
import struct

Usage = False
SocketFileName = None
def parse_args():
        global Usage
	global SocketFileName
        if len(sys.argv) < 2:
                Usage = True
                return
        i = 1
        while(1):
                if i == len(sys.argv):
                        break
		if sys.argv[i] == '':
			pass
                else:
			SocketFileName = sys.argv[i]
			break
                i += 1

def main():
	try:
		parse_args()
	except:
		emsg = sys.exc_info()[1]
		print(emsg)
		#raise
		return
	if Usage:
		print("使用法: {0} <Unixドメインソケットファイル名>".format(sys.argv[0]))
                print("")
                print("UNIXドメインソケットと通信します。")
		print("このツールはバッチサーバ上の eosl_migrate_stop.py から実行されることを想定しており、直接実行されることは想定しておりません。")
		print('')
		return
	sock = None
	try:
		sock = socket.socket(socket.AF_UNIX, socket.SOCK_SEQPACKET)
		sock.connect(SocketFileName)
	except Exception:
		sys.stderr.write('{0} への接続に失敗しました: {1}\n'.format(SocketFileName, sys.exc_info()[1]))
		return

	stdin_is_closed = False
	recv_msg_bytes = -1
	recv_buffer = ''

	try:
		while True:
			try:
				if stdin_is_closed:
					rfd_list, wfd_list, xfd_list = select.select([sock], [], [])
				else:
					rfd_list, wfd_list, xfd_list = select.select([sys.stdin.fileno(), sock], [], [])
			except select.error:
				sys.stderr.write('select()でエラーが発生しました: {0}\n'.format(sys.exc_info()[1]))
				return
			except KeyboardInterrupt:
				sys.stderr.write('select()中に割り込みが発生しました。\n')
				return
			#制御スクリプト <-> このプロキシスクリプト: stdin/stdoutはバイト指向のため、データの先頭に、データの大きさを示すリトルエンディアン4バイトを付加する
			#このプロキシスクリプト <-> APサーバスクリプト: メッセージ指向(SOCK_SEQPACKET)のソケットで通信するため、そのまま通信する
			for rfd in rfd_list:
				try:
					if rfd == sys.stdin.fileno():
						r = sys.stdin.read(1)
						if len(r) == 0:
							sys.stderr.write('標準入力がクローズされました。\n')
							stdin_is_closed = True
							break
						sys.stderr.write('標準入力からの入力データ:'+r+'\n')
						recv_buffer += r
						#制御スクリプト (強制停止スクリプト等) や端末からデータが到着した場合。
						if recv_msg_bytes < 0: #まだメッセージサイズが未確定の場合
							if len(recv_buffer) == 4: #データをリトルエンディアン4バイト(32ビット整数) 受信し、メッセージサイズが確定した。
								recv_msg_bytes = struct.unpack('<i', recv_buffer)[0]
								recv_buffer = '' #バッファをクリアする
								sys.stderr.write('メッセージサイズ確定: {0}\n'.format(recv_msg_bytes))
						else: #メッセージサイズが確定しているため、メッセージ本文を受信した場合
							if len(recv_buffer) == recv_msg_bytes: #メッセージ本文全体の受信が完了したとき
								sock.send(recv_buffer) #受け取ったメッセージをAPサーバスクリプトに送信する
								recv_msg_bytes = -1 #メッセージを再度受け取るための準備
								sys.stderr.write('送信したmsg: {0}\n'.format(recv_buffer))
								recv_buffer = '' #バッファをクリアする
					elif rfd == sock:
						#APサーバスクリプトからデータが到着した場合。
						r = sock.recv(128)
						if len(r) == 0:
							sys.stderr.write('通信が切断されました。\n')
							return
						sys.stderr.write('UNIXドメインソケットからの入力データ:'+r+'\n')
						msgsize = len(r)
						msgsize_bytes = struct.pack('<i', msgsize)
						sys.stdout.write(msgsize_bytes) #データの大きさを示すリトルエンディアン4バイト
						sys.stdout.write(r)
						sys.stdout.flush()
				except socket.error:
					sys.stderr.write('ソケットエラーが発生しました: {0}\n'.format(sys.exc_info()[1]))
					return
	finally:
		sock.shutdown(socket.SHUT_RDWR)
		sock.close()
main()
