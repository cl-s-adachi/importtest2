#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""VCSTaskInfoクラスを格納するモジュールです."""
import sys
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import urllib2
import re
import datetime
#vCenter Server TaskInfo
#
#taskMO: taskへのManagedObjectReference
#entityName: エンティティ名
#descriptionId: 操作の種類を表すID
#state: タスクの状態
#cancelable: タスクがキャンセル可能かどうか
#参照: http://pubs.vmware.com/vi30/sdk/ReferenceGuide/vim.TaskInfo.html
class VCSTaskInfo:
    """vCenter タスクを表します.

    Attributes
    ----------
    taskMO: str
        taskへのManagedObjectReference
    entityName: str
        エンティティ名
    descriptionId: str
        操作の種類を表すID
    state: str
        タスクの状態
    cancelable: str
       キャンセル可能かどうか
    """

    #def __init__(self, task_mo):
        #self.descriptionId = re.search('<descriptionId>(.+?)<', xml_taskinfo).group(1)
        #self.entityName = re.search('<entityName>(.+?)<', xml_taskinfo).group(1)
        #self.taskMO = re.search('<task.*?>(.+?)<', xml_taskinfo).group(1)

    #TaskMOからTaskInfoを取得する
    #task_mo: TaskへのManagedObjectReference
    #vcsclient: API実行に使用するVCSClient
    @staticmethod
    def fromTaskMO(task_mo, vcsclient):
        """taskMOからVCSTaskInfoクラスのインスタンスを作成します.

        Parameters
        ----------
        task_mo: str
            タスクへのManagedObjectReference (task-xxxxxx)
        vcsclient: VCSClient
            タスクへのVCSClient
        """
        self = VCSTaskInfo()
        self.taskMO = task_mo

        line_list = []
        line_list.append('<?xml version="1.0" encoding="UTF-8"?>')
        line_list.append('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">')
        line_list.append('  <S:Body>')
        line_list.append('   <RetrieveProperties xmlns="urn:vim25">')
        line_list.append('     <_this type="PropertyCollector">propertyCollector</_this>')
        line_list.append('       <specSet>')
        line_list.append('         <propSet><type>Task</type><pathSet>info</pathSet></propSet>')
        line_list.append('         <objectSet><obj type="Task">{0}</obj></objectSet>'.format(task_mo))
        line_list.append('       </specSet>')
        line_list.append('    </RetrieveProperties>')
        line_list.append('  </S:Body>')
        line_list.append('</S:Envelope>')
        envelope = ''.join(line_list)
        req = urllib2.Request('https://{host}/sdk/vimService'.format(host = vcsclient.host), data=envelope,
        headers={"Content-Type": "application/soap+xml", "Cookie": "vmware_soap_session=\"{0}\"".format(vcsclient.sessionId)})
        try:
                resp = urllib2.urlopen(req, timeout = vcsclient.timeout)
        except urllib2.HTTPError as e:
                print(e.read())
                #raise
                return #TODO: デバッグのため一時的に握りつぶしています

        content = resp.read()
        ensearch = re.search('<entityName>(.+?)</entityName>', content)
        self.entityName = None if ensearch is None else ensearch.group(1) 
        disearch = re.search('<descriptionId>(.+?)</descriptionId>', content)
        self.descriptionId = None if disearch is None else disearch.group(1)
        stsearch = re.search('<state>(.+?)</state>', content)
        self.state = None if stsearch is None else stsearch.group(1)
        csearch = re.search('<cancelable>(.+?)</cancelable>', content)
        self.cancelable = None if csearch is None else csearch.group(1)
        qsearch = re.search('<queueTime>(.+?)</queueTime>', content)
        self.queueTime = None if qsearch is None else qsearch.group(1) #タスク要求時刻
        self.queueTimeDateTime = None
        if self.queueTime is not None:
            self.queueTimeDateTime = datetime.datetime.strptime(self.queueTime, '%Y-%m-%dT%H:%M:%S.%fZ')
            
        #print(self.descriptionId)
        return self
