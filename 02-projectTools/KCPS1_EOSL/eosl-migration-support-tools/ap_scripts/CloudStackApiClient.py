#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""CloudStackApiClientクラスを含むモジュールです."""
import mysql.connector
import urllib
import urllib2
import base64
import sys
import hmac
import hashlib
import traceback
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)
from eosl_migrate_ap_common import CancellationRequestedException
import json
import time
import os
import select
class CloudStackApiClient(object):
    """CloudStackのAPIを叩くためのクラスです.

    Attributes
    ----------
    baseUrl: str
        ベースURL (http://localhost:8080/client/api? など)
    apiKey: str
        APIキー
    secretKey: str
        シークレットキー
    request: dictionary
        CloudStackマネジメントサーバに送信するキーバリューデータ (request['command'] = 'migrateVirtualMachine' など)
    defaultResponse: str
        デフォルトのレスポンス(json or xml)
    """

    def __init__(self, baseUrl, apiKey, secretKey, defaultResponse='json'):
        """CloudStackApiClientのインスタンスを作成します.

        Attributes
        ----------
        baseUrl: str
            ベースURL (http://localhost:8080/client/api? など)
        apiKey: str
            APIキー
        secretKey: str
            シークレットキー
        defaultResponse: str
            デフォルトのレスポンス(json or xml)
        """
        self.baseUrl = baseUrl
        self.apiKey = apiKey
        self.secretKey = secretKey
        self.request = {}
        self.defaultResponse = defaultResponse

    def getRequestString(self):
        """requestキーバリューデータを文字列に変換します (URLエンコーディング).

        Parameters
        ----------
        None

        Returns
        -------
        str
            リクエスト文字列
        """
        return '&'.join(['='.join([k, urllib.quote_plus(self.request[k], safe='*')]) for k in self.request.keys()])

    def getSignature(self):
        """署名を取得する.

        Parameters
        ----------
        None

        Returns
        -------
        str
            署名
        """
        sig_str = '&'.join(['='.join([k.lower(), urllib.quote_plus(self.request[k]).lower().replace('+', '%20')])for k in sorted(self.request.iterkeys())])
        sig = urllib.quote_plus(base64.encodestring(hmac.new(self.secretKey, sig_str, hashlib.sha1).digest()).strip())
        return sig
    
    def generateUrl(self):
        """URLを生成する.

        Parameters
        ----------
        None

        Returns
        -------
        str
            URL
        """
        return self.baseUrl + self.getRequestString() + '&signature='  + self.getSignature()

    @staticmethod
    def test(base_url, api_key, secret_key):
        """クラスをテストします.

        APIを実行し、その結果を標準出力に出力します.

        Parameters
        ----------
        baseUrl: str
            ベースURL (http://localhost:8080/client/api? など)
        apiKey: str
            APIキー
        secretKey: str
            シークレットキー

        Returns
        -------
        None
        """
        csac = CloudStackApiClient()
        csac.baseUrl = base_url
        csac.request['apikey'] = api_key
        csac.request['command'] = 'listUsers'
        csac.request['response'] = 'json'
        csac.secretKey = secret_key
        print(csac.generateUrl())
        res = urllib2.urlopen(csac.generateUrl())
        print(res.read())

    def __cleanRequest(self):
        """requestを初期化します.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        self.request = {}
        self.request['apikey'] = self.apiKey
        self.request['response'] = self.defaultResponse

    def createSnapshotPolicy(self, params):
        """ボリュームにスナップショットポリシを設定します。

        Parameters
        ----------
        params: dict
            APIに渡すパラメータを格納するディクショナリ
                'intervaltype': str
                    HOURLY, DAILY, WEEKLY, MONTHLY のいずれか
                'maxsnaps': int
                    最大スナップショット数
                'schedule': str
                    スケジュール
                'timezone': str
                    タイムゾーン
                'volumeid': int
                    ボリュームID
                'fordisplay': bool
                    GUIで表示するかどうか

        Returns
        -------
        str
            設定したスナップショットポリシUUID
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'createSnapshotPolicy'
        self.request['intervaltype'] = str(params['intervaltype'])
        self.request['maxsnaps'] = str(params['maxsnaps'])
        self.request['schedule'] = str(params['schedule'])
        self.request['timezone'] = str(params['timezone'])
        self.request['volumeid'] = str(params['volumeid'])
        self.request['fordisplay'] = 'true' if params['fordisplay'] else 'false'

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        resptext = response.read()
        json_dict = json.loads(resptext)
        try:
            return json_dict['createsnapshotpolicyresponse']['snapshotpolicy']['id']
        except Exception as e:
            raise Exception(e + resptext)
    
    def migrateVirtualMachine(self, vmId, destId, checkInterval = 5, callback = lambda: None, callbackargs = []):
        """仮想マシンを同期的にマイグレートします.

        仮想マシンを同期的にマイグレートします. マイグレーション状態をcheckInterval秒間隔で監視します。
        callbackはマイグレーション状態監視時に呼び出されます.callbackの引数はcallbackargsで指定可能です.

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        destId: int
            移行先ホストのID
        checkInterval: int
            マイグレーション状態監視間隔(秒)
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数リスト
        Returns
        -------
        None
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateVirtualMachine'
        self.request['virtualmachineid'] = str(vmId)
        self.request['hostid'] = str(destId)

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratevirtualmachineresponse']['jobid']

        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult' 
        self.request['jobid'] = str(jobid)

        #マイグレーション状態を取得する
        while True:
            response = urllib2.urlopen(self.generateUrl())
            json_dict = json.loads(response.read())
            jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']
            if jobstatus == 0:
                callback(*callbackargs)
            elif jobstatus == 1:
                return
            else:
                raise RuntimeError('インスタンス {0} のマイグレーションに失敗しました。'.format(vmId))
            time.sleep(checkInterval)

    def migrateSystemVm(self, vmId, destId, checkInterval = 5, callback = lambda: None, callbackargs = []):
        """システムVM(CPVM, SSVM, VR)を同期的にマイグレートします.

        システムVM(CPVM, SSVM, VR)を同期的にマイグレートします. マイグレーション状態をcheckInterval秒間隔で監視します。
        callbackはマイグレーション状態監視時に呼び出されます.callbackの引数はcallbackargsで指定可能です.

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        destId: int
            移行先ホストのID
        checkInterval: int
            マイグレーション状態監視間隔(秒)
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数リスト
        Returns
        -------
        None
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateSystemVm'
        self.request['virtualmachineid'] = str(vmId)
        self.request['hostid'] = str(destId)

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratesystemvmresponse']['jobid']

        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult' 
        self.request['jobid'] = str(jobid)

        #マイグレーション状態を取得する
        while True:
            response = urllib2.urlopen(self.generateUrl())
            json_dict = json.loads(response.read())
            jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']
            if jobstatus == 0:
                callback(*callbackargs)
            elif jobstatus == 1:
                return
            else:
                raise RuntimeError('システムVM {0} のマイグレーションに失敗しました。'.format(vmId))
            time.sleep(checkInterval)

    def migrateVirtualMachine2Async(self, vmId, poolId):    
        """仮想マシンのROOTボリュームを非同期的にマイグレートします..

        仮想マシンのROOTボリュームを非同期的にマイグレートします. 

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        poolId: int
            マイグレート先ストレージプール(プライマリストレージ)ID

        Returns
        -------
        str
            非同期ジョブID
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateVirtualMachine'
        self.request['virtualmachineid'] = str(vmId)
        self.request['storageid'] = str(poolId)

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratevirtualmachineresponse']['jobid']

        return jobid

    def migrateVirtualMachine2(self, vmId, poolId, checkInterval = 1, callback = lambda: None, callbackargs = []):
        """仮想マシンのROOTボリュームを同期的にマイグレートします..

        仮想マシンのROOTボリュームを同期的にマイグレートします. 

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        poolId: int
            マイグレート先ストレージプール(プライマリストレージ)ID
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数

        Returns
        -------
        None
        """
        jobid = self.migrateVirtualMachine2Async(vmId, poolId)

        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult' 
        self.request['jobid'] = str(jobid)

        #マイグレーション状態を取得する
        while True:
            response = urllib2.urlopen(self.generateUrl())
            json_dict = json.loads(response.read())
            jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']
            if jobstatus == 0:
                callback(*callbackargs)
            elif jobstatus == 1:
                return
            else:
                raise RuntimeError('インスタンス {0} のROOTボリュームのマイグレーションに失敗しました。'.format(vmId))
            time.sleep(checkInterval)

    def migrateVirtualMachineWithVolumeAsync(self, vmId, destId, volumePoolTupleList):
        """仮想マシンをボリュームと共に非同期的にマイグレートします..

        仮想マシンをボリュームと共に非同期的にマイグレートします. 

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        destId: int
            マイグレート先ホストのID
        volumePoolTupleList: list
            タプル(ボリュームID, ストレージプール(プライマリストレージ)ID) のリスト
        checkInterval: int

        Returns
        -------
        None
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateVirtualMachineWithVolume'
        self.request['virtualmachineid'] = str(vmId)
        self.request['hostid'] = str(destId)
        i = 0
        for vpt in volumePoolTupleList:
            self.request['migrateto[{0}].volume'.format(str(i))] = str(vpt[0])
            self.request['migrateto[{0}].pool'.format(str(i))] = str(vpt[1])
            i += 1

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratevirtualmachinewithvolumeresponse']['jobid']

        return jobid

    def migrateVirtualMachineWithVolume(self, vmId, destId, volumePoolTupleList, checkInterval = 1, callback = lambda: None, callbackargs = []):
        """仮想マシンをボリュームと共に同期的にマイグレートします..

        仮想マシンをボリュームと共に同期的にマイグレートします. 

        Parameters
        ----------
        vmId: int
            仮想マシンのID
        destId: int
            マイグレート先ホストのID
        volumePoolTupleList: list
            タプル(ボリュームID, ストレージプール(プライマリストレージ)ID) のリスト
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数

        Returns
        -------
        None
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateVirtualMachineWithVolume'
        self.request['virtualmachineid'] = str(vmId)
        self.request['hostid'] = str(destId)
        i = 0
        for vpt in volumePoolTupleList:
            self.request['migrateto[{0}].volume'.format(str(i))] = str(vpt[0])
            self.request['migrateto[{0}].pool'.format(str(i))] = str(vpt[1])
            i += 1

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratevirtualmachinewithvolumeresponse']['jobid']

        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult' 
        self.request['jobid'] = str(jobid)

        #マイグレーション状態を取得する
        while True:
            response = urllib2.urlopen(self.generateUrl())
            json_dict = json.loads(response.read())
            jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']
            if jobstatus == 0:
                callback(*callbackargs)
            elif jobstatus == 1:
                return
            else:
                raise RuntimeError('インスタンス {0} のmigrateVirtualMachineWithVolumeに失敗しました。'.format(vmId))
            time.sleep(checkInterval)

    def migrateVolumeAsync(self, vol, storage):
        """ボリュームを非同期的にマイグレートします..

        ボリュームを非同期的にマイグレートします. 

        Parameters
        ----------
        vol: Volume
            マイグレートするボリューム
        poolId: StoragePool
            マイグレート先ストレージプール

        Returns
        -------
        str
            非同期ジョブID
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'migrateVolume'
        self.request['volumeid'] = str(vol.id)
        self.request['storageid'] = str(storage.id)
        self.request['livemigrate'] = 'true'
    
        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['migratevolumeresponse']['jobid']

        return jobid
        
    def migrateVolume(self, vol, storage, checkInterval = 1, callback = lambda: None, callbackargs = [], cancel_flag_pipe = None):
        """ボリュームを同期的にマイグレートします..

        ボリュームを同期的にマイグレートします. 

        Parameters
        ----------
        vol: Vplume
            マイグレートするボリュームを
        storage: StoragePoool
            マイグレート先ストレージ
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数
        cancel_flag_pipe: (file descriptor)
            キャンセル時に使用するパイプのファイルディスクリプタ. 何か書き込むと監視を中止する.

        Returns
        -------
        None
        """
        jobid = self.migrateVolumeAsync(vol, storage)
        self.waitForAsyncJobCompletion(jobid, checkInterval, callback, callbackargs, cancel_flag_pipe)

    def detachVolumeAsync(self, vol):
        """ボリュームを同期的にマイグレートします..

        ボリュームを同期的にマイグレートします. 

        Parameters
        ----------
        vol: Vplume
            デタッチするボリューム.

        Returns
        -------
        str
            非同期ジョブID
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'detachVolume'
        self.request['id'] = str(vol.id)

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['detachvolumeresponse']['jobid']

        return jobid

    def detachVolume(self, vol, checkInterval = 1, callback = lambda: None, callbackargs = [], cancel_flag_pipe = None):
        """ボリュームを同期的にデタッチします..

        Parameters
        ----------
        vol: Vplume
            デタッチするボリューム
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数
        cancel_flag_pipe: (file descriptor)
            キャンセル時に使用するパイプのファイルディスクリプタ. 何か書き込むと監視を中止する.

        Returns
        -------
        None
        """
        jobid = self.detachVolumeAsync(vol)
        self.waitForAsyncJobCompletion(jobid, checkInterval, callback, callbackargs, cancel_flag_pipe)

    def attachVolumeAsync(self, vol, vm, dev_id):
        """ボリュームを非同期的にアタッチします..

        Parameters
        ----------
        vol: Volume
            デタッチするボリューム
        vm: VirtualMachine
            アタッチ先仮想マシン
        dev_id: int
            デバイスID

        Returns
        -------
        str
            非同期ジョブID
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'attachVolume'
        self.request['id'] = str(vol.uuid)
        self.request['virtualmachineid'] = str(vm.id)
        self.request['deviceid'] = str(dev_id)

        #APIを実行する
        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobid = json_dict['attachvolumeresponse']['jobid']

        return jobid

    def attachVolume(self, vol, vm, dev_id, checkInterval=1, callback=lambda: None, callbackargs=[], cancel_flag_pipe=None):
        """ボリュームを同期的にアタッチします.

        Parameters
        ----------
        vol: Vplume
            アタッチするボリューム
        vm: VirtualMachine
            アタッチ先仮想マシン
        dev_id: int
            デバイスID(0: vda 1: vdb 2: vdc ...)
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数
        cancel_flag_pipe: (file descriptor)
            キャンセル時に使用するパイプのファイルディスクリプタ. 何か書き込むと監視を中止する.

        Returns
        -------
        None
        """
        jobid = self.attachVolumeAsync(vol, vm, dev_id)
        self.waitForAsyncJobCompletion(jobid, checkInterval, callback, callbackargs, cancel_flag_pipe)
        
    def waitForAsyncJobCompletion(self, jobid, checkInterval = 1, callback = lambda: None, callbackargs = [], cancel_flag_pipe = None, **kwargs):
        """非同期ジョブの完了を待機します.

        Parameters
        ----------
        jobid: str
            非同期ジョブID
        checkInterval: int
            マイグレーション状態監視間隔
        callback: def
            マイグレーション状態監視時に呼び出すメソッド
        callbackargs: list
            callbackに渡す引数
        cancel_flag_pipe: (file descriptor)
            キャンセル時に使用するパイプのファイルディスクリプタ. 何か書き込むと監視を中止する.
        **kwargs: dictionary
            キーワード引数

            ['func_to_call_before_iteration']: def
                マイグレーション状態の監視を行う直前に呼び出すメソッド。未指定の場合、lambda: None
            ['args_to_pass_before_iteration']: list
                ['func_to_call_before_iteration'] に渡す引数リスト. 未指定の場合, []
                
        Returns
        -------
        None
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult' 
        self.request['jobid'] = str(jobid)

        #キーワード引数
        func_to_call_before_iteration = kwargs.get('func_to_call_before_iteration', lambda: None)
        args_to_pass_before_iteration = kwargs.get('args_to_pass_before_iteration', [])
        #ジョブ状態を取得する
        while True:
            #ジョブ監視を行う前に実行するメソッド
            func_to_call_before_iteration(*args_to_pass_before_iteration)

            #↓通常の処理
            response = urllib2.urlopen(self.generateUrl())
            json_dict = json.loads(response.read())
            jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']
            if jobstatus == 0:
                callback(*callbackargs)
            elif jobstatus == 1:
                callback(*callbackargs)
                return
            else:
                raise RuntimeError('非同期ジョブ {0} の実行に失敗しました。'.format(jobid))
            time.sleep(checkInterval)

    def queryAsyncJobResult(self, jobid):
        """非同期ジョブの完了を待機します.

        Parameters
        ----------
        jobid: str
            非同期ジョブID

        Returns
        -------
        int
            ジョブ結果 0:pending 1:ok 2:failed
        """
        #パラメータをセットする
        self.__cleanRequest()
        self.request['command'] = 'queryAsyncJobResult'
        self.request['jobid'] = str(jobid)

        response = urllib2.urlopen(self.generateUrl())
        json_dict = json.loads(response.read())
        jobstatus = json_dict['queryasyncjobresultresponse']['jobstatus']

        return jobstatus
