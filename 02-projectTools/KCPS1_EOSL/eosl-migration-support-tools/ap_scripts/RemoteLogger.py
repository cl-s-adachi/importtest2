#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""リモートホストにログを出力するためのRemoteLoggerクラスを含むモジュールです."""
import sys
import Queue
import threading
import subprocess
import datetime
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)
 
class RemoteLogger:
    """リモートホストにログを出力するクラスです.

    Attributes
    ----------
    filename: str
        リモートホスト内でのファイル名 (ファイルパス)
    host: str
        ホスト名 または IPアドレス
    password: str
        パスワード
    user: str
        ユーザ名
    sshpass: str
        sshpassコマンドへのパス
    """

    def __init__(self, filename, host, password, user, sshpass):
        """RemoteLoggerクラスのインスタンスを作成します.

        Parameters
        ----------
        filename: str
            リモートホスト内でのファイル名 (ファイルパス)
        host: str
            ホスト名 または IPアドレス
        password: str
            パスワード
        user: str
            ユーザ名
        sshpass: str
            sshpassコマンドへのパス
        """
        self.filename = filename
        self.host = host
        self.password = password
        self.user = user
        self.sshpass = sshpass
        self.textQueue = Queue.Queue()
        threading.Thread(target=self.writelnWorker).start()
    def writeln(self, text):
        """リモートログに1行のメッセージを書き出します.直ちに書き出されるとは限りません.

        Parameters
        ----------
        text: str
            書き出すメッセージ.

        Returns
        -------
        None
        """
        text = str(None) if text is None else text
        self.textQueue.put('[{0}] {1}'.format(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S"), text))

    def writelnWorker(self):
        """リモートホストにsshでログインし、echoコマンドを実行してログにメッセージを書き出すメソッドです.

        Returns
        -------
        None
        """
        while True:
            text = self.textQueue.get()
            if text is None:
                self.textQueue.task_done()
                return
            cmdargs = []
            cmdargs.append(self.sshpass)
            cmdargs.append('-p')
            cmdargs.append(self.password)
            cmdargs.append('ssh')
            cmdargs.append('-oNumberOfPasswordPrompts=1')
            cmdargs.append('{0}@{1}'.format(self.user, self.host))
            cmdargs.append('echo "{0}" >> {1}'.format(text.replace('"', '\\"'), self.filename))
            p = subprocess.Popen(cmdargs)
            p.communicate()
            self.textQueue.task_done()

    def join(self):
        """すべてのログの出力が完了するまで待機します.

        Returns
        -------
        None
        """
        self.textQueue.put(None)
        self.textQueue.join()
        

    def checkSsh(self):
        """ssh接続を確認します.

        Returns
        -------
        リモートホストに正常にsshできた場合はTrue.それ以外の場合はFalse.
        """
        cmdargs = []
        cmdargs.append(self.sshpass)
        cmdargs.append('-p')
        cmdargs.append(self.password)
        cmdargs.append('ssh')
        cmdargs.append('-oNumberOfPasswordPrompts=1')
        cmdargs.append('{0}@{1}'.format(self.user, self.host))
        cmdargs.append('true')
        res = subprocess.call(cmdargs)
        return res == 0
