#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""ESXiインスタンス/ボリュームのマイグレーションを行うスクリプトです."""
from __future__ import print_function
import sys
import traceback
import time
import os
import datetime
import subprocess
import threading
import socket
import select
import Queue
from collections import namedtuple
import mysql.connector

from eosl_migrate_ap_common import MigrationCancelledException
from eosl_migrate_ap_common import MigrationCancellationError
from eosl_migrate_ap_common import MigrationNoncontinuableError
from eosl_migrate_ap_common import CancellationRequestedException
from eosl_migrate_ap_common import DatabaseError
from eosl_migrate_ap_common import printToStdoutAndRemoteLogger
from eosl_migrate_ap_common import MigrationInfo
from CloudStackApiClient import CloudStackApiClient
from VCSClient import VCSClient
from VCSClient import VCSClientError
from VCSClient import VCSSessionExpiredError
from VCSClient import VCSManagedObjectNotFoundError
from VCSTaskInfo import VCSTaskInfo
from RemoteLogger import RemoteLogger
from Logger import Logger
from ap_batch_common import Utility
from ap_batch_common.Host import Host
from ap_batch_common.SnapshotPolicy import SnapshotPolicy
from ap_batch_common.SshClient import SshClient
from ap_batch_common.StoragePool import StoragePool
from ap_batch_common.Volume import Volume
from ap_batch_common.MsHost import MsHost
from ap_batch_common.VirtualMachine import VirtualMachine
from ap_batch_common.Utility import pingMySQLConnection
from ap_batch_common.Utility import raiseExceptionOnReadable

system_vm_types = ['ConsoleProxy', 'DomainRouter', 'SecondaryStorageVm']

def get_snapshot_policies_by_volumes(ctx):
    """
    ボリュームからスナップショットポリシを取得します.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            connection: MySQLConnection
                DB接続
            volumes: list<Volume>
                ボリュームのリスト
            remotelogger: RemoteLogger
                ログ出力先

    Returns
    -------
    list<namedtuple<policy: SnapshotPolicy,volume: Volume>>
        スナップショットポリシとボリュームの対応リスト
    """
    #スナップショットポリシを取得する
    printToStdoutAndRemoteLogger('スナップショットポリシを取得します。',
                                 ctx.remotelogger)
    policy_volume_map = []
    policy_volume_type = namedtuple('policy_volume_type', ('policy', 'volume'))
    for i, vol in enumerate(ctx.volumes):
        printToStdoutAndRemoteLogger('[{0}] ボリューム {1} の'
                                     'スナップショットポリシ: '
                                     .format(i, vol), ctx.remotelogger)
        policies_to_add = SnapshotPolicy.create_instances_from_volume_id(ctx.connection, vol.id)
        for policy in policies_to_add:
            policy_volume_map.append(policy_volume_type(policy=policy, volume=vol))
        if len(policies_to_add) == 0:
            printToStdoutAndRemoteLogger(' (なし)', ctx.remotelogger)
        else:
            for j, pta in enumerate(policies_to_add):
                printToStdoutAndRemoteLogger(' [{0}] {1}'.format(j, pta), ctx.remotelogger)

    return policy_volume_map

def restore_snapshot_policies(ctx):
    """
    ボリュームからスナップショットポリシを取得します.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            apiclient: CloudStackApiClient
                APIクライアント
            connection: MySQLConnection
                DB接続
            policyvolumemap: list<namedtuple<policy: SnapshotPolicy, volume: Volume>>
                スナップショットポリシとボリューム対応のリスト
            remotelogger: RemoteLogger
                ログ出力先
    """
    #スナップショットポリシを復元する
    printToStdoutAndRemoteLogger('スナップショットポリシを復元します。',
                                 ctx.remotelogger)
    for pvm in ctx.policyvolumemap:
        if SnapshotPolicy.check_existence_by_uuid(ctx.connection, pvm.policy.uuid):
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} の'
                                         '復元は不要です。'.format(pvm.policy), ctx.remotelogger)
        else:
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} が'
                                         '消えているため、再作成する必要が'
                                         'あります。'.format(pvm.policy), ctx.remotelogger)
            csp_params = {}
            if pvm.policy.interval == 0:
                csp_params['intervaltype'] = 'HOURLY'
            elif pvm.policy.interval == 1:
                csp_params['intervaltype'] = 'DAILY'
            elif pvm.policy.interval == 2:
                csp_params['intervaltype'] = 'WEEKLY'
            elif pvm.policy.interval == 3:
                csp_params['intervaltype'] = 'MONTHLY'
            csp_params['maxsnaps'] = pvm.policy.maxSnaps
            csp_params['schedule'] = pvm.policy.schedule
            csp_params['timezone'] = pvm.policy.timezone
            csp_params['volumeid'] = pvm.volume.uuid
            csp_params['fordisplay'] = pvm.policy.display
            ctx.apiclient.createSnapshotPolicy(csp_params)
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} を'
                                         '復元しました。'
                                         .format(pvm.policy), ctx.remotelogger)
    printToStdoutAndRemoteLogger('スナップショットポリシを復元しました。',
                                 ctx.remotelogger)

#[メインスレッド]
def get_vc_host_by_data_center_id(connection, dcid):
    """
    データセンタ(ゾーン)IDからvCenterホストのIPアドレスを取得します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の戻り値
    dcid: int
        データセンタ(ゾーン)ID

    Returns
    -------
    host: str
        vCenterサーバのIPアドレス
    """
    cur = connection.cursor()
    cur.execute('select vcenter_host from vmware_data_center where id='
                '(select vmware_data_center_id '
                'from vmware_data_center_zone_map where zone_id=%s);', (str(dcid), ))
    row = cur.fetchone()
    cur.close()
    if row is None:
        raise RuntimeError('データセンタ(ゾーン) ID {0} に対応するvCenter Serverのホストを取得できませんでした。'
                           .format(str(dcid)))
    return row[0]

def get_vmware_migration_result(connection, diskuuid, destpoolid):
    """マイグレーション結果を確認し, 結果を文字列として返します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の結果
    diskuuid: str
        ボリュームのuuid
    destpoolid: str
        マイグレーション先ボリュームのid

    Returns
    -------
    (resstr: str, resbool: bool)
        resstr: マイグレーション結果文字列
        resbool: マイグレーション結果論理値 (True=成功)
    """
    connection.ping(reconnect=True, attempts=3, delay=5)
    vol = Volume.createInstanceByUuid(connection, diskuuid)

    if vol.state != 'Ready':
        return ('マイグレーション結果確認でエラーと判定されました。ディスク{0}の状態がReadyではありません。'.format(vol.id), False, )

    if str(vol.poolId).strip() != str(destpoolid).strip():
        return ('マイグレーション結果確認でエラーと判定されました。'
                'ディスク{0}が指定されたマイグレーション先ストレージに'
                'マイグレーションされていません。'.format(vol.id), False, )

    return ('マイグレーションは正常に完了しました。', True, )

def get_dest_storage_pool_by_tag(conn, volume, tag):
    """
    ボリュームのマイグレーション先ストレージプール(プライマリストレージ)を取得します.

    ストレージプールは、現在ボリュームが存在するストレージプールと同じクラスタ内のストレージプールが返されます.

    Parameters
    ----------
    conn: MySQLConnection
        mysql.connector.connect()の戻り値.
    volume: Volume
        マイグレーションするVolume.
    tag: str
        マイグレーション先ストレージプールのタグ.

    Returns
    -------
    destp: StoragePool
        現在ボリュームが存在するストレージプールと同じクラスタ内のマイグレーション先ストレージプール(プライマリストレージ).

    """
    attachedto = None if volume.instanceId is None else VirtualMachine(conn, volume.instanceId)
    if volume.hypervisorType != 'VMware':
        raise RuntimeError('ボリューム{0}[{1}] (アタッチ先: {2}[{3}])'
                           'のハイパーバイザはVMwareでは'
                           'ありません: {4}'
                           .format(volume.id, volume.name, '<アタッチされていません>'
                                   if attachedto is None else attachedto.id,
                                   '' if attachedto is None else attachedto.name,
                                   volume.hypervisorType))
    sourcepool = StoragePool(conn, volume.poolId)

    if sourcepool.tag == tag:
        raise RuntimeError('マイグレーション元ストレージプールのタグとマイグレーション先ストレージプールのタグが同じです: {0}'.format(tag))

    pools = []
    pools = sorted([x for x in StoragePool.getPoolsByTag(conn, tag)
                    if x.clusterId == sourcepool.clusterId], key=lambda x: x.getUsage())

    for destp in pools:
        if destp.hasEnoughSpace(volume.size):
            return destp
    raise RuntimeError('マイグレーション先として適切なストレージプール '
                       '(プライマリストレージ) が見つかりませんでした。'
                       'ストレージの容量が不足していないか'
                       '確認してください。')

def api_handler(server, pipe, cancel_pipe, im_cancel_status_queue,
                im_cancel_specify_task_queue, ams_pipe):
    """APIハンドラースレッドです.

    Parameters
    ----------
    server: socket
        待ち受けるUNIXドメインソケット.
    pipe: fd
        読み込みパイプへのファイルディスクリプタ. 読み取り可能状態になるとAPIサーバを終了します.
    cancel_pipe: fd
        書き込みパイプへのファイルディスクリプタ. 強制キャンセルが要求されたとき、読み取り可能状態になります.
    im_cancel_status_queue: Queue
        最大要素数1のQueue. マイグレーション強制キャンセルの状態が変化したときに文字列をput()する必要があります.
    im_cancel_specify_task_queue: Queue
        最大要素数1のQueue.
        マイグレーションセッション中止時 (vCenterタスク放置),
        vCenterタスクを指定してマイグレーションを
        強制キャンセルするときにput()されます.
        put()されるのはTaskMO文字列です.
    ams_pipe: fd
        書き込みパイプへのファイルディスクリプタ.
    """
    server.listen(1)
    client = None
    im_cancel_status = 'NONE'

    no_next_loop = False
    while True:
        read_wait_list = [server]
        if client is not None:
            read_wait_list.append(client)
        if pipe is not None:
            read_wait_list.append(pipe)
        read_sockets = select.select(read_wait_list, [], [])[0]
        for sock in read_sockets:
            if sock == pipe:
                server.shutdown(socket.SHUT_RDWR)
                server.close()
                no_next_loop = True
                break
            if sock == server:
                #クライアントからの接続要求
                client = server.accept()[0]
            else:
                #クライアントからのメッセージ
                try:
                    cmsg = client.recv(128)
                    if len(cmsg) == 0: #切れている
                        client.close()
                        client = None
                        continue
                    if cmsg == 'QUIT': #通信終了
                        client.send('BYE')
                        client.shutdown(socket.SHUT_RDWR)
                        client.close()
                        client = None
                    elif cmsg == 'IMMEDIATE_CANCEL':
                        #キャンセルフラグを立てる
                        os.write(cancel_pipe, 'hogehoge') #適当なデータをcancel_pipeに書き込むことにより、キャンセルフラグとする
                    elif cmsg == 'IMMEDIATE_CANCELLATION_STATUS':
                        #強制キャンセルの状態を通知する
                        #NONE: 強制キャンセルはリクエストされていない
                        #STARTING: 強制キャンセルするためのタスクを探している。
                        #RUNNING: 強制キャンセルは実行中である
                        #PENDING: 強制キャンセルがリクエストされたが、候補タスクが複数存在するため保留している
                        #PENDING状態の場合には、2行目以降に候補タスクが出力される。
                        try:
                            im_cancel_status = im_cancel_status_queue.get_nowait()
                            im_cancel_status_queue.task_done()
                        except Queue.Empty:
                            pass
                        client.send(im_cancel_status) #強制キャンセルの状態を送信する
                    #elif cmsg.startswith('IMMEDIATE_CANCEL_WITH_SPECIFIC_TASK'):
                        #キャンセル対象のvCenterタスクを指定してキャンセルする
                        #vc_task_mo = cmsg.split(':')[1].strip() #キャンセル対象のvCenterタスクのTaskMO
                        #try:
                            ##キューを空にする
                            #im_cancel_specify_task_queue.get_nowait()
                            #im_cancel_specify_task_queue.task_done()
                        #except Queue.Empty:
                            #pass
                        #キャンセル対象のvCenterタスクをQueueを通して通知する
                        #im_cancel_specify_task_queue.put(vc_task_mo)
                       #pass
                    #マイグレーションセッションを中止する (vCenterタスクは放置する)
                    elif cmsg.startswith('ABORT_MIGRATION_SESSION'):
                        #適当なデータをams_pipeに書き込むことにより、マイグレーションセッション中止フラグとする
                        os.write(ams_pipe, 'hogehoge')
                    else:
                        client.send('INVALID_COMMAND')
                except socket.error:
                    #エラー時には切断する
                    client.shutdown(socket.SHUT_RDWR)
                    client.close()
                    client = None
        if no_next_loop:
            break

def start_api_handler_thread(server):
    """APIハンドラースレッドを開始し、APIを叩ける状態にします.

    Parameters
    ----------
    server: socket
        UNIXドメインソケット. このソケットは bind() されている必要があります.

    Returns
    -------
    t: Thread
        APIハンドラースレッド
    wp: fd
        書き込みパイプへのファイルディスクリプタです. このファイルディスクリプタに何か書き込むとスレッドを終了します. (API待ち受けを終了します).
    r_cancel_pipe: fd
        読み込みパイプへのファイルディスクリプタです. 強制キャンセルコマンドが実行されると、このファイルディスクリプタは読み取り可能になります.
    im_cancel_status_queue: Queue
        最大要素数1のQueueオブジェクトです. put()すると, 強制キャンセル状態を更新することができます.
    im_cancel_specify_task_queue: Queue
        最大要素数1のQueueオブジェクトです. get() すると, 強制キャンセル対象のvCenterタスクを取得することができます.
    ams_wp: fd
        読み込みパイプへのファイルディスクリプタです.
        マイグレーションセッション中止コマンド
        (vCenterタスクを放置して終了するコマンド) が実行されると、
        このファイルディスクリプタは読み取り可能になります.
    """
    readpipe, writepipe = os.pipe()
    r_cancel_pipe, w_cancel_pipe = os.pipe()
    im_cancel_status_queue = Queue.Queue(1)
    im_cancel_specify_task_queue = Queue.Queue(1)
    ams_rp, ams_wp = os.pipe()
    thread = threading.Thread(target=api_handler,
                              args=(server, readpipe, w_cancel_pipe, im_cancel_status_queue,
                                    im_cancel_specify_task_queue, ams_wp))
    thread.start()
    return (thread, writepipe, r_cancel_pipe, im_cancel_status_queue,
            im_cancel_specify_task_queue, ams_rp)

def parse_args():
    """コマンドライン引数をパースし、結果をグローバル変数群に格納します.

    Returns
    -------
    dict
        ['ap_info']: namedtuple
            password: str
                APサーバのパスワード
            user: str
                APサーバにログインするユーザ名
        ['apikey']: str
            APIキー
        ['db']: namedtuple
            データベース情報
                host: str
                    DBホスト
                password: str
                    DBパスワード
        ['fsdirectory']: str
            フラグ/セッションファイルディレクトリ名
        ['migrationinfolist']: list<MigrationInfo>
            マイグレーション情報
        ['migrationtimepergigabyte']: int
            1GBあたりのマイグレーション見積もり時間 (秒)
        ['usage']: bool
            使用例を表示するかどうか
        ['remotelogdirectory']: str
            リモートログディレクトリ名
        ['remoteloghost']: str
            リモートログ保存先ホスト
        ['remotelogpassword']: str
            リモートログ保存先ホストにログインするためのパスワード
        ['remoteloguser']: str
            リモートログ保存先ホストにログインするユーザ名
        ['secretkey']: str
            シークレットキー
        ['sessionid']: str
            セッションID
        ['vcenter']: namedtuple
            vCenterサーバ情報
                user: str
                    ユーザ名
                password: str
                    パスワード
    """
    dbtype = namedtuple('db', ('host', 'password',))
    vcenter = namedtuple('vcenter', ('user', 'password',))
    ret = {}
    ret['apikey'] = None
    ret['db'] = dbtype(host=None, password=None)
    ret['fsdirectory'] = None
    ret['migrationinfolist'] = []
    ret['usage'] = False
    ret['pollinginterval'] = 300
    ret['remotelogdirectory'] = None
    ret['remoteloghost'] = None
    ret['remotelogpassword'] = None
    ret['remoteloguser'] = None
    ret['secretkey'] = None
    ret['sessionid'] = None
    ret['vcenter'] = vcenter(user=None, password=None)
    if len(sys.argv) < 2:
        ret['usage'] = True
        return

    ap_info = namedtuple('ap_info', ('password', 'user'))
    ret['ap_info'] = ap_info(password=None, user=None)

    i = 1
    while True:
        if i == len(sys.argv):
            break
        if sys.argv[i] == '--host':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--host オプションの後にホストIDが指定されていません。')
            if (len(ret['migrationinfolist']) == 0
                    or (ret['migrationinfolist'][-1].virtualMachineId is not None
                        and ret['migrationinfolist'][-1].hostId is not None)):
                ret['migrationinfolist'].append(MigrationInfo())
            if ret['migrationinfolist'][-1].hostId is not None:
                raise RuntimeError(
                    'ホスト {0} にマイグレーションする仮想マシンを'
                    ' --virtualmachine で指定してください。'.format(ret['migrationinfolist'][-1].hostId))
            ret['migrationinfolist'][-1].hostId = (
                -1 if sys.argv[i].lower() == 'none' else int(sys.argv[i]))

        elif sys.argv[i] == '--virtualmachine':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--virtualmachine オプションの後にインスタンスIDが指定されていません。')
            if (len(ret['migrationinfolist']) == 0
                    or (ret['migrationinfolist'][-1].virtualMachineId is not None
                        and ret['migrationinfolist'][-1].hostId is not None)):
                ret['migrationinfolist'].append(MigrationInfo())
            if ret['migrationinfolist'][-1].virtualMachineId is not None:
                raise RuntimeError(
                    '仮想マシン {0} のマイグレーション先ホストを --host で指定してください。'
                    .format(ret['migrationinfolist'][-1].virtualMachineId))
            ret['migrationinfolist'][-1].virtualMachineId = (
                -1 if sys.argv[i].lower() == 'none' else int(sys.argv[i]))
        elif sys.argv[i] == '--volume':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--volume オプションの後にボリュームIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                raise RuntimeError(
                    '--volume オプションの前に --host オプション または '
                    '--virtualmachine オプションを指定してください。')
            else:
                if len(ret['migrationinfolist'][-1].volumePoolTagTupleList) == 0:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList.append((sys.argv[i],
                                                                                None, ))
                elif ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][0] is None:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList[-1] = (
                        sys.argv[i], ret['migrationinfolist'][-1].volumePoolTagTupleList[1])
                elif ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][1] is None:
                    raise RuntimeError(
                        'ボリューム ID={0} に対するマイグレーション先ストレージプールのタグを'
                        ' --pooltag オプションで指定してください。'
                        .format(ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][0]))
                else:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList.append((sys.argv[i],
                                                                                None, ))
        elif sys.argv[i] == '--pooltag':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--pooltag オプションの後にストレージプールIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                raise RuntimeError(
                    '--pooltag オプションの前に --host オプション または '
                    '--virtualmachine オプションを指定してください。')
            else:
                if len(ret['migrationinfolist'][-1].volumePoolTagTupleList) == 0:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList.append((None,
                                                                                sys.argv[i], ))
                elif ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][1] is None:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList[-1] = (
                        (ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][0], sys.argv[i]))
                elif ret['migrationinfolist'][-1].volumePoolTagTuple[-1][0] is None:
                    raise RuntimeError(
                        'ストレージプール ID={0} に対するマイグレーション対象ボリュームを'
                        ' --volume オプションで指定してください。'
                        .format(ret['migrationinfolist'][-1].volumePoolTagTupleList[-1][1]))
                else:
                    ret['migrationinfolist'][-1].volumePoolTagTupleList.append((None,
                                                                                sys.argv[i], ))
        elif sys.argv[i] == '--appassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--appassword オプションの後にAPサーバのパスワードが指定されていません。')
            ret['ap_info'] = ap_info(password=sys.argv[i], user=ret['ap_info'].user)
        elif sys.argv[i] == '--apuser':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--apuser オプションの後にAPサーバにログインするためのユーザ名が指定されていません。')
            ret['ap_info'] = ap_info(password=ret['ap_info'].password, user=sys.argv[i])
        elif sys.argv[i] == '--apikey':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--apikey オプションの後にAPIキーが指定されていません。')
            ret['apikey'] = sys.argv[i]
        elif sys.argv[i] == '--secretkey':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--secretkey オプションの後にシークレットキーが指定されていません')
            ret['secretkey'] = sys.argv[i]
        elif sys.argv[i] == '--polling-interval':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--polling-interval オプションの後にポーリング間隔が指定されていません。')
            ret['pollinginterval'] = int(sys.argv[i])
        elif sys.argv[i] == '--sessionid':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--sessionid オプションの後にセッションIDが指定されていません。')
            ret['sessionid'] = sys.argv[i]
        elif sys.argv[i] == '--fsdir' or sys.argv[i] == '--fsdirectory':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--fsdir|--fsdirectory オプションの後にディレクトリ名が指定されていません。')
            ret['fsdirectory'] = sys.argv[i]
        elif sys.argv[i] == '--dbhost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--dbhost オプションの後にデータベースホスト名が指定されていません。')
            ret['db'] = dbtype(host=sys.argv[i], password=ret['db'].password)
        elif sys.argv[i] == '--dbpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--dbpassword オプションの後にデータベースパスワードが指定されていません。')
            ret['db'] = dbtype(host=ret['db'].host, password=sys.argv[i])
        elif sys.argv[i] == '--vcusername':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--vcusername オプションの後にvCenter Server ユーザ名が指定されていません。')
            ret['vcenter'] = vcenter(user=sys.argv[i], password=ret['vcenter'].password)
        elif sys.argv[i] == '--vcpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--vcpassword オプションの後にvCenter Server パスワードが指定されていません。')
            ret['vcenter'] = vcenter(user=ret['vcenter'].user, password=sys.argv[i])
        elif sys.argv[i] == '--migration-time-per-gigabyte':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--migration-time-per-gigabyte オプションの後に秒数が設定されていません。')
            ret['migrationtimepergigabyte'] = int(sys.argv[i])
        elif sys.argv[i] == '--remoteloghost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remoteloghost オプションの後にリモートログ出力先ホストが指定されていません。')
            ret['remoteloghost'] = sys.argv[i]
        elif sys.argv[i] == '--remoteloguser':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remoteloguser オプションの後にリモートログ出力先ホストにログインするためのユーザ名が指定されていません。')
            ret['remoteloguser'] = sys.argv[i]
        elif sys.argv[i] == '--remotelogpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remotelogpassword オプションの後に'
                                   'リモートログ出力先ホストにログインするための'
                                   'パスワードが指定されていません。')
            ret['remotelogpassword'] = sys.argv[i]
        elif sys.argv[i] == '--remotelogdirectory':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remotelogdirectory オプションの後にリモートログ出力先パスが指定されていません。')
            ret['remotelogdirectory'] = sys.argv[i]
        else:
            raise RuntimeError('不明なオプション \'{0}\''.format(sys.argv[i]))
        i += 1
    for minfo in ret['migrationinfolist']:
        for tagtuple in minfo.volumePoolTagTupleList:
            if tagtuple[0] is None:
                raise RuntimeError('ストレージプール ID={0} に対するマイグレーション対象ボリュームを --volume オプションで指定してください。'
                                   .format(tagtuple[1]))
            elif tagtuple[1] is None:
                raise RuntimeError(
                    'ボリューム ID={0} に対する'
                    'マイグレーション先ストレージプールのタグを '
                    '--pooltag オプションで指定してください。'
                    .format(tagtuple[0]))
            if ret['sessionid'] is not None and ret['fsdirectory'] is None:
                raise RuntimeError(
                    'セッションIDが指定されましたが、fsdirが指定されていません。'
                    '--fsdirオプションを使用してディレクトリを指定してください。')
    return ret

def attach_volumes(ctx):
    """ボリュームをアタッチします.

    Parameters
    ----------
    ctx: namedtuple
        アタッチコンテキスト.
            apiclient: CloudStackApiClient
                APIを実行するためのクライアント.
            remotelogger: RemoteLogger
                リモートロガー.
            virtualmachine: VirtualMachine
                アタッチ先仮想マシン.
            volumes: list<Volume>
                アタッチするボリュームのリスト.

    Returns
    -------
    None
    """
    attached_volumes = []
    for detachedvol in ctx.volumes:
        try:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} をアタッチしています...'
                .format(detachedvol), ctx.remotelogger)
            ctx.apiclient.attachVolume(detachedvol, ctx.virtualmachine, detachedvol.deviceId)
            attached_volumes.append(detachedvol)
            printToStdoutAndRemoteLogger(
                'ボリューム {0} をアタッチしました。'
                .format(detachedvol), ctx.remotelogger)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のアタッチを行う際に'
                'エラーが発生しました。'.format(detachedvol), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

def attach_volumes_with_exc(ctx, exc):
    """ボリュームをアタッチし、excをraiseします.

    attach_volumes(ctx)を呼び出した後,例外excをraiseします.

    Parameters
    ----------
    ctx: namedtuple
        アタッチコンテキスト. attach_volumes(ctx)の引数を参照.
    """
    try:
        attach_volumes(ctx)
    except:
        printToStdoutAndRemoteLogger('ボリュームのアタッチを行う際にエラーが発生しました。', ctx.remotelogger)
        printToStdoutAndRemoteLogger('アタッチコンテキスト:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(str(ctx), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
    finally:
        if exc is not None:
            raise exc

def detach_volumes(ctx):
    """インスタンスにアタッチされているボリュームをすべてデタッチします. デタッチに失敗した場合には、再アタッチを自動的に行います.

    Parameters
    ----------
    ctx: namedtuple
        デタッチコンテキスト.
            apiclient: CloudStackApiClient
                APIを実行するためのクライアント.
            remotelogger: RemoteLogger
                リモートロガー.
            virtualmachine: VirtualMachine
                すべてのボリュームをデタッチするインスタンス.

    Returns
    -------
    list<Volume>
        デタッチしたボリューム.
    """
    non_root_volumes = [x for x in ctx.virtualmachine.volumes if x.volumeType != 'ROOT']
    detached_volumes = []
    for x in non_root_volumes:
        try:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} をデタッチしています...'
                .format(x), ctx.remotelogger)
            ctx.apiclient.detachVolume(x)
            detached_volumes.append(x)
            printToStdoutAndRemoteLogger(
                'ボリューム {0} をデタッチしました。'
                .format(x), ctx.remotelogger)
        except BaseException, exc:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のデタッチに失敗しました。'
                .format(x), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            printToStdoutAndRemoteLogger('デタッチしたボリュームを再アタッチします。:', ctx.remotelogger)
            attach_context_type = namedtuple('attach_context_type', ('apiclient', 'remotelogger',
                                                                     'virtualmachine', 'volumes'))
            attach_context = attach_context_type(apiclient=ctx.apiclient,
                                                 remotelogger=ctx.remotelogger,
                                                 virtualmachine=ctx.virtualmachine,
                                                 volumes=detached_volumes)
            attach_volumes_with_exc(attach_context, exc)
    return detached_volumes

def live_migrate_vm(ctx):
    """仮想マシンをライブマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            apiclient: CloudStackApiClient
                APIを実行するためのクライアント.
            connection: MySQLConnection
                DB接続
            desthost: Host
                マイグレーション先ホスト.
            remotelogger: RemoteLogger
                リモートロガー.
            virtualmachine: VirtualMachine
                マイグレーションする仮想マシン.
    """
    if ctx.desthost is None:
        printToStdoutAndRemoteLogger(
            'live_migrate_vm: インスタンス {0} の移行先ホストに None が'
            '指定されたため、ライブマイグレーションを行いません。'
            .format(ctx.virtualmachine), ctx.remotelogger)
        return

    if ctx.desthost.id == ctx.virtualmachine.host.id:
        printToStdoutAndRemoteLogger(
            'live_migrate_vm: 移行元ホストと移行先ホストが同じため、'
            'ライブマイグレーションをスキップします。(ホスト {0})'
            .format(ctx.desthost), ctx.remotelogger)
        return

    printToStdoutAndRemoteLogger(
        'インスタンス {0} を ホスト{1} から ホスト{2} に移行します.'
        .format(ctx.virtualmachine, ctx.virtualmachine.host, ctx.desthost), ctx.remotelogger)
    #インスタンスを移動(Live Migration)する
    if ctx.virtualmachine.type in system_vm_types:
        #システムVM
        ctx.apiclient.migrateSystemVm(ctx.virtualmachine.id, ctx.desthost.id)
    else:
        #一般VM
        ctx.apiclient.migrateVirtualMachine(ctx.virtualmachine.id, ctx.desthost.id)
    printToStdoutAndRemoteLogger(
        'インスタンス {0} を ホスト{1} から ホスト{2} に移行しました。'
        .format(ctx.virtualmachine, ctx.virtualmachine.host, ctx.desthost), ctx.remotelogger)
    newvm = VirtualMachine(ctx.connection, ctx.virtualmachine.id) #マイグレーション後のVM情報
    if newvm.state != 'Running':
        raise RuntimeError(
            'インスタンス {0} のマイグレーション: '
            'マイグレーション後の状態が'
            'Runningではありません: {1}'.format(ctx.virtualmachine, newvm.state)
            )
    if str(newvm.hostId).strip() != str(ctx.desthost.id).strip():
        printToStdoutAndRemoteLogger(
            '[DEBUG] {0} != {1} Failed!'
            .format(newvm.hostId, ctx.desthost.id), ctx.remotelogger)
        raise RuntimeError(
            'インスタンス {0} のマイグレーション: '
            'インスタンスがホスト {1} に存在しません: {2}'
            .format(ctx.virtualmachine, ctx.desthost, newvm.hostId))

def live_migrate_vm_with_exc(ctx, exc):
    """
    Parameters
    ----------
    ctx: namedtuple
        ライブマイグレーションコンテキスト. live_migrate_vm(ctx)の引数を参照.
    """
    try:
        live_migrate_vm(ctx)
    except:
        printToStdoutAndRemoteLogger('ライブマイグレーションを行う際にエラーが発生しました。', ctx.remotelogger)
        printToStdoutAndRemoteLogger('ライブマイグレーションコンテキスト:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(str(ctx), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
    finally:
        if exc is not None:
            raise exc

def on_check_onlinevol_migration(ctx):
    """オンラインインスタンスにアタッチされたボリュームのマイグレーション状態を確認するメソッドです.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            estimatedmigrationtime: timedelta
                見積もりマイグレーション時間
            migrationstartedtime: datetime
                マイグレーション開始日時 (UTC)
            remotelogger: RemoteLogger
                リモートロガー
            statusqueue: Queue
                スクリプトの状態を表すキュー.
            volumepooltag: namedtuple<volume:Volume, pooltag: str>
                マイグレーション中のボリュームとストレージプールのタグ
            vcenter: namedtuple<user: str, password: str>
                vCenter情報
    """
    #強制キャンセル要求(vCenterタスクをキャンセルする)を確認する
    rlist = select.select([ctx.cancelpipefd], [], [], 0)[0]
    if len(rlist) > 0:
        ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
        printToStdoutAndRemoteLogger(
            'マイグレーションの強制キャンセルが'
            '要求されました。'
            'マイグレーションをキャンセルしています...',
            ctx.remotelogger)
        vc_host = get_vc_host_by_data_center_id(ctx.connection,
                                                ctx.volumepooltag.volume.dataCenterId)
        vcsclient = VCSClient(vc_host, ctx.vcenter.user,
                              ctx.vcenter.password)

        printToStdoutAndRemoteLogger(
            'vCenter Server [{0}] にログインしています...'
            .format(vc_host), ctx.remotelogger)
        login_flag = vcsclient.login2(
            3, printToStdoutAndRemoteLogger, [ctx.remotelogger],
            printToStdoutAndRemoteLogger,
            ['vCenter Serverへログインしています...', ctx.remotelogger])
        printToStdoutAndRemoteLogger(
            'vCenter Server [{0}] にログインしました。'
            .format(vc_host), ctx.remotelogger)
        if not login_flag:
            raise MigrationCancellationError('vCenter Serverへのログインに失敗しました。')
        try:
            #キューを空にする
            ctx.statusqueue.get_nowait()
            ctx.statusqueue.task_done()
        except Queue.Empty:
            pass
        ctx.statusqueue.put('STARTING') #強制キャンセルの状態は STARTING

        try:
            vcsclient.cancelAttachedVolumeMigrationTask(ctx.volumepooltag.volume,
                                                        ctx.connection)
            printToStdoutAndRemoteLogger(
                'アタッチされているボリューム {0} の'
                'マイグレーションを強制キャンセル'
                'しました。'.format(ctx.volumepooltag.volume), ctx.remotelogger)
        except MigrationCancellationError:
            printToStdoutAndRemoteLogger(
                'アタッチされているボリューム {0} の'
                'マイグレーションの強制キャンセルに'
                '失敗しました: {1}'
                .format(ctx.volumepooltag.volume, sys.exc_info()[1]), ctx.remotelogger)
        except:
            printToStdoutAndRemoteLogger(
                'アタッチされているボリューム {0} の'
                'マイグレーションの強制キャンセルを'
                '行う際に予期していないエラーが'
                '発生しました: {1}'
                .format(ctx.volumepooltag.volume, sys.exc_info()[1]), ctx.remotelogger)
        finally:
            raise MigrationCancelledException('マイグレーションセッションは強制キャンセルされました。')

    printToStdoutAndRemoteLogger(
        'ボリューム {0}:'
        'マイグレーションは実行中です。'
        .format(ctx.volumepooltag.volume), ctx.remotelogger)

    mtime = datetime.datetime.utcnow() - ctx.migrationstartedtime
    overtime = mtime - ctx.estimatedmigrationtime
    if mtime > ctx.estimatedmigrationtime:
        printToStdoutAndRemoteLogger('ボリューム {0}: 警告: マイグレーション予測時間 {1} を {2} 過ぎています。'
                                     .format(ctx.volumepooltag.volume, ctx.estimatedmigrationtime, overtime)
                                     , ctx.remotelogger)

def on_check_offrootvol_migration(ctx):
    """オフラインインスタンスのROOTボリュームのマイグレーション状態を確認するメソッドです.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            estimatedmigrationtime: timedelta
                見積もりマイグレーション時間
            migrationstartedtime: datetime
                マイグレーション開始日時 (UTC)
            remotelogger: RemoteLogger
                リモートロガー
            rootvolumepooltag: namedtuple<volume:Volume, pooltag: str>
                ルートボリュームとストレージプールのタグ
            vcenter: namedtuple<user: str, password: str>
                vCenter情報
    """
    #強制キャンセル要求(vCenterタスクをキャンセルする)を確認する
    rlist = select.select([ctx.cancelpipefd], [], [], 0)[0]
    if len(rlist) > 0:
        printToStdoutAndRemoteLogger(
            'マイグレーションの強制停止が'
            '要求されました。'
            'vCenterタスクのキャンセルを試みています...',
            ctx.remotelogger)

        ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
        #オンライン && アタッチドと同様の方法でキャンセルする
        vc_host = (
            get_vc_host_by_data_center_id(
                ctx.connection, ctx.rootvolumepooltag.volume.dataCenterId))
        vcsclient = VCSClient(vc_host, ctx.vcenter.user,
                              ctx.vcenter.password)
        try:
            vcsclient.cancelAttachedVolumeMigrationTask(
                ctx.rootvolumepooltag.volume, ctx.connection)
            printToStdoutAndRemoteLogger(
                'オフラインインスタンスに'
                'アタッチされているROOTボリューム {0} の'
                'マイグレーションを'
                '強制キャンセルしました。'
                .format(ctx.rootvolumepooltag.volume), ctx.remotelogger)
        except:
            printToStdoutAndRemoteLogger(
                'オフラインインスタンスに'
                'アタッチされているROOTボリューム {0} の'
                'マイグレーションを'
                '強制キャンセルできませんでした。'
                .format(ctx.rootvolumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
        finally:
            raise MigrationCancelledException('マイグレーションは強制停止されました。')

    printToStdoutAndRemoteLogger(
        'オフラインインスタンスに'
        'アタッチされているROOTボリューム {0}:'
        'マイグレーションは実行中です。'
        .format(ctx.rootvolumepooltag.volume), ctx.remotelogger)

    #マイグレーション時間警告
    mtime = datetime.datetime.utcnow() - ctx.migrationstartedtime
    overtime = mtime - ctx.estimatedmigrationtime
    if mtime > ctx.estimatedmigrationtime:
        printToStdoutAndRemoteLogger('ボリューム {0}: 警告: マイグレーション予測時間 {1} を {2} 過ぎています。'
                                     .format(ctx.rootvolumepooltag.volume, ctx.estimatedmigrationtime, overtime)
                                     , ctx.remotelogger)

def on_check_detachedvol_migration(ctx):
    """デタッチされたボリュームのマイグレーションの状態を確認するためのメソッドです.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            estimatedmigrationtime: timedelta
                見積もりマイグレーション時間
            migrationstartedtime: datetime
                マイグレーション開始日時 (UTC)
            statusqueue: Queue
                スクリプトの状態を表すキュー.
            vcenter: namedtuple<user: str, password: str>
                vCenter情報
            volumepooltag: namedtuple<volume: Volume, pooltag: str>
                マイグレーション中のボリュームと宛先ストレージプールのタグ
    """
    #強制キャンセル要求(vCenterタスク放置)を確認する
    rlist = select.select([ctx.abortmigrationsessionpipefd], [], [], 0)[0]
    if len(rlist) > 0:
        raise MigrationCancelledException('マイグレーションセッションの'
                                          '強制停止が要求されました。'
                                          'vCenterタスクは実行中です。')

    #強制キャンセル要求(vCenterタスクをキャンセルする)を確認する
    rlist = select.select([ctx.cancelpipefd], [], [], 0)[0]
    if len(rlist) > 0:
        try:
            printToStdoutAndRemoteLogger(
                'マイグレーションの強制停止が'
                '要求されました。'
                'vCenterタスクのキャンセルを試みています...',
                ctx.remotelogger)
            ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
            vc_host = (
                get_vc_host_by_data_center_id(
                    ctx.connection, ctx.volumepooltag.volume.dataCenterId))

            vcsclient = VCSClient(vc_host, ctx.vcenter.user,
                                  ctx.vcenter.password)
            login_flag = (
                vcsclient.login2(3, printToStdoutAndRemoteLogger,
                                 [ctx.remotelogger], printToStdoutAndRemoteLogger,
                                 ['vCenter Serverへ'
                                  'ログインしています...',
                                  ctx.remotelogger]))

            if not login_flag:
                raise RuntimeError('vCenter Serverへログインできませんでした。')

            tasks_to_cancel = (
                vcsclient.getTasksToCancelDetached(ctx.migrationstartedtime, datetime.timedelta(minutes=300)))
            if tasks_to_cancel is None or len(tasks_to_cancel) == 0:
                raise MigrationCancellationError('キャンセルすべきvCenterタスクの'
                                                 '取得に失敗しました。'
                                                 'マイグレーションがすでに'
                                                 '完了している可能性があります。')
            if len(tasks_to_cancel) == 1:
                try:
                    #キューを空にする
                    ctx.statusqueue.get_nowait()
                    ctx.statusqueue.task_done()
                except Queue.Empty:
                    pass
                ctx.statusqueue.put('RUNNING') #強制キャンセルの状態は RUNNING
                vcsclient.cancelTask2(tasks_to_cancel[0].taskMO)
                raise MigrationCancelledException('デタッチされているボリュームのマイグレーションの強制キャンセルに成功しました。')
            else: #タスクが複数ありキャンセルできない場合
                try:
                    #キューを空にする
                    ctx.statusqueue.get_nowait()
                    ctx.statusqueue.task_done()
                except Queue.Empty:
                    pass
                status_str = 'PENDING\n'
                status_str += '\n'.join(['{0},{1},{2}'
                                         .format(x.taskMO, x.descriptionId, x.queueTime)
                                         for x in tasks_to_cancel])
                ctx.statusqueue.put(status_str)
        except MigrationCancellationError:
            printToStdoutAndRemoteLogger(
                'デタッチド/オフラインボリューム {0} の'
                'マイグレーションを強制キャンセル'
                'できませんでした。'.format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(
                traceback.format_exc(), ctx.remotelogger)
            return
        except MigrationCancelledException:
            printToStdoutAndRemoteLogger(
                'デタッチド/オフラインボリューム {0} '
                'のマイグレーションを'
                '強制キャンセルしました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            return
        except:
            printToStdoutAndRemoteLogger(
                'デタッチド/オフラインボリューム {0} の'
                'マイグレーションを強制キャンセル'
                'できませんでした。'
                '既にマイグレーションが完了しているか、'
                'キャンセルすべきタスクを取得できなかった'
                '可能性があります。'.format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(
                traceback.format_exc(), ctx.remotelogger)
            return
        finally:
            raise MigrationCancelledException()

    printToStdoutAndRemoteLogger(
        'ボリューム {0}:'
        'マイグレーションは実行中です。'
        .format(ctx.volumepooltag.volume), ctx.remotelogger)

    #マイグレーション時間警告
    mtime = datetime.datetime.utcnow() - ctx.migrationstartedtime
    overtime = mtime - ctx.estimatedmigrationtime
    if mtime > ctx.estimatedmigrationtime:
        printToStdoutAndRemoteLogger('ボリューム {0}: 警告: マイグレーション予測時間 {1} を {2} 過ぎています。'
                                     .format(ctx.volumepooltag.volume, ctx.estimatedmigrationtime, overtime)
                                     , ctx.remotelogger)

def migrate_offline_root_volume(ctx):
    """オフラインインスタンスのROOTボリュームをマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            apiclient: CloudStackApiClient
                APIの実行に使用するクライアント
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            migrationtimepergigabyte: int
                マイグレーション見積もり時間 (秒)
            remotelogger: RemoteLogger
                リモートロガー
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            rootvolumepooltag: namedtuple<volume: Volume, pooltag: str>
                ROOTボリュームと,そのマイグレーション先ストレージプールのタグ.
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            virtualmachine: VirtualMachine
                オフラインインスタンス.
            vcenter: namedtuple<user:str, password: str>
                vCenter情報
    """
    destp = None
    try:
        destp = get_dest_storage_pool_by_tag(ctx.connection,
                                             ctx.rootvolumepooltag.volume,
                                             ctx.rootvolumepooltag.pooltag)
    except:
        printToStdoutAndRemoteLogger(
            'オフラインインスタンス {0} のROOTボリューム {1} '
            'のマイグレーション先ストレージプール'
            '(プライマリストレージ)を決定できませんでした。'
            .format(ctx.virtualmachine, ctx.rootvolumepooltag.volume), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
        printToStdoutAndRemoteLogger('このボリュームをスキップします。', ctx.remotelogger)
        raise

    if destp is not None:
        printToStdoutAndRemoteLogger(
            'オフラインインスタンス {0} のROOTボリューム {1} を'
            'ストレージプール {2} にマイグレーションします。'
            .format(ctx.virtualmachine, ctx.rootvolumepooltag.volume, destp), ctx.remotelogger)
        try:
            jobid = ctx.apiclient.migrateVirtualMachine2Async(ctx.virtualmachine.uuid, destp.id)
        except:
            printToStdoutAndRemoteLogger(
                'オフラインインスタンス {0} のROOTボリューム'
                ' {1} のマイグレーションに失敗しました。'
                .format(ctx.virtualmachine, ctx.rootvolumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        migrationstartedtime = datetime.datetime.utcnow() #マイグレーション開始時刻

        #+++++ROOTボリュームのマイグレーション状態監視 ここから-----
        while True:
            try:
                ctx.connection.close() #DB切断

                ocom_ctx_type = namedtuple('ocom_ctx_type', ('abortmigrationsessionpipefd',
                                                             'cancelpipefd', 'connection',
                                                             'estimatedmigrationtime',
                                                             'migrationstartedtime',
                                                             'remotelogger', 'rootvolumepooltag',
                                                             'vcenter'))
                ocom_ctx = ocom_ctx_type(
                                    abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                                    cancelpipefd=ctx.cancelpipefd, connection=ctx.connection,
                                    estimatedmigrationtime=
                                    datetime.timedelta(seconds=ctx.rootvolumepooltag.volume.getEstimatedMigrationTime(ctx.migrationtimepergigabyte)),
                                    migrationstartedtime=migrationstartedtime,
                                    remotelogger=ctx.remotelogger,
                                    rootvolumepooltag=ctx.rootvolumepooltag, vcenter=ctx.vcenter)
                ctx.apiclient.waitForAsyncJobCompletion(jobid,
                                               ctx.pollinginterval,
                                               on_check_offrootvol_migration, [ocom_ctx])
                ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
                printToStdoutAndRemoteLogger(
                    'オフラインインスタンス {0} の'
                    'ROOTボリューム {1} のマイグレーションが'
                    '完了しました。'
                    .format(ctx.virtualmachine, ctx.rootvolumepooltag.volume), ctx.remotelogger)
                printToStdoutAndRemoteLogger(
                    'オフラインインスタンス {0} の'
                    'ROOTボリューム {1}: {2}'.format(ctx.virtualmachine, ctx.rootvolumepooltag.volume, (
                        get_vmware_migration_result(ctx.connection,
                                                    ctx.rootvolumepooltag.volume.uuid,
                                                    destp.id)[0])),
                    ctx.remotelogger)

                break
            except:
                printToStdoutAndRemoteLogger(
                    'オフラインインスタンスに'
                    'アタッチされているROOTボリューム {0} の'
                    'マイグレーション中にエラーが発生しました。'
                    .format(ctx.rootvolumepooltag.volume), ctx.remotelogger)
                printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
                raise
            finally:
                ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
        #-----ROOTボリュームのマイグレーション状態監視 ここまで-----
        printToStdoutAndRemoteLogger(
            'オフラインインスタンス {0} のROOTボリューム '
            '{1} のマイグレーションが完了しました。'
            .format(ctx.virtualmachine, ctx.rootvolumepooltag.volume), ctx.remotelogger)


def migrate_onlattached_volume(ctx):
    """オンラインインスタンスにアタッチされた単一のボリュームをmigrateVolume APIを使用してマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            apiclient: CloudStackApiClient
                APIを実行するためのクライアント.
            apsshclientlist: list<SshClient>
                APサーバのMSログを監視するためのSshClientリスト
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            migrationtimepergigabyte: int
                マイグレーション見積もり時間 (秒)
            remotelogger: RemoteLogger
                リモートロガー
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            statusqueue: Queue
                セッションの状態を表すキュー
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            vcenter: namedtuple
                vCenter情報
                    user: str
                        ユーザ名
                    password: str
                        パスワード
            volumepooltag: namedtuple<volume:Volume, pooltag: str>
                Volumeと,マイグレーション先ストレージプールのタグのペア
    """
    #ボリュームの最新の状態を取得する
    presentvolume = Volume(ctx.connection, ctx.volumepooltag.volume.id)

    #ボリュームがアタッチされているインスタンス
    attachedto = (None if presentvolume.instanceId is None
                  else VirtualMachine(ctx.connection, presentvolume.instanceId))

    #アタッチ先インスタンスの状態確認
    if attachedto is None:
        raise RuntimeError('ボリューム {0} はデタッチされています。'.format(presentvolume))

    if attachedto.state != 'Running':
        raise RuntimeError('ボリューム {0} がアタッチされている'
                           'インスタンス {1} の状態がRunningではありません: '
                           '"{2}"'.format(presentvolume, attachedto, attachedto.state))

    try:
        destp = get_dest_storage_pool_by_tag(ctx.connection,
                                             ctx.volumepooltag.volume, ctx.volumepooltag.pooltag)
    except:
        printToStdoutAndRemoteLogger(
            'ボリューム {0} (アタッチ先: {1}) の'
            'マイグレーション先ストレージプールを'
            '決定できませんでした。'
            .format(ctx.volumepooltag.volume, attachedto), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
        raise

    printToStdoutAndRemoteLogger(
        'ボリューム {0} (アタッチ先: {1}) のストレージプール '
        '{2} へのマイグレーションを開始します。'
        .format(ctx.volumepooltag.volume, attachedto, destp), ctx.remotelogger)
    started_time = datetime.datetime.utcnow() #マイグレーション開始時刻(UTC; vCenterサーバがUTCのため)
    try:
        #+++++ MSログ監視 (実際のマイグレーション開始を待つためのプロセス) +++++
        ap_tailp_list = []
        ms_rlist = []
        try:
            for ap_sshc in ctx.apsshclientlist:
                tailp = ap_sshc[1].run(
                    ['tail', '-f',
                     '/var/log/cloudstack/management/management-server.log'])
                printToStdoutAndRemoteLogger('ログ監視プロセス {0} を作成しました。'.format(str(tailp)), ctx.remotelogger)
                grepp1 = subprocess.Popen(['grep', '--line-buffered',
                                           'Executing resource '
                                           'MigrateVolumeCommand'],
                                          stdin=tailp.stdout,
                                          stdout=subprocess.PIPE)
                grepp2 = subprocess.Popen(['grep', '--line-buffered',
                                           '"volumeId":{0}'
                                           .format(ctx.volumepooltag.volume.id)],
                                          stdin=grepp1.stdout,
                                          stdout=subprocess.PIPE)
                ap_tailp_list.append((ap_sshc[0], tailp, ))
                ms_rlist.append(grepp2.stdout.fileno())
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーションの'
                '準備中にエラーが発生しました: '
                'マネジメントサーバでログの監視を行うことが'
                'できませんでした。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        #----- MSログ監視 (実際のマイグレーション開始を待つためのプロセス) -----
        try:
            #[本処理] ボリュームを非同期でマイグレートする
            jobid = ctx.apiclient.migrateVolumeAsync(ctx.volumepooltag.volume, destp)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} の'
                'マイグレーションに失敗しました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        #実際のマイグレーション開始を待機する
        try:
            #キューを空にする
            ctx.statusqueue.get_nowait()
            ctx.statusqueue.task_done()
        except Queue.Empty:
            pass

        #停止スクリプトで 「保留中」 とだすため
        ctx.statusqueue.put('WAITING_FOR_MIGRATION_START')
        printToStdoutAndRemoteLogger(
            'ボリューム {0} のマイグレーションの'
            'マネジメントサーバでの開始を待機しています...'
            .format(ctx.volumepooltag.volume), ctx.remotelogger)

        try:
            if len(ap_tailp_list) != 0:
                while True:
                    for ap_tailp in ap_tailp_list:
                        if ap_tailp[1].poll() is not None:
                            raise RuntimeError(
                                'APサーバ {0} でのログの監視が'
                                '予期せず終了しました。'
                                .format(ap_tailp[0].name))
                        else:
                            printToStdoutAndRemoteLogger(
                                'APサーバ {0}: 監視中'
                                .format(ap_tailp[0].name), ctx.remotelogger)

                    #ジョブ監視
                    jobstatus = ctx.apiclient.queryAsyncJobResult(jobid)
                    if jobstatus == 0:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブは実行中 (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                    elif jobstatus == 1:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブが予期せず完了しています (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                        break
                    elif jobstatus == 2:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブが失敗しました (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                        raise RuntimeError('マイグレーションジョブ(ジョブID: {0})が失敗しました。'.format(jobid))
                    else:
                        raise RuntimeError('マイグレーションジョブ(ジョブID: {0}))が不明な状態になっています: {1}'.format(jobid, jobstatus))

                    #ログ確認
                    rlist = select.select(ms_rlist, [], [], 10)[0]
                    if any(i in ms_rlist for i in rlist):
                        break


                    printToStdoutAndRemoteLogger(
                        'ボリューム {0} のマイグレーションは'
                        'マネジメントサーバで保留中です。'
                        .format(ctx.volumepooltag.volume), ctx.remotelogger)
                printToStdoutAndRemoteLogger(
                    'ボリューム {0} のマイグレーションが'
                    'マネジメントサーバで開始されました。'
                    .format(ctx.volumepooltag.volume), ctx.remotelogger)
                migrationstartedtime = datetime.datetime.utcnow()
            else:
                printToStdoutAndRemoteLogger(
                    'APサーバが指定されなかったため、'
                    'マイグレーションの開始の検出をスキップします。',
                    ctx.remotelogger)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} : MSログの監視中に'
                'エラーが発生しました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        try:
            #キューを空にする
            ctx.statusqueue.get_nowait()
            ctx.statusqueue.task_done()
        except Queue.Empty:
            pass
        ctx.statusqueue.put('MIGRATING')

        #インスタンスのstateをMigratingに変更する
        if attachedto is not None:
            time.sleep(10)
            try:
                printToStdoutAndRemoteLogger(
                    'インスタンス {0} の状態を {1} から Migrating に'
                    '変更しています...'.format(attachedto, attachedto.state), ctx.remotelogger)
                attachedto.setState(ctx.connection, 'Migrating')
                printToStdoutAndRemoteLogger(
                    'インスタンス {0} の状態を {1} から Migrating に'
                    '変更しました。'.format(attachedto, attachedto.state), ctx.remotelogger)
            except:
                printToStdoutAndRemoteLogger(
                    'エラー: インスタンス {0} の状態を'
                    '変更できませんでした。'
                    .format(attachedto), ctx.remotelogger)
                printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
                raise

    finally:
        for apserver, tailprocess in ap_tailp_list:
            tailprocess.terminate()
            printToStdoutAndRemoteLogger(
                'APサーバ {0} でのログ監視を終了しました。'
                .format(apserver.name), ctx.remotelogger)

    #+++++ マイグレーション状態監視 ここから +++++
    while True:
        try:
            ctx.connection.close() #DB切断

            #オンラインインスタンスにアタッチされたボリュームの
            #マイグレーション状態監視
            ocom_ctx_type = namedtuple('ocom_ctx_type', ('cancelpipefd', 'connection',
                                                         'estimatedmigrationtime',
                                                         'migrationstartedtime',
                                                         'remotelogger', 'statusqueue',
                                                         'vcenter', 'volumepooltag',))
            ocom_ctx = ocom_ctx_type(cancelpipefd=ctx.cancelpipefd, connection=ctx.connection,
                                     estimatedmigrationtime=
                                     datetime.timedelta(seconds=ctx.volumepooltag.volume.getEstimatedMigrationTime(ctx.migrationtimepergigabyte)),
                                     migrationstartedtime=migrationstartedtime,
                                     remotelogger=ctx.remotelogger, statusqueue=ctx.statusqueue,
                                     vcenter=ctx.vcenter,
                                     volumepooltag=ctx.volumepooltag)

            #マイグレーションジョブの完了を待つ
            #pollingintervalごとにon_check_onlinevol_migrationが実行される
            ctx.apiclient.waitForAsyncJobCompletion(jobid,
                ctx.pollinginterval, on_check_onlinevol_migration, [ocom_ctx])


            ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーションが'
                '完了しました。'.format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger(
                'ボリューム {0}: {1}'
                .format(ctx.volumepooltag.volume,
                        get_vmware_migration_result(ctx.connection,
                                                    ctx.volumepooltag.volume.uuid,
                                                    destp.id)[0]), ctx.remotelogger)

            break
        except MigrationCancelledException:
            printToStdoutAndRemoteLogger(
                'ボリューム {0}:'
                'マイグレーションは強制キャンセルされました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            raise
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーション中に'
                '予期していないエラーが発生しました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            printToStdoutAndRemoteLogger('このボリュームのマイグレーションをスキップします。', ctx.remotelogger)
            raise
        finally:
            #インスタンスのstateをMigratingから戻す
            if attachedto is not None:
                try:
                    printToStdoutAndRemoteLogger(
                        'インスタンス {0} の状態を Migrating から '
                        '{1} に変更しています...'
                        .format(attachedto, attachedto.state), ctx.remotelogger)

                    #DB再接続
                    ctx.connection.ping(reconnect=True, attempts=3, delay=5)

                    attachedto.setState(ctx.connection, attachedto.state)
                    printToStdoutAndRemoteLogger(
                        'インスタンス {0} の状態を Migrating から '
                        '{1} に変更しました。'
                        .format(attachedto, attachedto.state), ctx.remotelogger)
                except:
                    printToStdoutAndRemoteLogger(
                        '警告: インスタンス {0} の状態を変更できませんでした。'
                        .format(attachedto), ctx.remotelogger)
                    printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
                    printToStdoutAndRemoteLogger(traceback.format_exc()
                                                 , ctx.remotelogger)
    #----- マイグレーション状態監視 ここまで -----



def migrate_online_instance(ctx):
    """オンラインインスタンスをマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            apiclient: CloudStackApiClient
                APIの実行に使うクライアント.
            apsshclientlist: list<SshClient>
                APサーバのMSログ監視に使うSshClient
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            hahost: Host
                使用するHAホスト.
            migrationtimepergigabyte: int
                マイグレーション見積もり時間 (秒)
            remotelogger: RemoteLogger
                リモートロガー
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            statusqueue: Queue
                スクリプトの状態を表すキュー.
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            vcenter: namedtuple
                vCenter情報
                    user: str
                        ユーザ名
                    password: str
                        パスワード
            virtualmachine: VirtualMachine
                マイグレーションする仮想マシン.
            volumepooltaglist: list<volume: Volume, pooltag: str>
                マイグレーションするボリュームと,そのマイグレーション先のストレージプールのタグ.

    Returns
    -------
    int: マイグレーションに失敗したボリュームの数
    """
    printToStdoutAndRemoteLogger(
        'インスタンス {0} のライブマイグレーション(1/2回目)を開始します。'
        .format(ctx.virtualmachine), ctx.remotelogger)
    lm_ctx_type = namedtuple('lm_ctx_type', ('apiclient', 'connection', 'desthost',
                                             'remotelogger', 'virtualmachine'))
    lm_ctx = lm_ctx_type(apiclient=ctx.apiclient, connection=ctx.connection, desthost=ctx.hahost,
                         remotelogger=ctx.remotelogger, virtualmachine=ctx.virtualmachine)
    live_migrate_vm(lm_ctx)
    printToStdoutAndRemoteLogger(
        'インスタンス {0} のライブマイグレーション(1/2回目)が'
        '完了しました。'.format(ctx.virtualmachine), ctx.remotelogger)

    #インスタンスを元のホストに戻すための情報
    newvm = VirtualMachine(ctx.connection, ctx.virtualmachine.id)
    lm_ctx = lm_ctx_type(apiclient=ctx.apiclient, connection=ctx.connection,
                         desthost=ctx.virtualmachine.host, remotelogger=ctx.remotelogger,
                         virtualmachine=newvm)

    monv_ctx_type = namedtuple('monv_ctx_type', ('apiclient', 'apsshclientlist', 'connection',
                                             'cancelpipefd',
                                             'migrationtimepergigabyte',
                                             'remotelogger',
                                             'remotehistorylogger', 'statusqueue', 'vcenter',
                                             'pollinginterval',
                                             'volumepooltag'))

    numfailed = 0
    for i, volumepooltag in enumerate(ctx.volumepooltaglist):
        monv_ctx = monv_ctx_type(apiclient=ctx.apiclient, apsshclientlist=ctx.apsshclientlist,
                             cancelpipefd=ctx.cancelpipefd, connection=ctx.connection,
                             migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                             remotelogger=ctx.remotelogger,
                             remotehistorylogger=ctx.remotehistorylogger, vcenter=ctx.vcenter,
                             statusqueue=ctx.statusqueue,
                             pollinginterval=ctx.pollinginterval,
                             volumepooltag=volumepooltag)
        try:
            migrate_onlattached_volume(monv_ctx)
        except MigrationCancelledException, exc:
            printToStdoutAndRemoteLogger('マイグレーションの強制停止処理が行われました。',
                                         ctx.remotelogger)
            printToStdoutAndRemoteLogger('インスタンス{0} を ホスト{1} から '
                                         'ホスト{2} に戻します。'
                                         .format(lm_ctx.virtualmachine, lm_ctx.virtualmachine.host, lm_ctx.desthost),
                                         ctx.remotelogger)
            live_migrate_vm_with_exc(lm_ctx, exc)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーション中に'
                'エラーが発生しました。'
                .format(volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            printToStdoutAndRemoteLogger('このボリュームをスキップします。', ctx.remotelogger)
            numfailed += 1
            continue

    printToStdoutAndRemoteLogger(
        'インスタンス {0} のライブマイグレーション(2/2回目)を開始します。'
        .format(ctx.virtualmachine), ctx.remotelogger)

    live_migrate_vm(lm_ctx)
    printToStdoutAndRemoteLogger(
        'インスタンス {0} のライブマイグレーション(2/2回目)が'
        '完了しました。'.format(ctx.virtualmachine), ctx.remotelogger)

    #最終チェック
    ctx.connection.ping(reconnect=True, attempts=3, delay=5)
    completedvm = VirtualMachine(ctx.connection, ctx.virtualmachine.id)
    for vol in completedvm.volumes:
        pool = StoragePool(ctx.connection, vol.poolId)
        if pool.tag != 'SYSTEM_DISK' and pool.tag != 'DATA_DISK':
            printToStdoutAndRemoteLogger(
                '警告: インスタンス {0} にアタッチされているボリューム {1}'
                ' がSYSTEM_DISKストレージでもDATA_DISKストレージでもない'
                'ストレージに存在します。'
                .format(ctx.virtualmachine, vol), ctx.remotelogger)

    return numfailed

def migrate_offline_instance(ctx):
    """オフラインインスタンスをマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            apiclient: CloudStackApiClient
                APIの実行に使用するクライアント.
            apsshclientlist: list<SshClient>
                APサーバでMSログを監視するためのSshClient
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            migrationtimepergigabyte: int
                マイグレーション見積もり時間 (秒)
            remotelogger: RemoteLogger
                リモートロガー
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            statusqueue: Queue
                スクリプトの状態を表すキュー.
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            vcenter: namedtuple
                vCenter情報
                    user: str
                        ユーザ名
                    password: str
                        パスワード
            virtualmachine: VirtualMachine
                マイグレーションする仮想マシン
            volumepooltaglist: list<namedtuple<volume: Volume, pooltag: str>>
                マイグレーションするボリュームと,そのマイグレーション先のストレージプールのタグのペアのリスト.

    Returns
    -------
    int: マイグレーションに失敗したボリュームの数
    """
    #ROOTボリューム以外をデタッチする
    detach_context_type = namedtuple('detach_context_type', ('apiclient', 'remotelogger',
                                                                     'virtualmachine'))
    detach_context = detach_context_type(apiclient=ctx.apiclient, remotelogger=ctx.remotelogger,
                                         virtualmachine=ctx.virtualmachine)
    detached_volumes = detach_volumes(detach_context)

    numfailed = 0

    #ROOTボリュームを取得する
    rootvolumepooltaglist = [x for x in ctx.volumepooltaglist if x.volume.volumeType == 'ROOT']
    if len(rootvolumepooltaglist) > 0:
        rootvolumepooltag = rootvolumepooltaglist[0]

        #ROOTボリュームをマイグレーションする
        morv_ctx_type = namedtuple('morv_ctx_type', ('abortmigrationsessionpipefd', 'apiclient',
                                                     'cancelpipefd', 'connection',
                                                     'migrationtimepergigabyte', 'remotelogger',
                                                     'remotehistorylogger', 'rootvolumepooltag',
                                                     'statusqueue',
                                                     'pollinginterval',
                                                     'vcenter', 'virtualmachine'))
        morv_ctx = morv_ctx_type(abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                                 apiclient=ctx.apiclient, cancelpipefd=ctx.cancelpipefd,
                                 connection=ctx.connection,
                                 migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                                 remotelogger=ctx.remotelogger,
                                 remotehistorylogger=ctx.remotehistorylogger,
                                 statusqueue=ctx.statusqueue,
                                 rootvolumepooltag=rootvolumepooltag,
                                 pollinginterval=ctx.pollinginterval,
                                 vcenter=ctx.vcenter,
                                 virtualmachine=ctx.virtualmachine)

        try:
            migrate_offline_root_volume(morv_ctx)
        except MigrationCancelledException:
            raise
        except:
            printToStdoutAndRemoteLogger('オフラインインスタンス {0} のROOTボリューム'
                                         ' {1} のマイグレーションに失敗しました。'
                                         .format(morv_ctx.virtualmachine,
                                                 morv_ctx.rootvolumepooltag.volume),
                                         ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            numfailed += 1

    #VMの状態をMigratingにする (デタッチしているため、マイグレーションコマンド発行前でもOK)
    prevstate = ctx.virtualmachine.state
    ctx.virtualmachine.setState(ctx.connection, 'Migrating')

    #ROOTボリューム以外をマイグレーションする
    nonrootvolumepooltaglist = [x for x in ctx.volumepooltaglist if x.volume.volumeType != 'ROOT']
    mv_ctx_type = namedtuple('mv_ctx_type', ('abortmigrationsessionpipefd', 'apiclient',
                                             'apsshclientlist', 'cancelpipefd', 'connection',
                                             'migrationtimepergigabyte',
                                             'pollinginterval',
                                             'remotelogger', 'statusqueue',
                                             'vcenter', 'volumepooltag',))

    #デタッチしたボリュームを再アタッチするための情報
    av_ctx_type = namedtuple('av_ctx_type', ('apiclient', 'remotelogger', 'virtualmachine',
                                             'volumes',))
    av_ctx = av_ctx_type(apiclient=ctx.apiclient, remotelogger=ctx.remotelogger,
                         virtualmachine=ctx.virtualmachine, volumes=detached_volumes)

    for i, nonrootvolumepooltag in enumerate(nonrootvolumepooltaglist):
        mv_ctx = mv_ctx_type(abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                             apiclient=ctx.apiclient, apsshclientlist=ctx.apsshclientlist,
                             cancelpipefd=ctx.cancelpipefd,
                             connection=ctx.connection,
                             migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                             pollinginterval=ctx.pollinginterval,
                             remotelogger=ctx.remotelogger,
                             statusqueue=ctx.statusqueue,
                             vcenter=ctx.vcenter, volumepooltag=nonrootvolumepooltag)
        try:
            migrate_detached_volume(mv_ctx)
        except MigrationCancelledException, exc:
            try:
                printToStdoutAndRemoteLogger('マイグレーションの強制停止処理が行われました。',
                                             ctx.remotelogger)

                #VMの状態を元に戻す
                ctx.virtualmachine.setState(ctx.connection, prevstate)

                printToStdoutAndRemoteLogger('インスタンス {0} からデタッチしたボリュームを再アタッチします'
                                             .format(ctx.virtualmachine),
                                             ctx.remotelogger)

                ctx.connection.ping(reconnect=True, attempts=3, delay=5)

                #ボリュームの状態がReadyになるまで待機する
                printToStdoutAndRemoteLogger('ボリューム {0} の状態が'
                                             'Readyになるまで待機しています...'
                                             .format(nonrootvolumepooltag.volume),
                                             ctx.remotelogger)
                Volume.waitForStateByUuid(ctx.connection,
                                          nonrootvolumepooltag.volume.uuid, 'Ready')

                #デタッチしたボリュームを再アタッチする
                attach_volumes(av_ctx)
            except:
                printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション'
                                             '強制停止後処理中にエラーが発生しました。',
                                             ctx.remotelogger)
                printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
                printToStdoutAndRemoteLogger('このボリュームをスキップします。', ctx.remotelogger)
            finally:
                #確実にMigrationCancelledExceptionを発生させるため
                raise exc
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーション中に'
                'エラーが発生しました。'
                .format(nonrootvolumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            printToStdoutAndRemoteLogger('このボリュームをスキップします。', ctx.remotelogger)
            numfailed += 1

    #VMの状態を元に戻す
    ctx.connection.ping(reconnect=True, attempts=3, delay=5)
    ctx.virtualmachine.setState(ctx.connection, prevstate)

    #デタッチしたボリュームを再アタッチする
    attach_volumes(av_ctx)

    #最終チェック
    ctx.connection.ping(reconnect=True, attempts=3, delay=5)
    completedvm = VirtualMachine(ctx.connection, ctx.virtualmachine.id)
    for vol in completedvm.volumes:
        pool = StoragePool(ctx.connection, vol.poolId)
        if pool.tag != 'SYSTEM_DISK' and pool.tag != 'DATA_DISK':
            printToStdoutAndRemoteLogger(
                '警告: インスタンス {0} にアタッチされているボリューム {1}'
                ' がSYSTEM_DISKストレージでもDATA_DISKストレージでもない'
                'ストレージに存在します。'
                .format(ctx.virtualmachine, vol), ctx.remotelogger)

    return numfailed

def migrate_detached_volume(ctx):
    """単一のデタッチドボリュームをマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            apiclient: CloudStackApiClient
                APIの実行に使用するクライアント.
            apsshclientlist: list<SshClient>
                APサーバでMSログを監視するためのSshClientリスト.
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            remotelogger: RemoteLogger
                リモートロガー.
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            statusqueue: Queue
                スクリプトの状態を表すキュー.
            vcenter: namedtuple
                vCenter情報
                    user: str
                        ユーザ名
                    password: str
                        パスワード
            volumepooltag: namedtuple<volume: Volume, pooltag: str>
                ボリュームとそのマイグレーション先ストレージプールのタグ.

    Returns
    -------
    None
    """
    try:
        destp = get_dest_storage_pool_by_tag(ctx.connection,
                                             ctx.volumepooltag.volume, ctx.volumepooltag.pooltag)
    except:
        printToStdoutAndRemoteLogger(
            'ボリューム {0} の'
            'マイグレーション先ストレージプールを'
            '決定できませんでした。'
            .format(ctx.volumepooltag.volume), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
        raise


    #スナップショットポリシを取得する
    gspbv_ctx_type = namedtuple('gspbv_ctx_type', ('connection', 'volumes', 'remotelogger'))
    gspbv_ctx = gspbv_ctx_type(connection=ctx.connection, volumes=[ctx.volumepooltag.volume],
                               remotelogger=ctx.remotelogger)
    policy_volume_map = get_snapshot_policies_by_volumes(gspbv_ctx)

    printToStdoutAndRemoteLogger(
        'ボリューム {0} のストレージプール '
        '{1} へのマイグレーションを開始します。'
        .format(ctx.volumepooltag.volume, destp), ctx.remotelogger)
    started_time = datetime.datetime.utcnow() #マイグレーション開始時刻(UTC; vCenterサーバがUTCのため)
    try:
        #+++++ MSログ監視 (実際のマイグレーション開始を待つためのプロセス) +++++
        ap_tailp_list = []
        ms_rlist = []
        try:
            for ap_sshc in ctx.apsshclientlist:
                tailp = ap_sshc[1].run(
                    ['tail', '-f',
                     '/var/log/cloudstack/management/management-server.log'])
                grepp1 = subprocess.Popen(['grep', '--line-buffered',
                                           'Executing resource '
                                           'MigrateVolumeCommand'],
                                          stdin=tailp.stdout,
                                          stdout=subprocess.PIPE)
                grepp2 = subprocess.Popen(['grep', '--line-buffered',
                                           '"volumeId":{0}'
                                           .format(ctx.volumepooltag.volume.id)],
                                          stdin=grepp1.stdout,
                                          stdout=subprocess.PIPE)
                ap_tailp_list.append((ap_sshc[0], tailp, ))
                ms_rlist.append(grepp2.stdout.fileno())
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} のマイグレーションの'
                '準備中にエラーが発生しました: '
                'マネジメントサーバでログの監視を行うことが'
                'できませんでした。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        #----- MSログ監視 (実際のマイグレーション開始を待つためのプロセス) -----
        try:
            #[本処理] ボリュームを非同期でマイグレートする
            jobid = ctx.apiclient.migrateVolumeAsync(ctx.volumepooltag.volume, destp)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} の'
                'マイグレーションに失敗しました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        #実際のマイグレーション開始を待機する
        try:
            #キューを空にする
            ctx.statusqueue.get_nowait()
            ctx.statusqueue.task_done()
        except Queue.Empty:
            pass

        #停止スクリプトで 「保留中」 とだすため
        ctx.statusqueue.put('WAITING_FOR_MIGRATION_START')
        printToStdoutAndRemoteLogger(
            'ボリューム {0} のマイグレーションの'
            'マネジメントサーバでの開始を待機しています...'
            .format(ctx.volumepooltag.volume), ctx.remotelogger)

        try:
            if len(ap_tailp_list) != 0:
                while True:
                    rlist = select.select(ms_rlist, [], [], 10)[0]

                    #ジョブ監視
                    jobstatus = ctx.apiclient.queryAsyncJobResult(jobid)
                    if jobstatus == 0:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブは実行中 (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                    elif jobstatus == 1:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブが予期せず完了しています (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                        break
                    elif jobstatus == 2:
                        printToStdoutAndRemoteLogger(
                            'マイグレーションジョブが失敗しました (ステータス {0})'
                            .format(jobstatus), ctx.remotelogger)
                        raise RuntimeError('マイグレーションジョブ(ジョブID: {0})が失敗しました。'.format(jobid))
                    else:
                        raise RuntimeError('マイグレーションジョブ(ジョブID: {0}))が不明な状態になっています: {1}'.format(jobid, jobstatus))

                    #ログチェック
                    if any(i in ms_rlist for i in rlist):
                        break

                    for ap_tailp in ap_tailp_list:
                        if ap_tailp[1].poll() is None:
                            raise RuntimeError(
                                'APサーバ {0} でのログの監視が'
                                '予期せず終了しました。'
                                .format(ap_tailp[0].name))
                    printToStdoutAndRemoteLogger(
                        'ボリューム {0} のマイグレーションは'
                        'マネジメントサーバで保留中です。'
                        .format(ctx.volumepooltag.volume), ctx.remotelogger)
                printToStdoutAndRemoteLogger(
                    'ボリューム {0} のマイグレーションが'
                    'マネジメントサーバで開始されました。'
                    .format(ctx.volumepooltag.volume), ctx.remotelogger)
                migrationstartedtime = datetime.datetime.utcnow()
            else:
                printToStdoutAndRemoteLogger(
                    'APサーバが指定されなかったため、'
                    'マイグレーションの開始の検出をスキップします。',
                    ctx.remotelogger)
        except:
            printToStdoutAndRemoteLogger(
                'ボリューム {0} : MSログの監視中に'
                'エラーが発生しました。'
                .format(ctx.volumepooltag.volume), ctx.remotelogger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
            raise

        try:
            #キューを空にする
            ctx.statusqueue.get_nowait()
            ctx.statusqueue.task_done()
        except Queue.Empty:
            pass
        ctx.statusqueue.put('MIGRATING')


    finally:
        for apserver, tailprocess in ap_tailp_list:
            tailprocess.terminate()
            printToStdoutAndRemoteLogger(
                'APサーバ {0} でのログ監視を終了しました。'
                .format(apserver.name), ctx.remotelogger)

    #+++++ マイグレーション状態監視 ここから +++++
    try:
        ctx.connection.close() #DB切断

        #デタッチされたボリュームの
        #マイグレーション状態監視
        ocom_ctx_type = namedtuple('ocom_ctx_type', ('abortmigrationsessionpipefd',
                                                     'cancelpipefd',
                                                     'connection',
                                                     'estimatedmigrationtime',
                                                     'migrationstartedtime',
                                                     'remotelogger', 'statusqueue',
                                                     'vcenter', 'volumepooltag',))
        ocom_ctx = ocom_ctx_type(abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                                 cancelpipefd=ctx.cancelpipefd, connection=ctx.connection,
                                 estimatedmigrationtime=datetime.timedelta(seconds=ctx.volumepooltag.volume.getEstimatedMigrationTime(ctx.migrationtimepergigabyte)),
                                 migrationstartedtime=migrationstartedtime,
                                 remotelogger=ctx.remotelogger, statusqueue=ctx.statusqueue,
                                 vcenter=ctx.vcenter,
                                 volumepooltag=ctx.volumepooltag)
        ctx.apiclient.waitForAsyncJobCompletion(jobid,
            ctx.pollinginterval, on_check_detachedvol_migration, [ocom_ctx])


        ctx.connection.ping(reconnect=True, attempts=3, delay=5) #DB再接続
        printToStdoutAndRemoteLogger(
            'ボリューム {0} のマイグレーションが'
            '完了しました。'.format(ctx.volumepooltag.volume), ctx.remotelogger)

        #マイグレーション結果確認
        mres = get_vmware_migration_result(ctx.connection,
                                                ctx.volumepooltag.volume.uuid,
                                                destp.id)
        printToStdoutAndRemoteLogger(
            'ボリューム {0}: {1}'
            .format(ctx.volumepooltag.volume, mres[0]), ctx.remotelogger)

        if not mres[1]:
            raise RuntimeError('ボリューム {0}: マイグレーション結果が'
                               '異常です: {1}'.format(ctx.volumepooltag.volume, mres[0]))
    except MigrationCancelledException:
        printToStdoutAndRemoteLogger(
            'ボリューム {0}:'
            'マイグレーションは強制キャンセルされました。'
            .format(ctx.volumepooltag.volume), ctx.remotelogger)
        raise
    except:
        printToStdoutAndRemoteLogger(
            'ボリューム {0} のマイグレーション中に'
            'エラーが発生しました。'
            .format(ctx.volumepooltag.volume), ctx.remotelogger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remotelogger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remotelogger)
        raise

    #スナップショットポリシを復元する
    rsp_ctx_type = namedtuple('rsp_ctx_type', ('apiclient', 'connection',
                                               'policyvolumemap', 'remotelogger'))
    rsp_ctx = rsp_ctx_type(apiclient=ctx.apiclient, connection=ctx.connection,
                           policyvolumemap=policy_volume_map, remotelogger=ctx.remotelogger)
    restore_snapshot_policies(rsp_ctx)

def migrate(ctx):
    """オンラインインスタンス、オフラインインスタンス、またはデタッチドボリュームをマイグレーションします.
    ctx: namedtuple
        マイグレーションコンテキスト.
            abortmigrationsessionpipefd: ファイルディスクリプタ
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションセッションを強制キャンセルする.
                その際vCenterタスクは放置する.
            apiclient: CloudStackApiClient
                APIを実行するためのクライアント.
            apsshclientlist: list<SshClient>
                APサーバでMSログを監視するためのクライアント
            cancelpipefd: ファイルディスクリプタ.
                読み取り用のパイプのファイルディスクリプタ. 読み取り可能になるとマイグレーションの強制キャンセルを試みる.
                vCenterタスクをキャンセルしようと試みる.
            connection: MySQLConnection
                DB接続
            migrationinfo: MigrationInfo
                マイグレーション情報
            migrationtimepergigabyte: int
                マイグレーション見積もり時間 (秒)
            pollinginterval: int
                マイグレーション状態確認間隔 (秒)
            remotelogger: RemoteLogger
                リモートロガー
            remotehistorylogger: RemoteLogger
                リモートヒストリーロガー
            statusqueue: Queue
                マイグレーション状態を更新するためのキュー
            vcenter: namedtuple<user: str, password: str>
                vCenter情報
                    user: str
                        ユーザ名
                    password: str
                        パスワード
    """
    volumepooltagtype = namedtuple('volumepooltagtype', ('volume', 'pooltag',))
    volumepooltaglist = [volumepooltagtype(volume=Volume(ctx.connection, x[0]), pooltag=x[1])
                         for x in ctx.migrationinfo.volumePoolTagTupleList]
    if ctx.migrationinfo.virtualMachineId is None or ctx.migrationinfo.virtualMachineId < 0:
        #デタッチドボリュームをマイグレーションする
        mdv_ctx_type = namedtuple('mdv_ctx_type', ('abortmigrationsessionpipefd', 'apiclient',
                                  'apsshclientlist', 'connection',
                                  'cancelpipefd', 'pollinginterval', 'migrationtimepergigabyte',
                                  'remotelogger', 'remotehistorylogger', 'statusqueue', 'vcenter',
                                  'volumepooltag',))
        mdv_ctx = mdv_ctx_type(abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                               apiclient=ctx.apiclient, apsshclientlist=ctx.apsshclientlist,
                               cancelpipefd=ctx.cancelpipefd,
                               connection=ctx.connection,
                               migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                               pollinginterval=ctx.pollinginterval,
                               remotelogger=ctx.remotelogger,
                               remotehistorylogger=ctx.remotehistorylogger,
                               statusqueue=ctx.statusqueue,
                               vcenter=ctx.vcenter, volumepooltag=volumepooltaglist[0])

        printToStdoutAndRemoteLogger('Migration of volume {0} is started.'
                                     .format(volumepooltaglist[0].volume), ctx.remotehistorylogger)
        try:
            migrate_detached_volume(mdv_ctx)
        except MigrationCancelledException:
            printToStdoutAndRemoteLogger('Migration of volume {0} has been '
                                         'forcely terminated.'
                                         .format(volumepooltaglist[0].volume), ctx.remotehistorylogger)
            raise
        except:
            printToStdoutAndRemoteLogger('Migration of volume {0} failed.'
                                         .format(volumepooltaglist[0].volume),
                                         ctx.remotehistorylogger)
            raise
        printToStdoutAndRemoteLogger('Migration of volume {0} has been completed sucessfully.'
                                     .format(volumepooltaglist[0].volume), ctx.remotehistorylogger)
    else:
        virtualmachine = VirtualMachine(ctx.connection, ctx.migrationinfo.virtualMachineId)
        if virtualmachine.state == 'Running':
            #オンラインインスタンスをマイグレーションする
            hahost = Host(ctx.connection, ctx.migrationinfo.hostId) if ctx.migrationinfo.hostId > 0 else None
            mon_ctx_type = namedtuple('mon_ctx_type', ('apiclient', 'apsshclientlist',
                                      'cancelpipefd', 'connection', 'migrationtimepergigabyte',
                                      'hahost',
                                      'remotelogger', 'remotehistorylogger', 'pollinginterval',
                                      'statusqueue',
                                      'vcenter',
                                      'virtualmachine', 'volumepooltaglist'))
            mon_ctx = mon_ctx_type(apiclient=ctx.apiclient, apsshclientlist=ctx.apsshclientlist,
                                   cancelpipefd=ctx.cancelpipefd,
                                   connection=ctx.connection,
                                   migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                                   hahost=hahost,
                                   remotelogger=ctx.remotelogger,
                                   remotehistorylogger=ctx.remotehistorylogger,
                                   pollinginterval=ctx.pollinginterval,
                                   statusqueue=ctx.statusqueue,
                                   vcenter=ctx.vcenter,
                                   virtualmachine=virtualmachine,
                                   volumepooltaglist=volumepooltaglist)

            printToStdoutAndRemoteLogger('Migration of online instance {0} is started.'
                                         .format(virtualmachine), ctx.remotehistorylogger)

            numfailed = 0
            try:
                numfailed = migrate_online_instance(mon_ctx)
            except MigrationCancelledException:
                printToStdoutAndRemoteLogger('Migration of online instance {0} has been '
                                             'forcely terminated.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
                raise
            except:
                printToStdoutAndRemoteLogger('Migration of online instance {0} failed.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
                raise

            if numfailed == 0:
                printToStdoutAndRemoteLogger('Migration of online instance {0} '
                                             'has been completed sucessfully.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
            elif numfailed == len(volumepooltaglist):
                printToStdoutAndRemoteLogger('Migration of online instance {0} failed.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
            else:
                printToStdoutAndRemoteLogger('Migration of online instance {0} partially completed '
                                             '(There are {1} failed volumes.)'
                                             .format(virtualmachine, numfailed),
                                             ctx.remotehistorylogger)

        elif virtualmachine.state == 'Stopped':
            #オフラインインスタンスをマイグレーションする
            moff_ctx_type = namedtuple('moff_ctx_type', ('abortmigrationsessionpipefd',
                                                         'apiclient', 'cancelpipefd',
                                                         'apsshclientlist', 'connection',
                                                         'migrationtimepergigabyte',
                                                         'remotelogger', 'remotehistorylogger',
                                                         'statusqueue',
                                                         'pollinginterval', 'vcenter',
                                                         'virtualmachine', 'volumepooltaglist',))
            moff_ctx = moff_ctx_type(abortmigrationsessionpipefd=ctx.abortmigrationsessionpipefd,
                                     apiclient=ctx.apiclient,
                                     apsshclientlist=ctx.apsshclientlist,
                                     cancelpipefd=ctx.cancelpipefd,
                                     connection=ctx.connection,
                                     migrationtimepergigabyte=ctx.migrationtimepergigabyte,
                                     remotelogger=ctx.remotelogger,
                                     remotehistorylogger=ctx.remotehistorylogger,
                                     pollinginterval=ctx.pollinginterval,
                                     statusqueue=ctx.statusqueue,
                                     vcenter=ctx.vcenter,
                                     virtualmachine=virtualmachine,
                                     volumepooltaglist=volumepooltaglist)

            printToStdoutAndRemoteLogger('Migration of offline instance {0} is started.'
                                         .format(virtualmachine), ctx.remotehistorylogger)

            numfailed = 0
            try:
                numfailed = migrate_offline_instance(moff_ctx)
            except MigrationCancelledException:
                printToStdoutAndRemoteLogger('Migration of offline instance {0} has been '
                                             'forcely terminated.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
                raise
            except:
                printToStdoutAndRemoteLogger('Migration of offline instance {0} failed.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
                raise

            if numfailed == 0:
                printToStdoutAndRemoteLogger('Migration of offline instance {0} '
                                             'has been completed sucessfully.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
            elif numfailed == len(volumepooltaglist):
                printToStdoutAndRemoteLogger('Migration of offline instance {0} failed.'
                                             .format(virtualmachine), ctx.remotehistorylogger)
            else:
                printToStdoutAndRemoteLogger('Migration of offline instance {0} partially completed '
                                             '(There are {1} failed volumes.)'
                                             .format(virtualmachine, numfailed),
                                             ctx.remotehistorylogger)
        else:
            raise RuntimeError('インスタンス {0}: マイグレーション可能な'
                               '状態ではありません: "{1}"'
                               .format(virtualmachine, virtualmachine.state))

def print_usage():
    """このスクリプトの使用法を表示します.

    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    print('使用法: {0} [オプション...] '
          '[--host <マイグレーション先のホストのID> --virtualmachine '
          '<マイグレーション対象の仮想マシンのID> '
          '[--volume <ボリュームID> --pooltag <プールタグ>]...]...'
          .format(sys.argv[0]))
    print("")
    print("特定のボリュームを特定のストレージプールにマイグレーションします。")
    print("このツールはバッチサーバ上の eosl_migrate_esxi.py から実行されることを想定しており、直接実行されることは想定しておりません。")
    print('')
    print('オプション:')
    print('  --apikey <apikey>                 CloudStack APIを使用するためのAPIキーを指定する')
    print('  --secretkey <secretkey>           CloudStack APIを使用するためのシークレットキーを指定する')
    print('  --polling-interval <number>       マイグレーション状態の確認間隔を<number>秒にする')
    print('  --sessionid  <session id>         マイグレーションセッションのIDを<session id>にする')
    print('  --fsdir|--fsdirectory <directory> セッションファイルや停止フラグを配置するディレクトリを指定する')
    print('  --dbhost <hostname>               <hostname>をCloudStackのデータベースとして使う')
    print('  --dbpassword <password>           CloudStackのデータベースに'
          'ログインするために<password>をパスワードとして使う')
    print('  --vcusername <username>           vCenter Serverにログインするためのユーザ名を<username>にする')
    print('  --vcpassword <password>           vCenter Serverにログインするためのパスワードを<password>にする')
    print('  --migration-time-per-gigabyte <number>    '
          'ディスク1GBあたりのマイグレーション時間を<number>秒としてマイグレーション時間を見積もる')
    print('  --remoteloghost <hostname>        ログの出力先ホストを<hostname>にする')
    print('  --remoteloguser <username>        ログの出力先ホストにログインするためのユーザ名を<username>にする')
    print('  --remotelogkey <keyfile>          ログの出力先ホストにログインするための秘密鍵を<keyfile>にする')
    print('  --remotelogdirectory <path>       ログの出力先ホスト内での、ログの出力先ディレクトリを<path>にする')
    print('')
    print('  --host <id>                       マイグレーション先のホストのIDまたはUUIDを指定する')
    print('  --virtualmachine <id>             マイグレーションする仮想マシンのIDまたはUUIDを指定する')
    print('  --volume <id>                     <id>のIDまたはUUIDのボリュームをマイグレーションする')
    print('  --pooltag <tagname>               直前の--volumeオプションで'
          '指定されたボリュームを<tagname>のタグの'
          'ストレージプールにマイグレーションする')

def main():
    """スクリプトのエントリポイントです."""
    parsed_args = None
    try:
        parsed_args = parse_args()
    except:
        emsg = sys.exc_info()[1]
        print(emsg)
        #raise
        return

    if parsed_args['usage']:
        print_usage()
        return
    if parsed_args['fsdirectory'] is not None and not os.path.exists(parsed_args['fsdirectory']):
        os.makedirs(parsed_args['fsdirectory']) #フラグ・セッション管理ディレクトリを作成する
    log_file_name = None
    remote_log_file_name = None
    remote_history_log_file_name = None
    history_log_file_name = None
    session_file_name = None
    session_normal_cancel_file_name = None
    session_immed_cancel_file_name = None
    remote_logger = None
    remote_history_logger = None
    #history_logger = None
    vcsclient = None #vCenter Server Client
    dir_path = os.path.dirname(os.path.realpath(__file__))
    sshpass = dir_path + '/eosl_local_root/usr/local/bin/sshpass'
    #セッションファイル作成処理
    if parsed_args['sessionid'] is not None:
        session_file_name = parsed_args['fsdirectory'] + '/' + parsed_args['sessionid'] + '.session'
        open(session_file_name, 'a').close() #セッションファイル作成
        session_normal_cancel_file_name = (
            parsed_args['fsdirectory'] + '/' + parsed_args['sessionid'] + '_normal.flg')
        session_immed_cancel_file_name = (
            parsed_args['fsdirectory'] + '/' + parsed_args['sessionid'] + '_immediate.flg')
        if parsed_args['remotelogdirectory'] is not None:
            Utility.remoteMkdir(parsed_args['remotelogdirectory'],
                                parsed_args['remoteloguser'], parsed_args['remoteloghost'],
                                parsed_args['remotelogpassword'], option='-p')
            remote_log_file_name = (
                parsed_args['remotelogdirectory'] + '/' + 'eosl_migrate_esxi.' +
                parsed_args['sessionid'] + '.log')
            remote_logger = (
                RemoteLogger(remote_log_file_name, parsed_args['remoteloghost'],
                             parsed_args['remotelogpassword'],
                             parsed_args['remoteloguser'], sshpass))
            remote_history_log_file_name = (
                parsed_args['remotelogdirectory'] + '/' +
                'eosl_migrate_esxi_rireki.' + parsed_args['sessionid'] + '.log')
            remote_history_logger = RemoteLogger(remote_history_log_file_name,
                                                 parsed_args['remoteloghost'],
                                                 parsed_args['remotelogpassword'],
                                                 parsed_args['remoteloguser'], sshpass)
    logger = Logger(log_file_name)
    if remote_logger is not None:
        sys.stdout.write('リモートログ出力のためのSSH接続を確認中...')
        if remote_logger.checkSsh():
            print('SSH接続を確認しました。')
        else:
            print('SSH接続を確認できませんでした。')
            print('正しくリモートログ出力オプションを指定しているか確認してください。')
            raw_input()
            raise MigrationNoncontinuableError('リモートログ出力先サーバにSSH接続できませんでした。')


    #UNIXドメインソケットを作成する (外部からの操作(強制キャンセルなど)用)
    server = None
    server_sockfilename = parsed_args['fsdirectory'] + '/eosl_esxi_unix_socket-{0}'.format(parsed_args['sessionid'])
    if parsed_args['sessionid'] is not None:
        #セッションファイルにUNIXドメインソケットのパスを出力する (キャンセルスクリプトが見ることができるようにするため。)
        sessionfile = open(session_file_name, 'a')
        sessionfile.write(server_sockfilename)
        sessionfile.close()
        try:
            server = socket.socket(socket.AF_UNIX, socket.SOCK_SEQPACKET)
            server.bind(server_sockfilename)
        except Exception:
            printToStdoutAndRemoteLogger(
                'UNIXドメインソケットの作成に失敗しました: \"{0}\"'
                ' マイグレーションセッションを中止します。'.format(sys.exc_info()[1]),
                remote_logger)
            return
    (handler_thread, wpipe_fd, cancel_pipe_fd, im_cancel_status_queue,
     im_cancel_specify_task_queue, ams_pipe_fd) = start_api_handler_thread(server) #APIハンドラ
    wpipe = os.fdopen(wpipe_fd, 'w')

    #マイグレーションセッション開始ログ
    logger.writeln('マイグレーションセッション {0} を開始しました。'
                   .format('' if parsed_args['sessionid'] is None else parsed_args['sessionid']))
    if remote_logger is not None:
        remote_logger.writeln('マイグレーションセッション {0} を'
                              '開始しました。'
                              .format('' if parsed_args['sessionid'] is None
                                      else parsed_args['sessionid']))
    if remote_history_logger is not None:
        remote_history_logger.writeln('***** Migration session started: {0} *****'
                                      .format('<unspecified>' if parsed_args['sessionid'] is None
                                              else parsed_args['sessionid']))

    #発見したAPサーバをログに書き込む
    ap_servers = []
    try:
        connection = mysql.connector.connect(user='root', host=parsed_args['db'].host,
                                             password=parsed_args['db'].password,
                                             database='cloud', charset='utf8')
        ap_servers = MsHost.getAllUpManagementHosts(connection)
        numap = len(ap_servers)
        if numap == 0:
            printToStdoutAndRemoteLogger('エラー: Up状態のAPサーバが存在しません。セッションを終了します。', remote_logger)
            return
        printToStdoutAndRemoteLogger('{0}個のUp状態のAPサーバが見つかりました:'.format(numap), remote_logger)
        for i, aps in enumerate(ap_servers):
            printToStdoutAndRemoteLogger('[{0}] {1}'.format(i, aps.name), remote_logger)
        printToStdoutAndRemoteLogger('', remote_logger)
    except:
        printToStdoutAndRemoteLogger(
            'エラー: DBからAPサーバ情報を取得するときに'
            'エラーが発生しました。セッションを終了します。', remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
        return
    finally:
        connection.close()

    #APサーバとsshクライアントのタプルリスト
    ap_sshc_list = []
    for i, aps in enumerate(ap_servers):
        sshc = SshClient(parsed_args['ap_info'].user, aps.serviceIp,
                         parsed_args['ap_info'].password, sshpass,
                         options=[('NumberOfPasswordPrompts', 1,), ('ConnectTimeout', 10,),
                                  ('StrictHostKeyChecking', 'no',),
                                  ('UserKnownHostsFile', '/dev/null',)])
        ap_sshc_list.append((aps, sshc, ))

    #マイグレーションセッション開始
    try:
        connection = mysql.connector.connect(user='root',
                                             host=parsed_args['db'].host,
                                             password=parsed_args['db'].password,
                                             database='cloud', charset='utf8')
        apiclient = CloudStackApiClient('http://localhost:8080/client/api?',
                             parsed_args['apikey'], parsed_args['secretkey'])

        for migration_info in parsed_args['migrationinfolist']:
            #ソフト(通常)キャンセル
            if (session_normal_cancel_file_name is not None
                    and os.path.exists(session_normal_cancel_file_name)):
                os.remove(session_normal_cancel_file_name)
                printToStdoutAndRemoteLogger(
                    '通常キャンセルフラグファイルを検出したため、'
                    'マイグレーションセッションを中止します。', remote_logger)
                break

            migrate_context_type = namedtuple('migrate_context_type',
                                              ('abortmigrationsessionpipefd', 'apiclient',
                                               'apsshclientlist',
                                               'cancelpipefd', 'connection', 'migrationinfo',
                                               'migrationtimepergigabyte',
                                               'pollinginterval', 'remotelogger', 'statusqueue',
                                               'remotehistorylogger', 'vcenter'))
            migrate_context = migrate_context_type(abortmigrationsessionpipefd=ams_pipe_fd,
                                                   apiclient=apiclient,
                                                   apsshclientlist=ap_sshc_list,
                                                   cancelpipefd=cancel_pipe_fd,
                                                   connection=connection,
                                                   migrationinfo=migration_info,
                                                   migrationtimepergigabyte=
                                                   parsed_args['migrationtimepergigabyte'],
                                                   pollinginterval=parsed_args['pollinginterval'],
                                                   remotelogger=remote_logger,
                                                   remotehistorylogger=remote_history_logger,
                                                   statusqueue=im_cancel_status_queue,
                                                   vcenter=parsed_args['vcenter'])
            try:
                #インスタンス (またはボリューム) をマイグレーションする
                migrate(migrate_context)
            except MigrationCancelledException:
                printToStdoutAndRemoteLogger('マイグレーションセッションは強制キャンセルされました。', remote_logger)
                break
            except:
                printToStdoutAndRemoteLogger(
                    'マイグレーション中に'
                    '予期していないエラーが発生しました。', remote_logger)
                printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
                printToStdoutAndRemoteLogger('<デバッグ情報> migrate_context: {0}'
                                             .format(migrate_context), remote_logger)
                printToStdoutAndRemoteLogger('このマイグレーションをスキップします。', remote_logger)
                continue

    except MigrationNoncontinuableError:
        printToStdoutAndRemoteLogger(
            'マイグレーションセッションを中止します: \"{0}\"'
            .format(sys.exc_info()[1]), remote_logger)
    except Exception:
        printToStdoutAndRemoteLogger('マイグレーションセッションの実行中に予期していないエラーが発生しました。', remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
        printToStdoutAndRemoteLogger('マイグレーションセッションを中止します。', remote_logger)
    finally:
        if connection is not None:
            connection.close()
        #ハンドラースレッド終了
        wpipe.write('hoge') #書き込むものは適当でよい。
        wpipe.close()
        handler_thread.join()

        if server is not None:
            try:
                server.close()
            except Exception:
                printToStdoutAndRemoteLogger(
                    'UNIXドメインソケットのclose()に失敗しました: {0}'
                    .format(sys.exc_info()[1]), remote_logger)
        if server_sockfilename is not None:
            try:
                os.remove(server_sockfilename)
            except Exception:
                printToStdoutAndRemoteLogger(
                    'UNIXドメインソケットの削除に失敗しました: {0}'
                    .format(sys.exc_info()[1]), remote_logger)
        #セッションファイル削除処理
        if session_file_name is not None and os.path.exists(session_file_name):
            os.remove(session_file_name)
        logger.writeln('マイグレーションセッション {0} を終了します。'
                       .format(None if parsed_args['sessionid'] is None
                               else parsed_args['sessionid']))
        if remote_logger is not None:
            remote_logger.writeln('マイグレーションセッション {0} を終了します。'
                                  .format(None if parsed_args['sessionid'] is None
                                          else parsed_args['sessionid']))
        if remote_history_logger is not None:
            remote_history_logger.writeln('***** Migration session finished: {0} *****'
                                          .format('<unspecified>'
                                                  if parsed_args['sessionid'] is None
                                                  else parsed_args['sessionid']))
        logger.close()
        #if history_logger is not None:
            #history_logger.close()
        if remote_logger is not None:
            print('リモートログの出力が完了するのを待機しています...')
            remote_logger.join()
        if remote_history_logger is not None:
            remote_history_logger.join()

main()
