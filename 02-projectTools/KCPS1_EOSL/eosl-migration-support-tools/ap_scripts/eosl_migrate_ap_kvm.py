#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""KVMボリュームのマイグレーションを行うスクリプトです."""
from __future__ import print_function
import sys
import traceback
import os
import re
import datetime
from collections import namedtuple
import mysql.connector

from eosl_migrate_ap_common import printToStdoutAndRemoteLogger
from eosl_migrate_ap_common import MigrationCancelledException
from CloudStackApiClient import CloudStackApiClient
from RemoteLogger import RemoteLogger
from ap_batch_common import Utility
from ap_batch_common.Host import Host
from ap_batch_common.SshClient import SshClient
from ap_batch_common.StoragePool import StoragePool
from ap_batch_common.Volume import Volume
from ap_batch_common.SnapshotPolicy import SnapshotPolicy
from ap_batch_common.VirtualMachine import VirtualMachine
#import EoslUtil

def get_destination_storage_pools(conn, volume_pooltag_tuple_list):
    """
    ボリュームのマイグレーション先ストレージプール(プライマリストレージ)を取得します.

    ストレージプールは、現在ボリュームが存在するストレージプールと同じクラスタ内のストレージプールが返されます.

    Parameters
    ----------
    c: MySQLConnection
        mysql.connector.connect()の戻り値.
    volume_pooltag_tuple_list: list [tuple]
        マイグレーションするボリュームのUUIDと、マイグレーション先ストレージプールのタグのタプルのリスト.

    Returns
    -------
    list
        ボリュームと、マイグレーション先ストレージプールのnamedtuple(volume, destpool)のリスト
    """
    #ストレージプールと、ボリューム累積サイズ (先のボリュームのマイグレーションを加味するサイズ)
    pool_accumsize_type = namedtuple('pool_accumsize_type', ('pool', 'accumsize',))
    dest_pools = []

    for vpt in volume_pooltag_tuple_list:
        volume = Volume.createInstanceByUuid(conn, vpt[0])
        sourcep = StoragePool(conn, volume.poolId)
        if sourcep.tag == vpt[1]:
            raise RuntimeError('マイグレーション元ストレージプールのタグと'
                               'マイグレーション先ストレージプールのタグが同じです: {0}'.format(sourcep.tag))

        pools = [x for x in StoragePool.getPoolsByTag(conn, vpt[1])
                 if x.clusterId == sourcep.clusterId]

        for pool in pools:
            if pool.id not in [x.pool.id for x in dest_pools]:
                print("ストレージプール {0} をdest_poolsに追加します".format(pool))
                dest_pools.append(pool_accumsize_type(pool=pool, accumsize=0))

    volume_destpool_type = namedtuple('volume_destpool_type', ('volume', 'destpool',))
    ret = []
    for vpt in volume_pooltag_tuple_list:
        volume = Volume.createInstanceByUuid(conn, vpt[0])
        sourcep = StoragePool(conn, volume.poolId)
        candidate_pool_indexes = sorted([i for i, x in enumerate(dest_pools)
                                         if x.pool.tag == vpt[1]],
                                        key=lambda x: dest_pools[x].pool.getUsageWithExtraSize(dest_pools[x].accumsize))

        print('accumsize:\n' + ''.join([dest_pools[i].pool.name + ':' +
              str(dest_pools[i].pool.getUsageWithExtraSize(dest_pools[i].accumsize)) +
              "\n" for i in candidate_pool_indexes]))

        found = False
        for candp_index in candidate_pool_indexes:
            if dest_pools[candp_index].pool.hasEnoughSpace(
                    volume.size + dest_pools[candp_index].accumsize):
                ret.append(volume_destpool_type(
                    volume=volume, destpool=dest_pools[candp_index].pool))
                dest_pools[candp_index] = pool_accumsize_type(
                    pool=dest_pools[candp_index].pool,
                    accumsize=dest_pools[candp_index].accumsize + volume.size)
                found = True
                break

        if not found:
            raise RuntimeError('マイグレーション先として適切なストレージプール (プライマリストレージ) が見つかりませんでした。'
                               'ストレージの容量が不足していないか確認してください。')

    return ret

def get_kvm_migration_result_string(connection, minfo, instance, first_host_id):
    """マイグレーション結果を確認し、結果を文字列として取得します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の戻り値.
    minfo: list
        マイグレーションしたボリュームとマイグレーション先ストレージプールのnamedtuple(volume, destpool)のlist
    instance: VirtualMachine
        マイグレーションした仮想マシン
    first_host_id: str
        はじめにインスタンスが起動していたホストのID

    Returns
    -------
    str, bool
        マイグレーション結果についての文字列と、マイグレーションの成否
    """
    instance = VirtualMachine(connection, instance.id)
    if instance.state != 'Running':
        return 'マイグレーション結果確認でエラーと判定されました。インスタンスの状態が正常ではありません。', False
    if instance.hostId != first_host_id:
        return ('マイグレーション結果確認でエラーと判定されました。'
                '指定されたマイグレーション先ホストにマイグレーションされていません。'
                '(インスタンスが起動中のホスト:{0}, マイグレーション先ホスト: {1}'
                .format(str(instance.hostId), str(minfo.hostId)), False)

    vol_list = []
    for vpnt in minfo:
        vol_list.append(Volume.createInstanceByUuid(connection, vpnt.volume.uuid))

    non_ready_vol_list = [x for x in vol_list if x.state != 'Ready']
    if len(non_ready_vol_list) > 0:
        return ('マイグレーション結果確認でエラーと判定されました。ディスク' +
                ', '.join([str(x.id) for x in non_ready_vol_list]) + 'の状態がReadyではありません。', False)

    not_migrated_vol_list = []
    for i, vol in enumerate(vol_list):
        if vol.poolId != minfo[i].destpool.id:
            print("追加: {0}, {1}".format(vol.poolId, minfo[i].destpool.id))
            not_migrated_vol_list.append(vol)
    if len(not_migrated_vol_list) > 0:
        return ('マイグレーション結果確認でエラーと判定されました。ボリューム' +
                ', '.join([str(x) for x in not_migrated_vol_list]) +
                'が指定されたマイグレーション先ストレージにマイグレーションされていません。', False)

    return 'マイグレーションは正常に完了しました。', True

def parse_args():
    """コマンドライン引数をパースし、dictionaryとして返します.

    Parameters
    ----------
    None

    Returns
    -------
    dictionary
        'kvmhost': namedtuple
            KVMホストログイン情報
                user: str
                    ユーザ名
                password: str
                    パスワード
        'usage': bool
            使用方法を表示するかどうか
        'apikey': str
            APIキー
        'secretkey': str
            シークレットキー
        'pollinginterval': int
            マイグレーション状態確認間隔
        'sessionid': str
            セッションID
        'fsdirectory': str
            フラグ/セッションファイルディレクトリ名
        'sshpasscommandpath': str
            sshpassコマンドのパス
        'db': namedtuple
            データベース情報
                host: str
                    DBのホスト名
                password: str
                    DBのパスワード
        'migratespeed': int
            設定するマイグレーション速度制限(HAホスト→元ホスト) (MiB/s)
        'migratespeedtoha': int
            設定するマイグレーション速度制限(元ホスト→HAホスト) (MiB/s)
        'migrationtimepergigabyte': int
            1GBあたりのマイグレーション時間(秒)
        'remotelog': namedtuple
            リモートログ出力情報
                user: str
                    ユーザ名
                host: str
                    ログ出力先ホスト名
                password: str
                    ログ出力先パスワード
                directory: str
                    ログ出力先ディレクトリ名
        'migrationinfolist': list<MigrationInfo>
            マイグレーション情報(MigrationInfo)リスト
    """
    ret = {}
    kvmhost = namedtuple('kvmhost', ('user', 'password',))
    dbtype = namedtuple('db', ('host', 'password',))
    remotelog = namedtuple('remotelog', ('user', 'host', 'password', 'directory'))
    migrationinfo = namedtuple('migrationinfo',
                               ('hostid', 'virtualmachineid', 'volumepooltagtuplelist',))
    ret['kvmhost'] = kvmhost(user=None, password=None)
    ret['usage'] = False
    ret['apikey'] = None
    ret['pollinginterval'] = 300
    ret['sessionid'] = None
    ret['fsdirectory'] = None
    ret['sshpasscommandpath'] = None
    ret['db'] = dbtype(host=None, password=None)
    ret['migratespeed'] = None
    ret['migratespeedtoha'] = None
    ret['migrationtimepergigabyte'] = -1
    ret['remotelog'] = remotelog(user=None, host=None, password=None, directory=None)
    ret['migrationinfolist'] = []
    if len(sys.argv) < 2:
        ret['usage'] = True
        return
    i = 1
    while True:
        if i == len(sys.argv):
            break
        if sys.argv[i] == '--host':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--host オプションの後にホストIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                ret['migrationinfolist'].append(migrationinfo
                                                (hostid=sys.argv[i],
                                                 virtualmachineid=None, volumepooltagtuplelist=[]))
            else:
                if ret['migrationinfolist'][-1].hostid is not None:
                    if ret['migrationinfolist'][-1].virtualmachineid is not None:
                        ret['migrationinfolist'].append(migrationinfo
                                                        (hostid=sys.argv[i],
                                                         virtualmachineid=None,
                                                         volumepooltagtuplelist=[]))
                    else:
                        raise RuntimeError('仮想マシン ID={0} のマイグレーション対象仮想マシンが指定されていません。'
                                           .format(ret['migrationinfolist'][-1].hostid))
                else:
                    ret['migrationinfolist'][-1] = (
                        migrationinfo(hostid=sys.argv[i],
                                      virtualmachineid=(
                                          ret['migrationinfolist'][-1].virtualmachineid
                                      ),
                                      volumepooltagtuplelist=(
                                          ret['migrationinfolist'][-1].volumepooltagtuplelist
                                      )))
        elif sys.argv[i] == '--virtualmachine':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--virtualmachine オプションの後に仮想マシンIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                ret['migrationinfolist'].append((
                    migrationinfo(hostid=None,
                                  virtualmachineid=sys.argv[i], volumepooltagtuplelist=[])))
            else:
                if ret['migrationinfolist'][-1].virtualmachineid is not None:
                    if ret['migrationinfolist'][-1].hostid is not None:
                        ret['migrationinfolist'].append((
                            migrationinfo(hostid=None,
                                          virtualmachineid=sys.argv[i], volumepooltagtuplelist=[])))
                    else:
                        raise RuntimeError('ホスト ID{0} にマイグレーションする仮想マシンが指定されていません。'
                                           .format(ret['migrationinfolist'][-1].hostid))
                else:
                    ret['migrationinfolist'][-1] = (
                        migrationinfo(hostid=ret['migrationinfolist'][-1].hostid,
                                      virtualmachineid=sys.argv[i],
                                      volumepooltagtuplelist=ret['migrationinfolist'][-1]
                                      .volumepooltagtuplelist))
        elif sys.argv[i] == '--volume':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--volume オプションの後にボリュームIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                raise RuntimeError('--volume オプションを使用する前に、 --host または '
                                   '--virtualmachine オプションを使用してホストまたは仮想マシンを指定してください。')
            else:
                if len(ret['migrationinfolist'][-1].volumepooltagtuplelist) == 0:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist.append((sys.argv[i], None))
                elif ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][0] is None:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist[-1] = (
                        (sys.argv[i], ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][1]))
                elif ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][1] is None:
                    raise RuntimeError('ボリューム ID={0} に対するマイグレーション先ストレージプールを --pool オプションで指定してください。'
                                       .format(ret['migrationinfolist'][-1]
                                               .volumepooltagtuplelist[-1][0]))
                else:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist.append((sys.argv[i], None))
        elif sys.argv[i] == '--pooltag':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--pooltag オプションの後にストレージプールIDが指定されていません。')
            if len(ret['migrationinfolist']) == 0:
                raise RuntimeError('--pooltag オプションを使用する前に、 --host '
                                   'または --virtualmachine オプションを使用してホストまたは仮想マシンを指定してください。')
            else:
                if len(ret['migrationinfolist'][-1].volumepooltagtuplelist) == 0:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist.append((None, sys.argv[i]))
                elif ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][1] is None:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist[-1] = (
                        ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][0], sys.argv[i])
                elif ret['migrationinfolist'][-1].volumepooltagtuplelist[-1][0] is None:
                    raise RuntimeError('ストレージプール ID={0} に対するマイグレーション対象ボリュームを '
                                       '--volume オプションで指定してください。'
                                       .format(ret['migrationinfolist'][-1]
                                               .volumepooltagtuplelist[-1][1]))
                else:
                    ret['migrationinfolist'][-1].volumepooltagtuplelist.append((None, sys.argv[i]))
        elif sys.argv[i] == '--apikey':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--apikey オプションの後にAPIキーが指定されていません。')
            ret['apikey'] = sys.argv[i]
        elif sys.argv[i] == '--secretkey':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--secretkey オプションの後にシークレットキーが指定されていません')
            ret['secretkey'] = sys.argv[i]
        elif sys.argv[i] == '--polling-interval':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--polling-interval オプションの後にポーリング間隔が指定されていません。')
            ret['pollinginterval'] = int(sys.argv[i])
        elif sys.argv[i] == '--sessionid':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--sessionid オプションの後にセッションIDが指定されていません。')
            ret['sessionid'] = sys.argv[i]
        elif sys.argv[i] == '--fsdir' or sys.argv[i] == '--fsdirectory':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--fsdir|--fsdirectory オプションの後にディレクトリ名が指定されていません。')
            ret['fsdirectory'] = sys.argv[i]
        elif sys.argv[i] == '--sshpass' or sys.argv[i] == '--sshpasscommandpath':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--sshpass|--sshpasscommandpath オプションの後にsshコマンドへのパスが指定されていません。')
            ret['sshpasscommandpath'] = sys.argv[i]
        elif sys.argv[i] == '--kvmpassword' or sys.argv[i] == '--kvmhostpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--kvmpassword|--kvmhostpassword オプションの後に'
                                   'KVMホストへログインするためのパスワードが指定されていません。')
            ret['kvmhostpassword'] = sys.argv[i]
        elif sys.argv[i] == '--dbhost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--dbhost オプションの後にデータベースホスト名が指定されていません。')
            ret['db'] = dbtype(host=sys.argv[i], password=ret['db'].password)
        elif sys.argv[i] == '--dbpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--dbpassword オプションの後にデータベースパスワードが指定されていません。')
            ret['db'] = dbtype(host=ret['db'].host, password=sys.argv[i])
        elif (sys.argv[i] == '--migratespeed' or sys.argv[i] == '--migrate-speed' or
              sys.argv[i] == '--migrationspeed' or sys.argv[i] == '--migration-speed'):
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--migratespeedオプションの後にマイグレーション速度 (MiB/s)が指定されていません。')
            ret['migratespeed'] = int(sys.argv[i])
        elif (sys.argv[i] == '--migratespeedtoha' or sys.argv[i] == '--migrate-speedtoha' or
              sys.argv[i] == '--migrationspeedtoha' or sys.argv[i] == '--migration-speedtoha'):
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--migratespeedtohaオプションの後にマイグレーション速度 (MiB/s)が指定されていません。')
            ret['migratespeedtoha'] = int(sys.argv[i])
        elif sys.argv[i] == '--migration-time-per-gigabyte':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--migration-time-per-gigabyte オプションの後に秒数が設定されていません。')
            ret['migrationtimepergigabyte'] = int(sys.argv[i])
        elif sys.argv[i] == '--remoteloghost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remoteloghost オプションの後にリモートログ出力先ホストが指定されていません。')
            ret['remotelog'] = remotelog(host=sys.argv[i],
                                         user=ret['remotelog'].user,
                                         directory=ret['remotelog'].directory,
                                         password=ret['remotelog'].password)
        elif sys.argv[i] == '--remoteloguser':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remoteloguser オプションの後にリモートログ出力先ホストにログインするためのユーザ名が指定されていません。')
            ret['remotelog'] = remotelog(user=sys.argv[i],
                                         host=ret['remotelog'].host,
                                         directory=ret['remotelog'].directory,
                                         password=ret['remotelog'].password)
        elif sys.argv[i] == '--remotelogpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remotelogpassword オプションの後に'
                                   'リモートログ出力先ホストにログインするためのパスワードが指定されていません。')
            ret['remotelog'] = remotelog(password=sys.argv[i],
                                         host=ret['remotelog'].host,
                                         user=ret['remotelog'].user,
                                         directory=ret['remotelog'].directory)
        elif sys.argv[i] == '--remotelogdirectory':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('--remotelogdirectory オプションの後にリモートログ出力先パスが指定されていません。')
            ret['remotelog'] = remotelog(directory=sys.argv[i],
                                         host=ret['remotelog'].host,
                                         user=ret['remotelog'].user,
                                         password=ret['remotelog'].password)
        else:
            raise RuntimeError('不明なオプション \'{0}\''.format(sys.argv[i]))
        i += 1
    for minfo in ret['migrationinfolist']:
        if minfo.hostid is None:
            raise RuntimeError('マイグレーション対象仮想マシン ID={0} に対する'
                               'マイグレーション先ホストを指定してください'.format(minfo.virtualmachineid))
        if minfo.virtualmachineid is None:
            raise RuntimeError('マイグレーション先ホスト ID={0} に対する'
                               'マイグレーション対象仮想マシンを指定してください。'.format(minfo.hostid))
    return ret

def on_check_migration_status(ctx):
    """マイグレーション状態を確認するときに呼び出すメソッドです.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            instance: VirtualMachine
                マイグレーション中のVirtualMachine
            remote_logger: RemoteLogger
                リモートロガー.
            immediate_cancel_file_name: str
                強制キャンセルファイル名
            sshclient: SshClient
                KVMホストにログインするためのSshClient
            migration_started_time: datetime
                マイグレーション開始日時 (UTC)
            estimated_migration_time: timedelta
                見積もりマイグレーション時間
    """
    printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション中...'.format(ctx.instance), ctx.remote_logger)

    #+++++強制キャンセル ここから+++++
    if (ctx.immediate_cancel_file_name is not None
            and os.path.exists(ctx.immediate_cancel_file_name)):
        try:
            printToStdoutAndRemoteLogger('インスタンス {0}: マイグレーションの強制キャンセルが要求されました。'
                                         .format(ctx.instance), ctx.remote_logger)
            printToStdoutAndRemoteLogger('インスタンス {0}: domjobの実行状態を取得中...'
                                         .format(ctx.instance), ctx.remote_logger)
            domjobinfo = (ctx.sshclient.run_simple(['LANG=en_US.utf-8',
                                                    'virsh',
                                                    'domjobinfo',
                                                    ctx.instance.instanceName]))
            regexp = re.compile(r'^Job type:\s+Unbounded')
            if regexp.search(domjobinfo):
                try:
                    os.remove(ctx.immediate_cancel_file_name)
                    ctx.sshclient.run_simple(['virsh', 'domjobabort', ctx.instance.instanceName]) #ジョブキャンセル
                except:
                    printToStdoutAndRemoteLogger('マイグレーションの強制キャンセル中にエラーが発生しました。', ctx.remote_logger)
                    printToStdoutAndRemoteLogger('スタックトレース:', ctx.remote_logger)
                    printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remote_logger)
                finally:
                    raise MigrationCancelledException('マイグレーションは強制キャンセルされました。')
            else:
                printToStdoutAndRemoteLogger('インスタンス {0}: マイグレーションはまだキャンセルできる状態ではありません。'
                                             .format(ctx.instance), ctx.remote_logger)
        except:
            printToStdoutAndRemoteLogger('マイグレーションの強制キャンセル中にエラーが発生しました。', ctx.remote_logger)
            printToStdoutAndRemoteLogger('スタックトレース:', ctx.remote_logger)
            printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remote_logger)
            raise MigrationCancelledException('マイグレーションは強制キャンセルされました。')

    mtime = datetime.datetime.utcnow() - ctx.migration_started_time
    if mtime > ctx.estimated_migration_time:
        overtime = mtime - ctx.estimated_migration_time
        printToStdoutAndRemoteLogger('インスタンス {0}: 警告: マイグレーション予測時間 {1} を {2} 過ぎています。'
                                     .format(ctx.instance, ctx.estimated_migration_time, overtime)
                                     , ctx.remote_logger)

    #-----強制キャンセル ここまで-----

def rollback_instance(ctx):
    """インスタンスをロールバックします.

    インスタンスをHAホストに移行後、HAホストから本来のホストに戻す際に
    マイグレーションが強制停止されてしまう場合があります。
    この場合にインスタンスを本来のホストに戻すためのメソッドです.

    Parameters
    ----------
    ctx: namedtuple
        ロールバックコンテキスト
            apiclient: CloudStackApiClient
                APIクライアント.
            connection: MySQLConnection
                mysql.connector.connect()の戻り値.
            first_host_id: 最初のホストのID
                最初のホストのID.
            instance: VirtualMachine
                ロールバックするインスタンス.
            remote_logger: RemoteLogger
                リモートロガー.

    Returns
    -------
    None
    """
    printToStdoutAndRemoteLogger('インスタンス {0}: インスタンスを元のホスト {1} に戻します。'
                                 .format(ctx.instance, ctx.first_host_id),
                                 ctx.remote_logger)
    printToStdoutAndRemoteLogger('インスタンス {0}: インスタンスがRunning状態になるまで待機中...'
                                 .format(ctx.instance), ctx.remote_logger)
    VirtualMachine.waitForStateByUuid(ctx.connection, ctx.instance.uuid, 'Running')
    ctx.apiclient.migrateVirtualMachine(ctx.instance.id, ctx.first_host_id)
    printToStdoutAndRemoteLogger('インスタンス {0}: インスタンスを元のホスト {1} に戻しました。'
                                 .format(ctx.instance, ctx.first_host_id),
                                 ctx.remote_logger)

def rollback_instance_with_exc(ctx, exc):
    """インスタンスをロールバックし、excをraiseします.

    rollback_instance(ctx)を呼び出した後、例外excをraiseします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト. rollback_instance(ctx)の引数を参照.

    Returns
    -------
    """
    try:
        printToStdoutAndRemoteLogger('インスタンス {0}: ロールバック処理を行います。'
                                     .format(ctx.instance), ctx.remote_logger)
        rollback_instance(ctx)
        printToStdoutAndRemoteLogger('インスタンス {0}: ロールバック処理を完了しました。'
                                     .format(ctx.instance), ctx.remote_logger)
    except:
        printToStdoutAndRemoteLogger('インスタンス {0}: ロールバック処理に失敗しました。'
                                     .format(ctx.instance), ctx.remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remote_logger)
    finally:
        if exc is not None:
            raise exc

def set_migrate_speed(ctx):
    """インスタンスのマイグレーション速度を変更します.

    Parameters
    ----------
    ctx: namedtuple
        コンテキスト
            instance: VirtualMachine
                マイグレーション対象のインスタンス
            migratespeed: int
                マイグレーション速度 (MiB/s)
            remote_logger: RemoteLogger
                リモートロガー
            sshclient: SshClient
                KVMホストでコマンドを実行するためのSshClient
    """
    #virsh migrate-setspeed
    printToStdoutAndRemoteLogger('インスタンス {0}: マイグレーション速度を制限中...'
                                 .format(str(ctx.instance)), ctx.remote_logger)
    ctx.sshclient.run_simple(['virsh', 'migrate-setspeed', ctx.instance.instanceName,
                          str(ctx.migratespeed) if ctx.migratespeed is not None else '100'])
    printToStdoutAndRemoteLogger('インスタンス {0}: マイグレーション速度を制限しました。'
                                 .format(str(ctx.instance)), ctx.remote_logger)

def migrate_instance(ctx):
    """インスタンスを1つマイグレーションします.

    Parameters
    ----------
    ctx: namedtuple
        マイグレーションコンテキスト.
            apiclient: CloudStackApiClient
                CloudStackApiClient
            connection: MySQLConnection
                mysql.connector.connect()の戻り値
            kvmhostpassword: str
                KVMホストのパスワード
            migration_info: namedtuple
                マイグレーション情報
                    hostid: str
                        ホストID
                    virtualmachineid: str
                        仮想マシンID
                    volumepooltagtuplelist: list<tuple<Volume, str>>
                        ボリュームとマイグレーション先ストレージプールのタグの組み合わせのリスト
            migratespeed: int
                マイグレーション速度(HAホスト→元ホスト) (MiB/s)
            migratespeedtoha: int
                マイグレーション速度(元ホスト→HAホスト) (MiB/s)
            migrationtimepergigabyte: int
                1GBあたりのマイグレーション時間(秒)
            pollinginterval: int
                マイグレーション状態確認間隔
            remote_logger: RemoteLogger
                リモートロガー
            remote_history_logger: RemoteLogger
                リモートヒストリーロガー
            session_immed_cancel_file_name: str
                強制キャンセルフラグファイル名
            session_normal_cancel_file_name: str
                通常キャンセルフラグファイル名
            sshpass: str
                sshpassコマンドへのパス
    """
    ctx.connection.ping(reconnect=True, attempts=3, delay=10)
    print(ctx.migration_info)

    #インスタンス情報をDBから取得する
    instance = VirtualMachine(ctx.connection, ctx.migration_info.virtualmachineid)

    if instance.state != 'Running':
        printToStdoutAndRemoteLogger('インスタンス {0} はRunning状態ではありません: '
                                     '"{1}" このインスタンスをスキップします。'
                                     .format(instance, instance.state),
                                     ctx.remote_logger)
        return

    first_host_id = instance.hostId

    #マイグレーション先ストレージプールの決定
    printToStdoutAndRemoteLogger('インスタンス {0} にアタッチされているボリュームのマイグレーション先を決定します。'
                                 .format(str(instance)), ctx.remote_logger)
    volume_destpool_namedtuple_list = (
        get_destination_storage_pools(ctx.connection, ctx.migration_info.volumepooltagtuplelist)
        )
    volume_destpool_tuple_list = (
        [(x.volume.uuid, x.destpool.uuid,)
         for x in volume_destpool_namedtuple_list]
        )
    for i, vdn in enumerate(volume_destpool_namedtuple_list):
        printToStdoutAndRemoteLogger('[{0}] ボリューム {1} はストレージプール {2} '
                                     'にマイグレーションします。'
                                     .format(i, vdn.volume, vdn.destpool),
                                     ctx.remote_logger)

    #スナップショットポリシを取得する
    printToStdoutAndRemoteLogger('スナップショットポリシを取得します。',
                                 ctx.remote_logger)
    policy_volume_map = []
    policy_volume_type = namedtuple('policy_volume_type', ('policy', 'volume'))
    for i, vdn in enumerate(volume_destpool_namedtuple_list):
        printToStdoutAndRemoteLogger('[{0}] ボリューム {1} の'
                                     'スナップショットポリシ: '
                                     .format(i, vdn.volume), ctx.remote_logger)
        policies_to_add = SnapshotPolicy.create_instances_from_volume_id(ctx.connection, vdn.volume.id)
        for policy in policies_to_add:
            policy_volume_map.append(policy_volume_type(policy=policy, volume=vdn.volume))
        if len(policies_to_add) == 0:
            printToStdoutAndRemoteLogger(' (なし)', ctx.remote_logger)
        else:
            for j, pta in enumerate(policies_to_add):
                printToStdoutAndRemoteLogger(' [{0}] {1}'.format(j, pta), ctx.remote_logger)

    #virsh migrate-setspeed
    sms_context_type = namedtuple('sms_context_type',
                                  ('instance', 'migratespeed', 'remote_logger', 'sshclient'))
    current_host_ip = Host(ctx.connection, instance.hostId).privateIpAddress

    sshclient = SshClient('root', current_host_ip, ctx.kvmhostpassword,
                          ctx.sshpass, options=[('NumberOfPasswordPrompts', 1,),
                                                ('UserKnownHostsFile', '/dev/null',),
                                                ('StrictHostKeyChecking', 'no')])
    sms_ctx = sms_context_type(instance=instance, migratespeed=ctx.migratespeedtoha,
                               remote_logger=ctx.remote_logger, sshclient=sshclient)
    set_migrate_speed(sms_ctx)

    #API実行
    printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)を開始します。'
                                 .format(str(instance)), ctx.remote_logger)

    try:
        ctx.apiclient.migrateVirtualMachine(instance.id, ctx.migration_info.hostid)
    except:
        printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)中にエラーが発生しました。'
                                     .format(instance), ctx.remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remote_logger)
        raise

    #print('ジョブIDは {0} です。'.format(jobid))
    #if ctx.remote_logger is not None:
    #    ctx.remote_logger.writeln('このマイグレーションのジョブIDは {0} です。'.format(jobid))
    printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)が完了しました。'
                                 .format(str(instance)), ctx.remote_logger)

    #マイグレーションをキャンセルするホストのIPアドレス
    cancel_host_ip = Host(ctx.connection, ctx.migration_info.hostid).privateIpAddress

    sshclient = SshClient('root', cancel_host_ip, ctx.kvmhostpassword,
                          ctx.sshpass, options=[('NumberOfPasswordPrompts', 1,),
                                                ('UserKnownHostsFile', '/dev/null',),
                                                ('StrictHostKeyChecking', 'no')])

    #on_check_migration_status()に渡す引数の型
    on_check_migration_status_ctx = (
        namedtuple('on_check_migration_status_ctx',
                   ('instance', 'remote_logger', 'immediate_cancel_file_name', 'sshclient',
                    'migration_started_time', 'estimated_migration_time')))

    ocms_ctx = (
        on_check_migration_status_ctx(
            instance=instance,
            remote_logger=ctx.remote_logger,
            immediate_cancel_file_name=
            ctx.session_immed_cancel_file_name,
            sshclient=sshclient,
            migration_started_time=datetime.datetime.utcnow(),
            estimated_migration_time=
            datetime.timedelta(seconds=
                               instance.getEstimatedMigrationTime(
                                   ctx.migrationtimepergigabyte))))

    rollback_context_type = namedtuple('rollback_context_type',
                                       ('apiclient', 'connection', 'first_host_id',
                                        'instance', 'remote_logger'))
    rollback_ctx = rollback_context_type(apiclient=ctx.apiclient, connection=ctx.connection,
                                         first_host_id=first_host_id, instance=instance,
                                         remote_logger=ctx.remote_logger)

    #virsh migrate-setspeed
    sms_ctx = sms_context_type(instance=instance, migratespeed=ctx.migratespeed,
                               remote_logger=ctx.remote_logger, sshclient=sshclient)
    set_migrate_speed(sms_ctx)

    #API実行 (戻しマイグレーション)
    printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)を開始します。'
                                 .format(str(instance)), ctx.remote_logger)
    jobid = ctx.apiclient.migrateVirtualMachineWithVolumeAsync(instance.id,
                                                               first_host_id,
                                                               volume_destpool_tuple_list)

    try:
        ctx.connection.close()
        ctx.apiclient.waitForAsyncJobCompletion(jobid, ctx.pollinginterval,
                                                on_check_migration_status, [ocms_ctx])
        printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)が完了しました。'
                                     .format(str(instance)), ctx.remote_logger)
    except MigrationCancelledException, exc:
        ctx.connection.ping(reconnect=True, attempts=3, delay=10)
        printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーションは強制キャンセルされました。'
                                     .format(str(instance)), ctx.remote_logger)
        rollback_instance_with_exc(rollback_ctx, exc)
    except Exception, exc:
        ctx.connection.ping(reconnect=True, attempts=3, delay=10)
        printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)に失敗しました。'
                                     .format(str(instance)), ctx.remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', ctx.remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), ctx.remote_logger)
        rollback_instance_with_exc(rollback_ctx, exc)

    #DBping
    ctx.connection.ping(reconnect=True, attempts=3, delay=10)

    #スナップショットポリシを復元する
    printToStdoutAndRemoteLogger('スナップショットポリシを復元します。',
                                 ctx.remote_logger)
    for pvm in policy_volume_map:
        if SnapshotPolicy.check_existence_by_uuid(ctx.connection, pvm.policy.uuid):
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} の'
                                         '復元は不要です。'.format(pvm.policy), ctx.remote_logger)
        else:
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} が'
                                         '消えているため、再作成する必要が'
                                         'あります。'.format(pvm.policy), ctx.remote_logger)
            csp_params = {}
            if pvm.policy.interval == 0:
                csp_params['intervaltype'] = 'HOURLY'
            elif pvm.policy.interval == 1:
                csp_params['intervaltype'] = 'DAILY'
            elif pvm.policy.interval == 2:
                csp_params['intervaltype'] = 'WEEKLY'
            elif pvm.policy.interval == 3:
                csp_params['intervaltype'] = 'MONTHLY'
            csp_params['maxsnaps'] = pvm.policy.maxSnaps
            csp_params['schedule'] = pvm.policy.schedule
            csp_params['timezone'] = pvm.policy.timezone
            csp_params['volumeid'] = pvm.volume.uuid
            csp_params['fordisplay'] = pvm.policy.display
            restored_uuid = ctx.apiclient.createSnapshotPolicy(csp_params)
            printToStdoutAndRemoteLogger('スナップショットポリシ {0} を'
                                         '復元しました。'
                                         .format(pvm.policy), ctx.remote_logger)
            restored_policy = SnapshotPolicy.create_instance_by_uuid(ctx.connection, restored_uuid)
            printToStdoutAndRemoteLogger('復元後のスナップショットポリシ: {0}'
                                         .format(restored_policy), ctx.remote_logger)
    printToStdoutAndRemoteLogger('スナップショットポリシを復元しました。',
                                 ctx.remote_logger)

    #マイグレーション結果確認
    res_str, result = (
        get_kvm_migration_result_string(ctx.connection,
                                        volume_destpool_namedtuple_list,
                                        instance, first_host_id))
    printToStdoutAndRemoteLogger('インスタンス {0}: {1}'
                                 .format(str(instance), res_str), ctx.remote_logger)
    if result:
        printToStdoutAndRemoteLogger('Migration of instance {0} has been completed '
                                     'sucessfully.'
                                     .format(instance), ctx.remote_history_logger)
    else:
        printToStdoutAndRemoteLogger('Migration of instance {0} has been completed '
                                     'but there seems to be problems.'
                                     .format(instance), ctx.remote_history_logger)

def check_logger_ssh(remote_logger):
    """リモートロガーのssh接続を確認します.

    Parameters
    ----------
    remote_logger: RemoteLogger
        接続を確認するロガー

    Returns
    -------
    bool
        接続を確認できた場合はTrue. それ以外の場合はFalse
    """
    if remote_logger is not None:
        sys.stdout.write('リモートログ出力のためのSSH接続を確認中...')
        if remote_logger.checkSsh():
            print('SSH接続を確認しました。')
        else:
            print('SSH接続を確認できませんでした。')
            print('正しくリモートログ出力オプションを指定しているか確認してください。')
            return False
    return True

def join_logger(logger):
    """すべてのログの出力の完了を待機し、ロガースレッドをjoinします.

    Parameters
    ----------
    Logger: ロガー
    """
    if logger is not None:
        logger.join()

def print_usage():
    """このスクリプトの使用法を表示します.

    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    print('使用法: {0} [オプション...] [--host <マイグレーション先のホストのID> '
          '--virtualmachine <マイグレーション対象の仮想マシンのID> '
          '[--volume <ボリュームID> --pool <プールID>]...]...'.format(sys.argv[0]))
    print("")
    print("指定されたKVMインスタンスと、インスタンスに紐づいているストレージを移行します。")
    print("このツールはバッチサーバ上の eosl_migrate_kvm.py から実行されることを想定しており、直接実行されることは想定しておりません。")
    print('')
    print('オプション:')
    print('  --apikey <apikey>                 CloudStack APIを使用するためのAPIキーを<apikey>とする')
    print('  --secretkey <secretkey>           '
          'CloudStack APIを使用するためのシークレットキーを<secretkey>とする')
    print('  --polling-interval <number>       マイグレーション状態の確認を<number>秒ごとに行う')
    print('  --sessionid <sessionid>           マイグレーションセッションのIDを<sessionid>とする。')
    print('  --fsdir <path>                    セッションファイルや停止フラグを配置するディレクトリを指定する')
    print('  --sshpass <path>                  sshpassコマンドへのパスを指定する')
    print('  --kvmpassword <password>          KVMホストにログインするためのパスワードを<password>にする')
    print('  --dbhost <hostname>               CloudStackのデータベースを<hostname>にする')
    print('  --dbpassword <password>           CloudStackのデータベースにログインするためのパスワードを指定する')
    print('  --migration-time-per-gigabyte <number> ディスク1GBあたりの'
          'マイグレーション時間を<number>秒としてマイグレーション時間を見積もる')
    print('  --remoteloghost <hostname>        ログの出力先ホストを<hostname>にする')
    print('  --remoteloguser <username>        ログの出力先ホストにログインするためのユーザ名を<username>にする')
    print('  --remotelogkey <keyfile>          ログの出力先ホストにログインするための秘密鍵を<keyfile>にする')
    print('  --remotelogdirectory <path>       ログの出力先ホスト内での、ログの出力先ディレクトリを<path>にする')
    print('')
    print('  --host <id>                       マイグレーション先のホストのIDまたはUUIDを指定する')
    print('  --virtualmachine <id>             マイグレーションする仮想マシンのIDまたはUUIDを指定する')
    print('  --volume <id>                     マイグレーションするボリュームを指定する')
    print('  --pool <id>                       '
          '直前の--volumeオプションで指定したボリュームを<id>のIDまたはUUIDのストレージプールにマイグレーションする')

def main():
    """スクリプトのエントリポイントです."""
    try:
        try:
            parsed_args = parse_args()
        except:
            emsg = sys.exc_info()[1]
            print(emsg)
            #raise
            return
        if parsed_args['usage']:
            print_usage()
            return

        try:
            connection = mysql.connector.connect(user='root',
                                                 host=parsed_args['db'].host,
                                                 password=parsed_args['db'].password,
                                                 database='cloud', charset='utf8')
        except mysql.connector.Error:
            sys.stderr.write('DB接続で問題が発生しました: {0}'.format(sys.exc_info()[1]))
            return
        session_normal_cancel_file_name = None
        remote_logger = None
        remote_history_logger = None
        dir_path = os.path.dirname(os.path.realpath(__file__))
        sshpass = dir_path + '/eosl_local_root/usr/local/bin/sshpass'
        if (parsed_args['fsdirectory'] is not None
                and not os.path.exists(parsed_args['fsdirectory'])):
            os.makedirs(parsed_args['fsdirectory']) #フラグ・セッション管理ディレクトリを作成する
        #セッションファイル作成処理
        if parsed_args['sessionid'] is not None:
            session_file_name = (parsed_args['fsdirectory'] +
                                 '/' + parsed_args['sessionid'] + '.session')
            open(session_file_name, 'a').close()
            session_normal_cancel_file_name = (parsed_args['fsdirectory'] +
                                               '/' +
                                               parsed_args['sessionid'] +
                                               '_normal.flg') #通常キャンセルフラグファイル名
            session_immed_cancel_file_name = (parsed_args['fsdirectory'] +
                                              '/' +
                                              parsed_args['sessionid'] +
                                              '_immediate.flg') #強制キャンセルフラグファイル名

            if parsed_args['remotelog'].directory is not None:
                Utility.remoteMkdir(parsed_args['remotelog'].directory,
                                    parsed_args['remotelog'].user,
                                    parsed_args['remotelog'].host,
                                    parsed_args['remotelog'].password,
                                    option='-p')
                remote_log_file_name = (
                    parsed_args['remotelog'].directory +
                    '/' +
                    'eosl_migrate_kvm.' +
                    parsed_args['sessionid'] + '.log')
                remote_logger = RemoteLogger(remote_log_file_name,
                                             parsed_args['remotelog'].host,
                                             parsed_args['remotelog'].password,
                                             parsed_args['remotelog'].user, sshpass)
                remote_history_log_file_name = (
                    parsed_args['remotelog'].directory +
                    '/' +
                    'eosl_migrate_kvm_rireki.' + parsed_args['sessionid'] + '.log')
                remote_history_logger = RemoteLogger(remote_history_log_file_name,
                                                     parsed_args['remotelog'].host,
                                                     parsed_args['remotelog'].password,
                                                     parsed_args['remotelog'].user, sshpass)

        if not check_logger_ssh(remote_logger):
            return

        printToStdoutAndRemoteLogger('マイグレーションセッション {0} を開始しました。'
                                     .format('' if parsed_args['sessionid'] is None
                                             else parsed_args['sessionid']), remote_logger)
        printToStdoutAndRemoteLogger('***** Migration session started: {0} *****'
                                     .format('<unspecified>' if parsed_args['sessionid'] is None
                                             else parsed_args['sessionid']), remote_history_logger)

        csac = CloudStackApiClient('http://localhost:8080/client/api?',
                                   parsed_args['apikey'], parsed_args['secretkey'])

        migration_context = namedtuple('migration_context', ('apiclient', 'connection',
                                                             'kvmhostpassword',
                                                             'migration_info',
                                                             'migratespeed',
                                                             'migratespeedtoha',
                                                             'migrationtimepergigabyte',
                                                             'pollinginterval',
                                                             'remote_logger',
                                                             'remote_history_logger',
                                                             'session_normal_cancel_file_name',
                                                             'session_immed_cancel_file_name',
                                                             'sshpass'))

        for migration_info in parsed_args['migrationinfolist']:
            #通常キャンセル処理
            if (session_normal_cancel_file_name is not None
                    and os.path.exists(session_normal_cancel_file_name)):
                os.remove(ctx.session_normal_cancel_file_name)
                printToStdoutAndRemoteLogger('通常キャンセルフラグファイルを検出しました。'
                                             'マイグレーションセッションを終了しています...', ctx.remote_logger)
                break
            mctx = migration_context(apiclient=csac, connection=connection,
                                     kvmhostpassword=parsed_args['kvmhostpassword'],
                                     migration_info=migration_info,
                                     migratespeed=parsed_args['migratespeed'],
                                     migratespeedtoha=parsed_args['migratespeedtoha'],
                                     migrationtimepergigabyte=
                                     parsed_args['migrationtimepergigabyte'],
                                     pollinginterval=parsed_args['pollinginterval'],
                                     remote_logger=remote_logger,
                                     remote_history_logger=remote_history_logger,
                                     session_normal_cancel_file_name=
                                     session_normal_cancel_file_name,
                                     session_immed_cancel_file_name=
                                     session_immed_cancel_file_name,
                                     sshpass=sshpass)

            try:
                instance = VirtualMachine(connection, migration_info.virtualmachineid)
                printToStdoutAndRemoteLogger('Migration of instance {0} is started.'
                                             .format(instance), remote_history_logger)
                migrate_instance(mctx)
            except MigrationCancelledException:
                printToStdoutAndRemoteLogger('Migration of instance {0} has '
                                             'been forcely terminated.'
                                             .format(instance), remote_history_logger)
                printToStdoutAndRemoteLogger('インスタンス {0}: マイグレーションは強制キャンセルされました。'
                                             .format(migration_info.virtualmachineid),
                                             remote_logger)
                printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
                break
            except:
                printToStdoutAndRemoteLogger('Migration of instance {0} failed.'
                                             .format(instance), remote_history_logger)
                printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション中にエラーが発生しました。'
                                             .format(migration_info.virtualmachineid),
                                             remote_logger)
                printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
                printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
                printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
                continue
    except:
        printToStdoutAndRemoteLogger('マイグレーションセッションの実行中に予期していないエラーが発生しました。', remote_logger)
        printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
        printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
        printToStdoutAndRemoteLogger('マイグレーションセッションを中止します。', remote_logger)
    finally:
        #セッションファイル削除処理
        if session_file_name is not None and os.path.exists(session_file_name):
            os.remove(session_file_name)
        if remote_logger is not None:
            remote_logger.writeln('マイグレーションセッション {0} を終了します。'
                                  .format(None if parsed_args['sessionid'] is None
                                          else parsed_args['sessionid']))
            print('リモートログの出力が完了するのを待機しています...')
            join_logger(remote_logger)

        printToStdoutAndRemoteLogger(
            '***** Migration session finished: {0} *****'
            .format('<unspecified>' if parsed_args['sessionid'] is None
                    else parsed_args['sessionid']), remote_history_logger)
        join_logger(remote_history_logger)

main()
