#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""ESXiスクリプト/KVMスクリプトの共通コードです."""
import sys
import mysql.connector

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

#マイグレーション情報クラス
#hostId: マイグレーション先のホストのID
#virtualMachineId: マイグレーション対象の仮想マシンのID
#volumePoolTagTupleList: ボリュームとマイグレーション先ストレージのタグの組み合わせ []
class MigrationInfo:
    """マイグレーション情報を表します.

    Attributes
    ----------
    hostId: int
        ホストID
    virtualMachineId: int
        仮想マシンID
    volumePoolTagTupleList: tuple
        [0]: str
            ボリュームID
        [1]: str
            ストレージプール(プライマリストレージ)のタグ
    """

    def __init__(self):
        """MigrationInfoクラスのインスタンスを作成します.

        Parameters
        ----------
        None
        """
        self.hostId = None
        self.virtualMachineId = None
        self.volumePoolTagTupleList = []
        return

    def __str__(self):
        """マイグレーション情報を文字列にして返します.

        Returns
        -------
        str
            マイグレーション情報
        """
        ret_str = '仮想マシン{0}をホスト{1}にマイグレーションします。\n'.format(self.virtualMachineId, self.hostId)
        for i, t in enumerate(self.volumePoolTagTupleList):
            ret_str += '[{0}]ボリューム{1}をタグ {2} のストレージプール(プライマリストレージ)にマイグレーションします。\n'.format(i, t[0], t[1])
        return ret_str

    def setRequest(self, csac):
        """マイグレーション情報をもとに、CloudStackApiClientのパラメータをセットします.

        このメソッドはもう使用しないでください.

        Parameters
        ----------
        csac: CloudStackApiClient
            パラメータをセットする対象のCloudStackApiClientオブジェクト.

        Returns
        -------
        None
        """
        csac.request['command'] = 'migrateVirtualMachineWithVolume'
        csac.request['hostid'] = self.hostId
        csac.request['virtualmachineid'] = self.virtualMachineId
        for i, t in enumerate(self.volumePoolTagTupleList):
            csac.request['migrateto[{0}].volume'.format(i)] = t[0]
            csac.request['migrateto[{0}].pool'.format(i)] = t[1]

class MigrationCancelledException(Exception):
    r"""マイグレーションが"正常に"キャンセルされたときの例外です."""

    pass

class CancellationRequestedException(Exception):
    """操作のキャンセルが要求されたときの例外です."""

    pass

class CancellationError(Exception):
    """操作のキャンセルに失敗したときの例外です."""

    pass

class MigrationCancellationError(CancellationError):
    """マイグレーションのキャンセルに失敗したときの例外です."""

    pass

class MigrationNoncontinuableError(Exception):
    """マイグレーションセッションを続行できないときの例外です."""

    pass

class DatabaseError(MigrationNoncontinuableError):
    """データベース接続で問題が発生し、マイグレーションを続行できない場合の例外です."""

    pass

def printToStdoutAndRemoteLogger(msg, remote_logger):
    """標準出力およびRemoteLoggerにログを出力します.

    Parameters
    ----------
    msg: str
        出力する文字列.
    remote_logger: RemoteLogger
        出力先RemoteLoggerオブジェクト. None の場合には無視されます.

    Returne
    -------
    None
    """
    print(msg)
    if remote_logger is not None:
        remote_logger.writeln(msg)
