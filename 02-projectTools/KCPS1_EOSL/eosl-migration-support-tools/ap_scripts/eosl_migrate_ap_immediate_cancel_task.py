#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""Requests an ESXi script to kill specific vCenter task and exit its session using UNIX domain socket.

ESXiスクリプトに対し、指定したvCenterタスクをキャンセルしてマイグレーションセッションを終了するように要求します.
現在の仕様では、このスクリプトは使用されません.
"""
import sys
import socket

Usage = False
SocketFileName = None
TaskMO = None
def parse_args():
    """コマンドライン引数をパースし、グローバル変数に格納します."""
    global Usage
    global SocketFileName
    global TaskMO
    if len(sys.argv) < 3:
        Usage = True
        return
    i = 1
    while(1):
        if i == len(sys.argv):
            break
        if i == 1:
            SocketFileName = sys.argv[i]
        elif i == 2:
            TaskMO = sys.argv[i]
        i += 1

def main():
    """スクリプトのエントリポイントです."""
    try:
        parse_args()
    except:
        emsg = sys.exc_info()[1]
        print(emsg)
        #raise
        return
    if Usage:
        print("使用法: {0} <Unixドメインソケットファイル名> <キャンセルするvCenter TaskMO>".format(sys.argv[0]))
        print("")
        print("vCenterタスク(TaskMO)を指定してマイグレーションの強制停止をリクエストします。")
        print("このツールはバッチサーバ上の eosl_migrate_stop.py から実行されることを想定しており、直接実行されることは想定しておりません。")
        print('')
        return
    sock = None
    try:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_SEQPACKET)
        sock.connect(SocketFileName)
    except Exception:
        sys.stderr.write('{0} への接続に失敗しました: {1}\n'.format(SocketFileName, sys.exc_info()[1]))
        return

    try:
        sock.send('IMMEDIATE_CANCEL_WITH_SPECIFIC_TASK:{0}'.format(TaskMO))
    finally:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
main()
