#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""Loggerクラスを含むモジュールです."""
import datetime
import sys

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)
#ログクラス
#fp: ファイルオブジェクト
#newline: 改行コード
class Logger:
    """ログを実装するクラスです.

    Attributes
    ----------
    fp: ファイルオブジェクト
        ログの出力先ファイルです.
    newline: str
        改行コードです.
    """

    def __init__(self, filename, newline = '\n'):
        """Loggerクラスのインスタンスを作成します.
 
        Parameters
        ----------
        filename: str
            出力先ファイル名です.
        newline: str
            改行コードです.
        """
        self.fp = open(filename, 'w') if filename is not None else None
        self.newline = newline

    def writeln(self, text):
        """ログを一行, ファイルに出力します.

        Parameters
        ----------
        text: str
            出力する文字列.

        Returns
        -------
        None
        """
        if self.fp is not None:
            self.fp.write('[{0}] {1}{2}'.format(datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S"), text, self.newline))
    
    def close(self):
        """ログ出力先ファイルを閉じます.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        if self.fp is not None:
            self.fp.close()

