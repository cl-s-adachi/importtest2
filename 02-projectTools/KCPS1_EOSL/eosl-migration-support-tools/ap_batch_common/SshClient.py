#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""SSHクライアントクラス 'SshClient' を含むモジュールです."""
import sys
if __name__ == '__main__':
	print('このファイルは直接実行できません。')
	sys.exit(1)

import subprocess
class SshClient:
    """SSHクライアントクラスです. リモートホストでコマンドを実行することができます."""

    def __init__(self, username, host, password, sshpass, options = []):
        """
        コンストラクタ.
 
        Parameters
        ----------
        username: str
            ユーザ名
        host: str
            ホスト名またはIPアドレス
        password: str
            パスワード
        sshpass: str
            sshpassコマンドへのパス
        options: list
            SSHオプションリスト. たとえば, -oNumberOfPasswordPrompts=1と指定したい場合, [('NumberOfPasswordPrompts', 1, )]
        """
        self.username = username
        self.host = host
        self.password = password
        self.sshpass = sshpass
        self.options = options

    def generate_ssh_commandline(self):
        """
        sshコマンドラインを作成します.

        Parameters
        ----------
        None

        Returns
        -------
        ret: str
            sshコマンドライン
        """
        ret = [self.sshpass, '-p', self.password, 'ssh']
        for o in self.options:
            ret.append('-o{0}={1}'.format(o[0], o[1]))
        ret.append('{0}@{1}'.format(self.username, self.host))
        return ret

    def run_simple(self, arglist, check_return_code = True):
        """
        リモートホストでコマンドを実行し、その標準出力を返します.

        Parameters
        ----------
        arglist: list
            実行するコマンドライン ['ls', 'hoge/'] など
        check_return_code = True: bool
            戻り値が0でないときRuntimeErrorを発生させるかどうか

        Returns
        -------
        stdout: str
            標準出力の文字列
        """
        return self.run_simple_with_error(arglist, check_return_code)[0]

    def run_simple_with_error(self, arglist, check_return_code = True): 
        """
        リモートホストでコマンドを実行し、その標準出力と標準エラー出力を返します.

        Parameters
        ----------
        arglist: list
            実行するコマンドライン ['ls', 'hoge/'] など
        check_return_code = True: bool
            戻り値が0でないときRuntimeErrorを発生させるかどうか

        Returns
        -------
        stdout, stderr: str, str
            標準出力の文字列および標準エラー出力の文字列.
        """
        cmd = self.generate_ssh_commandline() + arglist
        p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout_data, stderr_data = p.communicate()
        if check_return_code and p.returncode != 0:
            raise RuntimeError('コマンドの戻り値が0ではありません: {0} 実行したコマンド: "{1}" 標準出力: "{2}" 標準エラー出力: "{3}" '.format(p.returncode, ' '.join(cmd), stdout_data, stderr_data))

        return (stdout_data, stderr_data, )

    def run(self, arglist):
        """
        リモートホストでコマンドを実行するsshプロセスを標準出力と標準エラー出力をパイプとして開いた状態で返します.

        Parameters
        ----------
        arglist: list
            実行するコマンドライン ['ls', 'hoge/'] など

        Returns
        -------
        subprocess.Popen
            開始したプロセス.
        """
        cmd = self.generate_ssh_commandline() + arglist
        #print('SshClient.run(): cmd: {0}'.format(cmd))
        p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)

        return p
