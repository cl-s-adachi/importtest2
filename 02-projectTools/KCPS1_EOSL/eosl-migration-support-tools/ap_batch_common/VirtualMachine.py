#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""VirtualMachineクラスを格納モジュールです."""
from __future__ import print_function
import sys
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import mysql.connector
import datetime
import time
from Volume import Volume
from Host import Host

class VirtualMachineNotFoundError(RuntimeError):
    pass

class VirtualMachine:
    """
    仮想マシンを表す VirtualMachine クラスです.

    Attributes
    ----------
    name: str
        インスタンス名
    instanceName: str
        インスタンスのi番号 (i-79-61887-VM)など
    hypervisorType: str
        ハイパーバイザ名 (KVM or VMware)
    hostId: int
        ホストID
    id: int
        インスタンスID
    uuid: str
        UUID
    state: str
        状態
    serviceType: str
        VALUE or PREMIUM
    host: Host
        インスタンスが起動しているHostのインスタンス.
    volumes: list (Volume)
        アタッチされているVolumeのリスト.
    numberOfVmSnapshots: int
        VMスナップショットの数
    ramSize: int
        メモリ容量
    type: str
        VMタイプ
    """

    def __init__(self, connection, id):
        """
        DBから仮想マシンの情報を取得し、VirtualMachineクラスのインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の結果
        id: int
            インスタンスID
        """
        self.id = id
        cur = connection.cursor()
        cur.execute('select host_id,instance_name,state,service_offering_id,hypervisor_type,name,uuid,type from vm_instance where removed is NULL and id=%s', (id,))
        row = cur.fetchone()
        if row is None:
                raise VirtualMachineNotFoundError('インスタンス {0} が見つかりませんでした。'.format(id))
        self.hostId = row[0]
        self.instanceName = row[1]
        self.state = row[2]
        service_offering_id = row[3]
        self.hypervisorType = row[4]
        self.name = row[5]
        self.uuid = row[6]
        self.type = row[7]

        cur.execute('select name from disk_offering where id=%s', (service_offering_id, ))
        row = cur.fetchone()
        if 'Premium' in row[0]:
                self.serviceType = 'PREMIUM'
        else:
                self.serviceType = 'VALUE'

        self.host = None if self.hostId is None else Host(connection, self.hostId)

        self.volumes = []
        cur.execute('select id from volumes where removed is NULL and instance_id = %s', (self.id, ))
        rows = cur.fetchall()
        for vrow in rows:
                self.volumes.append(Volume(connection, vrow[0]))

        #VMスナップショットの数を取得する
        cur.execute('select count(1) from vm_snapshots where vm_id = %s and removed is NULL', (id,))
        row = cur.fetchone()
        self.numberOfVmSnapshots = int(row[0])

        #メモリ容量
        cur.execute('select ram_size from service_offering where id = (select service_offering_id from vm_instance where id = %s)', (id,))
        row = cur.fetchone()
        self.ramSize = int(row[0])

        cur.close()
        return

    @staticmethod
    def createInstanceByInstanceName(connection, instance_name):
        """
        インスタンスのi番号からVirtualMachineのインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        instance_name: str
            インスタンスのi番号

        Returns
        -------
        ret: VirtualMachine
            i番号に対応するVirtualMachineのインスタンス.
        """
        cur = connection.cursor()
        cur.execute('select id from vm_instance where instance_name=%s', (instance_name,))
        row = cur.fetchone()
        if row is None:
            raise RuntimeError('インスタンス {0} が見つかりませんでした。'.format(instance_name))
        cur.close()
        return VirtualMachine(connection, row[0])

    @staticmethod
    def waitForStateByUuid(connection, uuid, state, timeout=60):
        """インスタンスの状態が指定した状態になるまで待機します。

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        uuid: str
            インスタンスのUUID
        state: str
            待機するインスタンスの状態
        timeout: int
            タイムアウト時間(秒)

        Returns
        -------
        None
        """
        started_time = datetime.datetime.utcnow()
        timeout_td = datetime.timedelta(seconds=timeout)
        while True:
            elapsed = datetime.datetime.utcnow() - started_time
            if elapsed > timeout_td:
                raise RuntimeError('インスタンスの状態が {0}秒 以内に {1} になりませんでした。'.format(timeout, state))
            connection.ping(reconnect=True, attempts=3, delay=10)
            try:
                cur = connection.cursor()
                cur.execute('select state from vm_instance where uuid=%s', (uuid,))
                row = cur.fetchone()
            finally:
                cur.close()
            if row is None:
                raise RuntimeError('インスタンス UUID={0} が見つかりませんでした。'.format(uuid))
            if state == row[0]:
                return
            time.sleep(1)

    def getEstimatedMigrationTime(self, migration_time_per_gigabytes):
        """
        このインスタンスのマイグレーションに必要な時間を見積もります.

        Parameters
        ----------
        migration_time_per_gigabytes: int
            1GBあたりのマイグレーション時間 (単位: 秒)

        Returns
        -------
        ret: int
            マイグレーション時間 (単位: 秒)
        """
        if migration_time_per_gigabytes < 0:
            return -1
        
        ret = 0
        for v in self.volumes:
            ret += v.getEstimatedMigrationTime(migration_time_per_gigabytes)
        return ret

    def setState(self, connection, state):
        """
        DB上のこのインスタンスの状態を変更します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        state: str
            状態. Migrating や Running など.

        Returns
        -------
        None
        """
        cur = connection.cursor()
        cur.execute('UPDATE vm_instance SET state=%s WHERE id=%s', (state, str(self.id),))
        connection.commit()
        cur.close()

    def __str__(self):
        """
        インスタンスのIDと名前を返します.

        Parameters
        ----------
        None

        Returns
        -------
        インスタンスのinstance_nameと名前を含む文字列.
        """
        return '{0}[{1}]'.format(self.instanceName, self.name)
