#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.
# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""スナップショットポリシを表す SnapshotPolicy クラスを含むモジュールです."""
from __future__ import print_function
import datetime
import sys
import time

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import mysql.connector
class SnapshotPolicy(object):
    """スナップショットポリシを表す SnapshotPolicy クラスです.

    Attributes
    ----------
    id: int
        スナップショットポリシのID
    uuid: str
        UUID
    volumeId: int
        ボリュームのID
    schedule: str
        スケジュール
    timezone: str
        タイムゾーン
    interval: int
        間隔
    maxSnaps
        最大スナップショット数
    active: bool
        有効かどうか
    display: bool
        スナップショットポリシを表示するかどうか
    """

    @staticmethod
    def create_instance_by_uuid(connection, policy_uuid):
        """DBにアクセスし、スナップショットポリシUUIDからインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        policy_uuid: int
            スナップショットポリシUUID.

        Returns
        -------
        SnapshotPolicy
        """
        ret = None
        cur = connection.cursor()
        cur.execute('select id, uuid, schedule, timezone, `interval`, max_snaps, active, display, '
                    'volume_id from snapshot_policy where uuid = %s', (str(policy_uuid),))
        row = cur.fetchone()
        cur.close()
        if row is None:
            return ret

        instance = SnapshotPolicy()
        instance.volumeId = int(row[8])
        instance.id = int(row[0])
        instance.uuid = row[1]
        instance.schedule = row[2]
        instance.timezone = row[3]
        instance.interval = row[4]
        instance.maxSnaps = int(row[5])
        instance.activate = True if row[6] == 1 else False
        instance.display = True if row[7] == 1 else False
        ret = instance

        return ret

    @staticmethod
    def create_instances_from_volume_id(connection, volume_id):
        """DBにアクセスし、ボリュームIDからインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        volume_id: int
            ボリュームID.

        Returns
        -------
        list<SnapshotPolicy>
            スナップショットポリシのリスト
        """
        ret = []
        cur = connection.cursor()
        cur.execute('select id, uuid, schedule, timezone, `interval`, max_snaps, active, display '
                    'from snapshot_policy where volume_id = %s', (str(volume_id),))
        rows = cur.fetchall()
        cur.close()
        if rows is None:
            return ret

        for row in rows:
            instance = SnapshotPolicy()
            instance.volumeId = int(volume_id)
            instance.id = int(row[0])
            instance.uuid = row[1]
            instance.schedule = row[2]
            instance.timezone = row[3]
            instance.interval = row[4]
            instance.maxSnaps = int(row[5])
            instance.activate = True if row[6] == 1 else False
            instance.display = True if row[7] == 1 else False
            ret.append(instance)

        return ret

    @staticmethod
    def check_existence_by_uuid(connection, uuid):
        """DBにアクセスし、UUIDから存在性を確認します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        volume_id: str
            スナップショットポリシのUUID

        Returns
        -------
        bool
            スナップショットポリシが存在するか
        """
        cur = connection.cursor()
        cur.execute('select count(*) from snapshot_policy where uuid=%s', (uuid,))
        row = cur.fetchone()
        cur.close()

        return row[0] > 0

    def __str__(self):
        """ボリュームのIDと名前を含む文字列を返します.

        Parameters
        ----------
        None

        Returns
        -------
        str
            ボリュームのIDと名前を含む文字列.
        """
        return 'SnapshotPolicy (id: {0}, uuid: {1}, volumeId: {2})'.format(self.id, self.uuid, self.volumeId)
