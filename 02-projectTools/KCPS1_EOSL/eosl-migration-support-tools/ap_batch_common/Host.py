#!/usr/bin/python2
# -*- coding:utf-8 -*-

# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""ホストを表すHostクラスを格納するモジュールです."""
import sys
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import re

class Host:
    """
    ホストを表すクラスです.

    Attributes
    ----------
    id: int
        ホストID
    name: str
        ホスト名
    hypervisorType: str
        ハイパーバイザ名 (KVM or VMware)
    privateIpAddress: str
        プライベートIPアドレス
    serviceType: str
        サービスタイプ (VALUE or PREMIUM)
    clusterId: int
        ホストが属するクラスタID
    totalCpuCapacity: int
        全体のCPUキャパシティ
    reservedCpuCapacity: int
        予約済みのCPUキャパシティ
    usedCpuCapacity: int
        使用済みのCPUキャパシティ
    """

    def __init__(self, connection, host_id):
        """
        DBからホスト情報を取得し、Hostクラスのインスタンスを作成します.
     
        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        host_id: int
            ホストID.
        """
        self.id = host_id
        cur = connection.cursor()
        cur.execute('select hypervisor_type,cluster_id,name,private_ip_address from host where removed is NULL and id=%s', (host_id,))
        row = cur.fetchone()
        if row is None:
            raise RuntimeError('ホスト ID={0} が見つかりませんでした。'.format(host_id))
        self.hypervisorType = row[0]
        self.clusterId = row[1]
        self.name = row[2]
        self.privateIpAddress = row[3]

        #タグ
        cur.execute('select tag from host_view where removed is NULL and id=%s', (host_id,))
        rows = cur.fetchall()
        self.tags = []
        for r in rows:
            self.tags.append(r[0])
        if 'VALUE' in self.tags:
            self.serviceType = 'VALUE'
        else:
            self.serviceType = 'Other'
            for t in self.tags:
                #PREMIUM判定
                if re.match('^.+@[a-zA-Z][0-9]{8}$', t):
                    self.serviceType = 'PREMIUM'
                    break

        cur.execute('select used_capacity,reserved_capacity, total_capacity from op_host_capacity where capacity_type=1 and host_id = %s', (host_id, ))
        row = cur.fetchone()
        self.usedCpuCapacity = row[0]
        self.reservedCpuCapacity = row[1]
        self.totalCpuCapacity = row[2]
        cur.execute('select used_capacity,reserved_capacity, total_capacity from op_host_capacity where capacity_type=0 and host_id =%s', (host_id, ))
        row = cur.fetchone()
        self.usedMemoryCapacity = row[0]
        self.reservedMemoryCapacity = row[1]
        self.totalMemoryCapacity = row[2]
        cur.close()
        return
    
    def getCpuUsage(self):
        """
        CPU使用率を取得します.

        Parameters
        ----------
        None

        Returns
        -------
        usage: float
            CPU使用率 (0.0 - 1.0)
        """
        return float(self.usedCpuCapacity + self.reservedCpuCapacity) / float(self.totalCpuCapacity)

    def getMemoryUsage(self):
        """
        メモリ使用率を取得します.

        Parameters
        ----------
        None

        Returns
        -------
        usage: float
            メモリ使用率 (0.0 - 1.0)
        """
        return self.getMemoryUsageWithExtraSize(0)

    def getMemoryUsageWithExtraSize(self, extrasize):
        """メモリ使用率をextrasizeを加味して取得します.

        Parameters
        ----------
        extrasize: int
            追加のサイズ

        Returns
        -------
        usage: float
            メモリ使用率 (0.0 - 1.0)
        """
        return float(self.usedMemoryCapacity + self.reservedMemoryCapacity + extrasize) / float(self.totalMemoryCapacity)

    def __str__(self):
        """
        ホストのIDと名前を返します.

        Parameters
        ----------
        None

        Returns
        -------
        exp: str
            ホストのIDと名前.
        """
        return '{0}[{1}]'.format(self.id, self.name)
