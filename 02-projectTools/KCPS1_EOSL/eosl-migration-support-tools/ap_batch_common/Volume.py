#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.
# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""ボリュームを表す Volume クラスを含むモジュールです."""
import datetime
import sys
import time

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import mysql.connector
class Volume:
    """ボリュームを表す Volume クラスです.

    Attributes
    ----------
    id: int
        ボリュームのID
    name: str
        ボリュームの名前
    state: str
        状態
    instanceId: str
        アタッチ先のインスタンスID
    poolId: int
        ストレージプールID
    size: int
        サイズ
    dateCenterId: int
        データセンタ (ゾーン) ID
    uuid: str
        UUID
    hypervisorType: str
        ハイパーバイザ名
    volumeType: str
        ROOT or DATADISK
    deviceId: int
        デバイスID
    """

    def __init__(self, connection, volume_id):
        """DBにアクセスし、ボリュームIDからインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        volume_id: int
            ボリュームID.
        """
        self.id = volume_id
        cur = connection.cursor()
        cur.execute('select name,state,instance_id,pool_id,size,data_center_id,uuid, volume_type, device_id from volumes where removed is NULL and id = %s', (volume_id,))
        row = cur.fetchone()
        if row is None:
                raise RuntimeError('ボリューム ID={0} が見つかりませんでした。'.format(volume_id))
        self.name = row[0]
        self.state = row[1]
        self.instanceId = row[2]
        self.poolId = row[3]
        self.size = row[4]
        self.dataCenterId = row[5]
        self.uuid = row[6]
        self.volumeType = row[7]
        self.deviceId = row[8]
        
        cur.execute('select hypervisor_type from volume_view where id=%s', (volume_id,))
        row = cur.fetchone()
        self.hypervisorType = row[0]

        cur.close()

    @staticmethod
    def createInstanceByUuid(connection, uuid):
        """UUIDからVolumeクラスのインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        uuid: str
            ボリュームのUUID.

        Returns
        -------
        Volume
            Volumeのインスタンス.
        """
        cur = connection.cursor()
        cur.execute('select id from volumes where uuid=%s', (uuid, ))
        row = cur.fetchone()
        if row is None:
            raise RuntimeError('ボリューム UUID={0} が見つかりませんでした。'.format(uuid))
        vol_id = row[0]
        cur.close()
        return Volume(connection, vol_id)

    @staticmethod
    def waitForStateByUuid(connection, uuid, state, timeout=60):
        """ボリュームの状態が指定した状態になるまで待機します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        uuid: str
            ボリュームのUUID
        state: str
            待機するインスタンスの状態
        timeout: int
            タイムアウト時間(秒)

        Returns
        -------
        None
        """
        started_time = datetime.datetime.utcnow()
        timeout_td = datetime.timedelta(seconds=timeout)
        while True:
            elapsed = datetime.datetime.utcnow() - started_time
            if elapsed > timeout_td:
                raise RuntimeError('ボリュームの状態が {0}秒 以内に {1} になりませんでした。'.format(timeout, state))
            connection.ping(reconnect=True, attempts=3, delay=10)
            try:
                cur = connection.cursor()
                cur.execute('select state from volumes where uuid=%s', (uuid,))
                row = cur.fetchone()
            finally:
                cur.close()
            if row is None:
                raise RuntimeError('ボリューム UUID={0} が見つかりませんでした。'.format(uuid))
            if state == row[0]:
                return
            time.sleep(1)

    def getEstimatedMigrationTime(self, migration_time_per_gigabytes):
        """マイグレーション時間を見積もります.

        Parameters
        ----------
        migration_time_per_gigabytes: int
            1GBあたりのマイグレーション時間.

        Returns
        -------
        int
            マイグレーション時間 (秒).
        """
        return -1 if migration_time_per_gigabytes < 0 else int(self.size / 1000 / 1000 / 1000) * migration_time_per_gigabytes

    def __str__(self):
        """ボリュームのIDと名前を含む文字列を返します.

        Parameters
        ----------
        None

        Returns
        -------
        str
            ボリュームのIDと名前を含む文字列.
        """
        return '{0}[{1}]'.format(self.id, self.name)
