#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""クラスタを表すClusterクラスを格納するモジュールです."""
import sys
import mysql.connector
from Host import Host
class Cluster:
    """クラスタを表すクラスです.

    Attributes
    ----------
    id: int
        クラスタID
    hosts: list
        ホストリスト
    """

    def __init__(self, connection, cluster_id):
        """
        クラスタ情報をDBから取得し、Clusterクラスのインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        cluster_id: int
            クラスタID
        """
        self.id = cluster_id
        cur = connection.cursor()
        cur.execute('select name from cluster where removed is NULL and id=%s', (str(cluster_id), ))
        row = cur.fetchone()
        self.name = row[0]
        cur.execute('select id from host_view where removed is NULL and cluster_id=%s', (cluster_id, ))
        rows = cur.fetchall()
        cur.close()
        self.hosts = []
        if rows is not None:
            for row in rows:
                self.hosts.append(Host(connection, row[0]))
                
        return

    def getAllKvmValueHosts(self):
        """
        クラスタ内の全KVM VALUEホストを取得します.

        Parameters
        ----------
        None

        Returns
        -------
        list
            Hostのインスタンスのlist
        """
        return [host for host in self.hosts if host.hypervisorType == 'KVM' and host.serviceType == 'VALUE']

    #クラスタ内の最もCPU使用率が低いKVM VALUEホストを取得する
    #def getLowestCpuUsageKvmValueHost(self):
        #sorted_list = sorted(self.getAllKvmValueHosts(), key=lambda x:x.getCpuUsage())
        #return None if len(sorted_list) == 0 else sorted_list[0]

    def getLowestMemoryUsageKvmValueHost(self):
        """
        クラスタ内の最もメモリ使用率が低いKVM VALUEホストを取得します.

        Parameters
        ----------
        None

        Returns
        -------
        host: Host
            クラスタ内の最もメモリ使用率が低いKVM VALUEホストのインスタンス
        """
        sorted_list = sorted(self.getAllKvmValueHosts(), key=lambda x:x.getMemoryUsage())
        return None if len(sorted_list) == 0 else sorted_list[0]

    def getHaHost(self):
        """
        クラスタ内のHAホストを1つ取得します.

        Parameters
        ----------
        None

        Returns
        -------
        host: Host
            HAホスト.存在しない場合はNone.
        """
        for host in self.hosts:
            if 'HA' in host.tags:
                return host
        return None

    def __str__(self):
        """クラスタのIDと名前を含む文字列を返します.

        Parameters
        ----------
        None

        Returns
        -------
        str
            クラスタのIDと名前を含む文字列.
        """
        return '{0}[{1}]'.format(self.id, self.name)
