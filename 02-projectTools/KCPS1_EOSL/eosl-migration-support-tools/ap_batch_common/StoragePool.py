#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""ストレージプール (プライマリストレージ) を表すStoragePoolクラスを格納するモジュールです."""
import sys
import mysql.connector

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

class StoragePool:
    """
    ストレージプール (プライマリストレージ) を表すクラスです.

    Attributes
    ----------
    id: int
        ストレージプールID
    name: str
        ストレージプール名
    tag: str
        タグ
    dataCenterId: int
        データセンタID
    clusterId: int
        クラスタID
    uuid: str
        UUID
    storagePoolCapacity: int
        ストレージプールの容量
    storagePoolOverProvisioningFactor: float
        ストレージプールのオーバープロビジョニングファクター
    storagePoolAllocatedCapacity: int
        ストレージプールの割り当て済み容量
    storagePoolAllocatedCapacityDisableThreshold: float
        ストレージプールの無効しきい値
    clusterHypervisorType: str
        ストレージプールが所属するクラスタのハイパーバイザ名    
    """

    #ストレージプールIDからインスタンスを作成する
    def __init__(self, connection, storagePoolId):
        """
        DBからストレージプールの情報を取得し、StoragePoolクラスのインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        storagePoolId: int
            ストレージプールのID
        """
        self.id = storagePoolId
        cur = connection.cursor()
        cur.execute('select name,capacity_bytes,tag,data_center_id,cluster_id,uuid from storage_pool_view where id=%s and removed is NULL;', (storagePoolId,))
        row = cur.fetchone()
        if row is None:
                raise RuntimeError('ストレージプール {0} が見つかりませんでした。'.format(storagePoolId))
        self.name = row[0]
        self.storagePoolCapacity = row[1]
        self.tag = row[2]
        self.dataCenterId = row[3]
        self.clusterId = row[4]
        self.uuid = row[5]
        cur.execute('select hypervisor_type from cluster as c inner join (select cluster_id from storage_pool where id=%s) as sp on c.id=sp.cluster_id;', (storagePoolId, ))
        row = cur.fetchone()
        self.clusterHypervisorType = row[0]
        #オーバープロビジョニングファクターを取得する
        cur.execute('select value from storage_pool_details where pool_id=%s and name=\'storage.overprovisioning.factor\'',(storagePoolId,)) #まずはストレージプールごとのファクターを取得
        row = cur.fetchone()
        if row is not None:
                self.storagePoolOverProvisioningFactor = float(row[0])
        else:
                cur.execute('select value from configuration where name=\'storage.overprovisioning.factor\';') #グローバルのファクターを取得
                row = cur.fetchone()
                self.storagePoolOverProvisioningFactor = float(row[0])

        total_vm_snapshot_capacity = 0
        total_volume_capacity = 0
        #VMスナップショットの合計容量を取得する
        cur.execute('select sum(vm_snapshot_chain_size) from volumes where removed is NULL and pool_id=%s',(self.id,))
        row = cur.fetchone()
        if row is not None and row[0] is not None:
                total_vm_snapshot_capacity = row[0]
        #ボリュームの合計容量を取得する
        cur.execute('select sum(size) from volumes where pool_id=%s and state not in(\'Destroy\',\'Expunged\')', (self.id,))
        row = cur.fetchone()
        if row is not None and row[0] is not None:
                total_volume_capacity = row[0]
        self.storagePoolAllocatedCapacity = total_vm_snapshot_capacity + total_volume_capacity
        #無効しきい値の取得
        cur.execute('select value from configuration where name = \'pool.storage.allocated.capacity.disablethreshold\'')
        row = cur.fetchone()
        self.storagePoolAllocatedCapacityDisableThreshold = float(row[0])

        cur.close()

    def getUsage(self):
        """
        ストレージの使用率を取得します.

        Parameters
        ----------
        None

        Returns
        -------
        usage: float
            ストレージの使用率
        """
        return self.getUsageWithExtraSize(0)

    def getUsageWithExtraSize(self, size):
        """
        ストレージの使用率を取得します.

        Parameters
        ----------
        size: int
            追加の使用量 (バイト)

        Returns
        -------
        usage: float
            ストレージの使用率
        """
        return float(float(self.storagePoolAllocatedCapacity + size) / float(self.storagePoolCapacity))

    def hasEnoughSpace(self, asking_size):
        """
        ストレージの容量が十分か確認します.

        Parameters
        ----------
        asking_size: int
            確保したい容量 (バイト).

        Returns
        -------
        ret: bool
            容量が十分な場合にはTrue. そうでない場合はFalse.
        """
        #無効しきい値チェック
        if float(self.storagePoolAllocatedCapacity + asking_size) / self.storagePoolCapacity > self.storagePoolAllocatedCapacityDisableThreshold:
            return False
        
        #容量チェック
        if (self.storagePoolAllocatedCapacity + asking_size) > self.storagePoolCapacity:
            return False
        return True
    
    def __str__(self):
        """
        ストレージプールのIDと名前を取得します.

        Parameters
        ----------
        None

        Returns
        -------
        exp: str
            ストレージプールのIDと名前を含む文字列.
        """
        return '{0}[{1}]'.format(self.id, self.name)

    @staticmethod
    def getPoolsByTag(connection, tag):
        """
        指定したタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        tag: str
            タグ

        Returns
        -------
        ret: list
            ストレージプールのリスト.
        """
        cur = connection.cursor()
        cur.execute('select id from storage_pool_view where removed is NULL and tag = %s', (tag,))
        rows = cur.fetchall()
        ret = []
        for r in rows:
            ret.append(StoragePool(connection, r[0]))
        return ret

    @staticmethod
    def getDataDiskPools(connection):
        """
        DATA_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return StoragePool.getPoolsByTag(connection, 'DATA_DISK')

    @staticmethod
    def getSystemDiskPools(connection):
        """
        SYSTEM_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return StoragePool.getPoolsByTag(connection, 'SYSTEM_DISK')

    @staticmethod
    def getDataDiskPoolsInDataCenter(connection, dc_id):
        """
        指定したゾーン内のDATA_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        dc_id: int
            データセンタ (ゾーン) ID

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return [x for x in StoragePool.getDataDiskPools(connection) if x.dataCenterId == dc_id]

    @staticmethod
    def getSystemDiskPoolsInDataCenter(connection, dc_id):
        """
        指定したゾーン内のSYSTEM_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        dc_id: int
            データセンタ (ゾーン) ID

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return [x for x in StoragePool.getSystemDiskPools(connection) if x.dataCenterId == dc_id]

    @staticmethod
    def getDataDiskPoolsInCluster(connection, cl_id):
        """
        指定したクラスタ内のDATA_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        cl_id: int
            クラスタID

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return [x for x in StoragePool.getDataDiskPools(connection) if x.clusterId == cl_id]

    @staticmethod
    def getSystemDiskPoolsInCluster(connection, cl_id):
        """
        指定したクラスタ内のSYSTEM_DISKタグを持つストレージプールを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値
        cl_id: int
            クラスタID

        Returns
        -------
        ret: list
            ストレージプールのリスト. 
        """
        return [x for x in StoragePool.getSystemDiskPools(connection) if x.clusterId == cl_id]
