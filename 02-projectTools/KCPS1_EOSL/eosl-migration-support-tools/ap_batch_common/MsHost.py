#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.
# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""マネジメントサーバを表す MsHost クラスを含むモジュールです."""

import sys

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import mysql.connector
class MsHost:
    """マネジメントサーバを表す Volume クラスです.

    Attributes
    ----------
    id: int
        マネジメントサーバID
    serviceIp: str
        IPアドレス
    name: str
        マネジメントサーバの名前
    state: str
        状態
    """

    def __init__(self, connection, id):
        """DBにアクセスし, IDからインスタンスを作成します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.
        id: int
            マネジメントサーバのID
        """
        self.id = id
        cur = connection.cursor()
        cur.execute('select service_ip, name, state from mshost where removed is NULL and id = %s', (str(id),))
        row = cur.fetchone()
        if row is None:
            raise RuntimeError('マネジメントサーバ ID={0} が見つかりませんでした。'.format(id))
        self.serviceIp = row[0]
        self.name = row[1]
        self.state = row[2]
        cur.close()

    @staticmethod
    def getAllManagementHosts(connection):
        """すべてのマネジメントサーバを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.

        Returns
        -------
        list
            すべてのMsHostのインスタンス.
        """
        ret = []
        cur = connection.cursor()
        cur.execute('select id from mshost where removed is NULL')
        rows = cur.fetchall()
        for row in rows:
            ret.append(MsHost(connection, row[0]))
        cur.close()
        return ret

    @staticmethod
    def getAllUpManagementHosts(connection):
        """すべてのUp状態のマネジメントサーバを取得します.

        Parameters
        ----------
        connection: MySQLConnection
            mysql.connector.connect()の戻り値.

        Returns
        -------
        list
            すべてのUp状態のMsHostのインスタンス.
        """
        return [x for x in MsHost.getAllManagementHosts(connection) if x.state == 'Up']

    def __str__(self):
        """マネジメントサーバのIDと名前を含む文字列を返します.

        Parameters
        ----------
        None

        Returns
        -------
        str
            マネジメントサーバのIDと名前を含む文字列.
        """
        return '{0}[{1}]'.format(self.id, self.name)
