#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""よく使用するメソッドを実装したユーティリティモジュールです."""
import sys
if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

import subprocess
import os
import select
from time import sleep
def remoteFileExists(path, username, host, password):
    """
    リモートホストにファイルが存在するかどうか確認します.
    
    Parameters
    ----------
    path: str
        ファイルパス.
    username: str
        リモートホストにログインするためのユーザ名
    host: str
        リモートホスト名またはIPアドレス
    password: str
        リモートホストにログインするためのパスワード

    Returns
    -------
    ret: bool
        リモートホストに指定したファイルが存在する場合はTrue. それ以外の場合はFalse
    """
    cmdargs = []
    cmdargs.append(os.path.dirname(__file__) + '/../eosl_local_root/usr/local/bin/sshpass')
    cmdargs.append('-p')
    cmdargs.append(password)
    cmdargs.append('ssh')
    cmdargs.append('-oNumberOfPasswordPrompts=1') #パスワードは1回しか聞かない
    cmdargs.append('-oStrictHostKeyChecking=no')
    cmdargs.append('-oUserKnownHostsFile=/dev/null')
    cmdargs.append('{0}@{1}'.format(username, host))
    cmdargs.append('test -f {0}'.format(path))
    p = subprocess.Popen(cmdargs, stderr=subprocess.PIPE)
    res = p.communicate()
    if p.returncode > 1:
        raise RuntimeError('リモートファイルの存在確認中にエラーが発生しました: {0}'.format(res[1]))
    return True if p.returncode == 0 else False

#sshでmkdirする
#dirname: ディレクトリ名
#kwargs:
#    option: オプション文字列
def remoteMkdir(dirname, username, host, password, **kwargs):
    """
    リモートホスト上にディレクトリを作成します.

    Parameters
    ----------
    dirname: str
        ディレクトリ名
    username: str
        リモートホストにログインするためのユーザ名
    kwargs: dictionary
        'option': str
            オプション文字列
    """
    cmdargs = []
    cmdargs.append(os.path.dirname(__file__) + '/../eosl_local_root/usr/local/bin/sshpass')
    cmdargs.append('-p')
    cmdargs.append(password)
    cmdargs.append('ssh')
    cmdargs.append('-oNumberOfPasswordPrompts=1') #パスワードは1回しか聞かない
    cmdargs.append('-oStrictHostKeyChecking=no')
    cmdargs.append('-oUserKnownHostsFile=/dev/null')
    cmdargs.append('{0}@{1}'.format(username, host))
    cmdargs.append('mkdir {0} {1}'.format(kwargs.get('option',''), dirname))
    p = subprocess.Popen(cmdargs, stderr=subprocess.PIPE)
    res = p.communicate()
    if p.returncode > 1:
        raise RuntimeError('リモートディレクトリの作成中にエラーが発生しました: {0}'.format(res[1]))
    return True if p.returncode == 0 else False

def pingMySQLConnection(connection, reconnect = False, attempts = 1, delay = 0, reconnect_cb = None, reconnect_cb_args = []):
    """
    MySQLConnectionの接続を維持します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の戻り値
    reconnect: bool
        再接続するかどうか
    attempts: int
        再接続の試行回数
    delay: int
        再接続の間隔 (秒)
    reconnect_cb: def
        再接続時に呼び出すメソッド.
    reconnect_cb_args: list
        reconnect_cbに与える引数.
    """
    if attempts < 1:
        raise ValueError('attemptsは1以上でなければいけません。')

    #再接続しないとき
    if not reconnect:
        return connection.is_connected()

    #再接続するとき
    for i in range(attempts):
        ctd = connection.is_connected()
        if ctd:
            return True #接続されていたらTrueを返す
        try:
            connection.reconnect()
            ctd = connection.is_connected()
            if ctd:
                return True #接続されていたらTrueを返す
        except:
            pass
        if reconnect_cb is not None:
            reconnect_cb(*reconnect_cb_args)
        if attempts - 1 == i:
            break #最後のループでディレイしないため
        sleep(delay) #ディレイ
    return False #再接続失敗

def raiseExceptionOnReadable(fdlist, ex):
    """
    ファイルディスクリプタが読み取り可能になった場合に、例外を発生させます.

    Parameters
    ----------
    fdlist: list
        ファイルディスクリプタのリスト.
    ex Exception
        発生させる例外.

    Returns
    -------
    None
    """
    rlist, wlist, xlist = select.select(fdlist, [], [], 0)
    if len(rlist) > 0:
        raise ex
