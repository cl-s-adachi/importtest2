#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import sys
import urllib2
import urllib
import traceback
import time
import os
import datetime
import mysql.connector
import re
import subprocess
import threading
import socket
import select
import Queue

from eosl_migrate_ap_common import MigrationCancelledException
from eosl_migrate_ap_common import MigrationCancellationError
from eosl_migrate_ap_common import MigrationNoncontinuableError
from eosl_migrate_ap_common import CancellationRequestedException
from eosl_migrate_ap_common import DatabaseError
from eosl_migrate_ap_common import printToStdoutAndRemoteLogger
from eosl_migrate_ap_common import MigrationInfo
from CloudStackApiClient import CloudStackApiClient
from VCSClient import VCSClient
from VCSClient import VCSClientError
from VCSClient import VCSSessionExpiredError
from VCSClient import VCSManagedObjectNotFoundError
from VCSTaskInfo import VCSTaskInfo
from RemoteLogger import RemoteLogger
from Logger import Logger
from ap_batch_common.Host import Host
from ap_batch_common.StoragePool import StoragePool
from ap_batch_common.Volume import Volume
from ap_batch_common.VirtualMachine import VirtualMachine
from ap_batch_common.Utility import pingMySQLConnection
from ap_batch_common.Utility import raiseExceptionOnReadable

#[メインスレッド]
def getVcenterHostByDataCenterId(connection, dcId):
	cur = connection.cursor()
	cur.execute('select vcenter_host from vmware_data_center where id=(select vmware_data_center_id from vmware_data_center_zone_map where zone_id=%s);', (str(dcId), ))
	row = cur.fetchone()
	if row is None:
		raise RuntimeError('データセンタ(ゾーン) ID {0} に対応するvCenter Serverのホストを取得できませんでした。'.format(str(dcId)))
	return row[0]
	cur.close()

#[メインスレッド]
#マイグレーション結果を確認し、結果を文字列として返す。
#connection: mysql.connector.connect()の結果
#diskUuid: マイグレーション結果を確認するディスクのUUID
#destPoolId: マイグレーション先のストレージプールのID
#returns: マイグレーション結果について格納されたタプル
#         [0] マイグレーション結果についての文字列
#         [1] マイグレーションが成功している場合にはTrue。失敗した場合にはFalse。
def getVmWareMigrationResultString(connection, diskUuid, destPoolId):
	connection.ping(reconnect=True, attempts=3, delay=5)
	vol = Volume.createInstanceByUuid(connection, diskUuid)
	
	if vol.state != 'Ready':
		return ('マイグレーション結果確認でエラーと判定されました。ディスク{0}の状態がReadyではありません。'.format(vol.id), False, )

	if str(vol.poolId).strip() != str(destPoolId).strip():
		return ('マイグレーション結果確認でエラーと判定されました。ディスク{0}が指定されたマイグレーション先ストレージにマイグレーションされていません。'.format(vol.id), False, )

	return ('マイグレーションは正常に完了しました。', True, )


#[メインスレッド]
#デタッチドボリュームのマイグレーション中に強制キャンセルを要求されたときに呼び出されます
#引数:
#vcsclient: vCenter Server Client
#im_cancel_status_queue: 強制キャンセル状態更新用キュー
#started_time: マイグレーション開始時間
#**kwargs:
#	gt_func_to_call_on_error: キャンセルすべきvCenterタスクの取得に失敗したときに呼び出すメソッド。未指定の場合、lambda: None
#	gt_arglist_to_pass_on_error: ↑に渡す引数のリスト。第一引数にはスタックトレースが渡される。未指定の場合、[]
def onCancelDetachedRequested(vcsclient, im_cancel_status_queue, started_time, **kwargs):
	gt_func_to_call_on_error = kwargs.get('gt_func_to_call_on_error', lambda: None)
	gt_arglist_to_pass_on_error = kwargs.get('gt_arglist_to_pass_on_error', [])
	tasks_to_cancel = vcsclient.getTasksToCancelDetached2(started_time, datetime.timedelta(minutes=40),#300
			gt_func_to_call_on_error = gt_func_to_call_on_error, gt_arglist_to_pass_on_error = gt_arglist_to_pass_on_error)
	if tasks_to_cancel is None or len(tasks_to_cancel) == 0:
		raise MigrationCancellationError('キャンセルすべきvCenterタスクの取得に失敗しました。マイグレーションがすでに完了している可能性があります。')
	if len(tasks_to_cancel) == 1:
		try:
			#キューを空にする
			im_cancel_status_queue.get_nowait()
			im_cancel_status_queue.task_done()
		except Queue.Empty:
			pass
		im_cancel_status_queue.put('RUNNING') #強制キャンセルの状態は RUNNING
		vcsclient.cancelTask2(tasks_to_cancel[0].taskMO)
		raise MigrationCancelledException('デタッチされているボリュームのマイグレーションの強制キャンセルに成功しました。')
	else: #タスクが複数ありキャンセルできない場合
		try:
			#キューを空にする
			im_cancel_status_queue.get_nowait()
			im_cancel_status_queue.task_done()
		except Queue.Empty:
			pass
		status_str = 'PENDING\n'
		status_str += '\n'.join(['{0},{1},{2}'.format(x.taskMO, x.descriptionId, x.queueTime) for x in tasks_to_cancel])
		im_cancel_status_queue.put(status_str)

#ログクラス (履歴用)
class HistoryLogger:	
	def __init__(self, filename, sessionid, newline = '\n'):
		self.fp = open(filename, 'w') if filename is not None else None
		self.sessionId = sessionid
		self.newline = newline
		self.wroteSessionId = False
	
	def write(self, text):
		if self.fp is not None:
			if not self.wroteSessionId:
				self.fp.write('[{0}] '.format(self.sessionId))
				self.wroteSessionId = True
			self.fp.write(text)

	def writeImmediateError(self, volume_name):
		self.writeStartTime(volume_name)
		self.writeStatus('Error')

	def writeStartTime(self, volume_name):
		nowstr = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
		self.write('{0} START:{1} '.format(volume_name, nowstr))

	def writeStatus(self, status):
		nowstr = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
		self.write('END:{0} {1}'.format(nowstr, status))
		self.endLine()

	def endLine(self):
		self.write(self.newline)
		self.wroteSessionId = False

	def close(self):
		if self.fp is not None:
			if self.wroteSessionId:
				self.endLine()
			self.fp.close()

#APIハンドラースレッド
#pipeに何か書き込むと待ち受けを終了します
#強制キャンセル時、cancel_pipeに適当なデータを書き込みます。
#im_cancel_status_queueは最大要素数1のQueueで、マイグレーション強制キャンセルの状態が変化したときに文字列をput()する必要があります。初期状態はNONEです。
#im_cancel_specify_task_queueは最大要素数1のQueueで、vCenterタスクを指定してマイグレーションを強制キャンセルするときにput()されます。put()されるのはTaskMO文字列です。
#マイグレーションセッション中止時 (vCenterタスクは放置)、ams_pipeに適当なデータをに書き込みます。
def apiHandler(server, pipe, cancel_pipe, im_cancel_status_queue, im_cancel_specify_task_queue, ams_pipe):
	server.listen(1)
	client = None
	im_cancel_status = 'NONE'

	no_next_loop = False
	while True:
		read_wait_list = [server]
		if client is not None:
			read_wait_list.append(client)
		if pipe is not None:
			read_wait_list.append(pipe)
		read_sockets, write_sockets, error_sockets = select.select(read_wait_list, [], [])
		for sock in read_sockets:
			if sock == pipe:
				server.shutdown(socket.SHUT_RDWR)
				server.close()
				no_next_loop = True
				break
			if sock == server:
				#クライアントからの接続要求
				client, addr = server.accept()
			else:
				#クライアントからのメッセージ
				try:
					cmsg = client.recv(128)
					if len(cmsg) == 0: #切れている
						client.close()
						client = None
						continue
					if cmsg == 'QUIT': #通信終了
						client.send('BYE')
						client.shutdown(socket.SHUT_RDWR)
						client.close()
						client = None
					elif cmsg == 'IMMEDIATE_CANCEL':
						#キャンセルフラグを立てる
						os.write(cancel_pipe, 'hogehoge') #適当なデータをcancel_pipeに書き込むことにより、キャンセルフラグとする
						pass
					elif cmsg == 'IMMEDIATE_CANCELLATION_STATUS':
						#強制キャンセルの状態を通知する
						#NONE: 強制キャンセルはリクエストされていない
						#STARTING: 強制キャンセルするためのタスクを探している。
						#RUNNING: 強制キャンセルは実行中である
						#PENDING: 強制キャンセルがリクエストされたが、候補タスクが複数存在するため保留している
						#PENDING状態の場合には、2行目以降に候補タスクが出力される。
						try:
							im_cancel_status = im_cancel_status_queue.get_nowait()
							im_cancel_status_queue.task_done()
						except Queue.Empty:
							pass
						client.send(im_cancel_status) #強制キャンセルの状態を送信する
					#elif cmsg.startswith('IMMEDIATE_CANCEL_WITH_SPECIFIC_TASK'): #キャンセル対象のvCenterタスクを指定してキャンセルする
						#vc_task_mo = cmsg.split(':')[1].strip() #キャンセル対象のvCenterタスクのTaskMO
						#try:
							##キューを空にする
							#im_cancel_specify_task_queue.get_nowait()
							#im_cancel_specify_task_queue.task_done()
						#except Queue.Empty:
							#pass
						#im_cancel_specify_task_queue.put(vc_task_mo) #キャンセル対象のvCenterタスクをQueueを通して通知する
						#pass
					elif cmsg.startswith('ABORT_MIGRATION_SESSION'):#マイグレーションセッションを中止する (vCenterタスクは放置する)
						os.write(ams_pipe, 'hogehoge') #適当なデータをams_pipeに書き込むことにより、マイグレーションセッション中止フラグとする
						pass
					else:
						client.send('INVALID_COMMAND')
				except socket.error:
					#エラー時には切断する
					client.shutdown(socket.SHUT_RDWR)
					client.close()
					client = None
		if no_next_loop:
			break

#APIハンドラースレッドを開始する
#server: UNIXドメインソケット。このソケットはbind()されている必要があります。
#returns: タプル
#1. 新しく作成されたスレッド。
#2. 書き込みパイプへのファイルディスクリプタ。何か書き出すとスレッドを終了します。
#3. 読み込みパイプへのファイルディスクリプタ。強制キャンセルコマンドが実行されると、適当なデータが書き出されます。
#4. 最大要素数1のQueueオブジェクト。put()して強制キャンセルの状態を更新します。
#5. 最大要素数1のQueueオブジェクト。get()すると、強制キャンセル対象のvCenterタスクを取得することができます。
#6. 読み込みパイプへのファイルディスクリプタ。マイグレーションセッション中止コマンド(vCenterタスクは放置)が実行されると、適当なデータが書きだされます。
def startApiHandlerThread(server):
	rp, wp = os.pipe()
	r_cancel_pipe, w_cancel_pipe = os.pipe()
	im_cancel_status_queue = Queue.Queue(1)
	im_cancel_specify_task_queue = Queue.Queue(1)
	ams_rp, ams_wp =os.pipe()
	t = threading.Thread(target = apiHandler, args = (server, rp, w_cancel_pipe, im_cancel_status_queue, im_cancel_specify_task_queue, ams_wp))
	t.start()
	return (t, wp, r_cancel_pipe, im_cancel_status_queue, im_cancel_specify_task_queue, ams_rp)

	

Usage = False
ApiKey = None
SecretKey = None
PollingInterval = 300 #デフォルトでは結果を300秒毎に確認する
SessionId = None #セッション管理に使用するID
FsDirectory = None #フラグとセッション管理ファイルを配置するディレクトリ
LogDirectory = None #ログディレクトリ
DBHost = None #DBホスト
DBPassword = None #DBパスワード
#VCHost = None #vCenter Serverホスト
VCUsername = None #vCenter Server ユーザ名
VCPassword = None #vCenter Server パスワード
MigrationTimePerGigabyte = -1 #1GBあたりのマイグレiーション時間見積もり
RemoteLogUser = None #リモートログ出力先ホストにログインするためのユーザ名
RemoteLogHost = None #リモートログ出力先ホスト名
RemoteLogPassword = None #リモートログ出力先ホストにログインするためのパスワード
RemoteLogDirectory = None #リモートログ出力先ディレクトリ名
MigrationInfoList = []
def parse_args():
        global Usage
	global ApiKey
	global SecretKey
	global PollingInterval
        global MigrationInfoList
	global SessionId
	global FsDirectory
	global LogDirectory
	global DBHost
	global DBPassword
#	global VCHost
	global VCUsername
	global VCPassword
	global MigrationTimePerGigabyte
	global RemoteLogUser
	global RemoteLogHost
	global RemoteLogPassword
	global RemoteLogDirectory
#        global InstanceListFileName
        if len(sys.argv) < 2:
                Usage = True
                return
        i = 1
        while(1):
                if i == len(sys.argv):
                        break
		if sys.argv[i] == '--host':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--host オプションの後にホストIDが指定されていません。')
			if len(MigrationInfoList) == 0 or (MigrationInfoList[-1].virtualMachineId is not None and MigrationInfoList[-1].hostId is not None):
				MigrationInfoList.append(MigrationInfo())
			if MigrationInfoList[-1].hostId is not None:
				raise RuntimeError('ホスト {0} にマイグレーションする仮想マシンを --virtualmachine で指定してください。'.format(MigrationInfoList[-1].hostId))
			MigrationInfoList[-1].hostId = -1 if sys.argv[i].lower() == 'none' else int(sys.argv[i])
				
		elif sys.argv[i] == '--virtualmachine':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--virtualmachine オプションの後にインスタンスIDが指定されていません。')
			if len(MigrationInfoList) == 0 or (MigrationInfoList[-1].virtualMachineId is not None and MigrationInfoList[-1].hostId is not None):
				MigrationInfoList.append(MigrationInfo())
			if MigrationInfoList[-1].virtualMachineId is not None:
				raise RuntimeError('仮想マシン {0} のマイグレーション先ホストを --host で指定してください。'.format(MigrationInfoList[-1].virtualMachineId))
			MigrationInfoList[-1].virtualMachineId = -1 if sys.argv[i].lower() == 'none' else int(sys.argv[i])
		elif sys.argv[i] == '--volume':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--volume オプションの後にボリュームIDが指定されていません。')
			if len(MigrationInfoList) == 0:
				raise RuntimeError('--volume オプションの前に --host オプション または --virtualmachine オプションを指定してください。')
			else:
				if len(MigrationInfoList[-1].volumePoolTupleList) == 0:
					MigrationInfoList[-1].volumePoolTupleList.append((sys.argv[i], None, ))
				elif MigrationInfoList[-1].volumePoolTupleList[-1][0] is None:
					MigrationInfoList[-1].volumePoolTupleList[-1] = (sys.argv[i], MigrationInfoList[-1].volumePoolTupleList[1])
				elif MigrationInfoList[-1].volumePoolTupleList[-1][1] is None:
					raise RuntimeError('ボリューム ID={0} に対するマイグレーション先ストレージプールを --pool オプションで指定してください。'
					.format(MigrationInfoList[-1].volumePoolTupleList[-1][0]))
				else:
					MigrationInfoList[-1].volumePoolTupleList.append((sys.argv[i], None, ))
		elif sys.argv[i] == '--pool':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--pool オプションの後にストレージプールIDが指定されていません。')
			if len(MigrationInfoList) == 0:
				raise RuntimeError('--pool オプションの前に --host オプション または --virtualmachine オプションを指定してください。')
			else:
				if len(MigrationInfoList[-1].volumePoolTupleList) == 0:
					MigrationInfoList[-1].volumePoolTupleList.append((None, sys.argv[i], ))
				elif MigrationInfoList[-1].volumePoolTupleList[-1][1] is None:
					MigrationInfoList[-1].volumePoolTupleList[-1] = (MigrationInfoList[-1].volumePoolTupleList[-1][0], sys.argv[i])
				elif MigrationInfoList[-1].volumePoolTuple[-1][0] is None:
					raise RuntimeError('ストレージプール ID={0} に対するマイグレーション対象ボリュームを --volume オプションで指定してください。'
					.format(MigrationInfoList[-1].volumePoolTupleList[-1][1]))
				else:
					MigrationInfoList[-1].volumePoolTupleList.append((None, sys.argv[i], ))
		elif sys.argv[i] == '--apikey':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--apikey オプションの後にAPIキーが指定されていません。')
			ApiKey = sys.argv[i]
		elif sys.argv[i] == '--secretkey':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--secretkey オプションの後にシークレットキーが指定されていません')
			SecretKey = sys.argv[i]	
		elif sys.argv[i] == '--polling-interval':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--polling-interval オプションの後にポーリング間隔が指定されていません。')
			PollingInterval = int(sys.argv[i])
		elif sys.argv[i] == '--sessionid':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--sessionid オプションの後にセッションIDが指定されていません。')
			SessionId = sys.argv[i]
		elif sys.argv[i] == '--fsdir' or sys.argv[i] == '--fsdirectory':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--fsdir|--fsdirectory オプションの後にディレクトリ名が指定されていません。')
			FsDirectory = sys.argv[i]
		elif sys.argv[i] == '--logdir' or sys.argv[i] == '--logdirectory':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--logdir|--logdirectory オプションの後にディレクトリ名が指定されていません。')
			LogDirectory = sys.argv[i]
		elif sys.argv[i] == '--dbhost':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--dbhost オプションの後にデータベースホスト名が指定されていません。')
			DBHost = sys.argv[i]
		elif sys.argv[i] == '--dbpassword':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--dbpassword オプションの後にデータベースパスワードが指定されていません。')
			DBPassword = sys.argiv[i]
		#elif sys.argv[i] == '--vchost':
		#	i += 1
		#	if i == len(sys.argv):
		#		raise RuntimeError('--vchost オプションの後にvCenter Serverホスト名が指定されていません。')
		#	VCHost = sys.argv[i]
		elif sys.argv[i] == '--vcusername':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--vcusername オプションの後にvCenter Server ユーザ名が指定されていません。')
			VCUsername = sys.argv[i]
		elif sys.argv[i] == '--vcpassword':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--vcpassword オプションの後にvCenter Server パスワードが指定されていません。')
			VCPassword = sys.argv[i]
		elif sys.argv[i] == '--migration-time-per-gigabyte':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--migration-time-per-gigabyte オプションの後に秒数が設定されていません。')
			MigrationTimePerGigabyte = int(sys.argv[i])
		elif sys.argv[i] == '--remoteloghost':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--remoteloghost オプションの後にリモートログ出力先ホストが指定されていません。')
			RemoteLogHost = sys.argv[i]
		elif sys.argv[i] == '--remoteloguser':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--remoteloguser オプションの後にリモートログ出力先ホストにログインするためのユーザ名が指定されていません。')
			RemoteLogUser = sys.argv[i]
		elif sys.argv[i] == '--remotelogpassword':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--remotelogpassword オプションの後にリモートログ出力先ホストにログインするためのパスワードが指定されていません。')
			RemoteLogPassword = sys.argv[i]
		elif sys.argv[i] == '--remotelogdirectory':
			i += 1
			if i == len(sys.argv):
				raise RuntimeError('--remotelogdirectory オプションの後にリモートログ出力先パスが指定されていません。')
			RemoteLogDirectory = sys.argv[i]
                else:
			raise RuntimeError('不明なオプション \'{0}\''.format(sys.argv[i]))
                i += 1
	for minfo in MigrationInfoList:
		for tp in minfo.volumePoolTupleList:
			if tp[0] is None:
				raise RuntimeError('ストレージプール ID={0} に対するマイグレーション対象ボリュームを --volume オプションで指定してください。'
				.format(tp[1]))
			elif tp[1] is None:
				raise RuntimeError('ボリューム ID={0} に対するマイグレーション先ストレージプールを --pool オプションで指定してください。'
				.format(tp[0]))
			if SessionId is not None and FsDirectory is None:
				raise RuntimeError('セッションIDが指定されましたが、fsdirが指定されていません。--fsdirオプションを使用してディレクトリを指定してください。')

def main():
	try:
		parse_args()
	except:
		emsg = sys.exc_info()[1]
		print(emsg)
		#raise
		return
	if Usage:
		print("使用法: {0} [オプション...] [--host <マイグレーション先のホストのID> --virtualmachine <マイグレーション対象の仮想マシンのID> [--volume <ボリュームID> --pool <プールID>]...]...".format(sys.argv[0]))
                print("")
                print("ストレージプールのタグにかかわらず、ボリュームをマイグレーションします。")
		print("このツールはデバッグ用であり、お客様が実行することは想定しておりません。")
		print('')
		print('オプション:')
		print('  --apikey <apikey>                 CloudStack APIを使用するためのAPIキーを指定する')
		print('  --secretkey <secretkey>           CloudStack APIを使用するためのシークレットキーを指定する')
		print('  --polling-interval <number>       マイグレーション状態の確認間隔を<number>秒にする')
		print('  --sessionid  <session id>         マイグレーションセッションのIDを<session id>にする')
		print('  --dbhost <hostname>               <hostname>をCloudStackのデータベースとして使う')
		print('  --dbpassword <password>           CloudStackのデータベースにログインするために<password>をパスワードとして使う')
		print('')
		print('  --host <id>                       マイグレーション先のホストのIDまたはUUIDを指定する')
		print('  --virtualmachine <id>             マイグレーションする仮想マシンのIDまたはUUIDを指定する')
		print('  --volume <id>                     <id>のIDまたはUUIDのボリュームをマイグレーションする')
		print('  --pool <id>                       直前の--volumeオプションで指定されたボリュームを<id>のIDまたはUUIDのストレージプールにマイグレーションする')
		return
	#CloudStackApiClient.test('http://localhost:8080/client/api?', 'MzAo69naFbZKe94LhkEbhnBk8N6KgQhHCl8Eisu1oa94lqEFC-_Ivv7Wom8zTPhWdj6kveXGXSWDgMq0ip2QEw', 'K-Nqd-y8CE-8Z1nNHLrhazR_BBWrijw6XRUUec0NOLqM0_xdbMm5V-vwdiwcnk6F7GvObXY_AqUea5o0UbqBTg')
	if FsDirectory is not None and not os.path.exists(FsDirectory):
		os.makedirs(FsDirectory) #フラグ・セッション管理ディレクトリを作成する
	if LogDirectory is not None and not os.path.exists(LogDirectory):
		os.makedirs(LogDirectory) #ログディレクトリを作成する
	log_file_name = None
	remote_log_file_name = None
	remote_history_log_file_name = None
	history_log_file_name = None
	session_file_name = None
	session_normal_cancel_file_name = None
	session_immediate_cancel_file_name = None
	remote_logger = None
	remote_history_logger = None
	history_logger = None
	vcsclient = None #vCenter Server Client
	dir_path = os.path.dirname(os.path.realpath(__file__))
	sshpass = dir_path + '/eosl_local_root/usr/local/bin/sshpass'
	#セッションファイル作成処理
	if SessionId is not None:
		session_file_name = FsDirectory + '/' + SessionId + '.session'
		open(session_file_name, 'a').close() #セッションファイル作成
		session_normal_cancel_file_name = FsDirectory + '/' + SessionId + '_normal.flg'
		session_immediate_cancel_file_name = FsDirectory + '/' + SessionId + '_immediate.flg'
		if LogDirectory is not None:
			log_file_name = LogDirectory + '/' + 'eosl_migrate_esxi.' + SessionId + '.log'
			history_log_file_name = LogDirectory + '/' + 'eosl_migrate_esxi_rireki.' + SessionId + '.log'
			history_logger = HistoryLogger(history_log_file_name, SessionId)
		if RemoteLogDirectory is not None:
			remote_log_file_name = RemoteLogDirectory + '/' + 'eosl_migrate_esxi.' + SessionId + '.log'
			remote_logger = RemoteLogger(remote_log_file_name, RemoteLogHost, RemoteLogPassword, RemoteLogUser, sshpass)
			remote_history_log_file_name = RemoteLogDirectory + '/' + 'eosl_migrate_esxi_rireki.' + SessionId + '.log'
			remote_history_logger = RemoteLogger(remote_history_log_file_name, RemoteLogHost, RemoteLogPassword, RemoteLogUser, sshpass)
	logger = Logger(log_file_name)
	if remote_logger is not None:
		sys.stdout.write('リモートログ出力のためのSSH接続を確認中...')
		if remote_logger.checkSsh():
			print('SSH接続を確認しました。')
		else:
			print('SSH接続を確認できませんでした。')
			print('正しくリモートログ出力オプションを指定しているか確認してください。')
			raw_input()
			raise MigrationNoncontinuableError('リモートログ出力先サーバにSSH接続できませんでした。')
			

	#UNIXドメインソケットを作成する (外部からの操作(強制キャンセルなど)用)
	server = None
	server_sockfilename = '/tmp/eosl_esxi_unix_socket-{0}'.format(SessionId)
	if SessionId is not None:
		f = open(session_file_name, 'a') #セッションファイルにUNIXドメインソケットのパスを出力する (キャンセルスクリプトが見ることができるようにするため。)
		f.write(server_sockfilename)
		f.close()
		try:
			server = socket.socket(socket.AF_UNIX, socket.SOCK_SEQPACKET)
			server.bind(server_sockfilename)
		except Exception:
			printToStdoutAndRemoteLogger('UNIXドメインソケットの作成に失敗しました: \"{0}\" マイグレーションセッションを中止します。'.format(sys.exc_info()[1]), remote_logger)
			return
	handler_thread, wpipe_fd, cancel_pipe_fd,im_cancel_status_queue, im_cancel_specify_task_queue, ams_pipe_fd = startApiHandlerThread(server) #APIハンドラ
	wpipe = os.fdopen(wpipe_fd, 'w')
	
			
			
	logger.writeln('マイグレーションセッション {0} を開始しました。'.format('' if SessionId is None else SessionId))
	if remote_logger is not None:
		remote_logger.writeln('マイグレーションセッション {0} を開始しました。'.format('' if SessionId is None else SessionId))
	if remote_history_logger is not None:
		remote_history_logger.writeln('***** Migration session started: {0} *****'.format('<unspecified>' if SessionId is None else SessionId))

	connection = None #DB接続
	#マイグレーションセッション開始
	try:
		for migration_info in MigrationInfoList:
			#ソフト(通常)キャンセル
			if session_normal_cancel_file_name is not None and os.path.exists(session_normal_cancel_file_name):
				os.remove(session_normal_cancel_file_name)
				printToStdoutAndRemoteLogger('通常キャンセルフラグファイルを検出したため、マイグレーションセッションを中止します。', remote_logger)
				break
			##### DB再接続処理 #####
			if connection is None or not connection.is_connected():
				if connection is not None:
					printToStdoutAndRemoteLogger('データベース {0} との接続が切れています。再接続を行います。'.format(DBHost), remote_logger)
					connection.close()

				retry_cnt = 3
				retry_interval = 10
				for i in range(retry_cnt):
					try:
						connection = mysql.connector.connect(user='root', host=DBHost, password=DBPassword, database='cloud', charset='utf8')
						break
					except mysql.connector.Error:
						if retry_cnt - 1 == i:
							raise MigrationNoncontinuableError('データベース {0} への接続で問題が発生しました: {1} マイグレーションセッションを中止します。'.format(DBHost, sys.exc_info()[1]))
						else:
							printToStdoutAndRemoteLogger('データベース {0} への接続で問題が発生しました: {1} {2}秒後に再試行します...'.format(DBHost, sys.exc_info()[1], retry_interval), remote_logger)
					time.sleep(retry_interval)
			##### DB再接続処理 ここまで #####

			print(migration_info)	
			csac = CloudStackApiClient('http://localhost:8080/client/api?', ApiKey, SecretKey)


			#VM情報をDBから取得する
			vm = None if migration_info.virtualMachineId is None or migration_info.virtualMachineId < 0 else VirtualMachine(connection, migration_info.virtualMachineId)
			vm_original_state = None if vm is None else vm.state

			#ホスト情報をDBから取得する
			destHost = None if migration_info.hostId is None or migration_info.hostId < 0 else Host(connection, migration_info.hostId)
			prevHost = None if vm is None or vm.hostId is None or vm.hostId < 0 else Host(connection, vm.hostId)
			
			migrate_vm = False #インスタンスのマイグレーションは行わないことになりました。
			#if migration_info.hostId is None:
				#printToStdoutAndRemoteLogger('ホストIDが指定されなかったため、インスタンスのマイグレーションは行いません。', remote_logger)
				#migrate_vm = False
			#elif migration_info.virtualMachineId is None:
				#printToStdoutAndRemoteLogger('仮想マシン(インスタンス)IDが指定されなかったため、インスタンスのマイグレーションは行いません。', remote_logger)
				#migrate_vm = False

			#DBからマイグレーションするボリュームとマイグレーション先ストレージプールの詳細情報を取得する
			volume_pool_list = []
			for vpt in migration_info.volumePoolTupleList:
				v = Volume(connection, vpt[0])
				p = StoragePool(connection, vpt[1])
				volume_pool_list.append((v, p, ))
			#インスタンスマイグレーション1回目 (元のホスト -> HAホスト)
			if migrate_vm:
				try:
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)を開始します。'.format(vm), remote_logger)
					csac.migrateVirtualMachine(vm.id, destHost.id) #インスタンスを移動(Live Migration)する
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)が完了しました。'.format(vm), remote_logger)
					newvm = VirtualMachine(connection, vm.id) #マイグレーション後のVM情報
					if newvm.state != 'Running':
						printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目): マイグレーション後の状態がRunningではありません: {1}'.format(vm, newvm.state), remote_logger)
						printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
						continue
					if str(newvm.hostId).strip() != str(destHost.id).strip():
						printToStdoutAndRemoteLogger('[DEBUG] {0} != {1} Failed!'.format(newvm.hostId, destHost.id), remote_logger)
						printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目): インスタンスがホスト {1} に存在しません: {2}'.format(vm, destHost, newvm.hostId), remote_logger)
						printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
						continue
				except:
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目)中にエラーが発生しました。このインスタンスをスキップします。'.format(vm), remote_logger)
					printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
					printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
					continue
				printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(1/2回目): マイグレーションは正常に完了しました。'.format(vm), remote_logger)

			#インスタンスのstateをMigratingに変更する
			#if vm is not None:
				#try:
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を {1} から Migrating に変更しています...'.format(vm, vm.state), remote_logger)
					#vm.setState(connection, 'Migrating')
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を {1} から Migrating に変更しました。'.format(vm, vm.state), remote_logger)
				#except:
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を変更できませんでした。このインスタンスをスキップします。'.format(vm, vm.state), remote_logger)
					#printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
					#printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
					#continue

			#インスタンスがオフラインの場合、ROOTボリューム以外をすべてデタッチする
			detach_disks = vm is not None and vm.state == 'Stopped'
			detach_ok = True
			non_root_volumes = []
			if detach_disks:
				printToStdoutAndRemoteLogger('インスタンス {0} はオフラインです。ROOTボリューム以外をデタッチします。'.format(vm), remote_logger)
				non_root_volumes = [x for x in vm.volumes if x.volumeType != 'ROOT']
				for x in non_root_volumes:
					try:
						printToStdoutAndRemoteLogger('ボリューム {0} をデタッチしています...'.format(x), remote_logger)
						csac.detachVolume(x)
						printToStdoutAndRemoteLogger('ボリューム {0} をデタッチしました。'.format(x), remote_logger)
					except:
						detach_ok = False
						break
				printToStdoutAndRemoteLogger('ROOTボリューム以外のデタッチを完了しました。', remote_logger)
			if not detach_ok:
				printToStdoutAndRemoteLogger('ボリュームのデタッチを行う際にエラーが発生しました。', remote_logger)
				printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
				printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
				printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
				continue

			migrate_root_volume_with_migratevirtualmachine = detach_disks
			if migrate_root_volume_with_migratevirtualmachine:
				root_volume_tuple = [x for x in volume_pool_list if x[0].volumeType == 'ROOT'][0]
				printToStdoutAndRemoteLogger('オフラインインスタンス {0} のROOTボリューム {1} をマイグレーションします。'.format(vm, root_volume_tuple[0]), remote_logger)
				csac.migrateVirtualMachine2 (vm.id, root_volume_tuple[1].id)
				printToStdoutAndRemoteLogger('オフラインインスタンス {0} のROOTボリューム {1} のマイグレーションが完了しました。'.format(vm, root_volume_tuple[0]), remote_logger)
			#+++++ ボリュームのマイグレーション ここから +++++
			i = 1
			length = len(migration_info.volumePoolTupleList)
			for vp in volume_pool_list:
				attachedTo = None if vp[0].instanceId is None else VirtualMachine(connection, vp[0].instanceId)
				printToStdoutAndRemoteLogger('ボリューム {0} (アタッチ先: {1}) のマイグレーションを開始します。'.format(vp[0], '<アタッチされていません>' if attachedTo is None else attachedTo), remote_logger)
				started_time = datetime.datetime.utcnow() #マイグレーション開始時刻(UTC; vCenterサーバがUTCのため)
				try:
					jobid = csac.migrateVolumeAsync(vp[0], vp[1]) #[本処理] ボリュームを非同期でマイグレートする	
					if remote_history_logger is not None:
						remote_history_logger.writeln('Migration of volume {0} is started.'.format(vp[0]))
				except:
					printToStdoutAndRemoteLogger('({0}/{1})ボリューム {2} のマイグレーションに失敗しました。'.format(i, length, vp[0]), remote_logger)
					printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
					printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
					printToStdoutAndRemoteLogger('このボリュームをスキップします。', remote_logger)
					continue
				#+++++ マイグレーション状態監視 ここから +++++
				while True:
					try:
						#マイグレーションセッションを終了すべきときに発生する例外
						class AbortMigrationSessionException(Exception):
							pass
						connection.close() #DB切断
						csac.waitForAsyncJobCompletion(jobid, PollingInterval, printToStdoutAndRemoteLogger,
							[ '({0}/{1})ボリューム {2} のマイグレーション中...'.format(i, length, vp[0]), remote_logger], cancel_pipe_fd,
							func_to_call_before_iteration = raiseExceptionOnReadable,
							args_to_pass_before_iteration = [[ams_pipe_fd], AbortMigrationSessionException])
						connection.ping(reconnect = True, attempts = 3, delay = 5) #DB再接続
						printToStdoutAndRemoteLogger('({0}/{1})ボリューム {2} のマイグレーションが完了しました。'.format(i, length, vp[0]), remote_logger)
						printToStdoutAndRemoteLogger('({0}/{1})ボリューム {2}: {3}'.format(i, length, vp[0],
							getVmWareMigrationResultString(connection, vp[0].uuid, vp[1].id)[0]), remote_logger)

						#マイグレーション結果確認?
						if remote_history_logger is not None:
							remote_history_logger.writeln('Migration of volume {0} has been completed successfully.'.format(vp[0]))
						break
					except AbortMigrationSessionException:
						#vCenterタスクを放置してマイグレーションを中止する
						printToStdoutAndRemoteLogger('マイグレーションセッションの強制停止が要求されたため、マイグレーションセッションを停止します。', remote_logger)
						printToStdoutAndRemoteLogger('実行中のvCenterタスクはキャンセルされません。', remote_logger)
						return
					except CancellationRequestedException:
						#+++++ ボリュームのマイグレーションキャンセル処理 ここから +++++
						connection.ping(reconnect = True, attempts = 3, delay = 5) #DB再接続
						printToStdoutAndRemoteLogger('マイグレーションのキャンセルが要求されました。マイグレーションをキャンセルしています...'.format(i, length, vp[0]), remote_logger)
						vc_host = getVcenterHostByDataCenterId(connection, vp[0].dataCenterId)
						vcsclient = VCSClient(vc_host, VCUsername, VCPassword)

						printToStdoutAndRemoteLogger('vCenter Server [{0}] にログインしています...'.format(vc_host), remote_logger)
						login_flag = vcsclient.login2(3, printToStdoutAndRemoteLogger, [remote_logger], printToStdoutAndRemoteLogger, ['vCenter Serverへログインしています...', remote_logger])
						printToStdoutAndRemoteLogger('vCenter Server [{0}] にログインしました。'.format(vc_host), remote_logger)
						if not login_flag:
							raise MigrationCancellationError('vCenter Serverへのログインに失敗しました。')
						try:
							#キューを空にする
							im_cancel_status_queue.get_nowait()
							im_cancel_status_queue.task_done()
						except Queue.Empty:
							pass
						im_cancel_status_queue.put('STARTING') #強制キャンセルの状態は STARTING

						#[強制キャンセル]デタッチド/オフラインの場合
						if vp[0].instanceId is None or vm.hostId is None:
							def on_error(stacktrace, remote_logger):	
								printToStdoutAndRemoteLogger('デタッチド/オフラインボリューム {0} のマイグレーションの強制キャンセル中にエラーが発生しました: {1}'.format(vp[0], stacktrace), remote_logger)
							try:
								onCancelDetachedRequested(vcsclient, im_cancel_status_queue, started_time, 
											gt_func_to_call_on_error = on_error, gt_arglist_to_pass_on_error = [remote_logger])
							except MigrationCancellationError:
								printToStdoutAndRemoteLogger('デタッチド/オフラインボリューム {0} のマイグレーションを強制キャンセルできませんでした。'.format(vp[0]), remote_logger)
								printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
								printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
							except MigrationCancelledException:
								printToStdoutAndRemoteLogger('デタッチド/オフラインボリューム {0} のマイグレーションを強制キャンセルしました。'.format(vp[0]), remote_logger)
								return
							printToStdoutAndRemoteLogger('デタッチド/オフラインボリューム {0} のマイグレーションを強制キャンセルできませんでした。既にマイグレーションが完了しているか、キャンセルすべきタスクを取得できなかった可能性があります。'.format(vp[0]), remote_logger)
						#[強制キャンセル]アタッチド && オンラインの場合
						else:
							try:
								vcsclient.cancelAttachedVolumeMigrationTask(vp[0], connection)
								printToStdoutAndRemoteLogger('アタッチされているボリューム {0} のマイグレーションを強制キャンセルしました。'.format(vp[0]), remote_logger)
								return
							except MigrationCancellationError:	
								printToStdoutAndRemoteLogger('アタッチされているボリューム {0} のマイグレーションの強制キャンセルに失敗しました: {1}'.format(vp[0], sys.exc_info()[1]), remote_logger)	
								raise MigrationNoncontinuableError('ボリュームのマイグレーションの強制キャンセルに失敗しました。'), None, sys.exc_info()[2]
							except:
								printToStdoutAndRemoteLogger('アタッチされているボリューム {0} のマイグレーションの強制キャンセルを行う際に予期していないエラーが発生しました: {1}'.format(vp[0], sys.exc_info()[1]), remote_logger)	
							raise MigrationNoncontinuableError('マイグレーションセッションは強制キャンセルされました。')
						#----- ボリュームのマイグレーションキャンセル処理 ここまで -----
				#----- マイグレーション状態監視 ここまで -----
				i += 1 #ボリューム番号 (ツール内の番号であり、CloudStackDBに存在するデータとの関連はありません)
			#----- ボリュームのマイグレーション ここまで -----

			#インスタンスのstateをMigratingから戻す
			#if vm is not None:
				#try:
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を Migrating から {1} に変更しています...'.format(vm, vm_original_state), remote_logger)
					#vm.setState(connection, vm_original_state)
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を Migrating から {1} に変更しました。'.format(vm, vm_original_state), remote_logger)
				#except:
					#printToStdoutAndRemoteLogger('インスタンス {0} の状態を変更できませんでした。このインスタンスをスキップします。'.format(vm, vm.state), remote_logger)
					#printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
					#printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
					#continue

			#インスタンスマイグレーション2回目 (元のホスト -> HAホスト)
			if migrate_vm:
				try:
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)を開始します。'.format(vm), remote_logger)
					csac.migrateVirtualMachine(vm.id, prevHost.id) #インスタンスを元の場所に戻す(Live Migration)
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)が完了しました。'.format(vm), remote_logger)
					newvm = VirtualMachine(connection, vm.id) #マイグレーション後のVM情報
					if newvm.state != 'Running':
						printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目): マイグレーション後の状態がRunningではありません: {1}'.format(vm, newvm.state), remote_logger)
						printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
						continue
					if str(newvm.hostId).strip() != str(prevHost.id).strip():
						printToStdoutAndRemoteLogger('[DEBUG] {0} != {1} Failed!'.format(newvm.hostId, prevHost.id), remote_logger)
						printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目): インスタンスがホスト {1} に存在しません: {2}'.format(vm, prevHost, newvm.hostId), remote_logger)
						printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
						continue
				except:
					printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目)中にエラーが発生しました。このインスタンスをスキップします。'.format(vm), remote_logger)
					printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
					printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
					if remote_history_logger is not None:
						remote_history_logger.writeln('Migration of volume {0} failed.'.format(str(volume)))
				printToStdoutAndRemoteLogger('インスタンス {0} のマイグレーション(2/2回目): マイグレーションは正常に完了しました。'.format(vm), remote_logger)

			#デタッチしたディスクを再アタッチする
			attach_ok = True
			if detach_disks:
				printToStdoutAndRemoteLogger('インスタンス {0} からデタッチしたディスクを再アタッチします。'.format(vm), remote_logger)
				for x in non_root_volumes:
					try:
						printToStdoutAndRemoteLogger('ボリューム {0} をアタッチしています...'.format(x), remote_logger)
						csac.attachVolume(x, x.instanceId, x.deviceId)
						printToStdoutAndRemoteLogger('ボリューム {0} をアタッチしました。'.format(x), remote_logger)
					except:
						attach_ok = False
						break
			if not attach_ok:
				printToStdoutAndRemoteLogger('ボリュームのアタッチを行う際にエラーが発生しました。', remote_logger)
				printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
				printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
				printToStdoutAndRemoteLogger('このインスタンスをスキップします。', remote_logger)
				continue

	except MigrationNoncontinuableError:
		printToStdoutAndRemoteLogger('マイグレーションセッションを中止します: \"{0}\"'.format(sys.exc_info()[1]), remote_logger)
	except Exception:
		printToStdoutAndRemoteLogger('予期しないエラーにより、マイグレーションセッションを続行できません: \"{0}\"'.format(sys.exc_info()[1]), remote_logger)
		printToStdoutAndRemoteLogger('スタックトレース:', remote_logger)
		printToStdoutAndRemoteLogger(traceback.format_exc(), remote_logger)
	finally:
		if connection is not None:
			connection.close()
		#ハンドラースレッド終了
		wpipe.write('hoge') #書き込むものは適当でよい。
		wpipe.close()
		handler_thread.join()
		
		if server is not None:
			try:
				server.close()
			except Exception:
				printToStdoutAndRemoteLogger('UNIXドメインソケットのclose()に失敗しました: {0}'.format(sys.exc_info()[1]), remote_logger)
		if server_sockfilename is not None:
			try:
				os.remove(server_sockfilename)
			except Exception:
				printToStdoutAndRemoteLogger('UNIXドメインソケットの削除に失敗しました: {0}'.format(sys.exc_info()[1]), remote_logger)
		#セッションファイル削除処理
		if session_file_name is not None and os.path.exists(session_file_name):
			os.remove(session_file_name)
		logger.writeln('マイグレーションセッション {0} を終了します。'.format(None if SessionId is None else SessionId))
		if remote_logger is not None:
			remote_logger.writeln('マイグレーションセッション {0} を終了します。'.format(None if SessionId is None else SessionId))
		if remote_history_logger is not None:
			remote_history_logger.writeln('***** Migration session finished: {0} *****'.format('<unspecified>' if SessionId is None else SessionId))
		logger.close()
		if history_logger is not None:
			history_logger.close()
		if remote_logger is not None:
			print('リモートログの出力が完了するのを待機しています...')
			remote_logger.join()
		if remote_history_logger is not None:
			remote_history_logger.join()

main()
