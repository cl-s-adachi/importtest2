#!/usr/bin/python2
# -*- coding:utf-8 -*-
"""EOSL移行支援ツールの設定ファイルです."""

#KCPS1保守環境 DBサーバ
DbHost = '172.27.172.160'
DbPassword = None #パスワードが存在しない場合はNoneを指定してください。

#KCPS1保守環境APIキー・シークレットキー
ApiKey = '4bToRy9oZ5jHiNRDLutWWGlkldFz2cUOUdzz14VqbtjWjOsN1OTEZNwdAXMSU0j9GCo6joVI3vZJUEB_xPDU4g'
SecretKey = 'v46h10gT7zQlmZU8FF0C25vRVjIjYdjuJGm_4e8pirjPvwL8lWRnw-DRkONsKc5xe2_xBx09rpNqkwESAipnoQ'

#KCPS1保守環境 APサーバスクリプトの配置ディレクトリ
ApToolPath = '/home/tckkcl/scripts/tool/eosl_migration_tools_ap_vr'

#KCPS1保守環境 KVMホストへログインするためのパスワード
KvmHostPassword = 'Admin123ckk!'

#KCPS1保守環境 vCenterサーバへログインするためのユーザ名
VcenterServerUsername = 'tckk_adm'

#KCPS1保守環境 vCenterサーバへログインするためのパスワード
VcenterServerPassword = 'Admin123ckk!'

#1GBあたりのマイグレーション時間(秒) マイグレーション時間の予測に使用します。
MigrationTimePerGigabyteKVM = 10
MigrationTimePerGigabyteESXi = 14

#APサーバログイン時のユーザ
ApUser = 'tckkcl'

#APサーバログイン時のパスワード
ApPassword = 'CKK@6004'

#途中停止フラグディレクトリ (APサーバ内)
ApStopFlagsDirectory = '/home/tckkcl/scripts/tool/eosl_migration_tools_ap_vr/flags'

#ログ出力先ホスト
RemoteLogHost = '172.22.132.70'

#ログ出力先ディレクトリ
RemoteLogDirectory = '/home/tckkcl/script/tool/eosl_migration_tools_bat_vr/log'

#ログ出力先ホストに接続するときのユーザ
RemoteLogUser = 'tckkcl'

#ログ出力先ホストに接続するときのパスワード
RemoteLogPassword = 'CKK@6004'

#マイグレーション状態確認間隔 (秒)
PollingInterval = 60

#マイグレーション速度(KVM) (MiB/s) → virsh migrate-setspeedする値
MigrateSpeed = 100
MigrateSpeedToHa = 128
