#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""バッチサーバスクリプトのハイパーバイザ共通コードです."""
import sys
import datetime
import uuid

if __name__ == '__main__':
    print('このファイルは直接実行できません。')
    sys.exit(1)

class IncompatibleHypervisorError(Exception):
    """ハイパーバイザに互換性がない場合の例外です."""

    pass

class InvalidStoragePoolTagError(Exception):
    """ストレージプールタグが無効なときの例外です."""

    pass

def printToStdoutAndRemoteLogger(msg, remote_logger):
    """
    標準出力とRemoteLoggerの両方にメッセージを出力します. RemoteLoggerがNoneだった場合には標準出力にのみメッセージを出力します.

    Parameters
    ----------
    msg: str
        出力するメッセージ
    remote_logger: RemoteLogger
        RemoteLoggerのインスタンス

    Returns
    -------
    None
    """
    print(msg)
    if remote_logger is not None:
        remote_logger.writeln(msg)

def createSessionId():
    """
    セッションIDを作成します.

    Parameters
    ----------
    None

    Returns
    -------
    sessionid: str
        セッションID文字列
    """
    return datetime.datetime.now().strftime('%Y%m%d-%H%M%S-') + str(uuid.uuid4())[:8]
