#!/usr/bin/python2
# -*- coding: utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""APサーバスクリプトに対し、マイグレーションの中止要求を送信するスクリプトです."""
from __future__ import print_function
import os
import sys
import time
import traceback
import imp
from collections import namedtuple
import mysql.connector

from ap_batch_common.SshClient import SshClient
from ap_batch_common.MsHost import MsHost

def parse_args():
    """コマンドライン引数をパースし、namedtupleとして返します.

    Parameters
    ----------
    None

    Returns
    -------
    namedtuple
        引数情報
            usage: bool
                使用方法を表示するかどうか
            configfile: str
                構成ファイル名
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    rettype = namedtuple('rettype', ('usage', 'configfile'))
    ret = rettype(usage=False, configfile=dir_path + '/eosl_migrate_config.py')
    #if len(sys.argv) < 2:
    #    Usage = True
    #    return
    i = 1
    while True:
        if i == len(sys.argv):
            break
        if sys.argv[i] == '--help':
            ret = rettype(usage=True, configfile=ret.configfile)
        elif sys.argv[i] == '-f':
            i += 1
            ret = rettype(usage=ret.usage, configfile=sys.argv[i])
        else:
            print('不明なオプション \'{0}\''.format(sys.argv[i]))
            sys.exit(1)
        i += 1
    return ret

def main():
    """スクリプトのエントリポイントです."""
    parsed_args = parse_args()
    if parsed_args.usage:
        print("使用法: {0} [-f <構成ファイル名>] [--help]".format(sys.argv[0]))
        print("")
        print("マイグレーションセッションを中止するツールです。")
        print("")
        print("オプション:")
        print("")
        print("  -f <構成ファイル名>  構成ファイルとして eosl_migrate_config.py の代わりに <構成ファイル名> を使用します。")
        return

    try:
        eosl_migrate_config = imp.load_source('', parsed_args.configfile)
    except Exception:
        traceback.print_exc()
        sys.exit('Error: 設定ファイル {0} をインポートできませんでした。'.format(parsed_args.configfile))

    dbhost = getattr(eosl_migrate_config, 'DbHost', '')
    dbpassword = getattr(eosl_migrate_config, 'DbPassword', None)
    connection = None
    ap_server_list = []
    try:
        connection = mysql.connector.connect(user='root', host=dbhost, password=dbpassword,
                                             database='cloud', charset='utf8')
        ap_server_list = MsHost.getAllUpManagementHosts(connection)
    except:
        print('DBからAPサーバ情報を取得しているときにエラーが発生しました。')
        print('スタックトレース:')
        print(traceback.format_exc())
        return
    finally:
        if connection is not None:
            connection.close()

    ap_immed_cancel_status_scr_path = (
        eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_immediate_cancellation_status.py')
    ap_abrt_migration_sess_scr_path = (
        eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_abort_migration_session.py')
    dir_path = os.path.dirname(os.path.realpath(__file__))
    sshpass = dir_path + '/eosl_local_root/usr/local/bin/sshpass'

    session_list = []
    #+++++セッション取得処理 ここから+++++
    for ap_server in ap_server_list:
        client = SshClient(eosl_migrate_config.ApUser, ap_server.serviceIp,
                           eosl_migrate_config.ApPassword, sshpass,
                           [('NumberOfPasswordPrompts', 1, ), ('UserKnownHostsFile', '/dev/null'),
                            ('StrictHostKeyChecking', 'no')])
        try:
            result = client.run_simple(['ls', eosl_migrate_config.ApStopFlagsDirectory])
        except:
            print('APサーバ[{0}]で実行中のセッションを取得することに失敗しました。このサーバをスキップします。'.format(ap_server.name))
            print('スタックトレース:')
            print(traceback.format_exc())
            continue
        session_file_list = [x for x in result.split('\n') if x.endswith('.session')]
        for session_file in session_file_list:
            session = {'session': session_file[0:-8], 'session_file_name': session_file,
                       'ap_server': ap_server}

            #強制キャンセルフラグファイルパス
            immed_cancel_flag_path = (
                eosl_migrate_config.ApStopFlagsDirectory + '/' +
                session['session'] + '_immediate.flg')

            try:
                session_file_content = (
                    client.run_simple(['cat', eosl_migrate_config.ApStopFlagsDirectory +
                                       '/' + session['session_file_name']]))
                ic_status, err = (
                    client.run_simple_with_error([ap_immed_cancel_status_scr_path,
                                                  session_file_content]))
                if 'Errno 2' in err:
                    continue
                session['state'] = ic_status
            except:
                print('セッション {0} (APサーバ[{1}]) の状態を取得できませんでした。'
                      .format(session['session'], session['ap_server'].name))
                print('スタックトレース:')
                print(traceback.format_exc())
                session['state'] = None

            process = client.run(['[', '-f', immed_cancel_flag_path, ']'])
            process.wait()
            session['immedcancelflag'] = process.returncode == 0

            session_list.append(session)
    #-----セッション取得処理 ここまで-----
    print('【移行支援ツールの途中停止用ツール】')
    print('現在実行中のセッション')
    print('0) 停止を行わない')
    for i, session in enumerate(session_list):
        sys.stdout.write('{0}) {1} C/S APサーバ[{2}] '
                         .format(i + 1, session['session'], session['ap_server'].name))
        if session['state'] is not None and session['state'] == 'WAITING_FOR_MIGRATION_START':
            print('(保留中)')
        elif session['immedcancelflag']:
            print('(強制停止処理中)')
        else:
            print('')

    print('')
    while True:
        try:
            inpt = int(raw_input('停止を行う場合は、停止対象のセッションを選択してください(0-{0}) > '
                                 .format(len(session_list))))
        except:
            continue
        if inpt >= 0 and inpt <= len(session_list):
            break
    if inpt == 0:
        print('セッション停止を中止します。')
        sys.exit()
    session_to_stop = session_list[inpt - 1]
    print('')
    print('1) 現在のマイグレーション処理終了後に停止')
    print('2) セッションの強制停止')
    print('')
    while True:
        try:
            inpt = int(raw_input('停止方法を選択してください > '))
        except:
            continue
        if inpt >= 1 and inpt <= 2:
            break

    immediate = inpt == 2
    print('セッション停止の内容')
    print('{0}) {1} C/S APサーバ[{2}] ({3})'.format(inpt, session_to_stop['session'],
                                                 session_to_stop['ap_server'].name,
                                                 '直ちに停止する(強制停止)' if immediate
                                                 else '現在のマイグレーション終了後に停止する'))
    print('')
    while True:
        inpt = raw_input('指定されたセッションの停止フラグをセットします。実行しますか? (y/n) > ')
        if inpt == 'y' or inpt == 'Y':
            inpt = 'y'
            break
        elif inpt == 'n' or inpt == 'N':
            inpt = 'n'
            break
    if inpt == 'n':
        print('セッション停止を中止します。')
        sys.exit()

    ap_immed_cancel_req_script_path = (
        eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_immediate_cancel_request.py')

    client = SshClient(eosl_migrate_config.ApUser, session_to_stop['ap_server'].serviceIp,
                       eosl_migrate_config.ApPassword,
                       sshpass, [('NumberOfPasswordPrompts', 1, ),
                                 ('UserKnownHostsFile', '/dev/null'),
                                 ('StrictHostKeyChecking', 'no')])

    print('マイグレーションを停止します。しばらくお待ちください...')
    if immediate: #強制キャンセルの場合
        session_file_content = client.run_simple(['cat', eosl_migrate_config.ApStopFlagsDirectory +
                                                  '/' + session_to_stop['session_file_name']])
        if session_file_content == '': #セッションファイルの中身がからっぽの場合
            #通常の方法 (フラグファイルを置く) でマイグレーションを強制停止する
            flag_file_name = (
                session_to_stop['session'] + ('_immediate' if immediate else '_normal') + '.flg')
            client.run_simple(['touch', eosl_migrate_config.ApStopFlagsDirectory +
                               '/' + flag_file_name])
        else: #セッションファイルの中に、ファイルパスが入っている場合 (UNIXドメインソケットの場所)
            if session_to_stop['state'] == 'WAITING_FOR_MIGRATION_START':
                print('セッション {0} (APサーバ[{1}]) のマイグレーションは保留中のため、強制キャンセルできません。'
                      .format(session_to_stop['session'], session_to_stop['ap_server'].name))
                return
            #UNIXドメインソケットを用いてAPサーバスクリプトと通信する
            #まずは、IMMEDIATE_CANCEL要求を送信する
            client.run_simple([ap_immed_cancel_req_script_path, session_file_content])
            #要求を送信したら、強制停止のステータスを確認する
            #RUNNINGになった→強制キャンセルはうまくいっている
            #PENDINGになった→オフライン/デタッチドボリュームの場合で、複数タスクがキャンセル候補に挙がってしまった
            #ステータスの確認は、eosl_migrate_ap_immediate_cancellation_status.pyを呼び出して行う。
            while True:
                ic_status, err = (
                    client.run_simple_with_error([ap_immed_cancel_status_scr_path,
                                                  session_file_content]))
                if 'Errno 2' in err:
                    print('サーバ側スクリプトが終了しました。マイグレーション状態についてはログを参照してください。')
                    break
                if ic_status.startswith('RUNNING'):
                    print('マイグレーションの強制停止は正常に進行中です。')
                    break
                elif ic_status.startswith('STARTING'):
                    print('マイグレーションの強制停止の準備中です...')
                elif ic_status.startswith('PENDING'):
                    ic_status_split = ic_status.split('\n')
                    print('強制停止するべきvCenterタスクを判別できませんでした:')
                    for i in range(1, len(ic_status_split)):
                        print('{0}'.format(ic_status_split[i]))
                    while True:
                        inpt = raw_input('yを選択するとvCenter上のタスクは停止されませんが、'
                                         '移行支援ツール上は強制終了となります。'
                                         'マイグレーションセッションを終了しますか(y/n)?> ').lower()
                        if inpt == 'y' or inpt == 'n':
                            break
                    if inpt == 'y':
                        print('マイグレーションセッションを終了します。')
                        client.run_simple([ap_abrt_migration_sess_scr_path,
                                           session_file_content])
                        break
                    else:
                        sys.exit()
                time.sleep(1) #    1秒ごとにステータスの確認を行う

    else:
        flag_file_name = (session_to_stop['session'] +
                          ('_immediate' if immediate else '_normal') + '.flg')
        client.run_simple(['touch', eosl_migrate_config.ApStopFlagsDirectory +
                           '/' + flag_file_name])
        print('停止フラグをセットしました。')

main()
