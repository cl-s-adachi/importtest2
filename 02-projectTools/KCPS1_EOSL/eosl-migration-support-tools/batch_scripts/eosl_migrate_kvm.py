#!/usr/bin/python2
# -*- coding:utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""EOSL移行支援ツール バッチサーバスクリプト (KVM用) です."""
from __future__ import print_function
import mysql.connector
import sys
import datetime
import subprocess
import os
import traceback
import imp
from collections import namedtuple
from ap_batch_common.StoragePool import StoragePool
from ap_batch_common.VirtualMachine import VirtualMachine
from ap_batch_common.MsHost import MsHost
from ap_batch_common.Cluster import Cluster
from eosl_migrate_batch_common import createSessionId

def debug_print(msg):
    """標準出力にデバッグ出力を行います."""
    print('[DEBUG]' + msg)
    return

class MigrationInfo:
    """マイグレーション情報を格納するクラスです."""

    def __init__(self, dest_host, vm):
        """MigrationInfoクラスのインスタンスを作成します.

        Parameters
        ----------
        dest_host: Host
            マイグレーション先ホスト
        vm: VirtualMachine
            マイグレーションする仮想マシン
        """
        self.destinationHost = dest_host
        self.virtualMachine = vm
        self.volumePoolPairList = []

    def addVolumePoolPair(self, volume, pooltag):
        """ボリュームとマイグレーション先ストレージプールのタグのペアを登録します.

        Parameters
        ----------
        volume: Volume
            マイグレーションするボリューム.
        pooltag: str
            マイグレーション先ストレージプールのタグ.
        """
        self.volumePoolPairList.append((volume, pooltag, ))


def getInstancesFromFile(connection, filename):
    """インスタンスリストファイルからVirtualMachineの配列を作成します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の戻り値
    filename: str
        インスタンスリストファイル名
    """
    instances = []
    f = open(filename)
    lines = f.readlines()
    for line in lines:
        if len(line.strip()) == 0:
            continue
        instances.append(VirtualMachine.createInstanceByInstanceName(connection, line.strip()))
    return instances

def get_instances_by_name_list(connection, namelist):
    """インスタンス名のリストからVirtualMachineの配列を作成します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()の戻り値
    namelist: list<str>
        インスタンス名のリスト
    """
    instances = []
    for name in namelist:
        if len(name.strip()) == 0:
            continue
        instances.append(VirtualMachine.createInstanceByInstanceName(connection, name.strip()))
    return instances

def checkPrerequisites(instance, destinationHost):
    """マイグレーション前提条件を確認します.

    Parameters
    ----------
    instance: VirtualMachine
        マイグレーション対象のインスタンス
    destinationHost: Host
        マイグレーション先ホスト

    Returns
    -------
    bool
        前提条件を満たしているかどうか
    """
    if instance.host is None:
        print("インスタンス {0} はオフライン状態です。".format(instance))
        return False

    memusage = destinationHost.getMemoryUsageWithExtraSize(instance.ramSize)
    if memusage > 0.8:
        print('マイグレーション先ホスト {0} のメモリ使用率がマイグレーション後に80%を超えてしまいます: {1}'
              .format(destinationHost, memusage))
        return False

    for v in instance.volumes:
        if v.state != "Ready":
            print("インスタンス{0}[{1}]: ボリューム{2}[{3}]がReady状態ではありません".format(instance.id, instance.instanceName, v.id, v.name))
            return False
    return True

def confirmation(mis, aps, mtpg):
    """マイグレーション実行前確認を行います.

    mis: MigrationInfoのlist
        マイグレーション情報のlist
    aps: list
        APサーバのlist
    mtpg: int
        1GBあたりのマイグレーション時間.
    """
    print("【KVM移行支援ツール マイグレーションコマンド実行前確認】")
    totalEstimatedMigrationTime = datetime.timedelta()
    for mi in mis:
        est = datetime.timedelta(seconds=mi.virtualMachine.getEstimatedMigrationTime(mtpg))
        printstate = '不明'
        if mi.virtualMachine.state == 'Running':
            printstate = 'オンライン'
        elif mi.virtualMachine.state == 'Stopped':
            printstate = 'オフライン'
        print("インスタンス: {0}({1}, {2})     マイグレーション予想時間: {3}"
              .format(mi.virtualMachine.instanceName,
                      mi.virtualMachine.host.serviceType, printstate, est))
        totalEstimatedMigrationTime += est
    print("")
    print("マイグレーション予想時間合計: {0}".format(totalEstimatedMigrationTime))
    print("")
    print("0) マイグレーションの中止")
    for i, ap in enumerate(aps):
        print("{0}) {1}".format(i + 1, ap.name))
        continue
    while True:
        try:
            apindex = int(raw_input("マイグレーションをバックグラウンドリリースするAPサーバを選択してください(1-{0}) >".format(len(aps))))
            if apindex >= 0 and apindex <= len(aps):
                break
        except ValueError:
            pass

    print("")
    if apindex == 0:
        return None
    print("実行サーバ: {0}".format(aps[apindex - 1].name))
    print("")
    yesno = None
    while(1):
        yesno = raw_input("マイグレーションを実行しますか(y/n)? >").strip()
        if yesno != 'y' and yesno != 'n':
            print("yまたはnと入力してください。")
        else:
            break
    return apindex - 1 if yesno == 'y' else None

def parse_args():
    """引数をパースし、dictとして返します

    Parameters
    ----------
    None

    Returns
    -------
    dict
        ['db']: namedtuple
            host: str
                DBホスト
            password: str
                DBパスワード (Noneの場合は、パスワードなし)
        ['configfile']: str
            構成ファイル名
        ['debugmode']: bool
            デバッグモードかどうか
        ['instancenamelist']: list<str>
            インスタンス名のリスト
        ['instancenamelistfile']: str
            インスタンス名のリストが格納されたファイル名
        ['usage']: bool
            使用法を表示するかどうか    
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    db_type = namedtuple('db_type', ('host', 'password'))
    ret = {}
    ret['db'] = db_type(host=None, password=None)

    ret['debugmode'] = False
    ret['configfile'] = dir_path + "/eosl_migrate_config.py"
    ret['instancenamelist'] = []
    ret['instancenamelistfile'] = None
    ret['usage'] = False
    if len(sys.argv) < 2:
        ret['usage'] = True
        return ret
    i = 1
    while(1):
        if i == len(sys.argv):
            break
        if sys.argv[i] == '-h' or sys.argv[i] == '--dbhost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('DBのホスト名を指定してください。')
            ret['db'] = db_type(host=sys.argv[i], password=ret['db'].password)
        elif sys.argv[i] == '-p' or sys.argv[i] == '--dbpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('DBのパスワードを指定してください。')
            ret['db'] = db_type(host=ret['db'].host, password=sys.argv[i])
        elif sys.argv[i] == '-f':
            i += 1
            ret['configfile'] = sys.argv[i]
        elif sys.argv[i] == '-d' or sys.argv[i] == '--debug':
            ret['debugmode'] = True
        elif sys.argv[i] == '-n':
            i += 1
            ret['instancenamelist'].append(sys.argv[i])
        else:
            ret['instancenamelistfile'] = sys.argv[i]
        i += 1
    return ret

def main():
    """スクリプトのエントリポイントです.

    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    try:
        args = parse_args()
    except RuntimeError:
        print(sys.exc_info()[1])
        return
    if args['usage']:
        print("使用法: {0} [-f <構成ファイル名>] [-h|--dbhost <DBアドレス>] [-p|--dbpassword <DBパスワード>] [-n インスタンス名]... [インスタンスのリストファイル]".format(sys.argv[0]))
        print("")
        print("リストファイルで指定されたKVMインスタンスのストレージを移行します。")
        return

    try:
        eosl_migrate_config = imp.load_source('', args['configfile'])
    except ImportError:
        sys.exit('Error: 構成ファイル {0} をインポートできません: {0}'.format(sys.exc_info()[1]))

    #DB情報
    dbhost = getattr(eosl_migrate_config, 'DbHost')
    dbpassword = getattr(eosl_migrate_config, 'DbPassword')
    dbhost = args['db'].host if dbhost is None else dbhost
    dbpassword = args['db'].password if dbpassword is None else dbpassword

    #APサーバツール情報
    ApSshpassCommandPath                      = eosl_migrate_config.ApToolPath + '/eosl_local_root/usr/local/bin/sshpass'
    ApScreenCommandPath                       = eosl_migrate_config.ApToolPath + '/eosl_local_root/usr/bin/screen'
    ApKvmToolPath                           = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_kvm.py'

    c = mysql.connector.connect(user='root', host=dbhost, password=dbpassword, database='cloud', charset='utf8')
    if args['instancenamelist'] is None or args['instancenamelist'] == 0:
        print('インスタンスが指定されていません。')
        return
    try:
        instances = get_instances_by_name_list(c, args['instancenamelist'])
        if args['instancenamelistfile'] is not None:
            instances.extend(getInstancesFromFile(c, args['instancenamelistfile']))
    except RuntimeError:
        print('インスタンスの情報を取得中にエラーが発生しました: {0}'.format(sys.exc_info()[1]))
        return
    except mysql.connector.Error:
        print('インスタンスの情報を取得中にDBエラーが発生しました: {0}'.format(sys.exc_info()[1]))
        return

    migration_info_list = []
    
    for i in instances:
        desthost = None
        migration_info = None

        #インスタンス状態確認
        if i.hostId is None:
            print('インスタンス{0}[{1}] はオフライン状態のため、マイグレーションできません。このインスタンスをスキップします。'.format(i.id, i.name))
            continue
        
        #ハイパーバイザ確認
        if i.hypervisorType != 'KVM':
            print('インスタンス{0}[{1}] のハイパーバイザはKVMではありません: {2}'.format(i.id, i.name, i.hypervisorType))
            return

        #ホスト選択
        if i.host.serviceType == 'VALUE':
            desthost = Cluster(c, i.host.clusterId).getHaHost()
        elif i.host.serviceType == 'PREMIUM':
            desthost = Cluster(c, i.host.clusterId).getHaHost()
        if desthost is None:
            print('インスタンス{0}[{1}] (ホスト: {2}[{3}], ホストタグ: {4})のマイグレーション先ホストが見つかりません。'.format(i.id, i.name, i.host.id, i.host.name, ','.join(i.host.tags)))
            return
        if not checkPrerequisites(i, desthost):
            print('インスタンス {0} はマイグレーション前提条件を満たしていないため、スキップします。'.format(i))
            continue
        migration_info = MigrationInfo(desthost, i)
        for v in i.volumes:
            #ストレージプール選択
            p = StoragePool(c, v.poolId)
            if 'SYSTEM_DISK' == p.tag or 'DATA_DISK' == p.tag:
                print('インスタンス{0}[{1}] にアタッチされた ボリューム{2}[{3}] のタグが {4} に設定されているため、マイグレーションを行うことができません。'.format(i.id, i.name, v.id, v.name, p.tag))
                print('このボリュームのマイグレーションをスキップします。')
                continue
            if 'Fast' in p.name:
                migration_info.addVolumePoolPair(v, 'SYSTEM_DISK')
            elif 'Middle' in p.name:
                migration_info.addVolumePoolPair(v, 'DATA_DISK')
            else:
                raise RuntimeError('ストレージプール名からオファリングを推測できません: {0}'.format(v.name.encode('utf-8')))

        if len(migration_info.volumePoolPairList) == 0:
            print('インスタンス {0} にはマイグレーションするべきボリュームが存在しません。このインスタンスをスキップします。'.format(i))
            continue

        migration_info_list.append(migration_info)

    if len(migration_info_list) == 0:
        print('マイグレーションするインスタンスがありません。')
        return

    #デバッグ出力
    if args['debugmode']:
        for m in migration_info_list:
            debug_print("インスタンス{0}[{1}]: マイグレーション元ホストは {src_host_id}[{src_host_name}] (ホストタグ: {src_host_tag}), マイグレーション先ホストは {2}[{3}] (ホストタグ: {dest_host_tag}, メモリ使用率: {dest_host_mem_usage})です。".format(m.virtualMachine.id, m.virtualMachine.instanceName, m.destinationHost.id, m.destinationHost.name, src_host_id = m.virtualMachine.host.id, src_host_name = m.virtualMachine.host.name, src_host_tag = ','.join(m.virtualMachine.host.tags), dest_host_mem_usage = m.destinationHost.getMemoryUsage(), dest_host_tag = ','.join(m.destinationHost.tags)))
            for v in m.volumePoolPairList:
                volume = v[0]
                src_pool = StoragePool(c, volume.poolId)
                dest_pool_tag = v[1]
                debug_print('+++ボリューム{volume_id}[{volume_name}]: マイグレーション元ストレージプールは {src_pool_id}[{src_pool_name}] (ストレージプールタグ: {src_pool_tag}), マイグレーション先ストレージプールのタグは {dest_pool_tag} です。'.format(volume_id = volume.id, volume_name = volume.name, src_pool_id = volume.poolId, src_pool_name = src_pool.name, src_pool_tag = src_pool.tag, dest_pool_tag = dest_pool_tag))

    try:
        mshosts = MsHost.getAllUpManagementHosts(c)
    except:
        print("エラー: マネジメントサーバを取得できませんでした。")
        print('スタックトレース:')
        traceback.print_exc()
        return

    #マイグレーション予測時間 (秒/GB)
    migrationtimepergigabyte = getattr(eosl_migrate_config, 'MigrationTimePerGigabyteKvm', None)
    if migrationtimepergigabyte is None:
        migrationtimepergigabyte = (
            getattr(eosl_migrate_config, 'MigrationTimePerGigabyteKVM', None))
    if migrationtimepergigabyte is None:
        print('マイグレーション見積もり時間 (MigrationTimePerGigabyteKVM) が'
              '設定されませんでした。デフォルト値 (150秒/GB) を'
              '使用します。')
        migrationtimepergigabyte = 150

    cfm = confirmation(migration_info_list, mshosts, migrationtimepergigabyte)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    sshpass = dir_path + '/eosl_local_root/usr/local/bin/sshpass'
    if cfm is not None:
        session_id = createSessionId()
        cmdargs = []
        cmdargs.append(sshpass)
        cmdargs.append('-p')
        cmdargs.append(eosl_migrate_config.ApPassword)
        cmdargs.append('ssh')
        cmdargs.append('-oNumberOfPasswordPrompts=1')
        cmdargs.append('-oUserKnownHostsFile=/dev/null')
        cmdargs.append('-oStrictHostKeyChecking=no')
        cmdargs.append('{0}@{1}'.format(eosl_migrate_config.ApUser, mshosts[cfm].serviceIp))
        cmdargs.append(ApScreenCommandPath)
        cmdargs.append('-AmdS')
        cmdargs.append('eosl_migrate_kvm-' + session_id) #screen セッション名
        cmdargs.append(ApKvmToolPath)
        cmdargs.append('--apikey')
        cmdargs.append(eosl_migrate_config.ApiKey)
        cmdargs.append('--secretkey')
        cmdargs.append(eosl_migrate_config.SecretKey)
        cmdargs.append('--sessionid')
        cmdargs.append(str(session_id))
        cmdargs.append('--fsdir')
        cmdargs.append(eosl_migrate_config.ApStopFlagsDirectory)
        cmdargs.append('--dbhost')
        cmdargs.append(eosl_migrate_config.DbHost)
        cmdargs.append('--polling-interval')
        cmdargs.append(str(eosl_migrate_config.PollingInterval))
        cmdargs.append('--kvmpassword')
        cmdargs.append(eosl_migrate_config.KvmHostPassword)
        cmdargs.append('--sshpass')
        cmdargs.append(ApSshpassCommandPath)
        if eosl_migrate_config.DbPassword is not None:
            cmdargs.append('--dbpassword')
            cmdargs.append(eosl_migrate_config.DbPassword)
        if hasattr(eosl_migrate_config, 'MigrateSpeed'):
            cmdargs.append('--migratespeed')
            cmdargs.append(str(getattr(eosl_migrate_config, 'MigrateSpeed')))
        if hasattr(eosl_migrate_config, 'MigrateSpeedToHa'):
            cmdargs.append('--migratespeedtoha')
            cmdargs.append(str(getattr(eosl_migrate_config, 'MigrateSpeedToHa')))
        if migrationtimepergigabyte >= 0:
            cmdargs.append('--migration-time-per-gigabyte')
            cmdargs.append(str(migrationtimepergigabyte))
        if eosl_migrate_config.RemoteLogDirectory is not None and eosl_migrate_config.RemoteLogHost is not None and eosl_migrate_config.RemoteLogPassword is not None and eosl_migrate_config.RemoteLogUser is not None:
            cmdargs.append('--remotelogdirectory')
            cmdargs.append(eosl_migrate_config.RemoteLogDirectory)
            cmdargs.append('--remoteloghost')
            cmdargs.append(eosl_migrate_config.RemoteLogHost)
            cmdargs.append('--remoteloguser')
            cmdargs.append(eosl_migrate_config.RemoteLogUser)
            cmdargs.append('--remotelogpassword')
            cmdargs.append(eosl_migrate_config.RemoteLogPassword)
        #print(str(len(migration_info_list))) #デバッグ用
        for m in migration_info_list:
            cmdargs.append('--host')
            cmdargs.append(str(m.destinationHost.id))
            cmdargs.append('--virtualmachine')
            cmdargs.append(str(m.virtualMachine.id))
            for vpp in m.volumePoolPairList:
                cmdargs.append('--volume')
                cmdargs.append(str(vpp[0].uuid))
                cmdargs.append('--pooltag')
                cmdargs.append(vpp[1])
        if args['debugmode']:
            debug_print('リリース用コマンドライン:' + ' '.join(cmdargs))
        p = subprocess.Popen(cmdargs)
        p.wait()
        print("マイグレーション処理をバックグラウンドリリースしました。セッション番号: {0}".format(session_id))
main()
