#!/usr/bin/python2
# -*- coding:utf-8 -*-
"""EOSL移行支援ツールの設定ファイルです."""

#KCPS1商用環境 DBサーバ
DbHost = 'ckktky4-pcldb10'
DbPassword = None #パスワードが存在しない場合はNoneを指定してください。

#KCPS1商用APIキー・シークレットキー
ApiKey = 'Ryv-rE8Vrp_LD8frFUqd9v3ny23FzDvX9c1cl_I2-i05lW25-IKjyS_oM1hZKI5Qom7trEsnjVPPSusm2_vyNA'
SecretKey = 'aLV1FcuNzzUCfGLrsNe2Vq5DRo0fEjFYfMCEQoskRLCS9zW7M13c26Ktth9EkmukEWjNOlIGERVI7uP0DF4NFw'

#KCPS1商用環境 APサーバスクリプトの配置ディレクトリ
ApToolPath = '/home/ckkcl/scripts/tool/eosl_migration_tools_ap_vr'

#KCPS1商用環境 KVMホストへログインするためのパスワード
KvmHostPassword = 'Admin123ckk!'

#KCPS1商用環境 vCenterサーバへログインするためのユーザ名
VcenterServerUsername = 'ckk_adm'

#KCPS1商用環境 vCenterサーバへログインするためのパスワード
VcenterServerPassword = 'Admin123ckk!'

#1GBあたりのマイグレーション時間(秒) マイグレーション時間の予測に使用します。
MigrationTimePerGigabyteKVM = 10
MigrationTimePerGigabyteESXi = 14

#KCPS1商用環境 APサーバログイン時のユーザ
ApUser = 'ckkcl'

#KCPS1商用環境 APサーバログイン時のパスワード
ApPassword = 'CKK@6004'

#途中停止フラグディレクトリ (APサーバ内)
ApStopFlagsDirectory = '/home/ckkcl/scripts/tool/eosl_migration_tools_ap_vr/flags'

#ログ出力先ホスト
RemoteLogHost = 'ckktky4-vbat01'

#ログ出力先ディレクトリ
RemoteLogDirectory = '/home/ckkcl/script/tool/eosl_migration_tools_bat_vr/log'

#ログ出力先ホストに接続するときのユーザ
RemoteLogUser = 'ckkcl'

#ログ出力先ホストに接続するときのパスワード
RemoteLogPassword = 'CKK@6004'

#マイグレーション状態確認間隔 (秒)
PollingInterval = 60

#マイグレーション速度(KVM) (MiB/s) → virsh migrate-setspeedする値
MigrateSpeed = 100
MigrateSpeedToHa = 128
