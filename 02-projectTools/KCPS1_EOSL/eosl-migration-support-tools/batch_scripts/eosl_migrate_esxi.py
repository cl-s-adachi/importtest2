#!/usr/bin/python2
# -*- coding: utf-8 -*-
# This file is part of EOSL Migration Support Tool.
# Copyright (c) 2018 Creationline and/or its affiliates. All rights reserved.

# EOSL Migration Support Tool is licensed under the terms of the GPLv2
# <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""このファイルは、インスタンス・ディスクリストファイルを読み込み, APサーバにSSHでログインしてマイグレーションスクリプト eosl_migrate_ap_esxi.pyを実行するスクリプトです."""
from __future__ import print_function
import sys
import datetime
import os
import traceback
import imp
from collections import namedtuple

import mysql.connector

from ap_batch_common.VirtualMachine import VirtualMachine
from ap_batch_common.Volume import Volume
from ap_batch_common.StoragePool import StoragePool
from ap_batch_common.MsHost import MsHost
from ap_batch_common.Cluster import Cluster
from ap_batch_common.Utility import remoteFileExists
from ap_batch_common.SshClient import SshClient
from eosl_migrate_batch_common import IncompatibleHypervisorError
from eosl_migrate_batch_common import InvalidStoragePoolTagError
from eosl_migrate_batch_common import createSessionId

def parse_args():
    """
    引数をパースし、dictとして返します.

    Parameters
    None

    Returns
    -------
    dict
        ['configfile']: str
            設定ファイル名 (デフォルト: eosl_migrate_config.py)
        ['db']: namedtuple
            host: str
                ホスト名
            password: str
                パスワード (None: パスワードなし)
        ['debug']: bool
            デバッグするかどうか
        ['dryrun']: bool
            ドライランするかどうか
        ['instancenamelist']: list<str>
            インスタンス名のリスト
        ['instancenamelistfile']: str
            インスタンス名リストのファイル名
        ['usage']: bool
            使用法を表示するかどうか
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    db_type = namedtuple('db_type', ('host', 'password'))
    ret = {}
    ret['configfile'] = dir_path + '/eosl_migrate_config.py'
    ret['db'] = db_type(host='', password=None)
    ret['debug'] = False
    ret['dryrun'] = False
    ret['instancenamelist'] = []
    ret['instancenamelistfile'] = None
    ret['usage'] = False
    if len(sys.argv) < 2:
        ret['usage'] = True
        return ret
    i = 1
    while(1):
        if i == len(sys.argv):
            break
        if sys.argv[i] == '-h' or sys.argv[i] == '--dbhost':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('DBのホスト名を指定してください。')
            ret['db'] = db_type(host=sys.argv[i], password=ret['db'].password)
        elif sys.argv[i] == '-p' or sys.argv[i] == '--dbpassword':
            i += 1
            if i == len(sys.argv):
                raise RuntimeError('DBのパスワードを指定してください。')
            ret['db'] = db_type(host=ret['db'].host, password=sys.argv[i])
        elif sys.argv[i] == '--dryrun':
            ret['dryrun'] = True
        elif sys.argv[i] == '--debug':
            ret['debug'] = True
        elif sys.argv[i] == '-f':
            i += 1
            ret['configfile'] = sys.argv[i]
        elif sys.argv[i] == '-n':
            i += 1
            ret['instancenamelist'].append(sys.argv[i])
        else:
            ret['instancenamelistfile'] = sys.argv[i]
        i += 1
    return ret

def getInstancesAndVolumesFromFile(connection, filename):
    """
    DBに接続し, インスタンスリストファイルからVolumeとVirtualMachineの'インスタンス'のリストを作成します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()で返されるmysqlconnection
    filename: str
        インスタンス・ボリュームリストファイル名

    Returns
    -------
    instances_and_volumes: list
        VolumeとVirtualMachineのインスタンスが格納されたリスト
    """
    ret = []
    with open(filename) as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()

            #インスタンス, Virtual Router, SSVM, CPVM
            if l.startswith('i') or l.startswith('r') or l.startswith('s') or l.startswith('v'):
                ret.append(VirtualMachine.createInstanceByInstanceName(connection, l))
            else:
                ret.append(Volume.createInstanceByUuid(connection, l))
    return ret

def get_instances_and_volumes_by_name_list(connection, namelist):
    """
    DBに接続し、インスタンス名/ボリューム名のリストからVolumeとVirtualMachineの'インスタンス'のリストを作成します.

    Parameters
    ----------
    connection: MySQLConnection
        mysql.connector.connect()で返されるmysqlconnection
    namelist: list<str>
        インスタンス名・ボリューム名のリスト

    Returns
    -------
    instances_and_volumes: list
        VolumeとVirtualMachineのインスタンスが格納されたリスト
    """
    ret = []
    for name in namelist:
        #インスタンス, Virtual Router, SSVM, CPVM
        if (name.startswith('i') or name.startswith('r')
            or name.startswith('s') or name.startswith('v')):
            ret.append(VirtualMachine.createInstanceByInstanceName(connection, name))
        else:
            ret.append(Volume.createInstanceByUuid(connection, name))
    return ret

def checkReady(instance_or_volumes):
    """
    指定したインスタンスに紐づいたすべてのボリュームと, 指定したボリュームがすべてReady状態か確認します.

    Parameters
    ----------
    instance_or_volumes: list
        VirtualMachineおよびVolumeのインスタンスからなるリスト.

    Returns
    -------
        指定した仮想マシンにアタッチされているすべてのボリュームがReady状態であり、
        指定したボリュームがすべてReady状態ならばTrue.それ以外ならばReadyでないVolume.
    """
    for i, j in enumerate(instance_or_volumes):
        if isinstance(j, VirtualMachine):
            for k, l in enumerate(j.volumes):
                if l.state != 'Ready':
                    return l
        elif isinstance(j, Volume):
            if j.state != 'Ready':
                return j
        else:
            raise RuntimeError('引数のタプルにVirtualMachineまたはVolume以外の型の要素が含まれています:' + 
                        type(j))
    return None

def getVmWareVolumeMigrationDestinationStoragePoolTag(c, v):
    """VMwareボリュームのマイグレーション先ストレージプール(プライマリストレージ)のタグを取得します.

    Parameters
    ----------
    c: MySQLConnection
        mysql.connector.connect()の戻り値
    v: Volume
        マイグレーションするVolume.

    Returns
    -------
    str
        マイグレーション先ストレージプール(プライマリストレージ)のタグ (SYSTEM_DISK or DATA_DISK).
    """
    attachedTo = None if v.instanceId is None else VirtualMachine(c, v.instanceId)
    if v.hypervisorType != 'VMware':
        raise IncompatibleHypervisorError('ボリューム{0}[{1}] (アタッチ先: {2}[{3}])のハイパーバイザはVMwareではありません: {4}'.format(v.id, v.name, '<アタッチされていません>' if attachedTo is None else attachedTo.id, '' if attachedTo is None else attachedTo.name, v.hypervisorType))
    p = StoragePool(c, v.poolId)
    if p.tag == 'SYSTEM_DISK' or p.tag == 'DATA_DISK':
        raise InvalidStoragePoolTagError('ボリューム {0} (アタッチ先: {1}[{2}])のタグが {3} に設定されているため、マイグレーションを行うことができません。\n'.format(str(v), '<アタッチされていません>' if attachedTo is None else attachedTo.id, '' if attachedTo is None else attachedTo.name, p.tag) +
        '(1) このボリュームはすでに移行が完了していないか\n' +
        '(2) 旧ストレージプールのタグを変更し忘れていないか\n' +
        'を確認してください。')

    if 'Fast' in p.name:
        return 'SYSTEM_DISK'
    elif 'Middle' in p.name:
        return 'DATA_DISK'
    else:
        raise RuntimeError('ストレージプール名からオファリングを推測できません: ' + v.name.encode('utf-8'))

def confirmation(vms, aps, migrationtimepergigabyte):
    """
    バックグランドリリースの実行前確認を出力します.

    Parameters
    ----------
    vms: list
        マイグレーションするVirtualMachineとVolumeのインスタンスを格納したリスト
    aps: list
        マイグレーションセッションを実行するAPサーバ (MsHost) のリスト
    migrationtimepergigabyte: int
        マイグレーション見積もり時間 (秒/GB)

    Returns
    -------
    マイグレーションを実行する場合は、マイグレーションセッションを実行するAPサーバのapsでのインデックス番号.
    マイグレーションを実行しない場合はNone.
    """
    print("【ESXi移行支援ツール マイグレーションコマンド実行前確認】")
    totalEstimatedMigrationTime = datetime.timedelta()
    for vm in vms:
        td = datetime.timedelta(seconds=vm.getEstimatedMigrationTime(migrationtimepergigabyte))
        if isinstance(vm, VirtualMachine):
            printstate = '不明'
            if vm.state == 'Running':
                printstate = 'オンライン'
            elif vm.state == 'Stopped':
                printstate = 'オフライン'
            print("インスタンス: {0}({1}, {2})     マイグレーション予想時間: {3}".format(vm.instanceName, vm.serviceType, printstate, td))
        elif isinstance(vm, Volume):
            print('ボリューム: {0}             マイグレーション予想時間: {1}'.format(str(vm), td))
        totalEstimatedMigrationTime += td
    print("")
    print("マイグレーション予想時間合計: {0}".format(totalEstimatedMigrationTime))
    print("")
    print("0) マイグレーションの中止")
    for i, ap in enumerate(aps):
        print("{0}) {1}".format(i + 1, ap.name))
        continue
    while True:
        try:
            apindex = int(raw_input("マイグレーションをバックグラウンドリリースするAPサーバを選択してください(1-{0}) >".format(len(aps))))
            if apindex >= 0 and apindex <= len(aps):
                break
        except ValueError:
            pass
        print('入力範囲外です。')
        print('')
    print("")
    if apindex == 0:
        return None
    print("実行サーバ: {0}".format(aps[apindex - 1].name))
    print("")
    yesno = None
    while(1):
        yesno = raw_input("マイグレーションを実行しますか(y/n)? >").strip()
        if yesno != 'y' and yesno != 'n':
            print("yまたはnと入力してください。")
        else:
            break
    return apindex - 1 if yesno == 'y' else None
    
def main():
    """スクリプトのエントリポイントです."""
    args = parse_args()
    if args['usage']:
        print("使用法: {0} [-f 構成ファイル名] <インスタンスのリストファイル> [-h|--dbhost] [-p|--dbpassword] [-n インスタンス名]...".format(sys.argv[0]))
        print("")
        print("リストファイルで指定されたVMwareインスタンスのストレージおよびデタッチされたストレージを移行します。")
        return

    try:
        eosl_migrate_config = imp.load_source('', args['configfile'])
    except Exception:
        traceback.print_exc()
        sys.exit('Error: 設定ファイル {0} をインポートできませんでした。'.format(args['configfile']))

    if args['instancenamelistfile'] is None and (args['instancenamelist'] is None or len(args['instancenamelist'])) == 0:
        print('インスタンスを指定してください。')
        return

    dbhost = getattr(eosl_migrate_config, 'DbHost', None)
    dbpassword = getattr(eosl_migrate_config, 'DbPassword', None)
    dbhost = args['db'].host if dbhost is None else dbhost
    dbpassword = args['db'].password if dbpassword is None else dbpassword
    c = None #DB接続
    try:
        c = mysql.connector.connect(user='root', host=dbhost, password=dbpassword, database='cloud', charset='utf8')
    except Exception:
        print('データベース {0} への接続中にエラーが発生しました: {1}'.format(dbhost, sys.exc_info()[1]))
        print('(1) 接続先データベースホスト名 (DbHost) が間違っていないか')
        print('(2) データベースへ接続できる状態か')
        print('を確認してください。')
        return

    try:
        instances = []
        instances = get_instances_and_volumes_by_name_list(c, args['instancenamelist'])
        if args['instancenamelistfile'] is not None:
            instances.extend(getInstancesAndVolumesFromFile(c, args['instancenamelistfile']))

        check_ready = checkReady(instances)
        if check_ready is not None:
            print('状態がReadyでないボリュームが含まれています: {0}[{1}]'.format(check_ready.id, check_ready.name))
            return
        
        mshosts = []
        migration_info = [] #辞書のリスト。
        #destHost:マイグレーション先HA Host または None
        #instance: マイグレーションするInstance または None
        #volumePoolTagList: ボリュームとそのマイグレーション先ストレージのタグのタプルのリスト
        for i in instances:
            try:
                if isinstance(i, Volume):
                    if i.instanceId is not None:
                        print('エラー: ボリューム {0} は インスタンス {1} にアタッチされているため、インスタンスのマイグレーションが必要です。このボリュームの代わりにインスタンス {1} をリストファイルに指定してください。'.format(i, i.instanceId))
                        return
                    dic = {} #ボリューム単体はマイグレーションを伴わない。
                    dic['destHost'] = None
                    dic['instance'] = None
                    dic['volumePoolTagList'] = [(i, getVmWareVolumeMigrationDestinationStoragePoolTag(c, i), )]
                    migration_info.append(dic)
                else:
                    if i.numberOfVmSnapshots > 0:
                        raise RuntimeError('インスタンス {0} に取得済みのVMスナップショットが存在するため、マイグレーションできません。'.format(i))
                    dic = {}
                    dic['destHost'] = None
                    if i.host is not None:
                        hahost = Cluster(c, i.host.clusterId).getHaHost() #オンラインのときだけインスタンスをHAホストに移す
                        if hahost is None:
                            raise RuntimeError('インスタンス {0} の移行先HAホストが見つかりませんでした。'.format(i))
                        dic['destHost'] = hahost

                    dic['instance'] = i
                    dic['volumePoolTagList'] = []
                    for v in i.volumes:
                        try:
                            dic['volumePoolTagList'].append((v, getVmWareVolumeMigrationDestinationStoragePoolTag(c, v), ))
                        except InvalidStoragePoolTagError:
                            pass #マイグレーション済みの場合でもエラーとしない

                    if len(dic['volumePoolTagList']) == 0:
                        raise RuntimeError('インスタンス {0} にはマイグレーションできるボリュームがありません。\n'.format(i) + 
                        'このインスタンスはすでにマイグレーションが完了している可能性があります。')
                    migration_info.append(dic)
            except:
                print('{0} {1} のマイグレーション先ストレージプールを取得中にエラーが発生しました:\n{2}'.format('ボリューム' if isinstance(i, Volume) else 'インスタンス', str(i), sys.exc_info()[1]))
                print('スタックトレース:')
                traceback.print_exc()
                return

        #マネジメントサーバリスト
        mshosts = MsHost.getAllUpManagementHosts(c)

        if args['dryrun']: #デバッグ用
            print('***** テスト実行モード  *****')
            cluster_set = set()
            for m in migration_info:
                for vpt in m['volumePoolTagList']:
                    cluster_set.add(StoragePool(c, vpt[0].poolId).clusterId)

            cluster_list = [Cluster(c, x) for x in cluster_set]

            for clu in cluster_list:
                print('クラスタ {0} のSYSTEM_DISKプール:'.format(clu))
                for p in sorted(StoragePool.getSystemDiskPoolsInCluster(c, clu.id), key=lambda x: x.getUsage()):
                    print('{0} 使用率 {1}'.format(str(p), p.getUsage()))
                print('クラスタ {0} のDATA_DISKプール:'.format(clu))
                for p in sorted(StoragePool.getDataDiskPoolsInCluster(c, clu.id), key=lambda x: x.getUsage()):
                    print('{0} 使用率 {1}'.format(str(p), p.getUsage()))
                print('')

            print('マイグレーション先:')
            for m in migration_info:
                for vpt in m['volumePoolTagList']:
                    srcP = StoragePool(c, vpt[0].poolId)
                    print('ボリューム {0}: {1} -> {2} タグのストレージプール(プライマリストレージ)'.format(str(vpt[0]), str(srcP), str(vpt[1])))
            return
    except mysql.connector.Error:
        print('データベースアクセス中にエラーが発生しました: {0}'.format(sys.exc_info()[1]))
        return
    except Exception:
        print('予期していないエラーが発生しました: {0}'.format(sys.exc_info()[1]))
        print('スタックトレース:')
        print(traceback.format_exc())
        return
    finally:
        c.close() #DB接続を閉じる

    #マイグレーション予測時間 (秒/GB)
    migrationtimepergigabyte = getattr(eosl_migrate_config, 'MigrationTimePerGigabyteEsxi', None)
    if migrationtimepergigabyte is None:
        migrationtimepergigabyte = (
            getattr(eosl_migrate_config, 'MigrationTimePerGigabyteESXi', None))
    if migrationtimepergigabyte is None:
        print('マイグレーション見積もり時間 (MigrationTimePerGigabyteESXi) が'
              '設定されませんでした。デフォルト値 (70秒/GB) を'
              '使用します。')
        migrationtimepergigabyte = 70

    cfm = confirmation(instances, mshosts, migrationtimepergigabyte)
    if cfm is None:
        return
    chosen_ap_server = mshosts[cfm]

    ap_screen_command_path                       = eosl_migrate_config.ApToolPath + '/eosl_local_root/usr/bin/screen'
    ap_sshpass_command_path                      = eosl_migrate_config.ApToolPath + '/eosl_local_root/usr/local/bin/sshpass'
    ap_immediate_cancel_request_script_path      = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_immediate_cancel_request.py'
    ap_immediate_cancellation_status_script_path = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_immediate_cancellation_status.py'
    ap_immediate_cancel_task_script_path         = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_immediate_cancel_task.py'
    ap_abort_migration_session_script_path       = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_abort_migration_session.py'
    ap_esxi_script_path                          = eosl_migrate_config.ApToolPath + '/eosl_migrate_ap_esxi.py'
    if cfm is not None:
        #AP側ツール存在確認
        try:
            if not remoteFileExists(ap_esxi_script_path, eosl_migrate_config.ApUser, mshosts[cfm].serviceIp, eosl_migrate_config.ApPassword):
                print('APサーバ内に実行ファイル {0} が見つかりませんでした。'.format(ap_esxi_script_path))
                print('マイグレーション処理を行うことができません。')
                return
        except Exception:
            print('APサーバ側スクリプトの存在確認中にエラーが発生しました: {0}'.format(sys.exc_info()[1]))
            print('* ログインユーザ名 (ApUser) を間違えていないか')
            print('* バッチサーバ <-> APサーバの通信に問題がないか')
            print('を確認してください。')
            return
        session_id = createSessionId()
        cmdargs = []
        cmdargs.append(ap_screen_command_path)
        cmdargs.append('-AmdS')
        cmdargs.append('eosl_migrate_esxi-' + session_id) #screen セッション名
        cmdargs.append(ap_esxi_script_path)
        cmdargs.append('--apikey')
        cmdargs.append(eosl_migrate_config.ApiKey)
        cmdargs.append('--secretkey')
        cmdargs.append(eosl_migrate_config.SecretKey)
        cmdargs.append('--sessionid')
        cmdargs.append(session_id)
        cmdargs.append('--fsdir')
        cmdargs.append(eosl_migrate_config.ApStopFlagsDirectory)
        cmdargs.append('--dbhost')
        cmdargs.append(eosl_migrate_config.DbHost)
        cmdargs.append('--polling-interval')
        cmdargs.append(str(eosl_migrate_config.PollingInterval))
        if eosl_migrate_config.DbPassword is not None:
            cmdargs.append('--dbpassword')
            cmdargs.append(eosl_migrate_config.DbPassword)
        if migrationtimepergigabyte >= 0:
            cmdargs.append('--migration-time-per-gigabyte')
            cmdargs.append(str(migrationtimepergigabyte))
        if eosl_migrate_config.RemoteLogDirectory is not None and eosl_migrate_config.RemoteLogHost is not None and eosl_migrate_config.RemoteLogPassword is not None and eosl_migrate_config.RemoteLogUser is not None:
            cmdargs.append('--remotelogdirectory')
            cmdargs.append(eosl_migrate_config.RemoteLogDirectory)
            cmdargs.append('--remoteloghost')
            cmdargs.append(eosl_migrate_config.RemoteLogHost)
            cmdargs.append('--remoteloguser')
            cmdargs.append(eosl_migrate_config.RemoteLogUser)
            cmdargs.append('--remotelogpassword')
            cmdargs.append(eosl_migrate_config.RemoteLogPassword)
        if eosl_migrate_config.VcenterServerUsername is not None and eosl_migrate_config.VcenterServerPassword is not None:
            cmdargs.append('--vcusername')
            cmdargs.append(eosl_migrate_config.VcenterServerUsername)
            cmdargs.append('--vcpassword')
            cmdargs.append(eosl_migrate_config.VcenterServerPassword)

        for m in migration_info:
            cmdargs.append('--host')
            cmdargs.append(str(m['destHost'].id) if m['destHost'] is not None else 'none')
            cmdargs.append('--virtualmachine')
            cmdargs.append(str(m['instance'].id) if m['instance'] is not None else 'none')
            for vpt in m['volumePoolTagList']:
                cmdargs.append('--volume')
                cmdargs.append(str(vpt[0].id))
                cmdargs.append('--pooltag')
                cmdargs.append(vpt[1])

        #APサーバ情報
        cmdargs.append('--apuser')
        cmdargs.append(eosl_migrate_config.ApUser)
        cmdargs.append('--appassword')
        cmdargs.append(eosl_migrate_config.ApPassword)

        if args['debug']:
            print('[DEBUG] 実行コマンド: {0}'.format(' '.join(cmdargs)))
        sc = SshClient(eosl_migrate_config.ApUser, mshosts[cfm].serviceIp, eosl_migrate_config.ApPassword,
                os.path.dirname(os.path.realpath(__file__)) + "/eosl_local_root/usr/local/bin/sshpass",
                                [('NumberOfPasswordPrompts', 1), ('UserKnownHostsFile', '/dev/null'), ('StrictHostKeyChecking', 'no'),])
        try:
            sc.run_simple(cmdargs)
            print('マイグレーション処理をバックグラウンドリリースしました。セッション番号:{0}'.format(session_id))
        except:
            print('マイグレーション処理のバックグランドリリースに失敗しました。')
            print('スタックトレース:')
            print(traceback.format_exc())    
main()
