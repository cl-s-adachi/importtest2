#!/usr/bin/python
# -*- coding: UTF-8 -*-

import argparse
import csv
import logging
import os
import re
import subprocess
import sys
import time

import pretty_table

# 検証環境では無い環境で実行する場合はテストのみの為、mysqlモジュールはインポートしない。
try:
    import mysql.connector
except:
    pass

# 共通設定
## DBサーバ
DB_HOST = '172.22.132.84'
## ログディレクトリ
LOGS = 'logs/'


class VmInstance:
    r"""
    システムVM・VRの情報管理用クラス

    Parameters
    ----------
    name: str
        vm_instance.name
    state: str
        vm_instance.state
    private_ip_address
        vm_instance.private_ip_address。このIPアドレスでAPサーバからシステムVM・VRにSSH接続をする。
    eject_status: boolean, None
        True: eject済み, False: eject未実施, None: 未確認

    Attributes
    ----------
    name: str
        vm_instance.name
    state: str
        vm_instance.state
    private_ip_address
        vm_instance.private_ip_address。このIPアドレスでAPサーバからシステムVM・VRにSSH接続をする。
    eject_status: boolean, None
        True: eject済み, False: eject未実施, None: 未確認
    """

    def __init__(self, name, state, private_ip_address, eject_status=None):
        self.name = name
        self.state = state
        self.private_ip_address = private_ip_address
        self.eject_status = eject_status

    def __eq__(self, other):
        r"""
        ツール実行時には不要だが、テストコード実行時に必要の為に作成
        """

        name = self.name == other.name
        state = self.state == other.state
        private_ip_address = self.private_ip_address == other.private_ip_address
        eject_status = self.eject_status == other.eject_status
        return name and state and private_ip_address and eject_status


def load_csv_as_list(filename, delimiter=','):
    r"""
    CSVファイルを読み込んでリストで返す。

    Parameters
    ----------
    filename: str
        ファイル名(フールパス可能)
    delimiter: str
        区切りに使う文字
        デフォルトは','

    Return 
    ------
    row: list
        ファイルの各行を1行ずつ返す。 (修正必要)

    """
    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=delimiter, quoting=csv.QUOTE_NONE)
        return [ row for row in reader ]


def exec_mysql(params):
    r"""
    mysqlコマンドを実行します。

    Parameters
    ----------
    params: dict
        mysqlコマンド実行時に必要な引数

    Returns
    -------
    SQL文実行結果: list, boolean
        Select分の場合: [ ['1行目1列目', '1行目2列目', ... ], ['2行目1列目', '2行目2列目', ... ], ... ]
        Update分の場合: True
    """
    
    # 実行するSQL文がSelectかUpdateかを確認する。
    operation = params['sql'].split()[0].lower()

    # selectとupdateと処理が若干違うので分岐を作る。
    logging.debug('DBから情報を取得します。')
    logging.debug('DBサーバ: ' + params['db_host'])
    logging.debug('実行するSQL: ' + str(params['sql']))
    if operation == 'select':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        result = cursor.fetchall()
        logging.debug('SQL実行結果: ' + repr(result))

        cursor.close()
        cnx.close()

        # tupleをlistに、Noneをstrの'NULL'にする。
        return manipulate_mysql_result(result)

    elif operation == 'update':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        cnx.commit()

        cursor.close()
        cnx.close()

        return True

    else:
        raise Exception('SQL文がSelect、もしくはUpdateではありません。指定されたのは: ' + operation)
        sys.exit(1)


def manipulate_mysql_result(raw_result):
    r"""
    mysql実行結果の各行をlistに変換し、Noneは'NULL'に変換する。

    Parameters
    ----------
    raw_result: list
        mysql.connectorでのfetchall()の結果

    Returns
    -------
    上記説明参照: result

    """

    result = []
    for i in raw_result:
        result.append(list(i))

    for i in range(len(result)):
        for j in range(len(result[i])):
            if result[i][j] == None:
                result[i][j] = 'NULL'
            if isinstance(result[i][j], int):
                result[i][j] = str(result[i][j])

    return result


def list_to_where_in(ids, double_quote=False):
    r"""
    複数のID情報(数字)を持っている変数をwhere文節向けに変換する。

    Parameters
    ----------
    ids: list
        変換するIDの一覧

    Returns
    -------
    変換されたlist: str
        'ID1,ID2,ID3, ...'

    Notes
    -----
    文字列には対応させるには``double_quote``を``True``にする必要がある。

    """

    result = ''

    if isinstance(ids[0], list):
        result = ','.join([x[0] for x in ids])
    else:
        # リストの要素がリストでなければ、文字列でみなす。
        result = ','.join(ids)

    if double_quote == False:
        return result
    else:
        result = re.sub(',', '","', result)
        result = re.sub(r'^', '"', result)
        result = re.sub(r'$', '"', result)
        return result

def continue_or_stop(message):
    r"""
    ツールを終了するか確認する。

    質問にy以外の文字列を入力した場合はスクリプトを終了させる。

    Parameters
    ----------
    message: str
        質問内容

    Returns
    -------
    True: boolean
        質問にyを入力するとTrueを返す。
    False: boolean
        質問にy以外を入力するとFalseを返す。

    """

    answer = ''

    answer = raw_input(message + '[y/N] ')
    logging.debug('ユーザに次の質問をし、' + answer + 'の答えを受けました: ' + message)

    if answer == 'y':
        return True
    else:
        return False

def vm_instance_to_list_for_table(vm_instance, include_eject_status=False):
    r"""
    VmInstanceをリストに変換に変換する。

    Parameters
    ----------
    vm_instance: VmInstance
        インスタンス情報
    include_eject_status: boolean
        リスト化する際にVmInstance.eject_statusを含めるかどうか
    
    Returns
    -------
    リスト化されたVmInstance: list
        include_eject_statusがFalseの場合
            [ ['<name>', '<state>', '<private_ip_address>'], ...]
        include_eject_statusがTrueの場合
            [ ['<name>', '<state>', '<private_ip_address>', '<eject_status>'], ...]
    """

    result = []
    
    result.append(vm_instance.name)
    result.append(vm_instance.state)
    result.append(vm_instance.private_ip_address)
    if include_eject_status:
        result.append(vm_instance.eject_status)
    
    return result

def search_in_list(listA, listB):
    r"""
    listBにないlistAの要素を洗い出す。

    Parameters
    ----------
    listA: list
    listB: list

    Returns
    -------
    listBにないlistAの要素: list

    """

    not_exist = []
    toggle = False

    for i in listA:
        toggle = False
        for j in listB:
            if i == j:
                toggle = True
                break
        if toggle == False:
            not_exist.append(i)

    return not_exist


class EjectModel:
    r"""
    データ取得などを担当するクラス

    Parameters
    ----------
    なし

    Attributes
    ----------
    なし

    """

    def get_vm_infos(self, vms):
        r"""
        インスタンスの情報を取得する。

        Parameters
        ----------
        vms: list
            [ <インスタンス名>, ... ]
        
        Returns
        -------
        VmInstanceで整理されたリスト: list
            [ VmInstance, ... ]
        """
        
        vms_quoted = list_to_where_in(vms, True)
        result = []

        db_params = {
            'db_host' : DB_HOST,
            'db_user' : 'root',
            'database' : 'cloud',
            'sql' : 'select name,state,private_ip_address from vm_instance where removed is null and name in (' + vms_quoted + ');',
        }
        vms_raw_result = exec_mysql(db_params)

        for vm in vms_raw_result:
            result.append(VmInstance(vm[0], vm[1], vm[2]))

        return result

    def load_vms(self, filename):
        r"""
        一覧ファイルからインスタンス名を読み込む。

        Parameters
        ----------
        filename: str
            ファイルのパス
        
        Returns
        -------
        インスタンス名のリスト: list
            [ <インスタンス名>, ... ]
        """

        raw_data = load_csv_as_list(filename)
        return [ vm[0] for vm in raw_data ]
        
    def exec_cmd_on_systemvm(self, ip_address, cmd):
        r"""
        システムVM・VRでコマンドを実行する。

        Parameters
        ----------
        ip_address: str
            システムVM・VRのIPアドレス
        cmd: str
            実行するコマンド

        Returns
        -------
        実行結果: tuple
            ( <標準出力>(str), <標準エラー>(str), <終了コード(int))
        """

        cmd = [
            'ssh',
            '-i', '/var/cloudstack/management/.ssh/id_rsa',
            '-o', 'StrictHostKeyChecking=no',
            '-o', 'UserKnownHostsFile=/dev/null',
            '-p', '3922',
            ip_address,
            cmd
        ]

        # コマンドを実行する。
        logging.debug('次のコマンドを実行します: ' + str(' '.join(cmd)))
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()

        # コマンド実行結果を回収する。
        p_stdout, p_stderr = p.communicate()

        logging.debug('実行結果(標準出力): ' + p_stdout)
        logging.debug('実行結果(標準エラー): ' + p_stderr)
        logging.debug('実行結果(終了コード): ' + str(p.returncode))

        return (p_stdout, p_stderr, p.returncode)


class EjectView:
    r"""
    出力を単横するクラス

    Parameters
    ----------
    なし

    Attributes
    ----------
    なし

    """

    def check_cd_before(self, vms):
        r"""
        systemvm.iso挿入確認の対象確認用ビュー
        
        Parameters
        ----------
        vms: list
            [ ['name', 'state', 'private_ip_address' ],
            [<vm_instance.name>, <vm_instance.state>, <vm_instance.private_ip_address>],
            ... ]
        """

        print '以下のシステムVM・VRのsystemvm.isoの状態を確認します。'

        # 対象を出力
        print pretty_table.create_table(vms)
        print ''
    
    def check_cd_after(self, vm_eject_status):
        r"""
        systemvm.iso挿入確認結果確認用ビュー
        
        Parameters
        ----------
        vms: list
            [ ['name', 'state', 'private_ip_address', 'eject_status' ],
            [<vm_instance.name>, <vm_instance.state>, <vm_instance.private_ip_address>, <確認結果>],
            ... ]
        """
        print '確認結果は以下の通りです。'
        print '※True: eject済み, False: eject未実施, None: 未確認'

        # 対象を出力
        print pretty_table.create_table(vm_eject_status)
        print ''
        
    def check_vms_running(self, vms):
        r"""
        起動中の対象確認用ビュー
        
        Parameters
        ----------
        vms: list
            [ ['name', 'state', 'private_ip_address' ],
            [<vm_instance.name>, <vm_instance.state>, <vm_instance.private_ip_address>],
            ... ]
        """
        print '対象は以下の起動中の対象のみとなります。'

        # 対象を出力
        print pretty_table.create_table(vms)
        print ''

    def check_eject_result(self, vms):
        r"""
        eject実行結果確認用ビュー

        Parameters
        ----------
        vms: list
            [ ['name', 'return_code'], [<インスタンス名>, <終了コード>], ... ]
        """

        print 'eject実行結果は以下の通りです。'

        # 対象を出力
        print pretty_table.create_table(vms)
        print ''

    def vm_not_exist(self, vms):
        r"""
        存在しない対象を出力するビュー
        
        Parameters
        ----------
        vms: list
            [ ['name'], ['対象名'], ... ]
        """

        print '以下の対象は存在しませんでした。'
        print pretty_table.create_table(vms)
        print ''

class EjectController:
    r"""
    ツールを制御するクラス

    Parameters
    ----------
    args: namespace
        argparseモジュールで取得した、コマンド実行時に指定したパラメータ

    Attributes
    ----------
    view: EjectView
        出力を単横するクラス
    model: EjectModel
        データ取得などを担当するクラス
    args: namespace
        argparseモジュールで取得した、コマンド実行時に指定したパラメータ
    vm_names: list
        実行対象の名前。 [ 'インスタンス名', ... ]
    vms: list
        VmInstance化した実行対象 [ VmInstance, ... ]

    """

    def __init__(self, args):
        self.view = EjectView()
        self.model = EjectModel()
        # コマンド実行時のパラメータ
        self.args = args
        # 一覧ファイルの対象を格納する為の変数
        self.vm_names = []
        # 対象の情報を格納する為の変数
        self.vms = []
    
    def run(self):
        r"""
        ツール実行に必要な情報を取得し、ツール実行時のパラメータによって実行内容を分岐する。
        """
        # 対象を読み込む。
        self.vm_names = self.model.load_vms(self.args.file)
        # CSDBから対象の情報を取得する。
        self.vms = self.model.get_vm_infos(self.vm_names)

        # CSDBに存在しない対象を出力する。
        vm_instance_names = [ x.name for x in self.vms ]
        not_exist = [ ['name'] ]
        [ not_exist.append([x]) for x in search_in_list(self.vm_names, vm_instance_names) ]
        
        if not_exist != [ ['name'] ]:
            self.view.vm_not_exist(not_exist)

        # 分岐
        if self.args.action == 'check':
            return self.check_cd()
        elif self.args.action == 'eject':
            return self.eject_cd()
        else:
            print '次のようなactionは存在しません: ' + str(self.args.action)
            return False

    def check_cd(self):
        r"""
        systemvm.isoの挿入状態を確認する。
        """

        result = []

        # 対象および実行内容を出力する。
        table_before = []
        table_before.append(['name', 'state', 'ip_address'])
        for vm in self.vms:
            table_before.append(vm_instance_to_list_for_table(vm))

        self.view.check_cd_before(table_before)

        # 実行中の対象のみ洗い出す。
        vms_running = [ vm for vm in self.vms if vm.state == 'Running']
        table_before = []
        table_before.append(['name', 'state', 'ip_address'])
        for vm in vms_running:
            table_before.append(vm_instance_to_list_for_table(vm))

        # ejectしているか確認する。
        for vm in vms_running:
            print str(vm.name) + 'の確認中です。'
	    try:
		(stdout, stderr, returncode) = self.model.exec_cmd_on_systemvm(vm.private_ip_address, 'blkid /dev/sr0')
		if returncode == 0:
		    vm.eject_status = False
		else:
		    vm.eject_status = True
            except:
	        print '対象\'' + str(vm.name) + '\'でのコマンド実行が失敗しました。'

            result.append(vm)
        print ''

        # 確認結果を出力する。
        table_after = []
        table_after.append(['name', 'state', 'ip_address', 'eject_status'])
        for vm in self.vms:
            table_after.append(vm_instance_to_list_for_table(vm, True))

        self.view.check_cd_after(table_after)

        return True
        
    def eject_cd(self):
        r"""
        systemvm.isoをejectする。
        """

        # 実行中の対象のみ洗い出す。
        vms_running = [ vm for vm in self.vms if vm.state == 'Running']
        table_before = []
        table_before.append(['name', 'state', 'ip_address'])
        for vm in vms_running:
            table_before.append(vm_instance_to_list_for_table(vm))

        self.view.check_vms_running(table_before)

        # ejectするかユーザに確認する。
        if not continue_or_stop('上記対象からsystemvm.isoをejectしますか?'):
            return False
        print ''

        # ejectする。
        result = []
        for vm in vms_running:
            print str(vm.name) + 'のsystemvm.isoをejectします。'

            try:
		(stdout, stderr, returncode) = self.model.exec_cmd_on_systemvm(vm.private_ip_address, 'eject /dev/sr0')
		result.append([vm.name, str(returncode)])
	    except:
	        print '対象\'' + str(vm.name) + '\'でのコマンド実行が失敗しました。'
		result.append([vm.name, None])
	        
        print ''

        # eject実行結果を出力する。
        table_after = []
        table_after.append(['name', 'return_code'])
        for vm in result:
            table_after.append(vm)

        self.view.check_eject_result(table_after)

        return True


def main(args):
    r"""
    ツールを実際実行する関数

    Parameters
    ----------
    args: namespace
        argparseモジュールで取得した、コマンド実行時に指定したパラメータ
    """

    # ログ設定をする。
    logging.basicConfig(level=logging.DEBUG, filename='logs/' + time.strftime('%Y%m%d%H%M%S') + '.log', filemode='w', format='%(asctime)s %(levelname)s [%(module)s.%(funcName)s] %(message)s')

    # コントローラを呼び出す。
    logging.debug('ツールが起動します。')
    controller = EjectController(args)
    controller.run()
    logging.debug('ツールが終了します。')



if __name__ == "__main__":
    r"""
    引数を確認してmain()関数を呼ぶ。
    """
    
    # コマンドの引数を設定する。
    parser = argparse.ArgumentParser(
        description='システムVM・VRのsystemvm.isoのeject実行ツール',
        usage='eject_cd_from_systemvm.py [-h] action -f FILE')
    parser.add_argument('action', help='eject: eject実行, check: eject済みか確認')
    parser.add_argument('-f', '--file', help='実行対象の一覧ファイル', required=True)
    args = parser.parse_args()

    # ログディレクトリを作成する。
    if not os.path.exists(LOGS):
        os.mkdir(LOGS)
        print('ログディレクトリ「' + LOGS + 'を作成しました。') 

    # main()関数を呼ぶ。
    main(args)

