#!/usr/bin/python
# -*- coding: UTF-8 -*-

r"""プライマリストレージ追加ツール
"""

import argparse
import sys

import common


def main(args):
    r"""
    main関数

    Parameters
    ----------

    args: namespace
        argparseモジュールで取得した、コマンド実行時に指定したパラメータ

    """
    line_a = '********************************************************************************'

    #コマンド実行時に指定したを取得
    list_file = args.file_path

    print line_a
    print 'ファイルを読み込みます。'
    pses_pre_load = common.load_csv_as_list_gen(list_file)

    print line_a
    print ''

    # Todo: 各行の列数確認がそもそも必要かどうか検討が必要
    print line_a
    print '各行の列数を確認します。'
    print ''
    verify_csv = common.check_columns(pses_pre_load)
    if verify_csv != True:
        print '以下の行が正常ではありません。ツールを終了します。'
        print ','.join(verify_csv)
        print ''
        sys.exit(1)
    else:
        print '全ての行の列数が一致しています'
    print ''

    print line_a
    print 'プライマリストレージの追加を開始します。'
    pses_failed = common.add_pses(common.load_csv_as_list_gen(list_file))
    print 'プライマリストレージの追加が完了しました。'
    print ''

    print line_a
    if pses_failed == []:
        print '全てが正常に登録できました。'
    else:
        print '以下のプライマリストレージは登録が失敗しました。'
        for row in pses_failed:
            print row[4], row[-1]

    print ''

    print line_a
    print 'スクリプトを終了します。'
    print ''

    sys.exit(0)



if __name__ == "__main__":
    r"""
    引数を確認してmain()関数を呼ぶ。
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('file_path', help='登録対象の一覧ファイル')
    args = parser.parse_args()

    main(args)
