# -*- coding: UTF-8 -*-

r"""このモジュールは以下のツールが利用する関数を纏めたモジュールである。

* プライマリストレージ一括追加ツール(add_ps.py)

"""

import csv
import subprocess
import sys
import re

# 検証環境では無い環境で実行する場合はテストのみの為、mysqlモジュールはインポートしない。
try:
    import mysql.connector
except:
    pass

import config


def add_pses(ps_list):
    r"""
    プライマリストレージを追加する。


    Parameters
    ----------
    ps_list: list
        [ ['SCOPE', 'ZONE', 'POD', 'CLUSTER', 'NAME', 'PROVIDER', 'URL', 'TAG'], ...]

    Returns
    -------
    追加が実行が失敗したプライマリストレージ情報: list

    Notes
    -----
    リスト「ps_list」の最初の要素はヘッダーの想定なので無視する。

    """

    # 登録の失敗したストレージ処理用変数
    pses_failed = []
    ps_failed = []
    # ヘッダーを無視する為に使われる。
    first_row = True
    # kick_api.sh実行結果確認用
    result_ok = False

    # kick_api.sh実行時のパラメータ
    command = 'createStoragePool'
    zoneid = ''
    podid = ''
    clusterid = ''
    name = ''
    provider = 'DefaultPrimary'
    url = ''
    tags = ''

    # kick_api.sh実行結果保存用変数
    kick_api_result = ()
    kick_api_result_stderr = ''


    # リスト「ps_list」の各行を処理する。
    for row in ps_list:
        # リスト「row」は以下のようになっている想定。
        # ['SCOPE', 'ZONE', 'POD', 'CLUSTER', 'NAME', 'PROVIDER', 'URL', 'TAG']

        # kick_api.sh実行結果確認用
        result_ok = False

        # kick_api.sh実行失敗時のログ収集用
        errortext = ''

        # 最初の行はヘッダーなので無視する。
        if first_row == True:
            first_row = False
            continue

        # 空白行があったら無視する。
        # この分岐を追加したのは、CSVファイルの最後の行の後ろに改行コードがあると、空白行が追加される為。
        if row == []:
            continue

        print '次のプライマリストレージを登録します。'
        print row
        print ''

        # CSDBから必要な情報を取ってくる。
        zoneid = get_uuid_by_name('data_center', row[1])
        podid = get_uuid_by_name('host_pod_ref', row[2])
        clusterid = get_uuid_by_name('cluster', row[3])

        # kick_api.sh実行用パラメータを準備する。
        kick_api_params = [
            'command=' + command,
            'zoneid=' + zoneid,
            'podid=' + podid,
            'clusterid=' + clusterid,
            'name=' + row[4],
            'provider=' + row[5],
            'url=' + row[6],
            'tags=' + row[7],
        ]

        # kick_api.sh実行
        print kick_api_params
        kick_api_result = exec_kick_api(kick_api_params)
        kick_api_result_stderr = kick_api_result[1].splitlines()
        kick_api_result = kick_api_result[0].splitlines()
        print kick_api_result
        print kick_api_result_stderr
        print ''

        # kick_api.sh実行結果確認
        for result_row in kick_api_result:
            if '<name>' + row[4] + '</name>' in result_row:
                result_ok = True
                break

        # 追加失敗時のログ収集
        if result_ok == False:
            for result_row in kick_api_result:
                if 'errortext' in result_row:
                    errortext = result_row
                    break

            ps_failed = row

            if errortext == '':
                ps_failed.append('実行ログ参照')
            else:
                ps_failed.append(errortext)

            pses_failed.append(ps_failed)

    return pses_failed


def check_columns(a_list):
    r"""
    リスト内のリストにNone、または空白のStringが存在するか、
    また、リスト内と各リストの要素数が同じか確認する。

    Parameters
    ----------
    a_list: list
        確認対象リスト

    Returns
    -------
    True: boolean
        確認で問題がない場合はTrueを返す。
    False: boolean
        該当のリスト内リストを返す。
    """

    first_row = True
    element_num = 0

    for row in a_list:
        if first_row == True:
            element_num = len(row)
            first_row = False
        else:
            for col in row:
                if col == None or col == '':
                    return row

    return True


def load_csv_as_list_gen(filename):
    r"""
    CSVファイルを読み込んでリストで返す。

    Parameters
    ----------
    filename: str
        ファイル名(フールパス可能)

    Yields
    ------
    row: list
        ファイルの各行を1行ずつ返す。

    """
    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            yield row


def exec_mysql(params):
    r"""
    mysqlコマンドを実行します。

    Parameters
    ----------
    params: dict
        mysqlコマンド実行時に必要な引数

    Returns
    -------
    SQL文実行結果: list
        [ ['1行目1列目', '1行目2列目', ... ], ['2行目1列目', '2行目2列目', ... ], ... ]

    """

    operation = params['sql'].split()[0].lower()

    # selectとupdateと処理が若干違うので分岐を作る。
    if operation == 'select':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        result = cursor.fetchall()

        cursor.close()
        cnx.close()

        # tupleをlistに、Noneをstrの'NULL'にする。
        return manipulate_mysql_result(result)

    elif operation == 'update':
        cnx = mysql.connector.connect(host=params['db_host'], user='root', database=params['database'])
        cursor = cnx.cursor()

        cursor.execute(params['sql'])

        cnx.commit()

        cursor.close()
        cnx.close()

        return True

    else:
        raise Exception('SQL文がSelect、もしくはUpdateではありません。指定されたのは: ' + operation)
        sys.exit(1)


def manipulate_mysql_result(raw_result):
    r"""
    mysql実行結果の各行をlistに変換し、Noneは'NULL'に変換する。

    Parameters
    ----------
    raw_result: list
        mysql.connectorでのfetchall()の結果

    Returns
    -------
    上記説明参照: result

    """

    result = []
    for i in raw_result:
        result.append(list(i))

    for i in range(len(result)):
        for j in range(len(result[i])):
            if result[i][j] == None:
                result[i][j] = 'NULL'
            if isinstance(result[i][j], int):
                result[i][j] = str(result[i][j])

    return result


def get_uuid_by_name(table,name):
    r"""
    DBの特定のテーブルで、名前(name)からUUID(uuid)を取得する。
    削除処理されたレコード(removed = NULL)は無視する。

    Parameters
    ----------
    table: str
        テーブル名
    name: str
        フィールド「name」に指定する名前

    Returns
    -------
    'UUID': str
        一致する結果が一個のみの場合に該当のUUIDを返す。
    False: boolean
        一致する結果が複数の場合に返す。
    'NULL': str
        結果がない場合に返す。
    """

    db_params = {
        'db_host' : config.DB_HOST,
        'db_user' : 'root',
        'database' : 'cloud',
        'sql' : 'select uuid from ' + table + ' where removed is null and name = "' + name + '";',
    }

    result = exec_mysql(db_params)

    if len(result) > 1:
        return False
    elif len(result) == 1 and len(result[0]):
        return result[0][0]
    else:
        return 'NULL'



def exec_kick_api(params):
    r"""
    kick_api.shを実行する。

    Parameters
    ----------
    params: list
        kick_api.shに指定するパラメータ
        ( 'パラメータ名=値', ... )

    Returns
    -------
    kick_api.shの実行結果: tuple
        ('レスポンス(XML)', 'curlコマンドの実行ステータス')

    """

    params.insert(0, config.KICK_API)
    command = params

    # コマンドを実行する。
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()

    # コマンド実行結果を回収する。
    p_stdout, p_stderr = p.communicate()

    return (p_stdout, p_stderr)
