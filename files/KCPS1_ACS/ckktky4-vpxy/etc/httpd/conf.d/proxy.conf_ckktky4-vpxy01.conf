<VirtualHost 10.189.0.21:80>

ServerName         10.189.0.21
DocumentRoot       /var/www/html

SetEnvIf       Remote_Addr 10.189.0.28 lblog
SetEnvIf       Remote_Addr 10.189.0.29 lblog

CustomLog      logs/access_log combined env=!lblog
CustomLog      logs/balancer_log balancer env=lblog

AllowEncodedSlashes      On
RewriteEngine            On

# for debug
RewriteLog "logs/rewrite_log"
RewriteLogLevel 0

<Location />
   <Limit PUT DELETE OPTIONS>
        Order deny,allow
        Deny from all
    </Limit>
</Location>

# reverse proxy filter
RewriteRule      ^/$ /portal/ [R]
RewriteRule      ^/help/(.*) /help/$1 [L]
RewriteRule ^/info/(.*) /info/$1 [L]

RewriteRule ^/client/index2.jsp /client/ [R]


# for maintenance
SetEnvIf Remote_Addr ^219\.117\.239\.166$ MAINTENANCE_PASS=yes
SetEnvIf Remote_Addr ^219\.117\.239\.180$ MAINTENANCE_PASS=yes
RewriteCond %{ENV:MAINTENANCE_PASS} !yes
RewriteCond /var/www/maintenance -f
RewriteRule /status$ /portal/status/index.html [L]
RewriteCond %{ENV:MAINTENANCE_PASS} !yes
RewriteCond /var/www/maintenance -f
RewriteRule /portal/status$ /portal/status/index.html [L]
RewriteCond %{ENV:MAINTENANCE_PASS} !yes
RewriteCond /var/www/maintenance -f
RewriteRule ^/portal/(.*) /maintenance/index.html [L]
RewriteCond %{ENV:MAINTENANCE_PASS} !yes
RewriteCond /var/www/maintenance -f
RewriteRule ^/client/(.*) /maintenance/index.html [L]
RewriteRule ^/maintenance/(.*)$ /maintenance/$1 [L]


# API filter
RewriteCond      %{QUERY_STRING} command=activateProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=addAccountToProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=addBaremetalDhcp [NC,OR]
RewriteCond      %{QUERY_STRING} command=addBaremetalHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=addBaremetalPxeKickStartServer [NC,OR]
RewriteCond      %{QUERY_STRING} command=addBaremetalPxePingServer [NC,OR]
RewriteCond      %{QUERY_STRING} command=addBigSwitchVnsDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=addCiscoAsa1000vResource [NC,OR]
RewriteCond      %{QUERY_STRING} command=addCiscoVnmcResource [NC,OR]
RewriteCond      %{QUERY_STRING} command=addCluster [NC,OR]
RewriteCond      %{QUERY_STRING} command=addExternalFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=addExternalLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=addF5LoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=addHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=addImageStore [NC,OR]
RewriteCond      %{QUERY_STRING} command=addLdapConfiguration [NC,OR]
RewriteCond      %{QUERY_STRING} command=addNetscalerLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=addNetworkDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=addNetworkServiceProvider [NC,OR]
RewriteCond      %{QUERY_STRING} command=addNiciraNvpDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=addPaloAltoFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=addRegion [NC,OR]
RewriteCond      %{QUERY_STRING} command=addResourceDetail [NC,OR]
RewriteCond      %{QUERY_STRING} command=addS3 [NC,OR]
RewriteCond      %{QUERY_STRING} command=addSecondaryStorage [NC,OR]
RewriteCond      %{QUERY_STRING} command=addSrxFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=addSwift [NC,OR]
RewriteCond      %{QUERY_STRING} command=addTrafficMonitor [NC,OR]
RewriteCond      %{QUERY_STRING} command=addTrafficType [NC,OR]
RewriteCond      %{QUERY_STRING} command=addUcsManager [NC,OR]
RewriteCond      %{QUERY_STRING} command=addVmwareDc [NC,OR]
RewriteCond      %{QUERY_STRING} command=addVpnUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=archiveAlerts [NC,OR]
RewriteCond      %{QUERY_STRING} command=assignCertToLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=assignToGlobalLoadBalancerRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=assignVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=associateLun [NC,OR]
RewriteCond      %{QUERY_STRING} command=associateUcsProfileToBlade [NC,OR]
RewriteCond      %{QUERY_STRING} command=authorizeSecurityGroupEgress [NC,OR]
RewriteCond      %{QUERY_STRING} command=authorizeSecurityGroupIngress [NC,OR]
RewriteCond      %{QUERY_STRING} command=cancelHostMaintenance [NC,OR]
RewriteCond      %{QUERY_STRING} command=cancelStorageMaintenance [NC,OR]
RewriteCond      %{QUERY_STRING} command=changeServiceForRouter [NC,OR]
RewriteCond      %{QUERY_STRING} command=changeServiceForSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=changeServiceForVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=cleanVMReservations [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureF5LoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureInternalLoadBalancerElement [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureNetscalerLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=configurePaloAltoFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureSimulator [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureSrxFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=configureVirtualRouterElement [NC,OR]
RewriteCond      %{QUERY_STRING} command=createAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=createAffinityGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=createAutoScalePolicy [NC,OR]
RewriteCond      %{QUERY_STRING} command=createAutoScaleVmGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=createAutoScaleVmProfile [NC,OR]
RewriteCond      %{QUERY_STRING} command=createCondition [NC,OR]
RewriteCond      %{QUERY_STRING} command=createCounter [NC,OR]
RewriteCond      %{QUERY_STRING} command=createDiskOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=createDomain [NC,OR]
RewriteCond      %{QUERY_STRING} command=createEgressFirewallRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=createGlobalLoadBalancerRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=createInstanceGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=createInternalLoadBalancerElement [NC,OR]
RewriteCond      %{QUERY_STRING} command=createLBHealthCheckPolicy [NC,OR]
RewriteCond      %{QUERY_STRING} command=createLunOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=createNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=createNetworkACL [NC,OR]
RewriteCond      %{QUERY_STRING} command=createNetworkACLList [NC,OR]
RewriteCond      %{QUERY_STRING} command=createNetworkOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=createPhysicalNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=createPod [NC,OR]
RewriteCond      %{QUERY_STRING} command=createPool [NC,OR]
RewriteCond      %{QUERY_STRING} command=createPortableIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=createPrivateGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=createProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=createRemoteAccessVpn [NC,OR]
RewriteCond      %{QUERY_STRING} command=createSecondaryStagingStore [NC,OR]
RewriteCond      %{QUERY_STRING} command=createSecurityGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=createServiceInstance [NC,OR]
RewriteCond      %{QUERY_STRING} command=createServiceOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=createStaticRoute [NC,OR]
RewriteCond      %{QUERY_STRING} command=createStorageNetworkIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=createStoragePool [NC,OR]
RewriteCond      %{QUERY_STRING} command=createUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVirtualRouterElement [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVlanIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVolumeOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVPC [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVPCOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVpnConnection [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVpnCustomerGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=createVpnGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=createZone [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicateCluster [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicateGuestVlanRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicateHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicatePod [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicatePublicIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=dedicateZone [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAccountFromProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAffinityGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAlerts [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAutoScalePolicy [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAutoScaleVmGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteAutoScaleVmProfile [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteBigSwitchVnsDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCiscoAsa1000vResource [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCiscoNexusVSM [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCiscoVnmcResource [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCluster [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCondition [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteCounter [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteDiskOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteDomain [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteEgressFirewallRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteExternalFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteExternalLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteF5LoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteGlobalLoadBalancerRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteImageStore [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteInstanceGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteLBHealthCheckPolicy [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteLdapConfiguration [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetscalerLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetworkACL [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetworkACLList [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetworkDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetworkOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNetworkServiceProvider [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteNiciraNvpDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePaloAltoFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePhysicalNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePod [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePool [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePortableIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=deletePrivateGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteProjectInvitation [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteRemoteAccessVpn [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteSecondaryStagingStore [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteSecurityGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteServiceOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteSrxFirewall [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteSslCert [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteStaticRoute [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteStorageNetworkIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteStoragePool [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteTrafficMonitor [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteTrafficType [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteUcsManager [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVlanIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVPC [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVPCOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVpnConnection [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVpnCustomerGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteVpnGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=deleteZone [NC,OR]
RewriteCond      %{QUERY_STRING} command=deployVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=destroyLunOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=destroyRouter [NC,OR]
RewriteCond      %{QUERY_STRING} command=destroySystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=destroyVolumeOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=disableAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=disableAutoScaleVmGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=disableCiscoNexusVSM [NC,OR]
RewriteCond      %{QUERY_STRING} command=disableUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=disassociateUcsProfileFromBlade [NC,OR]
RewriteCond      %{QUERY_STRING} command=dissociateLun [NC,OR]
RewriteCond      %{QUERY_STRING} command=enableAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=enableAutoScaleVmGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=enableCiscoNexusVSM [NC,OR]
RewriteCond      %{QUERY_STRING} command=enableStorageMaintenance [NC,OR]
RewriteCond      %{QUERY_STRING} command=enableUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=expungeVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=extractIso [NC,OR]
RewriteCond      %{QUERY_STRING} command=extractTemplate [NC,OR]
RewriteCond      %{QUERY_STRING} command=extractVolume [NC,OR]
RewriteCond      %{QUERY_STRING} command=findHostsForMigration [NC,OR]
RewriteCond      %{QUERY_STRING} command=findStoragePoolsForMigration [NC,OR]
RewriteCond      %{QUERY_STRING} command=generateAlert [NC,OR]
RewriteCond      %{QUERY_STRING} command=generateUsageRecords [NC,OR]
RewriteCond      %{QUERY_STRING} command=getApiLimit [NC,OR]
RewriteCond      %{QUERY_STRING} command=getCloudIdentifier [NC,OR]
RewriteCond      %{QUERY_STRING} command=getUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=importLdapUsers [NC,OR]
RewriteCond      %{QUERY_STRING} command=instantiateUcsTemplateAndAssocaciateToBlade [NC,OR]
RewriteCond      %{QUERY_STRING} command=ldapConfig [NC,OR]
RewriteCond      %{QUERY_STRING} command=ldapRemove [NC,OR]
RewriteCond      %{QUERY_STRING} command=ldapCreateAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=listAffinityGroups [NC,OR]
RewriteCond      %{QUERY_STRING} command=listAffinityGroupTypes [NC,OR]
RewriteCond      %{QUERY_STRING} command=listAlerts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listApis [NC,OR]
RewriteCond      %{QUERY_STRING} command=listAutoScalePolicies [NC,OR]
RewriteCond      %{QUERY_STRING} command=listAutoScaleVmProfiles [NC,OR]
RewriteCond      %{QUERY_STRING} command=listBaremetalDhcp [NC,OR]
RewriteCond      %{QUERY_STRING} command=listBaremetalPxeServers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listBigSwitchVnsDevices [NC,OR]
RewriteCond      %{QUERY_STRING} command=listCapacity [NC,OR]
RewriteCond      %{QUERY_STRING} command=listCiscoAsa1000vResources [NC,OR]
RewriteCond      %{QUERY_STRING} command=listCiscoNexusVSMs [NC,OR]
RewriteCond      %{QUERY_STRING} command=listCiscoVnmcResources [NC,OR]
RewriteCond      %{QUERY_STRING} command=listClusters [NC,OR]
RewriteCond      %{QUERY_STRING} command=listConditions [NC,OR]
RewriteCond      %{QUERY_STRING} command=listConfigurations [NC,OR]
RewriteCond      %{QUERY_STRING} command=listCounters [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDedicatedClusters [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDedicatedGuestVlanRanges [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDedicatedHosts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDedicatedPods [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDedicatedZones [NC,OR]
RewriteCond      %{QUERY_STRING} command=listDeploymentPlanners [NC,OR]
RewriteCond      %{QUERY_STRING} command=listExternalFirewalls [NC,OR]
RewriteCond      %{QUERY_STRING} command=listExternalLoadBalancers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listF5LoadBalancerNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listF5LoadBalancers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listGlobalLoadBalancerRules [NC,OR]
RewriteCond      %{QUERY_STRING} command=listHosts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listHypervisorCapabilities [NC,OR]
RewriteCond      %{QUERY_STRING} command=listImageStores [NC,OR]
RewriteCond      %{QUERY_STRING} command=listInstanceGroups [NC,OR]
RewriteCond      %{QUERY_STRING} command=listInternalLoadBalancerElements [NC,OR]
RewriteCond      %{QUERY_STRING} command=listInternalLoadBalancerVMs [NC,OR]
RewriteCond      %{QUERY_STRING} command=listLBHealthCheckPolicies [NC,OR]
RewriteCond      %{QUERY_STRING} command=listLdapConfigurations [NC,OR]
RewriteCond      %{QUERY_STRING} command=listLdapUsers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listLunsOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetscalerLoadBalancerNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetscalerLoadBalancers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetworkACLLists [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetworkACLs [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetworkDevice [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetworkIsolationMethods [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNetworkServiceProviders [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNiciraNvpDeviceNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listNiciraNvpDevices [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPaloAltoFirewallNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPaloAltoFirewalls [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPhysicalNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPods [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPools [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPortableIpRanges [NC,OR]
RewriteCond      %{QUERY_STRING} command=listPrivateGateways [NC,OR]
RewriteCond      %{QUERY_STRING} command=listProjectAccounts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listProjectInvitations [NC,OR]
RewriteCond      %{QUERY_STRING} command=listProjects [NC,OR]
RewriteCond      %{QUERY_STRING} command=listRegions [NC,OR]
RewriteCond      %{QUERY_STRING} command=listResourceDetails [NC,OR]
RewriteCond      %{QUERY_STRING} command=listRouters [NC,OR]
RewriteCond      %{QUERY_STRING} command=listS3s [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSecondaryStagingStores [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSecurityGroups [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSrxFirewallNetworks [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSrxFirewalls [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSslCerts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listStaticRoutes [NC,OR]
RewriteCond      %{QUERY_STRING} command=listStorageNetworkIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=listStoragePools [NC,OR]
RewriteCond      %{QUERY_STRING} command=listStorageProviders [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSupportedNetworkServices [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSwifts [NC,OR]
RewriteCond      %{QUERY_STRING} command=listSystemVms [NC,OR]
RewriteCond      %{QUERY_STRING} command=listTrafficMonitors [NC,OR]
RewriteCond      %{QUERY_STRING} command=listTrafficTypeImplementors [NC,OR]
RewriteCond      %{QUERY_STRING} command=listTrafficTypes [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUcsBlades [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUcsManagers [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUcsProfiles [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUcsTemplates [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUsageRecords [NC,OR]
RewriteCond      %{QUERY_STRING} command=listUsageTypes [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVirtualRouterElements [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVlanIpRanges [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVmwareDcs [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVolumesOnFiler [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVPCOfferings [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVpnConnections [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVpnCustomerGateways [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVpnGateways [NC,OR]
RewriteCond      %{QUERY_STRING} command=listVpnUsers [NC,OR]
RewriteCond      %{QUERY_STRING} command=lockAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=lockUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=markDefaultZoneForAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=migrateSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=migrateVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=migrateVirtualMachineWithVolume [NC,OR]
RewriteCond      %{QUERY_STRING} command=migrateVolume [NC,OR]
RewriteCond      %{QUERY_STRING} command=modifyPool [NC,OR]
RewriteCond      %{QUERY_STRING} command=prepareHostForMaintenance [NC,OR]
RewriteCond      %{QUERY_STRING} command=prepareTemplate [NC,OR]
RewriteCond      %{QUERY_STRING} command=rebootRouter [NC,OR]
RewriteCond      %{QUERY_STRING} command=rebootSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=reconnectHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=recoverVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=refreshUcsBlades [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseDedicatedCluster [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseDedicatedGuestVlanRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseDedicatedHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseDedicatedPod [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseDedicatedZone [NC,OR]
RewriteCond      %{QUERY_STRING} command=releaseHostReservation [NC,OR]
RewriteCond      %{QUERY_STRING} command=releasePublicIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeCertFromLoadBalancer [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeFromGlobalLoadBalancerRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeRegion [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeResourceDetail [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeVmwareDc [NC,OR]
RewriteCond      %{QUERY_STRING} command=removeVpnUser [NC,OR]
RewriteCond      %{QUERY_STRING} command=replaceNetworkACLList [NC,OR]
RewriteCond      %{QUERY_STRING} command=resetApiLimit [NC,OR]
RewriteCond      %{QUERY_STRING} command=resetSSHKeyForVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=resetVpnConnection [NC,OR]
RewriteCond      %{QUERY_STRING} command=restartVPC [NC,OR]
RewriteCond      %{QUERY_STRING} command=restoreVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=revertSnapshot [NC,OR]
RewriteCond      %{QUERY_STRING} command=revokeSecurityGroupEgress [NC,OR]
RewriteCond      %{QUERY_STRING} command=revokeSecurityGroupIngress [NC,OR]
RewriteCond      %{QUERY_STRING} command=scaleSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=scaleVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=startInternalLoadBalancerVM [NC,OR]
RewriteCond      %{QUERY_STRING} command=startRouter [NC,OR]
RewriteCond      %{QUERY_STRING} command=startSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=stopInternalLoadBalancerVM [NC,OR]
RewriteCond      %{QUERY_STRING} command=stopRouter [NC,OR]
RewriteCond      %{QUERY_STRING} command=stopSystemVm [NC,OR]
RewriteCond      %{QUERY_STRING} command=suspendProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateAccount [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateAutoScalePolicy [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateAutoScaleVmGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateAutoScaleVmProfile [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateCloudToUseObjectStore [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateCluster [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateConfiguration [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateDefaultNicForVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateDiskOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateDomain [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateGlobalLoadBalancerRule [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateHost [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateHostPassword [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateHypervisorCapabilities [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateInstanceGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateNetworkACLItem [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateNetworkOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateNetworkServiceProvider [NC,OR]
RewriteCond      %{QUERY_STRING} command=updatePhysicalNetwork [NC,OR]
RewriteCond      %{QUERY_STRING} command=updatePod [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateProject [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateProjectInvitation [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateRegion [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateResourceLimit [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateServiceOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateStorageNetworkIpRange [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateStoragePool [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateTrafficType [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVirtualMachine [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVMAffinityGroup [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVolume [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVPC [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVPCOffering [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateVpnCustomerGateway [NC,OR]
RewriteCond      %{QUERY_STRING} command=updateZone [NC,OR]
RewriteCond      %{QUERY_STRING} command=upgradeRouterTemplate [NC,OR]
RewriteCond      %{QUERY_STRING} command=uploadCustomCertificate [NC,OR]
RewriteCond      %{QUERY_STRING} command=uploadSslCert [NC,OR]
RewriteCond      %{QUERY_STRING} command=uploadVolume  [NC]
RewriteRule      ^/client/api$ /messages/general.error.asis

# create custom volume filter
RewriteCond      %{QUERY_STRING} command=createVolume
RewriteCond      %{QUERY_STRING} size=[0-9]+
RewriteCond      %{QUERY_STRING} !size=[0-9]+0
RewriteRule      ^/client/api$ /messages/createVolume.error.asis

# resize volume filter
RewriteCond %{QUERY_STRING} command=resizeVolume
RewriteCond %{QUERY_STRING} size=[0-9]+
RewriteCond %{QUERY_STRING} !size=[0-9]+0
RewriteRule  ^/client/api$ /messages/createVolume.error.asis

# list template filter
RewriteCond %{QUERY_STRING} command=listTemplates [NC]
RewriteCond %{QUERY_STRING} templatefilter=all [NC]
RewriteRule  ^/client/api$ /messages/general.error.asis


SecRuleEngine On
#SecRuleEngine DetectionOnly
SecRequestBodyAccess On
## Debug log
SecDebugLog logs/modsec_debug.log
SecDebugLogLevel 0

# Serial audit log
SecAuditEngine RelevantOnly
SecAuditLogRelevantStatus ^5
SecAuditLogParts ABIFHZ
SecAuditLogType Serial
SecAuditLog logs/modsec_audit.log

<Location /client/api>
SecRule ARGS:command "login" "chain,log,deny,status:400,msg:'permission denied'"
SecRule ARGS:username "@rx ^admin$"
SecRule ARGS:command "login" "chain,log,deny,status:400,msg:'permission denied'"
SecRule ARGS:username "@rx ^api$"
# deny apikey based api access
SecRule ARGS_NAMES "^apikey$" "log,deny,status:400,msg:'permission denied',id:1234568"
</Location>

<Location /portal>
SecRule ARGS:j_username "@rx ^admin$" "phase:2,log,redirect:/portal/auth/login.html"
SecRule ARGS:j_username "@rx ^admin@/$" "phase:2,log,redirect:/portal/auth/login.html"
SecRule	ARGS:j_username "@rx ^api$" "phase:2,log,redirect:/portal/"
SecRule	ARGS:j_username "@rx ^api@/$" "phase:2,log,redirect:/portal/"
</Location>

# forward to CS
ProxyPreserveHost      On
ProxyRequests          Off
ProxyPass             /portal http://10.189.0.33:18080/portal retry=0
ProxyPassReverse      /portal http://10.189.0.33:18080/portal
ProxyPass              / http://10.189.0.33:8080/ retry=0
ProxyPassReverse       / http://10.189.0.33:8080/
ProxyTimeout           900
SetEnv force-proxy-request-1.0 1
SetEnv proxy-nokeepalive 1


</VirtualHost>
