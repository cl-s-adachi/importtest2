#! /bin/sh

#
# Logging macro
#
# for Debug
LOGGER_INFO="/usr/bin/logger -s -p user.info -t ["$$":"`basename $0`"] Info: "
# for Production
LOGGER_NOTICE="/usr/bin/logger -p user.notice -t ["$$":"`basename $0`"] Notice: "
LOGGER_WARNING="/usr/bin/logger -p user.warning -t ["$$":"`basename $0`"] Warning: "
LOGGER_ERR="/usr/bin/logger -p user.err -t ["$$":"`basename $0`"] Error: "

# env check
# KCPS1
if [ -f /etc/init.d/cloud-management ] ; then 
	INIT_CMD=/etc/init.d/cloud-management
	PID_FILE=/var/run/cloud-management
	CONF_PATH=/etc/cloud/management/CloudStackManagementServerOperationMonitorEnv.sh
# KCPS2
else 
  if [ -f /etc/init.d/cloudstack-management ] ; then
	  INIT_CMD=/etc/init.d/cloudstack-management
	  PID_FILE=/var/run/cloudstack-management
	  CONF_PATH=/etc/cloudstack/management/CloudStackManagementServerOperationMonitorEnv.sh
  else 
	  $LOGGER_INFO "init script not found"
	  exit 1
  fi
fi


#
# PID check
#
if (( `ps -ef|grep $0|grep -v grep | wc -l` > 2 ));then
	$LOGGER_INFO "Dual boot check error"
        exit 1
fi

$LOGGER_NOTICE "Started"

#import conf
source ${CONF_PATH}

# CloudStackDB VIP
$LOGGER_INFO "DBVIP="$DBVIP

# NIC Device name for connecting to CloudStackDB VIP
$LOGGER_INFO "APNIC="$APNIC

#
# Get CloudStackDB VIP MAC address
/bin/ping -c 1 $DBVIP > /dev/null
MASTERMAC=`/sbin/arp -a -i $APNIC $DBVIP | /bin/awk '{print $4;}'`
$LOGGER_INFO "MASTERMAC="$MASTERMAC

#
# Give up count for restart CloudStack Management Server
# 
GIVEUP=10


#
# Give up count
#
COUNT=0


#
# Watch interval (unit: sec)
#
INTERVAL=3


#
# Wait for restart CloudStack Management Server (unit: sec)
#
RESTARTWAIT=30


#
# ping count
#
PINGCOUNT=3


while true
do
  #
  # Watch-1: Can this script communicate with CloudStackDB VIP?
  #   OK -> Go to Watch-2
  #   NG -> Stop CloudStack Management Server
  #
  $LOGGER_INFO "Can this script communicate with CloudStackDB VIP?"
  /bin/ping -c $PINGCOUNT $DBVIP > /dev/null
  if [ $? = 1 ]
  then
    $LOGGER_ERR "Watch-1: Communication is impossible with CloudStackDB VIP."

    # 2013/08/05  Watch-2 Comment
    #
    # Watch-2: Is CloudStack Management Server working?
    #   Working     -> Stop CloudStack Management Server
    #   Not working -> Do not anything
    #
    #$LOGGER_INFO "Watch-2: Is CloudStack Management Server working?"
    #if [ `/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep cloudstack-management | grep -v grep | wc -l` = 1 ]
    #then
    #  $LOGGER_WARNING "Since communication of MySQL VIP became impossible, CloudStack Management Server will be stopped."
    #  /etc/rc.d/init.d/cloudstack-usage stop
    #  $LOGGER_INFO "/etc/rc.d/init.d/cloudstack-usage stop"
    #  /etc/rc.d/init.d/cloudstack-management stop
    #  $LOGGER_INFO "/etc/rc.d/init.d/cloudstack-management stop"
    #  COUNT=0
    #  $LOGGER_INFO "Give up count="$COUNT
    #fi
  # OK:Connect to CloudStackDB VIP
  else
    #
    # Watch-3: Is CloudStack Management Server process detectable?
    #   Detected -> Go to Watch-4
    #   Not detected -> Start CloudStack Management Server
    #
    $LOGGER_INFO "Is CloudStack Management Server process detectable?"
    if [ `/bin/ps -ef | /bin/grep "org.apache.catalina.startup.Bootstrap" | grep cloud | /bin/grep -v grep | /usr/bin/wc -l` = 1 ]
    then
      #
      # Watch-4: MAC address acquired last time and this MAC address are the same ?
      #  Same -> Do not anything 	 
      #  Different -> Restart CloudStack Management Server
      #
      /bin/ping -c 1 $DBVIP > /dev/null
      GETMAC=`/sbin/arp -a -i $APNIC $DBVIP | /bin/awk '{print $4;}'`
      $LOGGER_INFO "GETMAC="$GETMAC
      if [ $MASTERMAC != $GETMAC ]
      then
        MASTERMAC=$GETMAC
        $LOGGER_INFO "MASTERMAC="$MASTERMAC
        $LOGGER_WARNING "Since CloudStackDB VIP MAC Address differ, CloudStack Management Server will be rebooted."
        ${INIT_CMD} restart
        # /etc/rc.d/init.d/cloudstack-usage restart
        #
        # Is CloudStack Management Server started?
        #   Yes -> Reset give up count
        #   No  -> Increase give up count
        #
        sleep $RESTARTWAIT
        if [ `/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep cloud | grep -v grep | wc -l` = 1 ]
        then
          $LOGGER_NOTICE "CloudStack Management Server has been started."
          COUNT=0
        else
          $LOGGER_WARNING "CloudStack Management Server was not able to be started."
          COUNT=`expr ${COUNT} + 1`
          $LOGGER_INFO "Give up count="$COUNT
        fi
      fi
    # Not detect CloudStack Management Server process
    else
      #
      # if gave up, do not anyting
      #
      $LOGGER_INFO "Give up count="$COUNT
      if [ $GIVEUP -ne $COUNT ]
      then
        $LOGGER_INFO "${INIT_CMD} start"
        ${INIT_CMD} start
        # $LOGGER_INFO "/etc/rc.d/init.d/cloudstack-usage start"
        # /etc/rc.d/init.d/cloudstack-usage start
        #
        # Is CloudStack Management Server started?
        #   Yes -> Reset give up count
        #   No  -> Increase give up count
        #
        sleep $RESTARTWAIT
        PID=`/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep cloud | grep -v grep | awk '{print $2}'`
        if [ x"${PID}" != "x"  ]
        then
          ${INIT_CMD} status
          if [ $? -eq 0 ] 
          then
              $LOGGER_NOTICE "CloudStack Management Server has been started."
              COUNT=0
          else
              $LOGGER_NOTICE "CloudStack Management Server has diffrent PID."
              echo ${PID} > ${PID_FILE}
              ${INIT_CMD} stop
              $LOGGER_WARNING "Since it gave up, CloudStack Management Server is not started."
          fi
        else
          $LOGGER_WARNING "CloudStack Management Server was not able to be started."
          COUNT=`expr ${COUNT} + 1`
        fi
        $LOGGER_INFO "Modified give up count="$COUNT
      else
        $LOGGER_WARNING "Since it gave up, CloudStack Management Server is not started."
      fi
    fi
  fi

  #
  # Spacing in time
  #
  sleep $INTERVAL
done
# EOF
