#! /bin/sh

#
# Logging macro
#
# for Debug
LOGGER_INFO="/usr/bin/logger -s -p user.info -t ["$$":"`basename $0`"] Info: "
# for Production
LOGGER_NOTICE="/usr/bin/logger -p user.notice -t ["$$":"`basename $0`"] Notice: "
LOGGER_WARNING="/usr/bin/logger -p user.warning -t ["$$":"`basename $0`"] Warning: "
LOGGER_ERR="/usr/bin/logger -p user.err -t ["$$":"`basename $0`"] Error: "

#
# PID check
#
if (( `ps -ef|grep $0|grep -v grep | wc -l` > 2 ));then
	$LOGGER_INFO "Dual boot check error"
        exit 1
fi

$LOGGER_NOTICE "Started"

#
# CloudStackDB VIP
# Production
#   DB1 eth2:0    Link encap:Ethernet  HWaddr 52:54:00:85:60:2C
#   DB2 eth2:0    Link encap:Ethernet  HWaddr 52:54:00:2C:EF:03
# CL Staging
#   eth0:1    Link encap:Ethernet  HWaddr 52:54:00:8E:1E:59
#
# -- Production
# DBVIP='10.189.0.15'
# -- CL Staging
DBVIP='10.189.0.15'
$LOGGER_INFO "DBVIP="$DBVIP

#
# NIC Device name for connecting to CloudStackDB VIP
# Production
#   AP1 eth2 inet addr:10.189.0.11  Bcast:10.189.0.31  Mask:255.255.255.224 HWaddr 52:54:00:E9:F3:7D
#   AP2 eth2 inet addr:10.189.0.12  Bcast:10.189.0.31  Mask:255.255.255.224 HWaddr 52:54:00:77:6D:C6
# CL Staging
#   eth0 inet addr:10.189.0.12  Bcast:10.189.0.255  Mask:255.255.255.0 HWaddr 52:54:00:8E:1E:59
#
# -- Production
# APNIC='eth2'
# -- CL Staging
APNIC='eth1'
$LOGGER_INFO "APNIC="$APNIC

#
# Get CloudStackDB VIP MAC address
#
# [ckkcl@ckktky4-vclweb01 ~]$ /sbin/arp -a -i eth2 10.189.0.15 | awk '{print $4;}'
# 52:54:00:85:60:2c
# [ckkcl@ckktky4-vclweb02 ~]$ /sbin/arp -a -i eth2 10.189.0.15 | awk '{print $4;}'
# 52:54:00:85:60:2c
#
/bin/ping -c 1 $DBVIP > /dev/null
MASTERMAC=`/sbin/arp -an -i $APNIC $DBVIP | /bin/awk '{print $4;}'`
$LOGGER_INFO "MASTERMAC="$MASTERMAC

#
# Give up count for restart CloudStack Management Server
# 
GIVEUP=10


#
# Give up count
#
COUNT=0


#
# Watch interval (unit: sec)
#
INTERVAL=3


#
# Wait for restart CloudStack Management Server (unit: sec)
#
RESTARTWAIT=30


#
# ping count
#
PINGCOUNT=3


while true
do
  #
  # Watch-1: Can this script communicate with CloudStackDB VIP?
  #   OK -> Go to Watch-2
  #   NG -> Stop CloudStack Management Server
  #
  $LOGGER_INFO "Can this script communicate with CloudStackDB VIP?"
  /bin/ping -c $PINGCOUNT $DBVIP > /dev/null
  if [ $? = 1 ]
  then
    $LOGGER_ERR "Watch-1: Communication is impossible with CloudStackDB VIP."

    # 2013/08/05  Watch-2 Comment
    #
    # Watch-2: Is CloudStack Management Server working?
    #   Working     -> Stop CloudStack Management Server
    #   Not working -> Do not anything
    #
    #$LOGGER_INFO "Watch-2: Is CloudStack Management Server working?"
    #if [ `/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep -v grep | wc -l` = 1 ]
    #then
    #  $LOGGER_WARNING "Since communication of MySQL VIP became impossible, CloudStack Management Server will be stopped."
    #  /etc/rc.d/init.d/cloud-usage stop
    #  $LOGGER_INFO "/etc/rc.d/init.d/cloud-usage stop"
    #  /etc/rc.d/init.d/cloud-management stop
    #  $LOGGER_INFO "/etc/rc.d/init.d/cloud-management stop"
    #  COUNT=0
    #  $LOGGER_INFO "Give up count="$COUNT
    #fi
  # OK:Connect to CloudStackDB VIP
  else
    #
    # Watch-3: Is CloudStack Management Server process detectable?
    #   Detected -> Go to Watch-4
    #   Not detected -> Start CloudStack Management Server
    #
    $LOGGER_INFO "Is CloudStack Management Server process detectable?"
    if [ `/bin/ps -ef | /bin/grep "org.apache.catalina.startup.Bootstrap" | /bin/grep -v grep | /usr/bin/wc -l` = 1 ]
    then
      #
      # Watch-4: MAC address acquired last time and this MAC address are the same ?
      #  Same -> Do not anything
      #  Different -> Restart CloudStack Management Server
      #
      /bin/ping -c 1 $DBVIP > /dev/null
      GETMAC=`/sbin/arp -an -i $APNIC $DBVIP | /bin/awk '{print $4;}'`
      $LOGGER_INFO "GETMAC="$GETMAC
      if [ $MASTERMAC != $GETMAC ]
      then
        MASTERMAC=$GETMAC
        $LOGGER_INFO "MASTERMAC="$MASTERMAC
        $LOGGER_WARNING "Since CloudStackDB VIP MAC Address differ, CloudStack Management Server will be rebooted."
        /etc/rc.d/init.d/cloud-management restart
        /etc/rc.d/init.d/cloud-usage restart
        #
        # Is CloudStack Management Server started?
        #   Yes -> Reset give up count
        #   No  -> Increase give up count
        #
        sleep $RESTARTWAIT
        if [ `/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep -v grep | wc -l` = 1 ]
        then
          $LOGGER_NOTICE "CloudStack Management Server has been started."
          COUNT=0
        else
          $LOGGER_WARNING "CloudStack Management Server was not able to be started."
          COUNT=`expr ${COUNT} + 1`
          $LOGGER_INFO "Give up count="$COUNT
        fi
      fi
    # Not detect CloudStack Management Server process
    else
      #
      # if gave up, do not anyting
      #
      $LOGGER_INFO "Give up count="$COUNT
      if [ $GIVEUP -ne $COUNT ]
      then
        $LOGGER_INFO "/etc/rc.d/init.d/cloud-management start"
        /etc/rc.d/init.d/cloud-management start
        $LOGGER_INFO "/etc/rc.d/init.d/cloud-usage start"
        /etc/rc.d/init.d/cloud-usage start
        #
        # Is CloudStack Management Server started?
        #   Yes -> Reset give up count
        #   No  -> Increase give up count
        #
        sleep $RESTARTWAIT
        if [ `/bin/ps -ef | grep "org.apache.catalina.startup.Bootstrap" | grep -v grep | wc -l` = 1 ]
        then
          $LOGGER_NOTICE "CloudStack Management Server has been started."
          COUNT=0
        else
          $LOGGER_WARNING "CloudStack Management Server was not able to be started."
          COUNT=`expr ${COUNT} + 1`
        fi
        $LOGGER_INFO "Modified give up count="$COUNT
      else
        $LOGGER_WARNING "Since it gave up, CloudStack Management Server is not started."
      fi
    fi
  fi

  #
  # Spacing in time
  #
  sleep $INTERVAL
done
# EOF
