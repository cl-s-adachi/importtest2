#!/bin/bash

date
echo "${0} start"

# リターンコード
rtn=0

# MySQLログ ローテートファイル
rotatefile=/backup/mysql-log-rotate

# バックアップ先ディレクトリ
dest=/backup/db_full

# バックアップ対象のデータディレクトリ
datadir_parent=/var/lib
datadir=mysql_backup

# ステータスファイル名
status_file=${dest}/full_backup_db_status

# タイムスタンプの取得
timestamp=`date +%Y%m%d`
judgeday=`date +%d`

# ログローテートの実行
/usr/sbin/logrotate ${rotatefile}

echo "RUNNING_full_backup_db" > ${status_file}
chmod 666 ${status_file}

# 以前のバックアップファイルを削除
outdated=${dest}/*.tar.gz

rm -f $outdated

# MySQLの停止
/etc/init.d/mysqld_backup stop

# バックアップ
cd ${datadir_parent}
tar cvfz ${dest}/${timestamp}.tar.gz ./${datadir}

if [ $? != 0 ]; then
  date
  echo "[ERROR] tar faild"
  echo "ERROR_full_backup_db" > ${status_file}
  chmod 666 ${status_file}
  rtn=1
else
  date
  echo "SUCCESS_full_backup_db" > ${status_file}
  chmod 666 ${status_file}
fi

# MySQLの開始
/etc/init.d/mysqld_backup start

flag=true
count=0
while [ ${flag} == "true" -a ${count} -ne  3 ]
do
/etc/init.d/mysqld_backup status
if [ $? != 0 ]; then
    # MySQLの開始
    /etc/init.d/mysqld_backup start
    count=`expr ${count} + 1`
	sleep 60
else
    flag=false
fi
done

echo "${0} end"

exit ${rtn}
