#!/bin/bash

date
echo "${0} start"

# バックアップ先ディレクトリ
dest=/backup/db_binlog/

# バックアップ対象のデータディレクトリ
datadir_parent=/var/lib
datadir=mysql_backup

# ログのフラッシュ
# (RELOAD権限を持ったパスワード"backup"のbackupユーザをあらかじめ作成しておく)
/usr/bin/mysql -u batch -S ${datadir_parent}/${datadir}/mysql.sock -e "flush logs" -pbatch

if [ $? != 0 ]; then
  date
  echo "[ERROR] mysql command faild"
  exit 1
fi

rsync -a -r -v --delete --include="${datadir}/" --include="${datadir}/binlog.*" --exclude="*" ${datadir_parent}/${datadir} ${dest}

if [ $? != 0 ]; then
  date
  echo "[ERROR] rsync faild"
  exit 1
fi

date
echo "${0} end"

