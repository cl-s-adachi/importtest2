#!/bin/bash

date
echo "${0} start"

INSTANCE_NAME="mysql_backup"

# (REPLICATION CLIENT権限を持ったパスワード"rplchk"のrplchkユーザを
# あらかじめ作成しておく)
CHECKER_USER="rplchk"
CHECKER_PASS="rplchk"

CHECK_COMMAND="SHOW SLAVE STATUS\\G";
ALLOW_DELAY_SECONDS=30;

echo "$CHECK_COMMAND";
slave_status=`/usr/bin/mysql -u ${CHECKER_USER} -S /var/lib/${INSTANCE_NAME}/mysql.sock -e "${CHECK_COMMAND}" -p${CHECKER_PASS}`;
echo "$slave_status";

# set variables from status
eval "`echo "$slave_status" | sed -ne 's/: \(.*\)/="\1"/p'`";

if [ "$Slave_IO_Running" != "Yes"  ]; then
    msg="Slave_IO_Running : $Slave_IO_Running\n";
fi

if [ "$Slave_SQL_Running" != "Yes" ]; then
    msg="${msg}Slave_SQL_Running : $Slave_SQL_Running\n";
fi

if [ "$Last_Errno" != "0" ]; then
    msg="${msg}Last_Error : [${Last_Errno}] $Last_Error\n";
fi

if [ "$Seconds_Behind_Master" -gt $ALLOW_DELAY_SECONDS ]; then
    msg="${msg}Seconds_Behind_Master : $Seconds_Behind_Master\n";
fi

if [ "$msg" != "" ]; then
  date
  echo "[ERROR] find slave error"
  echo "$msg"
  exit 1
fi

date
echo "${0} end"

